﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trReAssignHisRep : IRepository<trReAssignHi>, IDisposable
    {
        private readonly LegalSystemEntities _context;
        public trReAssignHisRep()
        {
            _context = new LegalSystemEntities();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public List<trReAssignHi> GetbyCondition(string RequestNo)
        {
            return _context.trReAssignHis.OrderBy(D => D.ReAssignDate).Where(w => w.RequestNo == RequestNo).ToList();
        }
        public void DeleteData(trReAssignHi model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        

        public List<trReAssignHi> GetAll()
        {
            throw new NotImplementedException();
        }

        public void InsertData(trReAssignHi model)
        {
            try
            {
                _context.trReAssignHis.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trReAssignHi> SearchData(trReAssignHi model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(trReAssignHi model)
        {
            try
            {
                _context.trReAssignHis.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public trReAssignHi GetbySeq(string RequestNo)
        {
            return _context.trReAssignHis.OrderByDescending(D => D.ReAssignDate).Where(w => w.RequestNo == RequestNo).FirstOrDefault();
        }
    }
}
