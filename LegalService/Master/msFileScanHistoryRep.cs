﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msFileScanHistoryRep : IRepository<msFileScanHistory>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public msFileScanHistoryRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msFileScanHistory model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScanHistory> GetAll()
        {
            return _context.msFileScanHistories.ToList();
        }

        public void InsertData(msFileScanHistory model)
        {
            try
            {
                _context.msFileScanHistories.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScanHistory> SearchData(msFileScanHistory model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msFileScanHistory model)
        {
            try
            {
                _context.msFileScanHistories.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
