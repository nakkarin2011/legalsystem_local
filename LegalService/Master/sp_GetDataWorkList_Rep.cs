﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class sp_GetDataWorkList_Rep : sp_GetDataWorkList_Result,IDisposable
    {
        private readonly LegalSystemEntities _context;
        public sp_GetDataWorkList_Rep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<LegalData.sp_GetDataWorkList_Result> GetAll(string Acc,string CIF,string DPD,string ACName)
        {
            return _context.sp_GetDataWorkList(Acc, CIF, DPD, ACName).OrderBy(O=>O.PDDAYS).ToList();
        }
       
    }
}

