﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService.View
{
    public class FixCostRep : BaseRepository<FixCost, LegalDataContext>
    {
        public List<FixCost> GetAllFixCost()
        {
            return this.SelectAll().OrderBy(D => D.TypeID).ThenBy(D => D.SubTypeCode).ToList();
        }

        public FixCost GetFixCostBySubType(int TypeID, string SubTypeCode)
        {
            return this.SelectDataWithCodition(D => D.TypeID == TypeID && D.SubTypeCode == SubTypeCode).FirstOrDefault();
        }

        public void insertData(FixCost data)
        {
            int newid = 1;
            List<FixCost> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.FixID).Max() + 1;
            }
            data.FixID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(FixCost data)
        {
            this.DeleteDataWithCodition(D => D.FixID == data.FixID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int FixID)
        {
            this.DeleteDataWithCodition(D => D.FixID == FixID);
            this.SummitChages();
        }
    }
}
