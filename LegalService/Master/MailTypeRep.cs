﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class MailTypeRep : IRepository<msMailType>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public MailTypeRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }


        public void DeleteData(msMailType model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailType> GetAll()
        {
            return _context.msMailTypes.ToList();
        }

        public void InsertData(msMailType model)
        {
            try
            {
                _context.msMailTypes.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailType> SearchData(msMailType model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msMailType model)
        {
            try
            {
                _context.msMailTypes.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
