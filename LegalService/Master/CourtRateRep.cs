﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CourtRateRep : BaseRepository<CourtRate, LegalDataContext>
    {
        public List<CourtRate> GetAllCourtRate()
        {
            List<CourtRate> list;
            try
            {
                list = this.SelectAll().OrderBy(D => D.MinAmt).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list; 
        }

        public List<CourtRate> GetCourtRateWithCondition(string scondition)
        {
            return this.SelectDataWithCodition(D => D.MinAmt.ToString().Contains(scondition)
                                                    || D.MaxAmt.ToString().Contains(scondition)
                                                    || D.Percentage.ToString().Contains(scondition)
                                                    || D.Condition.Contains(scondition)
                                                    || D.Devide.ToString().Contains(scondition)
                                                    || D.PayAmount.ToString().Contains(scondition)).OrderBy(D => D.MinAmt).ToList();
        }

        public void insertData(CourtRate data)
        {
            int newid = 1;
            List<CourtRate> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CourtRateID).Max() + 1;
            }
            data.CourtRateID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CourtRate data)
        {
            this.DeleteDataWithCodition(D => D.CourtRateID == data.CourtRateID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CourtRateID)
        {
            this.DeleteDataWithCodition(D => D.CourtRateID == CourtRateID);
            this.SummitChages();
        }
    }
}
