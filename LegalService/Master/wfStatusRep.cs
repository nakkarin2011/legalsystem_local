﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class wfStatusRep : BaseRepository<wfStatus, LegalDataContext>
    {
        public List<wfStatus> GetAllRequestStatus(int Version)
        {
            List<wfStatus> S = new List<wfStatus>();
            S.Add(new wfStatus() { StatusID = -1, StatusName = "- Please Select -" });
            S.AddRange(this.SelectDataWithCodition(D => D.Version == Version && D.Flag == "R").OrderBy(D => D.StatusID).ToList());
            return S;
        }

        public wfStatus GetRequestStatusByID(int Version, int StatusID)
        {
            return this.SelectDataWithCodition(D => D.Version == Version && D.Flag == "R" && D.StatusID == StatusID).FirstOrDefault();
        }

        public List<wfStatus> GetAllLegalStatus(int Version)
        {
            List<wfStatus> S = new List<wfStatus>();
            S.Add(new wfStatus() { StatusID = -1, StatusName = "- Please Select -" });
            S.AddRange(this.SelectDataWithCodition(D => D.Version == Version && D.Flag == "L").OrderBy(D => D.StatusID).ToList());
            return S;
        }

        public wfStatus GetLegalStatusByID(int Version, int StatusID)
        {
            return this.SelectDataWithCodition(D => D.Version == Version && D.Flag == "L" && D.StatusID == StatusID).FirstOrDefault();
        }
    }
}
