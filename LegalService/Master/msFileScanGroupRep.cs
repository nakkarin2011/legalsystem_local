﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msFileScanGroupRep : IRepository<msFileScanGroup>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public msFileScanGroupRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msFileScanGroup model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScanGroup> GetAll()
        {
            return _context.msFileScanGroups.ToList();
        }

        public void InsertData(msFileScanGroup model)
        {
            try
            {
                _context.msFileScanGroups.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScanGroup> SearchData(msFileScanGroup model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msFileScanGroup model)
        {
            try
            {
                _context.msFileScanGroups.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
