﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalService
{
    public class CostTypeRep : BaseRepository<CostType, LegalDataContext> 
    {
        public void insertData(CostType data)
        {
            int newid = 1;
            List<CostType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CostTypeID).Max() + 1;
            }
            data.CostTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CostType data)
        {
            this.DeleteDataWithCodition(D => D.CostTypeID == data.CostTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CostTypeID)
        {
            this.DeleteDataWithCodition(D => D.CostTypeID == CostTypeID);
            this.SummitChages();
        }
    }
}
