﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class ProdTypeRep : IRepository<LegalData.msProductType>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public ProdTypeRep ()
        {
            _context = new LegalSystemEntities();
        }

        public void DeleteData(msProductType model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<msProductType> GetAll()
        {
            try
            {
                var _result = from a in _context.msProductTypes.ToList()
                              select new msProductType()
                              {
                                 PrdTypeID = a.PrdTypeID,
                                 PrdTypeName = a.PrdTypeName,
                                 PrdTypeDesc = a.PrdTypeDesc
                              };
                return _result.ToList();
            }
            catch
            {
                throw;
            }
        }

        public void InsertData(msProductType model)
        {
            try
            {
                _context.msProductTypes.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msProductType> SearchData(msProductType model)
        {
            return _context.msProductTypes.Where(a => a.PrdTypeName.Contains(model.PrdTypeName.Trim())).ToList();
        }

        public void UpdateData(msProductType model)
        {
            try
            {
                _context.msProductTypes.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
