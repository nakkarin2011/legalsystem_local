﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CourtJudgementRep : BaseRepository<CourtJudgement, LegalDataContext> 
    {
        public List<CourtJudgement> GetAllCourtJudgement()
        {
            return this.SelectAll().OrderBy(D => D.CJName).ToList();
        }

        public List<CourtJudgement> GetCourtJudgementWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.CJName.Contains(condition)).OrderBy(D => D.CJName).ToList();
        }

        public void insertData(CourtJudgement data)
        {
            int newid = 1;
            List<CourtJudgement> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CJID).Max() + 1;
            }
            data.CJID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CourtJudgement data)
        {
            this.DeleteDataWithCodition(D => D.CJID == data.CJID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CJID)
        {
            this.DeleteDataWithCodition(D => D.CJID == CJID);
            this.SummitChages();
        }
    }
}