﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class DocTypeRep : IRepository<DocumentType>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public DocTypeRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void UpdateData(DocumentType model)
        {
            try
            {
                _context.DocumentTypes.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public void DeleteData(DocumentType model)
        {
           try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void InsertData(DocumentType model)
        {
            try
            {
                _context.DocumentTypes.Add(model);
                _context.SaveChanges();
            }
            catch 
            {
                throw;
            }
        }

        public List<DocumentType> GetAll()
        {
            var _result = from a in _context.DocumentTypes.ToList()
                          select new DocumentType()
                          {
                              DocTypeID = a.DocTypeID,
                              DocTypeName = a.DocTypeName
                          };

            return _result.ToList();
        }

        public List<DocumentType> BindDDLExceptData()
        {
            var list = (from r in _context.DocumentTypes
                        where !_context.DocumentTypeMappings.Any(f => f.DocTypeID == r.DocTypeID)
                        select r).ToList();
            return list;
        }
        public List<DocumentType> SearchData(DocumentType model)
        {
            return _context.DocumentTypes.Where(a => a.DocTypeName.Contains(model.DocTypeName)).ToList();
        }
    }
}
