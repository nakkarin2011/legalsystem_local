﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
   public class AttorneyRep : BaseRepository<Attorney, LegalDataContext>
    {
        public void insertData(Attorney data)
        {
            int newid = 1;
            List<Attorney> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.AttorneyID).Max() + 1;
            }
            data.AttorneyID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int ReqMapID)
        {
            this.DeleteDataWithCodition(D => D.AttorneyID == ReqMapID);
            this.SummitChages();
        }
    }
}
