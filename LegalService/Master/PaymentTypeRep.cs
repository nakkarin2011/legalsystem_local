﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class PaymentTypeRep : BaseRepository<PaymentType, LegalDataContext>
    {
        public List<PaymentType> GetAllPaymentType()
        {
            return this.SelectAll().OrderBy(D => D.PaymentTypeName).ToList();
        }

        public List<PaymentType> GetPaymentTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.PaymentTypeName.Contains(condition)).OrderBy(D => D.PaymentTypeName).ToList();
        }

        public List<PaymentType> GetDDLPaymentType()
        {
            List<PaymentType> R = new List<PaymentType>();
            R.Add(new PaymentType() { PaymentTypeID = -1, PaymentTypeName = "- Please Select -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.PaymentTypeID).ToList());
            return R;
        }

        public void insertData(PaymentType data)
        {
            int newid = 1;
            List<PaymentType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.PaymentTypeID).Max() + 1;
            }
            data.PaymentTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(PaymentType data)
        {
            this.DeleteDataWithCodition(D => D.PaymentTypeID == data.PaymentTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int PaymentTypeID)
        {
            this.DeleteDataWithCodition(D => D.PaymentTypeID == PaymentTypeID);
            this.SummitChages();
        }
    }
}
