﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Globalization;

namespace LegalService
{
    public class trLegalRep:GenericRepository<trLegal>
    {
        LegalDbContext _legalDbContext;

        public trLegalRep(LegalDbContext legalDbContext)
            : base(legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public trLegal GetLegalByLegalNo(string LegalNo)
        {
            try
            {
                return this.Get(x => x.LegalNo == LegalNo).First();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void updateDataByField_tab1(trLegal data)
        {
            try
            {
                trLegal val =this.Get(x => x.LegalNo == data.LegalNo).FirstOrDefault();
                if (val != null)
                {
                    val.T1_GenCallingDocDate = data.T1_GenCallingDocDate;
                    val.T1_LawyerID = data.T1_LawyerID;
                    val.T1_ReceiveDocDate = data.T1_ReceiveDocDate;
                    val.T1_ReceiveReqDate = data.T1_ReceiveReqDate;
                    val.T1_SendDocDate = data.T1_SendDocDate;
                    val.T1_SendToLawyerDate = data.T1_SendToLawyerDate;
                    val.UpdateBy = data.UpdateBy;
                    val.UpdateDateTime = data.UpdateDateTime;

                    dbSet.Attach(val);
                    _legalDbContext.Entry(val).State = EntityState.Modified;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updateDataByField_tab2(trLegal data)
        {
            try
            {
                trLegal val = _legalDbContext.trLegal.Where(x => x.LegalNo == data.LegalNo).FirstOrDefault();
                if (val != null)
                {
                    val.T2_AssignSueDate = data.T2_AssignSueDate;
                    val.T2_GenNoticeDate = data.T2_GenNoticeDate;
                    val.T2_SueDate = data.T2_SueDate;
                    val.UpdateBy = data.UpdateBy;
                    val.UpdateDateTime = data.UpdateDateTime;

                    dbSet.Attach(val);
                    _legalDbContext.Entry(val).State = EntityState.Modified;
                }
            }
            catch (Exception)
            {
                throw;
            }


        }

        public void updateDataByField_tab3(trLegal data)
        {
            try
            {
                trLegal val = _legalDbContext.trLegal.Where(x => x.LegalNo == data.LegalNo).FirstOrDefault();
                if (val != null)
                {
                    val.T3_AppAssetAmt = data.T3_AppAssetAmt;
                    val.T3_AppDate = data.T3_AppDate;
                    val.T3_AppDecidedCase = data.T3_AppDecidedCase;
                    val.T3_Appeal = data.T3_Appeal;
                    val.T3_AppFee = data.T3_AppFee;
                    val.T3_AppJudgeDate = data.T3_AppJudgeDate;
                    val.T3_AppSendAdjudeDate = data.T3_AppSendAdjudeDate;
                    val.T3_AppUndecidedCase = data.T3_AppUndecidedCase;
                    val.T3_AssetAmt = data.T3_AssetAmt;
                    val.T3_BendJudgeDate = data.T3_BendJudgeDate;
                    val.T3_CourtFee = data.T3_CourtFee;
                    val.T3_CourtJudge = data.T3_CourtJudge;
                    val.T3_CourtZoneID = data.T3_CourtZoneID;
                    val.T3_DecidedCase = data.T3_DecidedCase;
                    val.T3_JudgeDesc = data.T3_JudgeDesc;
                    val.T3_JudgeRight = data.T3_JudgeRight;
                    val.T3_PetAssetAmt = data.T3_PetAssetAmt;
                    val.T3_PetDate = data.T3_PetDate;
                    val.T3_PetDecidedCase = data.T3_PetDecidedCase;
                    val.T3_PetFee = data.T3_PetFee;
                    val.T3_Petition = data.T3_Petition;
                    val.T3_PetJudgeDate = data.T3_PetJudgeDate;
                    val.T3_PetSendAdjudeDate = data.T3_PetSendAdjudeDate;
                    val.T3_PetUndecidedCase = data.T3_PetUndecidedCase;
                    val.T3_PlaintReason = data.T3_PlaintReason;
                    val.T3_PlaintRight = data.T3_PlaintRight;
                    val.T3_Racha = data.T3_Racha;
                    val.T3_UndecidedCase = data.T3_UndecidedCase;
                    val.UpdateBy = data.UpdateBy;
                    val.UpdateDateTime = data.UpdateDateTime;

                    dbSet.Attach(val);
                    _legalDbContext.Entry(val).State = EntityState.Modified;
                }
            }
            catch (Exception)
            {
                throw;
            }


        }

        public void updateDataByField_tab4(trLegal data)
        {
            try
            {
                trLegal val = _legalDbContext.trLegal.Where(x => x.LegalNo == data.LegalNo).FirstOrDefault();
                if (val != null)
                {
                    val.T4_GenSubpoenaDate = data.T4_GenSubpoenaDate;
                    val.T4_GotExecutionDate = data.T4_GotExecutionDate;
                    val.T4_LawyerID = data.T4_LawyerID;
                    val.T4_SendToLawyerDate = data.T4_SendToLawyerDate;
                    val.UpdateBy = data.UpdateBy;
                    val.UpdateDateTime = data.UpdateDateTime;

                    dbSet.Attach(val);
                    _legalDbContext.Entry(val).State = EntityState.Modified;
                }
            }
            catch (Exception)
            {
                throw;
            }


        }

        public void updateDataByField_tab5(trLegal data)
        {
            try
            {
                trLegal val = _legalDbContext.trLegal.Where(x => x.LegalNo == data.LegalNo).FirstOrDefault();
                if (val != null)
                {
                    val.T5_PLawyerID = data.T5_PLawyerID;
                    val.T5_PSendtoLawyerDate = data.T5_PSendtoLawyerDate;
                    val.T5_SLawyerID = data.T5_SLawyerID;
                    val.T5_SSendtoLawyerDate = data.T5_SSendtoLawyerDate;
                    val.UpdateBy = data.UpdateBy;
                    val.UpdateDateTime = data.UpdateDateTime;

                    dbSet.Attach(val);
                    _legalDbContext.Entry(val).State = EntityState.Modified;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetNewLegalID()
        {         
            trLegal LgNo = this.Get(D => D.LegalNo != null && D.CreateDateTime.Year == DateTime.Today.Year &&!D.LegalNo.Contains("NOT")).OrderByDescending(D => D.LegalNo).FirstOrDefault();
            if (LgNo != null)
            {
                string LeNo = LgNo.LegalNo.Substring(7).Replace("0","");
                int LN = Convert.ToInt32(LeNo)+1;
                return string.Format("LEG{0}{1}", DateTime.Today.Year,(LN).ToString().PadLeft(5, '0'));
            }
            else
                return string.Format("LEG{0}{1}", DateTime.Today.Year, "00001");           
        }
        public string GetNewNoticeLegalNo()
        {
            trLegal LgNo = this.Get(D => D.LegalNo != null && D.CreateDateTime.Year == DateTime.Today.Year && D.LegalNo.Contains("NOT")).OrderByDescending(D => D.LegalNo).FirstOrDefault();
            if (LgNo != null)
            {
                string LeNo = LgNo.LegalNo.Substring(7).Replace("0", "");
                int LN = Convert.ToInt32(LeNo);
                return string.Format("NOT{0}{1}", DateTime.Today.Year, (LN + 1).ToString().PadLeft(5, '0'));
            }
            else
                return string.Format("NOT{0}{1}", DateTime.Today.Year, "00001");
        }

    }
}
