﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trSequesterRep
    {
        internal DbSet<trSequester> dbSet;
        LegalDbContext _legalDbContext;
        public trSequesterRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trSequester>();
        }
        public trSequesterRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trSequester>();
        }

        public List<trSequester> GetAllSequester(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.SeqTime).ToList();

            List < trSequester > DBSet;
            try
            {
                DBSet = _legalDbContext.trSequester.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.SeqTime).ToList();
                return DBSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public trSequester GetSequesterByID(int SequesterID)
        {
            //return this.SelectDataWithCodition(D => D.SequesterID == SequesterID).FirstOrDefault();

            try
            {
                trSequester entity = _legalDbContext.trSequester.Where(x => x.SequesterID == SequesterID).FirstOrDefault();
                return entity;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        //public void insertData(trSequester data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trSequester data)
        //{
        //    this.DeleteDataWithCodition(D => D.SequesterID == data.SequesterID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Contains(LegalNo));
        //    this.SummitChages();
        //}
    }
}