﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class StatementAccTypeRep : BaseRepository<StatementAccType, LegalDataContext>
    {
        public List<StatementAccType> GetAllStatementAccType()
        {
            return this.SelectAll().OrderBy(D => D.StatementName).ToList();
        }

        public List<StatementAccType> GetStatementAccTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.StatementName.Contains(condition)).OrderBy(D => D.StatementName).ToList();
        }

        public void insertData(StatementAccType data)
        {
            int newid = 1;
            List<StatementAccType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.StatementID).Max() + 1;
            }
            data.StatementID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(StatementAccType data)
        {
            this.DeleteDataWithCodition(D => D.StatementID == data.StatementID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int StatementID)
        {
            this.DeleteDataWithCodition(D => D.StatementID == StatementID);
            this.SummitChages();
        }
    }
}
