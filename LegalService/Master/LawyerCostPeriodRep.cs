﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LegalService
{
    public class LawyerCostPeriodRep : BaseRepository<Payment, LegalDataContext>
    {
        public void insertData(Payment data)
        {
            int newid = 1;
            List<Payment> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.PaymentID).Max() + 1;
            }
            data.PaymentID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(Payment data)
        {
            this.DeleteDataWithCodition(D => D.PaymentID == data.PaymentID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int PaymentID)
        {
            this.DeleteDataWithCodition(D => D.PaymentID == PaymentID);
            this.SummitChages();
        }
    }
}
