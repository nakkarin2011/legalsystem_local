﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class EmpDetailRep : BaseRepository<EmpDetail, LegalDataContext>
    {
        public List<EmpDetail> GetAllEmpDetail()
        {
            List<EmpDetail> empList = new List<EmpDetail>();
            empList = this.SelectAll().OrderBy(D => D.EmpName).ThenBy(D => D.EmpCode).ThenBy(D => D.EmpEmail).ToList();
            return empList;
        }

        public List<EmpDetail> GetEmpDetailWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.EmpCode.Contains(condition)
                || D.EmpName.Contains(condition)
                || D.EmpEmail.Contains(condition)).OrderBy(D => D.EmpName).ThenBy(D => D.EmpCode).ThenBy(D => D.EmpEmail).ToList();
        }

        public void insertData(EmpDetail data)
        {
            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(EmpDetail data)
        {
            this.DeleteDataWithCodition(D => D.EmpCode == data.EmpCode);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(string EmpCode)
        {
            this.DeleteDataWithCodition(D => D.EmpCode == EmpCode);
            this.SummitChages();
        }
    }
}
