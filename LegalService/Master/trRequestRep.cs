﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trRequestRep :GenericRepository<trRequest>
    {
        LegalDbContext _legalDbContext;
        public trRequestRep(LegalDbContext context):base(context)
        {
            _legalDbContext = new LegalDbContext();
        }
        

        public trRequest GetRequestDataByRequestNo(string RequestNo)
        {
            try
            {
                trRequest list =this.Get(x => x.RequestNo == RequestNo).First();
                return list;
            }
            catch (Exception)
            {
                throw;
            }   
        }
        public trRequest GetRequestByID(string RequestId)
        {
            if (string.IsNullOrEmpty(RequestId))
                return null;
            return this.Get(D => D.RequestID == new Guid(RequestId)).FirstOrDefault();
        }

        public List<trRequest> GetRequestByCIF(string CIF)
        {
            decimal cif = Convert.ToDecimal(CIF);
            return this.Get(D => D.CifNo == cif).ToList();
        }

        public string GetNewID()
        {
            trRequest R = this.Get(D => D.RequestDate.Value.Year == DateTime.Today.Year).OrderByDescending(D => D.RNo).FirstOrDefault();
            if (R != null)
                return string.Format("COL{0}{1}", DateTime.Today.Year, (R.RNo + 1).ToString().PadLeft(5, '0'));
            else
                return string.Format("COL{0}{1}", DateTime.Today.Year, "00001");
        }

        public string GetCaseEditID(string RequestNo)
        {
            trRequest R = new trRequest() ;
            R = this.Get(D => D.RequestNo.Contains("R")).OrderByDescending(D => D.RequestNo).FirstOrDefault();
            if (R != null)
            {
                int RNORefer = Convert.ToInt32(R.RequestNo.Substring(1, 2));
                return string.Format("R{0}{1}", (RNORefer + 1).ToString().PadLeft(2, '0'), R.RequestNo.Substring(3));
            }
            else
                return string.Format("R01{0}", RequestNo.Substring(3));
        }

        

    }
}
