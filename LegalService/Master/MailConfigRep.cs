﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class MailConfigRep : IRepository<msMailConfig>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public MailConfigRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msMailConfig model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailConfig> GetAll()
        {
            return _context.msMailConfigs.ToList();
        }

        public List<vEmployee> ListEmployee()
        {
            return _context.vEmployees.ToList();
        }
        public List<msMailConfig> GetByMail(int MailID)
        {
            var MailConfig = _context.msMailConfigs.Where(W=> W.Mail_ID == MailID).ToList();
            return MailConfig.ToList();
        }
        public List<msMailConfig> GetByMailType(int MailType)
        {
            var MailConfig = _context.msMailConfigs.Where(W => W.Mail_Type == MailType).ToList();
            return MailConfig.ToList();
        }
        public void InsertData(msMailConfig model)
        {
            try
            {
                _context.msMailConfigs.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msMailConfig> SearchData(msMailConfig model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msMailConfig model)
        {
            try
            {
                _context.msMailConfigs.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
