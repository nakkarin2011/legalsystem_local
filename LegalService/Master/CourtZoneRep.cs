﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CourtZoneRep : BaseRepository<CourtZone, LegalDataContext>
    {
        public List<CourtZone> GetAllCourtZone()
        {
            return this.SelectAll().OrderBy(D => D.CourtZoneName).ToList();
        }

        public List<CourtZone> GetCourtZoneWithCondition(string scondition)
        {
            return this.SelectDataWithCodition(D => D.CourtZoneName.Contains(scondition)
                || D.Days.ToString().Contains(scondition)).OrderBy(D => D.CourtZoneName).ToList();
        }

        public CourtZone GetCourtZoneByID(int CourtZoneID)
        {
            if (CourtZoneID > 0) return this.SelectDataWithCodition(D => D.CourtZoneID == CourtZoneID).First();
            else 
            { 
                CourtZone crt = new CourtZone(); 
                return crt; 
            }
        }

        public void insertData(CourtZone data)
        {
            int newid = 1;
            List<CourtZone> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CourtZoneID).Max() + 1;
            }
            data.CourtZoneID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CourtZone data)
        {
            this.DeleteDataWithCodition(D => D.CourtZoneID == data.CourtZoneID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CourtZoneID)
        {
            this.DeleteDataWithCodition(D => D.CourtZoneID == CourtZoneID);
            this.SummitChages();
        }
    }
}
