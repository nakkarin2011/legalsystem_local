﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trCostRep 
    {
        internal DbSet<trCost> dbSet;
        LegalDbContext _legalDbContext;
        public trCostRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trCost>();
        }
        public trCostRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trCost>();
        }

        public List<trCost> GetAllCost(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.CostID).ToList();
            return _legalDbContext.trCost.Where(x => x.LegalNo==LegalNo).OrderBy(x => x.CostID).ToList();
        }

        //public void insertData(trCost data)
        //{
        //    int newid = 1;
        //    List<trCost> list = this.SelectAll().ToList();
        //    if (list.Count > 0)
        //    {
        //        newid = this.SelectAll().Select(D => D.CostID).Max() + 1;
        //    }
        //    data.CostID = newid;

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trCost data)
        //{
        //    this.DeleteDataWithCodition(D => D.CostID == data.CostID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Equals(LegalNo));
        //    this.SummitChages();
        //}
    }
}
