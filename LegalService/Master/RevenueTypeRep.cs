﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class RevenueTypeRep : BaseRepository<RevenueType, LegalDataContext>
    {
        public List<RevenueType> GetAllRevenueType()
        {
            return this.SelectAll().OrderBy(D => D.RevenueTypeName).ToList();
        }

        public List<RevenueType> GetRevenueTypeIDWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.RevenueTypeName.Contains(condition)).OrderBy(D => D.RevenueTypeName).ToList();
        }

        public void insertData(RevenueType data)
        {
            int newid = 1;
            List<RevenueType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.RevenueTypeID).Max() + 1;
            }
            data.RevenueTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(RevenueType data)
        {
            this.DeleteDataWithCodition(D => D.RevenueTypeID == data.RevenueTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int RevenueTypeID)
        {
            this.DeleteDataWithCodition(D => D.RevenueTypeID == RevenueTypeID);
            this.SummitChages();
        }
    }
}
