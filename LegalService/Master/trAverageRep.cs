﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAverageRep 
    {
        internal DbSet<trAverage> dbSet;
        LegalDbContext _legalDbContext;
        public trAverageRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trAverage>();
        }
        public trAverageRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trAverage>();
        }

        public List<trAverage> GetAllAverage(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.AvrDate).ToList();
            return _legalDbContext.trAverage.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.AvrDate).ToList();
        }

        //public void insertData(trAverage data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Contains(LegalNo));
        //    this.SummitChages();
        //}
    }
}
