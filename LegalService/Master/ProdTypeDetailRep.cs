﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;
using System.Data.Entity;

namespace LegalService
{
    public class ProdTypeDetailRep : IRepository<LegalData.msProductTypeDetail>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public ProdTypeDetailRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msProductTypeDetail model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public msProductTypeDetail GetByTypeCode(string TypeCode)
        {
            try
            {
                var _result = _context.msProductTypeDetails.Where(W=>W.SubTypeCode == TypeCode).ToList();
                return _result.FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }
        public List<msProductTypeDetail> GetAll()
        {
            try
            {
                var _result = from a in _context.msProductTypeDetails.ToList()
                              select new msProductTypeDetail()
                              {
                                  PrdTypeDetailID = a.PrdTypeDetailID,
                                  PrdTypeID = a.PrdTypeID,
                                  SubTypeCode = a.SubTypeCode,
                                  SubTypeName = a.SubTypeName,
                                  SubTypeDesc = a.SubTypeDesc
                              };
                return _result.ToList();
            }
            catch
            {
                throw;
            }
        }

        public List<vProductTypeDetail> GetView()
        {
            try
            {
                var _result = _context.vProductTypeDetails;
                return _result.ToList();
            }
            catch
            {
                throw;
            }
        }

        public void InsertData(msProductTypeDetail model)
        {
            try
            {
                _context.msProductTypeDetails.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msProductTypeDetail> SearchData(msProductTypeDetail model)
        {
            return _context.msProductTypeDetails.Where(a => a.PrdTypeID == model.PrdTypeID
                                                      || a.SubTypeName.Contains(model.SubTypeName.Trim())
                                                      || a.SubTypeDesc.Contains(model.SubTypeDesc.Trim())).ToList();
        }

        public void UpdateData(msProductTypeDetail model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
