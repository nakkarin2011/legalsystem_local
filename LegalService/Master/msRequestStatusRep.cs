﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msRequestStatusRep :GenericRepository<msRequestStatus>
    {
        private LegalDbContext context;
        public msRequestStatusRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            context = dbContext;
        }
    }
}
