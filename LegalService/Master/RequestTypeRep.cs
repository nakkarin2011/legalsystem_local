﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class RequestTypeRep : BaseRepository<RequestType, LegalDataContext>
    {
        public void insertData(RequestType data)
        {
            int newid = 1;
            List<RequestType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.ReqTypeID).Max() + 1;
            }
            data.ReqTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int RequestTypeID)
        {
            this.DeleteDataWithCodition(D => D.ReqTypeID == RequestTypeID);
            this.SummitChages();
        }

        public List<RequestType> GetAllRequestTyp()
        {

            return this.SelectAll().OrderBy(D => D.ReqTypeID).ToList();
        }
        public List<RequestType> GetAssetTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.ReqTypeName.Contains(condition)).OrderBy(D => D.ReqTypeID).ToList();
        }
    }
}
