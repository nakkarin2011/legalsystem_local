﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CourtTypeRep : BaseRepository<CourtType, LegalDataContext>
    {
        public List<CourtType> GetAllCourtType()
        {
            return this.SelectAll().OrderBy(D => D.CourtTypeID).ToList();
        }

        public List<CourtType> GetCourtTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.CourtTypeName.Contains(condition)).OrderBy(D => D.CourtTypeName).ToList();
        }

        public void insertData(CourtType data)
        {
            int newid = 1;
            List<CourtType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CourtTypeID).Max() + 1;
            }
            data.CourtTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CourtType data)
        {
            this.DeleteDataWithCodition(D => D.CourtTypeID == data.CourtTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CourtTypeID)
        {
            this.DeleteDataWithCodition(D => D.CourtTypeID == CourtTypeID);
            this.SummitChages();
        }
    }
}
