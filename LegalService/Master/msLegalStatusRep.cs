﻿using Bell.Linq.Repository;
using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalService
{
    public class msLegalStatusRep :GenericRepository<msLegalStatus>
    {
        private LegalDbContext context;
        public msLegalStatusRep(LegalDbContext dbcontext)
            : base(dbcontext)
        {
            this.context = dbcontext;
        }
        //public List<LegalStatus> GetAllLegalStatus()
        //{
        //    return this.SelectAll().OrderBy(D => Convert.ToDecimal(D.ActionCode)).ToList();
        //}

        public List<msLegalStatus> GetLegalStatusByAssign()
        {
            List<msLegalStatus> R = new List<msLegalStatus>();
            R.Add(new msLegalStatus() { ActionCode = string.Empty, LegalStatusName = ServiceDDLLawyerHelper.PleaseSelect });
            var ListAsg = this.Get().OrderBy(o=>o.ActionCode).Where(D => ServiceLegalStatusHelper.FixedLegalStatusReAssign.Contains(D.ActionCode)).ToList();
            R.AddRange(ListAsg.ToList());
            return R;
        }

        public List<msLegalStatus> GetLegalStatusList()
        {
            List<msLegalStatus> R = new List<msLegalStatus>();
            R.Add(new msLegalStatus() { ActionCode = string.Empty, LegalStatusName = ServiceDDLLawyerHelper.PleaseSelect });
            R.AddRange(this.Get().OrderBy(D => D.LegalStatusName).ToList());
            return R;
        }
    }
}
