﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class RelationRep : BaseRepository<Relation, LegalDataContext>
    {
        public List<Relation> GetAllRelation()
        {
            return this.SelectAll().OrderBy(D => D.RelationOrder).ToList();
        }

        public List<Relation> GetRelationWithCondition(string scondition)
        {
            return this.SelectDataWithCodition(D => D.RelationName.Contains(scondition)
                || D.RelationCode.Contains(scondition)
                || D.RelationOrder.ToString().Contains(scondition)).OrderBy(D => D.RelationOrder).ToList();
        }

        public void insertData(Relation data)
        {
            int newid = 1;
            List<Relation> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.RelationID).Max() + 1;
            }
            data.RelationID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(Relation data)
        {
            this.DeleteDataWithCodition(D => D.RelationID == data.RelationID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int RelationID)
        {
            this.DeleteDataWithCodition(D => D.RelationID == RelationID);
            this.SummitChages();
        }
    }
}
