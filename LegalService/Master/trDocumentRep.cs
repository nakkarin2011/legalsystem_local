﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trDocumentRep 
    {
        internal DbSet<trDocument> dbSet;
        LegalDbContext _legalDbContext;
        public trDocumentRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trDocument>();
        }
        public trDocumentRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trDocument>();
        }

        public trDocument GetDocumentByID(int DocID)
        {
            //return this.SelectDataWithCodition(D => D.DocID == DocID).First();
            return _legalDbContext.trDocument.Where(x => x.DocID == DocID).First();
        }

        public List<trDocument> GetDocumentByLegalNo(string LegalNo)
        {
            try
            {
                List<trDocument> DBSet = new List<trDocument>();
                //List<vAuctionAssign> viewList = _legalDbContext.vAuctionAssign.Where(x => x.LegalNo == LegalNo).ToList();
                trDocumentMappingRep rep = new trDocumentMappingRep(_legalDbContext);
                List<trDocumentMapping> mapList = rep.GetAllDocMapping(LegalNo);
                foreach (var item in mapList)
                {
                    trDocument docList = _legalDbContext.trDocument.Where(x => x.DocID == item.DocID).FirstOrDefault();
                    DBSet.Add(docList);
                }
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public void insertData(trDocument data)
        //{
        //    int newid = 1;
        //    List<trDocument> list = this.SelectAll().ToList();
        //    if (list.Count > 0)
        //    {
        //        newid = this.SelectAll().Select(D => D.DocID).Max() + 1;
        //    }
        //    data.DocID = newid;

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trDocument data)
        //{
        //    this.DeleteDataWithCodition(D => D.DocID == data.DocID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(int DocID)
        //{
        //    this.DeleteDataWithCodition(D => D.DocID == DocID);
        //    this.SummitChages();
        //}
    }
}