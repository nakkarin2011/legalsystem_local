﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trMortgageRep
    {
        internal DbSet<trMortgage> dbSet;
        LegalDbContext _legalDbContext;
        public trMortgageRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trMortgage>();
        }
        public trMortgageRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trMortgage>();
        }

        public List<trMortgage> GetAllMortgage(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.MorDate).ToList();

            try
            {
                List<trMortgage> DBSet = _legalDbContext.trMortgage.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.MorDate).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }  
        }

        //public void insertData(trMortgage data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Contains(LegalNo));
        //    this.SummitChages();
        //}
    }
}
