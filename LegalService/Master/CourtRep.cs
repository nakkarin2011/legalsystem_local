﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CourtRep : BaseRepository<Court, LegalDataContext>
    {
        public List<Court> GetAllCourt()
        {
            return this.SelectAll().OrderBy(D => D.CourtID).ToList();
        }

        public List<Court> GetCourtWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.CourtName.Contains(condition)).OrderBy(D => D.CourtID).ToList();
        }

        public void insertData(Court data)
        {
            int newid = 1;
            List<Court> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CourtID).Max() + 1;
            }
            data.CourtID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(Court data)
        {
            this.DeleteDataWithCodition(D => D.CourtID == data.CourtID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CourtID)
        {
            this.DeleteDataWithCodition(D => D.CourtID == CourtID);
            this.SummitChages();
        }
    }
}
