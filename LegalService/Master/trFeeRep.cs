﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trFeeRep 
    {
        internal DbSet<trFee> dbSet;
        LegalDbContext _legalDbContext;
        public trFeeRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trFee>();
        }
        public trFeeRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trFee>();
        }

        public List<trFee> GetAllFee(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.FeeID).ToList();
            return _legalDbContext.trFee.Where(x => x.LegalNo==LegalNo).OrderBy(x => x.FeeID).ToList();
        }

        //public void insertData(trFee data)
        //{
        //    int newid = 1;
        //    List<trFee> list = this.SelectAll().ToList();
        //    if (list.Count > 0)
        //    {
        //        newid = this.SelectAll().Select(D => D.FeeID).Max() + 1;
        //    }
        //    data.FeeID = newid;

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trFee data)
        //{
        //    this.DeleteDataWithCodition(D => D.FeeID == data.FeeID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Equals(LegalNo));
        //    this.SummitChages();
        //}
    }
}
