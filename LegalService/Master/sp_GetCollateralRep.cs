﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;

namespace LegalService
{
    class sp_GetCollateralRep : sp_GetCollateral_Result, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public sp_GetCollateralRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public LegalData.sp_GetCollateral_Result GetCollateralbyAccount(string Acc)
        {
            sp_GetCollateral_Result rep = new sp_GetCollateral_Result();
            var LIST = _context.sp_GetCollateral(Acc).ToList();
            if (LIST.Count == 1)
            {
                rep = LIST.FirstOrDefault();
            }
            else if (LIST.Count > 1)
            {
                LIST = LIST.Where(W => W.CCMAST == "N").ToList();
                string COLLATERAL = string.Empty;
                foreach (var COL in LIST)
                {
                    COLLATERAL = COLLATERAL + COL.CollateralDesc + "#";
                }
                COLLATERAL = COLLATERAL + "END";
                rep = new sp_GetCollateral_Result()
                {
                    ACCTNO = LIST[0].ACCTNO,
                    CCDTTL = LIST[0].CCDTTL,
                    CCMAST = LIST[0].CCMAST,
                    CollateralDesc = COLLATERAL.ToString().Replace("END", ""),
                };
            }
            else
            {
                rep = new sp_GetCollateral_Result()
                {
                    CollateralDesc = ""
                };
            }
            return rep;
        }

    }
}
