﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msFileScanRep : IRepository<msFileScan>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public msFileScanRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msFileScan model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScan> GetAll()
        { 
            return _context.msFileScans.ToList();
        }

       
        public List<vFileScan> GetView()
        {
            return _context.vFileScans.ToList();
        }

        public void InsertData(msFileScan model)
        {
            try
            {
                _context.msFileScans.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msFileScan> SearchData(msFileScan model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msFileScan model)
        {
            try
            {
                _context.msFileScans.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
