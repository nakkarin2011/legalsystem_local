﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trRequestSequesterRep 
    {
        internal DbSet<trRequestSequester> dbSet;
        LegalDbContext _legalDbContext;
        public trRequestSequesterRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trRequestSequester>();
        }
        public trRequestSequesterRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trRequestSequester>();
        }

        public List<trRequestSequester> GetAllReqSeq(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.ReqSeqNo).ToList();

            List<trRequestSequester> DBSet;
            try
            {
                DBSet = _legalDbContext.trRequestSequester.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.ReqSeqNo).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        //public void insertData(trRequestSequester data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Contains(LegalNo));
        //    this.SummitChages();
        //}
    }
}
