﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class DocTypeMapRep : IRepository<DocumentTypeMapping>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public DocTypeMapRep()
        {
            _context = new LegalSystemEntities();
        }

        public void DeleteData(DocumentTypeMapping model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<DocumentTypeMapping> GetAll()
        {
            return _context.DocumentTypeMappings.OrderBy(D => D.ReqTypeID).ToList();
        }

        public List<vDocTypeMapping> GetView()
        {
            return _context.vDocTypeMappings.ToList();
        }

        public void InsertData(DocumentTypeMapping model)
        {
            try
            {
                _context.DocumentTypeMappings.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<DocumentTypeMapping> SearchData(DocumentTypeMapping model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(DocumentTypeMapping model)
        {
            try
            {
                _context.DocumentTypeMappings.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
