﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trPursueRep 
    {
        LegalDbContext _legalDbContext;
        public trPursueRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public trPursueRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<trPursue> GetAllPursue(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.PursueTime).ToList();

            try
            {
                List<trPursue> DBSet = _legalDbContext.trPursue.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.PursueTime).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        public trPursue GetPursueByID(int PursueID)
        {
            //return this.SelectDataWithCodition(D => D.PursueID == PursueID).FirstOrDefault();
            try
            {
                trPursue list = _legalDbContext.trPursue.Where(x => x.PursueID == PursueID).First();
                return list;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        //public void insertData(trPursue data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trPursue data)
        //{
        //    this.DeleteDataWithCodition(D => D.PursueID == data.PursueID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Contains(LegalNo));
        //    this.SummitChages();
        //}
    }
}