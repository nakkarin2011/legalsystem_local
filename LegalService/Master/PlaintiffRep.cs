﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class PlaintiffRep : BaseRepository<Plaintiff, LegalDataContext>
    {
        public List<Plaintiff> GetAllPlaintiff()
        {
            return this.SelectAll().OrderBy(D => D.PID).ThenBy(D => D.PName).ToList();
        }

        public List<Plaintiff> GetPlaintiffWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.PID.Contains(condition)
                || D.PName.Contains(condition)
                || D.PAddress.Contains(condition)
                || D.PCusType.Contains(condition)).OrderBy(D => D.PID).ThenBy(D => D.PName).ToList();
        }

        public void insertData(Plaintiff data)
        {
            int newid = 1;
            List<Plaintiff> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.PlaintiffID).Max() + 1;
            }
            data.PlaintiffID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(Plaintiff data)
        {
            this.DeleteDataWithCodition(D => D.PlaintiffID == data.PlaintiffID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int PlaintiffID)
        {
            this.DeleteDataWithCodition(D => D.PlaintiffID == PlaintiffID);
            this.SummitChages();
        }
    }
}
