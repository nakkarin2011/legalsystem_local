﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class SoldTypeRep : BaseRepository<SoldType, LegalDataContext>
    {
        public List<SoldType> GetAllSoldType()
        {
            return this.SelectAll().OrderBy(D => D.SoldID).ToList();
        }
    }
}
