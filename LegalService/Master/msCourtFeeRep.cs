﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;

namespace LegalService
{
    public class msCourtFeeRep : IRepository<LegalData.msCourtFee>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public msCourtFeeRep()
        {
            _context = new LegalSystemEntities();
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msCourtFee model)
        {
            throw new NotImplementedException();
        }

        public List<msCourtFee> GetAll()
        {
            var _result = from a in _context.msCourtFees.ToList()
                          select a;
            return _result.ToList();
        }

        public void InsertData(msCourtFee model)
        {
            throw new NotImplementedException();
        }

        public List<msCourtFee> SearchData(msCourtFee model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msCourtFee model)
        {
            throw new NotImplementedException();
        }
    }
}
