﻿using Bell.Linq.Repository;
using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msRequestTypeRep :GenericRepository<msRequestType>
    {
        private LegalDbContext dbContext;
        public msRequestTypeRep(LegalDbContext context)
            : base(context)
        {
            dbContext = context;
        }
        public List<msRequestType> GetAllRequestType()
        {
            List<msRequestType> R = new List<msRequestType>();
            R.Add(new msRequestType() { ReqTypeID = 0, ReqTypeName = "- Please Select -" });
            List<msRequestType> tmp = this.Get().OrderBy(D => D.ReqTypeID).ToList();
            R.AddRange(tmp);
            return R;
        }

        public msRequestType GetFixCostBySubType(int ReqTypeID)
        {
            return this.Get(D => D.ReqTypeID == ReqTypeID).FirstOrDefault();
        }

        public void insertData(msRequestType data)
        {
            int newid = 1;
            List<msRequestType> list = this.Get().ToList();
            if (list.Count > 0)
            {
                newid = this.Get().Select(D => D.ReqTypeID).Max() + 1;
            }
            data.ReqTypeID = newid;

            this.Insert(data);
            
        }

        public List<msRequestType> GetFixedRequestTypeOperPage()
        {
            List<msRequestType> RequestType = new List<msRequestType>();
            RequestType.Add(new msRequestType() { ReqTypeID = 0, ReqTypeName = ServiceDDLLawyerHelper.PleaseSelectEN });
            List<msRequestType> tmp = this.Get().OrderBy(D => D.ReqTypeID).Where(w => ServiceRequestTypeHelper.FixedRequestTypeOperPage.Contains(w.ReqTypeID)).ToList();
            RequestType.AddRange(tmp);
            return RequestType;
        }
        //public void updateData(msRequestType data)
        //{
        //    msRequestType t= this.Get(D => D.ReqTypeID == data.ReqTypeID).FirstOrDefault();
        //    this.Delete(t);
        //    this.Insert(data);
        //}

        //public void DeleteData(int ReqTypeID)
        //{
        //    this.DeleteDataWithCodition(D => D.ReqTypeID == ReqTypeID);
        //    this.SummitChages();
        //}
    }
}
