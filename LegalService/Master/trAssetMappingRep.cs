﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAssetMappingRep :GenericRepository<trAssetMapping>
    {

        LegalDbContext _legalDbContext;

        public trAssetMappingRep(LegalDbContext legalDbContext)
            : base(legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<trAssetMapping> GetAllAssetMapping(string LegalNo)
        {
            //List<trAssetMapping> maplist = new List<trAssetMapping>();
            //using (vAssetMappingRep rep = new vAssetMappingRep())
            //{
            //    List<vAssetMapping> list = rep.GetAllAsset(LegalNo);
            //    foreach (vAssetMapping itm in list)
            //    {
            //        trAssetMapping mapitm = this.SelectDataWithCodition(D => D.AssetID == itm.AssetID).First();
            //        maplist.Add(mapitm);
            //    }
            //}
            //return maplist;

            try
            {
                List<trAssetMapping> DBSet = new List<trAssetMapping>();
                List<vAssetMapping> vList = _legalDbContext.vAssetMapping.Where(x => x.LegalNo_pur == LegalNo).ToList();
                foreach (var item in vList)
                {
                    trAssetMapping assetmapList =this.Get(D => D.AssetID == item.AssetID).First();
                    DBSet.Add(assetmapList);
                }
                return DBSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }   
        }

        //public void insertData(trAssetMapping data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeletePursueData(int PursueID)
        //{
        //    this.DeleteDataWithCodition(D => D.PursueID == PursueID);
        //    this.SummitChages();
        //}

    }
}
