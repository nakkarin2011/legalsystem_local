﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class DocumentTypeRep : BaseRepository<DocumentType, LegalDataContext>
    {
        public List<DocumentType> GetAllDocumentType()
        {
            return this.SelectAll().OrderBy(D => D.DocTypeName).ToList();
        }

        //public List<DocumentType> GetLegalDocumentType()
        //{
        //    return this.SelectDataWithCodition(D => D.Flag.Contains("L")).OrderBy(D => D.DocTypeName).ToList();
        //}

        public List<DocumentType> GetDocumentTypeByFlag(string flag)
        {
            return this.SelectDataWithCodition(D => D.Flag.Contains(flag)).OrderBy(D => D.DocTypeName).ToList();
        }

        public void insertData(DocumentType data)
        {
            int newid = 1;
            List<DocumentType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.DocTypeID).Max() + 1;
            }
            data.DocTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(DocumentType data)
        {
            this.DeleteDataWithCodition(D => D.DocTypeID == data.DocTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int DocTypeID)
        {
            this.DeleteDataWithCodition(D => D.DocTypeID == DocTypeID);
            this.SummitChages();
        }
    }
}
