﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAuctionAssetRep 
    {
        internal DbSet<trAuctionAsset> dbSet;
        LegalDbContext _legalDbContext;
        public trAuctionAssetRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trAuctionAsset>();
        }
        public trAuctionAssetRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trAuctionAsset>();
        }

        public List<trAuctionAsset> GetAllAuctionAsset(string LegalNo)
        {
            //// Comment
            //List<trAuctionAsset> aucasslist = new List<trAuctionAsset>();
            //using (vAuctionAssetRep rep = new vAuctionAssetRep())
            //{
            //    List<vAuctionAsset> list = rep.GetAllAuctionAsset(LegalNo);//3
            //    foreach (vAuctionAsset itm in list)//3
            //    {
            //        trAuctionAsset newitm = this.SelectDataWithCodition(D => D.AuctionID == itm.AuctionID && D.AssetID == itm.AssetID).First();
            //        aucasslist.Add(newitm);
            //    }
            //}
            //return aucasslist;
            //// End Comment

            try
            {
                List<trAuctionAsset> DBSet = new List<trAuctionAsset>();
                //List<vAuctionAsset> viewList = _legalDbContext.vAuctionAsset.Where(x => x.LegalNo == LegalNo).ToList();
                vAuctionAssetRep rep = new vAuctionAssetRep(_legalDbContext);
                List<vAuctionAsset> viewList = rep.GetAllAuctionAsset(LegalNo);
                foreach (var item in viewList)
                {
                    trAuctionAsset aucassetList = _legalDbContext.trAuctionAsset.Where(D => D.AuctionID == item.AuctionID && D.AssetID == item.AssetID).First();
                    DBSet.Add(aucassetList);
                }
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        //public void insertData(trAuctionAsset data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trAuctionAsset data)
        //{
        //    this.DeleteDataWithCodition(D => D.AssetID == data.AssetID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(int AuctionID)
        //{
        //    this.DeleteDataWithCodition(D => D.AuctionID == AuctionID);
        //    this.SummitChages();
        //}
    }
}