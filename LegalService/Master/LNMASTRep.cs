﻿using LegalEntities;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Web;

namespace LegalService
{
    public class LNMASTRep : GenericRepository<LNMAST>
    {
        private LegalDbContext Dbcontext;
        public LNMASTRep(LegalDbContext context) : base(context)
        {
            this.Dbcontext = context;
        }
        public List<LNMAST> GetInfomationByCIF(string LinkServer, string CIF, string rid, int status)
        {
            //List<LNMAST> Ls = this.SelectDataWithCodition(D => D.CIFNO == Convert.ToDecimal(CIF) && D.STATUS != 2).OrderBy(D => D.ORGDT).ToList();
            LegalUnitOfWork unitofWork = new LegalUnitOfWork();
            List<LNMAST> Ls = unitofWork.LegalStoreProcedure.GetDataLnmast(LinkServer, int.Parse(CIF));
            if (Ls.Count > 0)
            {
                foreach (LNMAST L in Ls)
                {
                    L.isSelect = true;
                    L.isEnable = true;
                    using (sp_GetCollateralRep rep = new sp_GetCollateralRep())
                    {
                        var Collateral = rep.GetCollateralbyAccount(L.ACCTNO.ToString());
                        L.CollateralDesc = Collateral.CollateralDesc.ToString();
                    }
                }

                List<trRequest> Rs = new List<trRequest>();
                List<trRequestRelation> RRs = new List<trRequestRelation>();

                Rs = unitofWork.TrRequestRep.GetRequestByCIF(CIF);
                //using (RequestRep rep = new RequestRep())
                //    Rs = rep.GetRequestByCIF(CIF);
                if (Rs.Count > 0)
                {
                    if (!string.IsNullOrEmpty(rid))//Edit Mode
                    {
                        List<LNMAST> LNRelate = new List<LNMAST>();
                        Rs = Rs.Where(w => w.RequestID.ToString() == rid).Distinct().ToList();
                        foreach (var R in Rs)
                        {
                            RRs = unitofWork.TrRequestRelateRep.GetRequestRelationByRequestID(R.RequestID);
                            var GetRRS = RRs.Select(s => s.Account).Distinct().ToList();
                            foreach (var RR in GetRRS)
                            {
                                Ls.Where(D => D.ACCTNO == RR).FirstOrDefault().isSelect = Ls.Where(D => D.ACCTNO == RR).FirstOrDefault() != null;
                                Ls.Where(D => D.ACCTNO == RR).FirstOrDefault().isEnable = true;
                                LNRelate.AddRange(Ls.Where(w => w.ACCTNO == RR));
                            }
                        }
                        Ls = new List<LNMAST>();
                        Ls = LNRelate;
                    }
                    else /*NEW*/
                    {
                        List<LNMAST> LNRelate = new List<LNMAST>();
                        foreach (var R in Rs)
                        {
                            RRs = unitofWork.TrRequestRelateRep.GetRequestRelationByRequestID(R.RequestID);
                            var GetRRS = RRs.Select(s => s.Account).Distinct().ToList();
                            foreach (var RR in GetRRS)
                            {
                                Ls.Where(D => D.ACCTNO == RR).FirstOrDefault().isSelect = Ls.Where(D => D.ACCTNO == RR).FirstOrDefault() != null;
                                Ls.Where(D => D.ACCTNO == RR).FirstOrDefault().isEnable = true;
                                LNRelate.AddRange(Ls.Where(w => w.ACCTNO == RR));
                            }
                        }
                        Ls = new List<LNMAST>();
                        Ls = LNRelate;
                    }
                }
            }
            return Ls;
        }
    }
}
