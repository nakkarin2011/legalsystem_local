﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAuctionAssignRep
    {
        internal DbSet<trAuctionAssign> dbSet;
        LegalDbContext _legalDbContext;
        public trAuctionAssignRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trAuctionAssign>();
        }
        public trAuctionAssignRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trAuctionAssign>();
        }

        public List<trAuctionAssign> GetAllAuctionAssign(string LegalNo)
        {
            // Comment : NOT USE 
            //List<trAuctionAssign> aucasslist = new List<trAuctionAssign>();
            //using (vAuctionAssignRep rep = new vAuctionAssignRep())
            //{
            //    List<vAuctionAssign> list = rep.GetAllAuctionAssign(LegalNo);//3
            //    foreach (vAuctionAssign itm in list)//3
            //    {
            //        trAuctionAssign newitm = this.SelectDataWithCodition(D => D.AuctionID == itm.AuctionID && D.ActionTime == itm.ActionTime).First();
            //        aucasslist.Add(newitm);
            //    }
            //}
            //return aucasslist;
            // End Comment 

            try
            {
                List<trAuctionAssign> DBSet = new List<trAuctionAssign>();
                //List<vAuctionAssign> viewList = _legalDbContext.vAuctionAssign.Where(x => x.LegalNo == LegalNo).ToList();
                vAuctionAssignRep rep = new vAuctionAssignRep(_legalDbContext);
                List<vAuctionAssign> viewList = rep.GetAllAuctionAssign(LegalNo);
                foreach (var item in viewList)
                {
                    trAuctionAssign aucassignList = _legalDbContext.trAuctionAssign.Where(x => x.AuctionID == item.AuctionID && x.ActionTime == item.ActionTime).FirstOrDefault();
                    DBSet.Add(aucassignList);
                }
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public void insertData(trAuctionAssign data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trAuctionAssign data)
        //{
        //    this.DeleteDataWithCodition(D => D.AuctionID == data.AuctionID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(int AuctionID)
        //{
        //    this.DeleteDataWithCodition(D => D.AuctionID == AuctionID);
        //    this.SummitChages();
        //}
    }
}