﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class RequestReasonRep : BaseRepository<RequestReason, LegalDataContext>
    {
        public List<RequestReason> GetAllRequestReason()
        {
            return this.SelectAll().OrderBy(D => D.ReasonName).ToList();
        }

        public List<RequestReason> GetRequestReasonWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.ReasonName.Contains(condition)).OrderBy(D => D.ReasonName).ToList();
        }

        public void insertData(RequestReason data)
        {
            int newid = 1;
            List<RequestReason> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.ReasonID).Max() + 1;
            }
            data.ReasonID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(RequestReason data)
        {
            this.DeleteDataWithCodition(D => D.ReasonID == data.ReasonID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int ReasonID)
        {
            this.DeleteDataWithCodition(D => D.ReasonID == ReasonID);
            this.SummitChages();
        }
    }
}
