﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trLegal_CourtAssignRep 
    {
        internal DbSet<trLegal_CourtAssign> dbSet;
        LegalDbContext _legalDbContext;
        public trLegal_CourtAssignRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trLegal_CourtAssign>();
        }
        public trLegal_CourtAssignRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trLegal_CourtAssign>();
        }

        public List<trLegal_CourtAssign> GetAllCourtAssign(string LegalNo)
        {
            //return this.SelectDataWithCodition(D =>D.LegalNo.Equals(LegalNo) && D.CourtID == 1).OrderBy(D => D.AssignDate).ToList();

            try
            {
                return _legalDbContext.trLegal_CourtAssign.Where(x => x.LegalNo==LegalNo && x.CourtID == 1).OrderBy(x => x.AssignDate).ToList();
            }
            catch (Exception)
            {
                throw;
            }   
        }

        public List<trLegal_CourtAssign> GetAllAppealAssign(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Equals(LegalNo) && D.CourtID == 2).OrderBy(D => D.AssignDate).ToList();
            try
            {
                return _legalDbContext.trLegal_CourtAssign.Where(x => x.LegalNo == LegalNo && x.CourtID == 2).OrderBy(x => x.AssignDate).ToList();
            }
            catch (Exception)
            {
                throw;
            }   
        }

        public List<trLegal_CourtAssign> GetAllPettitionAssign(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Equals(LegalNo) && D.CourtID == 3).OrderBy(D => D.AssignDate).ToList();
            try
            {
                return _legalDbContext.trLegal_CourtAssign.Where(x => x.LegalNo == LegalNo && x.CourtID == 3).OrderBy(x => x.AssignDate).ToList();
            }
            catch (Exception)
            {
                throw;
            }   
        }

        public trLegal_CourtAssign GetCourtAssignByID(int AssignID)
        {
            //return this.SelectDataWithCodition(D => D.AssignID == AssignID).FirstOrDefault();
            try
            {
                return _legalDbContext.trLegal_CourtAssign.Where(x => x.AssignID == AssignID).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            } 
        }

        //public trLegal_CourtAssign GetAppealAssignByID(int AssignID)
        //{
        //    return this.SelectDataWithCodition(D => D.AssignID == AssignID).FirstOrDefault();
        //}

        //public trLegal_CourtAssign GetPetitionAssignByID(int AssignID)
        //{
        //    return this.SelectDataWithCodition(D => D.AssignID == AssignID).FirstOrDefault();
        //}

        //public void insertData(trLegal_CourtAssign data)
        //{
        //    int newid = 1;
        //    List<trLegal_CourtAssign> list = this.SelectAll().ToList();
        //    if (list.Count > 0)
        //    {
        //        newid = this.SelectAll().Select(D => D.AssignID).Max() + 1;
        //    }
        //    data.AssignID = newid;

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trLegal_CourtAssign data)
        //{
        //    this.DeleteDataWithCodition(D => D.AssignID == data.AssignID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Equals(LegalNo));
        //    this.SummitChages();
        //}
    }
}