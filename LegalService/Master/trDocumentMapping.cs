﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trDocumentMappingRep 
    {
        internal DbSet<trDocumentMapping> dbSet;
        LegalDbContext _legalDbContext;
        public trDocumentMappingRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trDocumentMapping>();
        }
        public trDocumentMappingRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trDocumentMapping>();
        }

        public List<trDocumentMapping> GetAllDocMapping(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Equals(LegalNo)).ToList();
            return _legalDbContext.trDocumentMapping.Where(x => x.LegalNo==LegalNo).ToList();
        }

        //public void insertData(trDocumentMapping data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trDocumentMapping data)
        //{
        //    this.DeleteDataWithCodition(D => D.DocID == data.DocID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Equals(LegalNo));
        //    this.SummitChages();
        //}
    }
}