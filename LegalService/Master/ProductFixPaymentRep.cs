﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;
using System.Data.Entity;

namespace LegalService
{
    public class ProductFixPaymentRep : IRepository<LegalData.msProductFixPayment>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public ProductFixPaymentRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msProductFixPayment model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msProductFixPayment> GetAll()
        {
            var _result = from a in _context.msProductFixPayments.ToList()
                          select new msProductFixPayment()
                          {
                              ProductFixID = a.ProductFixID,
                              PrdTypeID = a.PrdTypeID,
                              PrdTypeDetailID = a.PrdTypeDetailID,
                              FixCost = a.FixCost
                          };

            return _result.ToList();
        }

        public void InsertData(msProductFixPayment model)
        {
            try
            {
                _context.msProductFixPayments.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<msProductFixPayment> SearchData(msProductFixPayment model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msProductFixPayment model)
        {
            try
            {
                _context.msProductFixPayments.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }

        public List<vProductFixPayment> GetView()
        {
            try
            {
                var _result = _context.vProductFixPayments;
                return _result.ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}
