﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class AttorneyTypeRep : BaseRepository<AttorneyType, LegalDataContext>
    {
        public List<AttorneyType> GetAllAttorneyType()
        {
            return this.SelectAll().OrderBy(D => D.AttorneyTypeID).ToList();
        }
        public List<AttorneyType> GetAttorneyTypeList()
        {
            List<AttorneyType> R = new List<AttorneyType>();
            R.Add(new AttorneyType() { AttorneyTypeID = -1, AttorneyTypeName = "- กรุณาเลือก -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.AttorneyTypeID).ToList());
            return R;
        }

        public List<AttorneyType> GetAttorneyTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.AttorneyTypeName.Contains(condition)).OrderBy(D => D.AttorneyTypeName).ToList();
        }

        public List<AttorneyType> GetProductTypeList()
        {
            List<AttorneyType> R = new List<AttorneyType>();
            R.Add(new AttorneyType() { AttorneyTypeID = -1, AttorneyTypeName = "- กรุณาเลือก -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.AttorneyTypeID).ToList());
            return R;
        }

        public void insertData(AttorneyType data)
        {
            int newid = 1;
            List<AttorneyType> list = this.SelectAll().ToList();
            if(list.Count > 0){
                newid = this.SelectAll().Select(D => D.AttorneyTypeID).Max() + 1;
            }
            data.AttorneyTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(AttorneyType data)
        {
            this.DeleteDataWithCodition(D => D.AttorneyTypeID == data.AttorneyTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int AttorneyTypeID)
        {
            this.DeleteDataWithCodition(D => D.AttorneyTypeID == AttorneyTypeID);
            this.SummitChages();
        }
    }
}
