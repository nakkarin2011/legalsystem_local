﻿using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msAssignLawyerRep : GenericRepository<msAssignLawyer>
    {
            private readonly LegalDbContext context;
            public msAssignLawyerRep(LegalDbContext dbContext)
                : base(dbContext)
            {
                this.context = dbContext;
            }
            public List<msAssignLawyer> GetUsers(string EmpCode)
            {
                List<msAssignLawyer> S = new List<msAssignLawyer>();
                S.Add(new msAssignLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
                List<msAssignLawyer> tmp = Get().ToList();
                tmp = tmp.Where(w => w.GroupID != ServicePermissionHelper.SeniorLawyer).ToList();
                if (tmp.Count > 0)
                    S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
                return S;
            }
            public List<msAssignLawyer> GetAllUsers()
            {
                List<msAssignLawyer> S = new List<msAssignLawyer>();
                S.Add(new msAssignLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
                List<msAssignLawyer> tmp = Get().ToList();
                if (tmp.Count > 0)
                    S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
                return S;
            }
        
    }
}
