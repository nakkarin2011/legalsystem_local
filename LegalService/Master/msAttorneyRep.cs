﻿using Bell.Linq.Repository;
using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class msAttorneyRep :GenericRepository<msAttorney>
    {
        private LegalDbContext dbContext;
        public msAttorneyRep(LegalDbContext context):base(context)
        {
            this.dbContext = context;
        }
        public List<msAttorney> GetAllLawyer()
        {
            List<msAttorney> R = new List<msAttorney>();
            R.Add(new msAttorney() { AttorneyTypeID = -1, LawyerName = ServiceDDLLawyerHelper.PleaseSelect });
             List<msAttorney> tmp = this.Get().OrderBy(D => D.AttorneyTypeID).ToList();
             R.AddRange(tmp);
             return R;
        }

        public msAttorney GetLawyerByID(int LawyerID)
        {
            return this.Get(D => D.AttorneyID == LawyerID).FirstOrDefault();
        }

        public List<msAttorney> GetLawyerByTypeID(int AttorneyTypeID)
        {
            List<msAttorney> R = new List<msAttorney>();
            R.Add(new msAttorney() { AttorneyTypeID = -1, LawyerName = ServiceDDLLawyerHelper.PleaseSelect });
            List<msAttorney> tmp = this.Get(D => D.AttorneyTypeID == AttorneyTypeID).OrderBy(D => D.LawyerName).ToList();
            R.AddRange(tmp);
            return R;
        }

        //public void insertData(msLawyer data)
        //{

        //    //int newid = 1;
        //    //List<msLawyer> list = this.SelectAll().ToList();
        //    //if (list.Count > 0)
        //    //{
        //    //    newid = this.SelectAll().Select(D => D.LawyerID).Max() + 1;
        //    //}
        //    //data.LawyerID = newid;

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(Lawyer data)
        //{
        //    this.DeleteDataWithCodition(D => D.LawyerID == data.LawyerID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(int LawyerID)
        //{
        //    this.DeleteDataWithCodition(D => D.LawyerID == LawyerID);
        //    this.SummitChages();
        //}
    }

    //public class vLawyerRep : BaseRepository<vLawyer, LegalDataContext>
    //{
    //    public List<vLawyer> GetAllLawyer()
    //    {
    //        return this.SelectAll().OrderBy(D => D.LawyerID).ToList();
    //    }

    //    public vLawyer GetLawyerByID(int LawyerID)
    //    {
    //        return this.SelectDataWithCodition(D => D.LawyerID == LawyerID).First();
    //    }

    //    public List<vLawyer> GetLawyerWithCondition(string condition)
    //    {
    //        return this.SelectDataWithCodition(D => D.AttorneyTypeName.Contains(condition) || D.LawyerName.Contains(condition)).OrderBy(D => D.AttorneyTypeName).ThenBy(D => D.LawyerName).ToList();
    //    }
    //}
}
