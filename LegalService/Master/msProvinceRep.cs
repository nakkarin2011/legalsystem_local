﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;
using LegalData.Helper;

namespace LegalService
{
    public class msProvinceRep : IRepository<LegalData.msProvince>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public msProvinceRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(msProvince model)
        {
            throw new NotImplementedException();
        }

        public List<msProvince> GetAll()
        {
            var _result = from a in _context.msProvinces.ToList()
                          select new msProvince()
                          {
                              State = a.State,
                              Province = a.Province
                          };
            return _result.DistinctBy(p => new { p.State, p.Province }).ToList();
        }

        public void InsertData(msProvince model)
        {
            throw new NotImplementedException();
        }

        public List<msProvince> SearchData(msProvince model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(msProvince model)
        {
            throw new NotImplementedException();
        }
    }
}
