﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAuctionRep 
    {
        internal DbSet<trAuction> dbSet;
        LegalDbContext _legalDbContext;
        public trAuctionRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trAuction>();
        }
        public trAuctionRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trAuction>();
        }

        public List<trAuction> GetAllAuction(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.AuctionOrder).ToList();
            return _legalDbContext.trAuction.Where(x => x.LegalNo == LegalNo).OrderBy(x => x.AuctionOrder).ToList();
        }

        //public void insertData(trAuction data)
        //{
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trAuction data)
        //{
        //    this.DeleteDataWithCodition(D => D.AuctionID == data.AuctionID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(string LegalNo)
        //{
        //    this.DeleteDataWithCodition(D => D.LegalNo.Equals(LegalNo));
        //    this.SummitChages();
        //}
    }
}