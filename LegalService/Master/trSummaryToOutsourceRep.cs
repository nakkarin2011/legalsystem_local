﻿using LegalData;
using LegalData.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trSummaryToOutsourceRep : IRepository<trSummaryToOutsource>, IDisposable
    {
        private readonly LegalSystemEntities _context;
        public void Dispose()
        {
            _context.Dispose();
        }

        public trSummaryToOutsourceRep()
        {
            _context = new LegalSystemEntities();
        }

        public void UpdateData(trSummaryToOutsource model)
        {
            try
            {
                _context.trSummaryToOutsources.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeleteData(trSummaryToOutsource model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void InsertData(trSummaryToOutsource model)
        {
            try
            {
                _context.trSummaryToOutsources.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public trSummaryToOutsource GetbyAccNo(string AccountNo)
        {
            return _context.trSummaryToOutsources.OrderBy(D => D.RequestNo).Where(w => w.Account == AccountNo).FirstOrDefault();
        }
        public List<trSummaryToOutsource> GetAll()
        {
            return _context.trSummaryToOutsources.OrderBy(D => D.RequestNo).ToList();
        }

        public List<trSummaryToOutsource> SearchByCondition(trSummaryToOutsource model, string AttorneyID)
        {
            return _context.trSummaryToOutsources.OrderBy(O => O.RequestNo).Where(W => W.LawyerAttorneyID == AttorneyID).ToList();
        }

        public List<trSummaryToOutsource> SearchData(trSummaryToOutsource model)
        {
            throw new NotImplementedException();
        }
        public List<sp_GetRequestOperPage_Outsource_Result> GetDataDoctoOutsource(int Round,string AttorneyID , string SendStatus)
        {
            var list = _context.sp_GetRequestOperPage_Outsource(Round, AttorneyID, SendStatus).OrderByDescending(O=>O.SendRound).ToList();
            var NewList = list.DistinctBy(p => new { p.RequestNo }).ToList();

            return NewList.ToList();
        }
        public void InsertSummaryData(sp_GetRequestOperPage_Outsource_Result Smmry, int Round, int No,string EmailSendStatus, string Emp_Code)
        {
            try
            {
                string[] sNotic = (string.IsNullOrEmpty(Smmry.NoticeDate) ? null : Smmry.NoticeDate.Split('/'));
                string[] sActionIndictDate = (string.IsNullOrEmpty(Smmry.ActionIndictDate) ? null : Smmry.ActionIndictDate.Split('/'));
                string[] sIndictDate = (string.IsNullOrEmpty(Smmry.IndictDate) ? null : Smmry.IndictDate.Split('/'));
                var RequestID = Smmry.RequestID;
                _context.trSummaryToOutsources.Add(new trSummaryToOutsource
                {
                    SummaryID = Guid.NewGuid(),
                    SendRound = Round,
                    RequestID = RequestID,
                    RequestNo = Smmry.RequestNo,
                    RequestType = Smmry.RequestType,
                    LegalNo = Smmry.LegalNo,
                    LegalStatus = Smmry.LegalStatus,                    
                    CifNo = Smmry.CifNo,
                    Account = Smmry.ACCTNO.ToString(),
                    CFNAME = Smmry.CFNAME,
                    LawyerAttorneyID = Smmry.AttorneyID,
                    LawyerAttorneyName = Smmry.AttorneyName,
                    LawyerID = Smmry.LawyerID,
                    LawyerName = Smmry.LawyerName,
                    Topic = Smmry.Topic,
                    Seq = No++,
                    SpecialCase = Smmry.SpecialCase,
                    NoticeDate = (string.IsNullOrEmpty(Smmry.NoticeDate) ? (DateTime?)null : new DateTime(Convert.ToInt32(sNotic[2]), Convert.ToInt32(sNotic[1]), Convert.ToInt32(sNotic[0]), 0, 0, 0, 0)),
                    ActionIndictDate = (string.IsNullOrEmpty(Smmry.ActionIndictDate) ? (DateTime?)null : new DateTime(Convert.ToInt32(sActionIndictDate[2]), Convert.ToInt32(sActionIndictDate[1]), Convert.ToInt32(sActionIndictDate[0]), 0, 0, 0, 0)),
                    IndictDate = (string.IsNullOrEmpty(Smmry.IndictDate) ? (DateTime?)null : new DateTime(Convert.ToInt32(sIndictDate[2]), Convert.ToInt32(sIndictDate[1]), Convert.ToInt32(sIndictDate[0]), 0, 0, 0, 0)),

                    SendBy = Emp_Code,
                    SendDate = DateTime.Now,
                    EmailSendDate = DateTime.Now,
                    EmailSendStatus = EmailSendStatus,
                });
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
