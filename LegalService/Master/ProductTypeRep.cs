﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalService
{
    public class ProducTypeRep : BaseRepository<ProductType, LegalDataContext>
    {
        public List<ProductType> GetAllProductType()
        {
            return this.SelectAll().OrderBy(D => D.PrdTypeName).ToList();
        }

        public List<ProductType> GetProductTypeList()
        {
            List<ProductType> R = new List<ProductType>();
            R.Add(new ProductType() { PrdTypeID = -1, PrdTypeName = "- Please Select -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.PrdTypeName).ToList());
            return R;
        }

        public List<ProductType> GetProductTypeWithCondition(string scondition)
        {
            return this.SelectDataWithCodition(D => D.PrdTypeName.ToString().Contains(scondition)).OrderBy(D => D.PrdTypeName).ToList();
        }

        public void insertData(ProductType data)
        {
            int newid = 1;
            List<ProductType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.PrdTypeID).Max() + 1;
            }
            data.PrdTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(ProductType data)
        {
            this.DeleteDataWithCodition(D => D.PrdTypeID == data.PrdTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int PrdTypeID)
        {
            this.DeleteDataWithCodition(D => D.PrdTypeID == PrdTypeID);
            this.SummitChages();
        }
    }
}