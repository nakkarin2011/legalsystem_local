﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CoverPageGroupRep : BaseRepository<CoverPageGroup, LegalDataContext>
    {
        public List<CoverPageGroup> GetAllGroup()
        {
            return this.SelectAll().OrderBy(D => D.CovTypeID).ToList();
        }
    }
}
