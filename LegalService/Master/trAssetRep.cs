﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAssetRep :GenericRepository<trAsset>
    {
        LegalDbContext _legalDbContext;
        
        public trAssetRep(LegalDbContext legalDbContext):base(legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<trAsset> GetAllAsset(string LegalNo)
        {
            //List<trAsset> asslist = new List<trAsset>();
            //using (vAssetMappingRep rep = new vAssetMappingRep())
            //{
            //    List<vAssetMapping> list = rep.GetAllAsset(LegalNo);//3
            //    foreach (vAssetMapping itm in list)//3
            //    {
            //        trAsset mapitm = this.SelectDataWithCodition(D => D.AssetID == itm.AssetID).First();
            //        asslist.Add(mapitm);
            //    }
            //}
            //return asslist;

            try
            {
                List<trAsset> DBSet = new List<trAsset>();
                List<vAssetMapping> vList = _legalDbContext.vAssetMapping.Where(x => x.LegalNo_pur == LegalNo).ToList();
                foreach (var item in vList)
                {
                    trAsset assetList =this.Get(D => D.AssetID == item.AssetID).First();
                    DBSet.Add(assetList);
                }
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        //public void insertData(trAsset data)
        //{

        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void updateData(trAsset data)
        //{
        //    this.DeleteDataWithCodition(D => D.AssetID == data.AssetID);
        //    this.Insert(data);
        //    this.SummitChages();
        //}

        //public void DeleteData(int AssetID)
        //{
        //    this.DeleteDataWithCodition(D => D.AssetID == AssetID);
        //    this.SummitChages();
        //}
    }
}