﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class wfGroupRep : GenericRepository<wfGroup>
    {
        private readonly LegalDbContext context;
        public wfGroupRep(LegalDbContext dbContext): base(dbContext)
        {
            this.context = dbContext;
        }
        public List<wfGroup> GetGroupByRole(int StateID)
        {
            List<wfGroup> S = new List<wfGroup>();
            S.Add(new wfGroup() { EmpCode = "0", EmpName = "- Please Select -" });
            S.AddRange(this.Get(x=>x.StateID==StateID).OrderBy(D => D.EmpCode).ToList());
            return S;
        }
    }
}
