﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trRetainRep 
    {
        internal DbSet<trRetain> dbSet;
        LegalDbContext _legalDbContext;
        public trRetainRep()
        {
            _legalDbContext = new LegalDbContext();
            dbSet = _legalDbContext.Set<trRetain>();
        }
        public trRetainRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
            dbSet = _legalDbContext.Set<trRetain>();
        }

        public List<trRetain> GetAllRetain(string LegalNo)
        {
            List<trRetain> DBSet;
            try
            {
                DBSet = _legalDbContext.trRetain.Where(x => x.LegalNo == LegalNo).OrderBy(D => D.RetDate).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }
    }
}
