﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trRequestRelationRep :GenericRepository<trRequestRelation>
    {
        private readonly  LegalDbContext context;
        public trRequestRelationRep(LegalDbContext dbcontext)
            : base(dbcontext)
        {
            this.context = dbcontext;
        }
        public List<trRequestRelation> GetRequestRelationByRequestID(Guid RequestID)
        {
            List<trRequestRelation> datas = this.Get(x => x.RequestID == RequestID).OrderBy(x => x.RelationSeq).ToList();
            return datas;
        }

        public void updateData(List<trRequestRelation> datas)
        {
            trRequestRelation tr=this.Get(x=>x.RequestID== datas[0].RequestID).FirstOrDefault();
            this.Delete(tr);
            foreach (trRequestRelation data in datas)
            {
                data.RRID = Guid.NewGuid();
                this.Insert(data);
            }
        
        }

        public void DeleteData(Guid RRID)
        {
          List<trRequestRelation> datas = this.Get(x => x.RequestID == RRID).ToList();
            for(int i=0;i<datas.Count;i++)
            {
                this.Delete(datas[i]);
            }
           
        }
    }
}
