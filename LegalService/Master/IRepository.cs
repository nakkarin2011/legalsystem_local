﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public interface IRepository<T>
    {
        void InsertData(T model);
        void UpdateData(T model);
        void DeleteData(T model);
        List<T> SearchData(T model);
        List<T> GetAll();
    }
}
