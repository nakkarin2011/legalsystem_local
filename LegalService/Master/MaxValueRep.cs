﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class MaxValueRep : BaseRepository<MaxValue, LegalDataContext>
    {
        public List<MaxValue> GetAllMaxID()
        {
            return this.SelectAll().OrderBy(D => D.MaxID).ToList();
        }

        public List<MaxValue> GetMaxValueWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.MaxName.Contains(condition)
                || D.MaxAmt.ToString().Contains(condition)).OrderBy(D => D.MaxID).ToList();
        }

        public MaxValue GetMaxValueByMaxName(string MaxName)
        {
            return this.SelectDataWithCodition(D => D.MaxName.Contains(MaxName)).FirstOrDefault();
        }

        public void insertData(MaxValue data)
        {
            int newid = 1;
            List<MaxValue> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.MaxID).Max() + 1;
            }
            data.MaxID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(MaxValue data)
        {
            this.DeleteDataWithCodition(D => D.MaxID == data.MaxID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int MaxID)
        {
            this.DeleteDataWithCodition(D => D.MaxID == MaxID);
            this.SummitChages();
        }
    }
}
