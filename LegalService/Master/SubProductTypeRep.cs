﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalService
{
    public class SubProductTypeRep : BaseRepository<SubProductType, LegalDataContext> 
    {
        //public List<SubProductType> GetAllSubProductType()
        //{
        //    return this.SelectAll().OrderBy(D => D.).ToList();
        //}


        public void insertData(SubProductType data)
        {
            int newid = 1;
            List<SubProductType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.SubTypeID).Max() + 1;
            }
            data.SubTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void UpdateData(SubProductType data)
        {
            //this.u(data);
            //this.SummitChages();
        }

        public void DeleteData(int SubTypeID)
        {
            this.DeleteDataWithCodition(D => D.SubTypeID == SubTypeID);
            this.SummitChages();
        }
    }
}