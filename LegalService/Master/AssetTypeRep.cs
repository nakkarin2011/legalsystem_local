﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class AssetTypeRep : BaseRepository<AssetType, LegalDataContext> 
    {
        public List<AssetType> GetAllAssetType()
        {

            return this.SelectAll().OrderBy(D => D.AssetTypeName).ToList();
        }

        public List<AssetType> GetAssetTypeList()
        {
            List<AssetType> R = new List<AssetType>();
            R.Add(new AssetType() { AssetTypeID = -1, AssetTypeName = "- กรุณาเลือก -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.AssetTypeName).ToList());
            return R;
        }

        public List<AssetType> GetAssetTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.AssetTypeName.Contains(condition)).OrderBy(D => D.AssetTypeName).ToList();
        }
        
        public void insertData(AssetType data)
        {
            int newid = 1;
            List<AssetType> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.AssetTypeID).Max() + 1;
            }
            data.AssetTypeID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(AssetType data)
        {
            this.DeleteDataWithCodition(D => D.AssetTypeID == data.AssetTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int AssetTypeID)
        {
            this.DeleteDataWithCodition(D => D.AssetTypeID == AssetTypeID);
            this.SummitChages();
        }
    }
}