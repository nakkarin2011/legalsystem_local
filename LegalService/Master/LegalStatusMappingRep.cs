﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class LegalStatusMappingRep : BaseRepository<LegalStatusMapping, LegalDataContext>
    {
        public void insertData(LegalStatusMapping data)
        {
            int newid = 1;
            List<LegalStatusMapping> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.LegalMapId).Max() + 1;
            }
            data.LegalMapId = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(LegalStatusMapping data)
        {
            this.DeleteDataWithCodition(D => D.LegalStatus == data.LegalStatus && D.ReqTypeID == data.ReqTypeID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(string LegalStatus, int ReqTypeID)
        {
            this.DeleteDataWithCodition(D => D.LegalStatus == LegalStatus && D.ReqTypeID == ReqTypeID);
            this.SummitChages();
        }

        public void DeleteData(int ReqMapID)
        {
            this.DeleteDataWithCodition(D => D.LegalMapId == ReqMapID);
            this.SummitChages();
        }
    }
}
