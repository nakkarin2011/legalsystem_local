﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class CapitalRateRep : BaseRepository<CapitalRate, LegalDataContext> 
    {
        public List<CapitalRate> GetAllCapitalRate()
        {
            return this.SelectAll().OrderBy(D => D.MinWage).ToList();
        }

        public List<CapitalRate> GetCapitalRateWithCondition(string scondition)
        {
            return this.SelectDataWithCodition(D => D.MinWage.ToString().Contains(scondition)
                                                    || D.MaxWage.ToString().Contains(scondition)
                                                    || D.Percentage.ToString().Contains(scondition)
                                                    || D.Condition.Contains(scondition)
                                                    || D.PayAmount.ToString().Contains(scondition)).OrderBy(D => D.MinWage).ToList();
        }

        public void insertData(CapitalRate data)
        {
            int newid = 1;
            List<CapitalRate> list = this.SelectAll().ToList();
            if (list.Count > 0)
            {
                newid = this.SelectAll().Select(D => D.CapitalRateID).Max() + 1;
            }
            data.CapitalRateID = newid;

            this.Insert(data);
            this.SummitChages();
        }

        public void updateData(CapitalRate data)
        {
            this.DeleteDataWithCodition(D => D.CapitalRateID == data.CapitalRateID);
            this.Insert(data);
            this.SummitChages();
        }

        public void DeleteData(int CapitalRateID)
        {
            this.DeleteDataWithCodition(D => D.CapitalRateID == CapitalRateID);
            this.SummitChages();
        }
    }
}
