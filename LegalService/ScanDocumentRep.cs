﻿using System;
using LegalData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LegalService
{
    public class ScanDocumentRep : IRepository<ScanDocument>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public ScanDocumentRep()
        {
            _context = new LegalSystemEntities();
        }

        public void DeleteData(ScanDocument model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<ScanDocument> GetAll()
        {
            return _context.ScanDocuments.ToList();
        }

        public void InsertData(ScanDocument model)
        {
            try
            {
                _context.ScanDocuments.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<ScanDocument> SearchData(ScanDocument model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(ScanDocument model)
        {
            try
            {
                _context.ScanDocuments.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
