﻿using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class LegalDataContext : DataContext
    {
        public LegalDataContext() : base(System.Configuration.ConfigurationManager.ConnectionStrings["LegalConnectionString"].ConnectionString) { }

        [Function(Name = "dbo.sp_GetRelationship")]
        public ISingleResult<sp_GetRelationship> GetRelationship([Parameter(Name = "@ACCTNO", DbType = "VARCHAR")]string AcctNo, [Parameter(Name = "@RNO", DbType = "VARCHAR")]string RNo)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), AcctNo, RNo);
            return (ISingleResult<sp_GetRelationship>)result.ReturnValue;
        }

        [Function(Name = "dbo.sp_GetRequest")]
        public ISingleResult<sp_GetRequest> GetRequest([Parameter(Name = "@RequestType", DbType = "VARCHAR")]string RequestType, [Parameter(Name = "@RequestNo", DbType = "VARCHAR")]string RequestNo, [Parameter(Name = "@RequestDate", DbType = "VARCHAR")]string RequestDate, [Parameter(Name = "@CIF", DbType = "VARCHAR")]string CIF, [Parameter(Name = "@RequestStatus", DbType = "VARCHAR")]string RequestStatus, [Parameter(Name = "@LegalStatus", DbType = "VARCHAR")]string LegalStatus)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), RequestType, RequestNo, RequestDate, CIF, RequestStatus, LegalStatus);
            return (ISingleResult<sp_GetRequest>)result.ReturnValue;
        }

        [Function(Name = "dbo.sp_GetWorkList")]
        public ISingleResult<sp_GetWorkList> GetWorkList([Parameter(Name = "@AcctNo", DbType = "VARCHAR")]string AcctNo, [Parameter(Name = "@CIFNo", DbType = "VARCHAR")]string CIFNo, [Parameter(Name = "@DPD", DbType = "VARCHAR")]string DPD, [Parameter(Name = "@NAME", DbType = "VARCHAR")]string NAME)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), AcctNo, CIFNo, DPD, NAME);
            return (ISingleResult<sp_GetWorkList>)result.ReturnValue;
        }

    }
    
}
