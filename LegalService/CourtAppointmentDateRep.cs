﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LegalData;
using System.Data.Entity;

namespace LegalService
{
    public class CourtAppointmentDateRep : IRepository<LegalData.CourtAppointmentDate>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public CourtAppointmentDateRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(CourtAppointmentDate model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<CourtAppointmentDate> GetAll()
        {
            return _context.CourtAppointmentDates.ToList();
        }

        public List<vCourtAppointmentDate> GetView()
        {
            return _context.vCourtAppointmentDates.ToList();
        }

        public void InsertData(CourtAppointmentDate model)
        {
            try
            {
                _context.CourtAppointmentDates.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<CourtAppointmentDate> SearchData(CourtAppointmentDate model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(CourtAppointmentDate model)
        {
            try
            {
                _context.CourtAppointmentDates.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
