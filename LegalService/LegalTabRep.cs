﻿using LegalData;
using LegalData.Helper;
using LegalData.Model.Request;
using LegalData.Model.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class LegalTabRep : IDisposable
    {
        private readonly LegalSystemEntities _context;

        public LegalTabRep()
        {
            _context = new LegalSystemEntities();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<vListLegalStatu> getListLegalStatus()
        {
            try
            {
                return _context.vListLegalStatus.ToList();
            }
            catch
            {
                throw;
            }
        }

        public vLegalInfo getLegalInfoData(Guid rid)
        {
            try
            {

                var query = (from a in _context.vLegalInfoes.ToList()
                             where a.RequestID == rid
                             select a).FirstOrDefault();

                return query;

            }
            catch(Exception ex)
            {
                throw;
            }

        }


        public LegalTabResponse UpdateDataLegalTab1(legalTab1 model)
        {
            var result = new LegalTabResponse();

            try
            {
                var ReturnCode = new ObjectParameter("ReturnCode", typeof(int));
                var Msg = new ObjectParameter("Msg", typeof(string));

                _context.sp_SaveChangeLegalInfoTab1(model.RequestID
                    , model.LegalStatus
                    , model.ActiveTab
                    , model.RemarkTab1
                    , model.ReceivingDocdate
                    , model.SendingLawyerDate
                    , ReturnCode
                    , Msg);

                result.ReturnCode = ReturnCode.Value.ToString();
                result.Msg = Msg.Value.ToString();

                return result;
            }
            catch
            {
                throw;
            }
        }

        public LegalTabResponse UpdateDataLegalTab2(legalTab2 model)
        {
            var result = new LegalTabResponse();

            try
            {
                var ReturnCode = new ObjectParameter("ReturnCode", typeof(int));
                var Msg = new ObjectParameter("Msg", typeof(string));

                _context.sp_SaveChangeLegalInfoTab2(model.RequestID
                    , model.LegalStatus
                    , model.ActiveTab
                    , model.NoticeDate
                    , model.SetCaseDate
                    , model.CaseDate
                    , model.RemarkTab2
                    , ReturnCode
                    , Msg);

                result.ReturnCode = ReturnCode.Value.ToString();
                result.Msg = Msg.Value.ToString();

                return result;
            }
            catch
            {
                throw;
            }
        }

        public LegalTabResponse UpdateDatalegalTab3(legalTab3 model)
        {
            var result = new LegalTabResponse();

            try
            {
                var ReturnCode = new ObjectParameter("ReturnCode", typeof(int));
                var Msg = new ObjectParameter("Msg", typeof(string));

                _context.sp_SaveChangeLegalInfoTab3(model.RequestID
                    , model.LegalStatus
                    , model.ActiveTab
                    , model.IndictmentStatus
                    , model.IndictmentReason
                    , model.Blackcase
                    , model.CourtZoneID
                    , model.Capital
                    , model.Courtfees
                    , model.LastCourtDate1
                    , model.CourtDateReason1
                    , model.JudgmentDate
                    , model.JudgmentAllowDate
                    , model.RedCase
                    , model.CaseFee
                    , model.JudgmentStatus
                    , model.JudgmentReason
                    , model.AppealType
                    , model.AppealReason
                    , model.AppealDate
                    , model.AppealBlackcase
                    , model.AppealCapital
                    , model.AppealFee
                    , model.AppealEditdate
                    , model.LastCourtDate2
                    , model.CourtDateReason2
                    , model.AppealCourtjudgmentDate
                    , model.AppealRedCase
                    , model.PetitionType
                    , model.PetitionReason
                    , model.SupremeDate
                    , model.BlackcaseSupreme
                    , model.SupremeCapital
                    , model.SupremeFee
                    , model.SupremeEditDate
                    , model.LastCourtDate3
                    , model.CourtDateReason3
                    , model.SupremeCourtjudgmentDate
                    , model.SupremeRedCase
                    , ReturnCode
                    , Msg);

                result.ReturnCode = ReturnCode.Value.ToString();
                result.Msg = Msg.Value.ToString();
                return result;
            }
            catch
            {
                throw;
            }
        }

        public LegalTabResponse UpdateDatalegalTab4(legalTab4 model)
        {
            var result = new LegalTabResponse();

            try
            {
                var ReturnCode = new ObjectParameter("ReturnCode", typeof(int));
                var Msg = new ObjectParameter("Msg", typeof(string));

                _context.sp_SaveChangeLegalInfoTab4(model.RequestID
                    , model.LegalStatus
                    , model.ActiveTab
                    , model.ExecuteDate
                    , model.SetExecuteDate
                    , model.SendExecuteDocdate
                    , model.ReceivingExecuteDocdate
                    , model.ExecuteBookType
                    , ReturnCode
                    , Msg);

                result.ReturnCode = ReturnCode.Value.ToString();
                result.Msg = Msg.Value.ToString();
                return result;
            }
            catch
            {
                throw;
            }
        }

        public void UpdateDatalegalStatus(Guid rid,string status)
        {
            try
            {
                //Update trRequest

                var modelRequest = (from a in _context.trRequests
                             where a.RequestID == rid
                             select a).FirstOrDefault();

                modelRequest.LegalStatus = status;
                UpdateTRrequest(modelRequest);

                //Update trLegal

                var modelLegal = (from a in _context.trLegals
                                    where a.LegalID == modelRequest.LegalID
                                    select a).FirstOrDefault();

                modelLegal.LegalStatus = status;
                UpdateTRlegal(modelLegal);

            }
            catch
            {
                throw;
            }
        }


        public void UpdateTRrequest(LegalData.trRequest model)
        {
            try
            {
                _context.trRequests.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void UpdateTRlegal(LegalData.trLegal model)
        {
            try
            {
                _context.trLegals.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
}
