﻿using System;
using LegalData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LegalService
{
    public class AssetDescRep : IRepository<AssetDesc>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public AssetDescRep()
        {
            _context = new LegalSystemEntities();
        }

        public void DeleteData(AssetDesc model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<AssetDesc> GetAll()
        {
            return _context.AssetDescs.ToList();
        }

        public List<vAssetDesc>GetView()
        {
            return _context.vAssetDescs.ToList();
        }

        public void InsertData(AssetDesc model)
        {
            try
            {
                _context.AssetDescs.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<AssetDesc> SearchData(AssetDesc model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(AssetDesc model)
        {
            try
            {
                _context.AssetDescs.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
