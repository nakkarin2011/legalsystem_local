﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAssignLawyerRep:GenericRepository<vAssignLawyer>
    {
        LegalDbContext _legalDbContext;
        public vAssignLawyerRep(LegalDbContext context):base(context)
        {
            _legalDbContext = new LegalDbContext();
        }
        public List<vAssignLawyer> GetAssignByCondition(string CIF, string LegalStatus, string LegalNo, string RequestNo, string Lawyer, string Attorney)
        {
            try
            {
                string ArrSearch = CIF + LegalStatus + LegalNo + RequestNo + Lawyer + Attorney;
                List<vAssignLawyer> list = Get().ToList();
                if (ArrSearch.Length > 0)
                {
                    list = list.Where(x => x.CifNo.ToString().Contains(CIF) && x.LegalStatus.Contains(LegalStatus) && x.LegalNo.Contains(LegalNo)
                                        && x.RequestNo.Contains(RequestNo)).ToList();
                    if(!string.IsNullOrEmpty(Lawyer))
                        list = list.Where(x => x.LawyerID == Lawyer).ToList();
                    if(!string.IsNullOrEmpty(Attorney))
                        list = list.Where(x => x.AttorneyID == Attorney).ToList();
                }
                else
                    list = this.Get().OrderBy(o=>o.RequestNo).ToList();
                return list.OrderBy(o=>o.RequestNo).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public vAssignLawyer GetAssignByReqNo(string RequestNo)
        {
            try
            {
                vAssignLawyer list =this.Get(x => x.RequestNo == RequestNo).First();
                return list;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public vAssignLawyer GetAssignAll()
        {
            try
            {
                vAssignLawyer list = this.GetAssignAll();
                return list;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
    }
}
