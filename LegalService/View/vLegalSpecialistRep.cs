﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vLegalSpecialistRep : GenericRepository<vLegalSpecialist>
    {
        private readonly LegalDbContext context;
        public vLegalSpecialistRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            this.context = dbContext;
        }
        public List<vLegalSpecialist> GetUsers()
        {
            List<vLegalSpecialist> S = new List<vLegalSpecialist>();
            S.Add(new vLegalSpecialist() { Emp_Code = "0", TH_Name = "- Please Select -" });
            List<vLegalSpecialist> tmp = Get().ToList();
           if( tmp.Count > 0)
               S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            return S;
        }
    }
}
