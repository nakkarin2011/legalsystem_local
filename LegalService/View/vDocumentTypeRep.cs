﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vDocumentTypeRep : BaseRepository<vDocumentType, LegalDataContext>
    {
        public List<vDocumentType> GetAllDocumentType()
        {
            return this.SelectAll().OrderBy(D => D.ReqTypeName).ThenBy(D => D.DocTypeName).ToList();
        }

        public List<vDocumentType> GetDocumentTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.ReqTypeName.Contains(condition)
                || D.DocTypeName.Contains(condition)).OrderBy(D => D.ReqTypeName).ThenBy(D => D.DocTypeName).ToList();
        }
    }
}
