﻿using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace LegalService
{
    public class vCollectionHeadApproveRep : GenericRepository<vCollectionHeadApprove>
    {
        private readonly LegalDbContext context;
        public vCollectionHeadApproveRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            this.context = dbContext;
        }

        public List<vCollectionHeadApprove> GetApprove()
        {
            List<vCollectionHeadApprove> S = new List<vCollectionHeadApprove>();
            List<vCollectionHeadApprove> tmp = Get().ToList();
            S = tmp.Where(w => w.GroupID == 442).ToList();
            
            return S;
        }

        
    }
}
