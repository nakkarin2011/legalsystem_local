﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalService
{
    public class vSubProductTypeRep : BaseRepository<vSubProductType, LegalDataContext>
    {
        public List<vSubProductType> GetAllSubProductType()
        {
            return this.SelectAll().OrderBy(D => D.PrdTypeName).ThenBy(D => D.SubTypeName).ToList();
        }

        public List<vSubProductType> GetSubPrtTypWithConditions(string subcode, string subname, string prdname, string isfix, string fixcost)
        {
            List<vSubProductType> list;
            if (!string.IsNullOrEmpty(isfix) && string.IsNullOrEmpty(fixcost))
            {
                list = this.SelectDataWithCodition(
                            D => D.SubTypeCode.Contains(subcode)
                            || D.SubTypeName.Contains(subname)
                            || D.PrdTypeName.Contains(prdname)
                            || D.IsFixCost == Convert.ToBoolean(isfix)).OrderBy(D => D.PrdTypeName).ThenBy(D => D.SubTypeName).ToList();
            }
            else if (string.IsNullOrEmpty(isfix) && !string.IsNullOrEmpty(fixcost))
            {
                list = this.SelectDataWithCodition(
                            D => D.SubTypeCode.Contains(subcode)
                            || D.SubTypeName.Contains(subname)
                            || D.PrdTypeName.Contains(prdname)
                            || D.FixCostAmt == Convert.ToDecimal(fixcost)).ToList();
            }
            else if(!string.IsNullOrEmpty(isfix) && !string.IsNullOrEmpty(fixcost))
            {
                list = this.SelectDataWithCodition(
                            D => D.SubTypeCode.Contains(subcode)
                            || D.SubTypeName.Contains(subname)
                            || D.PrdTypeName.Contains(prdname)
                            || D.FixCostAmt == Convert.ToDecimal(fixcost)
                            || D.IsFixCost == Convert.ToBoolean(isfix)).ToList();
            }
            else
            {
                list = this.SelectDataWithCodition(
                            D => D.SubTypeCode.Contains(subcode.ToUpper())
                            || D.SubTypeName.Contains(subname.ToUpper())
                            || D.PrdTypeName.Contains(prdname.ToUpper())).ToList();
            }
            return list;
        }
    }
}