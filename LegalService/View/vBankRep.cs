﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vBankRep : BaseRepository<vBank, LegalDataContext>
    {
        public List<vBank> GetAllBank()
        {
            List<vBank> R = new List<vBank>();
            R.Add(new vBank() { Code = string.Empty, BankName = "- กรุณาเลือก -" });
            R.AddRange(this.SelectAll().OrderBy(D => D.BankName).ToList());
            return R;
        }
    }
}
