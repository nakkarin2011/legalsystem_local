﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAuctionAssignRep 
    {
        internal DbSet<vAuctionAssign> dbSet;
        LegalDbContext _legalDbContext;
        public vAuctionAssignRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vAuctionAssignRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vAuctionAssign> GetAllAuctionAssign(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.AuctionOrder).ThenBy(D => D.ActionTime).ToList();

            try
            {
                //List<vAuctionAssign> DBSet = _legalDbContext.vAuctionAssign.Where(x => x.LegalNo == LegalNo).OrderBy(D => D.AuctionOrder).ThenBy(D => D.ActionTime).ToList();
                List<vAuctionAssign> DBSet = _legalDbContext.vAuctionAssign.Where(x => x.LegalNo == LegalNo).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }

        public vAuctionAssign GetAuctionAssignByID(int AuctionID, int ActionTime)
        {
            //return this.SelectDataWithCodition(D => D.AuctionID == AuctionID && D.ActionTime == ActionTime).FirstOrDefault();

            try
            {
                vAuctionAssign list = _legalDbContext.vAuctionAssign.Where(D => D.AuctionID == AuctionID && D.ActionTime == ActionTime).FirstOrDefault();
                return list;
            }
            catch (Exception)
            {
                throw;
            }  
        }
    }
}
