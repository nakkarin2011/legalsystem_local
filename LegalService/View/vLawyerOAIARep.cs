﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
   public class vLawyerOAIARep : BaseRepository<vLawyerOAIA, LegalDataContext>
    {
        public List<vLawyerOAIA> GetAllOAIAMapping()
        {
            return this.SelectAll().OrderBy(D => D.LawyerName).OrderBy(D => D.LawyerName).ThenBy(D => D.AttorneyTypeID).ToList();
        }
        public List<vLawyerOAIA> GetOAIAWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.LawyerName.Contains(condition)
                || D.AttorneyTypeName.Contains(condition)).OrderBy(D => D.LawyerName).ThenBy(D => D.LawyerName).ToList();
        }
    }
}
