﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAssetMappingRep :GenericRepository<vAssetMapping>
    {
        LegalDbContext _legalDbContext;

        public vAssetMappingRep(LegalDbContext legalDbContext)
            : base(legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vAssetMapping> GetAllPursueAsset(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo_pur.Contains(LegalNo)).OrderBy(D => D.PursueTime).ThenBy(D => D.AssetID).ToList();

            try
            {
                List<vAssetMapping> DBSet =this.Get(D => D.LegalNo_pur == LegalNo).OrderBy(D => D.PursueTime).ThenBy(D => D.AssetID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<vAssetMapping> GetAllSequesterAsset(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo_seq.Contains(LegalNo)).OrderBy(D => D.SeqTime).ThenBy(D => D.AssetID).ToList();

            try
            {
                List<vAssetMapping> DBSet =this.Get(D => D.LegalNo_pur == LegalNo).OrderBy(D => D.SeqTime).ThenBy(D => D.AssetID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<vAssetMapping> GetAllAsset(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo_pur.Contains(LegalNo)).OrderBy(D => D.PursueTime).ThenBy(D => D.AssetID).ToList();

            try
            {
                List<vAssetMapping> DBSet =this.Get(D => D.LegalNo_pur==LegalNo).OrderBy(D => D.PursueTime).ThenBy(D => D.AssetID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            } 
        }

        public vAssetMapping GetAssetByID(int AssetID)
        {
            //return this.SelectDataWithCodition(D => D.AssetID == AssetID).FirstOrDefault();
            try
            {
                vAssetMapping list = this.Get(D => D.AssetID == AssetID).FirstOrDefault();
                return list;
            }
            catch (Exception)
            {
                throw;
            } 
        }
    }
}
