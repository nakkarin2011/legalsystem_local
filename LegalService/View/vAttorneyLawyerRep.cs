﻿using Bell.Linq.Repository;
using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAttorneyLawyerRep : GenericRepository<vAttorneyLawyer>
    {
        private readonly LegalDbContext context;
        public vAttorneyLawyerRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            this.context = dbContext;
        }
        public List<vAttorneyLawyer> GetUsers(string EmpCode)
        {
            List<vAttorneyLawyer> S = new List<vAttorneyLawyer>();
            S.Add(new vAttorneyLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vAttorneyLawyer> tmp = Get().ToList();
            tmp = tmp.Where(w => w.Emp_Code == EmpCode && w.GroupID != ServicePermissionHelper.SeniorLawyer).ToList();
            if (tmp.Count > 0)
                S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            return S;
        }
        public List<vAttorneyLawyer> GetAllUsers()
        {
            List<vAttorneyLawyer> S = new List<vAttorneyLawyer>();
            S.Add(new vAttorneyLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vAttorneyLawyer> tmp = Get().ToList();
            if (tmp.Count > 0)
                S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            return S;
        }
        public List<vAttorneyLawyer> GetUsersAssign(string EmpCode)
        {
            List<vAttorneyLawyer> S = new List<vAttorneyLawyer>();
            S.Add(new vAttorneyLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vAttorneyLawyer> tmp = Get().ToList();
            tmp = tmp.Where(w => w.GroupID != ServicePermissionHelper.SeniorLawyer).ToList();
            if (tmp.Count > 0)
                S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            return S;
        }
        public List<vAttorneyLawyer> GetAllUsersAssign()
        {
            List<vAttorneyLawyer> S = new List<vAttorneyLawyer>();
            S.Add(new vAttorneyLawyer() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vAttorneyLawyer> tmp = Get().ToList();
            if (tmp.Count > 0)
                S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            return S;
        }
    }
}
