﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vCostRep 
    {
        LegalDbContext _legalDbContext;
        public vCostRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vCostRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vCost> GetAllCost(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.Flag).ThenBy(D => D.CostID).ToList();
            return _legalDbContext.vCost.Where(D => D.LegalNo == LegalNo).OrderBy(D => D.CostID).ToList();
        }
        public List<vCost> GetCostByFlag(string TFlag, string SFlag)
        {
            // Flag: F = fix,  N = not fix
            return _legalDbContext.vCost.Where(D => D.TFlag == TFlag
                && D.SFlag == SFlag).OrderBy(D => D.CostID).ToList();
        }

        public vCost GetCostByID(int CostID)
        {
            return _legalDbContext.vCost.Where(D => D.CostID == CostID).FirstOrDefault();
        }
    }
}
