﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vCoverpageRep : BaseRepository<vCoverpage, LegalDataContext>
    {
        public List<vCoverpage> GetCoverpage(int CovTypeID)
        {
            List<vCoverpage> S = new List<vCoverpage>();
            S = this.SelectDataWithCodition(D => D.CovTypeID == CovTypeID).OrderBy(D => D.DocTypeID).ToList();
            return S;
        }
    }
}
