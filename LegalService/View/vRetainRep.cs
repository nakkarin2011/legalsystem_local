﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vRetainRep 
    {
         internal DbSet<trAssetMapping> dbSet;
        LegalDbContext _legalDbContext;
        public vRetainRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vRetainRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vRetain> GetAllRetain(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.RetainID).ToList();
            return _legalDbContext.vRetain.Where(D => D.LegalNo==LegalNo).OrderBy(D => D.RetainID).ToList();
        }
    }
}
