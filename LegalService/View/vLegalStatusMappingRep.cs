﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vLegalStatusMappingRep : BaseRepository<vLegalStatusMapping, LegalDataContext>
    {
        public List<vLegalStatusMapping> GetAllLegalStatusMapping()
        {
            return this.SelectAll().OrderBy(D => D.LegalStatusName).OrderBy(D => D.LegalStatusName).ThenBy(D => D.ReqTypeName).ToList();
        }

        public List<vLegalStatusMapping> GetAllLegalStatusMappingWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.LegalStatusName.Contains(condition)
                || D.ReqTypeName.Contains(condition)).OrderBy(D => D.LegalStatusName).ThenBy(D => D.ReqTypeName).ToList();
        }
    }
}
