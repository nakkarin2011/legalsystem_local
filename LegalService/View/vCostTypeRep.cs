﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vCostTypeRep : BaseRepository<vCostType, LegalDataContext>
    {
        public List<vCostType> GetAllCostType()
        {
            return this.SelectAll().OrderBy(D => D.TFlag).ThenBy(D => D.SFlag).ThenBy(D => D.CostTypeID).ToList();
        }

        public List<vCostType> GetCostTypeWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.TFlag.Contains(condition)
                || D.SFlag.Contains(condition)
                || D.AttorneyTypeName.Contains(condition)
                || D.CostTypeName.Contains(condition)
                || D.AttorneyTypeID.ToString().Contains(condition)
                || D.CostTypeID.ToString().Contains(condition)).OrderBy(D => D.TFlag).ThenBy(D => D.SFlag).ThenBy(D => D.CostTypeID).ToList();
        }

        public List<vCostType> GetCostTypeByFlag(string TFlag, string SFlag)
        {
            // TFlag: type flag, SFlag: status flag
            return this.SelectDataWithCodition(D => D.TFlag.Contains(TFlag)
                && D.SFlag.Contains(SFlag)).OrderBy(D => D.CostTypeID).ToList();
        }

        public vCostType GetCostTypeByID(int CostTypeID)
        {
            return this.SelectDataWithCodition(D => D.CostTypeID == CostTypeID).FirstOrDefault();
        }
    }
}
