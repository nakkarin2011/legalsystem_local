﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vRequestTypeMapLegalStstusRep : GenericRepository<vRequestTypeMapLegalStstus>
    {
        private readonly LegalDbContext context;
        public vRequestTypeMapLegalStstusRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            this.context = dbContext;
        }
        public List<vRequestTypeMapLegalStstus> GetRequestTypeByLegalStstus(string LegalStatus)
        {
            List<vRequestTypeMapLegalStstus> S = new List<vRequestTypeMapLegalStstus>();
            S.Add(new vRequestTypeMapLegalStstus() { ReqTypeID = 0, ReqTypeName = ServiceHelper.ServiceDDLLawyerHelper.PleaseSelect });
            int status = Convert.ToInt16(LegalStatus);
            List<vRequestTypeMapLegalStstus> tmp = Get(x => x.LegalStatus == status).ToList();
           if( tmp.Count > 0)
               S.AddRange(tmp.OrderBy(D => D.ReqTypeID).ToList());
            return S;
        }
        public List<vRequestTypeMapLegalStstus> GetRequestTypeRefer(string LegalStatus)
        {
            List<vRequestTypeMapLegalStstus> S = new List<vRequestTypeMapLegalStstus>();
            S.Add(new vRequestTypeMapLegalStstus() { ReqTypeID = 0, ReqTypeName = ServiceHelper.ServiceDDLLawyerHelper.PleaseSelect });
            int status = Convert.ToInt16(LegalStatus);
            List<vRequestTypeMapLegalStstus> tmp = Get(x => x.LegalStatus == status).ToList();
            List<vRequestTypeMapLegalStstus> tmpList = new List<vRequestTypeMapLegalStstus>();
            tmpList.AddRange(tmp.Where(s => s.ReqTypeID == 3));
            tmpList.AddRange(tmp.Where(s => s.ReqTypeID == 4));
            tmpList.AddRange(tmp.Where(s => s.ReqTypeID == 5));

            if (tmpList.Count > 0)
                S.AddRange(tmpList.OrderBy(D => D.ReqTypeID).ToList());
            return S;
        }

    }
}
