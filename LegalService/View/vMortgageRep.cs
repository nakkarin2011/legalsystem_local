﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vMortgageRep 
    {
        internal DbSet<vMortgage> dbSet;
        LegalDbContext _legalDbContext;
        public vMortgageRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vMortgageRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vMortgage> GetAllMortgage(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.MortgageID).ToList();

            try
            {
                List<vMortgage> DBSet = _legalDbContext.vMortgage.Where(x => x.LegalNo == LegalNo).OrderBy(D => D.MortgageID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }
    }
}
