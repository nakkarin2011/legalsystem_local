﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;

namespace LegalService
{
    public class vDocumentMappingRep
    {
        internal DbSet<trLegal> dbSet;
        LegalDbContext _legalDbContext;
        public vDocumentMappingRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vDocumentMappingRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        //public List<vDocumentMapping> GetAllPursueDocument(string LegalNo)
        //{
        //    return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo) && D.Flag.Equals("P")).OrderBy(D => D.PursueTime).ThenBy(D => D.DocID).ToList();
        //}

        //public List<vDocumentMapping> GetAllAuctionDocument(string LegalNo)
        //{
        //    return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo) && D.Flag.Equals("A")).OrderBy(D => D.AuctionOrder).ThenBy(D => D.DocID).ToList();
        //}

        //public List<vDocumentMapping> GetAllLegalDocument(string LegalNo)
        //{
        //    return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo) && D.Flag.Equals("L")).OrderBy(D => D.DocID).ToList();
        //}


        public List<vDocumentMapping> GetAllDocument(string LegalNo)
        {
            return _legalDbContext.vDocumentMapping.Where(x => x.LegalNo == LegalNo).ToList();
        }

        public vDocumentMapping GetDocumentByID(int DocID)
        {
            return _legalDbContext.vDocumentMapping.Where(x => x.DocID == DocID).FirstOrDefault();
        }
    }
}
