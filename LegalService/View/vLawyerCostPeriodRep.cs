﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vLawyerCostPeriodRep : BaseRepository<vLawyerCostPeriod, LegalDataContext>
    {
        public List<vLawyerCostPeriod> GetAllLawyerCostPeriod()
        {
            return this.SelectAll().OrderBy(D => D.PaymentTypeName).ThenBy(D => D.PaymentName).ToList();
        }

        public List<vLawyerCostPeriod> GetAllCostPeriodWithCondition(string condition)
        {
            return this.SelectDataWithCodition(D => D.PaymentTypeName.Contains(condition)
                || D.PaymentTypeName.Contains(condition)
                || D.Percentage.ToString().Contains(condition)).OrderBy(D => D.PaymentTypeName).ThenBy(D => D.PaymentName).ToList();
        }
    }
}
