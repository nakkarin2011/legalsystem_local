﻿using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LegalService
{
    public class vEmpLegalRep : GenericRepository<vEmpLegal>
    {
        private readonly LegalDbContext context;
        public vEmpLegalRep(LegalDbContext dbContext)
            : base(dbContext)
        {
            this.context = dbContext;
        }

        public List<vEmpLegal> GetAllUsers()
        {
            List<vEmpLegal> S = new List<vEmpLegal>();
            S.Add(new vEmpLegal() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vEmpLegal> tmp = Get().ToList();

            if (tmp.Count > 0)
            {
                var List = tmp.OrderBy(D => D.GroupID).ToList();
                string GroupID = string.Join(",", List);
                S.AddRange(tmp.OrderBy(D => D.Emp_Code).ToList());
            }
            return S;
        }

        public List<vEmpLegal> GetUsers(string User, string headApprover)
        {
            List<vEmpLegal> S = new List<vEmpLegal>();
            S.Add(new vEmpLegal() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vEmpLegal> tmp = Get().ToList();

            int GID;
            if (tmp.Count > 0)
            {
                var Li = tmp.OrderBy(D => D.GroupID).Where(w => w.Emp_Code == User).ToList();
                var List = Li.Select(s => s.GroupID).ToList();
                string GroupID = string.Join(",", List);
                GID = Convert.ToInt32(GroupID);
                switch (GID)
                {
                    case ServicePermissionHelper.CollectionMaker /*440*/:
                        GID = ServicePermissionHelper.CollectionChecker /*441*/;
                        break;
                    case ServicePermissionHelper.CollectionChecker /*441*/:
                        GID = ServicePermissionHelper.CollectionHead /*442*/;
                        break;
                    case ServicePermissionHelper.CollectionHead:
                        GID = ServicePermissionHelper.LegalSpecialist /*443*/;
                        if (User != headApprover)
                        {
                            S.AddRange(tmp.OrderBy(D => D.GroupID).Where(w => w.Emp_Code == headApprover).ToList());
                            S[1].TH_Name = ServiceDDLApproverHelper.ApprovedbyHead;
                        }
                        break;
                    case ServicePermissionHelper.LegalSpecialist /*443*/:
                        GID = ServicePermissionHelper.SeniorLawyer /*444*/;
                        S.Add(new vEmpLegal() { Emp_Code = null, TH_Name = ServiceDDLApproverHelper.SendtoSeniorLawyer });
                        break;
                    case ServicePermissionHelper.SeniorLawyer /*444*/:
                        GID = ServicePermissionHelper.Lawyer /*445*/;
                        S.AddRange(tmp.OrderByDescending(D => D.GroupID).Where(w => w.GroupID == GID).ToList());
                        break;
                }
                if (User == headApprover || GroupID == ServicePermissionHelper.CollectionHead.ToString())
                {                    
                    S.Add(new vEmpLegal() { Emp_Code = null, TH_Name = ServiceDDLApproverHelper.SendtoLegal });
                }
                else if (GroupID == ServicePermissionHelper.LegalSpecialist.ToString() /*"443"*/) { }
                else if (GroupID == ServicePermissionHelper.SeniorLawyer.ToString() /*"444"*/) { }
                else if (GroupID == ServicePermissionHelper.Lawyer.ToString() /*"445"*/) { }
                else
                {
                    S.AddRange(tmp.OrderByDescending(D => D.GroupID).Where(w => w.GroupID == GID).ToList());
                }
                if (GroupID == ServicePermissionHelper.CollectionChecker.ToString())
                {
                    S.RemoveAll(s => s.Emp_Code == headApprover);
                }
             }
            return S;
        }

        public List<vEmpLegal> GetApproverNotice(string headApprover)
        {
            List<vEmpLegal> S = new List<vEmpLegal>();
            S.Add(new vEmpLegal() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vEmpLegal> tmp = Get().ToList();

            if (tmp.Count > 0)
            {                
                var List = tmp.OrderByDescending(D => D.GroupID).Where(w => w.GroupID == ServicePermissionHelper.CollectionHead && w.Emp_Code != headApprover).ToList();
                S.AddRange(List.ToList());
            }
            return S;
            
        }
        public List<vEmpLegal> GetApproverNoticeSenior(string headApprover,string EmpCode)
        {
            List<vEmpLegal> S = new List<vEmpLegal>();
            S.Add(new vEmpLegal() { Emp_Code = "0", TH_Name = ServiceDDLLawyerHelper.PleaseSelect });
            List<vEmpLegal> tmp = Get().ToList();
            List<vEmpLegal> List = new List<vEmpLegal>();
            if (tmp.Count > 0)
            {
                if (headApprover != EmpCode)
                {
                    List = tmp.OrderByDescending(D => D.GroupID).Where(w => w.GroupID == ServicePermissionHelper.CollectionHead && w.Emp_Code == headApprover).ToList();
                    S.AddRange(List.ToList());
                }
                    S.Add(new vEmpLegal() { Emp_Code = "1", TH_Name = ServiceDDLApproverHelper.SendtoSeniorLawyer });
            }
            return S;            
        }
    }
}
