﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace LegalService
{
    public class vRequestReasonRep :GenericRepository<vRequestReason>
    {
        private LegalDbContext context;
        public vRequestReasonRep(LegalDbContext Dbcontext)
            : base(Dbcontext)
        {
            context = Dbcontext;
        }
        public List<vRequestReason> GetReasonByRequest(int ReqTypeID)
        {
            List<vRequestReason> S = new List<vRequestReason>();
            S.Add(new vRequestReason() { ReasonID = 0, ReasonName = "- Please Select -" });
            List<vRequestReason> tmp = this.Get(D => D.ReqTypeID == ReqTypeID).ToList();
            if (tmp.Count > 0)
                S.AddRange(tmp.OrderBy(D => D.ReasonID).ToList());
            return S;
        }

        public List<vRequestReason> GetReasonByRefercase(int ReqTypeID,DropDownList ddl)
        {
            try
            {
                List<vRequestReason> S = new List<vRequestReason>();
                S.Add(new vRequestReason() { ReasonID = 0, ReasonName = ServiceHelper.ServiceDDLLawyerHelper.PleaseSelect });
                List<vRequestReason> tmp = this.Get(D => D.ReqTypeID == ReqTypeID).ToList();

                if (tmp.Count > 0)
                    tmp.Distinct().ToList();
                S.AddRange(tmp.OrderBy(D => D.ReasonID).ToList());

                ddl.DataSource = S.ToList();
                ddl.DataBind();
                return S;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}
