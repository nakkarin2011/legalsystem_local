﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
   
    public class ProductCostRep : BaseRepository<ProductCost, LegalDataContext>
    {
        public List<ProductCost> GetAllProductCost()
        {
            return this.SelectAll().OrderBy(D => D.PrdTypeID).ThenBy(D => D.SubTypeCode).ToList();
        }

        public ProductCost GetProductCostBySubType(int PrdTypeID, string SubTypeCode)
        {
            return this.SelectDataWithCodition(D => D.PrdTypeID == PrdTypeID && D.SubTypeCode == SubTypeCode).FirstOrDefault();
        }
    }
}
