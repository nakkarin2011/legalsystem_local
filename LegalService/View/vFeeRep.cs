﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vFeeRep 
    {
        LegalDbContext _legalDbContext;
        public vFeeRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vFeeRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vFee> GetAllFee(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.Flag).ThenBy(D => D.CostID).ToList();
            return _legalDbContext.vFee.Where(D => D.LegalNo == LegalNo).OrderBy(D => D.FeeID).ToList();
        }

        public List<vFee> GetFeeByFlag(string TFlag, string SFlag)
        {
            // Flag: F = fix,  N = not fix
            return _legalDbContext.vFee.Where(D => D.TFlag == TFlag
                && D.SFlag == SFlag).OrderBy(D => D.FeeID).ToList();
        }

        public vFee GetFeeByID(int FeeID)
        {
            return _legalDbContext.vFee.Where(D => D.FeeID == FeeID).FirstOrDefault();
        }
    }
}
