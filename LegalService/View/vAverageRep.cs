﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAverageRep 
    {
        internal DbSet<vAverage> dbSet;
        LegalDbContext _legalDbContext;
        public vAverageRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vAverageRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vAverage> GetAllAverage(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.AverageID).ToList();

            try
            {
                List<vAverage> DBSet = _legalDbContext.vAverage.Where(x => x.LegalNo == LegalNo).OrderBy(D => D.AverageID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }   
        }
    }
}
