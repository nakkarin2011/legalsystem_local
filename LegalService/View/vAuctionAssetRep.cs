﻿using Bell.Linq.Repository;
using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class vAuctionAssetRep 
    {
        internal DbSet<vAuctionAsset> dbSet;
        LegalDbContext _legalDbContext;
        public vAuctionAssetRep()
        {
            _legalDbContext = new LegalDbContext();
        }
        public vAuctionAssetRep(LegalDbContext legalDbContext)
        {
            _legalDbContext = legalDbContext;
        }

        public List<vAuctionAsset> GetAllAuctionAsset(string LegalNo)
        {
            //return this.SelectDataWithCodition(D => D.LegalNo.Contains(LegalNo)).OrderBy(D => D.AuctionOrder).ThenBy(D => D.AssetID).ToList();

            try
            {
                List<vAuctionAsset> DBSet = _legalDbContext.vAuctionAsset.Where(x => x.LegalNo == LegalNo).OrderBy(D => D.AuctionOrder).ThenBy(D => D.AssetID).ToList();
                return DBSet;
            }
            catch (Exception)
            {
                throw;
            }  
        }

        public vAuctionAsset GetAuctionAssetByID(int AuctionID, int AssetID)
        {
            //return this.SelectDataWithCodition(D => D.AuctionID == AuctionID && D.AssetID == AssetID).FirstOrDefault();

            try
            {
                vAuctionAsset list = _legalDbContext.vAuctionAsset.Where(D => D.AuctionID == AuctionID && D.AssetID == AssetID).FirstOrDefault();
                return list;
            }
            catch (Exception)
            {
                throw;
            }  
        }
    }
}
