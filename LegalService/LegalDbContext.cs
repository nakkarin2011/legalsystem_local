﻿using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class LegalDbContext : DbContext
    {
        public LegalDbContext()
            : base(System.Configuration.ConfigurationManager.ConnectionStrings["LegalConnectionString"].ConnectionString)
        {
        }


        // Legal Page
        public DbSet<trLegal> trLegal { get; set; }
        public DbSet<trLegal_CourtAssign> trLegal_CourtAssign { get; set; }
        public DbSet<trAssetMapping> trAssetMapping { get; set; }
        public DbSet<trPursue> trPursue { get; set; }
        public DbSet<trSequester> trSequester { get; set; }
        public DbSet<trMortgage> trMortgage { get; set; }
        public DbSet<trAverage> trAverage { get; set; }
        public DbSet<vMortgage> vMortgage { get; set; }
        public DbSet<vAverage> vAverage { get; set; }
        public DbSet<trAuction> trAuction { get; set; }
        public DbSet<trAuctionAssign> trAuctionAssign { get; set; }
        public DbSet<trAuctionAsset> trAuctionAsset { get; set; }
        public DbSet<vAuctionAssign> vAuctionAssign { get; set; }
        public DbSet<vAuctionAsset> vAuctionAsset { get; set; }
        public DbSet<trRetain> trRetain { get; set; }
        public DbSet<vRetain> vRetain { get; set; }
        public DbSet<trRequestSequester> trRequestSequester { get; set; }
        public DbSet<trCost> trCost { get; set; }
        public DbSet<vCost> vCost { get; set; }
        public DbSet<trFee> trFee { get; set; }
        public DbSet<vFee> vFee { get; set; }
        public DbSet<vDocumentMapping> vDocumentMapping { get; set; }
        public DbSet<trDocumentMapping> trDocumentMapping { get; set; }
        public DbSet<trDocument> trDocument { get; set; }
        public DbSet<sp_GetDataLnmast> spGetDataLnmast { get; set; }
        public DbSet<sp_GetDataRelate> spGetDataRelate { get; set; }
        public DbSet<sp_GetRequest> spGetRequest { get; set; }
        public DbSet<sp_GetGuidID> spGetGuidID { get; set; }
        public DbSet<msLegalStatus> msLegalStatus { get; set; }
        public DbSet<vRequestTypeMapLegalStstus> vRequestTypeMapLegalStstus { get; set; }
        public DbSet<wfGroup> wfGroup { get; set; }
        public DbSet<trRequest> trRequest { get; set; }
        public DbSet<trRequestRelation> trRequestRelate { get; set; }
        public DbSet<vAssignLawyer> vAssignLawyer { get; set; }
        public DbSet<msRequestType> msRequestType { get; set; }
        public DbSet<msRequestStatus> msRequestStatus { get; set; }
        public DbSet<trAsset> trAsset { get; set; }
        public DbSet<vAssetMapping> vAssetMapping { get; set; }
        public DbSet<vRequestReason> vRequestReason { get; set; }
        public DbSet<vEmpLegal> vEmpLegal { get; set; }
        public DbSet<LNMAST> Lnmast { get; set; }
        public DbSet<msAttorney> msAttorney { get; set; }
        public DbSet<vAttorneyLawyer> vAttorneyLawyer { get; set; }
        public DbSet<wfDocument> wfDocument { get; set; }
        public DbSet<msDocumentType> msDocumentType { get; set; }
        public DbSet<vCollectionHeadApprove> VCollectionHeadApprove { get; set; }
        public DbSet<msAssignLawyer> msAssignLawyer { get; set; }
        public DbSet<sp_GetRequestOperPage> spGetRequestOperPage { get; set; }        
        public DbSet<sp_GetRequestInbox> spGetRequestInbox { get; set; }
        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
           // modelBuilder.Entity<wfGroup>()
           //.HasKey(c => new { c.StateID, c.EmpCode });
        }
    }
}
