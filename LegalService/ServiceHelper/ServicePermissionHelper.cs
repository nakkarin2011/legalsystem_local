﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService.ServiceHelper
{
    class ServicePermissionHelper
    {
        public const int CollectionMaker = 440;
        public const int CollectionChecker = 441;
        public const int CollectionHead = 442;
        //public const int CollectionHeadApprove = 442;
        public const int LegalSpecialist = 443;
        public const int SeniorLawyer = 444;
        public const int Lawyer = 445;
    }
    public class ServiceDDLApproverHelper
    {
        public const string ApprovedbyHead = "Approved by Head";
        public const string SendtoSeniorLawyer = "Send to Senior Lawyer";
        public const string SendtoLawyer = "Send to Lawyer";
        public const string SendtoLegal = "Send to Legal";
    }
    public class ServiceDDLLawyerHelper
    {
        public const string PleaseSelect = "- กรุณาเลือก -";
        public const string PleaseSelectEN = "- Please Select -";

    }
    public class ServiceRequestTypeHelper
    {
        public static readonly int[] FixedRequestTypeOperPage = { 1, 7 };
    }
    public class ServiceLegalStatusHelper
    {
        public static readonly string[] FixedLegalStatusReAssign = { "206","300","303" };
    }
}
