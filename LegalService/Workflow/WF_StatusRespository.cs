﻿using Bell.Linq.Repository;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace LegalService.Service
{
    public class WF_StatusRespository : BaseRepository<LegalEntities.wfStatus,LegalDataContext>
    {
        public List<LegalEntities.wfStatus> GetDisplayAll()
        {
            List<LegalEntities.wfStatus> result = new List<LegalEntities.wfStatus>();
            result = this.SelectAll();
            SetDisplayClass(result);
            return result;
        }

        protected void SetDisplayClass(List<LegalEntities.wfStatus> item)
        {
            LegalEntities.wfStatus c = new LegalEntities.wfStatus() 
            {
             StatusID = 0,
             StatusName = "-- กรุณาเลือก --"
            };
            item.Insert(0, c);
        }
    }
}