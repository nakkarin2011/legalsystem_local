﻿using System;
using System.Transactions;
using System.Linq;
using LegalEntities;
using System.Collections.Generic;
using LegalService;
using LegalSystem.Entities;

namespace LegalSystem.Service
{
    public class WorkflowRepository
    {
        public void CreateWorkfFlow(string ApplicationNo, string UserID, string ActionName, DateTime StartWorkFlow, int Version, LegalDataContext context)
        {
            var ProfileTable = context.GetTable<WF_Profile>();

            WF_Profile Profile = new WF_Profile()
            {
                ProfileID = Guid.NewGuid(),
                ApplicationNo = ApplicationNo,
                CreateBy = UserID,
                OwenBy = UserID,
                StartDate = StartWorkFlow,
                EndDate = DateTime.Now,
                StateID = 0,
                Version = Version,
                IsActive = "N",
                StatusID = 0,
            };
            ProfileTable.InsertOnSubmit(Profile);

            WF_Action Action = GetAction(ActionName, Version, context);
            WF_Step Step = GetStep(Action.ActionCode, Profile.StateID, Version, context);

            Profile = new WF_Profile()
            {
                ProfileID = Guid.NewGuid(),
                ApplicationNo = ApplicationNo,
                CreateBy = UserID,
                OwenBy = UserID,
                StartDate = DateTime.Now,
                StateID = Step.NextState,
                StatusID = Step.StatusID,
                Version = Version,
                IsActive = "A",
            };
            ProfileTable.InsertOnSubmit(Profile);
        }

        public void CreateWorkfFlow(string ApplicationNo, string UserID, string ActionName, DateTime ProspectDate, DateTime NCBDate, int Version, LegalDataContext context)
        {
            var ProfileTable = context.GetTable<WF_Profile>();

            WF_Profile Profile = new WF_Profile()
            {
                ProfileID = Guid.NewGuid(),
                ApplicationNo = ApplicationNo,
                CreateBy = UserID,
                OwenBy = UserID,
                StartDate = ProspectDate,
                EndDate = DateTime.Now,
                StateID = -1,
                Version = Version,
                IsActive = "N",
                StatusID = 0,
            };
            ProfileTable.InsertOnSubmit(Profile);

            WF_Profile Profile2 = new WF_Profile()
            {
                ProfileID = Guid.NewGuid(),
                ApplicationNo = ApplicationNo,
                CreateBy = UserID,
                OwenBy = UserID,
                StartDate = NCBDate,
                EndDate = DateTime.Now,
                StateID = 0,
                Version = Version,
                IsActive = "N",
                StatusID = 0,
            };
            ProfileTable.InsertOnSubmit(Profile2);

            WF_Action Action = GetAction(ActionName, Version, context);
            WF_Step Step = GetStep(Action.ActionCode, Profile2.StateID, Version, context);

            Profile = new WF_Profile()
            {
                ProfileID = Guid.NewGuid(),
                ApplicationNo = ApplicationNo,
                CreateBy = UserID,
                OwenBy = UserID,
                StartDate = DateTime.Now,
                StateID = Step.NextState,
                StatusID = Step.StatusID,
                Version = Version,
                IsActive = "A",
            };
            ProfileTable.InsertOnSubmit(Profile);
        }

        public void SetAction(Guid ProfileID, string ApplicationNo, string UserID, string OwnerID, string OverideBy, string ActionName, int Version, int NextState, int Status, string Comment)
        {
            using (TransactionScope oTran = new TransactionScope())
            {
                using (LegalDataContext context = new LegalDataContext())
                {
                    //var app = context.GetTable<ApplicationForm>();

                    //var appForm = app.Where(A => A.ApplicationNo == ApplicationNo).FirstOrDefault();
                    //appForm.Comment = Comment;

                    var ProfileTable = context.GetTable<WF_Profile>();

                    WF_Profile Profile = ProfileTable.Where(W => W.ApplicationNo == ApplicationNo && W.IsActive == "A").FirstOrDefault();

                    if (Profile != null && Profile.ProfileID == ProfileID)
                    {
                        Profile.EndDate = DateTime.Now;
                        Profile.IsActive = "N";

                        WF_Action Action = null;
                        WF_Step Step = null;

                        if (ActionName == "OverideMethod")
                        {
                            Step = new WF_Step()
                            {
                                NextState = NextState,
                                StatusID = Status
                            };
                        }
                        else
                        {
                            Action = GetAction(ActionName, Version, context);
                            Step = GetStep(Action.ActionCode, Profile.StateID, Version, context);
                        }

                        WF_Profile ProfileInsert = new WF_Profile()
                        {
                            ProfileID = Guid.NewGuid(),
                            ApplicationNo = ApplicationNo,
                            CreateBy = UserID,
                            OwenBy = OwnerID,
                            OverrideBy = OverideBy,
                            StartDate = DateTime.Now,
                            StateID = Step.NextState,
                            StatusID = Step.StatusID,
                            Version = Version,
                            IsActive = "A",
                        };
                        ProfileTable.InsertOnSubmit(ProfileInsert);
                    }
                    else
                        throw new Exception("ไม่สามารถดำเนินการ Workflow ได้เนื่องจาก Profile ID ไม่ถูกต้อง");
                    context.SubmitChanges();
                }
                oTran.Complete();
            }
        }

        public void SetAction(Guid ProfileID, string ApplicationNo, string UserID, string OwnerID, string OverideBy, string ActionName, int Version, LegalDataContext context)
        {
            var ProfileTable = context.GetTable<WF_Profile>();

            WF_Profile Profile = ProfileTable.Where(W => W.ApplicationNo == ApplicationNo && W.IsActive == "A").FirstOrDefault();
            //= GetProfileByApplication(ApplicationNo, context);

            if (Profile != null && Profile.ProfileID == ProfileID)
            {
                Profile.EndDate = DateTime.Now;
                Profile.IsActive = "N";

                WF_Action Action = null;// GetAction(ActionName, Version, context);
                WF_Step Step = null; //GetStep(Action.ActionCode, Profile.StateID, Version, context);

                if (ActionName == "OverideMethod")
                {
                    Step = new WF_Step()
                    {
                        NextState = Profile.StateID,
                        StatusID = Profile.StatusID
                    };
                }
                else
                {
                    Action = GetAction(ActionName, Version, context);
                    Step = GetStep(Action.ActionCode, Profile.StateID, Version, context);
                }

                //=============================================================
                //V1.3 AddOn ถ้าเคยเป็น Status 111 แลัว ให้เปลี่ยนจาก 100 เป็น 111 เลย

                if (Step.StatusID == 100)
                {
                    var tmp = ProfileTable.Where(S => S.ApplicationNo == ApplicationNo && S.StatusID == 111).FirstOrDefault();
                    if (tmp != null)
                        Step.StatusID = 111;
                }

                //=============================================================

                WF_Profile ProfileInsert = new WF_Profile()
                {
                    ProfileID = Guid.NewGuid(),
                    ApplicationNo = ApplicationNo,
                    CreateBy = UserID,
                    OwenBy = OwnerID,
                    OverrideBy = OverideBy,
                    StartDate = DateTime.Now,
                    StateID = Step.NextState,
                    StatusID = Step.StatusID,
                    Version = Version,
                    IsActive = "A",
                };
                ProfileTable.InsertOnSubmit(ProfileInsert);
            }
            else
                throw new Exception("ไม่สามารถดำเนินการ Workflow ได้เนื่องจาก Profile ID ไม่ถูกต้อง");
        }

        public void OrverideReject(Guid ProfileID, string ApplicationNo, string UserID, int Version, LegalDataContext context)
        {
            var ProfileTable = context.GetTable<WF_Profile>();

            WF_Profile Profile = ProfileTable.Where(W => W.ApplicationNo == ApplicationNo && W.IsActive == "A").FirstOrDefault();
            //= GetProfileByApplication(ApplicationNo, context);

            if (Profile != null && Profile.ProfileID == ProfileID)
            {
                Profile.EndDate = DateTime.Now;
                Profile.IsActive = "N";


                WF_Profile ProfileInsert = new WF_Profile()
                {
                    ProfileID = Guid.NewGuid(),
                    ApplicationNo = ApplicationNo,
                    CreateBy = UserID,
                    OwenBy = "System",
                    StartDate = DateTime.Now,
                    StateID = 998,
                    StatusID = 998,
                    Version = Version,
                    IsActive = "A",
                };
                ProfileTable.InsertOnSubmit(ProfileInsert);
            }
        }

        public WF_Profile GetProfileByApplication(string ApplicaionNo)
        {
            WF_Profile result = null;

            using (LegalDataContext context = new LegalDataContext())
            {
                var ProfileTable = context.GetTable<WF_Profile>();
                result = ProfileTable.Where(P => P.ApplicationNo == ApplicaionNo && P.IsActive == "A").FirstOrDefault();
            }

            return result;
        }

        public WF_Profile GetProfileByApplication(string ApplicaionNo, LegalDataContext context)
        {
            WF_Profile result = null;

            var ProfileTable = context.GetTable<WF_Profile>();
            result = ProfileTable.Where(P => P.ApplicationNo == ApplicaionNo && P.IsActive == "A").FirstOrDefault();

            return result;
        }

        public List<WF_Profile> GetWorkflow(string ApplicationNo)
        {
            List<WF_Profile> result = null;

            using (LegalDataContext context = new LegalDataContext())
            {
                var WFTable = context.GetTable<WF_Profile>();
                result = WFTable.Where(A => A.ApplicationNo == ApplicationNo).OrderBy(O => O.StartDate).ToList();
            }
            return result;
        }

        public WF_State GetState(int State)
        {
            WF_State result = null;

            using (LegalDataContext context = new LegalDataContext())
            {
                var WFT = context.GetTable<WF_State>();
                result = WFT.Where(S => S.StateID == State).FirstOrDefault();
            }
            return result;

        }

        private WF_Action GetAction(string ActionName, int Version, LegalDataContext context)
        {
            var ActionTable = context.GetTable<WF_Action>();

            return ActionTable.Where(A => A.ActionName == ActionName && A.Version == Version).FirstOrDefault();
        }

        private WF_Step GetStep(int ActionCode, int StateID, int Version, LegalDataContext context)
        {
            var StepTable = context.GetTable<WF_Step>();

            return StepTable.Where(S => S.ActionID == ActionCode && S.StateID == StateID && S.Version == Version).FirstOrDefault();
        }

        internal void Deletegate(WF_Profile Profile, string NewOwner, string CreateBy, LegalDataContext context)
        {
            DateTime WFDate = DateTime.Now;
            var ProfileTable = context.GetTable<WF_Profile>();
            var OldProfile = ProfileTable.Where(P => P.ProfileID == Profile.ProfileID && P.IsActive == "A").FirstOrDefault();

            //Update Mode
            if (OldProfile != null)
            {
                OldProfile.EndDate = WFDate;
                OldProfile.IsActive = "N";
                context.SubmitChanges();

                WF_Profile NewProfile = new WF_Profile()
                {
                    ProfileID = Guid.NewGuid(),
                    ApplicationNo = OldProfile.ApplicationNo,
                    StateID = OldProfile.StateID,
                    StatusID = OldProfile.StatusID,
                    StartDate = WFDate,
                    CreateBy = CreateBy,
                    OwenBy = NewOwner,
                    Version = OldProfile.Version,
                    IsActive = "A"
                };
                ProfileTable.InsertOnSubmit(NewProfile);
                context.SubmitChanges();
            }
            else
            {
                throw new Exception("เกิดข้อผิดพลาดระหว่างทำงาน Workflow อาจมีผู้ใช้งานอยู่หรือทำการส่งงานไป step อื่นแล้ว");
            }
        }

        public List<WF_Profile> GetStatus(int Status, int Daily)
        {
            //List<vRequestDocument> tmp = null;
            List<WF_Profile> result = null;
            DateTime Date = DateTime.Today;

            //using (vRequestDocumentRepository vRep = new vRequestDocumentRepository())
            //    tmp = vRep.SelectDataWithCodition(S => S.StatusID == Status && S.IsActive == "A" && S.WorkingDate == Date);

            //if (tmp != null && tmp.Count > 0)
            //{
            //    result = new List<WF_Profile>();
            //    foreach (vRequestDocument item in tmp)
            //    {
            //        WF_Profile tmpRD = new WF_Profile()
            //        {
            //            ApplicationNo = item.ApplicationNo,
            //            CreateBy = item.CreateBy,
            //            EndDate = item.WorkingDate.AddDays(2),
            //            IsActive = item.IsActive,
            //            OverrideBy = item.OverrideBy,
            //            OwenBy = item.OwenBy,
            //            StartDate = item.StartDate,
            //            StateID = item.StateID,
            //            StatusID = item.StatusID,
            //            Version = item.Version
            //        };
            //        result.Add(tmpRD);
            //    }
            //}

            return result;
        }

        internal void RejectWithAmen(string ApplicationNo, string CreateBy)
        {
            DateTime dtStartDate = DateTime.Now;
            using (LegalDataContext context = new LegalDataContext())
            {
                var ProfileTable = context.GetTable<WF_Profile>();

                var tmpWorkflow = ProfileTable.Where(F => F.ApplicationNo == ApplicationNo && F.IsActive == "A").FirstOrDefault();
                if (tmpWorkflow != null)
                {
                    tmpWorkflow.EndDate = dtStartDate;
                    tmpWorkflow.IsActive = "N";
                }

                WF_Profile wf = new WF_Profile()
                {
                    ProfileID = Guid.NewGuid(),
                    ApplicationNo = ApplicationNo,
                    StateID = 998,
                    StatusID = 920,
                    StartDate = dtStartDate,
                    EndDate = DateTime.Now,
                    CreateBy = CreateBy,
                    OwenBy = "System",
                    Version = 1,
                    IsActive = "A"
                };
                ProfileTable.InsertOnSubmit(wf);
                context.SubmitChanges();
            }
        }

        public void SetAPDone(string ApplicationNo, string CreateBy)
        {
            DateTime dtStartDate = DateTime.Now;
            WF_Profile WfCurrent = this.GetProfileByApplication(ApplicationNo);

            using (TransactionScope oTran = new TransactionScope())
            {
                using (LegalDataContext context = new LegalDataContext())
                {
                    var ProfileTable = context.GetTable<WF_Profile>();

                    //Set Not Active
                    var tmpWorkflow = ProfileTable.Where(F => F.ApplicationNo == ApplicationNo && F.IsActive == "A").FirstOrDefault();
                    if (tmpWorkflow != null)
                    {
                        tmpWorkflow.EndDate = dtStartDate;
                        tmpWorkflow.IsActive = "N";
                    }

                    //Set APDone
                    WF_Profile wf = new WF_Profile()
                    {
                        ProfileID = Guid.NewGuid(),
                        ApplicationNo = ApplicationNo,
                        StateID = 20,
                        StatusID = 105,
                        StartDate = dtStartDate,
                        EndDate = DateTime.Now,
                        CreateBy = CreateBy,
                        OwenBy = "System",
                        Version = 1,
                        IsActive = "N"
                    };
                    ProfileTable.InsertOnSubmit(wf);

                    //Reverse to current state
                    WF_Profile wfReverse = new WF_Profile()
                    {
                        ProfileID = Guid.NewGuid(),
                        ApplicationNo = ApplicationNo,
                        StateID = tmpWorkflow.StateID,
                        StatusID = tmpWorkflow.StatusID,
                        StartDate = DateTime.Now,
                        CreateBy = CreateBy,
                        OwenBy = tmpWorkflow.OwenBy,
                        Version = 1,
                        IsActive = "A"
                    };
                    ProfileTable.InsertOnSubmit(wfReverse);
                    context.SubmitChanges();
                }
                oTran.Complete();
            }
        }
    }
}