﻿using System;
using LegalData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LegalService
{
    public class trfindAssetRep : IRepository<trfindAsset>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public trfindAssetRep()
        {
            _context = new LegalSystemEntities();
        }

        public void DeleteData(trfindAsset model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<trfindAsset> GetAll()
        {
            return _context.trfindAssets.ToList();
        }

        public void InsertData(trfindAsset model)
        {
            try
            {
                _context.trfindAssets.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trfindAsset> SearchData(trfindAsset model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(trfindAsset model)
        {
            try
            {
                _context.trfindAssets.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
