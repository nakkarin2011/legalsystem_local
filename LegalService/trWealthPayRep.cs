﻿using System;
using LegalData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LegalService
{
    public class trWealthPayRep : IRepository<trWealthPay>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public trWealthPayRep()
        {
            _context = new LegalSystemEntities();
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public void DeleteData(trWealthPay model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trWealthPay> GetAll()
        {
            return _context.trWealthPays.ToList();
        }

        public void InsertData(trWealthPay model)
        {
            try
            {
                _context.trWealthPays.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trWealthPay> SearchData(trWealthPay model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(trWealthPay model)
        {
            try
            {
                _context.trWealthPays.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
