﻿using System;
using System.Collections.Generic;
using System.Text;
using LegalEntities;

namespace LegalService
{
    public class LegalUnitOfWork : IDisposable
    {
        public LegalDbContext context = new LegalDbContext();

    
        
        private GenericRepository<trLegal_CourtAssign> trCourtAssignRep;
        private GenericRepository<trPursue> trPursueRep;
        private GenericRepository<trSequester> trSequesterRep;
        private GenericRepository<trMortgage> trMortgageRep;
        private GenericRepository<trAverage> trAverageRep;
        private GenericRepository<trAuction> trAuctionRep;
        private GenericRepository<trAuctionAssign> trAuctionAssignRep;
        private GenericRepository<trAuctionAsset> trAuctionAssetRep;
        private GenericRepository<vAuctionAssign> vAuctionAssignRep;
        private GenericRepository<vAuctionAsset> vAuctionAssetRep;
        private GenericRepository<trRequestSequester> trRequestSequesterRep;
        private GenericRepository<trCost> trCostRep;
        private GenericRepository<trFee> trFeeRep;
        private GenericRepository<vCost> vCostRep;
        private GenericRepository<vFee> vFeeRep;
        private GenericRepository<trRetain> trRetainRep;
        private GenericRepository<vRetain> vRetainRep;
        private GenericRepository<vDocumentMapping> vDocumentMappingRep;
        private GenericRepository<trDocumentMapping> trDocumentMappingRep;
        private GenericRepository<trDocument> trDocumentRep;
        private trLegalRep trLegalRep;
        

        private msRequestStatusRep msrequestStatusRep;
        private msLegalStatusRep mslegalStatusRep;
        private trRequestRep trRequestRep;
        private vRequestTypeMapLegalStstusRep vrequestTypeMapLegalStstusRep;
        private wfGroupRep wfgroupRep;
        private msRequestTypeRep msRequestTypeRep;
        private vAssetMappingRep vAssetMappingRep;
        private trAssetMappingRep trAssetMappingRep;
        private trRequestRelationRep trRequestRelateRep;
        private vAssignLawyerRep vAssignLawyerRep;
        private trAssetRep trAssetRep;
        private vRequestReasonRep vRequestReasonRep;
        private vEmpLegalRep vEmpLegalRep;
        private LNMASTRep LNMASTRep;
        private msAttorneyRep msAttorneyRep;
        private msDocumentTypeRep msDocumentTypeRep;
        private vAttorneyLawyerRep vAttorneyLawyerRep;
        private wfDocumentRep wfDocumentRep;
        private LegalStore store;
        private vCollectionHeadApproveRep vCollectionHeadApproveRep;
        private msAssignLawyerRep msAssignLawyerRep;


        public GenericRepository<trLegal_CourtAssign> TrCourtAssignRep
        {
            get
            {
                if (this.trCourtAssignRep == null)
                {
                    this.trCourtAssignRep = new GenericRepository<trLegal_CourtAssign>(context);
                }
                return trCourtAssignRep;
            }
        }
        public GenericRepository<trPursue> TrPursueRep
        {
            get
            {
                if (this.trPursueRep == null)
                {
                    this.trPursueRep = new GenericRepository<trPursue>(context);
                }
                return trPursueRep;
            }
        }
        public GenericRepository<trSequester> TrSequesterRep
        {
            get
            {
                if (this.trSequesterRep == null)
                {
                    this.trSequesterRep = new GenericRepository<trSequester>(context);
                }
                return trSequesterRep;
            }
        }
        public GenericRepository<trMortgage> TrMortgageRep
        {
            get
            {
                if (this.trMortgageRep == null)
                {
                    this.trMortgageRep = new GenericRepository<trMortgage>(context);
                }
                return trMortgageRep;
            }
        }
        public GenericRepository<trAverage> TrAverageRep
        {
            get
            {
                if (this.trAverageRep == null)
                {
                    this.trAverageRep = new GenericRepository<trAverage>(context);
                }
                return trAverageRep;
            }
        }
        public GenericRepository<trAuction> TrAuctionRep
        {
            get
            {
                if (this.trAuctionRep == null)
                {
                    this.trAuctionRep = new GenericRepository<trAuction>(context);
                }
                return trAuctionRep;
            }
        }
        public GenericRepository<trAuctionAssign> TrAuctionAssignRep
        {
            get
            {
                if (this.trAuctionAssignRep == null)
                {
                    this.trAuctionAssignRep = new GenericRepository<trAuctionAssign>(context);
                }
                return trAuctionAssignRep;
            }
        }
        public GenericRepository<trAuctionAsset> TrAuctionAssetRep
        {
            get
            {
                if (this.trAuctionAssetRep == null)
                {
                    this.trAuctionAssetRep = new GenericRepository<trAuctionAsset>(context);
                }
                return trAuctionAssetRep;
            }
        }
        public GenericRepository<vAuctionAssign> ViewAuctionAssignRep
        {
            get
            {
                if (this.vAuctionAssignRep == null)
                {
                    this.vAuctionAssignRep = new GenericRepository<vAuctionAssign>(context);
                }
                return vAuctionAssignRep;
            }
        }
        public GenericRepository<vAuctionAsset> ViewAuctionAssetRep
        {
            get
            {
                if (this.vAuctionAssetRep == null)
                {
                    this.vAuctionAssetRep = new GenericRepository<vAuctionAsset>(context);
                }
                return vAuctionAssetRep;
            }
        }
        public GenericRepository<trRetain> TrRetainRep
        {
            get
            {
                if (this.trRetainRep == null)
                {
                    this.trRetainRep = new GenericRepository<trRetain>(context);
                }
                return trRetainRep;
            }
        }
        public GenericRepository<vRetain> ViewRetainRep
        {
            get
            {
                if (this.vRetainRep == null)
                {
                    this.vRetainRep = new GenericRepository<vRetain>(context);
                }
                return vRetainRep;
            }
        }
        public GenericRepository<trRequestSequester> TrRequestSequesterRep
        {
            get
            {
                if (this.trRequestSequesterRep == null)
                {
                    this.trRequestSequesterRep = new GenericRepository<trRequestSequester>(context);
                }
                return trRequestSequesterRep;
            }
        }
       
        //public GenericRepository<vAssignLawyer> ViewAssignLawyerRep
        //{
        //    get
        //    {
        //        if (this.vAssignLawyerRep == null)
        //        {
        //            this.vAssignLawyerRep = new GenericRepository<vAssignLawyer>(context);
        //        }
        //        return vAssignLawyerRep;
        //    }
        //}
        public GenericRepository<trCost> TrCostRep
        {
            get
            {
                if (this.trCostRep == null)
                {
                    this.trCostRep = new GenericRepository<trCost>(context);
                }
                return trCostRep;
            }
        }
        public GenericRepository<trFee> TrFeeRep
        {
            get
            {
                if (this.trFeeRep == null)
                {
                    this.trFeeRep = new GenericRepository<trFee>(context);
                }
                return trFeeRep;
            }
        }
        public GenericRepository<vCost> ViewCostRep
        {
            get
            {
                if (this.vCostRep == null)
                {
                    this.vCostRep = new GenericRepository<vCost>(context);
                }
                return vCostRep;
            }
        }
        public GenericRepository<vFee> ViewFeeRep
        {
            get
            {
                if (this.vFeeRep == null)
                {
                    this.vFeeRep = new GenericRepository<vFee>(context);
                }
                return vFeeRep;
            }
        }
        public GenericRepository<vDocumentMapping> ViewDocumentMappingRep
        {
            get
            {
                if (this.vDocumentMappingRep == null)
                {
                    this.vDocumentMappingRep = new GenericRepository<vDocumentMapping>(context);
                }
                return vDocumentMappingRep;
            }
        }
        public GenericRepository<trDocumentMapping> TrDocumentMappingRep
        {
            get
            {
                if (this.trDocumentMappingRep == null)
                {
                    this.trDocumentMappingRep = new GenericRepository<trDocumentMapping>(context);
                }
                return trDocumentMappingRep;
            }
        }
        public GenericRepository<trDocument> TrDocumentRep
        {
            get
            {
                if (this.trDocumentRep == null)
                {
                    this.trDocumentRep = new GenericRepository<trDocument>(context);
                }
                return trDocumentRep;
            }
        }
        public trLegalRep TrLegalRep
        {
            get
            {
                if (this.trLegalRep == null)
                {
                    this.trLegalRep = new trLegalRep(context);
                }
                return trLegalRep;
            }
        }


        public trAssetRep TrAssetRep
        {
            get
            {
                if (this.trAssetRep == null)
                {
                    this.trAssetRep = new trAssetRep(context);
                }
                return trAssetRep;
            }
        }
        public msLegalStatusRep MSLegalStatusRep
        {
            get
            {
                if (this.mslegalStatusRep == null)
                {
                    this.mslegalStatusRep = new msLegalStatusRep(context);
                }
                return mslegalStatusRep;
            }
        }
        public wfDocumentRep WFDocumentRep
        {
            get
            {
                if (this.wfDocumentRep == null)
                {
                    this.wfDocumentRep = new wfDocumentRep(context);
                }
                return this.wfDocumentRep;
            }
        }
        public msDocumentTypeRep MSDocumentTypeRep
        {
            get
            {
                if (this.msDocumentTypeRep == null)
                {
                    this.msDocumentTypeRep = new msDocumentTypeRep(context);
                }
                return this.msDocumentTypeRep;
            }
        }
        public trRequestRep TrRequestRep
        {
            get
            {
                if (this.trRequestRep == null)
                {
                    this.trRequestRep = new trRequestRep(context);
                }
                return trRequestRep;
            }
        }
        public trRequestRelationRep TrRequestRelateRep
        {
            get
            {
                if (this.trRequestRelateRep == null)
                {
                    this.trRequestRelateRep = new trRequestRelationRep(context);
                }
                return trRequestRelateRep;
            }
        }
        public vAssignLawyerRep VAssignLawyerRep
        {
            get
            {
                if (this.vAssignLawyerRep == null)
                {
                    this.vAssignLawyerRep = new vAssignLawyerRep(context);
                }
                return vAssignLawyerRep;
            }
        }
        public msRequestTypeRep MSRequestTypeRep
        {
            get
            {
                if (this.msRequestTypeRep == null)
                {
                    this.msRequestTypeRep = new msRequestTypeRep(context);
                }
                return msRequestTypeRep;
            }
        }
        public msRequestStatusRep MSRequestStatusRep
        {
            get
            {
                if (this.msrequestStatusRep == null)
                {
                    this.msrequestStatusRep = new msRequestStatusRep(context);
                }
                return msrequestStatusRep;
            }
        }
       
        public vRequestTypeMapLegalStstusRep VRequestTypeMapLegalStstusRep
        {
            get
            {
                if (this.vrequestTypeMapLegalStstusRep == null)
                {
                    this.vrequestTypeMapLegalStstusRep = new vRequestTypeMapLegalStstusRep(context);
                }
                return vrequestTypeMapLegalStstusRep;
            }
        }
        public vRequestReasonRep VRequestReasonRep
        {
            get
            {
                if (this.vRequestReasonRep == null)
                {
                    this.vRequestReasonRep = new vRequestReasonRep(context);
                }
                return vRequestReasonRep;
            }
        }
        public wfGroupRep WFGroupRep
        {
            get
            {
                if (this.wfgroupRep == null)
                {
                    this.wfgroupRep = new wfGroupRep(context);
                }
                return wfgroupRep;
            }
        }
        public vAssetMappingRep VAssetMappingRep
        {
            get
            {
                if (this.vAssetMappingRep == null)
                {
                    this.vAssetMappingRep = new vAssetMappingRep(context);
                }
                return this.vAssetMappingRep;
            }
        }
        public trAssetMappingRep TrAssetMappingRep
        {
            get
            {
                if (this.trAssetMappingRep == null)
                {
                    this.trAssetMappingRep = new trAssetMappingRep(context);
                }
                return trAssetMappingRep;
            }
        }
        public vEmpLegalRep VEmpLegalRep
        {
            get
            {
                if (this.vEmpLegalRep == null)
                {
                    this.vEmpLegalRep = new vEmpLegalRep(context);
                }
                return vEmpLegalRep;
            }
        }
        public vCollectionHeadApproveRep VCollectionHeadApproveRep
        {
            get
            {
                if (this.vCollectionHeadApproveRep == null)
                {
                    this.vCollectionHeadApproveRep = new vCollectionHeadApproveRep(context);
                }
                return this.vCollectionHeadApproveRep;
            }
        }

        public LNMASTRep LnmastRep
        {
            get
            {
                if (this.LNMASTRep == null)
                {
                    this.LNMASTRep = new LNMASTRep(context);
                }
                return LNMASTRep;
            }
        }
        public msAttorneyRep MSAttorneyRep
        {
            get
            {
                if (this.msAttorneyRep == null)
                {
                    this.msAttorneyRep = new msAttorneyRep(context);
                }
                return this.msAttorneyRep;
            }
        }
        public vAttorneyLawyerRep VAttorneyLawyerRep
        {
            get
            {
                if (this.vAttorneyLawyerRep == null)
                {
                    this.vAttorneyLawyerRep = new vAttorneyLawyerRep(context);
                }
                return this.vAttorneyLawyerRep;
            }
        }
        public LegalStore LegalStoreProcedure
        {
            get
            {
                if (this.store == null)
                {
                    this.store = new LegalStore(context);
                }
                return store;
            }
        }

        public msAssignLawyerRep MSAssignLawyerRep
        {
            get
            {
                if (this.msAssignLawyerRep == null)
                {
                    this.msAssignLawyerRep = new msAssignLawyerRep(context);
                }
                return msAssignLawyerRep;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }      
    }
}

