﻿using LegalEntities;
using LegalService.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class LegalStore
    {
        private readonly LegalDbContext context;
        public LegalStore(LegalDbContext context)
        {
            this.context = context;
        }
        public List<LNMAST> GetDataLnmast(string LinkAs400, int CIF)
        {
            var tmp = this.context.spGetDataLnmast.SqlQuery("exec sp_GetDataLnmast @Link,@CIF", new SqlParameter() { ParameterName = "@Link", DbType = DbType.String, Value = LinkAs400 }, new SqlParameter() { ParameterName = "@CIF", DbType = DbType.String, Value = CIF }).ToList();
            List<LNMAST> datas = new List<LNMAST>();
            for (int i = 0; i < tmp.Count; i++)
                datas.Add(new LNMAST
                {
                    ACCTNO = tmp[i].ACCTNO,
                    ACCI3 = tmp[i].ACCI3,
                    ACCINT = tmp[i].ACCINT,
                    ACNAME = tmp[i].ACNAME,
                    ACTYPE = tmp[i].ACTYPE,
                    CBAL = tmp[i].CBAL,
                    BRN = tmp[i].BRN,
                    CFADDR = tmp[i].CFADDR,
                    CFNAME = tmp[i].CFNAME,
                    CIFNO = tmp[i].CIFNO,
                    CollateralDesc = tmp[i].CollateralDesc,
                    FCODE = tmp[i].FCODE,
                    FSEQ = tmp[i].FSEQ,
                    INTRATE = tmp[i].INTRATE,
                    LMCOST = tmp[i].LMCOST,
                    LMPRDC = tmp[i].LMPRDC,
                    TDRCNT = tmp[i].TDRCNT,
                    ORGAMT = tmp[i].ORGAMT,
                    ORGDT = tmp[i].ORGDT,
                    PDDAYS = tmp[i].PDDAYS,
                    PENIN3 = tmp[i].PENIN3,
                    PENINT = tmp[i].PENINT,
                    TYPE = tmp[i].TYPE,
                    STATUS = tmp[i].STATUS,
                    STAGE = tmp[i].STAGE,
                });
            return datas;
        }
        public List<sp_GetDataRelate> GetDataRelate(string LinkAs400, decimal Account, string Collateral)
        {
            List<sp_GetDataRelate> datas = this.context.spGetDataRelate.SqlQuery("exec sp_GetDataRelate @Link,@Account,@GuarID", new SqlParameter() { ParameterName = "@Link", DbType = DbType.String, Value = LinkAs400 }, new SqlParameter() { ParameterName = "@Account", DbType = DbType.String, Value = Account }, new SqlParameter() { ParameterName = "@GuarID", DbType = DbType.String, Value = Collateral }).ToList<sp_GetDataRelate>();
            return datas;
        }

        public Guid GetGuidID()
        {
            List<sp_GetGuidID> datas = this.context.spGetGuidID.SqlQuery("exec sp_GetGuidID").ToList<sp_GetGuidID>();
            return datas.FirstOrDefault().ID;
        }
        public List<sp_GetRequest> GetRequest(string RequestType,string RequestNo,string RequestDate,string CIF,string RequestStatus,string LegalStatus)
        {
            List<sp_GetRequest> datas = this.context.spGetRequest.SqlQuery("exec sp_GetRequest @RequestType,@RequestNo,@RequestDate,@CIF,@RequestStatus,@LegalStatus"
                , new SqlParameter() { ParameterName = "@RequestType", DbType = DbType.String, Value = RequestType }
                , new SqlParameter() { ParameterName = "@RequestNo", DbType = DbType.String, Value = RequestNo }
                , new SqlParameter() { ParameterName = "@RequestDate", DbType = DbType.String, Value = RequestDate }
                , new SqlParameter() { ParameterName = "@CIF", DbType = DbType.String, Value = CIF }
                , new SqlParameter() { ParameterName = "@RequestStatus", DbType = DbType.String, Value = RequestStatus }
                , new SqlParameter() { ParameterName = "@LegalStatus", DbType = DbType.String, Value = LegalStatus }).ToList();
            return datas.OrderBy(O => O.RequestNo).ToList();
        }

        public List<sp_GetRequestOperPage> GetRequestOperPage(string RequestType, string RequestNo, string CIF, string Name, string EmailSendStatus,string EmailOSSendStatus)
        {
            var datas = this.context.spGetRequestOperPage.SqlQuery("exec sp_GetRequestOperPage @RequestType,@RequestNo,@CIF,@NAME,@EmailSendStatus,@EmailOSSendStatus"
                 , new SqlParameter() { ParameterName = "@RequestType", DbType = DbType.String, Value = RequestType }
                 , new SqlParameter() { ParameterName = "@RequestNo", DbType = DbType.String, Value = RequestNo }
                 , new SqlParameter() { ParameterName = "@CIF", DbType = DbType.String, Value = CIF }
                 , new SqlParameter() { ParameterName = "@NAME", DbType = DbType.String, Value = Name }
                 , new SqlParameter() { ParameterName = "@EmailSendStatus", DbType = DbType.String, Value = EmailSendStatus }
                 , new SqlParameter() { ParameterName = "@EmailOSSendStatus", DbType = DbType.String, Value = EmailOSSendStatus }
                 ).ToList();
            return datas.OrderBy(O => O.RequestNo).ToList();
        }
        public List<sp_GetRequestInbox> GetRequestInbox(string RequestType, string RequestNo, string RequestDate, string CIF, string RequestStatus, string LegalStatus,string LegalNo, string TypeSearch)
        {
            var datas = this.context.spGetRequestInbox.SqlQuery("exec sp_GetRequestInbox @RequestType,@RequestNo,@RequestDate,@CIF,@RequestStatus,@LegalStatus,@LegalNo,@TypeSearch"
                , new SqlParameter() { ParameterName = "@RequestType", DbType = DbType.String, Value = RequestType }
                , new SqlParameter() { ParameterName = "@RequestNo", DbType = DbType.String, Value = RequestNo }
                , new SqlParameter() { ParameterName = "@RequestDate", DbType = DbType.String, Value = RequestDate }
                , new SqlParameter() { ParameterName = "@CIF", DbType = DbType.String, Value = CIF }
                , new SqlParameter() { ParameterName = "@RequestStatus", DbType = DbType.String, Value = RequestStatus }
                , new SqlParameter() { ParameterName = "@LegalStatus", DbType = DbType.String, Value = LegalStatus }
                , new SqlParameter() { ParameterName = "@LegalNo", DbType = DbType.String, Value = LegalNo }
                , new SqlParameter() { ParameterName = "@TypeSearch", DbType = DbType.String, Value = TypeSearch }).ToList();

            return datas.OrderBy(O => O.RequestNo).Where(w => !w.RequestNo.Contains("R")).ToList();
        }

    }

}
