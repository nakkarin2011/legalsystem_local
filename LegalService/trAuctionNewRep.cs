﻿using LegalData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalService
{
    public class trAuctionNewRep : IRepository<trAuctionNew>, IDisposable
    {
        private readonly LegalSystemEntities _context;

        public trAuctionNewRep()
        {
            _context = new LegalSystemEntities();
        }
        public void Dispose()
        {
            _context.Dispose();
        }


        public void DeleteData(trAuctionNew model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trAuctionNew> GetAll()
        {
            return _context.trAuctionNews.ToList();
        }

        public void InsertData(trAuctionNew model)
        {
            try
            {
                _context.trAuctionNews.Add(model);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<trAuctionNew> SearchData(trAuctionNew model)
        {
            throw new NotImplementedException();
        }

        public void UpdateData(trAuctionNew model)
        {
            try
            {
                _context.trAuctionNews.Attach(model);
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                throw;
            }
        }
    }
}
