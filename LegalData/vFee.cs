//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LegalData
{
    using System;
    using System.Collections.Generic;
    
    public partial class vFee
    {
        public int FeeID { get; set; }
        public string LegalNo { get; set; }
        public string CostTypeName { get; set; }
        public int CostTypeID { get; set; }
        public string TFlag { get; set; }
        public string SFlag { get; set; }
        public Nullable<decimal> CourtPayAmt { get; set; }
        public Nullable<decimal> CourtRefundAmt { get; set; }
        public Nullable<System.DateTime> CourtRefundDate { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public string ChequeNo { get; set; }
        public string BankBranch { get; set; }
        public Nullable<System.DateTime> SendChqDate { get; set; }
    }
}
