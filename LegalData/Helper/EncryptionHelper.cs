﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Helper
{
    public enum EncodeMode
    {
        ASCII, Base64
    };

    public class EncryptionHelper
    {
        public static string EncryptString(string message, string KeyString, string IVString, EncodeMode encodeMode)
        {

            byte[] Key;
            byte[] IV;

            if (encodeMode == EncodeMode.ASCII)
            {
                Key = ASCIIEncoding.UTF8.GetBytes(KeyString);
                IV = ASCIIEncoding.UTF8.GetBytes(IVString);
            }

            else
            {
                Key = System.Convert.FromBase64String(KeyString);
                IV = System.Convert.FromBase64String(IVString);
            }

            string encrypted = null;
            RijndaelManaged rj = new RijndaelManaged();
            rj.Key = Key;
            rj.IV = IV;
            rj.Mode = CipherMode.CBC;

            try
            {
                MemoryStream ms = new MemoryStream();

                using (CryptoStream cs = new CryptoStream(ms, rj.CreateEncryptor(Key, IV), CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        sw.Write(message);
                        sw.Close();
                    }
                    cs.Close();
                }
                byte[] encoded = ms.ToArray();
                encrypted = Convert.ToBase64String(encoded);

                ms.Close();
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("A file error occurred: {0}", e.Message);
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                rj.Clear();
            }
            return encrypted;
        }
        public static string DecryptString(string cipherData, string keyString, string ivString, EncodeMode encodeMode)
        {
            byte[] key;
            byte[] iv;


            if (encodeMode == EncodeMode.ASCII)
            {
                key = Encoding.UTF8.GetBytes(keyString);
                iv = Encoding.UTF8.GetBytes(ivString);
            }
            else
            {
                key = System.Convert.FromBase64String(keyString);
                iv = System.Convert.FromBase64String(ivString);
            }

            try
            {
                using (var rijndaelManaged =
                       new RijndaelManaged { Key = key, IV = iv, Mode = CipherMode.CBC })
                using (var memoryStream =
                       new MemoryStream(Convert.FromBase64String(cipherData)))
                using (var cryptoStream =
                       new CryptoStream(memoryStream,
                           rijndaelManaged.CreateDecryptor(key, iv),
                           CryptoStreamMode.Read))
                {
                    return new StreamReader(cryptoStream).ReadToEnd();
                }
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            // You may want to catch more exceptions here...
        }

        public static KeyResult GenerateKey()
        {

            var Rijndael = new RijndaelManaged();
            Rijndael.GenerateKey();
            Rijndael.GenerateIV();

            Console.WriteLine(Convert.ToBase64String(Rijndael.Key));
            Console.WriteLine(Convert.ToBase64String(Rijndael.IV));

            return new KeyResult { Key = Convert.ToBase64String(Rijndael.Key), IV = Convert.ToBase64String(Rijndael.IV) };
        }
    }
    public class KeyResult
    {

        public string Key { get; set; }

        public string IV { get; set; }
    }
}
