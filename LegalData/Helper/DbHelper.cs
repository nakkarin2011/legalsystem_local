﻿using LegalData.Extension;
using LegalData.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Helper
{
    public class DbHelper
    {
        public static string ConnectionName = "DefaultConnection";
        public static string GetConnection(string name)
        {
            string connectionString = string.Empty;
            try
            {
                connectionString = ConfigurationManager.ConnectionStrings[name].ConnectionString;
            }

            catch { }

            return connectionString;
        }
        public static string NewBookingNo()
        {

            string BookingNo = string.Empty;
            try
            {
                BookingNo = Guid.NewGuid().ToString().ToUpper();
            }

            catch { }

            return BookingNo;
        }
        string ConnectionString { get; set; }
        public DbHelper(string ConString) => ConnectionString = ConString;
        public DataTable GetTableFromSP(string sp, Dictionary<string, object> parametersCollection)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };

                foreach (KeyValuePair<string, object> parameter in parametersCollection)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value);

                DataSet dataSet = new DataSet();
                (new SqlDataAdapter(command)).Fill(dataSet);
                command.Parameters.Clear();

                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
            finally
            {
                connection.Close();

            }
        }
        public DataTable GetTableFromSP(string sp, SqlParameter[] prms)
        {


            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();

                command.Parameters.AddRange(prms);

                DataSet dataSet = new DataSet();
                (new SqlDataAdapter(command)).Fill(dataSet);
                command.Parameters.Clear();

                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public DataTable GetTableFromSP(string sp)
        {


            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();

                DataSet dataSet = new DataSet();
                (new SqlDataAdapter(command)).Fill(dataSet);
                command.Parameters.Clear();

                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public void ExecuteNonQuery(string sp, SqlParameter[] prms)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();

                command.Parameters.AddRange(prms);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public void ExecuteNonQuery(string sp, SqlParameter prms)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();
                prms.SqlDbType = SqlDbType.Structured;
                command.Parameters.Add(prms);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public void ExecuteNonQuery(string sp, SqlParameter prm, SqlParameter[] prms)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();
                prm.SqlDbType = SqlDbType.Structured;
                command.Parameters.Add(prm);
                command.Parameters.AddRange(prms);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public void ExecuteNonQuery(string sp, SqlParameter prm, SqlParameter[] prms, SqlTransaction transaction)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection, transaction) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();
                prm.SqlDbType = SqlDbType.Structured;
                command.Parameters.Add(prm);
                command.Parameters.AddRange(prms);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public DataTable GetTableRow(string sp, SqlParameter[] prms)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                command.Parameters.AddRange(prms);
                connection.Open();

                DataSet dataSet = new DataSet();
                (new SqlDataAdapter(command)).Fill(dataSet);
                command.Parameters.Clear();

                if (dataSet.Tables.Count > 0)
                {
                    return dataSet.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public DataSet GetDataset(string name, List<SqlParameter> parameters)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                SqlCommand command = new SqlCommand(name, connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = connection.ConnectionTimeout
                };

                connection.Open();

                command.Parameters.AddRange(parameters.ToArray());

                DataSet dataSet = new DataSet();

                (new SqlDataAdapter(command)).Fill(dataSet);

                command.Parameters.Clear();

                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public int ExecuteNonQueryReturn(string sp, SqlParameter[] prms)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();
                command.Parameters.AddRange(prms);
                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string ExecuteScalarFunction(string CommandText)
        {
            string Result = "";

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                connection.Open();
                command = new SqlCommand(CommandText, connection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                da.Fill(dt);

                Result = dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return Result;

        }
        public void ExecuteMultipleDatatable(string sp, SqlParameter[] prms, DataSet ds)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command = new SqlCommand(sp, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = connection.ConnectionTimeout };
                connection.Open();
                command.Parameters.AddRange(prms);
                if (null != ds)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        SqlParameter parameter = new SqlParameter();
                        parameter.SqlDbType = SqlDbType.Structured;

                        //DataTable.TableName is the parameter Name
                        //e.g: @AppList
                        parameter.ParameterName = dt.TableName;
                        //DataTable.DisplayExpression is the equivalent SQLType Name. i.e. Name of the UserDefined Table type
                        //e.g: AppCollectionType
                        //parameter.TypeName = dt.DisplayExpression;
                        parameter.TypeName = dt.Namespace;
                        parameter.Value = dt;

                        command.Parameters.Add(parameter);
                    }
                }
                int result = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public ResultWithModel<T> Execute<T>(BaseParameterModel baseParameter)
        {

            //--1) Add the parameters: out 
            //  @ReturnCode INT OUTPUT
            //, @Msg VARCHAR(255) OUTPUT
            //, @Serverity VARCHAR(15) OUTPUT

            //-- 2) Add model parameters
            //, @RequestID     NVARCHAR(50) = NULL

            //-- 3) Add the paging parameters
            //, @TotalRecords  INT OUTPUT
            //, @Start         INT = 1
            //, @Limit         INT = 50
            //, @Sort          NVARCHAR(100) = 'RequestNo'
            //, @Dir           NVARCHAR(6)		= 'DESC'


            SqlConnection connection = new SqlConnection(ConnectionString);
            ResultWithModel<T> rwm = new ResultWithModel<T>();

            try
            {
                SqlCommand command = new SqlCommand(baseParameter.ProcedureName, connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = connection.ConnectionTimeout
                };

                connection.Open();

                ParameterBinding(command, baseParameter.Parameters);

                //forpaging
                if (baseParameter.Paging != null)
                {
                    command.Parameters.Add(new SqlParameter("@Start", baseParameter.Paging.Start));
                    command.Parameters.Add(new SqlParameter("@Limit", baseParameter.Paging.Limit));
                }

                if (baseParameter.Orders != null)
                {
                    var order = baseParameter.Orders.FirstOrDefault();

                    if (order != null)
                    {
                        command.Parameters.Add(new SqlParameter("@Sort", order.Name));
                        command.Parameters.Add(new SqlParameter("@Dir", (order.SortDirection == SortDirection.Descending) ? "DESC" : "ASC"));
                    }
                }

                SqlParameter outRefCode     = new SqlParameter("@ReturnCode", SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter outMessage     = new SqlParameter("@Msg", SqlDbType.VarChar, 255) { Direction = ParameterDirection.Output };
                SqlParameter outServerity   = new SqlParameter("@Serverity", SqlDbType.VarChar, 15) { Direction = ParameterDirection.Output };
                SqlParameter outTotalRecord = new SqlParameter("@TotalRecords", SqlDbType.Int) { Direction = ParameterDirection.Output };

                command.Parameters.Add(outRefCode);
                command.Parameters.Add(outMessage);
                command.Parameters.Add(outServerity);
                command.Parameters.Add(outTotalRecord);

                DataSet dataSet = new DataSet();

                (new SqlDataAdapter(command)).Fill(dataSet);

                if (baseParameter.ResultModelNames.Count > 0)
                {
                    foreach (DataTable t in dataSet.Tables)
                    {
                        try
                        {
                            var tableindex = dataSet.Tables.IndexOf(t);
                            if (!string.IsNullOrEmpty(baseParameter.ResultModelNames[tableindex]))
                            {
                                t.TableName = baseParameter.ResultModelNames[tableindex];
                            }
                        }
                        catch { }
                    }
                }

                rwm.RefCode = (outRefCode.Value == null) ? 0 : (int)outRefCode.Value;
                rwm.Total = (outTotalRecord.Value == null) ? 0 : (int)outTotalRecord.Value;
                rwm.Message = (outMessage.Value == null) ? string.Empty : outMessage.Value.ToString();
                rwm.Serverity = (outServerity.Value == null) ? string.Empty : outServerity.Value.ToString();

                if (rwm.RefCode.Equals(0))
                {

                    var mData = JsonConvert.SerializeObject(dataSet);
                    rwm.Data = JsonConvert.DeserializeObject<T>(mData);
                
                    rwm.Success = true;


                    if (string.IsNullOrEmpty(rwm.Message))
                    {
                        rwm.Message = "Successfully";
                    }
                }

                command.Parameters.Clear();
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                connection.Close();
            }

            return rwm;
        }
        public ResultWithModel<T> ExecuteNonQuery<T>(BaseParameterModel baseParameter, SqlConnection connection, SqlTransaction sqlTransaction)
        {
            ResultWithModel<T> rwm = new ResultWithModel<T>();
            SqlCommand command = new SqlCommand(baseParameter.ProcedureName, connection, sqlTransaction)
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = connection.ConnectionTimeout
            };

            try
            {


                //sending parameter
                //baseParameter.Parameters.ForEach(key => {

                //    if (key.DataType != null)
                //    {
                //        var para = new SqlParameter("@" + key.Name, key.DataType.Value, key.Size.GetValueOrDefault(0));
                //        para.Value = key.Value;
                //        command.Parameters.Add(para);
                //    }
                //    else
                //    {
                //        command.Parameters.Add(new SqlParameter("@" + key.Name, key.Value));
                //    }

                //});

                ParameterBinding(command, baseParameter.Parameters);

                //sending out parameter
                SqlParameter outRefCode = new SqlParameter("@ReturnCode", SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter outMessage = new SqlParameter("@Msg", SqlDbType.VarChar, 255) { Direction = ParameterDirection.Output };
                SqlParameter outServerity = new SqlParameter("@Serverity", SqlDbType.VarChar, 15) { Direction = ParameterDirection.Output };

                command.Parameters.Add(outRefCode);
                command.Parameters.Add(outMessage);
                command.Parameters.Add(outServerity);

                rwm.Total = command.ExecuteNonQuery();
                rwm.Data = default(T);

                //retrieve out parameter
                rwm.RefCode = (outRefCode.Value == null) ? 0 : (int)outRefCode.Value;
                rwm.Message = (outMessage.Value == null) ? string.Empty : outMessage.Value.ToString();
                rwm.Serverity = (outServerity.Value == null) ? string.Empty : outServerity.Value.ToString();

                if (rwm.RefCode.Equals(0))
                {
                    rwm.Success = true;

                    if (string.IsNullOrEmpty(rwm.Message))
                    {
                        rwm.Message = "Successfully";
                    }
                }

            }
            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                command.Parameters.Clear();
            }

            return rwm;
        }
        public ResultWithModel<T> ExecuteNonQuery<T>(List<BaseParameterModel> parameters)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            ResultWithModel<T> rwm = new ResultWithModel<T>();
            try
            {
                connection.Open();

                SqlTransaction transaction = connection.BeginTransaction();
                List<ResultWithModel<T>> rwms = new List<ResultWithModel<T>>();

                parameters.ForEach(procedure =>
                {
                    rwms.Add(ExecuteNonQuery<T>(procedure, connection, transaction));
                });

                if (rwms.Where(o => o.Success == false).Any())
                {
                    string message = "";
                    rwm.Message = "Transaction was rollback. Because: ";
                    var errors = rwms.Where(o => o.Success == false).ToList();

                    errors.ForEach(error =>
                    {
                        message += ((string.IsNullOrEmpty(message)) ? "" : ", ") + error.Message;
                    });

                    rwm.Message += message;

                    transaction.Rollback();
                }

                else
                {
                    rwm.Message = "Commands completed successfully";
                    rwm.Success = true;
                    transaction.Commit();
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                connection.Close();
            }

            return rwm;
        }
        public ResultWithModel Execute(BaseParameterModel baseParameter)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                SqlCommand command = new SqlCommand(baseParameter.ProcedureName, connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = connection.ConnectionTimeout
                };

                connection.Open();

                baseParameter.Parameters.ForEach(key => {

                    if (key.DataType != null)
                    {
                        var para = new SqlParameter("@" + key.Name, key.DataType.Value, key.Size.GetValueOrDefault(0));
                        para.Value = key.Value;
                        command.Parameters.Add(para);
                    }
                    else
                    {
                        command.Parameters.Add(new SqlParameter("@" + key.Name, key.Value));
                    }
                });


                //forpaging
                if (baseParameter.Paging != null)
                {
                    command.Parameters.Add(new SqlParameter("@Start", baseParameter.Paging.Start));
                    command.Parameters.Add(new SqlParameter("@Limit", baseParameter.Paging.Limit));
                }

                if (baseParameter.Orders != null)
                {
                    var order = baseParameter.Orders.FirstOrDefault();

                    if (order != null)
                    {
                        command.Parameters.Add(new SqlParameter("@Sort", order.Name));
                        command.Parameters.Add(new SqlParameter("@Dir", (order.SortDirection == SortDirection.Descending) ? "DESC" : "ASC"));
                    }
                }

                SqlParameter outRefCode = new SqlParameter("@ReturnCode", SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter outMessage = new SqlParameter("@Msg", SqlDbType.VarChar, 255) { Direction = ParameterDirection.Output };
                SqlParameter outServerity = new SqlParameter("@Serverity", SqlDbType.VarChar, 15) { Direction = ParameterDirection.Output };
                SqlParameter outTotalRecord = new SqlParameter("@TotalRecords", SqlDbType.Int) { Direction = ParameterDirection.Output };

                command.Parameters.Add(outRefCode);
                command.Parameters.Add(outMessage);
                command.Parameters.Add(outServerity);
                command.Parameters.Add(outTotalRecord);

                DataSet dataSet = new DataSet();

                (new SqlDataAdapter(command)).Fill(dataSet);

                if (baseParameter.ResultModelNames.Count > 0)
                {
                    foreach (DataTable t in dataSet.Tables)
                    {
                        try
                        {
                            var tableindex = dataSet.Tables.IndexOf(t);
                            if (!string.IsNullOrEmpty(baseParameter.ResultModelNames[tableindex]))
                            {
                                t.TableName = baseParameter.ResultModelNames[tableindex];
                            }
                        }
                        catch { }
                    }
                }

                rwm.RefCode = (outRefCode.Value == null) ? 0 : (int)outRefCode.Value;
                rwm.Total = (outTotalRecord.Value == null) ? 0 : (int)outTotalRecord.Value;
                rwm.Message = (outMessage.Value == null) ? string.Empty : outMessage.Value.ToString();
                rwm.Serverity = (outServerity.Value == null) ? string.Empty : outServerity.Value.ToString();

                if (rwm.RefCode.Equals(0))
                {
                    rwm.Success = true;
                    rwm.Data = dataSet;

                    if (string.IsNullOrEmpty(rwm.Message))
                    {
                        rwm.Message = "Successfully";
                    }
                }

                command.Parameters.Clear();
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                connection.Close();
            }

            return rwm;
        }
        public ResultWithModel ExecuteNonQuery(BaseParameterModel baseParameter, SqlConnection connection, SqlTransaction sqlTransaction)
        {
            ResultWithModel rwm = new ResultWithModel();
            SqlCommand command = new SqlCommand(baseParameter.ProcedureName, connection, sqlTransaction)
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = connection.ConnectionTimeout
            };

            try
            {


                //sending parameter
                //baseParameter.Parameters.ForEach(key => {

                //    if (key.DataType != null)
                //    {
                //        var para = new SqlParameter("@" + key.Name, key.DataType.Value, key.Size.GetValueOrDefault(0));
                //        para.Value = key.Value;
                //        command.Parameters.Add(para);
                //    }
                //    else
                //    {
                //        command.Parameters.Add(new SqlParameter("@" + key.Name, key.Value));
                //    }

                //});

                ParameterBinding(command, baseParameter.Parameters);

                //sending out parameter
                SqlParameter outRefCode = new SqlParameter("@ReturnCode", SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter outMessage = new SqlParameter("@Msg", SqlDbType.VarChar, 255) { Direction = ParameterDirection.Output };
                SqlParameter outServerity = new SqlParameter("@Serverity", SqlDbType.VarChar, 15) { Direction = ParameterDirection.Output };

                command.Parameters.Add(outRefCode);
                command.Parameters.Add(outMessage);
                command.Parameters.Add(outServerity);

                rwm.Data = command.ExecuteNonQuery();

                //retrieve out parameter
                rwm.RefCode = (outRefCode.Value == null) ? 0 : (int)outRefCode.Value;
                rwm.Message = (outMessage.Value == null) ? string.Empty : outMessage.Value.ToString();
                rwm.Serverity = (outServerity.Value == null) ? string.Empty : outServerity.Value.ToString();

                if (rwm.RefCode.Equals(0))
                {
                    rwm.Success = true;

                    if (string.IsNullOrEmpty(rwm.Message))
                    {
                        rwm.Message = "Successfully";
                    }
                }

            }
            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                command.Parameters.Clear();
            }

            return rwm;
        }
        public ResultWithModel ExecuteNonQuery(List<BaseParameterModel> parameters)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            ResultWithModel rwm = new ResultWithModel();
            try
            {
                connection.Open();

                SqlTransaction transaction = connection.BeginTransaction();
                List<ResultWithModel> rwms = new List<ResultWithModel>();

                parameters.ForEach(procedure =>
                {
                    rwms.Add(ExecuteNonQuery(procedure, connection, transaction));
                });

                if (rwms.Where(o => o.Success == false).Any())
                {
                    string message = "";
                    rwm.Message = "Transaction was rollback. Because: ";
                    var errors = rwms.Where(o => o.Success == false).ToList();

                    errors.ForEach(error =>
                    {
                        message += ((string.IsNullOrEmpty(message)) ? "" : ", ") + error.Message;
                    });

                    rwm.Message += message;

                    transaction.Rollback();
                }

                else
                {
                    rwm.Success = true;
                    rwm.Message = "Commands completed successfully";
                    transaction.Commit();
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Message = ex.Message;

                if (ex.InnerException != null)
                {
                    rwm.Message += " ,InnerException:" + ex.InnerException.Message;
                }
            }
            finally
            {
                connection.Close();
            }

            return rwm;
        }
        public static string ToProcedureName(string str)
        {
            str = str.Replace("Model", "");
            str = str.Replace("Request", "");
            str = str.Replace("Response", "");

            return str.SplitCamelCase().Replace(" ", "_").ToLower();

        }
        public void ParameterBinding(SqlCommand command, List<FieldModel> Fields)
        {
            AppSettingsReader settingsReader = new AppSettingsReader();

            //string DefaultDateFormat = "{0:dd/MM/yyyy}";
            //string DefaultDateTimeFormat = "{0:dd/MM/yyyy HH:mm:ss}";

            Fields.ForEach(key => {

                if (key.DataType != null)
                {
                    var para = new SqlParameter("@" + key.Name.Trim(), key.DataType.Value, key.Size.GetValueOrDefault(0));
                    para.Value = key.Value;
                    command.Parameters.Add(para);
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@" + key.Name.Trim(), key.Value));
                }

            });
        }

        public static object NullHandler(object instance)
        {
            if (instance != null)
                return instance;

            return DBNull.Value;
        }

        public static object NullHandler(string instance)
        {
            if (string.IsNullOrEmpty(instance))
                return DBNull.Value;

            return instance;
        }
    }
}
