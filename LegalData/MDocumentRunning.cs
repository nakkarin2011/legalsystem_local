//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LegalData
{
    using System;
    using System.Collections.Generic;
    
    public partial class MDocumentRunning
    {
        public System.Guid uDocumentRunningID { get; set; }
        public Nullable<int> nAttorneyID { get; set; }
        public Nullable<int> nYear { get; set; }
        public Nullable<int> nRunningNo { get; set; }
        public string sCreateBy { get; set; }
        public Nullable<System.DateTime> dCreateDate { get; set; }
        public string sUpdateBy { get; set; }
        public Nullable<System.DateTime> dUpdateDate { get; set; }
    }
}
