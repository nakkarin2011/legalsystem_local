﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLDropDownList: ModelSet<DDLItemModel, DDLItemResponse>
    {
        public BLLDropDownList(DatabaseContext dbContext) 
            : base(dbContext)
        {
        }

        public List<DDLItemModel> DDLLegalStatus()
        {
            var Fields = new List<FieldModel>();

            BaseParameterModel Parameter = new BaseParameterModel();
            Parameter.ProcedureName = "sp_ddl_leagal_status";
            Parameter.Parameters.AddRange(Fields);
            Parameter.ResultModelNames.Add("DDLItems");

            List<DDLItemModel> items = new List<DDLItemModel>();

            var rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<DDLItemResponse>(Parameter);

            if (rwm.Success)
            {
                items = rwm.Data.DDLItems;
            }

            return items;
        }

        public List<DDLItemModel> DDLTrayDocuments(int TrayID, int RequestTypeID)
        {
            var Fields = new List<FieldModel>();

            Fields.Add(new FieldModel { Name = "TrayID", Value = TrayID });
            Fields.Add(new FieldModel { Name = "RequestTypeID", Value = RequestTypeID });

            BaseParameterModel Parameter = new BaseParameterModel();
            Parameter.ProcedureName = "sp_ddl_tray_documents";
            Parameter.Parameters.AddRange(Fields);
            Parameter.ResultModelNames.Add("DDLItems");

            List<DDLItemModel> items = new List<DDLItemModel>();

            var rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<DDLItemResponse>(Parameter);

            if (rwm.Success)
            {
                items = rwm.Data.DDLItems;
            }

            return items;
        }

    }
}
