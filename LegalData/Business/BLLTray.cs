﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLTray: ModelSet<TrayRequest, TrayResponse>
    {
       BLLDocument Document { get; set; }

        public BLLTray(DatabaseContext dbContext) 
            : base(dbContext)
        {
            Document = new BLLDocument(dbContext);
        }
        public ResultWithModel<TrayResponse> Search(BaseParameterModel Parameter)
        {

            Parameter.ProcedureName = "sp_tray_search";

            var props = typeof(TrayResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<TrayResponse> rwm = new ResultWithModel<TrayResponse>();

            var con = dbContext.connectionName;

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<TrayResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new TrayResponse();
            }

            return rwm;
        }
        public ResultWithModel<PermissionControlResponse> GetButtonPermission(BaseParameterModel Parameter)
        {
            Parameter.ProcedureName = "sp_tray_permission";

            var props = typeof(PermissionControlResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<PermissionControlResponse> rwm = new ResultWithModel<PermissionControlResponse>();

            var con = dbContext.connectionName;

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<PermissionControlResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new PermissionControlResponse();
            }

            return rwm;
        }
        public ResultWithModel PrintDocument(PrintDocumentRequest document)
        {
            ResultWithModel rwm = new ResultWithModel();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();

            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = "sp_tray_printdocument",
                Parameters = new List<FieldModel> {
                     new FieldModel { Name ="PrintDocumentID", Value = document.PrintDocumentID } 
                   , new FieldModel { Name ="RequestID", Value = document.RequestID } 
                   , new FieldModel { Name ="DocumentTypeID", Value = document.DocumentTypeID } 
                   , new FieldModel { Name ="DocumentStatusID", Value = document.DocumentStatusID } 
                   , new FieldModel { Name ="CreateBy", Value = document.CreateBy } 
                   , new FieldModel { Name ="CreateDate", Value = document.CreateDate } 
                  }
            });

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery(Procedures);

            return rwm;
        }
        public ResultWithModel<GetTrayActionResponse> GetTrayAction(GetTrayActionRequest req)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_tray_action_info",
                Parameters = new List<FieldModel> {
                     new FieldModel { Name ="RequestType", Value = req.RequestType }
                   , new FieldModel { Name ="TrayID", Value = req.TrayID }

                  }
            };

            var props = typeof(GetTrayActionResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<GetTrayActionResponse> rwm = new ResultWithModel<GetTrayActionResponse>();
 
            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<GetTrayActionResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new GetTrayActionResponse();
            }

            return rwm;
        }
        public ResultWithModel<GetDocumentWithTrayActionResponse> GetDocumentWithTrayAction(GetDocumentWithTrayActionRequest req)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_tray_document_info",
                Parameters = new List<FieldModel> {
                      new FieldModel { Name ="TrayActionID", Value = req. TrayActionID }

                  }
            };

            var props = typeof(GetDocumentWithTrayActionResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<GetDocumentWithTrayActionResponse> rwm = new ResultWithModel<GetDocumentWithTrayActionResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<GetDocumentWithTrayActionResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new GetDocumentWithTrayActionResponse();
            }

            return rwm;
        }
        public ResultWithModel<List<GetTrayActionDocumentResponse>> GetTrayActionDocument(int requestType, int trayID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_trayaction_document_info",
                Parameters = new List<FieldModel> {
                     new FieldModel { Name ="RequestType", Value = requestType }
                   , new FieldModel { Name ="TrayID", Value = trayID}

                  }
            };

            var props = typeof(GetTrayActionDocumentMappingResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<List<GetTrayActionDocumentResponse>> rwm = new ResultWithModel<List<GetTrayActionDocumentResponse>>();

           var  result  = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<GetTrayActionDocumentMappingResponse>(baseParameter: Parameter);


            rwm.Success   = result.Success;
            rwm.Message   = result.Message;
            rwm.Serverity = result.Serverity;
            rwm.Total     = result.Total;
            rwm.RefCode   = result.RefCode;


            if (result.Success)
            {
               rwm.Data = result.Data
                .Documents
                .GroupBy(g => new   {
                    g.TrayActionID
                    , g.TrayActionCode
                    , g.TrayActionName
           
                })

                .Select(s => new GetTrayActionDocumentResponse {

                    TrayActionID = s.Key.TrayActionID
                    , TrayActionCode = s.Key.TrayActionCode
                    , TrayActionName = s.Key.TrayActionName
                    , DocumentID  = s.Select(p=> p.DocumentID).FirstOrDefault()
                    , Documents  =  s.Select( p => new DocumentModel {
                           DocumentID   = p.DocumentID 
                        ,  DocumentCode = p.DocumentCode 
                        ,  DocumentName =  p.DocumentName
                    }).ToList()

                })

                .ToList();


             }
            
            return rwm;
        }
    }
}