﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public  class BLLCoverPage : ModelSet<CoverPageRequest, CoverPageResponse>
    {
        public BLLCoverPage(DatabaseContext dbContext)
        : base(dbContext)
        {
        }

        public ResultWithModel<CoverPageResponse> GetCoverPageMaster(string documentID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_tray_coverpage_master",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="DocumentID", Value = documentID }
                }
            };

            var props = typeof(CoverPageResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<CoverPageResponse> rwm = new ResultWithModel<CoverPageResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<CoverPageResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new CoverPageResponse();
            }

            return rwm;
        }
        public ResultWithModel<CoverPageResponse> GetCoverPageInfo(CoverPageRequest request)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_tray_coverpage_info",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="RequestID", Value = request.RequestID }
                    ,   new FieldModel { Name ="DocumentID", Value = request.DocumentID }
                }
            };

            var props = typeof(CoverPageResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<CoverPageResponse> rwm = new ResultWithModel<CoverPageResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<CoverPageResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new CoverPageResponse();
            }

            return rwm;
        }
        public ResultWithModel Save(CoverPageModel CoverPage)
        {
            ResultWithModel rwm = new ResultWithModel();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();

            //Save Cover Page
            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = "sp_tray_coverpage_save",
                Parameters = new List<FieldModel> {
                      new FieldModel { Name ="CoverPageID", Value = CoverPage.CoverPageID	}
                    , new FieldModel { Name ="DocumentID", Value = CoverPage.DocumentID	    }
                    , new FieldModel { Name ="RequestID", Value = CoverPage.RequestID	    }
                    , new FieldModel { Name ="LegalNo", Value = CoverPage.LegalNo        }
                    , new FieldModel { Name ="IsActive", Value = CoverPage.IsActive		}
                    , new FieldModel { Name ="CreateBy", Value = CoverPage.CreateBy		}
                }
            });

            //Save Cover Page Detail
            CoverPage.CoverPageDetails.ForEach(detail => {
                Procedures.Add(new BaseParameterModel()
                {
                    ProcedureName = "sp_tray_coverpage_detail_save",
                    Parameters = new List<FieldModel> {
                          new FieldModel { Name ="CoverPageDetailID", Value = detail.CoverPageDetailID}
                        , new FieldModel { Name ="CoverPageID", Value = detail.CoverPageID}
                        , new FieldModel { Name ="CoverPageDocumentID", Value = detail.CoverPageDocumentID}
                        , new FieldModel { Name ="CoverPagePropertiesID", Value = detail.CoverPagePropertiesID}
                        , new FieldModel { Name ="Value", Value = detail.Value}
                              }

                });
            });

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery(Procedures);

            return rwm;
        }

    }
}
