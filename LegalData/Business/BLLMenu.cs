﻿using LegalData.Model;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;

 
namespace LegalData.Business
{
    public class BLLMenu: ModelSet<MenuModel, MenuResponse>
    {
        public BLLMenu(DatabaseContext dbContext)
           : base(dbContext)
        {

        }
        public ResultWithModel<MenuResponse> GetMenus(string RoleName)
        {
            ResultWithModel<MenuResponse> rwm = new ResultWithModel<MenuResponse>(); 

            try
            {
                MenuResponse response = new MenuResponse();

                switch (RoleName)
                {  case "CollectionMaker":
                        response.Menus.Add(CreateRequestMenu());
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        break;

                    case "CollectionChecker":
                        response.Menus.Add(CreateRequestMenu());
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(WorkListMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        break;

                    case "CollectionHead":
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        break;

                    case "LegalSpecialist":
                      
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        response.Menus.Add(PoolInboxMenu());
                        //response.Menus.Add(ReportMenu());

                        break;
                    case "SeniorLawyer":
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(PoolInboxMenu());
                        response.Menus.Add(AssignLawyerMenu());
                        response.Menus.Add(RequestOperMenu(RoleName));
                        response.Menus.Add(TrayMenu());
                       // response.Menus.Add(ReportMenu());
                        response.Menus.Add(SettingMenu());
                    
                        break;
                    case "Lawyer":
                
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        response.Menus.Add(RequestOperMenu(RoleName));
                        response.Menus.Add(TrayMenu());
                        //response.Menus.Add(ReportMenu());
                        response.Menus.Add(SettingMenu());
            
                        break;
                    default:
                        response.Menus.Add(CreateRequestMenu());
                        response.Menus.Add(InboxMenu());
                        response.Menus.Add(WorkListMenu());
                        response.Menus.Add(InquiryRequestMenu());
                        response.Menus.Add(PoolInboxMenu());
                        response.Menus.Add(AssignLawyerMenu());
                        response.Menus.Add(RequestOperMenu(RoleName));
                        response.Menus.Add(TrayMenu());
                       // response.Menus.Add(ReportMenu());
                        response.Menus.Add(SettingMenu());
                        break;

                }

                //Default All Role
                response.Menus.Add(LogoutMenu());

                rwm.Success   = true;
                rwm.RefCode   = 200;
                rwm.Serverity = "low"; 
                rwm.Message   = "Successful";
                rwm.Data      = response;
            }

            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   = 500;
                rwm.Serverity = "high";
            }

            return rwm;
        }


        //PRIVATE METHODE
        MenuModel CreateRequestMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Create Request"
                 , IconClass    = "fa fa-plus"
                 , Url          = "/LegalSystem_DEV/Pages/RequestPage.aspx"
            };
         
            return menu;
        }
        MenuModel InboxMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Inbox"
                 , IconClass    = "fa fa-inbox"
                 , Url          = "/LegalSystem_DEV/Pages/InboxPage.aspx"
            };
         
            return menu;
        }
        MenuModel RequestInboxMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Request Inbox"
                 , IconClass    = "fa fa-bell"
                 , Url          = "/LegalSystem_DEV/Pages/InboxPage.aspx?CaseEdit=Y"
            };
         
            return menu;
        }
        MenuModel WorkListMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Work List"
                 , IconClass    = "fa fa-briefcase"
                 , Url          = "/LegalSystem_DEV/Pages/WorkListPage.aspx"
            };

            return menu;
        }
        MenuModel InquiryRequestMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Inquiry Request"
                 , IconClass    = "fa fa-building-o"
                 , Url          = "/LegalSystem_DEV/Pages/InquiryPage.aspx"
            };
         
            return menu;
        }
        MenuModel PoolInboxMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Pool Inbox"
                 , IconClass    = "fa fa-inbox"
                 , Url          = "/LegalSystem_DEV/Pages/InboxPoolPage.aspx"
            };
         
            return menu;
        }
        MenuModel AssignLawyerMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Assign Lawyer"
                 , IconClass    = "fa fa-briefcase"
                 , Url          = "/LegalSystem_DEV/Pages/AssignPage.aspx"
            };
         
            return menu;
        }
        MenuModel RequestOperMenu(string RoleName)
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Request Oper"
                 , IconClass    = "fa fa-mail-forward"
            };

            if (RoleName.Equals("SeniorLawyer"))
            {
                menu.SubMenus = new List<MenuModel>()
                {
                    new MenuModel { MenuName = "Request Doc to Outsource"  , Url ="/LegalSystem_DEV/Pages/RequestOperPage.aspx?reqMode=ReqOutsource" }
                };
            }

            else
            {
                menu.SubMenus = new List<MenuModel>()
                {
                       new MenuModel { MenuName = "Request OS to Oper"  , Url ="/LegalSystem_DEV/Pages/RequestOperPage.aspx?reqMode=OSOper" }
                      , new MenuModel { MenuName = "Request Doc to Outsource"  , Url ="/LegalSystem_DEV/Pages/RequestOperPage.aspx?reqMode=ReqOutsource" }
                };
            }


            return menu;
        }
        MenuModel TrayMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Tray"
                 , IconClass    = "fa fa-file"
                 , SubMenus = new List<MenuModel> {
                      new MenuModel { ShortName="T1",  MenuName = "รับเรื่องดำเนินคดี" , Url ="/LegalSystem_DEV/Tray/Tray1"}
                    , new MenuModel { ShortName="T2",  MenuName = "ออกหนังสือทวงถาม" , Url ="/LegalSystem_DEV/Tray/Tray2"}
                    , new MenuModel { ShortName="T3",  MenuName = "อยุ่ระหว่างพิจารณาคดี" , Url ="/LegalSystem_DEV/Tray/Tray3"}
                    , new MenuModel { ShortName="T4",  MenuName = "บังคับคดี" , Url ="/LegalSystem_DEV/Tray/Tray4"}
                    , new MenuModel { ShortName="T5",  MenuName = "สืบทรัพย์/ยึดทรัพย์" , Url ="/LegalSystem_DEV/Tray/Tray5"}
                 }
            };
         
            return menu;
        }
        MenuModel ReportMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuName     = "Report"
                 , IconClass    = "fa fa-file"
                 , SubMenus = new List<MenuModel> {
                          new MenuModel { ShortName="R1",  MenuName = "Report1" , Url ="#Report1"}
                        , new MenuModel { ShortName="R2",  MenuName = "Report2" , Url ="#Report2"}
                        , new MenuModel { ShortName="R3",  MenuName = "Report3" , Url ="#Report3"}
                 }
            };
         
            return menu;
        }
        MenuModel SettingMenu()
        {
            MenuModel menu = new MenuModel(){
                   MenuID       = new Guid ("cabd50a5-394e-4db5-b0f9-30e3bb20889c")
                 , MenuName     = "Setting"
                 , IconClass    = "fa fa-cogs"
                 , SubMenus     = new List<MenuModel> {
                      new MenuModel { ShortName="RT",MenuName ="Request Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/RequestTypePage.aspx"}
                    , new MenuModel { ShortName="DT",MenuName ="Document Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/DocumentTypePage.aspx"}
                    , new MenuModel { ShortName="DM",MenuName ="Document Type Mapping" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/DocumentTypeMappingPage.aspx"}
                    , new MenuModel { ShortName="CZ",MenuName ="Court Zone" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/CourtZonePage.aspx"}
                    , new MenuModel { ShortName="PD",MenuName ="Product Detail" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/ProductDetailPage.aspx"}
                    , new MenuModel { ShortName="PF",MenuName ="Product เหมาจ่าย" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/ProductFixPaymentPage.aspx"}
                    , new MenuModel { ShortName="OA",MenuName ="OA/IA" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/LawyerOAPage.aspx"}
                    , new MenuModel { ShortName="MT",MenuName ="Mail Template" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Master/MailConfig.aspx"}
                    , new MenuModel { ShortName="AN",MenuName ="Attorney Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=AN"}
                    , new MenuModel { ShortName="AT",MenuName ="Asset Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=AT"}
                    , new MenuModel { ShortName="CJ",MenuName ="Court Judgement" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=CJ"}
                    , new MenuModel { ShortName="CF",MenuName ="Court Fee" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/CourtFee.aspx"}
                    , new MenuModel { ShortName="CO",MenuName ="Court Rate" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/CourtRate.aspx"}
                    , new MenuModel { ShortName="CP",MenuName ="Cover Page Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=CP"}
                    , new MenuModel { ShortName="CS",MenuName ="Cost Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=CT"}
                    , new MenuModel { ShortName="CT",MenuName ="Court Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=CT"}
                    , new MenuModel { ShortName="CR",MenuName ="Capital Rate" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/CapitalRate.aspx"}
                    , new MenuModel { ShortName="CD",MenuName ="Mapping Cover Page & Document" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=CD"}
                    , new MenuModel { ShortName="ES",MenuName ="Setting Email And Signature" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/SettingEmailAndSignature.aspx"}
                    , new MenuModel { ShortName="FE",MenuName ="Fee Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=FE"}
                    , new MenuModel { ShortName="LM",MenuName ="Legal Status Mapping" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=LM"}
                    , new MenuModel { ShortName="LT",MenuName ="Lawyer Costs Tier" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/LawyerCostsTier.aspx"}
                    , new MenuModel { ShortName="LP",MenuName ="Lawyer Costs Peroid" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/LawyerCostsPeroid.aspx"}
                    , new MenuModel { ShortName="MR",MenuName ="Mapping Reason Request" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=LS"}
                    , new MenuModel { ShortName="PD",MenuName ="Product Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=PD"}
                    , new MenuModel { ShortName="PL",MenuName ="Plaintiff" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/Plaintiff.aspx"}
                    , new MenuModel { ShortName="PP",MenuName ="Product Payment" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/ProductPayment.aspx"}
                    , new MenuModel { ShortName="PT",MenuName ="Payment Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=PT"}
                    , new MenuModel { ShortName="RD",MenuName ="Mapping Request & Document" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=RD"}
                    , new MenuModel { ShortName="RL",MenuName ="Relation" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/Setting/Relation.aspx"}
                    , new MenuModel { ShortName="RM",MenuName ="Mapping Request Reason & Request Type" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingMapping.aspx?type=LM"}
                    , new MenuModel { ShortName="RR",MenuName ="Reason Request" , Url ="http://localhost:58279/LegalSystem_DEV/Pages/SettingType.aspx?type=RR"}
                 }
            };
         
            return menu;
        }
        MenuModel LogoutMenu()
        {
            MenuModel menu = new MenuModel()
            {
                MenuName = "Logout"
                 ,   IconClass = "fa fa-lock"
                ,    Url = "/LegalSystem_DEV/Login.aspx"
            };

            return menu;
        }
    }
}