﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLFirstLetter
        : ModelSet<FirstLetterRequest, FirstLetterResponse>
    {
        BLLDocument Document { get; set; }
        public BLLFirstLetter(DatabaseContext dbContext)
            : base(dbContext)
        {
            Document = new BLLDocument(dbContext);
        }
        public ResultWithModel<FirstLetterResponse> Find(Guid firstLetterID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_firstletter_info",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="FirstLetterID", Value =  firstLetterID }
                }
            };

            var props = typeof(FirstLetterResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<FirstLetterResponse> rwm = new ResultWithModel<FirstLetterResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<FirstLetterResponse>(baseParameter: Parameter);

            if (rwm.Success)
            {
                if (!rwm.Data.firstLetterInformations.Any())
                {
                    rwm.Success = false;
                    rwm.Message = "Data not found.";
                    rwm.RefCode = 404;
                }
            } 

            else
            {
                rwm.Data = new FirstLetterResponse();
            }

            return rwm;
        }
        public ResultWithModel<FirstLetterResponse> FindByRequest(Guid requestID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_firstletter_info",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="RequestID", Value =  requestID }
                }
            };

            var props = typeof(FirstLetterResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<FirstLetterResponse> rwm = new ResultWithModel<FirstLetterResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<FirstLetterResponse>(baseParameter: Parameter);

            if (rwm.Success)
            {
                if (!rwm.Data.firstLetterInformations.Any())
                {
                    rwm.Success = false;
                    rwm.Message = "Data not found.";
                    rwm.RefCode = 404;
                }
            }

            else
            {
                rwm.Data = new FirstLetterResponse();
            }

            return rwm;
        }
        public ResultWithModel<List<FirstLetterReportModel>> Initial(string RequestID, string userID)
        {
            ResultWithModel<List<FirstLetterReportModel>> rwm = new ResultWithModel<List<FirstLetterReportModel>>();

            try
            {
                rwm.Data = new List<FirstLetterReportModel>(); 

                var InitialResponse  =  GetInitialFirstLetter(RequestID: RequestID);

                rwm.Success   = InitialResponse.Success;
                rwm.Message   = InitialResponse.Message;
                rwm.RefCode   = InitialResponse.RefCode;
                rwm.Serverity = InitialResponse.Serverity;
        
                if (InitialResponse.Success)
                {
                    var Responses = InitialResponse.Data.FirstLtterInitialModels;

                    var Borrower = Responses
                        .Where(p=> p.RelationCode.Equals("P"))
                                    .GroupBy(p => new { p.CIFNo, p.RelationCode, p.RelationName, p.CustomerName, p.LawyerAttorneyID, p.SeniorLawyerID, p.SeniorLawyerName })
                                    .Select(g => new FirstLetterReportModel {  
                                           CIFNo                = g.Key.CIFNo
                                        ,  CustomerName         = g.Key.CustomerName
                                        ,  BorrowerName         = g.Key.CustomerName
                                        ,  LawyerAttorneyID     = g.Key.LawyerAttorneyID
                                        ,  SeniorLawyerID       = g.Key.SeniorLawyerID
                                        ,  SeniorLawyerName     = g.Key.SeniorLawyerName
                                        ,  RelationCode         = g.Key.RelationCode
                                        ,  RelationName         = g.Key.RelationName
                                        ,  LoanDateText         = g.OrderBy(o => o.LoanDate)
                                                                   .Select(x => x.LoanDate.Value.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH")))
                                                                   .GroupBy(xdate => xdate)
                                                                   .Select(o=> o.Key)
                                                                   .Aggregate((s1, s2) => s1 + ", " + s2)
                                        ,  LoanAmount           = g.Sum(o => o.LoanAmount.GetValueOrDefault())
                                        ,  CurrentBalanceAmount = g.Sum(o => o.CurrentBalanceAmount.GetValueOrDefault())
                                        ,  InterestAmount       = g.Sum(o => o.InterestAmount.GetValueOrDefault())
                                        ,  OtherInterestAmount  = g.Sum(o => o.OtherInterestAmount.GetValueOrDefault())
                                    })
                                    .FirstOrDefault();


                    var Owners      = Responses
                                    .Where(p => p.RelationCode.Equals("OWNER"))
                                    .GroupBy(p => new { p.CIFNo, p.RelationCode, p.RelationName })
                                    .Select(g => new FirstLetterReportModel  {
                                          CIFNo = g.Key.CIFNo
                                        ,  RelationCode = g.Key.RelationCode
                                        ,  RelationName = g.Key.RelationName
                                    })

                                    .ToList();

                    var CoBorrowers = Responses
                                    .Where(p => !p.RelationCode.Equals("P") && !p.RelationCode.Equals("OWNER"))
                                    .GroupBy(p => new { p.CIFNo, p.RelationCode, p.RelationName})
                                    .Select(g=> new FirstLetterReportModel  {
                                          CIFNo        = g.Key.CIFNo
                                        , RelationCode = g.Key.RelationCode
                                        , RelationName = g.Key.RelationName
                                    })

                                    .ToList();

                    if (Borrower!= null)
                    {
                 
                        //var GenerateLegalNoResult         = Document.GenerateLegalNo(Borrower.LawyerAttorneyID, userID);

                        //if (GenerateLegalNoResult.Success)
                        //{
                        //    Borrower.FirstLetterNo = GenerateLegalNoResult.Data;
                        //}

                        //else
                        //{
                        //    rwm.Success  = GenerateLegalNoResult.Success;
                        //    rwm.RefCode  = GenerateLegalNoResult.RefCode;
                        //    rwm.Message  = $"Faild to generate legal no. ({GenerateLegalNoResult.Message})" ;

                        //    return rwm;
                        //}

                        Borrower.PrintLetterDate          = DateTime.Now;
                        Borrower.PrintLetterDateText      = Borrower.PrintLetterDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH"));
                   
                        Borrower.LoanAmountText           = ThaiBahtHelper.ThaiBahtText(Borrower.LoanAmount.GetValueOrDefault());
                        Borrower.CurrentBalanceAmountText = ThaiBahtHelper.ThaiBahtText(Borrower.CurrentBalanceAmount.GetValueOrDefault());
                        Borrower.InterestAmountText       = ThaiBahtHelper.ThaiBahtText(Borrower.InterestAmount.GetValueOrDefault());
                        Borrower.OtherInterestAmountText  = ThaiBahtHelper.ThaiBahtText(Borrower.OtherInterestAmount.GetValueOrDefault());

                        //Borrower
                        rwm.Data.Add(Borrower);

                        //Owner
                        Owners.ForEach(own => {

                            if (own.CIFNo.Equals(Borrower.CIFNo))
                            {

                                Borrower.RelationCode = $"{ Borrower.RelationCode }, {own.RelationCode}";
                                Borrower.RelationName = $"{ Borrower.RelationName } และ {own.RelationName }";

                            }
                            else
                            {
                                rwm.Data.Add(new FirstLetterReportModel {
                                      CIFNo                     = own.CIFNo
                                    , RelationCode              = own.RelationCode
                                    , RelationName              = own.RelationName
                                    , LawyerAttorneyID          = Borrower.LawyerAttorneyID
                                    , SeniorLawyerID            = Borrower.SeniorLawyerID                            
                                    , SeniorLawyerName          = Borrower.SeniorLawyerName  
                                    , BorrowerName              = Borrower.BorrowerName
                                    , LoanDateText              = Borrower.LoanDateText           
                                    , LoanAmount                = Borrower.LoanAmount                
                                    , CurrentBalanceAmount      = Borrower.CurrentBalanceAmount      
                                    , InterestAmount            = Borrower.InterestAmount            
                                    , OtherInterestAmount       = Borrower.OtherInterestAmount                
                                    , LoanAmountText            = Borrower.LoanAmountText            
                                    , CurrentBalanceAmountText  = Borrower.CurrentBalanceAmountText  
                                    , InterestAmountText        = Borrower.InterestAmountText        
                                    , OtherInterestAmountText   = Borrower.OtherInterestAmountText
                                    , PrintLetterDate           = Borrower.PrintLetterDate
                                    , PrintLetterDateText       = Borrower.PrintLetterDateText
                         
                                });
                            }
     
                        });

                         //Co-borrower
                        CoBorrowers.ForEach(c =>  {
                            rwm.Data.Add(new FirstLetterReportModel {
                                  CIFNo                     = c.CIFNo
                                , RelationCode              = c.RelationCode
                                , RelationName              = c.RelationName
                                , LawyerAttorneyID          = Borrower.LawyerAttorneyID
                                , SeniorLawyerID            = Borrower.SeniorLawyerID                            
                                , SeniorLawyerName          = Borrower.SeniorLawyerName  
                                , BorrowerName              = Borrower.BorrowerName
                                , LoanDateText              = Borrower.LoanDateText           
                                , LoanAmount                = Borrower.LoanAmount                
                                , CurrentBalanceAmount      = Borrower.CurrentBalanceAmount      
                                , InterestAmount            = Borrower.InterestAmount            
                                , OtherInterestAmount       = Borrower.OtherInterestAmount                
                                , LoanAmountText            = Borrower.LoanAmountText            
                                , CurrentBalanceAmountText  = Borrower.CurrentBalanceAmountText  
                                , InterestAmountText        = Borrower.InterestAmountText        
                                , OtherInterestAmountText   = Borrower.OtherInterestAmountText
                                , PrintLetterDate           = Borrower.PrintLetterDate
                                , PrintLetterDateText       = Borrower.PrintLetterDateText
                         
                            });
                        });

                        //Customer And Address Information
                        var Customers = rwm.Data.GroupBy(g =>  g.CIFNo ).Select(s =>  s.Key ) .ToList();

                        Customers.ForEach(CIFNO =>  {

                            var HasOwnner        = rwm.Data.Where(p => p.CIFNo.Equals(CIFNO) && p.RelationCode.Equals("OWNER")).Any();
                            var CustomerResponse = this.GetCustomerInformation(CIFNO, HasOwnner);

                            if (CustomerResponse.Success)
                            {
                                var customersResults  = CustomerResponse.Data.Customers; 
                                var rows              = rwm.Data.Where(p => p.CIFNo.Equals(CIFNO)).ToList();


                                rows.ForEach(row =>
                                {
                                        var info = customersResults.Where(p => p.CIFNo.Equals(row.CIFNo) && p.RelationCode.Equals(row.RelationCode)).FirstOrDefault();

                                        if (info == null)
                                        {
                                            info = customersResults.Where(p => p.CIFNo.Equals(row.CIFNo)).FirstOrDefault();
                                        }

                                        if (info != null)
                                        {
                                            //Update information
                                            row.CustomerName = info.CustomerName;
                                            row.Address1 = $"{info.HouseNo.Trim()} {info.VillageNo.Trim()} {info.Village.Trim()} {info.Streee.Trim()}".Replace("   ", " ").Replace("  ", " ");
                                            row.Address2 = $"{info.SubDistrict.Trim()} {info.DistrictAndProvince.Trim()}".Replace("   ", " ").Replace("  ", " ");
                                        }
                                });
                            }
                        });

 
                        //Save to Legal System database
                    }

                    //updare letter id and request id 
                    rwm.Data.ForEach(a =>
                    {
                
                        a.RequestID = new Guid(RequestID);
                       
                    });

                    rwm.Total = rwm.Data.Count;
                }

            }

            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   =  500;
                rwm.Serverity = "high";
            }

            return rwm;
        }
        public ResultWithModel Save(FirstLetterModel model)
        {
            ResultWithModel rwm = new ResultWithModel();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();

            //Save First Letter
            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = "sp_firstletter_save",
                Parameters = new List<FieldModel> {
                      new FieldModel { Name ="FirstLetterID	 ", Value = model.FirstLetterID	  }
                    , new FieldModel { Name ="RequestID		 ", Value = model.RequestID		  }
                    , new FieldModel { Name ="LegalNo		 ", Value = model.LegalNo		  }
                    , new FieldModel { Name ="CreateBy		 ", Value = model.CreateBy        }
                }
            });

            //Save First Letter Detail
            model.Details.ForEach(detail => {
                Procedures.Add(new BaseParameterModel()
                {
                    ProcedureName = "sp_firstletter_detail_save",
                    Parameters = new List<FieldModel> {
                          new FieldModel { Name ="FirstLetterDetailID	 ", Value = detail.FirstLetterDetailID	 }
                        , new FieldModel { Name ="FirstLetterID			 ", Value = detail.FirstLetterID		 }
                        , new FieldModel { Name ="CIFNo					 ", Value = detail.CIFNo				 }
                        , new FieldModel { Name ="LawyerAttorneyID		 ", Value = detail.LawyerAttorneyID		 }
                        , new FieldModel { Name ="SeniorLawyerID		 ", Value = detail.SeniorLawyerID		 }
                        , new FieldModel { Name ="SeniorLawyerName		 ", Value = detail.SeniorLawyerName		 }
                        , new FieldModel { Name ="RelationCode			 ", Value = detail.RelationCode			 }
                        , new FieldModel { Name ="RelationName			 ", Value = detail.RelationName			 }
                        , new FieldModel { Name ="LoanDateText			 ", Value = detail.LoanDateText			 }
                        , new FieldModel { Name ="BorrowerName			 ", Value = detail.BorrowerName			 }
                        , new FieldModel { Name ="CustomerName			 ", Value = detail.CustomerName			 }
                        , new FieldModel { Name ="LoanAmount			 ", Value = detail.LoanAmount			 }
                        , new FieldModel { Name ="CurrentBalanceAmount	 ", Value = detail.CurrentBalanceAmount	 }
                        , new FieldModel { Name ="InterestAmount		 ", Value = detail.InterestAmount		 }
                        , new FieldModel { Name ="OtherInterestAmount	 ", Value = detail.OtherInterestAmount	 }
                        , new FieldModel { Name ="Address1				 ", Value = detail.Address1				 }
                        , new FieldModel { Name ="Address2				 ", Value = detail.Address2				 }
                    }

                });
            });

            //if (model.Document.ExtensionID != Guid.Empty)
            //{
            //    //Save transaction control
            //    Procedures.Add(new BaseParameterModel() {
            //        ProcedureName = "sp_request_document_extention_save",
            //        Parameters = new List<FieldModel> {
            //              new FieldModel { Name ="RequestDocumentExtensionID", Value = model.Document.ExtensionID }
            //            , new FieldModel { Name ="RequestID                 ", Value = model.Document.RequestID                     }
            //            , new FieldModel { Name ="TrayActionID              ", Value = model.Document.TrayActionID                  }
            //            , new FieldModel { Name ="DocumentID                ", Value = model.Document.DocumentID                    }
            //            , new FieldModel { Name ="DocumentStatusID          ", Value = model.Document.DocumentStatusID              }
            //            , new FieldModel { Name ="DocumentStatusDate        ", Value = model.Document.DocumentStatusDate            }
            //            , new FieldModel { Name ="PrintDate                 ", Value = model.Document.PrintDate                     }
            //            , new FieldModel { Name ="CreateBy                  ", Value = model.Document.CreateBy                      }
            //        }
            //    });
            //}

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery(Procedures);

            return rwm;
        }
        public ResultWithModel<List<FirstLetterReportModel>> ReportData(Guid requestID)
        {
            ResultWithModel<List<FirstLetterReportModel>> rwm = new ResultWithModel<List<FirstLetterReportModel>>();

            try
            {
                var result      = FindByRequest(requestID); 

                rwm.Success     = result.Success;
                rwm.Message     = result.Message;
                rwm.RefCode     = result.RefCode;
                rwm.Serverity   = result.Serverity;
                rwm.Total       = result.Total;

                if (result.Success) {

                    rwm.Data = result.Data.firstLetterInformations.Select(s => new FirstLetterReportModel
                    {

                         FirstLetterID             = s.FirstLetterID
                        , CIFNo                    = s.CIFNo
                        , RelationCode             = s.RelationCode
                        , RelationName             = s.RelationName
                        , LegalNo                   = s.LegalNo
                        , LawyerAttorneyID         = s.LawyerAttorneyID
                        , SeniorLawyerID           = s.SeniorLawyerID
                        , SeniorLawyerName         = s.SeniorLawyerName
                        , BorrowerName             = s.BorrowerName
                        , LoanDateText             = s.LoanDateText
                        , LoanAmount               = s.LoanAmount
                        , CurrentBalanceAmount     = s.CurrentBalanceAmount
                        , InterestAmount           = s.InterestAmount
                        , OtherInterestAmount      = s.OtherInterestAmount
                        , CustomerName             = s.CustomerName
                        , Address1                 = s.Address1
                        , Address2                 = s.Address2
                        , LoanAmountText           = ThaiBahtHelper.ThaiBahtText(s.LoanAmount)
                        , CurrentBalanceAmountText = ThaiBahtHelper.ThaiBahtText(s.CurrentBalanceAmount)
                        , InterestAmountText       = ThaiBahtHelper.ThaiBahtText(s.InterestAmount)
                        , OtherInterestAmountText  = ThaiBahtHelper.ThaiBahtText(s.OtherInterestAmount)
                        , PrintLetterDate          = s.PrintDate
                        , PrintLetterDateText      = s.PrintDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("th-TH"))
                    })

                    .ToList();
                }

                else
                {
                    rwm.Data = new List<FirstLetterReportModel>();
                }
            }

            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   = 500;
                rwm.Serverity = "high";
                rwm.Data      = new List<FirstLetterReportModel>();
            }

            return rwm;
        }

        //PRIVATE BUSINESS
        ResultWithModel<FirstLetterInitialResponse> GetInitialFirstLetter(string RequestID)
        {
            ResultWithModel<FirstLetterInitialResponse> rwm = new ResultWithModel<FirstLetterInitialResponse>();

            try
            {
                BaseParameterModel Parameter = new BaseParameterModel();

                Parameter.ProcedureName = "sp_tray_firstletter_address_initial";

                var props = typeof(FirstLetterInitialResponse).GetProperties();

                foreach (var p in props)
                {
                    Parameter.ResultModelNames.Add(p.Name);
                }

                Parameter.Parameters = new List<FieldModel> { new FieldModel { Name = "RequestID", Value = RequestID } };

                rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<FirstLetterInitialResponse>(baseParameter: Parameter);

                if (!rwm.Success)
                {
                    rwm.Data = new FirstLetterInitialResponse();
                }
            }

            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   =  500;
                rwm.Serverity = "high";
            }

            return rwm;
        }
        ResultWithModel<CustomerInformationResponse> GetCustomerInformation(decimal CIFNo, bool IsOwner = false)
        {
            ResultWithModel<CustomerInformationResponse> rwm = new ResultWithModel<CustomerInformationResponse>();

            try
            {
                BaseParameterModel Parameter = new BaseParameterModel();
                Parameter.ProcedureName = "sp_tray_customer_info";

                var props = typeof(CustomerInformationResponse).GetProperties();

                foreach (var p in props)
                {
                    Parameter.ResultModelNames.Add(p.Name);
                }

                Parameter.Parameters = new List<FieldModel> {
                      new FieldModel { Name = "CIFNo", Value = CIFNo }
                    , new FieldModel { Name = "IsOwner", Value = IsOwner }
                };
             

                rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<CustomerInformationResponse>(baseParameter: Parameter);

                if (!rwm.Success)
                {
                    rwm.Data = new CustomerInformationResponse();
                }
            }

            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   =  500;
                rwm.Serverity = "high";
            }

            return rwm;
        }
 
    }
}
