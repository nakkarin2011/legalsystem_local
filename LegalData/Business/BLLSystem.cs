﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLSystem
    {
        DatabaseContext _dbContext;

        public BLLSystem(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ResultWithModel<RequestSignatureModel>RequestSignature(string signature) {

            ResultWithModel<RequestSignatureModel> rwm = new ResultWithModel<RequestSignatureModel>();
            try
            {
                    signature = signature.Replace(" ", "+");

                var _signature =Cryptography.Decrypt(signature);

                rwm.Success = true;
                rwm.Message = "Successful";
                rwm.RefCode = 200;
                rwm.Serverity = "low";
                rwm.Data = JsonConvert.DeserializeObject<RequestSignatureModel>(_signature);

            }


            catch (Exception ex)
            {
                rwm.Success = false;
                rwm.Message = ex.Message;
                rwm.RefCode = 500;
                rwm.Serverity = "high";
            }

            return rwm;
        }

        public ResultWithModel<string> CreateRequestSignature(RequestSignatureModel signature)
        {
            ResultWithModel<string> rwm = new ResultWithModel<string>();

            try
            {
                AppSettingsReader settingsReader = new AppSettingsReader();

                var _plaintext  = JsonConvert.SerializeObject(signature); 

                rwm.Success     = true;
                rwm.Message     = "Successful";
                rwm.RefCode     = 200;
                rwm.Serverity   = "low";
                rwm.Data        = Cryptography.Encrypt(JsonConvert.SerializeObject(signature));

 

            }
            catch (Exception ex)  {
                rwm.Success = false;
                rwm.Message = ex.Message;
                rwm.RefCode = 500;
                rwm.Serverity = "high";
            }

            return rwm;
        }
    }
}
