﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLRequestDocumentExtension 
        : ModelSet<RequestDocumentExtensionRequest, RequestDocumentExtensionResponse>
    {
        public BLLRequestDocumentExtension(DatabaseContext dbContext)
          : base(dbContext)
        {
        
        }

        public ResultWithModel<RequestDocumentExtensionResponse> Find(Guid extensionID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_request_document_extention_info",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="ExtensionID", Value = extensionID }
                }
            };

            var props = typeof(RequestDocumentExtensionResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<RequestDocumentExtensionResponse> rwm = new ResultWithModel<RequestDocumentExtensionResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<RequestDocumentExtensionResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new RequestDocumentExtensionResponse();
            }

            return rwm;
        }
        public ResultWithModel<RequestDocumentExtensionResponse> FindByRequest(Guid requestID)
        {
            var Parameter = new BaseParameterModel()
            {
                ProcedureName = "sp_request_document_extention_info",
                Parameters = new List<FieldModel> {
                    new FieldModel { Name ="RequestID", Value = requestID }
                }
            };

            var props = typeof(RequestDocumentExtensionResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<RequestDocumentExtensionResponse> rwm = new ResultWithModel<RequestDocumentExtensionResponse>();

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<RequestDocumentExtensionResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new RequestDocumentExtensionResponse();
            }

            return rwm;
        }
        public ResultWithModel Save(RequestDocumentExtensionModel model)
        {
            ResultWithModel rwm = new ResultWithModel();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();

            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = "sp_request_document_extention_save",
                Parameters = new List<FieldModel> {
                          new FieldModel { Name ="ExtensionID               ", Value = model.ExtensionID       }
                        , new FieldModel { Name ="RequestID                 ", Value = model.RequestID         }
                        , new FieldModel { Name ="TrayActionID              ", Value = model.TrayActionID      }
                        , new FieldModel { Name ="DocumentID                ", Value = model.DocumentID        }
                        , new FieldModel { Name ="DocumentStatusID          ", Value = model.DocumentStatusID  }
                        , new FieldModel { Name ="DocumentStatusDate        ", Value = model.DocumentStatusDate}
                        , new FieldModel { Name ="PrintBy                   ", Value = DbHelper.NullHandler(model.PrintBy)}
                        , new FieldModel { Name ="PrintDate                 ", Value = DbHelper.NullHandler(model.PrintDate)}
                        , new FieldModel { Name ="CreateBy                  ", Value = model.CreateBy          }
                    }
            });

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery(Procedures);

            return rwm;
        }


    }
}
