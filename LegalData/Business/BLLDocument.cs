﻿using LegalData.Helper;
using LegalData.Model;
using LegalData.Model.Response;
using LegalData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Business
{
    public class BLLDocument
    {
        DatabaseContext _dbContext;

        public BLLDocument(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ResultWithModel<DocumentRunningResponse> GetAttorneyDocumentRunning(BaseParameterModel Parameter)
        {
            Parameter.ProcedureName = "sp_attorney_docrunning_increment";

            var props = typeof(DocumentRunningResponse).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<DocumentRunningResponse> rwm = new ResultWithModel<DocumentRunningResponse>();

            var con = _dbContext.connectionName;

            rwm = (new DbHelper(DbHelper.GetConnection(name: _dbContext.connectionName))).Execute<DocumentRunningResponse>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new DocumentRunningResponse();
            }

            return rwm;
        }

        public ResultWithModel<string> GenerateLegalNo(int lawyerAttorneyID, string userID) {

            ResultWithModel<string> rwm = new ResultWithModel<string>();

            try
            {
                int RunningNo = 0;
                int LawyerAttorneyID = 0;

                var DocumentRunningResult = GetAttorneyDocumentRunning(new BaseParameterModel
                {
                    Parameters = new List<FieldModel> {
                                  new FieldModel {  Name = "AttorneyID", Value = lawyerAttorneyID }
                                , new FieldModel {  Name = "UserID", Value =  userID }
                         }
                });

                if (DocumentRunningResult.Success)
                {
                    var running = DocumentRunningResult.Data.documents.FirstOrDefault();

                    RunningNo = running.RunningNo;
                }

                rwm.RefCode = 200;
                rwm.Message = "Successful";
                rwm.Success = true;
                rwm.Total   = 1; 
                rwm.Data    = $"กม.(คดี {LawyerAttorneyID.ToString("00")})-{RunningNo.ToString("0000")}/{DateTime.Now.ToString("yyyy", new System.Globalization.CultureInfo("th-TH"))}";
            }
            catch (Exception ex)
            {
                rwm.Success   = false;
                rwm.Message   = ex.Message;
                rwm.RefCode   = 500;
                rwm.Serverity = "high";
            }

            return rwm;
        }
 
    }
}
