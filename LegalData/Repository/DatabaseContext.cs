﻿using LegalData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Repository
{
    public partial class DatabaseContext
    {
        public string connectionName;

        public DatabaseContext()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Connection string Name from Web.config/AppSetting</param>
        public DatabaseContext(string name)
        {
           connectionName = name;
        }

        public List<FieldModel> GetFields<M>(M model)
        {
            List<FieldModel> Fields = new List<FieldModel>();

            var props = model.GetType().GetProperties();

            foreach (var p in props)
            {
                var Value = p.GetValue(model);

                if (Value != null)
                {

                    var t = p.GetValue(model).GetType();

                    if (t == typeof(DateTime))
                    {
                        Fields.Add(new FieldModel { Name = p.Name, Value = p.GetValue(model), DataType = System.Data.SqlDbType.DateTime });
                    }

                    else
                    {
                        Fields.Add(new FieldModel { Name = p.Name, Value = p.GetValue(model) });
                    }
                }
            }

            return Fields;
        }

    }
}
