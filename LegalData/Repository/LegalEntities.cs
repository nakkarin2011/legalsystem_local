﻿using LegalData.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Repository
{
    public class LegalEntities : DatabaseContext
    {
        public LegalEntities()
         : base(name: "LegalConnectionString")
        {
            Menu = new BLLMenu(this);
            Tray = new BLLTray(this);
            DropDownList = new BLLDropDownList(this);
            CoverPage = new BLLCoverPage(this);
            FirstLetter = new BLLFirstLetter(this);
            Document = new BLLDocument(this);
            System = new BLLSystem(this);
            DocumentExtension = new BLLRequestDocumentExtension(this);
        }

        public virtual BLLMenu Menu { get; set; }
        public virtual BLLTray Tray { get; set; }
        public virtual BLLDocument Document { get; set; }
        public virtual BLLCoverPage CoverPage { get; set; }
        public virtual BLLFirstLetter FirstLetter { get; set; }
        public virtual BLLDropDownList DropDownList { get; set; }
        public virtual BLLSystem System { get; set; }

        public virtual BLLRequestDocumentExtension DocumentExtension  { get; set;}
    }
}
