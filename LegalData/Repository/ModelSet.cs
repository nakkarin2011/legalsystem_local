﻿using LegalData.Helper;
using LegalData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Repository
{
    public class ModelSet<T, R> where R : new()
    {
        public DatabaseContext dbContext;

        public string ModuleName;
        public string ProcedureNameTemplate = "sp_{0}_{1}";
        public enum ProcedureAction
        {
            list,
            create,
            update,
            delete
        };
        public ModelSet(DatabaseContext dbContext)
        {
            this.dbContext = dbContext;

            ModuleName = DbHelper.ToProcedureName(typeof(T).Name);
        }
        public virtual ResultWithModel<R> Search(BaseParameterModel Parameter)
        {

            Parameter.ProcedureName = string.Format(ProcedureNameTemplate, ModuleName, ProcedureAction.list);

            var props = typeof(R).GetProperties();

            foreach (var p in props)
            {
                Parameter.ResultModelNames.Add(p.Name);
            }

            ResultWithModel<R> rwm = new ResultWithModel<R>();

            var con = dbContext.connectionName;

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).Execute<R>(baseParameter: Parameter);

            if (!rwm.Success)
            {
                rwm.Data = new R();
            }

            return rwm;
        }
        public virtual ResultWithModel<R> Create(T model)
        {
            ResultWithModel<R> rwm = new ResultWithModel<R>();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();
            List<FieldModel> FieldModels = new List<FieldModel>();

            var props = model.GetType().GetProperties();

            foreach (var p in props)
            {
                var Value = p.GetValue(model);

                if (Value != null)
                {
                    FieldModels.Add(new FieldModel { Name = p.Name, Value = p.GetValue(model) });
                }
            }

            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = string.Format(ProcedureNameTemplate, ModuleName, ProcedureAction.create),
                Parameters = FieldModels
            });

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery<R>(Procedures);
            return rwm;
        }
        public virtual ResultWithModel<R> Update(T model)
        {
            ResultWithModel<R> rwm = new ResultWithModel<R>();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();
            List<FieldModel> FieldModels = new List<FieldModel>();

            var props = model.GetType().GetProperties();

            foreach (var p in props)
            {
                var Value = p.GetValue(model);

                if (Value != null)
                {
                    FieldModels.Add(new FieldModel { Name = p.Name, Value = p.GetValue(model) });
                }
            }

            Procedures.Add(new BaseParameterModel()
            {
                ProcedureName = string.Format(ProcedureNameTemplate, ModuleName, ProcedureAction.update),
                Parameters = FieldModels
            });

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery<R>(Procedures);
            return rwm;
        }
        public virtual ResultWithModel<R> Delete(List<FieldModel> parameters)
        {
            ResultWithModel<R> rwm = new ResultWithModel<R>();
            List<BaseParameterModel> Procedures = new List<BaseParameterModel>();
            BaseParameterModel Parameter = new BaseParameterModel();
            Parameter.ProcedureName = string.Format(ProcedureNameTemplate, ModuleName, ProcedureAction.delete);
            Parameter.Parameters.AddRange(parameters);
            Procedures.Add(Parameter);

            rwm = (new DbHelper(DbHelper.GetConnection(name: dbContext.connectionName))).ExecuteNonQuery<R>(Procedures);
            return rwm;
        }
    }
}
