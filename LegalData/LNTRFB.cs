//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LegalData
{
    using System;
    using System.Collections.Generic;
    
    public partial class LNTRFB
    {
        public decimal NWACCT { get; set; }
        public decimal OLACCT { get; set; }
        public decimal NWBR { get; set; }
        public decimal OLBR { get; set; }
        public decimal NWAANO { get; set; }
        public decimal OLAANO { get; set; }
        public string OLFCDE { get; set; }
        public decimal OLFSEQ { get; set; }
        public decimal OLCIF { get; set; }
        public string OLSNME { get; set; }
        public string OLFSNM { get; set; }
        public string OLTYPE { get; set; }
        public decimal OLGRP { get; set; }
        public decimal INPDAT { get; set; }
        public decimal INPDT7 { get; set; }
        public decimal TRFED6 { get; set; }
        public decimal TRFED7 { get; set; }
        public string TRFFLG { get; set; }
    }
}
