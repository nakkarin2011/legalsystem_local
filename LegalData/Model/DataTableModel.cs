﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class DataTableModel
    {
        public DataTableModel()
        {
            this.start = 0;
            this.length = 50;
        }

        // properties are not capital due to json mapping
        public int draw { get; set; }
        int _start = 0;

        public int start
        {
            get
            {
                return _start += 1;

            }

            set
            {
                _start = value;
            }


        }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }
    }

    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
    }
    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }
    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
}
