﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class SignatureModel
    {
        public string PermissionID { get; set; }
        public string UserID { get; set; }
        public DateTime ExpireDate { get; } = DateTime.Now.AddDays(1);
    }
}
