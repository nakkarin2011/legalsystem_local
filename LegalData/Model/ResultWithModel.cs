﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class ResultWithModel
    {
        public ResultWithModel()
        {
            this.Success = false;
            this.RefCode = 0;
            this.Serverity = "None";
            this.Message = string.Empty;
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public int Total { get; set; }
        public object Data { get; set; }
        public int RefCode { get; set; }
        public string Serverity { get; set; }
        public string Signature { get; set; }
    }
    public class ResultWithModel<T>
    {
        public ResultWithModel()
        {
            this.Success = false;
            this.RefCode = 0;
            this.Serverity = "None";
            this.Message = string.Empty;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public int Total { get; set; }
        public T Data { get; set; }
        public int RefCode { get; set; }
        public string Serverity { get; set; }
    }
}
