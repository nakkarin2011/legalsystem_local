﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class DDLItemModel
    {
        public string Text { get; set; }
        public object Value { get; set; }
    }
}
