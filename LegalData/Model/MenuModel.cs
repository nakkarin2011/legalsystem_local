﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class MenuModel
    {

        public MenuModel() {
            SubMenus = new List<MenuModel>();
        }

        public Guid MenuID { get; set; }
        public string MenuName { get; set; }
        public string ShortName { get; set; }
        public string IconClass { get; set; }
        public string Url { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public List<MenuModel> SubMenus { get; set; }
    }
}
