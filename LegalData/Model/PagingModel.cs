﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class PagingModel
    {
        public PagingModel()
        {
            Start = 0;
            Limit = 50;
        }

        public int Start { get; set; }
        public int Limit { get; set; }
    }
}
