﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class ButtonPermissionModel
    {
        public bool IsPrint { get; set; }
        public bool IsApprove { get; set; }
        public bool IsReturn { get; set; }
        public bool IsSend { get; set; }
        public bool IsClose { get; set; }

        public bool IsFirstLetter { get; set; }
        public bool IsCoverPage { get; set; }
        public bool IsNotice { get; set; }
        public int TrayID { get; set; }

    }
}
