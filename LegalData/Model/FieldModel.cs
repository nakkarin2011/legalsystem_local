﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class FieldModel
    {
       public string Name { get; set; }

        private object _value = DBNull.Value;
        public object Value
        {
            get
            {
                return _value != null ? _value : DBNull.Value;
            }

            set
            {
                _value = value;
            }
        }

        public SqlDbType? DataType { get; set; }
        public int? Size { get; set; }

    }
}
