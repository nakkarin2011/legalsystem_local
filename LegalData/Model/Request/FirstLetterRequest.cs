﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class FirstLetterRequest
    {
        public string RequestID { get; set; }
        public string UserID { get; set; }
    }
}
