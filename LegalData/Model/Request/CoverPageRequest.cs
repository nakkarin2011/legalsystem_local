﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class CoverPageRequest
    {
        public string DocumentID { get; set; }

        public string RequestID { get; set; }
    }
}
