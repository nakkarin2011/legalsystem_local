﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class TrayRequest
    {
        public decimal CIFNo             {get; set;}
        public int DocumentStatusID     { get; set; }
        public int DocumentTypeID       { get; set; }

        public string RequestNo { get; set; }
        public string AssignOAName { get; set; }
        public string LegalNo { get; set; }


    }
}
