﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class PrintDocumentRequest
    {
        public Guid PrintDocumentID     {get; set;}
        public Guid RequestID           {get; set;}
        public int DocumentTypeID       {get; set;}
        public int DocumentStatusID     {get; set;}
        public string CreateBy          {get; set;}
        public DateTime CreateDate      {get; set;}
    }                                     
}                                         
