﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class SaveFirstLetterAddressRequest
    {
        public Guid requestID { get; set; }
        public string userID { get; set; }
        public List<FirstLetterAddressModel> Address { get; set; }
    }
}
