﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class SaveFirstLetterTabRequest
    {
        public string requestID { get; set; }
        public string userID { get; set; }
        public List<FirstLetterTabModel> tabs { get; set; }
    }

 
    public class FirstLetterTabModel
    {
        public string letterid { get; set; }
        public decimal cifno { get; set; }
        public string name { get; set; }
        public string relationcode { get; set; }
        public string relationname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
    }
}
