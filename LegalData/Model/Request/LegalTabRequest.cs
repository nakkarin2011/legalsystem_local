﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class legalTab1
    {
        public Guid RequestID { get; set; }
        public string LegalStatus { get; set; }
        public string ActiveTab { get; set; }
        public DateTime? ReceivingDocdate { get; set; }
        public DateTime? SendingLawyerDate { get; set; }
        public string RemarkTab1 { get; set; }
    }

    public class legalTab2
    {
        public Guid RequestID { get; set; }
        public string LegalStatus { get; set; }
        public DateTime? NoticeDate { get; set; }
        public DateTime? SetCaseDate { get; set; }
        public DateTime? CaseDate { get; set; }
        public string RemarkTab2 { get; set; }
        public string ActiveTab { get; set; }
    }

    public class legalTab3
    {
        public Guid RequestID { get; set; }
        public string LegalStatus { get; set; }

        public string IndictmentStatus { get; set; }
        public string IndictmentReason { get; set; }
        public string Blackcase { get; set; }
        public int CourtZoneID { get; set; }
        public decimal Capital { get; set; }
        public decimal Courtfees { get; set; }
        public DateTime? LastCourtDate1 { get; set; }
        public string CourtDateReason1 { get; set; }
        public DateTime? JudgmentDate { get; set; }
        public DateTime? JudgmentAllowDate { get; set; }
        public string RedCase { get; set; }
        public string CaseFee { get; set; }
        public string JudgmentStatus { get; set; }
        public string JudgmentReason { get; set; }
        public string AppealType { get; set; }
        public string AppealReason { get; set; }
        public DateTime? AppealDate { get; set; }
        public string AppealBlackcase { get; set; }
        public decimal AppealCapital { get; set; }
        public decimal AppealFee { get; set; }
        public DateTime? AppealEditdate { get; set; }
        public DateTime? LastCourtDate2 { get; set; }
        public string CourtDateReason2 { get; set; }
        public DateTime? AppealCourtjudgmentDate { get; set; }
        public string AppealRedCase { get; set; }
        public string PetitionType { get; set; }
        public string PetitionReason { get; set; }
        public DateTime? SupremeDate { get; set; }
        public string BlackcaseSupreme { get; set; }
        public decimal SupremeCapital { get; set; }
        public decimal SupremeFee { get; set; }
        public DateTime? SupremeEditDate { get; set; }
        public DateTime? LastCourtDate3 { get; set; }
        public string CourtDateReason3 { get; set; }
        public DateTime? SupremeCourtjudgmentDate { get; set; }
        public string SupremeRedCase { get; set; }
        public string ActiveTab { get; set; }

    }

    public class legalTab4
    {
        public Guid RequestID { get; set; }
        public string LegalStatus { get; set; }
        public DateTime? ExecuteDate { get; set; }
        public DateTime? SetExecuteDate { get; set; }
        public DateTime? SendExecuteDocdate { get; set; }
        public DateTime? ReceivingExecuteDocdate { get; set; }
        public string ExecuteBookType { get; set; }

        public string ActiveTab { get; set; }
    }

    public class ModalCourt
    {
        public int CA_ID { get; set; }
        public int CourtID { get; set; }
        public Guid RequestID { get; set; }
        public DateTime? CourtDate { get; set; }
        public string Comment { get; set; }
    }


     
}
