﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Request
{
    public class UpdateFirstLetterInfoRequest
    {
         public string FirstLetterID    {get; set;}
         public string CustomerName     {get; set;}
	     public string Address1         {get; set;}
	     public string Address2         {get; set;}
	     public string UpdateBy         {get; set;}

    }
}
