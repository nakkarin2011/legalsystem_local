﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class CoverPageModel
    {
        public CoverPageModel() {

            CoverPageDetails = new List<CoverPageDetailModel>(); 

        }

        public Guid CoverPageID    { get;set;}
        public Guid DocumentID     { get;set;}
        public Guid  RequestID     { get;set;}
        public string LegalNo      { get;set;}
        public bool IsActive       { get;set;}
        public string CreateBy     { get;set;}

        public List<CoverPageDetailModel> CoverPageDetails { get; set; }
    }
}
