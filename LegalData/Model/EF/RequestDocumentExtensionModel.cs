﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class RequestDocumentExtensionModel
    {
        public Guid RequestID               {get; set;}
        public int LawyerAttorneyID         {get; set;}
        public int RequestTypeNo            { get; set;}
        public Guid? ExtensionID            {get; set;}
        public Guid? TrayActionID           {get; set;}
        public string TrayActionName        {get; set;}
        public Guid? DocumentID             {get; set;}
        public Guid? DocumentStatusID       {get; set;}
        public int? DocumentStatusNo        {get; set;}
        public string DocumentStatusName    {get; set;}
        public int? TrayID                  {get; set;}

        public string CreateBy { get; set; }
        public DateTime? DocumentStatusDate { get; set; }
        public string PrintBy { get; set; }
        public DateTime? PrintDate { get; set; }
    }
}
