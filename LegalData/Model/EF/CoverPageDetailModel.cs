﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class CoverPageDetailModel
    {
         public Guid CoverPageDetailID     {get; set;}
         public Guid CoverPageID           {get; set;}
	     public Guid CoverPageDocumentID   {get; set;}
         public Guid CoverPagePropertiesID {get; set;}
	     public string  Value              {get; set;}
    }
}
