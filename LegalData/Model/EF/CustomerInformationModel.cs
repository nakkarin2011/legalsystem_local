﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class CustomerInformationModel
    {
           public decimal CIFNo                 {get; set;}
           public decimal AccountNo             {get; set;}
           public string RelationCode           {get; set;}
           public string CustomerName           {get; set;}
           public string HouseNo                {get; set;}
           public string VillageNo              {get; set;}
           public string Village                {get; set;}
           public string Streee                 {get; set;}
           public string SubDistrict            {get; set;}
           public string DistrictAndProvince    {get; set;}
           public string FullAddress            {get; set;}
    }
}
