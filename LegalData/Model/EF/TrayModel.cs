﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class TrayModel
    {
        public int RowNo                       {get; set;}
        public Guid RequestID                  {get; set;}
        public string RequestNo                {get; set;}
        public string RequestTypeID            {get; set;}
        public string RequestTypeName          {get; set;}
        public string LoanType                 {get; set;}
        public string RequestStatusID          {get; set;}
        public string RequestStatusName        {get; set;}
        public decimal? CIFNo                  {get; set;}
        public string CIFName                  {get; set;}
        public Guid? LegalID                   {get; set;}
        public string LegalNo                  {get; set;}
        public string LegalStatusID            {get; set;}
        public string LegalStatusName          {get; set;}
        public DateTime? SLASendDate           {get; set;}
        public string AssignOAID               {get; set;}
        public string AssignOAName             {get; set;}
        public int? TrayID                     {get; set;}
        public Guid? TrayActionID              {get; set;}
        public string TrayActionName           {get; set;}
        public Guid? DocumentID                {get; set;}
        public string DocumentName             {get; set;}
        public Guid? DocumentStatusID          {get; set;}
        public string DocumentStatusName       {get; set;}
        public DateTime? DocumentStatusDate    {get; set;}
        public DateTime? PrintDate             {get; set;}
    }
}
