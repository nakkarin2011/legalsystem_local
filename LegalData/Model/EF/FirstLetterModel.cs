﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class FirstLetterModel
    {
         public Guid FirstLetterID   {get; set;}
         public Guid RequestID       {get; set;}
         public string LegalNo       {get; set;}
         public string CreateBy      {get; set;}
         public bool IsSave          {get; set;}

         public List<FirstLetterInformationModel> Details { get; set; }

         public RequestDocumentExtensionModel Document { get; set; }
    }
}
