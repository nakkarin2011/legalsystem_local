﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class CoverPageInformationModel
    {
        public Guid? CoverPageID { get; set; }
        public string LegalNo { get; set; }
        public Guid CoverPageDocumentID { get; set; }
        public int CoverPageDocumentNo { get; set; }
        public string CoverPageDocumentName { get; set; }
        public Guid? CoverPagePropertiesID { get; set; }
        public int? CoverPagePropertiesNo { get; set; }
        public string InputType { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public string Source { get; set; }
        public string Unit { get; set; }
    }
}
