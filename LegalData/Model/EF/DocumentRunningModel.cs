﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class DocumentRunningModel
    {

        public Guid DocumentRunningID { get; set; }
        public int AttorneyID { get; set; }
        public int Year { get; set; }
        public int RunningNo { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }


    }
}
