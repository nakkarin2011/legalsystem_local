﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class FirstLtterInitialModel
    {

        public decimal CIFNo { get; set; }
        public int LawyerAttorneyID { get; set; }
        public string SeniorLawyerID { get; set; }
        public string SeniorLawyerName { get; set; }
        public string RelationCode { get; set; }
        public string RelationName { get; set; }
        public DateTime? LoanDate { get; set; }
        public string CustomerName { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? CurrentBalanceAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? OtherInterestAmount { get; set; }
    }
}
