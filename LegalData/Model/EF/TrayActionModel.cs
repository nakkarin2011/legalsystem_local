﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class TrayActionModel
    {
        public Guid  TrayActionID       {get; set;}
        public string TrayActionCode    {get; set;}
        public string TrayActionName    {get; set;}
        public bool IsDisable           { get; set; }
    }
}
