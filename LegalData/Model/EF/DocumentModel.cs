﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class DocumentModel
    {
            public Guid DocumentID          { get; set;}  
            public string DocumentCode        { get; set;}
            public string DocumentName        { get; set;}
    }
}
