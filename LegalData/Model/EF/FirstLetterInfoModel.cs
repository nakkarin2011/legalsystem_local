﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class FirstLetterInfoModel
    {
        public Guid FirstLetterID       { get; set; }
        public string FirstLetterNo       { get; set; }
        public Guid RequestID           { get; set; }
        public decimal CIFNo               { get; set; }
        public int LawyerAttorneyID    { get; set; }
        public string  SeniorLawyerID { get; set; }
        public string SeniorLawyerName    { get; set; }
        public string RelationCode        { get; set; }
        public string RelationName        { get; set; }
        public string LoanDateText        { get; set; }
        public string CustomerName        { get; set; }
        public decimal LoanAmount          { get; set; }
        public decimal CurrentBalanceAmount { get; set; }
        public decimal InterestAmount      { get; set; }
        public decimal OtherInterestAmount { get; set; }
        public string BorrowerName        { get; set; }
        public string Address1            { get; set; }
        public string Address2            { get; set; }
        public DateTime PrintLetterDate     { get; set; }
    }
}
