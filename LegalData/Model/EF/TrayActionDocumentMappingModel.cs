﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class TrayActionDocumentMappingModel
    {
        public Guid TrayActionDocumentMappingID { get; set;}
        public Guid TrayActionID                { get; set;}
        public string TrayActionCode              { get; set;}
        public string TrayActionName              { get; set;}
        public Guid DocumentID                  { get; set;}
        public string DocumentCode                { get; set;}
        public string DocumentName                { get; set;}

    }
}
