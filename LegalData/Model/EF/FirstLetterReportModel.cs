﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.EF
{
    public class FirstLetterReportModel
        : FirstLtterInitialModel
    {

        public Guid FirstLetterID { get; set; }
        public Guid RequestID { get; set; }
        public string LoanDateText { get; set; } 
        public string LoanAmountText { get; set; }
        public string CurrentBalanceAmountText { get; set; }
        public string InterestAmountText { get; set; }
        public string OtherInterestAmountText { get; set; }
        public string BorrowerName { get; set; }
  
        public string LegalNo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        
        public DateTime PrintLetterDate { get; set; }
        public string PrintLetterDateText { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }

    }
}
