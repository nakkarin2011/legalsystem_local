﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class RequestSignatureModel
        : SignatureModel
    {
        public string RequestID { get; set; }
    }
}
