﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model
{
    public class BaseParameterModel
    {

        public BaseParameterModel()
        {
            Parameters = new List<FieldModel>();
            ResultModelNames = new List<string>();
            Paging = new PagingModel();
            Orders = new List<OrderByModel>();
        }


        public string ProcedureName { get; set; }
        public List<FieldModel> Parameters { get; set; }
        public List<OrderByModel> Orders { get; set; }
        public PagingModel Paging { get; set; }
        public List<string> ResultModelNames { get; set; }

    }
}
