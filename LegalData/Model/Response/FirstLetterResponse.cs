﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class FirstLetterResponse
    {
        public FirstLetterResponse() {

            firstLetterInformations = new List<FirstLetterInformationModel>();
        }
        public List<FirstLetterInformationModel> firstLetterInformations { get; set; }
    }
}
