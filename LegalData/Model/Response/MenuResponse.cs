﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class MenuResponse
    {

        public MenuResponse() {
            Menus = new List<MenuModel>();
        }
         public List<MenuModel> Menus { get; set; }
    }
}
