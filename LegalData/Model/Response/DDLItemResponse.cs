﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class DDLItemResponse
    {
            public DDLItemResponse()
            {
                DDLItems = new List<DDLItemModel>();
            }

            public List<DDLItemModel> DDLItems { get; set; }
    }
}
