﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class GetTrayActionDocumentResponse
        : TrayActionModel
    {

        public Guid DocumentID { get; set; }
        public List<DocumentModel> Documents { get; set; }
    }
}
