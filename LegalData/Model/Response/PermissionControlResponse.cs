﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class PermissionControlResponse
    {
        public PermissionControlResponse()
        {
            Permissions = new List<ButtonPermissionModel>();
        }
        public List<ButtonPermissionModel> Permissions { get; set; }
    }
}
