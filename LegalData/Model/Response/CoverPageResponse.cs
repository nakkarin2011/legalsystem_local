﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class CoverPageResponse
    {
        public List<CoverPageInformationModel> CoverPages { get; set; }
    }
}
