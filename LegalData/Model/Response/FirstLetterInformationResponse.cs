﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class FirstLetterInformationResponse
    {
        public List<FirstLetterInfoModel> firstLetters { get; set; }
    }
}
