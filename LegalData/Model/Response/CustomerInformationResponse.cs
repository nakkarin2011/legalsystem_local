﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class CustomerInformationResponse
    {
        public List<CustomerInformationModel> Customers { get; set; }
    }
}
