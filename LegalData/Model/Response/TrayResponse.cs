﻿using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalData.Model.Response
{
    public class TrayResponse
    {

        public TrayResponse() {

            Trays = new List<TrayModel>();
        }
        public List<TrayModel> Trays { get; set; }

    }
}
