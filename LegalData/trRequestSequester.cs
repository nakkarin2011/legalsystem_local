//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LegalData
{
    using System;
    using System.Collections.Generic;
    
    public partial class trRequestSequester
    {
        public int ReqSeqID { get; set; }
        public int ReqSeqNo { get; set; }
        public string LegalNo { get; set; }
        public System.DateTime ReqSeqDate { get; set; }
        public decimal ReqSeqAmt { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime CreateDateTime { get; set; }
        public string UpdateBy { get; set; }
        public System.DateTime UpdateDateTime { get; set; }
    }
}
