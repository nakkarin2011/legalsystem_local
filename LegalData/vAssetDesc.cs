//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LegalData
{
    using System;
    using System.Collections.Generic;
    
    public partial class vAssetDesc
    {
        public int A_ID { get; set; }
        public Nullable<System.Guid> f_ID { get; set; }
        public string AssetType { get; set; }
        public string AssetNo { get; set; }
        public string AssetProvince { get; set; }
        public string AssetDistrict { get; set; }
        public string AssetSubDistrict { get; set; }
        public string AssetAddress { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string AssetTypeName { get; set; }
        public string Province { get; set; }
    }
}
