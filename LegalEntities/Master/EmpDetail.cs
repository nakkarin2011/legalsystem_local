﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msEmpDetail")]
    public class EmpDetail
    {

        [Column(IsPrimaryKey = true)]
        public string EmpCode { get; set; }

        [Column]
        public string EmpName { get; set; }

        [Column]
        public string EmpEmail { get; set; }

        [Column]
        public Byte[] EmpSignature { get; set; }

    }
}
