﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msStatementAccType")]
    public class StatementAccType
    {

        [Column(IsPrimaryKey = true)]
        public int StatementID { get; set; }

        [Column]
        public string StatementName { get; set; }

        [Column]
        public string Description { get; set; }

    }
}
