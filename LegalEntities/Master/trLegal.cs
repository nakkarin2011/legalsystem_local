﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trLegal")]
    public class trLegal
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public Guid LegalID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public string LegalStatus { get; set; }

        [Column]
        public DateTime? T1_ReceiveReqDate { get; set; }

        [Column]
        public DateTime? T1_SendDocDate { get; set; }

        [Column]
        public int? T1_LawyerID { get; set; }

        [Column]
        public DateTime? T1_GenCallingDocDate { get; set; }

        [Column]
        public DateTime? T1_ReceiveDocDate { get; set; }

        [Column]
        public DateTime? T1_SendToLawyerDate { get; set; }

        [Column]
        public DateTime? T2_GenNoticeDate { get; set; }

        [Column]
        public DateTime? T2_AssignSueDate { get; set; }

        [Column]
        public DateTime? T2_SueDate { get; set; }

        [Column]
        public bool? T3_PlaintRight { get; set; }

        [Column]
        public string T3_PlaintReason { get; set; }

        [Column]
        public string T3_UndecidedCase { get; set; }

        [Column]
        public int? T3_CourtZoneID { get; set; }

        [Column]
        public decimal T3_AssetAmt { get; set; }

        [Column]
        public decimal T3_CourtFee { get; set; }

        [Column]
        public DateTime? T3_CourtJudge { get; set; }

        [Column]
        public DateTime? T3_BendJudgeDate { get; set; }

        [Column]
        public string T3_DecidedCase { get; set; }

        [Column]
        public bool? T3_Racha { get; set; }

        [Column]
        public bool? T3_JudgeRight { get; set; }

        [Column]
        public string T3_JudgeDesc { get; set; }

        [Column]
        public bool? T3_Appeal { get; set; }

        [Column]
        public DateTime? T3_AppDate { get; set; }

        [Column]
        public string T3_AppUndecidedCase { get; set; }

        [Column]
        public decimal T3_AppAssetAmt { get; set; }

        [Column]
        public decimal T3_AppFee { get; set; }

        [Column]
        public DateTime? T3_AppSendAdjudeDate { get; set; }

        [Column]
        public DateTime? T3_AppJudgeDate { get; set; }

        [Column]
        public string T3_AppDecidedCase { get; set; }

        [Column]
        public bool? T3_Petition { get; set; }

        [Column]
        public DateTime? T3_PetDate { get; set; }

        [Column]
        public string T3_PetUndecidedCase { get; set; }

        [Column]
        public decimal T3_PetAssetAmt { get; set; }

        [Column]
        public decimal T3_PetFee { get; set; }

        [Column]
        public DateTime? T3_PetSendAdjudeDate { get; set; }

        [Column]
        public DateTime? T3_PetJudgeDate { get; set; }

        [Column]
        public string T3_PetDecidedCase { get; set; }

        [Column]
        public DateTime? T4_GenSubpoenaDate { get; set; }

        [Column]
        public DateTime? T4_GotExecutionDate { get; set; }

        [Column]
        public int? T4_LawyerID { get; set; }

        [Column]
        public DateTime? T4_SendToLawyerDate { get; set; }

        [Column]
        public int? T5_PLawyerID { get; set; }

        [Column]
        public DateTime? T5_PSendtoLawyerDate { get; set; }

        [Column]
        public int? T5_SLawyerID { get; set; }

        [Column]
        public DateTime? T5_SSendtoLawyerDate { get; set; }

        [Column]
        public string Owner { get; set; } /*Add 26/08/2019 .b*/

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

        [Column]
        public int? SeqSendDoc { get; set; }
        [Column]
        public int? SeqSendOS { get; set; }

    }
}
