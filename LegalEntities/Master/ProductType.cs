﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LegalEntities
{

    [Table(Name = "dbo.msProductType")]
    public class ProductType
    {

        [Column(IsPrimaryKey = true)]
        public int PrdTypeID { get; set; }

        [Column]
        public string PrdTypeName { get; set; }

        [Column]
        public string PrdTypeDesc { get; set; }

    }
}
