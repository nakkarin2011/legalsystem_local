﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trLegal_CourtAssign")]
    public class trLegal_CourtAssign
    {
        [Key, Column(Order=0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AssignID { get; set; }

        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourtID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public DateTime AssignDate { get; set; }

        [Column]
        public string AssignReason { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
