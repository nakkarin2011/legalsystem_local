﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msDocumentType")]
    public class DocumentType
    {

        [Column(IsPrimaryKey = true)]
        public int DocTypeID { get; set; }

        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public string DocTypeName { get; set; }

        [Column]
        public string Flag { get; set; }

    }
}
