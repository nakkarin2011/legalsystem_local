﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LegalEntities
{
    [Table("dbo.msLegalStatus")]
    public class msLegalStatus
    {

        [Key,Column(Order=0)]
        public string ActionCode { get; set; }

        [Column]
        public string LegalStatusName { get; set; }

        [Column]
        public string Flag { get; set; }

        [Column]
        public string NCBStatus { get; set; }

        public string LegalStatusDisplay
        {
            get
            {
                return string.Format("{0}-{1}", ActionCode, LegalStatusName);
            }
        }

    }
}
