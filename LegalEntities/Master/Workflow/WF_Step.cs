using System;
using System.Data.Linq.Mapping;

namespace LegalSystem.Entities
{
	[Table(Name = "dbo.WF_Step")]
	public class WF_Step
	{
		[Column(IsPrimaryKey=true)]
		public int StateID { get; set; }

		[Column(IsPrimaryKey=true)]
		public int ActionID { get; set; }

		[Column(IsPrimaryKey=true)]
		public int Version { get; set; }

		[Column]
		public int NextState { get; set; }

		[Column]
		public int StatusID { get; set; }

        [Column]
        public string Remark { get; set; }
	}
}