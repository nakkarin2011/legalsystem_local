using System;
using System.Data.Linq.Mapping;

namespace LegalSystem.Entities
{
	[Table(Name = "dbo.WF_Status")]
	public class WF_Status
	{
		[Column(IsPrimaryKey=true)]
		public int StatusID { get; set; }

		[Column(IsPrimaryKey=true)]
		public int Version { get; set; }

		[Column]
		public string StatusName { get; set; }

       
	}
}