using System;
using System.Data.Linq.Mapping;
namespace LegalSystem.Entities
{
	[Table(Name = "dbo.WF_Profile")]
	public class WF_Profile
    {
		[Column(IsPrimaryKey=true)]
		public Guid ProfileID { get; set; }

        [Column]
		public string ApplicationNo { get; set; }

		[Column]
		public int StateID { get; set; }

		[Column]
		public int StatusID { get; set; }

		[Column]
		public DateTime StartDate { get; set; }

		[Column]
		public DateTime? EndDate { get; set; }

		[Column]
		public string CreateBy { get; set; }

        [Column]
        public string OwenBy { get; set; }

        [Column]
        public string OverrideBy { get; set; }

		[Column]
		public int Version { get; set; }

		[Column]
		public string IsActive { get; set; }
	}
}