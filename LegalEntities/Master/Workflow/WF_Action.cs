using System;
using System.Data.Linq.Mapping;
namespace LegalSystem.Entities
{
    [Table(Name = "dbo.WF_Action")]
    public class WF_Action
    {
        [Column(IsPrimaryKey = true)]
        public int ActionCode { get; set; }

        [Column(IsPrimaryKey = true)]
        public int Version { get; set; }

        [Column]
        public string ActionName { get; set; }
    }
}