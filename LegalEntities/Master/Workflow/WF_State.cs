using System;
using System.Data.Linq.Mapping;
namespace LegalSystem.Entities
{
	[Table(Name = "dbo.WF_State")]
	public class WF_State
	{
		[Column(IsPrimaryKey=true)]
		public int StateID { get; set; }

		[Column(IsPrimaryKey=true)]
		public int Version { get; set; }

		[Column]
		public string StateName { get; set; }
	}
}