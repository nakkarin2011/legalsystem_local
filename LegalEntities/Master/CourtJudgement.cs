﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msCourtJudgement")]
    public class CourtJudgement
    {

        [Column(IsPrimaryKey = true)]
        public int CJID { get; set; }

        [Column]
        public string CJName { get; set; }

    }
}
