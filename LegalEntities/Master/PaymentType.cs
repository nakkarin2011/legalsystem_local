﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msPaymentType")]
    public class PaymentType
    {

        [Column(IsPrimaryKey = true)]
        public int PaymentTypeID { get; set; }

        [Column]
        public string PaymentTypeName { get; set; }

    }
}
