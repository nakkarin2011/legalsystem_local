﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msCourtType")]
    public class CourtType
    {

        [Column(IsPrimaryKey = true)]
        public int CourtTypeID { get; set; }

        [Column]
        public string CourtTypeName { get; set; }

    }
}
