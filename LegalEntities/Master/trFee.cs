﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trFee")]
    public class trFee
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int FeeID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int CostTypeID { get; set; }

        [Column]
        public decimal CourtPayAmt { get; set; }

        [Column]
        public decimal CourtRefundAmt { get; set; }

        [Column]
        public DateTime? CourtRefundDate { get; set; }

        [Column]
        public string BankCode { get; set; }

        [Column]
        public DateTime? ChequeDate { get; set; }

        [Column]
        public string ChequeNo { get; set; }

        [Column]
        public string BankBranch { get; set; }

        [Column]
        public DateTime? SendChqDate { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
