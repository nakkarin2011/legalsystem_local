﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trPursue")]
    public class trPursue
    {
        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int PursueID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int PursueTime { get; set; }

        [Column]
        public DateTime PusrsueDate { get; set; }

        [Column]
        public bool FoundAssets { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }
    }
}
