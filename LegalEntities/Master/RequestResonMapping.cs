﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msRequestReasonMapping")]
    public class RequestReasonMapping
    {

        [Column(IsPrimaryKey = true)]
        public string ReasonID { get; set; }

        [Column(IsPrimaryKey = true)]
        public int ReqTypeID { get; set; }

    }
}
