﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trAsset")]
    public class trAsset
    {
        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AssetID { get; set; }

        [Column]
        public int AssetTypeID { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string District { get; set; }

        [Column]
        public string Province { get; set; }

        [Column]
        public string ZipCode { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
