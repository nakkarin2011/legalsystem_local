﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace LegalEntities
{
    [Table("dbo.LNMAST")]
    public class LNMAST
    {

        [Key, Column(Order = 0)]
        public Guid LID { get; set; }
        [Column]
        public decimal CIFNO { get; set; }

        [Column]
        public string CFNAME { get; set; }

        [Column]
        public decimal ACCTNO { get; set; }

        [Column]
        public string ACTYPE { get; set; }

        [Column]
        public decimal BRN { get; set; }

        [Column]
        public string ACNAME { get; set; }

        [Column]
        public string FCODE { get; set; }

        [Column]
        public decimal FSEQ { get; set; }

        [Column]
        public string TYPE { get; set; }

        [Column]
        public decimal STATUS { get; set; }
       
        [Column]
        public decimal LMCOST { get; set; }

        [Column]
        public decimal LMPRDC { get; set; }
        [Column]
        public string TDRCNT { get; set; }

        [Column]
        public DateTime ORGDT { get; set; }

        [Column]
        public decimal ORGAMT { get; set; }

        [Column]
        public decimal CBAL { get; set; }

        [Column]
        public decimal INTRATE { get; set; }

        [Column]
        public decimal ACCINT { get; set; }

        [Column]
        public decimal ACCI3 { get; set; }

        [Column]
        public decimal PENIN3 { get; set; }

        [Column]
        public decimal PENINT { get; set; }

        [Column]
        public decimal PDDAYS { get; set; }

        [Column]
        public string CFADDR { get; set; }

        [Column]
        public string CollateralDesc { get; set; }
        [Column]
        public string STAGE { get; set; }

        [NotMapped]
        public string _STATUS
        {
            get
            {
                switch ((int)STATUS)
                {
                    case 2: return string.Format("{0}-{1}", STATUS.ToString(), "Close");
                    case 9: return string.Format("{0}-{1}", STATUS.ToString(), "Transfer");
                    default: return string.Format("{0}-{1}", STATUS.ToString(), "Active");
                }
            }
        }

         [NotMapped]
        public bool isSelect { get; set; }

         [NotMapped]
        public bool isEnable { get; set; }
       

    }
}
