﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msPayment")]
    public class Payment
    {

        [Column(IsPrimaryKey = true)]
        public int PaymentID { get; set; }

        [Column]
        public int PaymentTypeID { get; set; }

        [Column]
        public string PaymentName { get; set; }

        [Column]
        public decimal Percentage { get; set; }

    }
}
