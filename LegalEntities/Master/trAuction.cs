﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trAuction")]
    public class trAuction
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AuctionID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AuctionOrder { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
