﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msAttorney")]
    public class Attorney
    {
        [Column(IsPrimaryKey = true)]
        public int AttorneyID { get; set; }

        [Column]
        public int AttorneyTypeID { get; set; }

        [Column]
        public string LawyerName { get; set; }
    }
}
