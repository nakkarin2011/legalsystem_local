﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{

    [Table(Name = "dbo.msRequestReason")]
    public class RequestReason
    {

        [Column(IsPrimaryKey = true)]
        public int ReasonID { get; set; }

        [Column]
        public string ReasonName { get; set; }

        [Column]
        public string ReasonDesc { get; set; }

    }
}
