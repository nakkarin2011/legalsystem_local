﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msLegalStatusMapping")]
    public class LegalStatusMapping
    {
        [Column(IsPrimaryKey = true)]
        public int LegalMapId { get; set; }

        [Column]
        public string LegalStatus { get; set; }

        [Column]
        public int ReqTypeID { get; set; }

    }
}
