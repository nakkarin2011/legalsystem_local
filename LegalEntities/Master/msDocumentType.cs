﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LegalEntities
{
    [Table("dbo.msDocumentType")]
    public class msDocumentType
    {
        [Key, Column(Order = 0)]
        public int DocTypeID { get; set; }

        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public string DocTypeName { get; set; }

        [Column]
        public string Flag { get; set; }
    }
}
