﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LegalEntities
{
    [Table("dbo.msRequestType")]
    public class msRequestType
    {
        [Key, Column(Order = 0)]
        public int ReqTypeID { get; set; }

        [Column]
        public string ReqTypeName { get; set; }

    }
}
