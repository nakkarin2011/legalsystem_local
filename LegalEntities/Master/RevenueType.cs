﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msRevenueType")]
    public class RevenueType
    {

        [Column(IsPrimaryKey = true)]
        public int RevenueTypeID { get; set; }

        [Column]
        public string RevenueTypeName { get; set; }

    }
}
