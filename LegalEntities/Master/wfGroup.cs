﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Drawing;


namespace LegalEntities
{
    [Table("dbo.wfGroup")]
    public class wfGroup
    {

        [Key,Column(Order=0)]
        public int StateID { get; set; }

        [Key,Column(Order=1)]
        public string EmpCode { get; set; }

        [Column]
        public string EmpName { get; set; }

        [Column]
        public string Email { get; set; }

        [Column]
        public Byte[] Signature { get; set; }

        //public Image SignatureImage
        //{
        //    get
        //    {
        //        MemoryStream mem = new MemoryStream(this.Signature.ToArray());
        //        return Bitmap.FromStream(mem);
        //    }
        //}

    }
}
