﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.wfStatus")]
    public class wfStatus
    {

        [Column(IsPrimaryKey = true)]
        public int StatusID { get; set; }

        [Column(IsPrimaryKey = true)]
        public int Version { get; set; }

        [Column]
        public string StatusName { get; set; }

        [Column]
        public string Flag { get; set; }

        public string StatusDisplay
        {
            get
            {
                if (Flag == "L")
                    return string.Format("{0}-{1}", StatusID.ToString().PadLeft(3, '0'), StatusName);
                else
                    return StatusName;
            }
        }

    }
}
