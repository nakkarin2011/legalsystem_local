﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msFixCost")]
    public class FixCost
    {

        [Column(IsPrimaryKey = true)]
        public int FixID { get; set; }

        [Column]
        public int TypeID { get; set; }

        [Column]
        public string SubTypeCode { get; set; }

        [Column]
        public decimal CostAmt { get; set; }

    }
}
