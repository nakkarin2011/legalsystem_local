﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msSubProductType")]
    public class SubProductType
    {

        [Column(IsPrimaryKey = true)]
        public int SubTypeID { get; set; }

        [Column]
        public int PrdTypeID { get; set; }

        [Column]
        public string SubTypeCode { get; set; }

        [Column]
        public string SubTypeName { get; set; }

        [Column]
        public bool IsFixCost { get; set; }

        [Column]
        public decimal FixCostAmt { get; set; }

    }

}
