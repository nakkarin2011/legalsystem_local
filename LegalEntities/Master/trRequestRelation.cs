﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LegalEntities
{
    [Table("dbo.trRequestRelation")]
    public class trRequestRelation
    {

        [Key,Column(Order=0)]
        public Guid RRID { get; set; }

        [Column]
        public Guid RequestID { get; set; }

        [Column]
        public decimal Account { get; set; }

        [Column]
        public decimal RelationCifNo { get; set; }

        [Column]
        public string RelationType { get; set; }

        [Column]
        public int RelationSeq { get; set; }

       

    }
}
