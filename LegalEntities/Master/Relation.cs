﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msRelation")]
    public class Relation
    {

        [Column(IsPrimaryKey = true)]
        public int RelationID { get; set; }

        [Column]
        public string RelationCode { get; set; }

        [Column]
        public string RelationName { get; set; }

        [Column]
        public int RelationOrder { get; set; }

    }
}
