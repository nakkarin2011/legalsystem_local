﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msRequestType")]
    public class RequestType
    {
        [Column(IsPrimaryKey = true)]
        public int ReqTypeID { get; set; }

        [Column]
        public string ReqTypeName { get; set; }

    }
}
