﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msCourtRate")]
    public class CourtRate
    {

        [Column(IsPrimaryKey = true)]
        public int CourtRateID { get; set; }

        [Column]
        public decimal MinAmt { get; set; }

        [Column]
        public decimal MaxAmt { get; set; }

        [Column]
        public decimal Percentage { get; set; }

        [Column]
        public decimal Devide { get; set; }

        [Column]
        public string Condition { get; set; }

        [Column]
        public decimal PayAmount { get; set; }

    }
}
