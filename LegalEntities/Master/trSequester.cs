﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trSequester")]
    public class trSequester
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int SequesterID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int SeqTime { get; set; }

        [Column]
        public DateTime? GotDeedDate { get; set; }

        [Column]
        public string Proposition { get; set; }

        [Column]
        public DateTime? SendDeedDate { get; set; }

        [Column]
        public int? ExecutionID { get; set; }

        [Column]
        public decimal Debt { get; set; }

        [Column]
        public int? CourtZoneID { get; set; }

        [Column]
        public int? SoldType { get; set; }

        [Column]
        public DateTime SeqDate { get; set; }

        [Column]
        public decimal SeqFee { get; set; }

        [Column]
        public DateTime Seq2Date { get; set; }

        [Column]
        public decimal Seq2Fee { get; set; }

        [Column]
        public string Comment { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
