﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table("dbo.msRequestStatus")]
    public class msRequestStatus
    {

              [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ReqStatusID { get; set; }

        [Column]
        public string ReqStatusName { get; set; }

        [Column]
        public string ReqStatusDesc { get; set; }

    }
}
