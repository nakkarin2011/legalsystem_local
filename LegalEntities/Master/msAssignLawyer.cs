﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table("dbo.msAssignLawyer")]
    public class msAssignLawyer
    {
        [Key, Column(Order = 0)]
        public string TH_GroupName { get; set; }

        [Column]
        public string Emp_Code { get; set; }

        [Column]
        public string TH_Name { get; set; }

        [Column]
        public string Email { get; set; }

        [Column]
        public int GroupID { get; set; }


    }
}
