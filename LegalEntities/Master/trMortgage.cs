﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trMortgage")]
    public class trMortgage
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int MortgageID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AssetID { get; set; }

        [Column]
        public DateTime MorDate { get; set; }

        [Column]
        public decimal MorFee { get; set; }

        [Column]
        public DateTime? InvestigateDate { get; set; }

        [Column]
        public DateTime FiatDate { get; set; }

        [Column]
        public DateTime? HandDate { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdatedateTime { get; set; }

    }
}
