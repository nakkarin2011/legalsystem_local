﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msCoverPageGroup")]
    public class CoverPageGroup
    {

        [Column(IsPrimaryKey = true)]
        public int CovTypeID { get; set; }

        [Column]
        public string CovTypeName { get; set; }

    }
}
