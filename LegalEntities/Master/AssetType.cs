﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{

    [Table(Name = "dbo.msAssetType")]
    public class AssetType
    {

        [Column(IsPrimaryKey = true)]
        public int AssetTypeID { get; set; }

        [Column]
        public string AssetTypeName { get; set; }

        [Column]
        public string AssetTypeDesc { get; set; }

    }
}