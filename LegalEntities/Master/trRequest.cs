﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trRequest")]
    public class trRequest
    {
        [Key, Column(Order=0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public Guid RequestID { get; set; }

        [Column]
        public string RequestNo { get; set; }

        [Column]
        //public DateTime RequestDate { get; set; }
        public Nullable<DateTime> RequestDate { get; set; }
       
        [Column]
        public string RequestType { get; set; }

        [Column]
        public string RequestStatus { get; set; }
        [NotMapped]
        public int RequestStatus_
        {
            get
            {
                if (!string.IsNullOrEmpty(RequestStatus))
                   return int.Parse(RequestStatus.PadRight(10,' ').Substring(1, 3).Trim());
                return 0;
            }
        }

        [Column]
        //public DateTime RequestStatusDate { get; set; }
        public Nullable<DateTime> RequestStatusDate { get; set; }

        [Column]
        public string RequestReason { get; set; }

        [Column]
        public int DefendantNo { get; set; }

        [Column]
        public string RefRequestNo { get; set; }

        [Column]
        public string RefRequestType { get; set; }

        [Column]
        //public DateTime RefRequestDate { get; set; }
        public Nullable<DateTime> RefRequestDate { get; set; }

        [Column]
        public string RefRequestReason { get; set; }

        [Column]
        public Guid? LegalID { get; set; }

    

        [Column]
        public string LegalStatus { get; set; }

        [Column]
        //public DateTime LegalStatusDate { get; set; }
        public Nullable<DateTime> LegalStatusDate { get; set; }
        [Column]
        public string LawyerAttorneyID { get; set; }

        [Column]
        public string LawyerID { get; set; }

        [Column]
        public Nullable<DateTime> LawyerIDReceive { get; set; }

        [Column]
        public Nullable<DateTime> LawyerIDApprove { get; set; }

        [Column]
        public int LawyerStatus { get; set; }

        [Column]
        //public DateTime LegalStatusDate { get; set; }
        public Nullable<DateTime> LawyerStatusDate { get; set; }

        [Column]
        public decimal CifNo { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string Owner { get; set; }

        [Column]
        public Nullable<DateTime> OwnerApprove { get; set; }

        [Column]
        public string Attachments { get; set; }
        [Column]
        public string ColChecker { get; set; }
        [Column]
        public Nullable<DateTime> ColCheckerApprove { get; set; }

        [Column]
        public string ColHead { get; set; }
        
        [Column]
        public Nullable<DateTime> ColHeadApprove { get; set; }

        [Column]
        public string LegalSpecial { get; set; }

        [Column]
        public Nullable<DateTime> LegalSpecialApprove { get; set; }

        [Column]
        public string SeniorLawyer { get; set; }
       
        [Column]
        public Nullable<DateTime> SeniorLawyerReceive { get; set; }

        [Column]
        public Nullable<DateTime> SeniorLawyerApprove { get; set; }


        [Column]
        public string CreateBy { get; set; }

        [Column]
        //public DateTime CreateDate { get; set; }
        public Nullable<DateTime> CreateDate { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        //public DateTime UpdateDate { get; set; }
        public Nullable<DateTime> UpdateDate { get; set; }

        [Column]
        public string Comment { get; set; }

        public int RNo
        {
            get
            {
                return Convert.ToInt32(RequestNo.Substring(7, 5));
            }
        }
        [Column]
        public DateTime? RefRequestDateCancelCase { get; set; }
        [Column]
        public int? DocTypeID { get; set; }

        [Column]
        public DateTime? EmailSendDate { get; set; }

        [Column]
        public string EmailSendStatus { get; set; }
        [Column]
        public DateTime? LegalNoRegister { get; set; }
        [Column]
        public string ActiveTab { get; set; }
        [Column]
        public DateTime? EmailOutstandingSendDate { get; set; }

        [Column]
        public string EmailOutstandingSendStatus { get; set; }

        [Column]
        public Nullable<DateTime> CaseDate { get; set; }
    }
}
