﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msAttorneyType")]
    public class AttorneyType
    {

        [Column(IsPrimaryKey = true)]
        public int AttorneyTypeID { get; set; }

        [Column]
        public string AttorneyTypeName { get; set; }

    }
}
