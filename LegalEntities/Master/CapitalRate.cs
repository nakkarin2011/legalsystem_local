﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msCapitalRate")]
    public class CapitalRate
    {

        [Column(IsPrimaryKey = true)]
        public int CapitalRateID { get; set; }

        [Column]
        public decimal MinWage { get; set; }

        [Column]
        public decimal MaxWage { get; set; }

        [Column]
        public decimal Percentage { get; set; }

        [Column]
        public string Condition { get; set; }

        [Column]
        public decimal PayAmount { get; set; }

    }
}
