﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
namespace LegalEntities
{

    [Table(Name = "dbo.msSoldType")]
    public class SoldType
    {

        [Column(IsPrimaryKey = true)]
        public int SoldID { get; set; }

        [Column]
        public string SoldName { get; set; }

    }
}

