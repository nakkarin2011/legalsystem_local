﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.msAttorney")]
    public class msAttorney
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)] 
        public int AttorneyID { get; set; }

        [Column]
        public int AttorneyTypeID { get; set; }

        [Column]
        public string LawyerName { get; set; }
        [Column]
        public string AttorneyMail { get; set; }

    }
}
