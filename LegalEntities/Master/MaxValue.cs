﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msMaxValue")]
    public class MaxValue
    {

        [Column(IsPrimaryKey = true)]
        public int MaxID { get; set; }

        [Column]
        public string MaxName { get; set; }

        [Column]
        public decimal MaxAmt { get; set; }

    }
}
