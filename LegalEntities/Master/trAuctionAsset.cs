﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trAuctionAsset")]
    public class trAuctionAsset
    {

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AuctionID { get; set; }

        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AssetID { get; set; }

        [Column]
        public DateTime? AnotherBuyDate { get; set; }

        [Column]
        public decimal AnotherBuyAmt { get; set; }

        [Column]
        public DateTime? BankBuyDate { get; set; }

        [Column]
        public decimal BankBuyAmt { get; set; }

        [Column]
        public DateTime? GotMoneyDate { get; set; }

        [Column]
        public decimal AccountPerAmt { get; set; }

        [Column]
        public decimal Price { get; set; }

        [Column]
        public decimal PriceOnBoard { get; set; }

        [Column]
        public decimal PriceOnBoardSold { get; set; }

        [Column]
        public string Remark { get; set; }

        [Column]
        public string Flag { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
