﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.msCourt")]
    public class Court
    {

        [Column(IsPrimaryKey = true)]
        public int CourtID { get; set; }

        [Column]
        public string CourtName { get; set; }

    }
}
