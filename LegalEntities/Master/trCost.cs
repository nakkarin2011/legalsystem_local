﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trCost")]
    public class trCost
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int CostID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int CostTypeID { get; set; }

        [Column]
        public int AttorneyTypeID { get; set; }

        [Column]
        public DateTime? CostDate { get; set; }

        [Column]
        public decimal CostAmt { get; set; }

        [Column]
        public string CostDesc { get; set; }

        [Column]
        public DateTime? PayDate { get; set; }

        [Column]
        public decimal PayAmt { get; set; }

        [Column]
        public decimal Income { get; set; }

        [Column]
        public DateTime? ReceiveDate { get; set; }

        [Column]
        public string AdviseNo { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
