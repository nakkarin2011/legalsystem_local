﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.wfDocument")]
    public class wfDocument
    {

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] 
        public Guid wfDocID { get; set; }

        [Column]
        public Guid? LegalID { get; set; }

        [Column]
        public int DocTypeID { get; set; }

        [Column]
        public int Order { get; set; }

        [Column]
        public string DocTypeDesc { get; set; }

        [Column]
        public int DocStatus { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDate { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDate { get; set; }

        [Column]
        public string IsCurrent { get; set; }

    }
}
