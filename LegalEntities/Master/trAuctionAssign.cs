﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trAuctionAssign")]
    public class trAuctionAssign
    {

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int AuctionID { get; set; }

        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int ActionTime { get; set; }

        [Column]
        public DateTime AuctionDate { get; set; }

        [Column]
        public string Remark { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
