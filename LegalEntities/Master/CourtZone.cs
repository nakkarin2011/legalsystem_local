﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msCourtZone")]
    public class CourtZone
    {

        [Column(IsPrimaryKey = true)]
        public int CourtZoneID { get; set; }

        [Column]
        public string CourtZoneName { get; set; }

        [Column]
        public string CourtZoneDesc { get; set; }

        [Column]
        public int Days { get; set; }

        [Column]
        public decimal CrtFeeByZone { get; set; }

    }
}
