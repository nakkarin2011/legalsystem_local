﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.msPlaintiff")]
    public class Plaintiff
    {

        [Column(IsPrimaryKey = true)]
        public int PlaintiffID { get; set; }

        [Column]
        public string PID { get; set; }

        [Column]
        public string PName { get; set; }

        [Column]
        public string PAddress { get; set; }

        [Column]
        public string PCusType { get; set; }

    }
}
