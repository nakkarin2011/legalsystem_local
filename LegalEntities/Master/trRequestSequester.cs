﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.trRequestSequester")]
    public class trRequestSequester
    {

        [Key, Column]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // is not auto generate Key
        public int ReqSeqID { get; set; }

        [Column]
        public int ReqSeqNo { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public DateTime ReqSeqDate { get; set; }

        [Column]
        public decimal ReqSeqAmt { get; set; }

        [Column]
        public string CreateBy { get; set; }

        [Column]
        public DateTime CreateDateTime { get; set; }

        [Column]
        public string UpdateBy { get; set; }

        [Column]
        public DateTime UpdateDateTime { get; set; }

    }
}
