﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vEmpLegal")]
    public class vEmpLegal
    {
        [Key, Column(Order = 0)]
        public int ID { get; set; }
        [Column]
        public string TH_GroupName { get; set; }

        [Column]
        public string Emp_Code { get; set; }

        [Column]
        public string TH_Name { get; set; }

        [Column]
        public string Email { get; set; }
        
        [Column]
        public int GroupID { get; set; }


    }
}
