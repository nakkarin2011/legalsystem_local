﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vAverage")]
    public class vAverage
    {

        [Key, Column]
        public int AverageID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AssetID { get; set; }

        [Column]
        public string AssetDesc { get; set; }

        [Column]
        public int PursueID { get; set; }

        [Column]
        public DateTime AvrDate { get; set; }

        [Column]
        public decimal AvrFee { get; set; }

        [Column]
        public DateTime? InvestigateDate { get; set; }

        [Column]
        public DateTime FiatDate { get; set; }

        [Column]
        public DateTime? HandDate { get; set; }

        [Column]
        public DateTime? ReceiveDate { get; set; }

        [Column]
        public decimal ReceiveAmt { get; set; }

    }
}
