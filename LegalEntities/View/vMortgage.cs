﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vMortgage")]
    public class vMortgage
    {

        [Key,Column]
        public int MortgageID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AssetID { get; set; }

        [Column]
        public string AssetDesc { get; set; }

        [Column]
        public int PursueID { get; set; }

        [Column]
        public DateTime MorDate { get; set; }

        [Column]
        public decimal MorFee { get; set; }

        [Column]
        public DateTime? InvestigateDate { get; set; }

        [Column]
        public DateTime FiatDate { get; set; }

        [Column]
        public DateTime? HandDate { get; set; }

    }
}
