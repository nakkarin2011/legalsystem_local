﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vRequestTypeMapLegalStstus")]
    public class vRequestTypeMapLegalStstus
    {
       [Key, Column(Order = 0)]
        public int RowID { get; set; }
        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public string ReqTypeName { get; set; }

        [Column]
        public int LegalStatus { get; set; }

        [Column]
        public string LegalStatusName { get; set; }

    }
}
