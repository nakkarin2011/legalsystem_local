﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LegalEntities
{
    [Table("dbo.vAuctionAsset")]
    public class vAuctionAsset
    {

        [Key, Column(Order = 0)]
        public int AuctionID { get; set; }

        [Key, Column(Order = 1)]
        public int AssetID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AuctionOrder { get; set; }

        [Column]
        public int AssetTypeID { get; set; }

        [Column]
        public string AssetTypeName { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string District { get; set; }

        [Column]
        public string Province { get; set; }

        [Column]
        public string ZipCode { get; set; }

        [Column]
        public DateTime? AnotherBuyDate { get; set; }

        [Column]
        public decimal AnotherBuyAmt { get; set; }

        [Column]
        public DateTime? BankBuyDate { get; set; }

        [Column]
        public decimal BankBuyAmt { get; set; }

        [Column]
        public DateTime? GotMoneyDate { get; set; }

        [Column]
        public decimal AccountPerAmt { get; set; }

        [Column]
        public decimal Price { get; set; }

        [Column]
        public decimal PriceOnBoard { get; set; }

        [Column]
        public decimal PriceOnBoardSold { get; set; }

        [Column]
        public string Remark { get; set; }

        [Column]
        public string Flag { get; set; }

    }
}
