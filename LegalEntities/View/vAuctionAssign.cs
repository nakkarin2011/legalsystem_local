﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vAuctionAssign")]
    public class vAuctionAssign
    {

        [Key, Column(Order=0)]
        public int AuctionID { get; set; }

        [Key, Column(Order=1)]
        public int ActionTime { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AuctionOrder { get; set; }

        [Column]
        public DateTime AuctionDate { get; set; }

        [Column]
        public string Remark { get; set; }

    }
}
