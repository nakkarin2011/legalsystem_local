﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.vCostType")]
    public class vCostType
    {

        [Column]
        public int CostTypeID { get; set; }

        [Column]
        public int AttorneyTypeID { get; set; }

        [Column]
        public string AttorneyTypeName { get; set; }

        [Column]
        public string CostTypeName { get; set; }

        [Column]
        public string TFlag { get; set; }

        [Column]
        public string SFlag { get; set; }

    }
}
