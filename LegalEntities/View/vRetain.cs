﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vRetain")]
    public class vRetain
    {

        [Key,Column]
        public int RetainID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int AssetID { get; set; }

        [Column]
        public string AssetDesc { get; set; }

        [Column]
        public int PursueID { get; set; }

        [Column]
        public DateTime RetDate { get; set; }

        [Column]
        public decimal RetFee { get; set; }

        [Column]
        public DateTime? InvestigateDate { get; set; }

        [Column]
        public DateTime FiatDate { get; set; }

        [Column]
        public DateTime? HandDate { get; set; }

        [Column]
        public DateTime? ReceiveDate { get; set; }

        [Column]
        public decimal ReceiveAmt { get; set; }

    }
}
