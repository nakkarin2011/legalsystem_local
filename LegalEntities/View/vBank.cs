﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.vBank")]
    public class vBank
    {

        [Column]
        public string Code { get; set; }

        [Column]
        public string BankName { get; set; }

    }
}
