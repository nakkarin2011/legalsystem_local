﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.vCoverpage")]
    public class vCoverpage
    {

        [Column]
        public int CovTypeID { get; set; }

        [Column]
        public int DocTypeID { get; set; }

        [Column]
        public string DocTypeName { get; set; }

        [Column]
        public string DocTypeName_Begin { get; set; }

        [Column]
        public string CoverpageText { get; set; }

        [Column]
        public bool OriginalCopy { get; set; }

        [Column]
        public bool NumRound { get; set; }

        [Column]
        public bool NumEdition { get; set; }

        [Column]
        public bool NumOrder { get; set; }

        [Column]
        public bool NumDeed { get; set; }

        [Column]
        public bool NumPlot { get; set; }

        [Column]
        public bool NumSuite { get; set; }

        [Column]
        public bool TypeDeed { get; set; }

        [Column]
        public bool Number { get; set; }

        [Column]
        public bool DateAs { get; set; }

        [Column]
        public bool DateAsCount { get; set; }

        [Column]
        public bool DeedNo { get; set; }

        [Column]
        public bool FreeText { get; set; }

        [Column]
        public bool WithOriginalContract { get; set; }

        [Column]
        public bool AddSecuritiesIncreasing { get; set; }

        [Column]
        public bool Particular { get; set; }

        [Column]
        public bool MortgageeIssue { get; set; }

        [Column]
        public bool IsSelect { get; set; }

    }
}
