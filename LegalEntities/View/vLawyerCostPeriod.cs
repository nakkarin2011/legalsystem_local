﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.vLawyerCostPeriod")]
    public class vLawyerCostPeriod
    {

        [Column(IsPrimaryKey = true)]
        public int PaymentID { get; set; }

        [Column]
        public int PaymentTypeID { get; set; }

        [Column]
        public string PaymentTypeName { get; set; }

        [Column]
        public string PaymentName { get; set; }

        [Column]
        public decimal Percentage { get; set; }

    }
}
