﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vFee")]
    public class vFee
    {

        [Key,Column]
        public int FeeID { get; set; }

        [Column]
        public string LegalNo { get; set; }

        [Column]
        public int CostTypeID { get; set; }

        [Column]
        public string CostTypeName { get; set; }

        [Column]
        public string TFlag { get; set; }

        [Column]
        public string SFlag { get; set; }

        [Column]
        public decimal CourtPayAmt { get; set; }

        [Column]
        public decimal CourtRefundAmt { get; set; }

        [Column]
        public DateTime? CourtRefundDate { get; set; }

        [Column]
        public string BankCode { get; set; }

        [Column]
        public string BankName { get; set; }

        [Column]
        public DateTime? ChequeDate { get; set; }

        [Column]
        public string ChequeNo { get; set; }

        [Column]
        public string BankBranch { get; set; }

        [Column]
        public DateTime? SendChqDate { get; set; }

    }
}
