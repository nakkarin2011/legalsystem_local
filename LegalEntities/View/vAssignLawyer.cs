﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vAssignLawyer")]
    public class vAssignLawyer
    {
        [Key, Column]
        public Guid RequestID { get; set; }
        [Column]
        public string RequestNo { get; set; }
        [Column]
        public string RequestType { get; set; }
        [Column]
        public string ReqTypeName { get; set; }
        [Column]
        public decimal CifNo { get; set; }
        [Column]
        public int DefendantNo { get; set; }
        [Column]
        public string CFNAME { get; set; }
        [Column]
        public string Address { get; set; }
        [Column]
        public DateTime? RequestDate { get; set; }
        [Column]
        public string LegalNo { get; set; }
        [Column]
        public Guid? LegalID { get; set; }
        [Column]
        public string LegalStatus { get; set; }
        [Column]
        public DateTime? LegalStatusDate { get; set; }
        [Column]
        public string LegalStatusName { get; set; }
        [Column]
        public string SeniorLawyer { get; set; }
  
        [Column]
        public string LawyerID { get; set; }
        [Column]
        public string LawyerName { get; set; }

        [Column]
        public string AttorneyID { get; set; }
        [Column]
        public string AttorneyName { get; set; }
        [Column]
        public int LawyerStatus { get; set; }
        [Column]
        public string LawyerStatusName { get; set; }
        [Column]
        public DateTime? LawyerStatusDate { get; set; }

        [Column]
        public int? AttorneyTypeID { get; set; }
        [Column]
        public string AttorneyTypeName { get; set; }
        [Column]
        public string Account { get; set; }
    }
}
