﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vDocumentMapping")]
    public class vDocumentMapping
    {

        [Key, Column(Order = 0)]
        public int DocID { get; set; }

        [Key, Column(Order = 1)]
        public string LegalNo { get; set; }

        [Column]
        public int DocTypeID { get; set; }

        [Column]
        public string DocTypeName { get; set; }

        [Column]
        public string DocName { get; set; }

        [Column]
        public string DocPath { get; set; }

        [Column]
        public string Flag { get; set; }

        [Column]
        public int? FID { get; set; }

        [Column]
        public int? PursueTime { get; set; }

        [Column]
        public int? AuctionOrder { get; set; }

    }

}
