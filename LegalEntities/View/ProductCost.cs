﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Table(Name = "dbo.ProductCost")]
    public class ProductCost
    {

        [Column]
        public int PrdTypeID { get; set; }

        [Column]
        public string PrdTypeName { get; set; }

        [Column]
        public string SubTypeCode { get; set; }

        [Column]
        public string SubTypeName { get; set; }

        [Column]
        public int IsFixCost { get; set; }

        [Column]
        public decimal CostAmt { get; set; }

    }
}
