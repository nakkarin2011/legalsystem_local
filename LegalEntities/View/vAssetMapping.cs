﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{

    [Table("dbo.vAssetMapping")]
    public class vAssetMapping
    {

        [Key, Column(Order = 0)]
        public int AssetID { get; set; }

        [Key, Column(Order = 1)]
        public int AssetTypeID { get; set; }

        [Column]
        public string AssetTypeName { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string District { get; set; }

        [Column]
        public string Province { get; set; }

        [Column]
        public string ZipCode { get; set; }

        [Column]
        public int PursueID { get; set; }

        [Column]
        public int PursueTime { get; set; }

        [Column]
        public string LegalNo_pur { get; set; }

        [Column]
        public int? SequesterID { get; set; }

        [Column]
        public string LegalNo_seq { get; set; }

        [Column]
        public int? SeqTime { get; set; }

    }
}
