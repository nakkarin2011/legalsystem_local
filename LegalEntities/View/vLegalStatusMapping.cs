﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.vLegalStatusMapping")]
    public class vLegalStatusMapping
    {
        [Column]
        public string LegalStatus { get; set; }

        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public string LegalStatusName { get; set; }

        [Column]
        public string ReqTypeName { get; set; }
        [Column]
        public int LegalMapId { get; set; }


    }
}
