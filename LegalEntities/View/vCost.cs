﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
        [Table("dbo.vCost")]
        public class vCost
        {

            [Key, Column]
            public int CostID { get; set; }

            [Column]
            public string LegalNo { get; set; }

            [Column]
            public int CostTypeID { get; set; }

            [Column]
            public int AttorneyTypeID { get; set; }

            [Column]
            public DateTime? CostDate { get; set; }

            [Column]
            public decimal CostAmt { get; set; }

            [Column]
            public string CostDesc { get; set; }

            [Column]
            public DateTime? PayDate { get; set; }

            [Column]
            public decimal PayAmt { get; set; }

            [Column]
            public decimal Income { get; set; }

            [Column]
            public DateTime? ReceiveDate { get; set; }

            [Column]
            public string AdviseNo { get; set; }

            [Column]
            public string AttorneyTypeName { get; set; }

            [Column]
            public string CostTypeName { get; set; }

            [Column]
            public string TFlag { get; set; }

            [Column]
            public string SFlag { get; set; }

        }
}
