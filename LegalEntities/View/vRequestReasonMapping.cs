﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.vRequestReasonMapping")]
    public class vRequestReasonMapping
    {
        [Column]
        public string ReasonID { get; set; }

        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public string ReqTypeName { get; set; }

        [Column]
        public string ReasonName { get; set; }

    }
}
