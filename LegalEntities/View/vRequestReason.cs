﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LegalEntities
{
    [Table("dbo.vRequestReason")]
    public class vRequestReason
    {
        [Key, Column]
        public int RowID { get; set; }
        [Column]
        public int ReqTypeID { get; set; }

        [Column]
        public int ReasonID { get; set; }

        [Column]
        public string ReasonName { get; set; }

        public string ReasonDisplay { get { return (ReasonID != 0) ? string.Format("{0} - {1}", ReasonID.ToString().PadLeft(3, '0'), ReasonName) : ReasonName; } }

    }
}
