﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LegalEntities
{
    [Table(Name = "dbo.vLawyerOA_IA")]
    public class vLawyerOAIA
    {

        [Column]
        public int AttorneyID { get; set; }

        [Column]
        public int AttorneyTypeID { get; set; }

        [Column]
        public string LawyerName { get; set; }

        [Column]
        public string AttorneyTypeName { get; set; }
        [Column]
        public int MapID { get; set; }
    }
}
