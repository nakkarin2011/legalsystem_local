﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    public class sp_GetWorkList
    {
        public long ID { get; set; }
        public string ACCTNO { get; set; }
        public string ACTYPE { get; set; }
        public decimal? BRN { get; set; }
        public decimal? STATUS { get; set; }
        public decimal? PDDAYS { get; set; }
        public decimal? A_STATUS { get; set; }
        public string A_CFCIFN { get; set; }
        public string A_CFNAME { get; set; }
        public string A_CFSSNO { get; set; }
        public string A_CFRELA { get; set; }
        public int? RelationOrder { get; set; }
        public string RelationName { get; set; }
        public string TYPE { get; set; }
        public string RequestNo { get; set; }
        public string LegalNo { get; set; }
        public string LegalStatus { get; set; }
        public DateTime? LegalStatusDate { get; set; }

    }
}
