﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    public class sp_GetDataLnmast
    {
        public decimal CIFNO { get; set; }
        public string CFNAME { get; set; }
        [Key]
        public decimal ACCTNO { get; set; }
        public string ACTYPE { get; set; }
        public decimal BRN { get; set; }
        public string ACNAME { get; set; }
        public string FCODE { get; set; }
        public decimal FSEQ { get; set; }
        public string TYPE { get; set; }
        public decimal STATUS { get; set; }
        //public string _STATUS
        //{
        //    get
        //    {
        //        switch ((int)STATUS)
        //        {
        //            case 2: return string.Format("{0}-{1}", STATUS.ToString(), "Close");
        //            case 9: return string.Format("{0}-{1}", STATUS.ToString(), "Transfer");
        //            default: return string.Format("{0}-{1}", STATUS.ToString(), "Active");
        //        }
        //    }
        //}

        public decimal LMCOST { get; set; }
        public decimal LMPRDC { get; set; }
        public string TDRCNT { get; set; }
        public DateTime ORGDT { get; set; }
        public decimal ORGAMT { get; set; }
        public decimal CBAL { get; set; }
        public decimal INTRATE { get; set; }
        public decimal ACCINT { get; set; }
        public decimal ACCI3 { get; set; }
        public decimal PENIN3 { get; set; }
        public decimal PENINT { get; set; }
        public decimal PDDAYS { get; set; }
        public string CFADDR { get; set; }
        public string CollateralDesc { get; set; }
        public string STAGE { get; set; }
        //public bool isSelect { get; set; }
        //public bool isEnable { get; set; }
    }
}
