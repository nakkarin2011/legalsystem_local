﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    public class sp_GetRelationship
    {
        public decimal A_ACCTNO { get; set; }
        public decimal A_CFCIFN { get; set; }
        public string A_CFNAME { get; set; }
        public string A_CFSSNO { get; set; }
        public string A_CFRELA { get; set; }
        public string RelationName { get; set; }
        public int RelationOrder { get; set; }
        public string ACC_NO { get; set; }
        public decimal CUR_BAL01 { get; set; }
        public bool IsSelect { get; set; }
        public bool IsEnable { get; set; }
    }
}
