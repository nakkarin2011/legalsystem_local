﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalEntities
{
    [Serializable]
    public class sp_GetRequestInbox
    {
        [Key]
        public Guid RequestID { get; set; }
        public string RequestNo { get; set; }
        public DateTime RequestDate { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeName { get; set; }
        public string RequestStatus { get; set; }
        public string RequestStatusName { get; set; }
        public DateTime RequestStatusDate { get; set; }
        public string RequestReason { get; set; }
        public string RequestReasonName { get; set; }
        public int DefendantNo { get; set; }
        public string RefRequestNo { get; set; }
        public string RefRequestType { get; set; }
        public DateTime? RefRequestDate { get; set; }
        public string RefRequestReason { get; set; }
        public string LegalNo { get; set; }
        public string LegalStatus { get; set; }
        public string LegalStatusName { get; set; }
        public DateTime? LegalStatusDate { get; set; }
        public decimal CifNo { get; set; }
        public string CFNAME { get; set; }
        public string Address { get; set; }
        public string Owner { get; set; }
        public string ColHead { get; set; }
        public string ColChecker { get; set; }
        public DateTime? ColCheckerApprove { get; set; }
        public string SeniorLawyer { get; set; }
        public DateTime? SeniorLawyerReceive { get; set; }
        public string ApproverLaw_Name { get; set; }
        public string ApproverLaw_Email { get; set; }
        public string LawyerID { get; set; }
        public DateTime? LawyerIDReceive { get; set; }
        public string Attachments { get; set; }
        public bool? IsGuarantee { get; set; }
        public string SendDocDate { get; set; }
        public string StatusReqOper { get; set; }

    }
}

