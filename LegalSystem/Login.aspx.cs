﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalSystem.Common;
using TCRB.Securities;

namespace LegalSystem
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Session.Clear();
            
            if (!IsPostBack)
            {
                InitailJavascript();
                txtUserName.Focus();
            }
        }

        protected void InitailJavascript()
        {
            //txtUserName.Attributes.Add("onkeypress", "Button_Enter(this,'" + this.btnLogin.ClientID + "');");
            //txtPassword.Attributes.Add("onkeypress", "Button_Enter(this,'" + this.btnLogin.ClientID + "');");
        }

        private void ValidateUI()
        {
            if (string.IsNullOrEmpty(txtUserName.Value) || string.IsNullOrEmpty(txtPassword.Value))
                throw new Exception("Incorrect User/Password, Please login again.");
                //throw new Exception("ชื่อผู้ใช้/รหัสผ่านไม่ถูกต้อง กรุณาเข้าสู่ระบบอีกครั้ง");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                PanelError.Visible = false;
                //ValidateUI();
                //Pratanporn
                UserLogin ul = new UserLogin();
                //UserIdentity user = ul.Login("-@Fource@-", "training01"); 
                //UserIdentity user = ul.Login(txtUserName.Value, txtPassword.Value);
                UserIdentity user = ul.Login("-@Fource@-", txtUserName.Value);
                if (user != null)
                {
                    GlobalSession.UserLogin = user;
                    GlobalSession.Permission = user.Permision;
                    GlobalSession.UserRole = 11;//Developเท่านั้น

                    //ADD .b 23/08/2019
                    Session.Add("UserLogin", user);
                    UserIdentity UserLogin = (UserIdentity)Session["UserLogin"];
                    //End 
                    Response.Redirect("Pages/InboxPage.aspx");
                }
                else
                {
                    PanelError.Visible = true;
                    lblError.Text = "คุณไม่มีสิทธิ เข้าใช้งานระบบ";
                }                                               
            }
            catch (Exception ex)
            {
                PanelError.Visible = true;
                lblError.Text = ex.Message;
            }
        }
    }
}