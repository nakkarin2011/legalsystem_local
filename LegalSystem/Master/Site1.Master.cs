﻿using LegalSystem.Common;
using LegalSystem.CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TCRB.Securities;

namespace LegalSystem.Master
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        AlertMessage Alert = new AlertMessage();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //*** Add .b 23/08/2019 ***//

                    if (Session["UserLogin"] == null)
                    {
                        Response.Redirect(MasterPageHelper.RedirectLoginPage);
                    }
                    else
                    {
                        UserIdentity UserLogin = (UserIdentity)Session["UserLogin"];

                        lblUserProfile.Text = UserLogin.TH_Name;
                        //lblUserProfile.Text = string.Format(lblUserProfile.Text, GlobalSession.UserLogin.TH_Name);  //*** OLD ***//
                        InitPermission();
                    }
                }
                catch (Exception ex)
                {
                    Alert.MsgReturn(ex.Message.ToString(), Page, this,MasterPageHelper.RedirectLoginPage,MessageEventType.error);
                }
            }
        }

        private void InitPermission()
        {
            #region //*** OLD ***//
            //if (GlobalSession.GetPermission == GlobalSession.PermissionID.SeniorLawyer)
            //{
            //    liCreateRequest.Visible = false;
            //    liInbox.Visible = false;
            //    liReqDoc.Visible = false;
            //    liReqOS.Visible = false;
            //}
            //else if (GlobalSession.GetPermission == GlobalSession.PermissionID.Lawyer)
            //{
            //    liAssign.Visible = false;
            //    liPoolInbox.Visible = false;
            //}
            //else
            //{
            //    if(GlobalSession.GetPermission == GlobalSession.PermissionID.CollectionHead)
            //    {
            //        liCreateRequest.Visible = false;
            //    }
            //    else
            //    {
            //        liPoolInbox.Visible = false;
            //        liInbox.Visible = false;
            //    }
            //    liAssign.Visible = false;
            //    liworklist.Visible = false;
            //    liRequestOper.Visible = false;
            //    liTray.Visible = false;
            //    liReports.Visible = false;
            //    liSetting.Visible = false;
            //}
            #endregion 

            switch (GlobalSession.GetPermission)
            {
                case GlobalSession.PermissionID.CollectionChecker: //** Checker
                    liworklist.Visible = true;
                    liCreateRequest.Visible = true;
                    liPoolInbox.Visible = false;
                    liAssign.Visible = false;
                    liRequestOper.Visible = false;
                    liTray.Visible = false;
                    liReports.Visible = false;
                    liSetting.Visible = false;
                    break;
                case GlobalSession.PermissionID.CollectionMaker:
                    liPoolInbox.Visible = false;
                    //liInbox.Visible = false;
                    liAssign.Visible = false;
                    liRequestOper.Visible = false;
                    liTray.Visible = false;
                    liReports.Visible = false;
                    liSetting.Visible = false;
                    break;
                case GlobalSession.PermissionID.CollectionHead:
                    liCreateRequest.Visible = false;
                    liAssign.Visible = false;
                    liRequestOper.Visible = false;
                    liTray.Visible = false;
                    liReports.Visible = false;
                    liSetting.Visible = false;
                    liPoolInbox.Visible = false;
                    break;                
                case GlobalSession.PermissionID.LegalSpecialist:
                    liCreateRequest.Visible = false;
                    liAssign.Visible = false;
                    liTray.Visible = false;
                    break;
                case GlobalSession.PermissionID.SeniorLawyer:
                    liCreateRequest.Visible = false;
                    liRequestOper.Visible = true;
                    liAssign.Visible = true;
                    liReqOS.Visible = false;
                    liReqOutsource.Visible = false;
                    // liInquiryRequest.Visible = false;
                    break;
                case GlobalSession.PermissionID.Lawyer:
                    liCreateRequest.Visible = false;
                    liPoolInbox.Visible = false;
                    liAssign.Visible = false;
                    liReqDoc.Visible = false;
                    liRequestOper.Visible = true;
                    break;
            }
            liReports.Visible = false;
        }
    }
}