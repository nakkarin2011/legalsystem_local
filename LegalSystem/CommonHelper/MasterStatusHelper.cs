﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LegalSystem.CommonHelper
{
    public class MasterStatusHelper
    {
        public static readonly string[] StatusTypeID_SL = { "2", "7" };
        //RequestStatus
        public const string StatusDraft = "F1";
        public const string StatusWaitingforApproved = "F2";
        public const string StatusApproved = "F3";
        public const string StatusRegistered = "F4";
        public const string StatusAssigned = "F5";
        public const string StatusApprovedtoLawyer = "F6";
        public const string StatusAccepted = "F7";
        public const string StatusReturn = "R1";
        public const string StatusReturntoSeniorLawyer = "R6";

        public const string StatusRejected = "5";
        public const string StatusRejectByLawyer = "C5";
        //LegalStatus
        public const string LegalStatus101 = "101";
        public const string LegalStatus100 = "100";//ส่งเรื่องให้ฝ่ายกฎหมายดำเนินการ
    }

    public class MasterReasonHelper
    {
        public const string ReasonCloseAccount = "ปิดบัญชี";
        public const string ReasonReferCaseAsOfDate = "ชะลอดำเนินคดี";
        public const string ReasonReferCaseASOfDateCancel = "ยกเลิกชะลอดำเนินคดี";
        public const string ReasonLitigationsCancel = "ยกเลิกดำเนินคดี";
        public const string ReasonWithdrawtheCase = "ถอนฟ้อง";
        public const string PleaseSelectTH = "- กรุณาเลือก -";
        public const string PleaseSelectENG = "- Please Select -";
        public const string ReasonCICReasonHave = "มี";
        public const string ReasonCICReasonDontsHave = "ไม่มี";
        public static List<CICReasonModel> BindCICReason<T1, T2>(DropDownList ddlCIC)
        {
            var listitem = new List<CICReasonModel>();
            listitem.Add(new CICReasonModel
            {
                Value = "0",
                Text = MasterReasonHelper.PleaseSelectTH
            });
            listitem.Add(new CICReasonModel
            {
                Value = "1",
                Text = MasterReasonHelper.ReasonCICReasonHave
            });
            listitem.Add(new CICReasonModel
            {
                Value = "2",
                Text = MasterReasonHelper.ReasonCICReasonDontsHave
            });
            ddlCIC.DataSource = listitem;
            ddlCIC.DataValueField = "Value";
            ddlCIC.DataTextField = "Text";
            ddlCIC.DataBind();
            return listitem;
        }
    }
    

    public class MasterButtonHelper
    {
        public const string Submitted = "Submitted";
        public const string Register = "Register";
        public const string Accept = "Accept";
        public const string Approve = "Approved";
        public const string btnDocument = "btnDocument";
        public const string GetTask = "GetTask";

    }
    public class MasterECommandNameHelper
    {
        public const string Select = "Select";
        public const string View = "View";
        public const string Edit = "Edit";
        public const string GetTask = "GetTask";
        public const string Accept = "Accept";
        public const string Page = "Page";
    }

    public class MasterAttributesHelper
    {
        public const string disabled = "disabled";
        public const string enable = "enable";
        public const string Checked = "checked";
        public const string Readonly = "readonly";
    }
    public class MasterRequestHelper
    {
        public const string RequestNoCOL = "COL";
        public const string RequestNoRefer = "R";
    }

    public class DropDownListHelper
    {
        public static bool ddlSelectTypeContains(DropDownList ddlSelectType, string Str)
        {
            if (ddlSelectType.SelectedItem.Text.Replace("การ", "").Contains(Str))
                return true;
            else
                return false;
        }
    }
    public class CICReasonModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class MasterRequestReasonHelper
    {
        public const int LitigationsCancelReason = 5;
        public const int ReasonWithdrawal = 6;
        public const int ExecutionsReason = 7;
        public const int CancelExecutionReason = 8;
        public const int ExecutiondelayReason = 9;
        public const int CancelExecutiondelayReason = 10;
        public const int AuctionDelayReason = 11;
        public const int AuctionDelayReasonToDate = 12;
      
    }
}