﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.CommonHelper
{
    public class MasterPageHelper
    {
        public const string RedirectLoginPage = "/LegalSystem_DEV/Login.aspx";

        public const string InboxPage   = "InboxPage.aspx";
        public const string InquiryPage = "InquiryPage.aspx";
        public const string RequestPage = "RequestPage.aspx";
        public const string CoverPage   = "CoverPage.aspx";
        public const string AssignPage  = "AssignPage.aspx";
        public const string InboxPoolPage = "InboxPoolPage.aspx";
    }

    public class MasterPageTitleHelper
    {
        public const string InnerReferRequest   = "Create Refer-Request";
        public const string InnerInquiryRequest = "Inquiry Request";
        public const string InnerRequest        = "Request";
        public const string InnerRegister       = "Register";
        public const string InnerRequestCusToOper  = "Request Customer Outstanding to Operation";
        public const string InnerRequestDocToOper  = "Request Customer Document to Operation";
        public const string InnerRequestDocToOutsource = "Request Customer Document to Outsource";
    }
    public class MasterParamPageHelper
    {        
        public const string ParamRNO = "rno";
        public const string ParamRID = "rid";
    }
    public class MasterReqModeHelper
    {
        public const string ReqOSOper = "OSOper";
        public const string ReqDocOper = "DocOper";
        public const string ReqDoctoOutsource = "ReqOutsource";
    }
}