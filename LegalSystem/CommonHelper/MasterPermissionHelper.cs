﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.CommonHelper
{
    
    public class MasterPermissionHelper
    {
        public const int CollectionMaker    = 440;
        public const int CollectionChecker  = 441;
        public const int CollectionHead     = 442;
        //public const int CollectionHeadApprove = 442;
        public const int LegalSpecialist    = 443;
        public const int SeniorLawyer       = 444;
        public const int Lawyer             = 445;

        public static readonly string[] PermissionPool = { "444", "445" };
        
    }
    
}