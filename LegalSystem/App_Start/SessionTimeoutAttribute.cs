﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LegalSystem.App_Start
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            if (HttpContext.Current.Session["UserLogin"] == null)
            {
                filterContext.Result = new RedirectResult("~/Login.aspx");
                return;
            }

            base.OnActionExecuting(filterContext);
         }
    }
}