﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class CoverPagePropertiesViewModel
    {
        public string CoverPagePropertiesID { get; set; }
        public int CoverPagePropertiesNo { get; set; }
        public string InputType { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public string Source { get; set; }
        public string Unit { get; set; }
    }
}