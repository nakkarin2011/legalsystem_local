﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class DropdownItemModel
    {
        public string DropdownType { get; set; }
        public string Text { get; set; }
        public object Value { get; set; }
    }
}