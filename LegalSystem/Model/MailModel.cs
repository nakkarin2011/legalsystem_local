﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class MailModel
    {
        public string Mail_ID { get; set; }
        public string Mail_SMTP { get; set; }
        public string Mail_From { get; set; }
        public string Mail_To { get; set; }
        public string Mail_CC1 { get; set; }
        public string Mail_CC2 { get; set; }
        public string Mail_Subject { get; set; }
        public string Mail_Body { get; set; }
    }
}