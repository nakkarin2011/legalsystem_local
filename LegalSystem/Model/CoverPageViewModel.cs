﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class CoverPageViewModel
    {

        public Guid documentTypeID { get; set; }
        public Guid requestID { get; set; }
        public Guid? coverPageID { get; set; }
        public string userID { get; set; }

        public List<CoverPageDocumentViewModel> documents { get; set;  }
    }
}