﻿using LegalData.Model;
using LegalData.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class LegalTabViewModel
    {
        public LegalTabViewModel() {
            ButtonPermission = new ButtonPermissionModel();
        }
        public string RequestID { get; set; }
        public ButtonPermissionModel ButtonPermission { get; set; }
        public int? DocumentTypeID { get; set; }
        public int? DocStatusID { get; set; }
        public int RequestTypeID { get; set; }
        public int TrayID { get; set; }

        public DateTime? DocUpdateDate { get; set; }
        public LegalData.vLegalInfo legalInfo { get; set; }
        public string UserID { get; set; }

        public string Signature { get; set; }
        public RequestDocumentExtensionModel DocumentExtension { get; set; }



    }
}