﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class CoverPageDocumentViewModel
    {
        public bool IsSelect { get; set; }
        public string CoverPageDocumentID { get; set; }
        public string CoverPageDocumentName { get; set; }
        public int CoverPageDocumentNo { get; set; }
        public List<CoverPagePropertiesViewModel> Properties { get; set; }
    }
}