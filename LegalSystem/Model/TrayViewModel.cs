﻿using LegalData.Model.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class TrayViewModel 
    {

        [Display(Name = "CIF No.")]
        public string CIFNo { get; set; }

        [Display(Name = "Document Status")]
        public int DocumentStatusID { get; set; }

        [Display(Name = "Document Type")]
        public int DocumentTypeID { get; set; }

        [Display(Name = "Request No.")]
        public string RequestNo { get; set; }

        [Display(Name = "Assign OA")]
        public string AssignOAName { get; set; }

        [Display(Name = "Legal No.")]
        public string LegalNo { get; set; }
    }
}