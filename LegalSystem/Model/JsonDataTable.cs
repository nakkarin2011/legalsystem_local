﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Model
{
    public class JsonDataTable
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public object data { get; set; }
        public JsonDataTable()
        {
            draw = 30;
        }
    }

    public class DatableOption
    {
        public string orderby { get => order != null ? order[0]?.FirstOrDefault(r => r.Key == "dir").Value : null; }
        public int sortingby { get => order != null ? int.Parse(order[0].FirstOrDefault(r => r.Key == "column").Value) : 0; }
        public int start { get; set; }
        public int length { get; set; }
        public int draw { get; set; }
        public List<Dictionary<string, string>> order { get; set; }
    }

    public class UseListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class FileUploaeModel
    {
        public Guid ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string EncodeBase64 { get; set; }
        public decimal Size { get; set; }
        public int IsAction { get; set; }
    }
}