﻿using LegalService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LegalSystem.Model;

namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsDocumentTypeMapping
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsDocumentTypeMapping : System.Web.Services.WebService
    {

        [WebMethod]
        public JsonDataTable InqueryDocumentTypeMapping(DatableOption option, string strRequest,string strDocTypeName)
        {
            using (DocTypeMapRep rep = new DocTypeMapRep())
            {
                strRequest = (strRequest == "0" ? string.Empty : strRequest);

                var query = (from a in rep.GetView()
                             where string.IsNullOrEmpty(strRequest) || a.ReqTypeID == Convert.ToInt32(strRequest.Trim())
                              && (string.IsNullOrEmpty(strDocTypeName) || a.DocTypeName.ToUpper().Contains(strDocTypeName.ToUpper().Trim()))
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeID) : query.OrderByDescending(r => r.ReqTypeID)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeName) : query.OrderByDescending(r => r.ReqTypeName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.DocTypeName) : query.OrderByDescending(r => r.DocTypeName)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeID) : query.OrderByDescending(r => r.ReqTypeID)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }


        [WebMethod]
        public string GetListRequestType()
        {

            using (RequestTypeRep rep = new RequestTypeRep())
            {
                var query = (from a in rep.GetAllRequestTyp()
                             select new UseListItem
                             {
                                 Value = a.ReqTypeID.ToString().Trim(),
                                 Text = string.Format("{0}:{1}",a.ReqTypeID,a.ReqTypeName)
                             }).ToList();

                return JsonConvert.SerializeObject(query);
            }

        }
        [WebMethod]
        public string GetListDocumentType()
        {

            using (DocTypeRep rep = new DocTypeRep())
            {
                var query = (from a in rep.GetAll()
                             select new UseListItem
                             {
                                 Value = a.DocTypeID.ToString().Trim(),
                                 Text = string.Format("{0}:{1}", a.DocTypeID, a.DocTypeName)
                             }).ToList();

                return JsonConvert.SerializeObject(query);
            }

        }

        [WebMethod]
        public string DeleteDocumentTypeMapping(string ID)
        {
            var strResult = string.Empty;
            try
            {
                using (DocTypeMapRep rep = new DocTypeMapRep())
                {
                    rep.DeleteData(new LegalData.DocumentTypeMapping
                    {
                        ID = Convert.ToInt32(ID.Trim())
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string SaveChangeData(string ID
                               , string DocTypeID
                               , string ReqTypeID
                               , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (DocTypeMapRep rep = new DocTypeMapRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.DocumentTypeMapping
                                {
                                    DocTypeID = Convert.ToInt32(DocTypeID),
                                    ReqTypeID = Convert.ToInt32(ReqTypeID)
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.DocumentTypeMapping
                                {
                                    ID = Convert.ToInt32(ID),
                                    DocTypeID = Convert.ToInt32(DocTypeID),
                                    ReqTypeID = Convert.ToInt32(ReqTypeID)
                                });
                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
