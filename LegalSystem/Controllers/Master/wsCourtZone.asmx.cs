﻿using LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LegalSystem.Model;

namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsCourtZone
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsCourtZone : System.Web.Services.WebService
    {

        [WebMethod]
        public JsonDataTable InqueryCourtZone(DatableOption option, string strSearch)
        {
            using (CourtZoneRep rep = new CourtZoneRep())
            {
                var query = (from a in rep.GetAllCourtZone()
                             where string.IsNullOrEmpty(strSearch) || a.CourtZoneName.ToUpper().Contains(strSearch.ToUpper().Trim())
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.CourtZoneID) : query.OrderByDescending(r => r.CourtZoneID)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.CourtZoneName) : query.OrderByDescending(r => r.CourtZoneName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.CourtZoneDesc) : query.OrderByDescending(r => r.CourtZoneDesc)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.CourtZoneID) : query.OrderByDescending(r => r.CourtZoneID)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public string DeleteData(string CourtZoneID)
        {
            var strResult = string.Empty;
            try
            {
                using (CourtZoneRep rep = new CourtZoneRep())
                {
                    rep.DeleteData(Convert.ToInt32(CourtZoneID.Trim()));
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string SaveChangeData(string CourtZoneID
                                    , string CourtZoneName
                                    ,string CourtZoneDesc
                                    , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (CourtZoneRep rep = new CourtZoneRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.insertData(new CourtZone
                                {
                                    CourtZoneName = CourtZoneName,
                                    CourtZoneDesc = CourtZoneDesc
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.updateData(new CourtZone
                                {
                                    CourtZoneID = Convert.ToInt32(CourtZoneID),
                                    CourtZoneName = CourtZoneName,
                                    CourtZoneDesc = CourtZoneDesc

                                });

                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
