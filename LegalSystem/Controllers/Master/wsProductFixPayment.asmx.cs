﻿using LegalService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LegalSystem.Model;

namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsProductFixPayment
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsProductFixPayment : System.Web.Services.WebService
    {

        [WebMethod]
        public JsonDataTable InqueryProductFixPayment(DatableOption option, string PrdTypeID)
        {
            using (ProductFixPaymentRep rep = new ProductFixPaymentRep())
            {
                PrdTypeID = (PrdTypeID == "0" ? string.Empty : PrdTypeID);
                var query = (from a in rep.GetView()
                             where string.IsNullOrEmpty(PrdTypeID) || a.PrdTypeID == Convert.ToInt32(PrdTypeID)
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeName) : query.OrderByDescending(r => r.PrdTypeName)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.SubTypeName) : query.OrderByDescending(r => r.SubTypeName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.FixCost) : query.OrderByDescending(r => r.FixCost)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeName) : query.OrderByDescending(r => r.PrdTypeName)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public string GetListSubProductType(string PrdTypeID)
        {

            using (ProdTypeDetailRep rep = new ProdTypeDetailRep())
            {   
                var query = (from a in rep.GetAll()
                             where a.PrdTypeID == Convert.ToInt32(PrdTypeID)
                             select new UseListItem
                             {
                                 Value = a.PrdTypeDetailID.ToString().Trim(),
                                 Text = a.SubTypeName.Trim()
                             }).ToList();

                return JsonConvert.SerializeObject(query);
            }

        }

        [WebMethod]
        public string DeleteData(string ProductFixID)
        {
            var strResult = string.Empty;
            try
            {
                using (ProductFixPaymentRep rep = new ProductFixPaymentRep())
                {
                    rep.DeleteData(new LegalData.msProductFixPayment
                    {
                        ProductFixID = Convert.ToInt32(ProductFixID.Trim())
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }


        [WebMethod]
        public string SaveChangeData(string ProductFixID
                           , string PrdTypeID
                           , string PrdTypeDetailID
                           , string FixCost
                           , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (ProductFixPaymentRep rep = new ProductFixPaymentRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.msProductFixPayment
                                {
                                    PrdTypeID = Convert.ToInt32(PrdTypeID),
                                    PrdTypeDetailID = Convert.ToInt32(PrdTypeDetailID),
                                    FixCost = Convert.ToDecimal(FixCost)
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.msProductFixPayment
                                {
                                    ProductFixID = Convert.ToInt32(ProductFixID),
                                    PrdTypeID = Convert.ToInt32(PrdTypeID),
                                    PrdTypeDetailID = Convert.ToInt32(PrdTypeDetailID),
                                    FixCost = Convert.ToDecimal(FixCost)
                                });
                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
