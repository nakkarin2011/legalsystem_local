﻿using LegalService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using LegalSystem.Model;


namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsProductType
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsProductType : System.Web.Services.WebService
    {

        [WebMethod]
        public  JsonDataTable InqueryProductType(DatableOption option, string strProdTypeName)
        {
            using (ProdTypeRep rep = new ProdTypeRep())
            {
                var query = (from a in rep.GetAll()
                             where string.IsNullOrEmpty(strProdTypeName) || a.PrdTypeName.ToUpper().Contains(strProdTypeName.ToUpper().Trim())
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeID) : query.OrderByDescending(r => r.PrdTypeID)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeName) : query.OrderByDescending(r => r.PrdTypeName)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeDesc) : query.OrderByDescending(r => r.PrdTypeDesc)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeID) : query.OrderByDescending(r => r.PrdTypeID)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public JsonDataTable InqueryProductTypeDetail(DatableOption option, string PrdTypeID,string SubTypeCode,string SubTypeName)
        {
            using (ProdTypeDetailRep rep = new ProdTypeDetailRep())
            {
                PrdTypeID = (PrdTypeID == "0" ? string.Empty : PrdTypeID);
                var query = (from a in rep.GetView()
                             where string.IsNullOrEmpty(PrdTypeID) || a.PrdTypeID == Convert.ToInt32(PrdTypeID)
                             && (string.IsNullOrEmpty(SubTypeCode) || a.SubTypeCode.ToUpper().Contains(SubTypeCode.ToUpper().Trim()))
                             && (string.IsNullOrEmpty(SubTypeName) || a.SubTypeName.ToUpper().Contains(SubTypeName.ToUpper().Trim()))
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeName) : query.OrderByDescending(r => r.PrdTypeName)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.SubTypeCode) : query.OrderByDescending(r => r.SubTypeCode)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.SubTypeName) : query.OrderByDescending(r => r.SubTypeName)); break;
                    case 4: query = (option.orderby == "asc" ? query.OrderBy(r => r.SubTypeDesc) : query.OrderByDescending(r => r.SubTypeDesc)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.PrdTypeName) : query.OrderByDescending(r => r.PrdTypeName)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public  string GetProductTypeByID(string PrdTypeID)
        {
            var strResult = string.Empty;

            using (ProdTypeRep rep = new ProdTypeRep())
            {
                var query = (from a in rep.GetAll()
                             where a.PrdTypeID == Convert.ToInt32(PrdTypeID.Trim())
                             select a).FirstOrDefault();

                if (query != null)
                {
                    strResult = JsonConvert.SerializeObject(query);
                }
            }
            return strResult;
        }
        [WebMethod]
        public  string DeleteProductType(string PrdTypeID)
        {
            var strResult = string.Empty;
            try
            {
                using (ProdTypeRep rep = new ProdTypeRep())
                {
                    rep.DeleteData(new LegalData.msProductType
                    {
                        PrdTypeID = Convert.ToInt32(PrdTypeID.Trim())
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public  string SaveChangeData(string PrdTypeID
                                   , string PrdTypeName
                                   , string PrdTypeDesc
                                   , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (ProdTypeRep rep = new ProdTypeRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.msProductType
                                {
                                    PrdTypeName = PrdTypeName,
                                    PrdTypeDesc = PrdTypeDesc
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.msProductType
                                {
                                    PrdTypeID = Convert.ToInt32(PrdTypeID),
                                    PrdTypeName = PrdTypeName,
                                    PrdTypeDesc = PrdTypeDesc
                                });
                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string GetListProductType()
        {
           
            using (ProdTypeRep rep = new ProdTypeRep())
            {
                var query = (from a in rep.GetAll()
                             select new UseListItem
                             {
                                 Value = a.PrdTypeID.ToString().Trim(),
                                 Text = a.PrdTypeName.Trim()
                             }).ToList();

                return  JsonConvert.SerializeObject(query);
            }
          
        }
       
        [WebMethod]
        public string SaveChangeDataDetail(string PrdTypeDetailID
                                    , string PrdTypeID
                                   , string SubTypeCode
                                   , string SubTypeName
                                   , string SubTypeDesc
                                    ,string status)

        {
            var strResult = string.Empty;
            try
            {
                using (ProdTypeDetailRep rep = new ProdTypeDetailRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.msProductTypeDetail
                                {
                                    PrdTypeID = Convert.ToInt32(PrdTypeID),
                                    SubTypeCode = SubTypeCode,
                                    SubTypeName = SubTypeName,
                                    SubTypeDesc = SubTypeDesc
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.msProductTypeDetail
                                {
                                    PrdTypeDetailID = Convert.ToInt32(PrdTypeDetailID),
                                    PrdTypeID = Convert.ToInt32(PrdTypeID),
                                    SubTypeCode = SubTypeCode,
                                    SubTypeName = SubTypeName,
                                    SubTypeDesc = SubTypeDesc
                                });
                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string DeleteProductTypeDetail(string PrdTypeDetailID)
        {
            var strResult = string.Empty;
            try
            {
                using (ProdTypeDetailRep rep = new ProdTypeDetailRep())
                {
                    rep.DeleteData(new LegalData.msProductTypeDetail
                    {
                        PrdTypeDetailID = Convert.ToInt32(PrdTypeDetailID.Trim())
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
