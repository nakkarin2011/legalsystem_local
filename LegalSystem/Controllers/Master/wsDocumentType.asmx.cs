﻿using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LegalSystem.Model;

namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsDocumentType
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsDocumentType : System.Web.Services.WebService
    {

        [WebMethod]
        public JsonDataTable InqueryDocumentType(DatableOption option, string strSearch)
        {
            using (DocTypeRep rep = new DocTypeRep())
            {
                var query = (from a in rep.GetAll()
                             where string.IsNullOrEmpty(strSearch) || a.DocTypeName.ToUpper().Contains(strSearch.ToUpper().Trim())
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.DocTypeID) : query.OrderByDescending(r => r.DocTypeID)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.DocTypeName) : query.OrderByDescending(r => r.DocTypeName)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.DocTypeID) : query.OrderByDescending(r => r.DocTypeID)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public string DeleteData(string DocTypeID)
        {
            var strResult = string.Empty;
            try
            {
                using (DocTypeRep rep = new DocTypeRep())
                {
                    rep.DeleteData(new LegalData.DocumentType
                    {
                        DocTypeID = Convert.ToInt32(DocTypeID)
                    });
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string SaveChangeData(string DocTypeID
                                    , string DocTypeName
                                    , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (DocTypeRep rep = new DocTypeRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.DocumentType
                                {
                                    DocTypeName = DocTypeName
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.DocumentType
                                {
                                    DocTypeID = Convert.ToInt32(DocTypeID),
                                    DocTypeName = DocTypeName

                                });

                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
