﻿using LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LegalSystem.Model;

namespace LegalSystem.Controllers.Master
{
    /// <summary>
    /// Summary description for wsRequestType
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsRequestType : System.Web.Services.WebService
    {

        public class InqueryRequestTypeModel
        {
            public string strSearch { get; set; }
            //public string strSearch2 { get; set; }
            //public string strSearch3 { get; set; }

        }

        [WebMethod]
        public JsonDataTable InqueryRequestType(DatableOption option, InqueryRequestTypeModel model)
        {
            using (RequestTypeRep rep = new RequestTypeRep())
            {
                var query = (from a in rep.GetAllRequestTyp()
                             where string.IsNullOrEmpty(model.strSearch) || a.ReqTypeName.ToUpper().Contains(model.strSearch.ToUpper().Trim())
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeID) : query.OrderByDescending(r => r.ReqTypeID)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeName) : query.OrderByDescending(r => r.ReqTypeName)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.ReqTypeID) : query.OrderByDescending(r => r.ReqTypeID)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return result;
            }
        }

        [WebMethod]
        public string DeleteRequestType(string ReqTypeID)
        {
            var strResult = string.Empty;
            try
            {
                using (RequestTypeRep rep = new RequestTypeRep())
                {
                    rep.DeleteData(Convert.ToInt32(ReqTypeID));
                    strResult = "Delete Data Success!!";

                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        [WebMethod]
        public string SaveChangeData(string ReqTypeID
                                    , string ReqTypeName
                                    , string status)

        {
            var strResult = string.Empty;
            try
            {
                using (RequestTypeRep rep = new RequestTypeRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.insertData(new RequestType
                                {
                                    ReqTypeName = ReqTypeName
                                });
                                strResult = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                // delete
                                var id = Convert.ToInt32(ReqTypeID);
                                rep.DeleteData(id);

                                // insert
                                rep.insertData(new RequestType
                                {
                                    ReqTypeID = id,
                                    ReqTypeName = ReqTypeName
                                });

                                strResult = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }
    }
}
