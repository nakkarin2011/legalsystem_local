﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LegalSystem.Controllers
{
    public class DocumentController : Controller
    {
        private LegalData.Repository.LegalEntities business = new LegalData.Repository.LegalEntities();
        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();

        [HttpPost]
        public JsonResult GetDocumentWithTrayAction(GetDocumentWithTrayActionRequest request)
        {
            ResultWithModel<List<DocumentModel>> rwm = new ResultWithModel<List<DocumentModel>>();

            try
            {
                var result    = business.Tray.GetDocumentWithTrayAction(request);

                rwm.Success   = result.Success;
                rwm.Message   = result.Message;
                rwm.Serverity = result.Serverity;
                rwm.Total     = result.Total;
                rwm.RefCode   = result.RefCode;

                if (result.Success)
                {
                    rwm.Data = result.Data.documents; 
                }

                else
                {
                    rwm.Data = new List<DocumentModel>();
                }
 
            }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Send(Guid id)
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestDocumentExtensionModel rdm = new RequestDocumentExtensionModel();
            RequestSignatureModel rsm = new RequestSignatureModel();

            try
            {
                //step : 1
                var signature       = Request.Headers.GetValues("signature").FirstOrDefault();
                var token           = HttpUtility.UrlDecode(signature);
                var SignatureResult = business.System.RequestSignature(token);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success = SignatureResult.Success;
                    rwm.Message = "Invalid Token";
                    rwm.RefCode = SignatureResult.RefCode;
                    rwm.Serverity = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 2
                var RequestResult =  business.DocumentExtension.Find(id);

                if (RequestResult.Success)
                {
                    if (!RequestResult.Data.Extenstions.Any())
                    {
                        rwm.Success = false;
                        rwm.Message = "Data not found";
                        rwm.RefCode = 404;

                        return Json(rwm);
                    }

                    rdm = RequestResult.Data.Extenstions.FirstOrDefault();
                }

                else
                {
                    rwm.Success     = RequestResult.Success;
                    rwm.Message     = RequestResult.Message;
                    rwm.RefCode     = RequestResult.RefCode;
                    rwm.Serverity   = RequestResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 3

                rdm.DocumentStatusID    = new Guid("3B97C866-49FC-4AEE-A4C8-59537137A602"); ////status = send
                rdm.DocumentStatusNo    = 1; 
                rdm.DocumentStatusDate  = DateTime.Now;

                var result = business.DocumentExtension.Save(rdm);

                rwm.Success   = result.Success;
                rwm.Message   = result.Message;
                rwm.RefCode   = result.RefCode;
                rwm.Serverity = result.Serverity;

                if (result.Success)
                {
                    rwm.Data = rdm;
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Approve(Guid id)
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestDocumentExtensionModel rdm = new RequestDocumentExtensionModel();
            RequestSignatureModel rsm = new RequestSignatureModel();

            try
            {

                //step : 1
                var signature       = Request.Headers.GetValues("signature").FirstOrDefault();
                var token           = HttpUtility.UrlDecode(signature);
                var SignatureResult = business.System.RequestSignature(token);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success = SignatureResult.Success;
                    rwm.Message = "Invalid Token";
                    rwm.RefCode = SignatureResult.RefCode;
                    rwm.Serverity = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 2
                var RequestResult =  business.DocumentExtension.Find(id);

                if (RequestResult.Success)
                {
                    if (!RequestResult.Data.Extenstions.Any())
                    {
                        rwm.Success = false;
                        rwm.Message = "Data not found";
                        rwm.RefCode = 404;

                        return Json(rwm);
                    }

                    rdm = RequestResult.Data.Extenstions.FirstOrDefault();
                }

                else
                {
                    rwm.Success     = RequestResult.Success;
                    rwm.Message     = RequestResult.Message;
                    rwm.RefCode     = RequestResult.RefCode;
                    rwm.Serverity   = RequestResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 3
                rdm.DocumentStatusID    = new Guid("4D0B1398-F7E1-45A6-80B1-D657DB8B8E48"); //status = approve
                rdm.DocumentStatusNo    = 3;
                rdm.DocumentStatusDate  = DateTime.Now;

                var result = business.DocumentExtension.Save(rdm);

                rwm.Success   = result.Success;
                rwm.Message   = result.Message;
                rwm.RefCode   = result.RefCode;
                rwm.Serverity = result.Serverity;

                if (result.Success)
                {
                    rwm.Data = rdm;
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Return(Guid id)
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestDocumentExtensionModel rdm = new RequestDocumentExtensionModel();
            RequestSignatureModel rsm = new RequestSignatureModel();

            try
            {
                //step : 1
                var signature       = Request.Headers.GetValues("signature").FirstOrDefault();
                var token           = HttpUtility.UrlDecode(signature);
                var SignatureResult = business.System.RequestSignature(token);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success = SignatureResult.Success;
                    rwm.Message = "Invalid Token";
                    rwm.RefCode = SignatureResult.RefCode;
                    rwm.Serverity = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 2
                var RequestResult =  business.DocumentExtension.Find(id);

                if (RequestResult.Success)
                {
                    if (!RequestResult.Data.Extenstions.Any())
                    {
                        rwm.Success = false;
                        rwm.Message = "Data not found";
                        rwm.RefCode = 404;

                        return Json(rwm);
                    }

                    rdm = RequestResult.Data.Extenstions.FirstOrDefault();
                }

                else
                {
                    rwm.Success     = RequestResult.Success;
                    rwm.Message     = RequestResult.Message;
                    rwm.RefCode     = RequestResult.RefCode;
                    rwm.Serverity   = RequestResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //step : 3
                rdm.DocumentStatusID    = new Guid("03E02734-F192-400B-8A15-F5E89D5971A9"); //status = return
                rdm.DocumentStatusNo    = 2;
                rdm.DocumentStatusDate  = DateTime.Now;

                var result = business.DocumentExtension.Save(rdm);

                rwm.Success   = result.Success;
                rwm.Message   = result.Message;
                rwm.RefCode   = result.RefCode;
                rwm.Serverity = result.Serverity;

                if (result.Success)
                {
                    rwm.Data = rdm;
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Print(Guid id, string token)
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestDocumentExtensionModel rdm = new RequestDocumentExtensionModel();
            RequestSignatureModel rsm = new RequestSignatureModel();

            var signature = HttpUtility.UrlDecode(token);

            var SignatureResult = business.System.RequestSignature(signature);

            if (SignatureResult.Success)
            {
                rsm = SignatureResult.Data;
            }

            else
            {
                rwm.Success = SignatureResult.Success;
                rwm.Message = "Invalid Token";
                rwm.RefCode = SignatureResult.RefCode;
                rwm.Serverity = SignatureResult.Serverity;

                return Json(rwm, JsonRequestBehavior.AllowGet);
            }


            var result = business.DocumentExtension.Find(id);

            if (result.Success)
            {
                rdm = result.Data.Extenstions.FirstOrDefault();
            }

            else
            {
                rwm.Success = false;
                rwm.Message = "Data not found";
                rwm.RefCode = 404;

                return Json(rwm, JsonRequestBehavior.AllowGet);
            }
 
            byte[] bytes;

            try
            {
                switch (rdm.TrayActionID.ToString().ToLower())
                {
                    //First Letter
                    case "04fa5305-1732-4179-b03f-8c3e02914016": bytes = GenFirstLetter(rdm.RequestID); break;

                    //Cover Page
                    case "24f09a3a-59ca-46a7-81bb-a688379a57fe": bytes = GenFirstLetter(rdm.RequestID); break;

                    default:
                        throw new Exception($"Not implement { rdm.TrayActionID }");
                 }
                
                MemoryStream output;
 

                //Document Status = Approve and Print 
                if (rdm.DocumentStatusNo.Equals(3) || rdm.DocumentStatusNo.Equals(4))
                {
                    output = new MemoryStream(bytes);
                }

                else
                {
                    output = new MemoryStream();
                    PdfReader reader = new PdfReader(bytes);

                    byte[] byteInfo;

                    using (var ms = new MemoryStream())
                    {
                        using (var stamper = new PdfStamper(reader, ms))
                        {
                            int PageCount = reader.NumberOfPages;

                            for (int i = 1; i <= PageCount; i++)
                            {
                                var realPageSize = reader.GetPageSize(i);
                                var cb = stamper.GetOverContent(i);

                                AddWaterMarkText(cb, "ตัวอย่างก่อนพิมพ์", 50, 45, realPageSize);
                            }
                        }

                        byteInfo = ms.ToArray();
                    }

                    output.Write(byteInfo, 0, byteInfo.Length);
                    output.Position = 0;
                }

                //stamp print date
                if (rdm.DocumentStatusNo.Equals(3))
                {
                    //Stamp Print Document Date
                    if (rdm.PrintDate == null)
                    {
                        //update print date
                        rdm.DocumentStatusID   = new Guid("6EF144C8-245F-43D9-B4C8-818AC4DE9ED9"); //status = print
                        rdm.DocumentStatusDate = DateTime.Now;
                        rdm.PrintDate          = DateTime.Now;
                        rdm.PrintBy            = rsm.UserID;

                        business.DocumentExtension.Save(rdm);
                    }

                    //Save Print History
                    //var printResult = business.Tray.PrintDocument(new LegalData.Model.Request.PrintDocumentRequest
                    //{
                    //    PrintDocumentID   = Guid.NewGuid()
                    //   , RequestID        = _request.RequestID
                    //   , DocumentTypeID   = _request.DocTypeID.GetValueOrDefault()
                    //   , DocumentStatusID = _request.DocStatusID.GetValueOrDefault()
                    //   , CreateBy         = userID
                    //   , CreateDate       = DateTime.Now
                    //});
                }
 
                return File(output, "application/pdf");
            }

            catch (Exception ex)
            {
                rwm.Success = false;
                rwm.Message = ex.Message;
                rwm.RefCode = 500;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        private byte[] GenFirstLetter(Guid requestID) {
            
            string extension = string.Empty;
            string encoding = string.Empty;
            string mimeType = string.Empty;

            Warning[] warnings;
            string[] streamIds;
  
            var ReportResponse = business.FirstLetter.ReportData(requestID);
            ReportViewer viewer = new ReportViewer();
            viewer.LocalReport.ReportPath = Server.MapPath("~\\Reports\\FirstLetterReport.rdlc");
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Clear();
            viewer.LocalReport.DataSources.Add(new ReportDataSource("FirstLetterData", ReportResponse.Data));
            viewer.LocalReport.Refresh();

           return viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

        }
        private void AddWaterMarkText(PdfContentByte directContent, string textWatermark, float fontSize, float angle, iTextSharp.text.Rectangle realPageSize)
        {
            //BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);

            BaseFont bfComic = BaseFont.CreateFont(Server.MapPath("~\\Fonts\\ARIALUNI.TTF"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            var gstate = new PdfGState { FillOpacity = 0.5f, StrokeOpacity = 0.7f };

            directContent.SaveState();
            directContent.SetGState(gstate);
            directContent.SetColorFill(BaseColor.GRAY);
            directContent.BeginText();
            directContent.SetFontAndSize(bfComic, fontSize);

            var x = (realPageSize.Right + realPageSize.Left) / 2;
            var y = (realPageSize.Bottom + realPageSize.Top) / 2;

            directContent.ShowTextAligned(Element.ALIGN_CENTER, textWatermark, x, y, angle);
            directContent.EndText();
            directContent.RestoreState();
        }
    }
}