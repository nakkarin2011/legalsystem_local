﻿using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LegalSystem.Controllers
{
    public class FirstLetterController : Controller
    {
        private LegalData.Repository.LegalEntities business = new LegalData.Repository.LegalEntities();
        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();

        // GET: FirstLetter
        [HttpPost]
        public JsonResult Information()
        {
            //avalible requestid and documentId 
            ResultWithModel<FirstLetterModel> rwm = new ResultWithModel<FirstLetterModel>(); 

            try
            {
                RequestSignatureModel rsm = new RequestSignatureModel();

                var signature = Request.Headers.GetValues("signature").FirstOrDefault();
                var SignatureResult = business.System.RequestSignature(signature);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success = SignatureResult.Success;
                    rwm.Message = SignatureResult.Message;
                    rwm.RefCode = SignatureResult.RefCode;
                    rwm.Serverity = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                ResultWithModel<FirstLetterResponse> result = new ResultWithModel<FirstLetterResponse>();

                var firstLetterInformationResult = business.FirstLetter.FindByRequest(new Guid(rsm.RequestID));

                rwm.Success     = firstLetterInformationResult.Success;
                rwm.Message     = firstLetterInformationResult.Message;
                rwm.Serverity   = firstLetterInformationResult.Serverity;
                rwm.Total       = firstLetterInformationResult.Total;
                rwm.RefCode     = firstLetterInformationResult.RefCode;

                if (firstLetterInformationResult.Success)
                {
                    //get first letter already.

                    rwm.Data = firstLetterInformationResult.Data.firstLetterInformations.Select(s => new FirstLetterModel {

                         FirstLetterID  = s.FirstLetterID
                         , RequestID    = s.RequestID
                         , LegalNo      = s.LegalNo
                         , CreateBy     = s.CreateBy
                         , IsSave       = true

                    }).FirstOrDefault();

                    if (rwm.Data != null)
                    {
                        var documentExtensionResult = business.DocumentExtension.FindByRequest(new Guid(rsm.RequestID));

                        if (documentExtensionResult.Success)
                        {
                            rwm.Data.Document = documentExtensionResult.Data.Extenstions.FirstOrDefault();
                        }

                        rwm.Data.Details = new List<FirstLetterInformationModel>(); 

                        var  details = firstLetterInformationResult.Data
                            .firstLetterInformations
                            .Select(s => new FirstLetterInformationModel
                            {
                                 FirstLetterDetailID  = s.FirstLetterDetailID
                               , FirstLetterID        = s.FirstLetterID
                               , CIFNo                = s.CIFNo
                               , LawyerAttorneyID     = s.LawyerAttorneyID
                               , SeniorLawyerID       = s.SeniorLawyerID
                               , SeniorLawyerName     = s.SeniorLawyerName
                               , RelationCode         = s.RelationCode
                               , RelationName         = s.RelationName
                               , LoanDateText         = s.LoanDateText
                               , BorrowerName         = s.BorrowerName
                               , CustomerName         = s.CustomerName
                               , LoanAmount           = s.LoanAmount
                               , CurrentBalanceAmount = s.CurrentBalanceAmount
                               , InterestAmount       = s.InterestAmount
                               , OtherInterestAmount  = s.OtherInterestAmount
                               , Address1             = s.Address1
                               , Address2             = s.Address2

                            })

                            .ToList();

                        rwm.Data.Details.AddRange(details);
                    }
                }

                else
                {

                    //get new information for first letter
                    var firstLetterInitialResult = business.FirstLetter.Initial(rsm.RequestID, rsm.UserID);

                    rwm.Success   = firstLetterInitialResult.Success;
                    rwm.Message   = firstLetterInitialResult.Message;
                    rwm.Serverity = firstLetterInitialResult.Serverity;
                    rwm.Total     = firstLetterInitialResult.Total;
                    rwm.RefCode   = firstLetterInitialResult.RefCode;

                    if (firstLetterInitialResult.Success)
                    {
                        rwm.Data = firstLetterInitialResult.Data.Select(s => new FirstLetterModel {

                          FirstLetterID  = Guid.NewGuid()
                        , RequestID      = new Guid(rsm.RequestID)
                        , LegalNo        = s.LegalNo
                        , CreateBy       = rsm.UserID
                        , IsSave         = false

                        }).FirstOrDefault();

                        if (rwm.Data != null)
                        {
                            rwm.Data.Document = new RequestDocumentExtensionModel
                            {
                                   ExtensionID          = Guid.NewGuid()
                                 , RequestID            = new Guid(rsm.RequestID)
                                 , DocumentStatusID     = new Guid("ac2428cf-87ac-4dfc-8cf6-332162f826c9")
                                 , DocumentStatusDate   = DateTime.Now
                                 , CreateBy             = rsm.UserID
                            };

                            rwm.Data.Details = new List<FirstLetterInformationModel>(); 

                            var details = firstLetterInitialResult
                                        .Data
                                        .Select(s => new FirstLetterInformationModel {
                                            FirstLetterDetailID  = Guid.NewGuid()
                                            , FirstLetterID        = s.FirstLetterID
                                            , CIFNo                = s.CIFNo
                                            , LawyerAttorneyID     = s.LawyerAttorneyID
                                            , SeniorLawyerID       = s.SeniorLawyerID
                                            , SeniorLawyerName     = s.SeniorLawyerName
                                            , RelationCode         = s.RelationCode
                                            , RelationName         = s.RelationName
                                            , LoanDateText         = s.LoanDateText
                                            , BorrowerName         = s.BorrowerName
                                            , CustomerName         = s.CustomerName
                                            , LoanAmount           = s.LoanAmount.GetValueOrDefault()
                                            , CurrentBalanceAmount = s.CurrentBalanceAmount.GetValueOrDefault()
                                            , InterestAmount       = s.InterestAmount.GetValueOrDefault()
                                            , OtherInterestAmount  = s.OtherInterestAmount.GetValueOrDefault()
                                            , Address1             = s.Address1
                                            , Address2             = s.Address2
                                        })
                                        
                                        .ToList();
                         
                            rwm.Data.Details.AddRange(details);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SaveAsync(FirstLetterModel formModel)
        {
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                FirstLetterInformationModel FirstLetter = null;

                var firstLetterResult = business.FirstLetter.Find(formModel.FirstLetterID);

                if (firstLetterResult.Success)
                {
                    FirstLetter = firstLetterResult.Data.firstLetterInformations.FirstOrDefault();
                }

                var documentExtensionResult = business.DocumentExtension.FindByRequest(formModel.RequestID);

                if (documentExtensionResult.Success)
                {
                    var documentExtension = documentExtensionResult.Data.Extenstions.FirstOrDefault();

                    if (documentExtension != null)
                    {
                        if (documentExtension.DocumentStatusNo.Equals(1))
                        {
                            rwm.Success = false;
                            rwm.Message = "The document status is waiting to approve.";
                            rwm.RefCode = 500;

                            return Json(rwm, JsonRequestBehavior.AllowGet);
                        }

                        else if (documentExtension.DocumentStatusNo.Equals(3) 
                            || documentExtension.DocumentStatusNo.Equals(4))
                        {
                            rwm.Success = false;
                            rwm.Message = "The document has been approved.";
                            rwm.RefCode = 500;

                            return Json(rwm, JsonRequestBehavior.AllowGet);
                        }

                        //Create New First Letter
                        if (FirstLetter == null)
                        {

                            var GenerateLegalNoResult = business.Document.GenerateLegalNo(documentExtension.LawyerAttorneyID, formModel.CreateBy);

                            if (GenerateLegalNoResult.Success)
                            {
                                formModel.LegalNo = GenerateLegalNoResult.Data;
                            }

                            else
                            {
                                rwm.Success = GenerateLegalNoResult.Success;
                                rwm.RefCode = GenerateLegalNoResult.RefCode;
                                rwm.Message = $"Faild to generate legal no. ({GenerateLegalNoResult.Message})";

                                return Json(rwm, JsonRequestBehavior.AllowGet);
                            }

                            //loop update first letterid on first letter detail
                            formModel.Details
                                .Select(p => p.FirstLetterID = formModel.FirstLetterID)
                                .ToList();

                            //save New
                            rwm = business.FirstLetter.Save(formModel);

                            if (rwm.Success)
                            {
                                var updateDocumentResult = business.DocumentExtension.Save(formModel.Document); 
                            }


                         }

                        else
                        {
                            //Save Update
                            rwm = business.FirstLetter.Save(formModel);

                            if (rwm.Success)
                            {
                                //change status from return to new
                                if (formModel.Document.DocumentStatusNo.Equals(2))
                                {

                                    formModel.Document.DocumentStatusID     = new Guid("ac2428cf-87ac-4dfc-8cf6-332162f826c9");
                                    formModel.Document.DocumentStatusNo     = 0;
                                    formModel.Document.DocumentStatusDate   = DateTime.Now;

                                    var updateDocumentResult = business.DocumentExtension.Save(formModel.Document);
                                }
                            }
                        }


                        formModel.IsSave = rwm.Success;

                        if (rwm.Success)
                        {
                            rwm.Data = formModel;
                        }

                    }

                    else
                    {
                        rwm.Message = "The Request information was not found.";

                        return Json(rwm);
                    }
                }

                else
                {
                    rwm.Message = "The Request information was not found.";

                    return Json(rwm);
                }
            }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }
    }
}