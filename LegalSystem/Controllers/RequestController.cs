﻿using LegalData.Model;
using LegalData.Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LegalSystem.Controllers
{
    public class RequestController : Controller
    {
        private LegalData.Repository.LegalEntities business = new LegalData.Repository.LegalEntities();
        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();

        // GET: Request
        public ActionResult Index()
        {
            return View();
        }
 
        [HttpPost]
        public async Task<JsonResult> Information()
        {
            ResultWithModel rwm = new ResultWithModel();

            RequestSignatureModel rsm = new RequestSignatureModel();

            try
            {
                var signature = Request.Headers.GetValues("signature").FirstOrDefault();
                var SignatureResult = business.System.RequestSignature(signature);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success     = SignatureResult.Success;
                    rwm.Message     = SignatureResult.Message;
                    rwm.RefCode     = SignatureResult.RefCode;
                    rwm.Serverity   = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }


               var RequestResult =  business.DocumentExtension.FindByRequest(new Guid(rsm.RequestID));

                rwm.Success     = RequestResult.Success;
                rwm.Message     = RequestResult.Message;
                rwm.RefCode     = RequestResult.RefCode;
                rwm.Serverity   = RequestResult.Serverity;

                if (RequestResult.Success)
                {
                    if (!RequestResult.Data.Extenstions.Any())
                    {
                        rwm.Success = false;
                        rwm.Message = "Data not found";
                        rwm.RefCode = 404;

                        return Json(rwm);
                    }
                    rwm.Signature   = HttpUtility.UrlEncode(signature);
                    rwm.Data        = RequestResult.Data.Extenstions.FirstOrDefault();
                }
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

    }
}