﻿using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LegalSystem.Controllers
{
    public class CoverPageController : Controller
    {
        private LegalData.Repository.LegalEntities business = new LegalData.Repository.LegalEntities();
        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();
        [HttpPost]
        public async Task<JsonResult> List(CoverPageRequest request)
        {

            //avalible requestid and documentId 
            ResultWithModel rwm = new ResultWithModel ();

            try
            {

                bool isSave = false; 

                //get 
                ResultWithModel<CoverPageResponse> CoverpageResult = new ResultWithModel<CoverPageResponse>();

                CoverpageResult = business.CoverPage.GetCoverPageInfo(request);


                //if (!isSave)
                //{
                //    CoverpageResult = business.CoverPage.GetCoverPageMaster(request.DocumentID);
                //}

                rwm.Success   = CoverpageResult.Success;
                rwm.Message   = CoverpageResult.Message;
                rwm.Serverity = CoverpageResult.Serverity;
                rwm.Total     = CoverpageResult.Total;
                rwm.RefCode   = CoverpageResult.RefCode;

                if (CoverpageResult.Success)
                {
                    var CoverPages = CoverpageResult.Data.CoverPages;

                    var _documents = CoverPages.GroupBy(g => new
                    {
                        g.CoverPageID,
                        g.CoverPageDocumentID,
                        g.CoverPageDocumentName,
                        g.CoverPageDocumentNo
                    })

                    .Select(s => new CoverPageDocumentViewModel
                    {
                        CoverPageDocumentID   = s.Key.CoverPageDocumentID.ToString(),
                        CoverPageDocumentName = s.Key.CoverPageDocumentName ,
                        CoverPageDocumentNo   = s.Key.CoverPageDocumentNo   ,
                        IsSelect              = s.Key.CoverPageID.HasValue  ,
                        Properties            = s.Where(p=> p.CoverPagePropertiesID != null).Select(p => new CoverPagePropertiesViewModel {
                              CoverPagePropertiesID   = p.CoverPagePropertiesID.ToString()
                              , CoverPagePropertiesNo = p.CoverPagePropertiesNo.Value 
                              , InputType             = p.InputType 
                              , Label                 = p.Label
                              , Value                 = p.Value 
                              , Source                = p.Source 
                              , Unit                  = p.Unit 
                         }).ToList()
                    })

                    .ToList();

                    string _coverPageID = string.Empty;

                    var  _coverpage  = CoverPages.Where(s => s.CoverPageID != null).FirstOrDefault();

                    if (_coverpage != null)
                    {
                        _coverPageID = _coverpage.CoverPageID.ToString();

                        isSave = true;
                    }

                    rwm.Data = new { isSave =  isSave  , documents = _documents, coverPageID = _coverPageID };
                }

                else
                {
                    rwm.Data =  new  {
                        isSave = false
                        , documents = new List<CoverPageDocumentViewModel>()
                    };
                }
            }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> Save(CoverPageViewModel formModel)
        {
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                CoverPageModel coverPage = new CoverPageModel();

                var Request = await db.trRequests.FindAsync(formModel.requestID);

                if (Request == null)
                {
                    rwm.Success = false;
                    rwm.Message = "The Request information was not found.";

                    return Json(rwm);
                }

                if (Request.DocStatusID.Equals(1))
                {

                    rwm.Success = false;
                    rwm.Message = "The document status is waiting to approve.";
                    rwm.RefCode = 500;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                else if (Request.DocStatusID.Equals(3))
                {
                    rwm.Success = false;
                    rwm.Message = "The document has been approved.";
                    rwm.RefCode = 500;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }


                if (formModel.coverPageID == null)
                {
                    //Create Coverpage
                    coverPage.CoverPageID   = Guid.NewGuid();
                    coverPage.DocumentID    = formModel.documentTypeID;
                    coverPage.RequestID     = formModel.requestID;
                    coverPage.IsActive      = true;
                    coverPage.CreateBy      = formModel.userID;

                    int LawyerAttorneyID    = 0;

                    if (int.TryParse(Request.LawyerAttorneyID, out LawyerAttorneyID))
                    {
                        var GenerateLegalNoResult = business.Document.GenerateLegalNo(LawyerAttorneyID, formModel.userID);

                        if (GenerateLegalNoResult.Success)
                        {
                            coverPage.LegalNo = GenerateLegalNoResult.Data;
                        }

                        else
                        {
                            rwm.Success = GenerateLegalNoResult.Success;
                            rwm.RefCode = GenerateLegalNoResult.RefCode;
                            rwm.Message = $"Faild to generate legal no. ({GenerateLegalNoResult.Message})";

                            return Json(rwm, JsonRequestBehavior.AllowGet);
                        }
                    }

                    else { coverPage.LegalNo = null; }

                }

                else
                {
                    //update edit Coverpage

                    var CoverpageInfoResult = business.CoverPage.GetCoverPageInfo(new CoverPageRequest {
                          RequestID  = formModel.requestID.ToString()
                        , DocumentID = formModel.documentTypeID.ToString()
                    });

                    if (CoverpageInfoResult.Success)
                    {
                       var oldCoverPage         =  CoverpageInfoResult.Data.CoverPages.FirstOrDefault();
                        coverPage.CoverPageID   = oldCoverPage.CoverPageID.Value;
                        coverPage.LegalNo       = oldCoverPage.LegalNo;
                    }

                    coverPage.DocumentID        = formModel.documentTypeID;
                    coverPage.RequestID         = formModel.requestID;
                    coverPage.IsActive          = true;
                    coverPage.CreateBy          = formModel.userID;
                 }

                formModel.documents.ForEach(cvp =>
                {
                    cvp.Properties.ForEach(cpp =>
                    {
                        coverPage.CoverPageDetails.Add(new CoverPageDetailModel
                        {
                           CoverPageDetailID        = Guid.NewGuid()
                           , CoverPageID            = coverPage.CoverPageID
                           , CoverPageDocumentID    = new Guid( cvp.CoverPageDocumentID)
                           , CoverPagePropertiesID  = new Guid(cpp.CoverPagePropertiesID)
                           , Value                  = cpp.Value

                        });
                    });
                });

                rwm = business.CoverPage.Save(coverPage);

                if (rwm.Success)
                {

                    //Coverpage Document
                    //Request.DocTypeID   = 1;
                    //Request.DocStatusID = 0;
                    //Request.DocUpdateDate = null;

                    await db.SaveChangesAsync();



                    rwm.Data = new { coverPageID = coverPage.CoverPageID };
                }
             }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }
    }
}