﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LegalData.Model;
using LegalService;
using LegalSystem.Common;
using LegalSystem.Model;
using static LegalSystem.Common.GlobalSession;
using LegalData.Model.Request;
using LegalData.Model.Response;
using LegalData;
using System.IO;
using System.Globalization;
using Newtonsoft.Json;
using LegalData.Model.EF;
//using LegalSystem.Model;

namespace LegalSystem.Controllers
{
    public class LegalTabController : Controller
    {

        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();
        private LegalData.Repository.LegalEntities legal = new LegalData.Repository.LegalEntities();

        // GET: LegalTab
        [HttpPost]
        public async Task<ActionResult> Index(string rid)
        {
            LegalTabViewModel model = new LegalTabViewModel();
   
            try
            {
                var Request = await db.trRequests.FindAsync(new Guid(rid));

                if (Request == null)
                {
                    return View(model);
                }

                model.RequestID         = Request.RequestID.ToString();
                var RequestResult = legal.DocumentExtension.FindByRequest(new Guid(rid));
                if (RequestResult.Success)
                {
                    if (RequestResult.Data.Extenstions.Any())
                    {
                        model.DocumentExtension = RequestResult.Data.Extenstions.FirstOrDefault();
                    }
                }

                var CreateRequestSignatureResult = legal.System.CreateRequestSignature(new RequestSignatureModel{
                     PermissionID   = GetPermission.ToString()
                     , UserID       = GlobalSession.UserLogin.Emp_Code
                     , RequestID    = rid
                });

                if (CreateRequestSignatureResult.Success)
                {
                    model.Signature = CreateRequestSignatureResult.Data;
                }
 
                using (LegalTabRep legalrep = new LegalTabRep())
                {
                    model.legalInfo = legalrep.getLegalInfoData(new Guid(rid));
                }

                return View(model);
            }

            catch
            {
                return View(model);
            }

        }

        [HttpPost]
        public JsonResult ListLegalStatus()
        {
            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    var statusList = new List<string> { "206", "209", "212","300","303" };
                    var query = (from a in rep.getListLegalStatus()
                                 select new System.Web.Mvc.SelectListItem
                                 {
                                     Text = string.Format("{0}-{1}", a.LegalStatus, a.LegalStatusName),
                                     Value = a.LegalStatus,
                                     Disabled = statusList.Contains(a.LegalStatus)
                                 });

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult ListAppeal()
        {
            try
            {
                var ListItem = new List<SelectListItem>();

                ListItem.Add(new SelectListItem
                {
                    Text = "อุทธรณ์",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "งดอุทธรณ์",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult ListPetition()
        {
            try
            {
                var ListItem = new List<SelectListItem>();
           
                ListItem.Add(new SelectListItem
                {
                    Text = "ฎีกา",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "งดฎีกา",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult ListFindType()
        {
            try
            {
                var ListItem = new List<SelectListItem>();

                ListItem.Add(new SelectListItem
                {
                    Text = "พบ",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "ไม่พบ",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult ListIndictment()
        {
            try
            {
                var ListItem = new List<SelectListItem>();

                ListItem.Add(new SelectListItem
                {
                    Text = "ถูกต้อง",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "ไม่ถูกต้อง",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult ListExecuteBookType()
        {
            try
            {
                var ListItem = new List<SelectListItem>();

                ListItem.Add(new SelectListItem
                {
                    Text = "ยึดทรัพย์",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "ยื่นบุริมสิทธิ",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult ListCaseFee()
        {
            try
            {
                var ListItem = new List<SelectListItem>();

                ListItem.Add(new SelectListItem
                {
                    Text = "มี",
                    Value = "1"
                });
                ListItem.Add(new SelectListItem
                {
                    Text = "ไม่มี",
                    Value = "2"
                });

                return Json(ListItem);
            }
            catch
            {
                throw;
            }

        }


        [HttpPost]
        public JsonResult ListProvince()
        {
            try
            {
                using (msProvinceRep rep = new msProvinceRep())
                {
                    var query = (from a in rep.GetAll()
                                 select new System.Web.Mvc.SelectListItem
                                 {
                                     Text = a.Province,
                                     Value = a.State
                                 });

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult ListCourtFee(int id,int capital)
        {
            try
            {
                decimal calFee = 0;

                using (msCourtFeeRep rep = new msCourtFeeRep())
                {
                    var query = from a in rep.GetAll()
                                where a.CourtID == id
                                select a;

                    switch (id)
                    {
                        
                        case 2:
                            {
                                var g = query.ToList().FirstOrDefault();

                                if (g != null)
                                {
                                    calFee = (capital * Convert.ToInt32(g.FeePercent)) / 100;
                                    calFee = (calFee >= g.MaxLimitFee ? g.MaxLimitFee.Value : calFee);
                                }

                                break;
                            }
                        case 3:
                            {
                                var g = query.ToList().FirstOrDefault();

                                if (g != null)
                                {
                                    calFee = (capital * Convert.ToInt32(g.FeePercent)) / 100;
                                    calFee = (calFee >= g.MaxLimitFee ? g.MaxLimitFee.Value : calFee);
                                }

                                break;
                            }
                        default:
                            {
                                foreach (var g in query.ToList())
                                {
                                    switch (g.Condition)
                                    {
                                        case "<=":
                                            {
                                                if (capital <=  Convert.ToInt32(g.Capital))
                                                {
                                                    calFee = g.FeeAmount.Value;
                                                }
                                                break;
                                            }
                                        default:
                                            {
                                                if (capital > Convert.ToInt32(g.Capital))
                                                {
                                                    calFee = (capital * Convert.ToInt32(g.FeePercent)) / 100;
                                                    calFee = (calFee >= g.MaxLimitFee ? g.MaxLimitFee.Value : calFee);
                                                }
                                                break;
                                            }
                                    }
                                }

                                break;
                            }
                    }



                    return Json(calFee);
                }
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult SaveChangeLegalInfoTab1(legalTab1 model)
        {
            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    var result = rep.UpdateDataLegalTab1(model);

                    return Json(result);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult SaveChangeLegalInfoTab2(legalTab2 model)
        {
            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    var result = rep.UpdateDataLegalTab2(model);

                    return Json(result);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult SaveChangeLegalInfoTab3(legalTab3 model)
        {
            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    var result = rep.UpdateDatalegalTab3(model);

                    return Json(result);
                }
            }
            catch
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult SaveChangeLegalInfoTab4(legalTab4 model)
        {
            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    var result = rep.UpdateDatalegalTab4(model);

                    return Json(result);
                }
            }
            catch
            {
                throw;
            }
        }
        [HttpPost]
        public JsonResult SaveChangeLegalInfoTab5(vLegalInfo model)
        {
            var Result = string.Empty;

            try
            {
                using (LegalTabRep rep = new LegalTabRep())
                {
                    rep.UpdateDatalegalStatus(model.RequestID, model.LegalStatus);
                    Result = "Update Data successfully.";
                }
            }
            catch (Exception ex)
            {
                Result = ex.Message;
                throw;
            }

            return Json(Result);
        }
        [HttpPost]
        public JsonResult SaveChangeCourtAppointmentDate(ModalCourt model, string status)
        {
            var dd = CultureInfo.CurrentCulture;
            var Result = string.Empty;

            try
            {
                
                using (CourtAppointmentDateRep rep = new CourtAppointmentDateRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(new LegalData.CourtAppointmentDate
                                {
                                    CourtID = model.CourtID,
                                    RequestID = model.RequestID,
                                    CourtDate = model.CourtDate,
                                    Comment = model.Comment

                                });
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(new LegalData.CourtAppointmentDate
                                {
                                    CA_ID = model.CA_ID,
                                    CourtID = model.CourtID,
                                    RequestID = model.RequestID,
                                    CourtDate = model.CourtDate,
                                    Comment = model.Comment
                                });
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteCourtAppointmentDate(string strCA_ID)
        {
            var Result = string.Empty;

            try
            {

                using (CourtAppointmentDateRep rep = new CourtAppointmentDateRep())
                {
                    rep.DeleteData(new LegalData.CourtAppointmentDate
                    {
                        CA_ID = Convert.ToInt32(strCA_ID),
                    });
                    Result = "Delete Data Success!!";
                }
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult InquiryCourtAppointmentDate(DatableOption option, ModalCourt model)
        {
            using (CourtAppointmentDateRep rep = new CourtAppointmentDateRep())
            {
                var query = (from a in rep.GetView()
                             where a.RequestID == model.RequestID && a.CourtID == model.CourtID
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.RowNo) : query.OrderByDescending(r => r.CourtDate)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.CourtDate) : query.OrderByDescending(r => r.Comment)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.Comment) : query.OrderByDescending(r => r.Comment)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.RowNo) : query.OrderByDescending(r => r.CourtDate)); break;
                }


                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult GetLastAppointmentDate(ModalCourt model)
        {
            var LastCourtDate = string.Empty;
            try
            {
                using (CourtAppointmentDateRep rep = new CourtAppointmentDateRep())
                {
                    var query = (from a in rep.GetView()
                                 where   a.CourtID == model.CourtID && a.RequestID == model.RequestID
                                 select a).OrderByDescending(a => a.CA_ID).FirstOrDefault();

                    if (query != null)
                    {
                        LastCourtDate = query.CourtDate.Value.ToShortDateString();
                    }

                    return Json(LastCourtDate);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult InquiryfindAsset(DatableOption option, trfindAsset model)
        {
            using (trfindAssetRep rep = new trfindAssetRep())
            {
                var query = (from a in rep.GetAll()
                             where a.RequestID == model.RequestID && a.findType == model.findType
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.findDate) : query.OrderByDescending(r => r.findDate)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.findStatus) : query.OrderByDescending(r => r.findStatus)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.findDate) : query.OrderByDescending(r => r.findDate)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }
        [HttpPost]
        public JsonResult InquiryfindAssetDesc(DatableOption option, Guid fid)
        {
            using (AssetDescRep rep = new AssetDescRep())
            {
                var query = (from a in rep.GetView() 
                             where a.f_ID == fid
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetTypeName) : query.OrderByDescending(r => r.AssetTypeName)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetNo) : query.OrderByDescending(r => r.AssetNo)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetDistrict) : query.OrderByDescending(r => r.AssetDistrict)); break;
                    case 4: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetSubDistrict) : query.OrderByDescending(r => r.AssetSubDistrict)); break;
                    case 5: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetAddress) : query.OrderByDescending(r => r.AssetAddress)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.AssetTypeName) : query.OrderByDescending(r => r.AssetTypeName)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult InquiryFileUpload(DatableOption option, Guid fid)
        {
            using (ScanDocumentRep rep = new ScanDocumentRep())
            {
                var query = (from a in rep.GetAll()
                             where a.f_ID == fid
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.ScanDocName) : query.OrderByDescending(r => r.ScanDocName)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Type) : query.OrderByDescending(r => r.File_Type)); break;     
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.ScanDocName) : query.OrderByDescending(r => r.ScanDocName)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult ListAssetType()
        {
            try
            {
                using (LegalSystemEntities _context = new LegalSystemEntities())
                {
                    var query = (from a in _context.msAssetTypes.ToList()
                                 select new System.Web.Mvc.SelectListItem
                                 {
                                     Text = a.AssetTypeName,
                                     Value = a.AssetTypeID.ToString()
                                 });

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult SaveChangeAssetDesc(AssetDesc model, string status)
        {
            var Result = string.Empty;

            try
            {

                using (AssetDescRep rep = new AssetDescRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(model);
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(model);
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

              
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult UploadData(List<FileUploaeModel>  files, ScanDocument model)
        {
            var Result = string.Empty;

            try
            {
                var F = files.FirstOrDefault();
                //var dirPath =  "D:\\fileupload\\";
                var dirPath = System.Configuration.ConfigurationManager.AppSettings["PathFileUpload"].ToString();
                var fileName = F.Name;

                //Create Floder
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }

                files.ForEach(item =>
                {
                   
                    using (ScanDocumentRep rep = new ScanDocumentRep())
                    {
                        rep.InsertData(new ScanDocument {
                            f_ID = model.f_ID,
                            File_GUID = item.ID,
                            File_Type = item.Type,
                            ScanDocName = item.Name,
                            ScanDocPath = dirPath,
                            CreateDate = model.CreateDate,
                            CreateBy = model.CreateBy
                        });
                    }

                    var Fullpath = Path.Combine(dirPath, string.Format("{0}{1}", item.ID, item.Name));
                    var imgByteArray = Convert.FromBase64String(item.EncodeBase64);
                    System.IO.File.WriteAllBytes(Fullpath, imgByteArray);

                });

                Result = "Upload Data Success!!";
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteFileUpload(ScanDocument model)
        {
            var Result = string.Empty;

            try
            {
                using (ScanDocumentRep rep = new ScanDocumentRep())
                {
                    var Fullpath = Path.Combine(model.ScanDocPath, string.Format("{0}{1}",model.File_GUID ,model.ScanDocName));

                    rep.DeleteData(model);

                    //Delete File
                    if (System.IO.File.Exists(Fullpath))
                    {
                        System.IO.File.Delete(Fullpath);
                    }

                    Result = "Delete Data Success!!";

                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }


        [HttpPost]
        public JsonResult SelectDataView(ScanDocument model)
        {
            using (ScanDocumentRep rep = new ScanDocumentRep())
            {
                var query = (from a in rep.GetAll()
                             where a.S_ID == model.S_ID
                             select a).FirstOrDefault();


                return Json(new { filename = string.Format("{0}{1}",query.File_GUID,query.ScanDocName), type= query.File_Type, filepath = query.ScanDocPath });

            } 
        }

        [HttpPost]
        public JsonResult SelectDataViewHistory(msFileScanHistory model)
        {
            using (msFileScanHistoryRep rep = new msFileScanHistoryRep())
            {
                var query = (from a in rep.GetAll()
                             where a.fh_ID == model.fh_ID
                             select a).FirstOrDefault();


                return Json(new { filename = string.Format("{0}{1}", query.File_GUID, query.File_Name), type = query.File_Type,filepath = query.File_Path });

            }
        }

        [HttpGet]
        public FileResult Download(string strfilename , string contype, string path)
        {
            //var dirPath = "D:\\fileupload\\";
            return File(Path.Combine(path, strfilename) ,contype);
        }

        [HttpPost]
        public JsonResult SaveChangefindAsset(trfindAsset model, string status)
        {
            var Result = string.Empty;

            try
            {

                using (trfindAssetRep rep = new trfindAssetRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(model);
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(model);
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

                switch (model.findType)
                {
                    case "2":
                        using (LegalTabRep le = new LegalTabRep())
                        {
                            if (model.SeizeDate.Value != null)
                            {
                                le.UpdateDatalegalStatus(model.RequestID.Value, "303");
                                return Json(new { legalstatus = "303", message = Result });
                            }
                            if (model.DistrainDate.Value != null)
                            {
                                le.UpdateDatalegalStatus(model.RequestID.Value, "304");
                                return Json(new { legalstatus = "304", message = Result });
                            }


                        }
                        break;
                    default:
                        using (LegalTabRep le = new LegalTabRep())
                        {
                            le.UpdateDatalegalStatus(model.RequestID.Value, "300");
                        }
                        return Json(new { legalstatus = "300", message = Result });
                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteDataAccessDesc(AssetDesc model)
        {
            var Result = string.Empty;

            try
            {

                using (AssetDescRep rep = new AssetDescRep())
                {
                    rep.DeleteData(model);
                    Result = "Delete Data Success!!";
                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteDatafindAsset(trfindAsset model)
        {
            var Result = string.Empty;

            try
            {
                using (AssetDescRep ad = new AssetDescRep())
                {
                    var listAD = (from a in ad.GetAll()
                                   where a.f_ID == model.f_ID
                                   select a).ToList();

                    listAD.ForEach(item => 
                    {
                        ad.DeleteData(item);
                    });
                }

                using (ScanDocumentRep SD = new ScanDocumentRep())
                {
                    var listSD = (from a in SD.GetAll()
                                   where a.f_ID == model.f_ID
                                   select a).ToList();

                    listSD.ForEach(item => 
                    {
                        var Fullpath = Path.Combine(item.ScanDocPath, string.Format("{0}{1}", item.File_GUID, item.ScanDocName));

                        //Delete File
                        if (System.IO.File.Exists(Fullpath))
                        {
                            System.IO.File.Delete(Fullpath);
                        }

                        SD.DeleteData(item);
                    });
                }


                using (trfindAssetRep rep = new trfindAssetRep())
                {
                    rep.DeleteData(model);
                    Result = "Delete Data Success!!";
                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult ListCourtZone()
        {
            try
            {
                using (LegalSystemEntities _context = new LegalSystemEntities())
                {
                    var query = (from a in _context.msCourtZones.ToList()
                                 select new Select2Model
                                 {
                                     id = a.CourtZoneID.ToString(),
                                     text = string.Format("ศาล-{0}", a.CourtZoneName),
                                 }).ToList();
                                 //select new System.Web.Mvc.SelectListItem
                                 //{
                                 //    Text = string.Format("ศาล-{0}", a.CourtZoneName),
                                 //    Value = a.CourtZoneID.ToString()
                                 //});

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult ListLawOffice()
        {
            try
            {
                using (LegalSystemEntities _context = new LegalSystemEntities())
                {
                    var query = (from a in _context.msLawOffices.ToList()
                                 select new Select2Model
                                 {
                                     id = a.LO_ID.ToString(),
                                     text = a.LO_Name
                                 }).ToList();
                                 //select new System.Web.Mvc.SelectListItem
                                 //{
                                 //    Text =  a.LO_Name,
                                 //    Value = a.LO_ID.ToString()
                                 //});

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult SaveChangeWealthPay(trWealthPay model, string status)
        {
            var Result = string.Empty;

            try
            {
                using (trWealthPayRep rep = new trWealthPayRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(model);
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(model);
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteDataWealthPay(trWealthPay model)
        {
            var Result = string.Empty;

            try
            {

                using (trWealthPayRep rep = new trWealthPayRep())
                {
                    rep.DeleteData(model);
                    Result = "Delete Data Success!!";
                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult InquiryWealthPay(DatableOption option, trWealthPay model)
        {
            using (trWealthPayRep rep = new trWealthPayRep())
            {
                var query = (from a in rep.GetAll()
                             where a.RequestID == model.RequestID && a.W_Type == model.W_Type
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_Date) : query.OrderByDescending(r => r.W_Date)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_Desc) : query.OrderByDescending(r => r.W_Desc)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_Fee) : query.OrderByDescending(r => r.W_Fee)); break;
                    case 4: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_LegalDate) : query.OrderByDescending(r => r.W_LegalDate)); break;
                    case 5: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_CourtDate) : query.OrderByDescending(r => r.W_CourtDate)); break;
                    case 6: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_SendDate) : query.OrderByDescending(r => r.W_SendDate)); break;
                    case 7: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_ReciveMoneyDate) : query.OrderByDescending(r => r.W_ReciveMoneyDate)); break;
                    case 8: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_Money) : query.OrderByDescending(r => r.W_Money)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.W_Date) : query.OrderByDescending(r => r.W_Date)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult SaveChangeAuctionNew(trAuctionNew model, string status)
        {
            var Result = string.Empty;

            try
            {
                using (trAuctionNewRep rep = new trAuctionNewRep())
                {
                    switch (status)
                    {
                        case "Add":
                            {
                                rep.InsertData(model);
                                Result = "Insert Data Success!!";
                                break;
                            }
                        case "Edit":
                            {
                                rep.UpdateData(model);
                                Result = "Update Data Success!!";
                                break;
                            }

                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

                //Update Legal Status
                using (LegalTabRep le = new LegalTabRep())
                {
                    le.UpdateDatalegalStatus(model.RequestID.Value, "305");
                }
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteDataAuctionNew(trAuctionNew model)
        {
            var Result = string.Empty;

            try
            {

                using (trAuctionNewRep rep = new trAuctionNewRep())
                {
                    rep.DeleteData(model);
                    Result = "Delete Data Success!!";
                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult InquiryAuctionNew(DatableOption option, trAuctionNew model)
        {
            using (trAuctionNewRep rep = new trAuctionNewRep())
            {
                var query = (from a in rep.GetAll()
                             where a.RequestID == model.RequestID 
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.SellDate) : query.OrderByDescending(r => r.SellDate)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.Remark) : query.OrderByDescending(r => r.Remark)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.SellDate) : query.OrderByDescending(r => r.SellDate)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }


        [HttpPost]
        public JsonResult ListFileScanType()
        {
            try
            {
                using (msFileScanGroupRep rep = new msFileScanGroupRep())
                {
                    var query = (from a in rep.GetAll()
                                 select new System.Web.Mvc.SelectListItem
                                 {
                                     Text = a.fg_Name,
                                     Value = a.fg_ID.ToString()
                                 });

                    return Json(query);
                }
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult InquiryFileScan(DatableOption option, msFileScan model)
        {
            using (msFileScanRep rep = new msFileScanRep())
            {
                var query = (from a in rep.GetView()
                             where a.RequestID == model.RequestID
                             && (string.IsNullOrEmpty(model.File_Name) || a.File_Name == model.File_Name)
                             && (model.fg_ID == null || a.fg_ID == model.fg_ID)
                             select a);

                switch (option.sortingby)
                {
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.fg_Name) : query.OrderByDescending(r => r.fg_Name)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Name) : query.OrderByDescending(r => r.File_Name)); break;
                    case 3: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Version) : query.OrderByDescending(r => r.File_Version)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.fg_Name) : query.OrderByDescending(r => r.fg_Name)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }
        [HttpPost]
        public JsonResult InquiryFileScanHistory(DatableOption option, msFileScanHistory model)
        {
            using (msFileScanHistoryRep rep = new msFileScanHistoryRep())
            {
                var query = (from a in rep.GetAll()
                             where a.fs_ID == model.fs_ID
                             select a);

                switch (option.sortingby)
                {
                  
                    case 1: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Name) : query.OrderByDescending(r => r.File_Name)); break;
                    case 2: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Version) : query.OrderByDescending(r => r.File_Version)); break;
                    default: query = (option.orderby == "asc" ? query.OrderBy(r => r.File_Name) : query.OrderByDescending(r => r.File_Name)); break;
                }

                var datas = query.Skip(option.start).Take(option.length).ToList();
                var recordsTotal = query.Count();

                var result = new JsonDataTable
                {
                    status = true,
                    message = "success",
                    data = datas,
                    draw = option.draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsTotal
                };

                return Json(result);
            }
        }
        [HttpPost]
        public JsonResult UploadFileScan(List<FileUploaeModel> files, msFileScan model, string status)
        {
            var Result = string.Empty;

            try
            {
                var F = files.FirstOrDefault();
                //var dirPath = "D:\\fileScan_Upload\\";
                var dirPath = System.Configuration.ConfigurationManager.AppSettings["PathFilScanUpload"].ToString();
                var fileName = F.Name;

                //Create Floder
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }

                files.ForEach(item =>
                {

                    using (msFileScanRep rep = new msFileScanRep())
                    {
                        switch (status)
                        {
                            case "Edit":
                                {
                                    rep.UpdateData(new msFileScan
                                    {
                                        fs_ID = model.fs_ID,
                                        RequestID = model.RequestID,
                                        fg_ID = model.fg_ID,
                                        File_GUID = item.ID,
                                        File_Type = item.Type,
                                        File_Name = item.Name,
                                        File_Path = dirPath,
                                        File_Version = model.File_Version + 1,
                                        CreateDate = DateTime.Now,
                                        CreateBy = model.CreateBy
                                    });
                                    break;
                                }
                            default:
                                {
                                    rep.InsertData(new msFileScan
                                    {
                                        RequestID = model.RequestID,
                                        fg_ID = model.fg_ID,
                                        File_GUID = item.ID,
                                        File_Type = item.Type,
                                        File_Name = item.Name,
                                        File_Path = dirPath,
                                        File_Version = 1,
                                        CreateDate = model.CreateDate,
                                        CreateBy = model.CreateBy
                                    });
                                    break;
                                }
                        }

                        //เก็บ History
                        using (msFileScanHistoryRep rep2 = new msFileScanHistoryRep())
                        {
                            var modelfileScan = (from a in rep.GetAll()
                                                 where a.File_GUID == item.ID
                                                 select a).FirstOrDefault();
                            
                            if (modelfileScan != null)
                            {
                                rep2.InsertData(new msFileScanHistory
                                {
                                    fs_ID = modelfileScan.fs_ID,
                                    File_GUID = modelfileScan.File_GUID,
                                    File_Type = modelfileScan.File_Type,
                                    File_Name = modelfileScan.File_Name,
                                    File_Path = modelfileScan.File_Path,
                                    File_Version = modelfileScan.File_Version
                                });
                            }

                        }

                    }


                    var Fullpath = Path.Combine(dirPath, string.Format("{0}{1}", item.ID, item.Name));
                    var imgByteArray = Convert.FromBase64String(item.EncodeBase64);
                    System.IO.File.WriteAllBytes(Fullpath, imgByteArray);
                });

                Result = "Upload File Success!!";
            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }

        [HttpPost]
        public JsonResult DeleteFileScan(msFileScan model)
        {
            var Result = string.Empty;
            var Fullpath = string.Empty;

            try
            {
                using (msFileScanRep rep = new msFileScanRep())
                {
                    using (msFileScanHistoryRep rep2 = new msFileScanHistoryRep())
                    {
                        var modelhistory = (from a in rep2.GetAll()
                                            where a.fs_ID == model.fs_ID
                                            select a).ToList();

                        if (modelhistory != null)
                        {
                            modelhistory.ForEach(item =>
                            {
                                rep2.DeleteData(item);
                            });
                           
                        }
                    }

                    rep.DeleteData(model);

                   
                    if (model.File_Version > 1)
                    {
                        var d = new DirectoryInfo(model.File_Path);
                        var Files = d.GetFiles("*" + model.File_Name);

                        foreach (FileInfo file in Files)
                        {
                            //Delete File
                            if (System.IO.File.Exists(file.FullName))
                            {
                                System.IO.File.Delete(file.FullName);
                            }
                        }
                    }
                    else
                    {
                        Fullpath = Path.Combine(model.File_Path, string.Format("{0}{1}", model.File_GUID, model.File_Name));

                        //Delete File
                        if (System.IO.File.Exists(Fullpath))
                        {
                            System.IO.File.Delete(Fullpath);
                        }

                    }

                    Result = "Delete File Upload Success!!";

                }

            }
            catch (Exception ex)
            {
                Result = ex.Message;
            }

            return Json(Result);
        }
    }
}