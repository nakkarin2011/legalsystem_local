﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using LegalData.Model;
using LegalData.Model.EF;
using LegalData.Model.Request;
using LegalSystem.App_Start;
using LegalSystem.Common;
using LegalSystem.Model;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static LegalSystem.Common.GlobalSession;

namespace LegalSystem.Controllers
{

    public class TrayController : Controller
    {
        private LegalData.Repository.LegalEntities business = new LegalData.Repository.LegalEntities();
        private LegalData.LegalSystemEntities db = new LegalData.LegalSystemEntities();

        [SessionTimeout]
        public ActionResult Tray1()
        {
            TrayViewModel model = new TrayViewModel();

            var DocumentTypes = db.msDocumentTypes
                .Where(o => o.ReqTypeID.Equals(1) || o.ReqTypeID.Equals(2))
                .Select(p => new { Value = p.DocTypeID, Text = p.DocTypeName });

            ViewBag.DocumentTypeID = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.DocumentStatusID = new SelectList(db.msDocumentStatus.Select(p => new { Value = p.DocStatusID, Text = p.DocStatusName ?? p.DocStatusDesc }), "Value", "Text");

            return View(model);
        }

        [SessionTimeout]
        public ActionResult Tray2()
        {
            TrayViewModel model = new TrayViewModel();

            var DocumentTypes = db.msDocumentTypes
                .Where(o => o.ReqTypeID.Equals(3) || o.ReqTypeID.Equals(4) || o.ReqTypeID.Equals(5))
                .Select(p => new { Value = p.DocTypeID, Text = p.DocTypeName });

            ViewBag.DocumentTypeID = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.DocumentStatusID = new SelectList(db.msDocumentStatus.Select(p => new { Value = p.DocStatusID, Text = p.DocStatusName ?? p.DocStatusDesc }), "Value", "Text");

            return View(model);
        }

        [SessionTimeout]
        public ActionResult Tray3()
        {
            TrayViewModel model = new TrayViewModel();
            var DocumentTypes = db.msDocumentTypes
                  .Where(o => o.ReqTypeID.Equals(6) || o.ReqTypeID.Equals(7) || o.ReqTypeID.Equals(8) || o.ReqTypeID.Equals(9) || o.ReqTypeID.Equals(10) || o.ReqTypeID.Equals(18))
                  .Select(p => new { Value = p.DocTypeID, Text = p.DocTypeName });

            ViewBag.DocumentTypeID = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.DocumentStatusID = new SelectList(db.msDocumentStatus.Select(p => new { Value = p.DocStatusID, Text = p.DocStatusName ?? p.DocStatusDesc }), "Value", "Text");

            return View(model);
        }

        [SessionTimeout]
        public ActionResult Tray4()
        {
            TrayViewModel model = new TrayViewModel();
            var DocumentTypes = db.msDocumentTypes
                  .Where(o => o.ReqTypeID.Equals(1) || o.ReqTypeID.Equals(8) || o.ReqTypeID.Equals(9) || o.ReqTypeID.Equals(10))
                  .Select(p => new { Value = p.DocTypeID, Text = p.DocTypeName });

            ViewBag.DocumentTypeID = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.DocumentStatusID = new SelectList(db.msDocumentStatus.Select(p => new { Value = p.DocStatusID, Text = p.DocStatusName ?? p.DocStatusDesc }), "Value", "Text");

            return View(model);
        }

        [SessionTimeout]
        public ActionResult Tray5()
        {
            TrayViewModel model = new TrayViewModel();
            var DocumentTypes = db.msDocumentTypes
         .Where(o => o.ReqTypeID.Equals(8) || o.ReqTypeID.Equals(11) || o.ReqTypeID.Equals(12))
         .Select(p => new { Value = p.DocTypeID, Text = p.DocTypeName });

            ViewBag.DocumentTypeID = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.DocumentStatusID = new SelectList(db.msDocumentStatus.Select(p => new { Value = p.DocStatusID, Text = p.DocStatusName ?? p.DocStatusDesc }), "Value", "Text");

            return View(model);
        }

        [HttpPost]
        [SessionTimeout]
        public JsonResult Search(DataTableModel model, int TrayID)
        {
            var columns = model.columns.Where(o => !string.IsNullOrEmpty(o.search.value)).ToList();
            var filters = new List<FieldModel>();
            filters.Add(new FieldModel { Name = "TrayID", Value = TrayID, DataType = System.Data.SqlDbType.Int });

            //System Parameter
            filters.Add(new FieldModel { Name = "PermissionID", Value = ((PermissionID)GlobalSession.GetPermission).ToString() });
            filters.Add(new FieldModel { Name = "UserID", Value = GlobalSession.UserLogin.Emp_Code });

            columns.ForEach(column =>
            {
                filters.Add(new FieldModel { Name = column.data, Value = column.search.value });
            });

            var orders = new List<OrderByModel>();
            if (model.order != null)
            {
                model.order.ForEach(o =>
                {
                    var col = model.columns[o.column];
                    orders.Add(new OrderByModel { Name = col.data, SortDirection = (o.dir.Equals("desc") ? SortDirection.Descending : SortDirection.Ascending) });
                });
            }

            var result = business.Tray.Search(new BaseParameterModel()
            {
                Paging = new PagingModel { Start = model.start, Limit = model.length },
                Orders = orders,
                Parameters = filters
            });

 



            return Json(new
            {
                draw = model.draw,
                recordsTotal = result.Total,
                recordsFiltered = result.Total,
                data = result.Data.Trays
            });
        }

        [HttpPost]
        public async Task<JsonResult> TrayActionAsync(int trayID)
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestSignatureModel rsm = new RequestSignatureModel();

            try
            {
                var signature = Request.Headers.GetValues("signature").FirstOrDefault();
                var SignatureResult = business.System.RequestSignature(signature);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success   = SignatureResult.Success;
                    rwm.Message   = SignatureResult.Message;
                    rwm.RefCode   = SignatureResult.RefCode;
                    rwm.Serverity = SignatureResult.Serverity;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                var _request =   await db.trRequests.FindAsync(new Guid(rsm.RequestID));

                if (_request == null) {


                    rwm.RefCode = 404;
                    rwm.Success = false;
                    rwm.Message = "Data not found";

                    return Json(rwm );
                }

                int RequestTypeID = 0;

                if (int.TryParse(_request.RequestType, out RequestTypeID))
                {
                    var result = business.Tray.GetTrayActionDocument(RequestTypeID, trayID);

                    rwm.Success = result.Success;
                    rwm.Message = result.Message;
                    rwm.Serverity = result.Serverity;
                    rwm.Total = result.Total;
                    rwm.RefCode = result.RefCode;

                    if (result.Success)
                    {
                        rwm.Data = result.Data;
                    }
                }

                else {
                    rwm.RefCode = 500;
                    rwm.Success = false;
                    rwm.Message = "String request type cannot be converted into Integer";
                }

            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }
 
        [HttpPost]
        public JsonResult ButtonPermission()
        {
            ResultWithModel rwm = new ResultWithModel();
            RequestSignatureModel rsm = new RequestSignatureModel(); 

            try
            {
                var signature       = Request.Headers.GetValues("signature").FirstOrDefault();
                var SignatureResult = business.System.RequestSignature(signature);

                if (SignatureResult.Success)
                {
                    rsm = SignatureResult.Data;
                }

                else
                {
                    rwm.Success     = SignatureResult.Success    ;
                    rwm.Message     = SignatureResult.Message    ;
                    rwm.RefCode     = SignatureResult.RefCode    ;
                    rwm.Serverity   = SignatureResult.Serverity  ;

                    return Json(rwm, JsonRequestBehavior.AllowGet);
                }

                //get permisstion
               var ButtonPermissionresult= business.Tray.GetButtonPermission(new BaseParameterModel()  {
                    Parameters = new List<FieldModel>() {
                          new FieldModel { Name = "PermissionID", Value =  rsm.PermissionID }
                        , new FieldModel { Name = "UserID", Value =  rsm.UserID }
                        , new FieldModel { Name = "RequestID", Value = rsm.RequestID }
                    }
                });

                rwm.Success     = ButtonPermissionresult.Success    ;
                rwm.Message     = ButtonPermissionresult.Message    ;
                rwm.RefCode     = ButtonPermissionresult.RefCode    ;
                rwm.Serverity   = ButtonPermissionresult.Serverity  ;

                if (ButtonPermissionresult.Success)
                {
                   rwm.Data = ButtonPermissionresult.Data.Permissions.FirstOrDefault();
                }
             }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm, JsonRequestBehavior.AllowGet);
        }
 
        [HttpPost]
        public async Task<JsonResult> Send(string id)
        {
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                var Request = await db.trRequests.FindAsync(new Guid(id));

                if (Request == null)
                {
                    rwm.Success = false;
                    rwm.Message = "Data not found.";

                    return Json(rwm);
                }

                if (Request.DocTypeID != null)
                {
                    //Update status to Waiting to approve                                                                                                                                                                                                                                                
                    Request.DocStatusID = 1;
                    await db.SaveChangesAsync();
                }

                rwm.Success = true;
                rwm.Message = "Successfully saved.";
                rwm.Data = Request;
            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm);
        }
        [HttpPost]
        public async Task<JsonResult> Approve(string id)
        {
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                var Request = await db.trRequests.FindAsync(new Guid(id));

                if (Request == null)
                {
                    rwm.Success = false;
                    rwm.Message = "Data not found.";

                    return Json(rwm);
                }

                if (Request.DocTypeID == null)

                {
                    if (Request.RequestType.Equals("2"))
                    {
                        Request.DocTypeID = 11;
                        Request.DocStatusID = 3;

                        await db.SaveChangesAsync();
                    }
                }

                else
                {
                    Request.DocStatusID = 3;

                    await db.SaveChangesAsync();
                }

                rwm.Success = true;
                rwm.Message = "Successfully saved.";
                rwm.Data = Request;
            }

            catch (Exception ex)
            {
                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm);
        }
        [HttpPost]
        public async Task<JsonResult> Return(string id)
        {
            ResultWithModel rwm = new ResultWithModel();

            try
            {
                var Request = await db.trRequests.FindAsync(new Guid(id));

                if (Request == null)
                {
                    rwm.Success = false;
                    rwm.Message = "Data not found.";

                    return Json(rwm);
                }

                if (Request.DocTypeID != null)
                {
                    //Update status to  return                                                                                                                                                                                                                                            
                    Request.DocStatusID = 2;

                    await db.SaveChangesAsync();
                }

                rwm.Success = true;
                rwm.Message = "Successfully saved.";
                rwm.Data = Request;

            }

            catch (Exception ex)
            {

                rwm.RefCode = 500;
                rwm.Success = false;
                rwm.Message = ex.Message;
            }

            return Json(rwm);
        }
  
    }
}
