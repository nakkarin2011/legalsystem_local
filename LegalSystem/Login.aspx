﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LegalSystem.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>::: Legal System :::</title>
    <link href="/LegalSystem_DEV/Content/TCRBStyle.css" rel="stylesheet" />

    <%-- -------detault-------%>
    <link href="/LegalSystem_DEV/Content/Site.css" rel="stylesheet" />
    <link href="/LegalSystem_DEV/Content/bootstrap-plugin-pages.css" rel="stylesheet" />

    <%-- -------vendor--------%>
    <link href="/LegalSystem_DEV/Content/vendor/material-dashboard.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <%--<style>
        .Mabody {
            background: url('/LegalSystem_DEV/Images/astronomy.jpeg');
            background-size: cover;
            background-repeat: no-repeat;
            width: 100vw;
            height: 100vh;
        }    
    </style>--%>
</head>
<body class="my-container" style="background-color: #1f283e; opacity: .94;">
    <div style="padding-top: 15%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
                    <form class="form" runat="server">
                        <div class="card card-login" style="padding-bottom: 1em;">
                            <div class="card-header card-header-tabs text-center" style="background-color: #333; /*opacity: .94; */">
                                <%--<h3 class="card-title">Legal System</h3>--%>
                                <div class="social-line">
                                    <img src="/LegalSystem_DEV/Images/TCR_Logo_ORG.png" alt="" />
                                    <%--<img src="Images/TCR_Logo.png" alt="" />--%>
                                </div>
                            </div>
                            <div class="card-body">
                                <%--<p class="card-description text-center">~ User Login ~</p>--%>
                                <h3 class="text-center" style="padding-bottom: 0.5em">Legal System</h3>
                                <span class="bmd-form-group">
                                    <div class="input-group col-md-11" >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">person</i>
                                            </span>
                                        </div>
                                        <input id="txtUserName" runat="server" type="text" class="form-control" style="padding-left: 0.5em" placeholder=" UserName ..." required />
                                    </div>
                                </span>
                                <br>
                                <span class="bmd-form-group">
                                    <div class="input-group  col-md-11">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">lock</i>
                                            </span>
                                        </div>
                                        <input id="txtPassword" runat="server" type="password" class="form-control" placeholder=" Password ..." style="padding-left: 0.5em" required />
                                    </div>
                                </span>
                                <span class="bmd-form-group">
                                    <div id="dvMessageBox" class="col-md-11" style="color: red">
                                        <asp:Panel ID="PanelError" runat="server" Style="width: 100%; vertical-align: middle;" Visible="false">
                                            <asp:Image ID="imgerror" runat="server" Height="16px" Width="16px" Style="display: inline;" ImageAlign="AbsMiddle" ImageUrl="~/Images/Exclamation.png" />
                                            <asp:Label ID="lblError" runat="server" CssClass="labelerror" ForeColor="Red"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </span>
                            </div>
                            <div class="card-footer justify-content-center">
                                <asp:Button ID="btnLogin" runat="server" class="btn btn-lg" Text="Log in" Style="background: #ff9933; color: white;" OnClick="btnLogin_Click" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
