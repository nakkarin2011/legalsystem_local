﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using LegalData.Repository;
using LegalSystem.Common;

namespace LegalSystem.Extensions
{
    public static class NavigationExtension
    {
      
        public static MvcHtmlString Navigation(this HtmlHelper helper)
        {
            LegalData.Repository.LegalEntities legal = new LegalData.Repository.LegalEntities();
                 
            StringBuilder navBuilder = new StringBuilder();

            try
            {
                var menuResult = legal.Menu.GetMenus(GlobalSession.GetPermission.ToString());

                if (menuResult.Success)
                {
                    var Menus = menuResult.Data.Menus; 

                    navBuilder.AppendLine("<ul class=\"nav collapse\">");

                    Menus.ForEach(menu =>  {

                        navBuilder.AppendLine("<li class=\"nav-item\">");

                        if (menu.SubMenus.Any())
                        {
                            var RefID = Guid.NewGuid().ToString(); 

                            navBuilder.AppendLine($"<a class=\"nav-link\" data-toggle=\"collapse\" href=\"#{RefID}\" aria-expanded=\"false\" aria-controls=\"{RefID}\">");
                            navBuilder.AppendLine($"    <i class=\"{menu.IconClass}\"></i>");
                            navBuilder.AppendLine($"    <p class=\"dropdown-toggle\">{menu.MenuName}</p>");
                            navBuilder.AppendLine($"</a>");

                            //check 
                            if (menu.SubMenus.Count() > 5)
                            {
                                navBuilder.AppendLine($"<div class=\"collapse\" id=\"{RefID}\" style=\"max-height: 300px; overflow-y: scroll;\">");
                            }

                            else
                            {
                                navBuilder.AppendLine($"<div class=\"collapse\" id=\"{RefID}\">");
                            }

                            navBuilder.AppendLine($"    <ul class=\"nav nav-item\">");

                            menu.SubMenus.ForEach(submenu =>
                            {
                                navBuilder.AppendLine($"<li class=\"nav-item\">");
                                navBuilder.AppendLine($"    <a class=\"nav-link\" href=\"{submenu.Url}\">");
                                navBuilder.AppendLine($"       <span class=\"sidebar-mini\">{submenu.ShortName}</span>");
                                navBuilder.AppendLine($"       <span class=\"sidebar-normal\">{submenu.MenuName}</span>");
                                navBuilder.AppendLine($"    </a>");
                                navBuilder.AppendLine($"</li>");

                            });

                            navBuilder.AppendLine($"    </ul>");
                            navBuilder.AppendLine($"</div>");

                        }

                        else
                        {

                            navBuilder.AppendLine($"<a class=\"nav-link\" href=\"{menu.Url}\">");
                            navBuilder.AppendLine($"    <i class=\"{menu.IconClass}\"></i>");
                            navBuilder.AppendLine($"    <p>{menu.MenuName}</p>");
                            navBuilder.AppendLine($"</a>");
                        }

                        navBuilder.AppendLine("</li>");
                    });

                    navBuilder.AppendLine("</ul>");
                }

                else
                {
                    navBuilder.AppendLine("Get navigation data failed.");
                }
            }

            catch  
            {
                navBuilder.AppendLine("Render Navigation Failed.");

            }

            return new MvcHtmlString(navBuilder.ToString());
        }
    }
}