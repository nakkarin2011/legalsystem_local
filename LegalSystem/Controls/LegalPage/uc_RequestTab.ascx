﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_RequestTab.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_RequestTab" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Panel ID="Panel1" runat="server">
    <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="card ">
                <div class="card-header card-header-tabs card-header-info">
                    <h4 class="card-title">Request Infomation</h4>
                </div>
                <div class="card-body ">
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="row">
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Request No :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtRequestNo" runat="server" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Request Date :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtRequestDate" runat="server" class="datepicker form-control" disabled />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Request Status :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlRequestStatus" runat="server" CssClass="form-control" DataValueField="ReqStatusID" DataTextField="ReqStatusName" disabled></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Request Type</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlRequestType" runat="server" CssClass="form-control" DataValueField="ReqTypeID" DataTextField="ReqTypeName" Enabled="false"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Refer Request No :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtReferRequestNo" runat="server" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Refer Request Type :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtReferRequestType" runat="server" class="form-control" autocomplete="off" readonly />
                                <asp:HiddenField ID="hidReferRequestType" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">CIF</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtCIF" runat="server" class="form-control" readonly />
                            </div>
                        </div>
                        <%--<div class="col-md-1 col-sm-1 text-right">
                                                            <asp:ImageButton ID="btnFindCIF" runat="server" CssClass="btn btn-outline-success" ImageUrl="~/Images/Search.png" AutoPostBack="true" Visible="false" OnClick="btnFindCIF_Click" />
                                                        </div>--%>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Legal No :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtLegalNo" runat="server" class="form-control" autocomplete="off" readonly />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Legal Status :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlLegalStatus" runat="server" CssClass="form-control" DataValueField="ActionCode" DataTextField="LegalStatusDisplay" disabled></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Address :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtAddress" runat="server" class="form-control" autocomplete="off" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-warning">
                                    <h4 class="card-title">บัญชีสินเชื่อ</h4>
                                    <p class="card-category">
                                        <asp:Label ID="Label1" runat="server"><text style="color:#e6f2ff; font-style:italic;">กรุณาเลือกอย่างน้อย 1 บัญชี </text></asp:Label>
                                    </p>
                                </div>
                                <div class="card-body">
                                    <asp:GridView ID="gvAccountLoan" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                        ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="Center" DataKeyNames="ACCTNO" AllowPaging="false" AllowSorting="false" ShowHeader="false"
                                        OnRowDataBound="gvAccountLoan_RowDataBound" OnRowCommand="gvAccountLoan_RowCommand" OnSelectedIndexChanged="gvAccountLoan_SelectedIndexChanged">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="card">
                                                        <div class="card-body" style="background-color: #F0F0F0">
                                                            <div class="row">
                                                                <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                                                                    <div class="form-check">
                                                                        <label class="form-check-label">
                                                                            <input class="form-check-input" type="checkbox" id="chkAccount" runat="server" disabled /><span class="form-check-sign"><span class="check"></span></span></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 text-right">
                                                                    <div class="tim-typo mt-4-5">
                                                                        <h5>
                                                                            <span class="tim-note">Account :</span>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                    <div class="form-group">
                                                                        <input type="text" id="txtAcctLoan" runat="server" class="form-control" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-sm-2 text-right">
                                                                    <div class="tim-typo mt-4-5">
                                                                        <h5>
                                                                            <span class="tim-note">Account Status :</span>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                    <div class="form-group">
                                                                        <input type="text" id="txtAcctStatus" runat="server" class="form-control" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1 col-sm-1 text-right">
                                                                    <div class="tim-typo mt-4-5">
                                                                        <h5>
                                                                            <span class="tim-note">DPD :</span>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                    <div class="form-group">
                                                                        <input type="text" id="txtDPD" runat="server" class="form-control" readonly />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 text-right">
                                                                    <div class="tim-typo mt-4-5">
                                                                        <h5>
                                                                            <span class="tim-note">Collateral Type :</span>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9 col-lg-9 col-sm-1 col-1 ">
                                                                    <div class="form-group">
                                                                        <input type="text" id="txtCollateralType" runat="server" class="form-control" readonly />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 col-centered" style="background-color: white">
                                                                    <div class="wizard-navigation">
                                                                        <ul class="nav nav-pills">
                                                                            <li class="nav-item">
                                                                                <a class="nav-link active" href='#<%= this.ClientID + "_tabDeposit_" %><%#Eval("ACCTNO")%>' data-toggle="tab" role="tab">Account Infomation
                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" href='#<%= this.ClientID + "_tabLoan_" %><%#Eval("ACCTNO")%>' data-toggle="tab" role="tab">Loan Infomation
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane active" id='<%= this.ClientID + "_tabDeposit_" %><%#Eval("ACCTNO")%>'>
                                                                            <div class="row justify-content-center">
                                                                                <div class="col-md-12">
                                                                                    <asp:GridView ID="gvRelationship" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                                                        ShowHeaderWhenEmpty="true" DataKeyNames="A_CFCIFN" AllowPaging="false" AllowSorting="false" OnRowDataBound="gvRelationship_RowDataBound">
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:HiddenField ID="hidCIF" runat="server" Value='<%#Eval("A_CFCIFN") %>' />
                                                                                                    <asp:HiddenField ID="hidType" runat="server" Value='<%#Eval("A_CFRELA") %>' />
                                                                                                    <asp:HiddenField ID="hidOrder" runat="server" Value='<%#Eval("RelationOrder") %>' />
                                                                                                    <div class="form-check">
                                                                                                        <label class="form-check-label">
                                                                                                            <input class="form-check-input" type="checkbox" id="chkRelationship" runat="server" onclick="OnDeSelectAcctLoanAll(this)" disabled />
                                                                                                            <span class="form-check-sign">
                                                                                                                <span class="check"></span>
                                                                                                            </span>
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="RelationName" SortExpression="RelationName" HeaderText="Relationship" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField DataField="A_CFCIFN" SortExpression="A_CFCIFN" HeaderText="CIF No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField DataField="A_CFNAME" SortExpression="A_CFNAME" HeaderText="CIF Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField DataField="A_CFSSNO" SortExpression="A_CFSSNO" HeaderText="Citizen ID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField DataField="ACC_NO" SortExpression="ACC_NO" HeaderText="Deposit A/C" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                            <asp:BoundField DataField="CUR_BAL01" SortExpression="CUR_BAL01" HeaderText="Deposit Outstanding" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ItemStyle-Width="120px"></asp:BoundField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id='<%= this.ClientID + "_tabLoan_" %><%#Eval("ACCTNO")%>'>
                                                                            <div class="row justify-content-center">
                                                                                <div class="col-md-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <span class="tim-note">Loan Type :</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtLoanType" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">เงินต้น :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtCurBalAmt" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">วงเงินกู้ :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtOriginalAmt" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">ดอกเบี้ยปกติ :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtAcctInterest" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">อัตราดอกเบี้ย % :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtInterestRate" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">ดอกเบี้ยค้างรับตั้งพัก :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtACCI3" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">จำนวนวันค้างชำระ :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtNumOfDPD" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">ดอกเบี้ยผิดนัดตั้งพัก :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtPENIN3" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">ดอกเบี้ยผิดนัดชำระล่าช้า :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtPENINT" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <span class="tim-note">ค่าใช้จ่ายอื่นๆ :</span>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtPOTHER" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                        <div class="col-md-3 col-sm-3 text-right">
                                                                                            <div class="tim-typo mt-4-5">
                                                                                                <h5>
                                                                                                    <strong class="tim-note">รวม :</strong>
                                                                                                </h5>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                            <div class="form-group">
                                                                                                <input type="text" id="txtTotal" runat="server" class="form-control text-right" readonly />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="card" id="divAttachments" runat="server">
                                <div class="card-header card-header-tabs card-header-warning">
                                    <h4 class="card-title">สิ่งที่แนบมาด้วย</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" id="chk1" runat="server" checked="checked" disabled />
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>หนังสือบอกกล่าวผู้ค้ำประกัน พร้อมไปรษณีย์ตอบรับ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="col-sm-10 checkbox-radios">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="chk11" runat="server" checked="checked" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        มี
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label tim-note">
                                                        <input class="form-check-input" type="checkbox" id="chk12" runat="server" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        ไม่มี เพราะไม่มีผู้ค้ำประกัน
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" id="chk2" runat="server" checked="checked" disabled />
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>หนังสือแจ้งวันขายทอดตลาดหลักทรัพย์จำนำ พร้อมไปรษณีย์ตอบรับ</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" id="chk21" runat="server" checked="checked" disabled />
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>ใบรายงานการประมูล</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" id="chk3" runat="server" checked="checked" disabled />
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>มีบัญชีเงินฝาก</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="col-sm-10 checkbox-radios">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="chk31" runat="server" checked="checked" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        มี ตัดบัญชีเงินฝากชำระหนี้เงินกู้ทั้งจำนวน
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label tim-note">
                                                        <input class="form-check-input" type="checkbox" id="chk32" runat="server" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        มีบันทึกถึงฝ่ายปฏิบัติการกลาง (ไม่สามารถตัดบัญชีออมทรัพย์ได้)
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" id="chk4" runat="server" checked="checked" disabled />
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>ไม่มีบัญชีเงินฝาก</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                            <div class="col-sm-10 checkbox-radios">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="chk41" runat="server" checked="checked" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        ลูกหนี้หลบหนี/ไม่สามารถติดต่อลูกหนี้ได้เป็นเวลานาน
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label tim-note">
                                                        <input class="form-check-input" type="checkbox" id="chk42" runat="server" disabled />
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                        ลูกหนี้ไม่มีความสามารถในการชำระหนี้
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">เหตุผล</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlReason" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonDisplay" Enabled="false"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">ผู้อนุมัติ</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlApprover" runat="server" CssClass="form-control" DataValueField="EmpCode" DataTextField="EmpName" Enabled="false"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div id="divReferCaseAsOfDate" runat="server" class="row" visible="false">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-warning">
                                    <h4 id="titleReferCaseAsOfDate" class="card-title">กรณี</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">ถีงวันที่</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtReferCaseAsOfDate" runat="server" class="datepicker form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div id="divReferCaseReason" runat="server" class="row" visible="false">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-warning">
                                    <h4 id="titleReferCaseReason" class="card-title">กรณี</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">เหตุผล</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlReferCaseReason" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonDisplay" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div id="divReferFilingWithdrawal" runat="server" class="row" visible="false">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-warning">
                                    <h4 class="card-title">กรณีถอนฟ้อง</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">ค่าจ้าง/ค่าทนายความ</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtFilingWithdrawal_LawyerFee" runat="server" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">ค่าธรรมเนียมศาล</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtFilingWithdrawal_CourtFee" runat="server" class="form-control" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">ค่าใช้จ่ายอื่นๆ</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtFilingWithdrawal_OtherExpense" runat="server" class="form-control" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">ยอดรวม :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtFilingWithdrawal_Total" runat="server" class="form-control" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div id="divComment" runat="server" class="row" visible="false">
                        <div class="col-md-2 col-sm-1 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5>
                                    <span class="tim-note">Comment :</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:TextBox ID="txtComment" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

