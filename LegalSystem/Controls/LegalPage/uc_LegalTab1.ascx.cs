﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalSystem.Controls;
using System.Transactions;
using System.Web.Services;
using System.Web.UI.HtmlControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab1 : System.Web.UI.UserControl
    {
        #region Properties
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public trLegal ucdata
        {
            get
            {
                return (trLegal)Session["LegalData"];
            }
            set
            {
                Session["LegalData"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{

            //}
        }

        public void BindControl()
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    List<LegalEntities.AttorneyType> Att;
                    using (AttorneyTypeRep rep = new AttorneyTypeRep()) Att = rep.GetAttorneyTypeList();
                    ddlAttorneyType.DataSource = Att;
                    ddlAttorneyType.DataValueField = "AttorneyTypeID";
                    ddlAttorneyType.DataTextField = "AttorneyTypeName";
                    ddlAttorneyType.DataBind();

                    List<msAttorney> Lw=unitOfwork.MSAttorneyRep.GetLawyerByTypeID(1);
                    
                    ddlLawyer.DataSource = Lw;
                    ddlLawyer.DataValueField = "LawyerID";
                    ddlLawyer.DataTextField = "LawyerName";
                    ddlLawyer.DataBind();
                    UcMessageBoxMain.VisibleControl();
                }
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
            
        }

        public void SetUI(trLegal data)
        {            
            try
            {
                ucdata = data;

                BindControl();

                int attorneytypeID = 0;
                int lawyerID = 0;

                try { lawyerID = (int)ucdata.T1_LawyerID; }
                catch (Exception ex) { }

                if (lawyerID > 0)
                {
                    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                    {
                        attorneytypeID =unitOfwork.MSAttorneyRep.GetLawyerByID(lawyerID).AttorneyTypeID;
                    }
                    ddlAttorneyType.SelectedValue = attorneytypeID.ToString();
                    ddlLawyer.SelectedValue = ucdata.T1_LawyerID.ToString();
                }
                else
                {
                    ddlAttorneyType.SelectedIndex = 0;
                    ddlLawyer.SelectedIndex = 0;
                }

                if (ucdata.T1_ReceiveReqDate != null) dateGotRequest.Value = ((DateTime)ucdata.T1_ReceiveReqDate).ToString("dd/MM/yyyy");
                if (ucdata.T1_SendDocDate != null) dateSentReqDoc.Value = ((DateTime)ucdata.T1_SendDocDate).ToString("dd/MM/yyyy");
                if (ucdata.T1_GenCallingDocDate != null) dateSentCallDoc.Value = ((DateTime)ucdata.T1_GenCallingDocDate).ToString("dd/MM/yyyy");
                if (ucdata.T1_ReceiveDocDate != null) dateGetDoc.Value = ((DateTime)ucdata.T1_ReceiveDocDate).ToString("dd/MM/yyyy");
                if (ucdata.T1_SendToLawyerDate != null) dateSentCaseToLawyer.Value = ((DateTime)ucdata.T1_SendToLawyerDate).ToString("dd/MM/yyyy");
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
        }

        public trLegal GetUI()
        {
            trLegal l_data = ucdata;     
            try
            {
                if (!string.IsNullOrEmpty(dateGotRequest.Value)) l_data.T1_ReceiveReqDate = Convert.ToDateTime(dateGotRequest.Value.ToString());
                if (!string.IsNullOrEmpty(ddlLawyer.SelectedValue))
                    if (Convert.ToInt32(ddlLawyer.SelectedValue) > 0) l_data.T1_LawyerID = Convert.ToInt32(ddlLawyer.SelectedValue.ToString());
                    else l_data.T1_LawyerID = null;
                else l_data.T1_LawyerID = null;
                if (!string.IsNullOrEmpty(dateSentReqDoc.Value)) l_data.T1_SendDocDate = Convert.ToDateTime(dateSentReqDoc.Value.ToString());
                if (!string.IsNullOrEmpty(dateSentCallDoc.Value)) l_data.T1_GenCallingDocDate = Convert.ToDateTime(dateSentCallDoc.Value.ToString());
                if (!string.IsNullOrEmpty(dateGetDoc.Value)) l_data.T1_ReceiveDocDate = Convert.ToDateTime(dateGetDoc.Value.ToString());
                else throw new Exception("กรุณากรอกข้อมูล");
                if (!string.IsNullOrEmpty(dateSentCaseToLawyer.Value)) l_data.T1_SendToLawyerDate = Convert.ToDateTime(dateSentCaseToLawyer.Value.ToString());
                l_data.UpdateBy = userCode;
                l_data.UpdateDateTime = DateTime.Now;
                UcMessageBoxMain.VisibleControl();                
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
            return l_data;
        }

        public string ValidateTab()
        {
            string err = string.Empty;
            if (string.IsNullOrEmpty(dateGetDoc.Value)) err = "กรุณากรอก \"วันรับเอกสาร\"";
            return err;
        }

        protected void ddlAttorneyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    int atnType = Convert.ToInt32(ddlAttorneyType.SelectedValue);
                    List<msAttorney> A = unitOfwork.MSAttorneyRep.GetLawyerByTypeID(atnType);
                    if (A.Count > 0)
                    {
                        ddlLawyer.Enabled = true;
                        ddlLawyer.DataSource = A;
                        ddlLawyer.DataBind();
                        ddlLawyer.DataValueField = "LawyerID";
                        ddlLawyer.DataTextField = "LawyerName";
                        ddlLawyer.SelectedIndex = 0;
                    }
                    else ddlLawyer.Enabled = false;
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }            
        }
    }
}