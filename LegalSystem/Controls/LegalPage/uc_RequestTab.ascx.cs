﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalSystem.Controls;
using System.Transactions;
using System.Web.Services;
using System.Web.UI.HtmlControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_RequestTab : System.Web.UI.UserControl
    {
        #region Properties
        private string rno
        {
            get { return Session["RNO"] != null ? Session["RNO"] as string : null; }
            set { Session["RNO"] = value; }
        }
        private string acct
        {
            get { return Session["ACCT"] != null ? Session["ACCT"] as string : null; }
            set { Session["ACCT"] = value; }
        }
        private bool IsSubmited
        {
            get { return Session["IsSubmited"] != null ? Convert.ToBoolean(Session["IsSubmited"]) : false; }
            set { Session["IsSubmited"] = value; }
        }
        private List<LNMAST> LNs
        {
            get { return Session["ListAcctLoan"] != null ? Session["ListAcctLoan"] as List<LNMAST> : null; }
            set { Session["ListAcctLoan"] = value; }
        }
        private trRequest R
        {
            get { return Session["Request"] != null ? Session["Request"] as trRequest : null; }
            set { Session["Request"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetUI_forLegalPage();
        }

        #region Functions

        private void SetUI_forLegalPage()
        {
            //rno = (Request.QueryString["rno"] != null) ? Request.QueryString["rno"].ToString() : string.Empty;
            //acct = (Request.QueryString["ACCT"] != null) ? Request.QueryString["ACCT"].ToString() : string.Empty;
            //string cif = (Request.QueryString["CIF"] != null) ? Request.QueryString["CIF"].ToString() : string.Empty;
            LegalUnitOfWork unitOfWork = new LegalUnitOfWork();
            
                ddlRequestStatus.DataSource =unitOfWork.MSRequestStatusRep.Get();
                ddlRequestStatus.DataBind();

                ddlLegalStatus.DataSource =unitOfWork.MSLegalStatusRep.Get();
                ddlLegalStatus.DataBind();
            
            

            ddlRequestType.DataSource = unitOfWork.VRequestTypeMapLegalStstusRep.GetRequestTypeByLegalStstus((!string.IsNullOrEmpty(rno)) ? "-1" : ddlLegalStatus.SelectedValue);
            ddlRequestType.DataBind();

            ddlApprover.DataSource = unitOfWork.WFGroupRep.GetGroupByRole(3);
            ddlApprover.DataBind();
            //using (vRequestTypeMapLegalStstusRep rep = new vRequestTypeMapLegalStstusRep())
            //{
            //    ddlRequestType.DataSource = rep.GetRequestTypeByLegalStstus((!string.IsNullOrEmpty(rno)) ? "-1" : ddlLegalStatus.SelectedValue);
            //    ddlRequestType.DataBind();
            //}
            //using (wfGroupRep rep = new wfGroupRep())
            //{
            //    ddlApprover.DataSource = rep.GetGroupByRole(3);
            //    ddlApprover.DataBind();
            //}
            GetRequestReason(ddlReason);

            if (!string.IsNullOrEmpty(rno)) /****Edit****/
            {
                R = new trRequest();
                R=unitOfWork.TrRequestRep.GetRequestByID(rno);
                trLegal Leg = unitOfWork.TrLegalRep.Get(x => x.LegalID == R.LegalID).FirstOrDefault();
                //using (RequestRep rep = new RequestRep())
                //    R = rep.GetRequestByID(rno);

                /****Set disable fields****/
                IsSubmited = (Convert.ToInt32(R.RequestStatus_) >= 2 && Convert.ToInt32(R.RequestStatus_) != 4);//ใช้ Disable field
                txtCIF.Attributes.Add("readonly", "readonly");
                divComment.Visible = (Convert.ToInt32(R.RequestStatus_) >= 2);

                /****Set value fields****/
                txtRequestNo.Value = R.RequestNo;
                txtRequestDate.Value = R.RequestDate.Value.ToString("dd/MM/yyyy",new System.Globalization.CultureInfo("th-TH"));
                ddlRequestStatus.SelectedIndex = Convert.ToInt32(R.RequestStatus_);
                ddlRequestType.SelectedIndex = Convert.ToInt32(R.RequestType);
                ddlReason.SelectedValue = R.RequestReason;
                txtReferRequestNo.Value = R.RefRequestNo;
                txtReferRequestType.Value = R.RefRequestType;
                txtLegalNo.Value =Leg.LegalNo;
                ddlLegalStatus.SelectedValue = R.LegalStatus;
                txtCIF.Value = R.CifNo.ToString();
                txtAddress.Value = R.Address;
                ddlApprover.SelectedValue = R.ColHead;
                txtComment.Text = R.Comment;
                //using (LNMASTRep rep = new LNMASTRep())
                //{
                LNs = unitOfWork.LnmastRep.GetInfomationByCIF(LegalSystem.Properties.Settings.Default.LinkServer, R.CifNo.ToString(), rno, Convert.ToInt32(R.RequestStatus_));
                    BinducAccountLoan();
                //}

                string[] chks = new string[] { "chk1", "chk11", "chk12", "chk2", "chk21", "chk3", "chk31", "chk32", "chk4", "chk41", "chk42" };
                string[] attachs = R.Attachments.Split(',');
                for (int i = 0; i < chks.Count(); i++)
                {
                    var control = ((HtmlInputCheckBox)divAttachments.FindControl(chks[i]));
                    if (attachs[i] == "1")
                        control.Attributes.Add("checked", "checked");
                    else
                        control.Attributes.Remove("checked");
                    if (IsSubmited)
                        control.Attributes.Add("disabled", "disabled");
                }
            }
        }

        private void GetRequestReason(DropDownList ddl)
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    ddl.DataSource = unitOfwork.VRequestReasonRep.GetReasonByRequest(Convert.ToInt32(ddlRequestType.SelectedValue));
                    ddl.DataBind();
                    ddl.SelectedIndex = 0;
                }
               
            }
            catch (Exception ex)
            {
                //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        private void BinducAccountLoan()
        {
            gvAccountLoan.DataSource = LNs;
            gvAccountLoan.DataBind();
            if (LNs.Count > 0)
                txtAddress.Value = LNs[0].CFADDR;
        }

        private string GetAttachments()
        {
            string Attachments = (chk1.Checked ? "1," : "0,")//หนังสือบอกกล่าวผู้ค้ำประกัน พร้อมไปรษณีย์ตอบรับ
                + (chk11.Checked ? "1," : "0,")   //มี
                + (chk12.Checked ? "1," : "0,")   //ไม่มี เพราะไม่มีผู้ค้ำประกัน
                + (chk2.Checked ? "1," : "0,")    //หนังสือแจ้งวันขายทอดตลาดหลักทรัพย์จำนำ พร้อมไปรษณีย์ตอบรับ
                + (chk21.Checked ? "1," : "0,")   //ใบรายงานการประมูล
                + (chk3.Checked ? "1," : "0,")    //มีบัญชีเงินฝาก
                + (chk31.Checked ? "1," : "0,")   //มี ตัดบัญชีเงินฝากชำระหนี้เงินกู้ทั้งจำนวน
                + (chk32.Checked ? "1," : "0,")   //มีบันทึกถึงฝ่ายปฏิบัติการกลาง (ไม่สามารถตัดบัญชีออมทรัพย์ได้)
                + (chk4.Checked ? "1," : "0,")    //ไม่มีบัญชีเงินฝาก
                + (chk41.Checked ? "1," : "0,")   //ลูกหนี้หลบหนี/ไม่สามารถติดต่อลูกหนี้ได้เป็นเวลานาน
                + (chk42.Checked ? "1" : "0");    //ลูกหนี้ไม่มีความสามารถในการชำระหนี้
            return Attachments;
        }

        private void GetUIRequest()
        {
            if (R != null)
            {
                if (R.RequestNo == txtRequestNo.Value)
                {
                    R.RequestType = ddlRequestType.SelectedValue;
                    R.RequestReason = ddlReason.SelectedValue;
                    R.ColHead = ddlApprover.SelectedValue;
                }
                else//Refer Case
                {
                    R.RequestNo = txtRequestNo.Value;
                    R.RequestDate = DateTime.Today;
                    R.RequestType = ddlRequestType.SelectedValue;
                    R.RequestStatusDate = DateTime.Today;
                    R.RefRequestNo = txtReferRequestNo.Value;
                    R.RefRequestType = hidReferRequestType.Value;
                    R.RefRequestDate = Convert.ToDateTime(txtReferCaseAsOfDate.Value);
                    R.RefRequestReason = ddlReferCaseReason.SelectedValue;
                    R.ColHead = ddlApprover.SelectedValue;
                    R.Owner = GlobalSession.UserLogin.Emp_Code;
                }
            }
            else
            {
                R = new trRequest()
                {
                    RequestNo = txtRequestNo.Value,
                    RequestDate = DateTime.Today,
                    RequestType = ddlRequestType.SelectedValue,
                    RequestStatusDate = DateTime.Today,
                    RequestReason = ddlReason.SelectedValue,
                    //DefendantNo = txtRequestNo.Value,
                    RefRequestNo = txtReferRequestNo.Value,
                    RefRequestType = hidReferRequestType.Value,
                    //RefRequestDate=
                    RefRequestReason = string.Empty,
                    LegalStatus = ddlLegalStatus.SelectedValue,
                    //LegalStatusDate = txtRequestNo.Value,
                    CifNo = Convert.ToDecimal(txtCIF.Value),
                    Address = txtAddress.Value,
                    Owner = GlobalSession.UserLogin.Emp_Code,
                    ColHead = ddlApprover.SelectedValue,
                    SeniorLawyer = string.Empty
                    //Attachments = txtRequestNo.Value,
                };
            }
        }

        private List<trRequestRelation> GetUIRequestRelation()
        {
            List<trRequestRelation> RRs = new List<trRequestRelation>();
            trRequestRelation RR = new trRequestRelation();
            string CollateralType = string.Empty;
            int DefendantNo = 0;
            foreach (GridViewRow rowA in gvAccountLoan.Rows)
            {
                var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                if (chkAL.Checked)
                {
                    DefendantNo++;
                    var txtAL = (HtmlInputText)rowA.FindControl("txtAcctLoan");
                    var txtCT = (HtmlInputText)rowA.FindControl("txtCollateralType");
                    if (string.IsNullOrEmpty(CollateralType))
                        CollateralType = txtCT.Value;
                    else
                    {
                        if (CollateralType != txtCT.Value)
                            throw new Exception("กรุณาเลือกบัญชีที่มีหลักประกันเดียวกัน");
                    }
                    var gvR = (GridView)rowA.FindControl("gvRelationship");
                    int rrNum = 0;
                    foreach (GridViewRow rowR in gvR.Rows)
                    {
                        var hidCIF = (HiddenField)rowR.FindControl("hidCIF");
                        var hidType = (HiddenField)rowR.FindControl("hidType");
                        var hidOrder = (HiddenField)rowR.FindControl("hidOrder");
                        var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                        if (chkR.Checked)
                        {
                            rrNum++;
                            RR = new trRequestRelation()
                            {
                               // RequestID = txtRequestNo.Value,
                                Account = Convert.ToDecimal(txtAL.Value),
                                RelationCifNo = Convert.ToDecimal(hidCIF.Value),
                                RelationType = hidType.Value,
                                RelationSeq = Convert.ToInt32(hidOrder.Value)
                            };
                            RRs.Add(RR);
                        }
                    }
                    if (rrNum == 0)
                        throw new Exception("กรุณาเลือก Relationship ของบัญชี " + txtAL.Value);
                }
                else
                {
                    var gvR = (GridView)rowA.FindControl("gvRelationship");
                    foreach (GridViewRow rowR in gvR.Rows)
                    {
                        var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                        chkR.Attributes.Remove("checked");
                    }
                }
            }
            R.DefendantNo = DefendantNo;
            if (R.DefendantNo == 0 || RRs.Count == 0)
                throw new Exception("กรุณาเลือกบัญชีสินเชื่อและ Relationship อย่างน้อย 1 บัญชี");
            return RRs;
        }

        #endregion

        protected void gvAccountLoan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LNMAST ln = e.Row.DataItem as LNMAST;
                var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkAccount")));
                control.Attributes.Add("onclick", "javascript:checkChange('" + ln.ACCTNO + "');");
                control.Style.Add("cursor", "pointer");
                if (ln.isSelect)
                    control.Attributes.Add("checked", "checked");
                if (IsSubmited || !ln.isEnable)//
                    control.Attributes.Add("disabled", "disabled");
                ((HtmlInputText)(e.Row.FindControl("txtAcctLoan"))).Value = ln.ACCTNO.ToString();
                ((HtmlInputText)(e.Row.FindControl("txtAcctStatus"))).Value = ln._STATUS;
                ((HtmlInputText)(e.Row.FindControl("txtDPD"))).Value = ln.PDDAYS.ToString();
                ((HtmlInputText)(e.Row.FindControl("txtCollateralType"))).Value = ln.CollateralDesc;

                List<sp_GetRelationship> Rs = new List<sp_GetRelationship>();
                using (LegalDataContext rep = new LegalDataContext())
                    Rs = rep.GetRelationship(ln.ACCTNO.ToString(), rno).ToList();
                foreach (sp_GetRelationship tmp in Rs)
                {
                    tmp.IsSelect = ln.isSelect;
                    tmp.IsEnable = ln.isEnable;
                }
                ((GridView)(e.Row.FindControl("gvRelationship"))).DataSource = Rs;
                ((GridView)(e.Row.FindControl("gvRelationship"))).DataBind();

                ((HtmlInputText)(e.Row.FindControl("txtLoanType"))).Value = ln.TYPE;
                ((HtmlInputText)(e.Row.FindControl("txtCurBalAmt"))).Value = ln.CBAL.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtOriginalAmt"))).Value = ln.ORGAMT.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtAcctInterest"))).Value = ln.ACCINT.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtInterestRate"))).Value = ln.INTRATE.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtACCI3"))).Value = ln.ACCI3.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtNumOfDPD"))).Value = ln.PDDAYS.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtPENIN3"))).Value = ln.PENIN3.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtPENINT"))).Value = ln.PENINT.ToString("N2");
                ((HtmlInputText)(e.Row.FindControl("txtPOTHER"))).Value = "0.00";
                ((HtmlInputText)(e.Row.FindControl("txtTotal"))).Value = (ln.CBAL + ln.ACCINT + ln.ACCI3 + ln.PENIN3 + ln.PENINT).ToString("N2");
            }
        }

        protected void gvAccountLoan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            { }
        }

        protected void gvAccountLoan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvRelationship_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sp_GetRelationship r = e.Row.DataItem as sp_GetRelationship;
                var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkRelationship")));
                if (r.IsSelect)
                    control.Attributes.Add("checked", "checked");
                if (IsSubmited || !r.IsEnable)
                    control.Attributes.Add("disabled", "disabled");
            }
        }
    }
}