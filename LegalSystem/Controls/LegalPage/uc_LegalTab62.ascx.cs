﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab62 : System.Web.UI.UserControl
    {
        #region Properties
        public LegalDbContext context;
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }
        public string LegalNo
        {
            get
            {
                return (string)Session["LegalNo"];
            }
            set
            {

                Session["LegalNo"] = value;
            }
        }
        //public trLegal LegalData
        //{
        //    get
        //    {
        //        return (trLegal)Session["LegalData"];
        //    }
        //    set
        //    {
        //        Session["LegalData"] = value;
        //    }
        //}
        private List<trFee> feeList;
        public List<trFee> FeeList
        {
            get
            {
                return (List<trFee>)Session["FeeList"];
            }
            set
            {
                Session["FeeList"] = value;
            }
        }
        private List<vFee> vfeeList;
        public List<vFee> vFeeList
        {
            get
            {
                return (List<vFee>)Session["vFeeList"];
            }
            set
            {
                Session["vFeeList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{
            //    context = LegalSession._legalDbContext;
            //}
        }

        public void SetUI(string legalno)
        {
            try
            {
                LegalNo = legalno;

                //============trFee============
                trFeeRep feerep = new trFeeRep();
                feeList = feerep.GetAllFee(LegalNo);
                FeeList = feeList.OrderBy(list => list.CostTypeID).ThenBy(list => list.FeeID).ToList();
                //============vFee============
                vFeeRep vfee_rep = new vFeeRep();
                vfeeList = vfee_rep.GetAllFee(LegalNo);
                vFeeList = vfeeList;

                if (vFeeList.Count == 0) InitialRacha();
                gvRacha.DataSource = vFeeList;
                gvRacha.DataBind();
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
        }
        public List<trFee> GetUI()
        {
            return FeeList;
        }
        public string ValidateTab()
        {
            return string.Empty;
        }
        public void InitialRacha()
        {
            try
            {
                List<vCostType> typeList = new List<vCostType>();
                using (vCostTypeRep rep = new vCostTypeRep())
                {
                    typeList = rep.GetCostTypeByFlag("R", "F");
                }

                int feeID = 1;
                int costTypeID = 0;
                string costTypeName = string.Empty;
                string tflag = string.Empty;
                string sflag = string.Empty;
                //string user = "T89005";
                string chequeNo = string.Empty;
                string bankBranch = string.Empty;
                string bankCode = string.Empty;
                string bankName = string.Empty;
                decimal courtPayAmt = 0;
                decimal courtRefundAmt = 0;
                //Nullable<DateTime> courtRefundDate = null;
                //Nullable<DateTime> chequeDate = null;
                //Nullable<DateTime> sendChqDate = null;
                DateTime courtRefundDate = DateTime.Now;
                DateTime chequeDate = DateTime.Now;
                DateTime sendChqDate = DateTime.Now;
                DateTime updatedate = DateTime.Now;
                using (MaxValueRep rep = new MaxValueRep())
                {
                    courtPayAmt = rep.GetMaxValueByMaxName("DefaultRacha").MaxAmt;
                }

                feeList = FeeList;
                vfeeList = vFeeList;
                foreach (vCostType item in typeList)
                {
                    if (vfeeList.Count > 0)
                    {
                        feeID = vfeeList.Max(c => c.FeeID) + 1;
                    }
                    costTypeID = item.CostTypeID;
                    costTypeName = item.CostTypeName;
                    tflag = item.TFlag;
                    sflag = item.SFlag;

                    //=========trFee=========
                    trFee flist = new trFee();
                    flist.FeeID = feeID;
                    flist.LegalNo = LegalNo;
                    flist.CostTypeID = costTypeID;
                    flist.CourtPayAmt = courtPayAmt;
                    flist.CourtRefundAmt = courtRefundAmt;
                    flist.BankCode = bankCode;
                    flist.CourtRefundDate = courtRefundDate;
                    flist.ChequeDate = chequeDate;
                    flist.SendChqDate = sendChqDate;
                    //if (courtRefundDate.HasValue) flist.CourtRefundDate = (DateTime)courtRefundDate;
                    //if (chequeDate.HasValue) flist.ChequeDate = (DateTime)chequeDate;
                    //if (sendChqDate.HasValue) flist.SendChqDate = (DateTime)sendChqDate;
                    flist.ChequeNo = chequeNo;
                    flist.BankBranch = bankBranch;
                    flist.UpdateBy = userCode;
                    flist.UpdateDateTime = updatedate;
                    flist.CreateBy = userCode;
                    flist.CreateDateTime = updatedate;
                    feeList.Add(flist);
                    //=========vFee=========
                    vFee vlist = new vFee();
                    vlist.FeeID = feeID;
                    vlist.LegalNo = LegalNo;
                    vlist.CostTypeID = costTypeID;
                    vlist.CostTypeName = costTypeName;
                    vlist.TFlag = tflag;
                    vlist.SFlag = sflag;
                    vlist.CourtPayAmt = courtPayAmt;
                    vlist.CourtRefundAmt = courtRefundAmt;
                    vlist.BankCode = bankCode;
                    vlist.BankName = bankName;
                    vlist.ChequeNo = chequeNo;
                    vlist.BankBranch = bankBranch;
                    vlist.CourtRefundDate = courtRefundDate;
                    vlist.ChequeDate = chequeDate;
                    vlist.SendChqDate = sendChqDate;
                    //if (courtRefundDate.HasValue) vlist.CourtRefundDate = (DateTime)courtRefundDate;
                    //if (chequeDate.HasValue) vlist.ChequeDate = (DateTime)chequeDate;
                    //if (sendChqDate.HasValue) vlist.SendChqDate = (DateTime)sendChqDate;
                    vfeeList.Add(vlist);
                }
                FeeList = feeList;
                vFeeList = vfeeList;
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }
        public void GetUpdateFee()
        {
            gvRacha.DataSource = vFeeList.OrderBy(M => M.FeeID).ToList();
            gvRacha.DataBind();
        }
        public bool CheckFixCostType(int costTypeID)
        {
            bool isfix = false;
            if (costTypeID != 0)
            {
                string sSFlag = string.Empty;
                using (vCostTypeRep rep = new vCostTypeRep())
                {
                    sSFlag = rep.GetCostTypeByID(costTypeID).SFlag;
                }
                if (sSFlag.Equals("F")) //SFlag => F:fix: N:not fix
                {
                    isfix = true;
                }
            }

            return isfix;
        }

        #region GridView Event
        protected void gvRacha_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                HiddenField hidFeeID_shw = gvRacha.Rows[index].FindControl("hidFeeID_shw") as HiddenField;
                int feeID = Convert.ToInt32(hidFeeID_shw.Value);

                //===========MortgageList===========
                feeList = FeeList;
                feeList.RemoveAll(list => list.FeeID == feeID);
                FeeList = feeList;
                //===========vMortgageList===========
                vfeeList = vFeeList;
                vfeeList.RemoveAll(list => list.FeeID == feeID);
                vFeeList = vfeeList;

                CommandName = "Delete";
                GetUpdateFee();
                //UpdatePanel1.Update();
                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datepicker", "postback();", true);
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        protected void gvRacha_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvRacha.PageIndex = e.NewPageIndex;
                gvRacha.DataSource = vFeeList;
                gvRacha.DataBind();
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        protected void gvRacha_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;

                switch (e.CommandName)
                {
                    case "Update":
                        #region get data from GridView
                        int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                        HiddenField hidFeeID_edt = gvRacha.Rows[index].FindControl("hidFeeID_edt") as HiddenField;
                        DropDownList ddlFeeType = gvRacha.Rows[index].FindControl("ddlFeeType") as DropDownList;
                        DropDownList ddlBank = gvRacha.Rows[index].FindControl("ddlBank") as DropDownList;
                        TextBox txtCourtPay = gvRacha.Rows[index].FindControl("txtCourtPay") as TextBox;
                        TextBox txtCourtRefund = gvRacha.Rows[index].FindControl("txtCourtRefund") as TextBox;
                        TextBox txtCourtRefundDate = gvRacha.Rows[index].FindControl("txtCourtRefundDate") as TextBox;
                        TextBox txtChequeDate = gvRacha.Rows[index].FindControl("txtChequeDate") as TextBox;
                        TextBox txtChequeNo = gvRacha.Rows[index].FindControl("txtChequeNo") as TextBox;
                        TextBox txtBranch = gvRacha.Rows[index].FindControl("txtBranch") as TextBox;
                        TextBox txtSendChqDate = gvRacha.Rows[index].FindControl("txtSendChqDate") as TextBox;


                        int feeID = Convert.ToInt32(hidFeeID_edt.Value);
                        int costTypeID = Convert.ToInt32(ddlFeeType.SelectedValue);
                        //string user = "T65007";
                        string costTypeName = string.Empty;
                        string tflag = string.Empty;
                        string sflag = string.Empty;
                        string chequeNo = string.Empty;
                        string bankBranch = string.Empty;
                        string bankCode = string.Empty;
                        string bankName = string.Empty;
                        decimal courtPayAmt = 0;
                        decimal courtRefundAmt = 0;
                        DateTime? courtRefundDate = null;
                        DateTime? chequeDate = null;
                        DateTime? sendChqDate = null;
                        DateTime updatedate = DateTime.Now;

                        vCostType f = new vCostType();
                        using (vCostTypeRep rep = new vCostTypeRep())
                        {
                            f = rep.GetCostTypeByID(costTypeID);
                        }
                        costTypeName = f.CostTypeName;
                        tflag = f.TFlag;
                        sflag = f.SFlag;

                        if (!string.IsNullOrEmpty(txtChequeNo.Text)) chequeNo = txtChequeNo.Text.Trim();
                        if (!string.IsNullOrEmpty(txtBranch.Text)) bankBranch = txtBranch.Text.Trim();
                        if (!string.IsNullOrEmpty(ddlBank.SelectedValue))
                        {
                            bankCode = ddlBank.SelectedValue.Trim();
                            bankName = ddlBank.SelectedItem.Text.Trim();
                        }
                        if (!string.IsNullOrEmpty(txtCourtPay.Text)) courtPayAmt = Convert.ToDecimal(txtCourtPay.Text.Trim());
                        if (!string.IsNullOrEmpty(txtCourtRefund.Text)) courtRefundAmt = Convert.ToDecimal(txtCourtRefund.Text.Trim());
                        if (!string.IsNullOrEmpty(txtCourtRefundDate.Text)) courtRefundDate = Convert.ToDateTime(txtCourtRefundDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtChequeDate.Text)) chequeDate = Convert.ToDateTime(txtChequeDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtSendChqDate.Text)) sendChqDate = Convert.ToDateTime(txtSendChqDate.Text.Trim());
                        #endregion

                        //=========trCost=========
                        trFee flist = new trFee();
                        flist.FeeID = feeID;
                        flist.LegalNo = LegalNo;
                        flist.CostTypeID = costTypeID;
                        flist.CourtPayAmt = courtPayAmt;
                        flist.CourtRefundAmt = courtRefundAmt;
                        flist.CourtRefundDate = courtRefundDate;
                        flist.BankCode = bankCode;
                        flist.ChequeDate = chequeDate;
                        flist.ChequeNo = chequeNo;
                        flist.BankBranch = bankBranch;
                        flist.SendChqDate = sendChqDate;
                        flist.UpdateBy = userCode;
                        flist.UpdateDateTime = updatedate;
                        if (CommandName.Equals("Add"))
                        {
                            flist.CreateBy = userCode;
                            flist.CreateDateTime = updatedate;
                        }
                        else
                        {
                            flist.CreateBy = FeeList.Find(M => M.FeeID == feeID).CreateBy;
                            flist.CreateDateTime = FeeList.Find(M => M.FeeID == feeID).CreateDateTime;
                        }

                        feeList = FeeList;
                        feeList.RemoveAll(list => list.FeeID == feeID);
                        feeList.Add(flist);
                        FeeList = feeList;
                        //=========vCost=========
                        vFee vlist = new vFee();
                        vlist.FeeID = feeID;
                        vlist.LegalNo = LegalNo;
                        vlist.CostTypeID = costTypeID;
                        vlist.CostTypeName = costTypeName;
                        vlist.TFlag = tflag;
                        vlist.SFlag = sflag;
                        vlist.CourtPayAmt = courtPayAmt;
                        vlist.CourtRefundAmt = courtRefundAmt;
                        vlist.CourtRefundDate = courtRefundDate;
                        vlist.BankCode = bankCode;
                        vlist.BankName = bankName;
                        vlist.ChequeDate = chequeDate;
                        vlist.ChequeNo = chequeNo;
                        vlist.BankBranch = bankBranch;
                        vlist.SendChqDate = sendChqDate;

                        vfeeList = vFeeList;
                        vfeeList.RemoveAll(list => list.FeeID == feeID);
                        vfeeList.Add(vlist);
                        vFeeList = vfeeList;

                        gvRacha.EditIndex = -1;
                        CommandName = "Update";
                        break;
                }
                GetUpdateFee();
                //UpdatePanel1.Update();
                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datepicker", "postback();", true);
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        protected void gvRacha_RowEditing1(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvRacha.EditIndex = e.NewEditIndex;
                GetUpdateFee();

                bool isFix = false;
                int index = e.NewEditIndex;
                int costTypeID = 0;
                HiddenField hidFeeID_edt = gvRacha.Rows[index].FindControl("hidFeeID_edt") as HiddenField;
                DropDownList ddlFeeType = gvRacha.Rows[index].FindControl("ddlFeeType") as DropDownList;
                DropDownList ddlBank = gvRacha.Rows[index].FindControl("ddlBank") as DropDownList;
                //if (ddlCostType == null) { return; }

                // ddlFeeType
                int feeID = Convert.ToInt32(hidFeeID_edt.Value);
                costTypeID = vFeeList.Find(c => c.FeeID == feeID).CostTypeID;
                isFix = CheckFixCostType(costTypeID);
                List<vCostType> list = new List<vCostType>();
                if (isFix)
                {
                    using (vCostTypeRep rep = new vCostTypeRep())
                    {
                        vCostType item = rep.GetCostTypeByID(costTypeID);
                        list.Add(item);
                    }
                }
                else
                {
                    using (vCostTypeRep rep = new vCostTypeRep())
                    {
                        list = rep.GetCostTypeByFlag("R", "N");
                    }
                }
                ddlFeeType.DataSource = list;
                ddlFeeType.DataBind();
                ddlFeeType.SelectedIndex = 0;

                // ddlBank
                List<vBank> banklist = new List<vBank>();
                using (vBankRep rep = new vBankRep())
                {
                    banklist = rep.GetAllBank();
                }
                ddlBank.DataSource = banklist;
                ddlBank.DataBind();
                ddlBank.SelectedIndex = 0;

                if (CommandName.Equals("Add"))
                {
                    TextBox txtCourtRefundDate = gvRacha.Rows[index].FindControl("txtCourtRefundDate") as TextBox;
                    TextBox txtChequeDate = gvRacha.Rows[index].FindControl("txtChequeDate") as TextBox;
                    TextBox txtSendChqDate = gvRacha.Rows[index].FindControl("txtSendChqDate") as TextBox;

                    txtCourtRefundDate.Text = string.Empty;
                    txtChequeDate.Text = string.Empty;
                    txtSendChqDate.Text = string.Empty;
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        protected void gvRacha_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvRacha_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (CommandName.Equals("Add"))
                {
                    int index = e.RowIndex;
                    HiddenField hidFeeID_edt = gvRacha.Rows[index].FindControl("hidFeeID_edt") as HiddenField;
                    int feeID = Convert.ToInt32(hidFeeID_edt.Value);

                    //===========trCost===========
                    feeList = FeeList;
                    feeList.RemoveAll(list => list.FeeID == feeID);
                    FeeList = feeList;
                    //===========vCost===========
                    vfeeList = vFeeList;
                    vfeeList.RemoveAll(list => list.FeeID == feeID);
                    vFeeList = vfeeList;
                }
                CommandName = "Cancel";
                gvRacha.EditIndex = -1;
                GetUpdateFee();
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        protected void gvRacha_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton LinkButton2 = e.Row.FindControl("LinkButton2") as LinkButton; // delete button
                    HiddenField hidFeeTypeID = e.Row.FindControl("hidFeeTypeID") as HiddenField;

                    if (LinkButton2 == null) { return; } // for edit mode
                    if (hidFeeTypeID == null) { return; }

                    int costTypeID = 0;
                    bool isFix = false;

                    costTypeID = Convert.ToInt32(hidFeeTypeID.Value);
                    isFix = CheckFixCostType(costTypeID);
                    if (isFix)
                    {
                        LinkButton2.Visible = false;
                    }
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }
        #endregion

        #region Controls Event
        protected void btnadd_racha_Click(object sender, EventArgs e)
        {
            try
            {
                int feeID = 1; int vfeeID = 0; int index = 0;
                if (FeeList.Count > 0)
                {
                    feeID = FeeList.Max(M => M.FeeID) + 1;
                }
                if (vFeeList.Count > 0)
                {
                    vfeeID = vFeeList.Max(F => F.FeeID);
                }
                if (feeID > vfeeID)
                {
                    CommandName = "Add";

                    decimal courtpayAmt = 0;
                    using (MaxValueRep rep = new MaxValueRep())
                    {
                        courtpayAmt = rep.GetMaxValueByMaxName("DefaultRacha").MaxAmt;
                    }

                    feeList = FeeList;
                    vfeeList = vFeeList;
                    //=========trFee=========
                    trFee flist = new trFee();
                    flist.FeeID = feeID;
                    flist.CourtPayAmt = courtpayAmt;
                    feeList.Add(flist);
                    FeeList = feeList;

                    //=========vFee=========
                    vFee vlist = new vFee();
                    vlist.FeeID = feeID;
                    vlist.TFlag = "R";
                    vlist.SFlag = "N";
                    vlist.CourtPayAmt = courtpayAmt;
                    vfeeList.Add(vlist);
                    vFeeList = vfeeList;

                    GetUpdateFee();

                    gvRacha.PageIndex = gvRacha.PageCount - 1;
                    if (gvRacha.Rows.Count > 0)
                    {
                        index = gvRacha.Rows.Count - 1;
                    }
                    gvRacha.SetEditRow(index);
                }
                //UpdatePanel1.Update();
                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }
        #endregion
    }
}