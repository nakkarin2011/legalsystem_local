﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab3 : System.Web.UI.UserControl
    {
        #region Properties
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }
        public trLegal LegalData
        {
            get
            {
                return (trLegal)Session["LegalData"];
            }
            set
            {
                Session["LegalData"] = value;
            }
        }

        private List<trLegal_CourtAssign> courtAssignList;
        public List<trLegal_CourtAssign> CourtAssignList
        {
            get
            {
                return (List<trLegal_CourtAssign>)Session["CourtAssignList"];
            }
            set
            {
                Session["CourtAssignList"] = value;
            }
        }

        private List<trLegal_CourtAssign> appealAssignList;
        public List<trLegal_CourtAssign> AppealAssignList
        {
            get
            {
                return (List<trLegal_CourtAssign>)Session["AppealAssignList"];
            }
            set
            {
                Session["AppealAssignList"] = value;
            }
        }

        private List<trLegal_CourtAssign> petitionAssignList;
        public List<trLegal_CourtAssign> PetitionAssignList
        {
            get
            {
                return (List<trLegal_CourtAssign>)Session["PetitionAssignList"];
            }
            set
            {
                Session["PetitionAssignList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{
            //    BindControl();
            //    InitData();
            //}
        }


        public void InitData()
        {
            courtAssignList = new List<trLegal_CourtAssign>();
            appealAssignList = new List<trLegal_CourtAssign>();
            petitionAssignList = new List<trLegal_CourtAssign>();
            CourtAssignList = new List<trLegal_CourtAssign>();
            AppealAssignList = new List<trLegal_CourtAssign>();
            PetitionAssignList = new List<trLegal_CourtAssign>();
        }
        public void BindControl()
        {
            List<CourtZone> A;
            using (CourtZoneRep rep = new CourtZoneRep()) A = rep.GetAllCourtZone();

            CourtZone newzone = new CourtZone();
            newzone.CourtZoneID = -1;
            newzone.CourtZoneName = "-กรุณาเลือก-";
            A.Insert(0, newzone);
            ddlCourt.DataSource = A;
            if (A.Count > 0)
            {
                ddlCourt.DataValueField = "CourtZoneID";
                ddlCourt.DataTextField = "CourtZoneName";
                //ddlCourt.SelectedIndex = 0;
            }
            ddlCourt.DataBind();
        }
        public void SetUI(trLegal legal)
        {
            try
            {
                LegalData = legal;
                BindControl();

                txtProcecuteNote.Attributes.Add("disabled", "disabled");
                txtExct1Desc.Attributes.Add("disabled", "disabled");
                //txtProcecuteNote.Attributes.Add("required", "required");
                //txtExct1Desc.Attributes.Add("required", "required");
                radioCourt1_Next.SelectedValue = "false";
                radioCourt2_Next.SelectedValue = "false";

                if (LegalData.T3_PlaintRight != null) ddlProsecute1Result.SelectedValue = LegalData.T3_PlaintRight.ToString(); // คำฟ้องถูกต้อง/ไม่ถูกต้อง
                if (LegalData.T3_PlaintReason != null) txtProcecuteNote.Value = LegalData.T3_PlaintReason;
                if (LegalData.T3_UndecidedCase != null) txtUndecidedCase.Value = LegalData.T3_UndecidedCase; // คดีดำ (ศาลชั้นต้น)
                if (LegalData.T3_CourtZoneID != null) ddlCourt.SelectedValue = LegalData.T3_CourtZoneID.ToString().ToLower(); // ศาล
                else BindCourtFeeByCourtZone(Convert.ToInt32(ddlCourt.SelectedValue));
                //if (LegalData.T3_AssetAmt != null) txtCapital.Value = LegalData.T3_AssetAmt.ToString();
                //if (LegalData.T3_CourtFee != null) txtCourt1Fee.Value = LegalData.T3_CourtFee.ToString();
                txtCapital.Value = LegalData.T3_AssetAmt.ToString();
                txtCourt1Fee.Value = LegalData.T3_CourtFee.ToString();
                if (LegalData.T3_Racha != null) ddlRachaFee.SelectedValue = LegalData.T3_Racha.ToString().ToLower(); // บัญชีค่าฤชาธรรมเนียม
                if (LegalData.T3_CourtJudge != null) txtExctDate.Value = ((DateTime)LegalData.T3_CourtJudge).ToString("dd/MM/yyyy");
                if (LegalData.T3_BendJudgeDate != null) txtBendDate.Value = ((DateTime)LegalData.T3_BendJudgeDate).ToString("dd/MM/yyyy");
                if (LegalData.T3_DecidedCase != null) txtDecidedCase.Value = LegalData.T3_DecidedCase;
                if (LegalData.T3_JudgeRight != null) ddlExct1Result.SelectedValue = LegalData.T3_JudgeRight.ToString().ToLower(); //คำพิพากษาถูกต้อง/ไม่ถูกต้อง
                else ddlExct1Result.SelectedValue = "true";
                if (LegalData.T3_JudgeDesc != null) txtExct1Desc.Value = LegalData.T3_JudgeDesc; //รายละเอียด
                if (LegalData.T3_Appeal != null) radioCourt1_Next.SelectedValue = ((bool)LegalData.T3_Appeal).ToString().ToLower();
                else LegalData.T3_Appeal = false;
                if (Convert.ToBoolean(LegalData.T3_Appeal))
                {
                    divCourt1_Next.Attributes.Add("style", "display:block");

                    if (LegalData.T3_AppDate != null) dateApp.Value = ((DateTime)LegalData.T3_AppDate).ToString("dd/MM/yyyy");
                    if (LegalData.T3_AppUndecidedCase != null) txtundecided_app.Value = LegalData.T3_AppUndecidedCase;
                    //if (LegalData.T3_AssetAmt != null) txtasset_app.Value = LegalData.T3_AssetAmt.ToString();
                    //if (LegalData.T3_AppFee != null) txtfee_app.Value = LegalData.T3_AppFee.ToString();
                    txtasset_app.Value = LegalData.T3_AssetAmt.ToString();
                    txtfee_app.Value = LegalData.T3_AppFee.ToString();
                    if (LegalData.T3_AppSendAdjudeDate != null) txtSendAdjd_app.Value = ((DateTime)LegalData.T3_AppSendAdjudeDate).ToString("dd/MM/yyyy");
                    if (LegalData.T3_AppJudgeDate != null) dateExecute_app.Value = ((DateTime)LegalData.T3_AppJudgeDate).ToString("dd/MM/yyyy");
                    if (LegalData.T3_AppDecidedCase != null) txtDecidedCase_app.Value = LegalData.T3_AppDecidedCase;
                    if (LegalData.T3_Petition != null) radioCourt2_Next.SelectedValue = ((bool)LegalData.T3_Petition).ToString().ToLower();
                    if (Convert.ToBoolean(radioCourt2_Next.SelectedValue))
                    {
                        //ValidateTab3_Petition(true);
                        divCourt2_Next.Attributes.Add("style", "display:block");

                        if (LegalData.T3_PetDate != null) datePetition.Value = ((DateTime)LegalData.T3_PetDate).ToString("dd/MM/yyyy");
                        if (LegalData.T3_PetUndecidedCase != null) txtUndecided_pet.Value = LegalData.T3_PetUndecidedCase;
                        //if (LegalData.T3_PetAssetAmt != null) txtAsset_pet.Value = LegalData.T3_PetAssetAmt.ToString();
                        //if (LegalData.T3_PetFee != null) txtfee_pet.Value = LegalData.T3_PetFee.ToString();
                        txtAsset_pet.Value = LegalData.T3_PetAssetAmt.ToString();
                        txtfee_pet.Value = LegalData.T3_PetFee.ToString();
                        if (LegalData.T3_PetSendAdjudeDate != null) dateSendAdjd_pet.Value = ((DateTime)LegalData.T3_PetSendAdjudeDate).ToString("dd/MM/yyyy");
                        if (LegalData.T3_PetJudgeDate != null) dateSendPet.Value = ((DateTime)LegalData.T3_PetJudgeDate).ToString("dd/MM/yyyy");
                        if (LegalData.T3_PetDecidedCase != null) txtDecided_pet.Value = LegalData.T3_PetDecidedCase;
                    }
                    else
                    {
                        //ValidateTab3_Petition(false);
                        divCourt2_Next.Attributes.Add("style", "display:none");
                    }
                }
                else
                {
                    //ValidateTab3_Appeal(false);
                    divCourt1_Next.Attributes.Add("style", "display:none");
                }

                GetAllCourtAssign();
                GetAllAppealAssign();
                GetAllPetitionAssign();
            }
            catch { }
        }
        public Tab3Data GetUI()
        {
            Tab3Data tab3 = new Tab3Data();
            try
            {
                LegalData.T3_PlaintRight = Convert.ToBoolean(ddlProsecute1Result.SelectedValue); // คำฟ้องถูกต้อง/ไม่ถูกต้อง
                LegalData.T3_PlaintReason = txtProcecuteNote.Value;
                LegalData.T3_UndecidedCase = txtUndecidedCase.Value.Trim(); // คดีดำ (ศาลชั้นต้น)
                LegalData.T3_CourtZoneID = Convert.ToInt32(ddlCourt.SelectedValue); // ศาล
                if (!string.IsNullOrEmpty(txtCapital.Value)) LegalData.T3_AssetAmt = Convert.ToDecimal(txtCapital.Value);
                else LegalData.T3_AssetAmt = 0;
                if (!string.IsNullOrEmpty(txtCourt1Fee.Value)) LegalData.T3_CourtFee = Convert.ToDecimal(txtCourt1Fee.Value);
                else LegalData.T3_CourtFee = 0;
                LegalData.T3_Racha = Convert.ToBoolean(ddlRachaFee.SelectedValue); // บัญชีค่าฤชาธรรมเนียม
                LegalData.T3_CourtJudge = Convert.ToDateTime(txtExctDate.Value);
                LegalData.T3_BendJudgeDate = Convert.ToDateTime(txtBendDate.Value);
                LegalData.T3_DecidedCase = txtDecidedCase.Value.Trim();
                LegalData.T3_JudgeRight = Convert.ToBoolean(ddlExct1Result.SelectedValue); //คำพิพากษาถูกต้อง/ไม่ถูกต้อง
                LegalData.T3_JudgeDesc = txtExct1Desc.Value; //รายละเอียด
                LegalData.T3_Appeal = Convert.ToBoolean(radioCourt1_Next.SelectedValue);
                if (Convert.ToBoolean(radioCourt1_Next.SelectedValue))
                {
                    LegalData.T3_AppDate = Convert.ToDateTime(dateApp.Value);
                    LegalData.T3_AppUndecidedCase = txtundecided_app.Value;
                    if (!string.IsNullOrEmpty(txtasset_app.Value)) LegalData.T3_AppAssetAmt = Convert.ToDecimal(txtasset_app.Value);
                    else LegalData.T3_AppAssetAmt = 0;
                    if (!string.IsNullOrEmpty(txtfee_app.Value)) LegalData.T3_AppFee = Convert.ToDecimal(txtfee_app.Value);
                    else LegalData.T3_AppFee = 0;
                    LegalData.T3_AppSendAdjudeDate = Convert.ToDateTime(txtSendAdjd_app.Value);
                    LegalData.T3_AppJudgeDate = Convert.ToDateTime(dateExecute_app.Value);
                    LegalData.T3_AppDecidedCase = txtDecidedCase_app.Value;
                    LegalData.T3_Petition = Convert.ToBoolean(radioCourt2_Next.SelectedValue);
                    if (Convert.ToBoolean(radioCourt2_Next.SelectedValue))
                    {
                        LegalData.T3_PetDate = Convert.ToDateTime(datePetition.Value);
                        LegalData.T3_PetUndecidedCase = txtUndecided_pet.Value;
                        if (!string.IsNullOrEmpty(txtAsset_pet.Value)) LegalData.T3_PetAssetAmt = Convert.ToDecimal(txtAsset_pet.Value);
                        else LegalData.T3_PetAssetAmt = 0;
                        if (!string.IsNullOrEmpty(txtfee_pet.Value)) LegalData.T3_PetFee = Convert.ToDecimal(txtfee_pet.Value);
                        else LegalData.T3_PetFee = 0;
                        LegalData.T3_PetSendAdjudeDate = Convert.ToDateTime(dateSendAdjd_pet.Value);
                        LegalData.T3_PetJudgeDate = Convert.ToDateTime(dateSendPet.Value);
                        LegalData.T3_PetDecidedCase = txtDecided_pet.Value;
                    }
                    else
                    {
                        LegalData.T3_PetDate = null;
                        LegalData.T3_PetUndecidedCase = null;
                        LegalData.T3_PetAssetAmt = 0;
                        LegalData.T3_PetFee = 0;
                        LegalData.T3_PetSendAdjudeDate = null;
                        LegalData.T3_PetJudgeDate = null;
                        LegalData.T3_PetDecidedCase = null;
                    }
                }
                else
                {
                    LegalData.T3_AppDate = null;
                    LegalData.T3_AppUndecidedCase = null;
                    LegalData.T3_AppAssetAmt = 0;
                    LegalData.T3_AppFee = 0;
                    LegalData.T3_AppSendAdjudeDate = null;
                    LegalData.T3_AppJudgeDate = null;
                    LegalData.T3_AppDecidedCase = null;

                    LegalData.T3_PetDate = null;
                    LegalData.T3_PetUndecidedCase = null;
                    LegalData.T3_PetAssetAmt = 0;
                    LegalData.T3_PetFee = 0;
                    LegalData.T3_PetSendAdjudeDate = null;
                    LegalData.T3_PetJudgeDate = null;
                    LegalData.T3_PetDecidedCase = null;
                }
                tab3.LegalData = LegalData;
                tab3.CourtAssign = CourtAssignList;
                tab3.AppealAssign = AppealAssignList;
                tab3.PetitionAssign = PetitionAssignList;
            }
            catch (Exception ex)
            {

            }
            return tab3;
        }
        public string ValidateTab3()
        {
            string scourt = ValidateCourt();
            if (!string.IsNullOrEmpty(scourt)) return scourt;
            string sapp = ValidateAppeal();
            if (!string.IsNullOrEmpty(sapp)) return sapp;
            string spet = ValidatePetition();
            if (!string.IsNullOrEmpty(spet)) return spet;

            return string.Empty;
        }

        public void GetAllCourtAssign()
        {
            trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep();
            //trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep(context);
            courtAssignList = rep.GetAllCourtAssign(LegalData.LegalNo);
            CourtAssignList = courtAssignList;
            gvCourtAssign.DataSource = CourtAssignList;
            gvCourtAssign.DataBind();
        }
        public void GetAllAppealAssign()
        {
            //trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep(context);
            trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep();
            appealAssignList = rep.GetAllAppealAssign(LegalData.LegalNo);
            AppealAssignList = appealAssignList;
            gvAppealAssign.DataSource = AppealAssignList;
            gvAppealAssign.DataBind();
        }
        public void GetAllPetitionAssign()
        {
            //trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep(context);
            trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep();
            petitionAssignList = rep.GetAllPettitionAssign(LegalData.LegalNo);
            PetitionAssignList = petitionAssignList;
            gvPetitionAssign.DataSource = PetitionAssignList;
            gvPetitionAssign.DataBind();
        }
        public void GetUpdateCourtAssign()
        {
            gvCourtAssign.DataSource = CourtAssignList.OrderBy(list => list.AssignDate).ToList();
            gvCourtAssign.DataBind();
        }
        public void GetUpdateAppealAssign()
        {
            gvAppealAssign.DataSource = AppealAssignList.OrderBy(list => list.AssignDate).ToList();
            gvAppealAssign.DataBind();
        }
        public void GetUpdatePetitionAssign()
        {
            gvPetitionAssign.DataSource = PetitionAssignList.OrderBy(list => list.AssignDate).ToList();
            gvPetitionAssign.DataBind();
        }
        public string ValidateCourt()
        {
            //if (string.IsNullOrEmpty(ddlProsecute1Result.SelectedValue)) return("กรุณาเลือก \"คำฟ้องถูกต้อง/ไม่ถูกต้อง\"");
            if (ddlProsecute1Result.SelectedValue.Equals("false"))
            {
                if (string.IsNullOrEmpty(txtProcecuteNote.Value)) return ("กรุณากรอก \"รายละเอียด\"");
            }
            if (string.IsNullOrEmpty(txtUndecidedCase.Value)) return ("กรุณากรอก \"คดีดำ (ศาลชั้นต้น)\"");
            if (Convert.ToInt32(ddlCourt.SelectedValue) <= 0) return ("กรุณาเลือก \"ศาล\"");
            if (string.IsNullOrEmpty(txtCapital.Value)) return ("กรุณากรอก \"ทุนทรัพย์\"");
            //if (Convert.ToInt32(ddlRachaFee.SelectedValue) <= 0) return ("กรุณาเลือก \"บัญชีค่าฤชาธรรมเนียม\"");
            if (string.IsNullOrEmpty(txtExctDate.Value)) return ("กรุณากรอก \"วันพิพากษา\"");
            if (string.IsNullOrEmpty(txtBendDate.Value)) return ("กรุณากรอก \"วันพิพากษาตามยอม\"");
            if (string.IsNullOrEmpty(txtDecidedCase.Value)) return ("กรุณากรอก \"คดีดำ (ศาลชั้นต้น)\"");
            if (ddlExct1Result.SelectedValue.Equals("false")) //คำพิพากษาถูกต้อง/ไม่ถูกต้อง
            {
                if (string.IsNullOrEmpty(txtExct1Desc.Value)) return ("กรุณากรอก \"รายละเอียด\"");
            }
            if (CourtAssignList.Count == 0) return ("กรุณากรอก \"ศาลนัด (ศาลชั้นต้น)\"");

            return string.Empty;
        }
        public string ValidateAppeal()
        {
            if (radioCourt1_Next.SelectedValue.Equals("true"))
            {
                if (string.IsNullOrEmpty(dateApp.Value)) return ("กรุณากรอก \"วันอุทรณ์\"");
                if (string.IsNullOrEmpty(txtundecided_app.Value)) return ("กรุณากรอก \"คดีดำ (อุทรณ์)\"");
                if (string.IsNullOrEmpty(txtasset_app.Value)) return ("กรุณากรอก \"ทุนทรัพย์ชั้นอุทรณ์\"");
                if (string.IsNullOrEmpty(txtSendAdjd_app.Value)) return ("กรุณากรอก \"วันยื่นแก้อุทรณ์\"");
                if (string.IsNullOrEmpty(dateExecute_app.Value)) return ("กรุณากรอก \"วันพิพากษาชั้นอุทรณ์\"");
                if (string.IsNullOrEmpty(txtDecidedCase_app.Value)) return ("กรุณากรอก \"คดีแดง (อุทรณ์)\"");
                if (AppealAssignList.Count == 0) return ("กรุณากรอก \"ศาลนัด (ศาลอุทรณ์)\"");
            }
            return string.Empty;
        }
        public string ValidatePetition()
        {
            if (radioCourt2_Next.SelectedValue.Equals("true"))
            {
                if (string.IsNullOrEmpty(datePetition.Value)) return ("กรุณากรอก \"วันฎีกา\"");
                if (string.IsNullOrEmpty(txtUndecided_pet.Value)) return ("กรุณากรอก \"คดีดำ (ฎีกา)\"");
                if (string.IsNullOrEmpty(txtAsset_pet.Value)) return ("กรุณากรอก \"ทุนทรัพย์ชั้นฎีกา\"");
                if (string.IsNullOrEmpty(dateSendAdjd_pet.Value)) return ("กรุณากรอก \"วันยื่นแก้ฎีกาณ์\"");
                if (string.IsNullOrEmpty(dateSendPet.Value)) return ("กรุณากรอก \"วันพิพากษาชั้นฎีกา\"");
                if (string.IsNullOrEmpty(txtDecided_pet.Value)) return ("กรุณากรอก \"คดีแดง (ฎีกา)\"");
                if (PetitionAssignList.Count == 0) return ("กรุณากรอก \"ศาลนัด (ศาลฎีกา)\"");
            }
            return string.Empty;
        }
        public void ClearAppealData()
        {
            radioCourt1_Next.SelectedValue = "false";
            dateApp.Value = string.Empty;
            txtundecided_app.Value = string.Empty;
            txtasset_app.Value = string.Empty;
            txtfee_app.Value = string.Empty;
            txtSendAdjd_app.Value = string.Empty;
            dateExecute_app.Value = string.Empty;
            txtDecidedCase_app.Value = string.Empty;
            AppealAssignList.Clear();
            GetUpdateAppealAssign();
        }
        public void ClearPetitionData()
        {
            radioCourt2_Next.SelectedValue = "false";
            datePetition.Value = string.Empty;
            txtUndecided_pet.Value = string.Empty;
            txtAsset_pet.Value = string.Empty;
            txtfee_pet.Value = string.Empty;
            dateSendAdjd_pet.Value = string.Empty;
            dateSendPet.Value = string.Empty;
            txtDecided_pet.Value = string.Empty;
            PetitionAssignList.Clear();
            GetUpdatePetitionAssign();
        }
        public void BindCourtFeeByCourtZone(int courtZoneID)
        {
            decimal fee = 0;
            using (CourtZoneRep rep = new CourtZoneRep())
            {
                fee = rep.GetCourtZoneByID(courtZoneID).CrtFeeByZone;
            }
            txtCourt1Fee.Value = Convert.ToDecimal(fee).ToString("F2");
            txtfee_app.Value = Convert.ToDecimal(fee).ToString("F2");
            txtfee_pet.Value = Convert.ToDecimal(fee).ToString("F2");
        }

        #region Control Event
        protected void radioCourt1_Next_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (radioCourt1_Next.SelectedValue)
            {
                case "true":
                    divCourt1_Next.Attributes.Add("style", "display:block");
                    //ValidateTab3_Appeal(true);
                    BindCourtFeeByCourtZone(Convert.ToInt32(ddlCourt.SelectedValue));
                    break;
                case "false":
                    divCourt1_Next.Attributes.Add("style", "display:none");
                    //ValidateTab3_Appeal(false);
                    ClearAppealData();

                    break;
            }
            UpdatePanel1.Update();
        }
        protected void radioCourt2_Next_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (radioCourt2_Next.SelectedValue)
            {
                case "true":
                    divCourt2_Next.Attributes.Add("style", "display:block");
                    //ValidateTab3_Petition(true);
                    BindCourtFeeByCourtZone(Convert.ToInt32(ddlCourt.SelectedValue));
                    break;
                case "false":
                    divCourt2_Next.Attributes.Add("style", "display:none");
                    //ValidateTab3_Petition(false);
                    ClearPetitionData();
                    break;
            }
            UpdatePanel1.Update();
        }
        protected void ddlProsecute1Result_SelectedIndexChanged(object sender, EventArgs e)
        {
            // คำฟ้องถูกต้อง/ไม่ถูกต้อง
            txtProcecuteNote.Value = string.Empty;
            if (Convert.ToBoolean(ddlProsecute1Result.SelectedValue))
            {
                txtProcecuteNote.Attributes.Add("disabled", "disabled");
                //txtProcecuteNote.Attributes.Remove("required");
                txtProcecuteNote.Focus();
            }
            else
            {
                txtProcecuteNote.Attributes.Remove("disabled");
                //txtProcecuteNote.Attributes.Add("required", "required");
                txtProcecuteNote.Focus();
            }
        }
        protected void ddlExct1Result_SelectedIndexChanged(object sender, EventArgs e)
        {
            // คำพิพากษาถูกต้อง/ไม่ถูกต้อง
            txtExct1Desc.Value = string.Empty;
            if (Convert.ToBoolean(ddlExct1Result.SelectedValue))
            {
                txtExct1Desc.Attributes.Add("disabled", "disabled");
                //txtExct1Desc.Attributes.Remove("required");
                txtExct1Desc.Focus();
            }
            else
            {
                txtExct1Desc.Attributes.Remove("disabled");
                //txtExct1Desc.Attributes.Add("required", "required");
                txtExct1Desc.Focus();
            }
        }
        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            int zoneID = Convert.ToInt32(ddlCourt.SelectedValue);
            BindCourtFeeByCourtZone(zoneID);
        }
        protected void btnAdd_CourtAssign_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hidAssignID_modal.Value = string.Empty;
            hidCourtID_modal.Value = string.Empty;
            txtDate_courtModal.Value = string.Empty;
            txtReason_courtModal.Value = string.Empty;
            hidCourtID_modal.Value = "1";
            UpdatePanel2.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "date", "postback();", true);

        }
        protected void btnAdd_AppealAssign_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hidAssignID_modal.Value = string.Empty;
            hidCourtID_modal.Value = string.Empty;
            txtDate_courtModal.Value = string.Empty;
            txtReason_courtModal.Value = string.Empty;
            hidCourtID_modal.Value = "2";
            UpdatePanel2.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "date", "postback();", true);
        }
        protected void btnAdd_PetitionAssign_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hidAssignID_modal.Value = string.Empty;
            hidCourtID_modal.Value = string.Empty;
            txtDate_courtModal.Value = string.Empty;
            txtReason_courtModal.Value = string.Empty;

            hidCourtID_modal.Value = "3";
            UpdatePanel2.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "date", "postback();", true);
        }
        #endregion

        #region GridView Event
        #region gvCourtAssign Event
        protected void gvCourtAssign_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetUpdateCourtAssign();
        }

        protected void gvCourtAssign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvCourtAssign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCourtAssign.PageIndex = e.NewPageIndex;
            gvCourtAssign.DataSource = CourtAssignList;
            gvCourtAssign.DataBind();
        }

        protected void gvCourtAssign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditAA")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string id = (gvCourtAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim();
                string courtID = (gvCourtAssign.Rows[index].FindControl("hidCourtID") as HiddenField).Value.Trim();
                string date = (gvCourtAssign.Rows[index].FindControl("lbDate") as Label).Text.Trim();
                string Reason = gvCourtAssign.Rows[index].Cells[1].Text.Trim();
                // bind to controller
                hidAssignID_modal.Value = id;
                hidCourtID_modal.Value = courtID;
                txtDate_courtModal.Value = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                txtReason_courtModal.Value = Reason;

                hidCourtID_modal.Value = "1";
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            }
            else if (e.CommandName == "Delete")
            {
                ////V1
                //using (trLegal_CourtAssignRep rep = new trLegal_CourtAssignRep())
                //{
                //    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //    int index = rindex.RowIndex;
                //    int id = Convert.ToInt32((gvCourtAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim());
                //    rep.DeleteData(id);
                //}

                //V2
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int id = Convert.ToInt32((gvCourtAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim());
                courtAssignList = CourtAssignList;
                courtAssignList.RemoveAll(courtAssign => courtAssign.AssignID == id);
                CourtAssignList = courtAssignList;
            }
        }
        #endregion

        #region gvAppealAssign Event
        protected void gvAppealAssign_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetUpdateAppealAssign();
        }

        protected void gvAppealAssign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvAppealAssign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAppealAssign.PageIndex = e.NewPageIndex;
            gvAppealAssign.DataSource = AppealAssignList;
            gvAppealAssign.DataBind();
        }

        protected void gvAppealAssign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string id = (gvAppealAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim();
                string courtID = (gvAppealAssign.Rows[index].FindControl("hidCourtID") as HiddenField).Value.Trim();
                string date = (gvAppealAssign.Rows[index].FindControl("lbDate") as Label).Text.Trim();
                string Reason = gvAppealAssign.Rows[index].Cells[1].Text.Trim();

                // bind to controller
                hidAssignID_modal.Value = id;
                hidCourtID_modal.Value = courtID;
                txtDate_courtModal.Value = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                txtReason_courtModal.Value = Reason;

                hidCourtID_modal.Value = "2";
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int id = Convert.ToInt32((gvAppealAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim());
                appealAssignList = AppealAssignList;
                appealAssignList.RemoveAll(courtAssign => courtAssign.AssignID == id);
                AppealAssignList = appealAssignList;
            }
        }
        #endregion

        #region gvPetitionAssign Event
        protected void gvPetitionAssign_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetUpdatePetitionAssign();
        }

        protected void gvPetitionAssign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvPetitionAssign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPetitionAssign.PageIndex = e.NewPageIndex;
            gvPetitionAssign.DataSource = PetitionAssignList;
            gvPetitionAssign.DataBind();
        }

        protected void gvPetitionAssign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string id = (gvPetitionAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim();
                string courtID = (gvPetitionAssign.Rows[index].FindControl("hidCourtID") as HiddenField).Value.Trim();
                string date = (gvPetitionAssign.Rows[index].FindControl("lbDate") as Label).Text.Trim();
                string Reason = gvPetitionAssign.Rows[index].Cells[1].Text.Trim();

                // bind to controller
                hidAssignID_modal.Value = id;
                hidCourtID_modal.Value = courtID;
                txtDate_courtModal.Value = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                txtReason_courtModal.Value = Reason;

                hidCourtID_modal.Value = "3";
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowcor();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int id = Convert.ToInt32((gvPetitionAssign.Rows[index].FindControl("hidAssignID") as HiddenField).Value.Trim());
                petitionAssignList = PetitionAssignList;
                petitionAssignList.RemoveAll(courtAssign => courtAssign.AssignID == id);
                PetitionAssignList = petitionAssignList;
            }
        }
        #endregion
        #endregion

        #region Modal Event
        public string ValidateCourteModal()
        {
            if (string.IsNullOrEmpty(txtDate_courtModal.Value)) return ("กรุณาระบุ \"วันที่นัด\"");
            if (string.IsNullOrEmpty(txtReason_courtModal.Value)) return ("กรุณาระบุ \"เหตุผล\"");
            return string.Empty;
        }

        protected void btnSave_CourtModal_Click(object sender, EventArgs e)
        {
            try
            {
                string err = ValidateCourteModal();
                if (!string.IsNullOrEmpty(err)) throw new Exception(err);

                int assignid = 0;
                int courtid = Convert.ToInt32(hidCourtID_modal.Value);
                DateTime date = Convert.ToDateTime(txtDate_courtModal.Value);
                string reason = txtReason_courtModal.Value.Trim();
                DateTime dateUpdate = DateTime.Now;

                trLegal_CourtAssign ass = new trLegal_CourtAssign();

                switch (courtid)
                {
                    case 1: // CourtAssign
                        if (CommandName.Equals("Add"))
                        {
                            assignid = 1;
                            if (CourtAssignList.Count > 0)
                            {
                                assignid = CourtAssignList.Max(courtAssign => courtAssign.AssignID) + 1;
                            }

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = userCode;
                            ass.CreateDateTime = dateUpdate;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;
                            courtAssignList = CourtAssignList;
                            courtAssignList.Add(ass);
                            CourtAssignList = courtAssignList;
                        }
                        else if (CommandName.Equals("Edit"))
                        {
                            assignid = Convert.ToInt32(hidAssignID_modal.Value);

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = CourtAssignList.Find(courtAssign => courtAssign.AssignID == assignid).CreateBy;
                            ass.CreateDateTime = CourtAssignList.Find(courtAssign => courtAssign.AssignID == assignid).CreateDateTime;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;

                            courtAssignList = CourtAssignList;
                            courtAssignList.RemoveAll(courtAssign => courtAssign.AssignID == assignid);
                            courtAssignList.Add(ass);
                            CourtAssignList = courtAssignList;
                        }
                        GetUpdateCourtAssign();
                        break;
                    case 2: // AppealAssign
                        if (CommandName.Equals("Add"))
                        {
                            assignid = 1;
                            if (AppealAssignList.Count > 0)
                            {
                                assignid = AppealAssignList.Max(x => x.AssignID) + 1;
                            }

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = userCode;
                            ass.CreateDateTime = dateUpdate;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;
                            appealAssignList = AppealAssignList;
                            appealAssignList.Add(ass);
                            AppealAssignList = appealAssignList;
                        }
                        else if (CommandName.Equals("Edit"))
                        {
                            assignid = Convert.ToInt32(hidAssignID_modal.Value);

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = AppealAssignList.Find(appealAssign => appealAssign.AssignID == assignid).CreateBy;
                            ass.CreateDateTime = AppealAssignList.Find(appealAssign => appealAssign.AssignID == assignid).CreateDateTime;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;

                            appealAssignList = AppealAssignList;
                            appealAssignList.RemoveAll(appealAssign => appealAssign.AssignID == assignid);
                            appealAssignList.Add(ass);
                            AppealAssignList = appealAssignList;
                        }
                        GetUpdateAppealAssign();
                        break;
                    case 3: // PetitionAssign
                        if (CommandName.Equals("Add"))
                        {
                            assignid = 1;
                            if (PetitionAssignList.Count > 0)
                            {
                                assignid = PetitionAssignList.Max(x => x.AssignID) + 1;
                            }

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = userCode;
                            ass.CreateDateTime = dateUpdate;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;
                            petitionAssignList = PetitionAssignList;
                            petitionAssignList.Add(ass);
                            PetitionAssignList = petitionAssignList;
                        }
                        else if (CommandName.Equals("Edit"))
                        {
                            assignid = Convert.ToInt32(hidAssignID_modal.Value);

                            ass.AssignID = assignid;
                            ass.LegalNo = LegalData.LegalNo;
                            ass.CourtID = courtid;
                            ass.AssignDate = date;
                            ass.AssignReason = reason;
                            ass.CreateBy = PetitionAssignList.Find(petitionAssign => petitionAssign.AssignID == assignid).CreateBy;
                            ass.CreateDateTime = PetitionAssignList.Find(petitionAssign => petitionAssign.AssignID == assignid).CreateDateTime;
                            ass.UpdateBy = userCode;
                            ass.UpdateDateTime = dateUpdate;

                            petitionAssignList = PetitionAssignList;
                            petitionAssignList.RemoveAll(petitionAssign => petitionAssign.AssignID == assignid);
                            petitionAssignList.Add(ass);
                            PetitionAssignList = petitionAssignList;
                        }
                        GetUpdatePetitionAssign();
                        break;
                }

                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "hide-courtModal", "modalhidecor();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "date", "postback();", true);
            }
        }
        #endregion
    }

    public class Tab3Data
    {
        public trLegal LegalData { get; set; }
        public List<trLegal_CourtAssign> CourtAssign { get; set; }
        public List<trLegal_CourtAssign> AppealAssign { get; set; }
        public List<trLegal_CourtAssign> PetitionAssign { get; set; }
    }
}