﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab5.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab5" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<script type="text/javascript">
    function validateAlert(v_text) {
        alert(v_text);
    }
    function modalshowpursue() {
        $('#pursueModal').modal('toggle');
    }
    function modalshowpdf() {
        $('#pdfModal').modal('toggle');
    }
    function modalshowasset() {
        $('#assetModal').modal('toggle');
    }
    function modalshowsequester() {
        //$('#attachModal').modal('toggle');
        $('#sequesterModal').modal('toggle');
    }
    function modalshowattach1() {
        $('#attachModal').modal('toggle');
    }
    function modalshowattach() {
        $('#attachModal').modal('toggle');
    }
    function modalshowassetSeq() {
        $('#assetSeqModal').modal('toggle');
    }
    function modalshowauction() {
        $('#auctionModal').modal('toggle');
    }
    function modalshowAuctionAssign() {
        $('#auctionAssignModal').modal('toggle');
    }
    function modalshowAuctionAsset() {
        $('#auctionAssetModal').modal('toggle');
    }
    function modalhidepursue() {
        $('#pursueModal').modal('hide');
    }
    function modalhidepdf() {
        $('#pdfModal').modal('hide');
    }
    function modalhideasset() {
        $('#assetModal').modal('hide');
    }
    function modalhidesequester() {
        $('#sequesterModal').modal('hide');
    }
    function modalhideattach1() {
        $('#attachModal').modal('hide');
    }
    function modalhideassetseq() {
        $('#assetSeqModal').modal('hide');
    }
    function modalhideauction() {
        $('#auctionModal').modal('hide');
    }
    function modalhideAuctionAssign() {
        $('#auctionAssignModal').modal('hide');
    }
    function modalhideAuctionAsset() {
        $('#auctionAssetModal').modal('hide');
    }
    function modalhidedoclist() {
        $('#doclistModal').modal('hide');
    }
</script>

<asp:Panel ID="Panel1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">สืบทรัพย์</h5>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                                                <ContentTemplate>--%>
                                <div class="col-md-12 col-sm-12">
                                    <asp:Button ID="btnAdd_Peusue" runat="server" Text="Add" CssClass="btn btn-info btn-sm"
                                        OnClick="btnAdd_Peusue_Click" UseSubmitBehavior="false" />
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <%-- CssClass="table table-striped table-no-bordered table-hover" GridLines="None" --%>
                                    <asp:GridView ID="gvPursue" runat="server" AutoGenerateColumns="False" Font-Size="12px" EmptyDataText="No data"
                                        CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvPursue_RowDataBound" OnPageIndexChanging="gvPursue_PageIndexChanging"
                                        OnRowCommand="gvPursue_RowCommand" OnRowDeleting="gvPursue_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="สืบครั้งที่">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbPDate" runat="server" Text='<%#Eval("PursueTime")%>'></asp:Label>
                                                    <asp:HiddenField ID="hidPID" runat="server" Value='<%#Eval("PursueID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="พบ/ไม่พบ" DataField="FoundAssets" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่สืบทรัพย์สิน" DataField="PusrsueDate" />
                                            <%--<asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="btnView" runat="server" Text="View" CssClass="btn btn-fill btn-info  btn-sm" CommandName="ViewCommand" />
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                                                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%--<asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm"
                                                                                                                        CausesValidation="false" data-toggle="modal" data-target="#pursueModal" CommandName="EditCommand" />--%>
                                                    <asp:Button ID="btnEdit" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">Assign สืบทรัพย์</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5><span class="tim-note">ทนายใน/นอก :</span></h5>
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlAttorneyType51" runat="server" AutoPostBack="true"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlAttorneyType51_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5>
                                        <span class="tim-note">ชื่อทนาย :</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlLawyer51" runat="server" AutoPostBack="true"
                                        CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5>
                                        <span class="tim-note">วันส่งเรื่องให้ทนาย :</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <input type="text" id="txtSentoLawyer_p" runat="server" class="datepicker form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">ยึดทรัพย์/อายัดทรัพย์</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                                                <ContentTemplate>--%>
                                <div class="col-md-12 col-sm-12 ">
                                    <asp:Button ID="btnAdd_Seq" runat="server" Text="Add" CssClass="btn btn-info btn-sm" OnClick="btnAdd_Seq_Click" UseSubmitBehavior="false" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <%-- table table-striped table-no-bordered table-hover dataTable dtr-inline --%>
                                    <asp:GridView ID="gvSequester" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvSequester_RowDataBound" OnPageIndexChanging="gvSequester_PageIndexChanging"
                                        OnRowCommand="gvSequester_RowCommand" OnRowDeleting="gvSequester_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ยึดครั้งที่">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbSeqTime" runat="server" Text='<%#Eval("SeqTime")%>'></asp:Label>
                                                    <asp:HiddenField ID="hidSeqID" runat="server" Value='<%#Eval("SequesterID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันตั้งเรื่องยึด" DataField="SeqDate" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันตั้งเรื่องอายัด" DataField="Seq2Date" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="หมายเหตุ" DataField="Comment" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">Assign ยึดทรัพย์/ขายทอดตลาด</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5><span class="tim-note">ทนายใน/นอก :</span></h5>
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlAttorneyType52" runat="server" AutoPostBack="true"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlAttorneyType52_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5>
                                        <span class="tim-note">ชื่อทนาย :</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlLawyer52" runat="server" AutoPostBack="true"
                                        CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5>
                                        <span class="tim-note">วันส่งเรื่องให้ทนาย :</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <input type="text" id="txtSentoLawyer_s" runat="server" class="datepicker form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">ขอรับชำระหนี้จำนอง</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <asp:Button ID="btnAdd_morgage2" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="btnAdd_morgage_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvMortgage" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateEditButton="true" AutoGenerateDeleteButton="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvMortgage_RowDataBound" OnPageIndexChanging="gvMortgage_PageIndexChanging"
                                        OnRowCommand="gvMortgage_RowCommand" OnRowEditing="gvMortgage_RowEditing1"
                                        OnRowUpdating="gvMortgage_RowUpdating" OnRowCancelingEdit="gvMortgage_RowCancelingEdit" OnRowDeleting="gvMortgage_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidMorID_shw" runat="server" Value='<%#Eval("MortgageID")%>' />
                                                    <asp:Label ID="lbMorDate" runat="server" Text='<%#Eval("MorDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="hidMorID_edt" runat="server" Value='<%#Eval("MortgageID")%>' />
                                                    <asp:TextBox ID="txtMorDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("MorDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="รายละเอียดทรัพย์">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAssetID" runat="server" />
                                                    <asp:Label ID="lbAssDesc" runat="server" Text='<%#Eval("AssetDesc")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlAsset" runat="server" CssClass="form-control"
                                                        DataValueField="AssetID" DataTextField="AssetDesc">
                                                        <asp:ListItem Text="- กรุณาเลือก -" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียมขอรับชำระหนี้">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbMorFee" runat="server" Text='<%#Eval("MorFee")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <%--<input id="" runat="server" class="form-control" type="number" step="any" value='<%#Eval("MorFee")%>' />--%>
                                                    <asp:TextBox ID="morFee" runat="server" CssClass="form-control" Text='<%#Eval("MorFee")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันนัดไต่สวน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbMorInvestDate" runat="server" Text='<%#Eval("InvestigateDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtInvestDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("InvestigateDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลมีคำสั่ง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbFiatDate" runat="server" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFiatDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ยื่นคำสั่งให้ จพค.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbHandDate" runat="server" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txHandDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">ขอเฉลี่ยทรัพย์</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <asp:Button ID="btnadd_average" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="btnadd_average_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvAverage" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateEditButton="True" AutoGenerateDeleteButton="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvAverage_RowDataBound" OnPageIndexChanging="gvAverage_PageIndexChanging"
                                        OnRowCommand="gvAverage_RowCommand" OnRowEditing="gvAverage_RowEditing1"
                                        OnRowUpdating="gvAverage_RowUpdating" OnRowCancelingEdit="gvAverage_RowCancelingEdit" OnRowDeleting="gvAverage_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAvgID_shw" runat="server" Value='<%#Eval("AverageID")%>' />
                                                    <asp:Label ID="lbAvgDate" runat="server" Text='<%#Eval("AvrDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="hidAvgID_edt" runat="server" Value='<%#Eval("AverageID")%>' />
                                                    <asp:TextBox ID="txtAvgDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("AvrDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="รายละเอียดทรัพย์">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAssetID" runat="server" />
                                                    <asp:Label ID="lbAssDesc" runat="server" Text='<%#Eval("AssetDesc")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlAsset" runat="server" CssClass="form-control"
                                                        DataValueField="AssetID" DataTextField="AssetDesc">
                                                        <asp:ListItem Text="- กรุณาเลือก -" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียมขอเฉลี่ยทรัพย์">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbAvgFee" runat="server" Text='<%#Eval("AvrFee")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAvgFee" runat="server" CssClass="form-control" Text='<%#Eval("AvrFee")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันนัดไต่สวน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbAvgInvestDate" runat="server" Text='<%#Eval("InvestigateDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtInvestDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("InvestigateDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลมีคำสั่ง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbFiatDate" runat="server" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFiatDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ยื่นคำสั่งให้ จพค.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbHandDate" runat="server" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txHandDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่รับเงิน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveDate" runat="server" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงินที่รับ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveAmt" runat="server" Text='<%#Eval("ReceiveAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveAmt" runat="server" CssClass="form-control" Text='<%#Eval("ReceiveAmt")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">ขอกันส่วน</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <asp:Button ID="gvAdd_Retain" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="gvAdd_Retain_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvRetain" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvRetain_RowDataBound" OnPageIndexChanging="gvRetain_PageIndexChanging"
                                        OnRowCommand="gvRetain_RowCommand" OnRowEditing="gvRetain_RowEditing"
                                        OnRowUpdating="gvRetain_RowUpdating" OnRowCancelingEdit="gvRetain_RowCancelingEdit" OnRowDeleting="gvRetain_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="btnDelete" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="btnUpdate" CommandName="Update" runat="server">Update</asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" CommandName="Cancel" runat="server">Cancel</asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidRetainID_shw" runat="server" Value='<%#Eval("RetainID")%>' />
                                                    <asp:Label ID="lbRetainDate" runat="server" Text='<%#Eval("RetDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="hidRetainID_edt" runat="server" Value='<%#Eval("RetainID")%>' />
                                                    <asp:TextBox ID="txtRetainDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("RetDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="รายละเอียดทรัพย์">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAssetID" runat="server" />
                                                    <asp:Label ID="lbAssDesc" runat="server" Text='<%#Eval("AssetDesc")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlAsset" runat="server" CssClass="form-control"
                                                        DataValueField="AssetID" DataTextField="AssetDesc">
                                                        <asp:ListItem Text="- กรุณาเลือก -" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียมขอกันส่วน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbRetainFee" runat="server" Text='<%#Eval("RetFee")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRetainFee" runat="server" CssClass="form-control" Text='<%#Eval("RetFee")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันนัดไต่สวน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbAvgInvestDate" runat="server" Text='<%#Eval("InvestigateDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtInvestDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("InvestigateDate")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลมีคำสั่ง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbFiatDate" runat="server" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFiatDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("FiatDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ยื่นคำสั่งให้ จพค.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbHandDate" runat="server" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txHandDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("HandDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่รับเงิน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveDate" runat="server" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงินที่รับ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveAmt" runat="server" Text='<%#Eval("ReceiveAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveAmt" runat="server" CssClass="form-control" Text='<%#Eval("ReceiveAmt")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">ขายทอดตลาด</h5>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 ">
                                    <asp:Button ID="btnAddAuction" runat="server" Text="Add" CssClass="btn btn-info btn-sm" OnClick="btnAddAuction_Click" UseSubmitBehavior="false" />
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <asp:GridView ID="gvAuction" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvAuction_RowDataBound" OnPageIndexChanging="gvAuction_PageIndexChanging"
                                        OnRowCommand="gvAuction_RowCommand" OnRowDeleting="gvAuction_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ลำดับการบันทึก">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidaucID" runat="server" Value='<%#Eval("AuctionID")%>' />
                                                    <asp:Label ID="lbAuctionOrder" runat="server" Text='<%#Eval("AuctionOrder")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="นัดที่" DataField="ActionTime" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่นัด" DataField="AuctionDate" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="หมายเหตุ" DataField="Remark" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;">
                        <div class="card-header card-header-tabs card-header-warning">
                            <h5 class="card-title">อายัดสิทธิ์เรียกร้อง</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <asp:Button ID="gvadd_request" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="gvadd_request_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvReqSeq" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateEditButton="true" AutoGenerateDeleteButton="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvReqSeq_RowDataBound" OnPageIndexChanging="gvReqSeq_PageIndexChanging"
                                        OnRowCommand="gvReqSeq_RowCommand" OnRowEditing="gvReqSeq_RowEditing"
                                        OnRowUpdating="gvReqSeq_RowUpdating" OnRowCancelingEdit="gvReqSeq_RowCancelingEdit" OnRowDeleting="gvReqSeq_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ลำดับที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidReqseqID_shw" runat="server" Value='<%#Eval("ReqSeqID")%>'></asp:HiddenField>
                                                    <asp:Label ID="lbreqNo" runat="server" Text='<%#Eval("ReqSeqNo")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="hidReqseqID" runat="server" Value='<%#Eval("ReqSeqID")%>'></asp:HiddenField>
                                                    <asp:DropDownList ID="ddlReqseqNo" runat="server" CssClass="form-control"
                                                        DataValueField="ReqSeqNo" DataTextField="ReqSeqNo">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่รับเงิน">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveDate" runat="server" Text='<%#Eval("ReqSeqDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("ReqSeqDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงินที่รับ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbReceiveAmt" runat="server" Text='<%#String.Format("{0:f2}",Eval("ReqSeqAmt"))%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtReceiveAmt" runat="server" CssClass="form-control" Text='<%#Eval("ReqSeqAmt")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%-- Pursue Modal --%>
    <div id="pursueModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div class="modal-header">
                            ข้อมูลสืบทรัพย์
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <%-- Modal --%>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    ครั้งที่
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidid_pursuemodal" runat="server" />
                                        <input type="number" id="txtPTime" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    วันที่สืบทรัพย์
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="txtdate_pursuemodal" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    พบ/ไม่พบ
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlFoundAsset_pursuemodal" runat="server" CssClass="form-control"
                                            CausesValidation="true" EnableViewState="true"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlFoundAsset_pursuemodal_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="ไม่พบ"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="พบ"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="hiddiv_pursuemodal" runat="server" style="display: none">
                                <div class="row">
                                    <label class="col-sm-12 col-form-label">
                                        รายละเอียดทรัพย์
                                    <label style="color: red">*</label>
                                        :</label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:Button ID="btnAddDoc_PursueModal" runat="server" Text="Add" CssClass="btn btn-fill btn-info btn-sm"
                                            OnClick="btnAddDoc_PursueModal_Click" UseSubmitBehavior="false" />
                                        <asp:GridView ID="gvAssetView" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                            AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" DataKeyNames="AssetID"
                                            EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                            AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                            PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                            PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                            OnRowDataBound="gvAssetView_RowDataBound" OnPageIndexChanging="gvAssetView_PageIndexChanging"
                                            OnRowCommand="gvAssetView_RowCommand" OnRowDeleting="gvAssetView_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ประเภท">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hidAssetID" runat="server" Value='<%#Eval("AssetID")%>' />
                                                        <asp:Label ID="lbAssetType" runat="server" Text='<%#Eval("AssetTypeName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ที่อยู่">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("District")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("Province")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("ZipCode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-12 col-form-label">แนบสิ่งที่สืบพบ :</label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <asp:Button ID="btnUpload_PursueModal" runat="server" Text="Add" CssClass="btn btn-fill btn-info btn-sm"
                                                OnClick="btnUpload_PursueModal_Click" UseSubmitBehavior="false" />
                                            <asp:GridView ID="gvPursueAttach" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                                AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                                OnRowDataBound="gvPursueAttach_RowDataBound" OnPageIndexChanging="gvPursueAttach_PageIndexChanging"
                                                OnRowCommand="gvPursueAttach_RowCommand" OnRowDeleting="gvPursueAttach_RowDeleting">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ชื่อ">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hidDocID" runat="server" Value='<%#Eval("DocID")%>' />
                                                            <asp:Label ID="lbDocName" runat="server" Text='<%#Eval("DocName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ประเภท">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hidDocTypeID" runat="server" Value='<%#Eval("DocTypeID")%>' />
                                                            <asp:Label ID="lbDocType" runat="server" Text='<%#Eval("DocTypeName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkPcName" runat="server" Text="View" Target="_blank" CssClass="btn btn-fill btn-info btn-sm"
                                                                CommandName="ViewCommand" NavigateUrl='<%# "~/Pages/Helper/PDFPage.aspx?val=" + Eval("DocID") %>'></asp:HyperLink>
                                                            <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="ViewCommand" />--%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_PursueModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_PursueModal_Click" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Sequester Modal--%>
    <div id="sequesterModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width: 1000px;">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลยึด/อายัดทรัพย์
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <%-- Modal --%>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    ยึด/อายัดครั้งที่
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-2">
                                    <asp:HiddenField ID="hidpurID_seqmodal" runat="server" />
                                    <asp:HiddenField ID="hidseqID_seqmodal" runat="server" />
                                    <input type="number" id="txtSTime_seqmodal" runat="server" class="form-control" />
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    วันที่รับหนังสือให้ส่งโฉนด :</label>
                                <div class="col-sm-2">
                                    <input type="text" id="txtdate1_seqmodal" runat="server" class="datepicker form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    โจทย์ผู้นำยึด :</label>
                                <div class="col-sm-2">
                                    <input type="text" id="txtProposition" runat="server" class="form-control" />
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    วันที่ส่งโฉนด/สัญญา :</label>
                                <div class="col-sm-2">
                                    <input type="text" id="txtdate2_seqmodal" runat="server" class="datepicker form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    สำนักงานบังคับคดี :</label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlExecution" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    ภาระหนี้ ณ วันที่นำส่ง :</label>
                                <div class="col-sm-2">
                                    <input type="number" step="any" id="txtDebt_sqemodal" runat="server" class="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    ศาล :</label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlCourtZone_seqmodal" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    วิธีขาย :</label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlSoldType" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    วันตั้งเรื่องยึด
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <input type="text" id="txtseq2date" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    ค่าธรรมเนียมตั้งเรื่องยึด :</label>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <input type="number" step="any" id="txtseqfee" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    วันตั้งเรื่องอายัด
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <input type="text" id="txtdate4_seqmodal" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    ค่าธรรมเนียมตั้งเรื่องอายัด :</label>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <input type="number" step="any" id="seq2fee_seqmodal" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right align-content-center">
                                    แปลงที่ยึด :</label>
                                <div class="col-sm-7">
                                    <asp:Button ID="btnAddAsset_seqmodal" runat="server" Text="Add/Edit" CssClass="btn btn-fill btn-info btn-sm"
                                        OnClick="btnAddAsset_seqmodal_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvSequesterView" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvSequesterView_RowDataBound" OnPageIndexChanging="gvSequesterView_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ยึดครั้งที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidSeqID" runat="server" Value='<%#Eval("SequesterID")%>' />
                                                    <asp:HiddenField ID="hidAssetID" runat="server" Value='<%#Eval("AssetID")%>' />
                                                    <asp:Label ID="lbSeqTime" runat="server" Text='<%#Eval("SeqTime")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ทรัพย์สิน">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAssetTypeID" runat="server" Value='<%#Eval("AssetTypeID")%>' />
                                                    <asp:Label ID="lbAssetTypeName" runat="server" Text='<%#Eval("AssetTypeName")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbDistrict" runat="server" Text='<%#Eval("District")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbProvince" runat="server" Text='<%#Eval("Province")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbZipcode" runat="server" Text='<%#Eval("ZipCode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 col-form-label text-right  align-content-center">หมายเหตุ :</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input id="txtRemark_seqmodal" runat="server" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_seqmodal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_seqmodal_Click" UseSubmitBehavior="false" />&nbsp;
                    <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Auction Modal --%>
    <div id="auctionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลขายทอดตลาด
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    ลำดับการบันทึก
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidAucID_auctionmodal" runat="server" />
                                        <input type="text" id="txtOrder_auctionmodal" runat="server" class="form-control" />
                                        <%--<asp:TextBox ID="txtOrder_auctionmodal" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-12 col-form-label">
                                    นัดการขายทอดตลาด
                                    <label style="color: red">*</label>
                                    :</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:Button ID="btnAdd_auctionModal" runat="server" Text="Add" CssClass="btn btn-fill btn-info btn-sm" OnClick="btnAddAuctionAssign_auction_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvAuctionAssign" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvAuctionAssign_RowDataBound" OnPageIndexChanging="gvAuctionAssign_PageIndexChanging"
                                        OnRowCommand="gvAuctionAssign_RowCommand" OnRowDeleting="gvAuctionAssign_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="นัดที่">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAuctionID" runat="server" Value='<%#Eval("AuctionID")%>' />
                                                    <asp:Label ID="lbAuctionTime" runat="server" Text='<%#Eval("ActionTime")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่นัด" DataField="AuctionDate" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="หมายเหตุ" DataField="Remark" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">แนบประกาศขาย : </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:Button ID="btnadd_aucdoc" runat="server" Text="Add" CssClass="btn btn-fill btn-info btn-sm" OnClick="btnadd_aucdoc_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvAuctionDoc" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvAuctionDoc_RowDataBound" OnPageIndexChanging="gvAuctionDoc_PageIndexChanging"
                                        OnRowCommand="gvAuctionDoc_RowCommand" OnRowDeleting="gvAuctionDoc_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="เอกสาร">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidDocID" runat="server" Value='<%#Eval("DocID")%>' />
                                                    <asp:Label ID="lbDocName" runat="server" Text='<%#Eval("DocName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkPcName" runat="server" Text="View" Target="_blank" CssClass="btn btn-fill btn-info btn-sm"
                                                        CommandName="ViewCommand" NavigateUrl='<%# "~/Pages/Helper/PDFPage.aspx?val=" + Eval("DocID") %>'></asp:HyperLink>
                                                    <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="ViewCommand" />--%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-12 col-form-label">ทรัพย์สินที่ประกาศขาย : </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:Button ID="btnAddauctionAsset_auction" runat="server" Text="Add" CssClass="btn btn-fill btn-info btn-sm" OnClick="btnAddauctionAsset_auction_Click" UseSubmitBehavior="false" />
                                    <asp:GridView ID="gvAuctionAsset" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnRowDataBound="gvAuctionAsset_RowDataBound" OnPageIndexChanging="gvAuctionAsset_PageIndexChanging"
                                        OnRowCommand="gvAuctionAsset_RowCommand" OnRowDeleting="gvAuctionAsset_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ทรัพย์สิน">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidAucID" runat="server" Value='<%#Eval("AuctionID")%>' />
                                                    <asp:HiddenField ID="hidAsset" runat="server" Value='<%#Eval("AssetID")%>' />
                                                    <asp:Label ID="lbAssTypeName" runat="server" Text='<%#Eval("AssetTypeName")%>'></asp:Label>
                                                    :
                                                    <asp:Label ID="lbAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbDistrict" runat="server" Text='<%#Eval("District")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbProvince" runat="server" Text='<%#Eval("Province")%>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lbZipcode" runat="server" Text='<%#Eval("ZipCode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnEdit" runat="server" Text="View" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_auctionModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_auctionModal_Click" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Auction Assign Modal --%>
    <div id="auctionAssignModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลนัดขายทอดตลาด
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="intimesdiv_aucassign" runat="server">
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">
                                        ลำดับการบันทึก
                                    <label style="color: red">*</label>
                                        :</label>
                                    <div class="col-sm-8">
                                        <asp:HiddenField ID="hidAucID_auctionassignModal" runat="server" />
                                        <input type="text" id="txtOrder_auctionassignModal" runat="server" class="form-control" disabled="disabled" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">
                                        นัดที่
                                    <label style="color: red">*</label>
                                        :</label>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlAuctionTime_auctionAssignModal" runat="server" class="form-control"
                                                DataValueField="ordervalue" DataTextField="ordertext">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">
                                        วันที่ประกาศขาย
                                    <label style="color: red">*</label>
                                        :</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="txtDate_auctionAssignModal" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 col-form-label">
                                        หมายเหตุ
                                    <label style="color: red">*</label>
                                        :</label>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" id="txtRemark_auctionAssignModal" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="outoftimesdiv_aucassign" runat="server">
                                <label class="col-sm-12 col-form-label text-center align-content-center">ไม่สามารถเพิ่มนัดได้</label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_auctionassignModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_auctionassignModal_Click" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Auction Asset Modal--%>
    <div id="auctionAssetModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width: 800px;">
                <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลทรัพย์สินขายทอดตลาด
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="foundassetdiv" runat="server">
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        แปลงที่ยึด :</label>
                                    <div class="col-sm-8">
                                        <asp:HiddenField ID="hidAuctionid_modal" runat="server" />
                                        <asp:HiddenField ID="hidorder_auctionassetmodal" runat="server" />
                                        <asp:DropDownList ID="ddlAsset_auctionassetmodal" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="" Text="- กรุณาเลือก -"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        วันที่บุคคลภายนอกซื้อ :</label>
                                    <div class="col-sm-2">
                                        <input type="text" id="txtOtherDate_modal" runat="server" class="datepicker form-control" />
                                    </div>
                                    <label class="col-sm-4 col-form-label text-right align-content-center">
                                        จำนวนที่ขายได้ :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtOtherAmt_modal" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        วันที่ธนาคารซื้อทรัพย์ :</label>
                                    <div class="col-sm-2">
                                        <input type="text" id="txtBankBuy_modal" runat="server" class="datepicker form-control" />
                                    </div>
                                    <label class="col-sm-4 col-form-label text-right align-content-center">
                                        ราคาที่ธนาคารซื้อ :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtBankAmt_modal" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        วันที่รับเงินจากการขาย :</label>
                                    <div class="col-sm-2">
                                        <input type="text" id="txtGetMoneyDate" runat="server" class="datepicker form-control" />
                                    </div>
                                    <label class="col-sm-4 col-form-label text-right align-content-center">
                                        บัญชีรับ-จ่าย/จำนวนเงินที่รับ :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtGetMoneyAmt" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        ราคา :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtPrice_modal" runat="server" class="form-control" />
                                    </div>
                                    <label class="col-sm-4 col-form-label text-right align-content-center">
                                        ราคาที่บอร์ดอนุมัติ :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtPriceBoard_modal" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        ราคาที่บอร์ดอนุมัติขาย :</label>
                                    <div class="col-sm-2">
                                        <input type="number" step="any" id="txtPriceApprove" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label text-right align-content-center">
                                        หมายเหตุ :</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="txtremark_modal" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div id="notfoundassetdiv" runat="server">
                                <label class="col-sm-12 col-form-label text-center align-content-center">ไม่พบทรัพย์สิน</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_auctionasset" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_auctionasset_Click" UseSubmitBehavior="false" />&nbsp;
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Asset Modal --%>
    <div id="assetModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลทรัพย์
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <%-- Modal --%>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    ประเภท
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidAssetID" runat="server" />
                                        <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">ที่อยู่ :</label>
                                <div class="col-sm-8">
                                    <input id="txtAddress_assetmodal" type="text" runat="server" class="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">เขต/อำเภอ :</label>
                                <div class="col-sm-8">
                                    <input id="txtDistrinct_assetmodal" runat="server" type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    จัดหวัด :</label>
                                <div class="col-sm-8">
                                    <input id="txtProvince_assetmodal" runat="server" type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    รหัสไปรษณีย์ :</label>
                                <div class="col-sm-8">
                                    <input id="txtZipcode_assetmodal" runat="server" type="text" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_AssetModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_AssetModal_Click" UseSubmitBehavior="false" />&nbsp;
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Select Asset Modal--%>
    <div id="assetSeqModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            ข้อมูลทรัพย์สิน
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <%-- Modal --%>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    รายการทรัพย์ :
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:GridView ID="gvSelectAssetSeq" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                        OnPageIndexChanging="gvSelectAssetSeq_PageIndexChanging" OnRowDataBound="gvSelectAssetSeq_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%--<asp:CheckBox ID="CheckBox1"  runat="server"
                                                        Checked='<%# Eval("SArchive") == DBNull.Value ? false : Convert.ToBoolean(Eval("SArchive")) %>'/>--%>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="สืบครั้งที่">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbPurTime" runat="server" Text='<%#Eval("PursueTime")%>'></asp:Label>
                                                    <asp:HiddenField ID="hidPurID" runat="server" Value='<%#Eval("PursueID")%>' />
                                                    <asp:HiddenField ID="hidSeqID" runat="server" Value='<%#Eval("SequesterID")%>' />
                                                    <asp:HiddenField ID="hidAssetID" runat="server" Value='<%#Eval("AssetID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ที่อยู่">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("AssetTypeName")%>'></asp:Label></b>&nbsp;
                                                        <asp:Label ID="lbAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("District")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("Province")%>'></asp:Label>&nbsp;
                                                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("ZipCode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_AssetSeqModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_AssetSeqModal_Click" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- Document Modal --%>
    <div id="attachModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-header">
                            เอกสารแนบ
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    ประเภทเอกสาร
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidFlag_attachmodal" runat="server" />
                                        <asp:DropDownList ID="ddlDoctype_attachmodal" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">เลือกเอกสาร :</label>
                                <div class="col-sm-8">
                                    <iframe id="iRender" runat="server" src="~/Pages/Helper/UploadFile.aspx" style="width: 100%; border: 0;"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnsave_attachdoc" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnsave_attachdoc_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%-- PDF Modal --%>
    <div id="pdfModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                            <iframe id="iPDF" runat="server" src=""
                                width="400px" height="500px" scrolling="yes"></iframe>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
