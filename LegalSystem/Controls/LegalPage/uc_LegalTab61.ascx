﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab61.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab61" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<asp:Panel ID="Panel1" runat="server">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
            <%--<div class="col-md-12 col-lg-12 col-sm-1 col-1 "></div>--%>
            <asp:Button ID="btnadd_cost" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="btnadd_cost_Click" />
            <asp:GridView ID="gvCost" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                OnRowDataBound="gvCost_RowDataBound"
                OnRowCommand="gvCost_RowCommand" OnRowEditing="gvCost_RowEditing1"
                OnRowUpdating="gvCost_RowUpdating" OnRowCancelingEdit="gvCost_RowCancelingEdit" OnRowDeleting="gvCost_RowDeleting">
                <Columns>
                    <asp:TemplateField ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandName="Update" runat="server">Update</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server">Cancel</asp:LinkButton>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าใช้จ่าย">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidCostID_shw" runat="server" Value='<%#Eval("CostID")%>' />
                            <asp:HiddenField ID="hidCostTypeID" runat="server" Value='<%#Eval("CostTypeID")%>' />
                            <asp:Label ID="lbCostType" runat="server" Text='<%#Eval("CostTypeName")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:HiddenField ID="hidCostID_edt" runat="server" Value='<%#Eval("CostID")%>' />
                            <asp:DropDownList ID="ddlCostType" runat="server" CssClass="form-control"
                                DataValueField="CostTypeID" DataTextField="CostTypeName" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlCostType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="OA/IA">
                        <ItemTemplate>
                            <asp:Label ID="lbAttorney" runat="server" Text='<%#Eval("AttorneyTypeName")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lbAttorney" runat="server" Text='<%#Eval("AttorneyTypeName")%>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่เกิดค่าใช้จ่าย">
                        <ItemTemplate>
                            <asp:Label ID="lbCostDate" runat="server" Text='<%#Eval("CostDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCostDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("CostDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงิน">
                        <ItemTemplate>
                            <asp:Label ID="lbCostAmt" runat="server" Text='<%#String.Format("{0:f2}", Eval("CostAmt"))%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCostAmt" runat="server" CssClass="form-control" Text='<%#Eval("CostAmt")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="รายละเอียด">
                        <ItemTemplate>
                            <asp:Label ID="lbCostDesc" runat="server" Text='<%#Eval("CostDesc")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCostDesc" runat="server" CssClass="form-control" Text='<%#Eval("CostDesc")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่จ่าย">
                        <ItemTemplate>
                            <asp:Label ID="lbPayDate" runat="server" Text='<%#Eval("PayDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPayDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("PayDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงินที่จ่าย">
                        <ItemTemplate>
                            <asp:Label ID="lbPayAmt" runat="server" Text='<%#String.Format("{0:f2}", Eval("PayAmt"))%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPayAmt" runat="server" CssClass="form-control" Text='<%#Eval("PayAmt")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่รับ">
                        <ItemTemplate>
                            <asp:Label ID="lbReceiveDate" runat="server" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReceiveDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("ReceiveDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="จำนวนเงินที่รับ">
                        <ItemTemplate>
                            <asp:Label ID="lbReceiveAmt" runat="server" Text='<%#String.Format("{0:f2}", Eval("Income"))%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReceiveAmt" runat="server" CssClass="form-control" Text='<%#Eval("Income")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Advise No.">
                        <ItemTemplate>
                            <asp:Label ID="lbAdviseNo" runat="server" Text='<%#Eval("AdviseNo")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAdviseNo" runat="server" CssClass="form-control" Text='<%#Eval("AdviseNo")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-sm-10 text-right text"><a class="font-bold">รวมค่าใช้จ่าย</a></div>
        <div class="col-md-2 col-sm-2">
            <asp:Label ID="lbCostAmt" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label7" runat="server" Text=" บาท"></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-sm-10 text-right"><a class="font-bold">รวมรายจ่าย</a></div>
        <div class="col-md-2 col-sm-2">
            <asp:Label ID="lbPayAmt" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label9" runat="server" Text=" บาท"></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-sm-10 text-right"><a class="font-bold">รวมรายรับ</a></div>
        <div class="col-md-2 col-sm-2">
            <asp:Label ID="lbReceiveAmt" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label11" runat="server" Text=" บาท"></asp:Label>
        </div>
    </div>
</asp:Panel>
