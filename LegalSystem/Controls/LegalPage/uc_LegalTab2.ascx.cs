﻿//using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalSystem.Controls;
using System.Transactions;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using LegalEntities;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab2 : System.Web.UI.UserControl
    {
        #region Properties
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public trLegal LegalData
        {
            get
            {
                return (trLegal)Session["LegalData"];
            }
            set
            {
                Session["LegalData"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);
            //if (!IsPostBack)
            //{
            //}
        }
        public string ValidateTab()
        {
            string err = string.Empty;
            if (string.IsNullOrEmpty(datePintNotice.Value)) err = "กรุณากรอก \"วันออก Notice\"";
            if (string.IsNullOrEmpty(dateAssignExecute.Value)) err = "กรุณากรอก \"วันออกกำหนดฟ้อง\"";
            if (string.IsNullOrEmpty(dateExecute.Value)) err = "กรุณากรอก \"วันที่ฟ้อง\"";
            return err;
        }
        public trLegal GetUI()
        {
            LegalData.T2_AssignSueDate = Convert.ToDateTime(dateAssignExecute.Value);
            LegalData.T2_GenNoticeDate = Convert.ToDateTime(datePintNotice.Value);
            LegalData.T2_SueDate = Convert.ToDateTime(dateExecute.Value);
            return LegalData;
        }
        public void SetUI(trLegal data)
        {
            if (LegalData.T2_GenNoticeDate != null) datePintNotice.Value = ((DateTime)LegalData.T2_GenNoticeDate).ToString("dd/MM/yyyy");
            if (LegalData.T2_AssignSueDate != null) dateAssignExecute.Value = ((DateTime)LegalData.T2_AssignSueDate).ToString("dd/MM/yyyy");
            if (LegalData.T2_SueDate != null) dateExecute.Value = ((DateTime)LegalData.T2_SueDate).ToString("dd/MM/yyyy");
        }
    }
}