﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab5 : System.Web.UI.UserControl
    {
        #region Properties
        public LegalDbContext context;
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }
        public string SubCommandName
        {
            get
            {
                return (string)Session["SubCommandName"];
            }
            set
            {

                Session["SubCommandName"] = value;
            }
        }
        public trLegal LegalData
        {
            get
            {
                return (trLegal)Session["LegalData"];
            }
            set
            {
                Session["LegalData"] = value;
            }
        }
        private List<trDocument> attachDocList;
        public List<trDocument> AttachDocList
        {
            get
            {
                return (List<trDocument>)Session["AttachDocList"];
            }
            set
            {
                Session["AttachDocList"] = value;
            }
        }

        private List<trDocumentMapping> docMappingList;
        public List<trDocumentMapping> DocMappingList
        {
            get
            {
                return (List<trDocumentMapping>)Session["DocMappingList"];
            }
            set
            {
                Session["DocMappingList"] = value;
            }
        }

        private List<vDocumentMapping> vattachDocList;
        public List<vDocumentMapping> vAttachDocList
        {
            get
            {
                return (List<vDocumentMapping>)Session["vAttachDocList"];
            }
            set
            {
                Session["vAttachDocList"] = value;
            }
        }

        private List<vDocumentMapping> documentByTimeList;
        public List<vDocumentMapping> DocumentByTimeList
        {
            get
            {
                return (List<vDocumentMapping>)Session["DocumentByTimeList"];
            }
            set
            {
                Session["DocumentByTimeList"] = value;
            }
        }
        private List<trAsset> assetList;
        public List<trAsset> AssetList
        {
            get
            {
                return (List<trAsset>)Session["AssetList"];
            }
            set
            {
                Session["AssetList"] = value;
            }
        }
        private List<trAssetMapping> assetMappingList;
        public List<trAssetMapping> AssetMappingList
        {
            get
            {
                return (List<trAssetMapping>)Session["AssetMappingList"];
            }
            set
            {
                Session["AssetMappingList"] = value;
            }
        }

        private List<vAssetMapping> vassetMappingList;
        public List<vAssetMapping> vAssetMappingList
        {
            get
            {
                return (List<vAssetMapping>)Session["vAssetMappingList"];
            }
            set
            {
                Session["vAssetMappingList"] = value;
            }
        }
        private List<vAssetMapping> assetByTimeList;
        public List<vAssetMapping> AssetByTimeList
        {
            get
            {
                return (List<vAssetMapping>)Session["AssetByTimeList"];
            }
            set
            {
                Session["AssetByTimeList"] = value;
            }
        }
        private List<trLegal_CourtAssign> petitionAssignList;
        public List<trLegal_CourtAssign> PetitionAssignList
        {
            get
            {
                return (List<trLegal_CourtAssign>)Session["PetitionAssignList"];
            }
            set
            {
                Session["PetitionAssignList"] = value;
            }
        }
        private List<trPursue> pursueList;
        public List<trPursue> PursueList
        {
            get
            {
                return (List<LegalEntities.trPursue>)Session["PursueList"];
            }
            set
            {
                Session["PursueList"] = value;
            }
        }
        private List<trSequester> sequesterList;
        public List<trSequester> SequesterList
        {
            get
            {
                return (List<trSequester>)Session["SequesterList"];
            }
            set
            {
                Session["SequesterList"] = value;
            }
        }

        private List<trAuction> auctionList;
        public List<trAuction> AuctionList
        {
            get
            {
                return (List<trAuction>)Session["AuctionList"];
            }
            set
            {
                Session["AuctionList"] = value;
            }
        }

        private List<trAuctionAssign> auctionAssignList;
        public List<trAuctionAssign> AuctionAssignList
        {
            get
            {
                return (List<trAuctionAssign>)Session["AuctionAssignList"];
            }
            set
            {
                Session["AuctionAssignList"] = value;
            }
        }

        private List<vAuctionAssign> vauctionAssignList;
        public List<vAuctionAssign> vAuctionAssignList
        {
            get
            {
                return (List<vAuctionAssign>)Session["vAuctionAssignList"];
            }
            set
            {
                Session["vAuctionAssignList"] = value;
            }
        }

        private List<vAuctionAssign> vauctionAssignByOrderList;
        public List<vAuctionAssign> vAuctionAssignByOrderList
        {
            get
            {
                return (List<vAuctionAssign>)Session["vAuctionAssignByOrderList"];
            }
            set
            {
                Session["vAuctionAssignByOrderList"] = value;
            }
        }

        private List<trAuctionAsset> auctionAssetList;
        public List<trAuctionAsset> AuctionAssetList
        {
            get
            {
                return (List<trAuctionAsset>)Session["AuctionAssetList"];
            }
            set
            {
                Session["AuctionAssetList"] = value;
            }
        }

        private List<vAuctionAsset> vauctionAssetList;
        public List<vAuctionAsset> vAuctionAssetList
        {
            get
            {
                return (List<vAuctionAsset>)Session["vAuctionAssetList"];
            }
            set
            {
                Session["vAuctionAssetList"] = value;
            }
        }

        private List<vAuctionAsset> vauctionAssetByOrderList;
        public List<vAuctionAsset> vAuctionAssetByOrderList
        {
            get
            {
                return (List<vAuctionAsset>)Session["vAuctionAssetByOrderList"];
            }
            set
            {
                Session["vAuctionAssetByOrderList"] = value;
            }
        }
        private List<trMortgage> mortgageList;
        public List<trMortgage> MortgageList
        {
            get
            {
                return (List<trMortgage>)Session["MortgageList"];
            }
            set
            {
                Session["MortgageList"] = value;
            }
        }

        private List<vMortgage> vmortgageList;
        public List<vMortgage> vMortgageList
        {
            get
            {
                return (List<vMortgage>)Session["vMortgageList"];
            }
            set
            {
                Session["vMortgageList"] = value;
            }
        }

        private List<trAverage> averageList;
        public List<trAverage> AverageList
        {
            get
            {
                return (List<trAverage>)Session["AverageList"];
            }
            set
            {
                Session["AverageList"] = value;
            }
        }

        private List<vAverage> vaverageList;
        public List<vAverage> vAverageList
        {
            get
            {
                return (List<vAverage>)Session["vAverageList"];
            }
            set
            {
                Session["vAverageList"] = value;
            }
        }

        private List<trRetain> retainList;
        public List<trRetain> RetainList
        {
            get
            {
                return (List<trRetain>)Session["RetainList"];
            }
            set
            {
                Session["RetainList"] = value;
            }
        }

        private List<vRetain> vretainList;
        public List<vRetain> vRetainList
        {
            get
            {
                return (List<vRetain>)Session["vRetainList"];
            }
            set
            {
                Session["vRetainList"] = value;
            }
        }

        private List<trRequestSequester> reqseqList;
        public List<trRequestSequester> ReqSeqList
        {
            get
            {
                return (List<trRequestSequester>)Session["ReqSeqList"];
            }
            set
            {
                Session["ReqSeqList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datetimepicker", "postback();", true);
            //if (!IsPostBack)
            //{
            //    BindControl();
            //    InitAssetData();
            //}
            //else
            //{

            //}
        }

        public void InitData()
        {
            // Document is initail from main page

            assetList = new List<trAsset>();
            AssetList = new List<trAsset>();
            assetMappingList = new List<trAssetMapping>();
            AssetMappingList = new List<trAssetMapping>();
            vassetMappingList = new List<vAssetMapping>();
            vAssetMappingList = new List<vAssetMapping>();
            assetByTimeList = new List<vAssetMapping>();
            AssetByTimeList = new List<vAssetMapping>();
            petitionAssignList = new List<trLegal_CourtAssign>();
            PetitionAssignList = new List<trLegal_CourtAssign>();
            pursueList = new List<trPursue>();
            PursueList = new List<trPursue>();
            sequesterList = new List<trSequester>();
            SequesterList = new List<trSequester>();
            auctionList = new List<trAuction>();
            AuctionList = new List<trAuction>();
            auctionAssignList = new List<trAuctionAssign>();
            AuctionAssignList = new List<trAuctionAssign>();
            vauctionAssignList = new List<vAuctionAssign>();
            vAuctionAssignList = new List<vAuctionAssign>();
            vauctionAssignByOrderList = new List<vAuctionAssign>();
            vAuctionAssignByOrderList = new List<vAuctionAssign>();
            auctionAssetList = new List<trAuctionAsset>();
            AuctionAssetList = new List<trAuctionAsset>();
            vauctionAssetList = new List<vAuctionAsset>();
            vAuctionAssetList = new List<vAuctionAsset>();
            vauctionAssetByOrderList = new List<vAuctionAsset>();
            vAuctionAssetByOrderList = new List<vAuctionAsset>();
            mortgageList = new List<trMortgage>();
            MortgageList = new List<trMortgage>();
            vmortgageList = new List<vMortgage>();
            vMortgageList = new List<vMortgage>();
            averageList = new List<trAverage>();
            AverageList = new List<trAverage>();
            vaverageList = new List<vAverage>();
            vAverageList = new List<vAverage>();
            retainList = new List<trRetain>();
            RetainList = new List<trRetain>();
            vretainList = new List<vRetain>();
            vRetainList = new List<vRetain>();
            reqseqList = new List<trRequestSequester>();
            ReqSeqList = new List<trRequestSequester>();
        }
        public void BindControl()
        {
            try
            {
                LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
                List<AttorneyType> Att;
                AttorneyTypeRep attrep = new AttorneyTypeRep(); Att = attrep.GetAttorneyTypeList();

                List<msAttorney> Lw = unitOfwork.MSAttorneyRep.GetLawyerByTypeID(1);

                ddlAttorneyType51.DataSource = Att;
                ddlAttorneyType51.DataValueField = "AttorneyTypeID";
                ddlAttorneyType51.DataTextField = "AttorneyTypeName";
                ddlAttorneyType51.DataBind();

                ddlAttorneyType52.DataSource = Att;
                ddlAttorneyType52.DataValueField = "AttorneyTypeID";
                ddlAttorneyType52.DataTextField = "AttorneyTypeName";
                ddlAttorneyType52.DataBind();

                ddlLawyer51.DataSource = Lw;
                ddlLawyer51.DataValueField = "LawyerID";
                ddlLawyer51.DataTextField = "LawyerName";
                ddlLawyer51.DataBind();

                ddlLawyer52.DataSource = Lw;
                ddlLawyer52.DataValueField = "LawyerID";
                ddlLawyer52.DataTextField = "LawyerName";
                ddlLawyer52.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SetUI(trLegal data, List<trDocument> docList, List<trDocumentMapping> docMapList, List<vDocumentMapping> vdocMapList)
        {
            try
            {
                LegalData = data; AttachDocList = docList; DocMappingList = docMapList; vAttachDocList = vdocMapList;

                BindControl();
                InitAssetData();
                //InitDocumentData(); // not do this, get doc data from main page - use together with tab7.

                GetAllPursue();
                GetAllSequester();
                GetAllPursueAsset();
                GetAllPursueDocument();
                GetAllSequesterAsset();
                GetAllAuction();
                GetAllMortgage();
                GetAllAverage();
                GetAllRetain();
                GetAllReqSeq();

                //vLawyerRep vlawyerrep = new vLawyerRep();
               // if (LegalData.T5_PLawyerID > 0) ddlAttorneyType51.SelectedValue = vlawyerrep.GetLawyerByID(Convert.ToInt32(LegalData.T5_PLawyerID)).AttorneyTypeID.ToString();
              //  if (LegalData.T5_SLawyerID > 0) ddlAttorneyType52.SelectedValue = vlawyerrep.GetLawyerByID(Convert.ToInt32(LegalData.T5_SLawyerID)).AttorneyTypeID.ToString();

                ddlLawyer51.SelectedValue = LegalData.T5_PLawyerID.ToString();
                txtSentoLawyer_p.Value = (Convert.ToDateTime(LegalData.T5_PSendtoLawyerDate)).ToString("dd/MM/yyyy");
                ddlLawyer52.SelectedValue = LegalData.T5_SLawyerID.ToString();
                txtSentoLawyer_s.Value = (Convert.ToDateTime(LegalData.T5_SSendtoLawyerDate)).ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ValidateTab()
        {
            return string.Empty;
        }
        public Tab5Data GetUI()
        {
            Tab5Data tab5 = new Tab5Data();
            try
            {
                if (!string.IsNullOrEmpty(ddlLawyer51.SelectedValue)) LegalData.T5_PLawyerID = Convert.ToInt32(ddlLawyer51.SelectedValue);
                if (!string.IsNullOrEmpty(txtSentoLawyer_s.Value)) LegalData.T5_PSendtoLawyerDate = Convert.ToDateTime(txtSentoLawyer_p.Value);
                if (!string.IsNullOrEmpty(ddlLawyer52.SelectedValue)) LegalData.T5_SLawyerID = Convert.ToInt32(ddlLawyer52.SelectedValue);
                if (!string.IsNullOrEmpty(txtSentoLawyer_s.Value)) LegalData.T5_SSendtoLawyerDate = Convert.ToDateTime(txtSentoLawyer_s.Value);
                LegalData.UpdateBy = userCode;
                LegalData.UpdateDateTime = DateTime.Now;

                tab5.LegalData = LegalData;
                tab5.DocumentList = AttachDocList;
                tab5.DocumentMappingList = DocMappingList;
                tab5.vDocumentList = vAttachDocList;
                tab5.AssetList = AssetList;
                tab5.AssetMappingList = AssetMappingList;
                tab5.vAssetMappingList = vAssetMappingList;
                tab5.PursueList = PursueList;
                tab5.SequesterList = SequesterList;
                tab5.MortgageList = MortgageList;
                tab5.AverageList = AverageList;
                tab5.AuctionList = AuctionList;
                tab5.AuctionAssignList = AuctionAssignList;
                tab5.AuctionAssetList = AuctionAssetList;
                tab5.RetainList = RetainList;
                tab5.RequestSequesterList = ReqSeqList;

            }
            catch (Exception ex)
            {

            }
            return tab5;
        }

        #region Control Event
        protected void ddlAttorneyType51_SelectedIndexChanged(object sender, EventArgs e)
        {
            LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
            int atnType = Convert.ToInt32(ddlAttorneyType51.SelectedValue);
            List<msAttorney> A = unitOfwork.MSAttorneyRep.GetLawyerByTypeID(atnType);
            if (A.Count > 0)
            {
                ddlLawyer51.Enabled = true;
                ddlLawyer51.DataSource = A;
                ddlLawyer51.DataBind();
                ddlLawyer51.DataValueField = "LawyerID";
                ddlLawyer51.DataTextField = "LawyerName";
                ddlLawyer51.SelectedIndex = 0;
            }
            else ddlLawyer51.Enabled = false;

        }

        protected void ddlAttorneyType52_SelectedIndexChanged(object sender, EventArgs e)
        {
            LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
            int atnType = Convert.ToInt32(ddlAttorneyType52.SelectedValue);
            List<msAttorney> A =unitOfwork.MSAttorneyRep.GetLawyerByTypeID(atnType);
            if (A.Count > 0)
            {
                ddlLawyer52.Enabled = true;
                ddlLawyer52.DataSource = A;
                ddlLawyer52.DataBind();
                ddlLawyer52.DataValueField = "LawyerID";
                ddlLawyer52.DataTextField = "LawyerName";
                ddlLawyer52.SelectedIndex = 0;
            }
            else ddlLawyer52.Enabled = false;

        }

        protected void btnAdd_morgage_Click(object sender, EventArgs e)
        {
            try
            {


                int morID = 1; int vmorID = 0; int index = 0;
                if (MortgageList.Count > 0)
                {
                    morID = MortgageList.Max(M => M.MortgageID) + 1;
                }
                if (vMortgageList.Count > 0)
                {
                    vmorID = vMortgageList.Max(M => M.MortgageID);
                }
                if (morID > vmorID)
                {
                    CommandName = "Add";

                    vMortgage mor = new vMortgage();
                    mor.MortgageID = morID;
                    vMortgageList.Add(mor);
                    GetUpdateMortgage();

                    if (gvMortgage.Rows.Count > 0) index = gvMortgage.Rows.Count - 1;
                    gvMortgage.SetEditRow(index);
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                //if (vAssetMappingList.Count == 0) ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "alert-no-data", "assetalert();", true);
                if (vAssetMappingList.Count == 0) throw new Exception("ไม่พบทรัพย์สิน");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        protected void btnadd_average_Click(object sender, EventArgs e)
        {
            try
            {
                //if (string.IsNullOrEmpty(CommandName) || !CommandName.Equals("Add"))
                int avgID = 1; int vavgID = 0;
                if (AverageList.Count > 0)
                {
                    avgID = AverageList.Max(M => M.AverageID) + 1;
                }
                if (vAverageList.Count > 0)
                {
                    vavgID = vAverageList.Max(M => M.AverageID);
                }
                if (avgID > vavgID)
                {

                    CommandName = "Add";

                    vAverage avg = new vAverage();
                    avg.AverageID = avgID;
                    vAverageList.Add(avg);
                    GetUpdateAverage();

                    int index = 1;
                    if (gvAverage.Rows.Count > 0) index = gvAverage.Rows.Count - 1;
                    gvAverage.SetEditRow(index);
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                //if (vAssetMappingList.Count == 0) ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "alert-no-data", "assetalert();", true);
                if (vAssetMappingList.Count == 0) throw new Exception("ไม่พบทรัพย์สิน");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }

        }

        protected void gvAdd_Retain_Click(object sender, EventArgs e)
        {
            try
            {
                int retID = 1; int vretID = 0;
                if (RetainList.Count > 0)
                {
                    retID = RetainList.Max(M => M.RetainID) + 1;
                }
                if (vRetainList.Count > 0)
                {
                    vretID = vRetainList.Max(M => M.RetainID);
                }
                if (retID > vretID)
                {
                    CommandName = "Add";

                    vRetain ret = new vRetain();
                    ret.RetainID = retID;
                    vRetainList.Add(ret);
                    GetUpdateRetain();

                    int index = 1;
                    if (gvRetain.Rows.Count > 0) index = gvRetain.Rows.Count - 1;
                    gvRetain.SetEditRow(index);
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                //if (vAssetMappingList.Count == 0) ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "alert-no-data", "assetalert();", true);

                if (vAssetMappingList.Count == 0) throw new Exception("ไม่พบทรัพย์สิน");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }

        }

        protected void gvadd_request_Click(object sender, EventArgs e)
        {
            int reqseqID = 1; int vreqseqID = 0;
            if (ReqSeqList.Count > 0)
            {
                reqseqID = ReqSeqList.Max(M => M.ReqSeqID) + 1;
            }
            //if (vRetainList.Count > 0)
            //{
            //    vreqseqID = vReqSeqList.Max(M => M.RetainID);
            //}
            if (reqseqID > vreqseqID)
            {
                CommandName = "Add";
                // new reqseqID in gridview

                trRequestSequester reqseq = new trRequestSequester();
                reqseq.ReqSeqID = reqseqID;
                ReqSeqList.Add(reqseq);
                GetUpdateReqSeq();
                // get index and set to edit mode
                int index = 1;
                if (gvReqSeq.Rows.Count > 0) index = gvReqSeq.Rows.Count - 1;
                gvReqSeq.SetEditRow(index);
                HiddenField hidReqseqID = gvReqSeq.Rows[index].FindControl("hidReqseqID") as HiddenField;
                hidReqseqID.Value = reqseqID.ToString();
                DropDownList ddlReqseqNo = gvReqSeq.Rows[index].FindControl("ddlReqseqNo") as DropDownList;
                ddlReqseqNo.SelectedIndex = 0;
            }
            UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
        }
        #endregion

        #region Update GridView by function
        #region get and binding for init
        public void InitAssetData()
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                assetList =unitOfwork.TrAssetRep.GetAllAsset(LegalData.LegalNo);
                assetMappingList = unitOfwork.TrAssetMappingRep.GetAllAssetMapping(LegalData.LegalNo);
                vassetMappingList=unitOfwork.VAssetMappingRep.GetAllAsset(LegalData.LegalNo);
            }
           

            AssetList = assetList;
            AssetMappingList = assetMappingList;
            vAssetMappingList = vassetMappingList;
        }
        public void GetAllPursue()
        {
            trPursueRep rep = new trPursueRep();
            pursueList = rep.GetAllPursue(LegalData.LegalNo);
            PursueList = pursueList;
            gvPursue.DataSource = PursueList;
            gvPursue.DataBind();
        }

        public void GetAllSequester()
        {
            trSequesterRep rep = new trSequesterRep();
            SequesterList = rep.GetAllSequester(LegalData.LegalNo);
            gvSequester.DataSource = SequesterList;
            gvSequester.DataBind();
        }

        public void GetAllPursueAsset()
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                vAssetMappingList = unitOfwork.VAssetMappingRep.GetAllPursueAsset(LegalData.LegalNo);
                gvAssetView.DataSource = vAssetMappingList;
                gvAssetView.DataBind();
            }
        }

        public void GetAllPursueDocument()
        {

            try
            {
                vDocumentMappingRep rep = new vDocumentMappingRep();
                gvPursueAttach.DataSource = rep.GetAllDocument(LegalData.LegalNo).FindAll(d => d.Flag.Equals("P"));
                gvPursueAttach.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetAllSequesterAsset()
        {
            LegalUnitOfWork unitofwork = new LegalUnitOfWork();

            vAssetMappingList = unitofwork.VAssetMappingRep.GetAllSequesterAsset(LegalData.LegalNo);
            gvSequesterView.DataSource = vAssetMappingList;
            gvSequesterView.DataBind();
        }

        public void GetAllAuction()
        {
            trAuctionRep auc_rep = new trAuctionRep();
            auctionList = auc_rep.GetAllAuction(LegalData.LegalNo);
            AuctionList = auctionList.OrderBy(list => list.AuctionID).ToList();

            trAuctionAssignRep aucassign_rep = new trAuctionAssignRep();
            auctionAssignList = aucassign_rep.GetAllAuctionAssign(LegalData.LegalNo);
            AuctionAssignList = auctionAssignList.OrderBy(list => list.AuctionID).ToList();

            trAuctionAssetRep aucasset_rep = new trAuctionAssetRep();
            auctionAssetList = aucasset_rep.GetAllAuctionAsset(LegalData.LegalNo);
            AuctionAssetList = auctionAssetList.OrderBy(list => list.AuctionID).ToList();

            vAuctionAssignRep vaucassign_rep = new vAuctionAssignRep();
            vauctionAssignList = vaucassign_rep.GetAllAuctionAssign(LegalData.LegalNo);
            vAuctionAssignList = vauctionAssignList.OrderBy(list => list.AuctionOrder).ToList();
            gvAuction.DataSource = vAuctionAssignList;
            gvAuction.DataBind();
            gvAuctionAssign.DataSource = vAuctionAssignList;
            gvAuctionAssign.DataBind();

            vAuctionAssetRep vaucasset_rep = new vAuctionAssetRep();
            vauctionAssetList = vaucasset_rep.GetAllAuctionAsset(LegalData.LegalNo);
            vAuctionAssetList = vauctionAssetList.OrderBy(list => list.AuctionOrder).ToList();
            gvAuctionAsset.DataSource = vAuctionAssetList;
            gvAuctionAsset.DataBind();
        }

        public void GetAllMortgage()
        {
            trMortgageRep rep = new trMortgageRep();
            mortgageList = rep.GetAllMortgage(LegalData.LegalNo);
            MortgageList = mortgageList;

            vMortgageRep morrep = new vMortgageRep();
            vmortgageList = morrep.GetAllMortgage(LegalData.LegalNo);
            vMortgageList = vmortgageList;
            gvMortgage.DataSource = vMortgageList;
            gvMortgage.DataBind();
        }

        public void GetAllAverage()
        {
            trAverageRep average_rep = new trAverageRep();
            averageList = average_rep.GetAllAverage(LegalData.LegalNo);
            AverageList = averageList;

            vAverageRep vaverage_rep = new vAverageRep();
            vaverageList = vaverage_rep.GetAllAverage(LegalData.LegalNo);
            vAverageList = vaverageList;
            gvAverage.DataSource = vAverageList;
            gvAverage.DataBind();
        }

        public void GetAllRetain()
        {
            trRetainRep retrep = new trRetainRep();
            retainList = retrep.GetAllRetain(LegalData.LegalNo);
            RetainList = retainList;

            vRetainRep rep = new vRetainRep();
            vretainList = rep.GetAllRetain(LegalData.LegalNo);
            vRetainList = vretainList;
            gvRetain.DataSource = vRetainList;
            gvRetain.DataBind();

        }

        public void GetAllReqSeq()
        {
            trRequestSequesterRep rep = new trRequestSequesterRep();
            reqseqList = rep.GetAllReqSeq(LegalData.LegalNo);
            ReqSeqList = reqseqList;
            gvReqSeq.DataSource = ReqSeqList;
            gvReqSeq.DataBind();
        }

        #endregion

        #region update UpdatePanel
        public void GetUpdateDocMapping(string flag, int order)
        {
            // flag => P:Pursue(สีบทรัพย์) A:Auction(ยึก/อายัดทรัพย์) L:Legal(เอกสารแนบ Tab7)
            switch (flag)
            {
                case "P":
                    DocumentByTimeList = vAttachDocList.FindAll(list => (list.Flag.Equals(flag)) && (list.PursueTime == order));
                    gvPursueAttach.DataSource = DocumentByTimeList;
                    gvPursueAttach.DataBind();
                    break;
                case "A":
                    DocumentByTimeList = vAttachDocList.FindAll(list => (list.Flag.Equals(flag)) && (list.AuctionOrder == order));
                    gvAuctionDoc.DataSource = DocumentByTimeList;
                    gvAuctionDoc.DataBind();
                    break;
                //// do a below case as tab7
                //case "L":
                //    documentByTimeList = new List<vDocumentMapping>();
                //    vattachDocList = vAttachDocList.FindAll(list => (list.Flag.Equals(flag)));
                //    foreach (vDocumentMapping item in vattachDocList)
                //    {
                //        List<vDocumentMapping> doclist = documentByTimeList.FindAll(list => list.DocTypeID == item.DocTypeID);
                //        if (doclist.Count == 0)
                //        {
                //            documentByTimeList.Add(item);
                //        }
                //    }
                //    DocumentByTimeList = documentByTimeList;
                //    gvAttach.DataSource = DocumentByTimeList;
                //    gvAttach.DataBind();
                //    break;
            }
        }
        public void GetUpdatePursue()
        {
            gvPursue.DataSource = PursueList.OrderBy(list => list.PursueTime).ToList();
            gvPursue.DataBind();
        }

        public void GetUpdateSequester()
        {
            gvSequester.DataSource = SequesterList.OrderBy(list => list.SeqTime).ToList();
            gvSequester.DataBind();
        }

        public void GetUpdateAuction()
        {
            List<vAuctionAssign> auclist = new List<vAuctionAssign>();
            foreach (trAuction item in AuctionList)
            {
                vAuctionAssign auc = new vAuctionAssign();
                int order = item.AuctionOrder;
                vauctionAssignList = vAuctionAssignList.FindAll(A => A.AuctionOrder == order).OrderBy(A => A.ActionTime).ToList();
                if (vauctionAssignList.Count > 0)
                {
                    foreach (vAuctionAssign itm in vauctionAssignList)
                    {
                        auc = itm;
                        auclist.Add(auc);
                    }
                }
                else
                {
                    auc.LegalNo = item.LegalNo;
                    auc.AuctionID = item.AuctionID;
                    auc.AuctionOrder = item.AuctionOrder;
                    auclist.Add(auc);
                }

            }
            vauctionAssignList = auclist;
            vAuctionAssignList = vauctionAssignList;
            gvAuction.DataSource = vAuctionAssignList.OrderBy(A => A.AuctionOrder).ThenBy(A => A.ActionTime).ToList();
            gvAuction.DataBind();
        }

        public void GetUpdateAssetMapping(int pursueTime)
        {
            //AssetByTimeList = vAssetMappingList.FindAll(list => (list.Flag.Contains("P")) && (list.PursueTime == pursueTime));
            AssetByTimeList = vAssetMappingList.FindAll(list => list.PursueTime == pursueTime);
            gvAssetView.DataSource = AssetByTimeList.OrderBy(list => list.PursueTime).ToList();
            gvAssetView.DataBind();
        }

        public void GetUpdateSelectAssetByTime(int sequesterTime)
        {
            //AssetByTimeList = vAssetMappingList.FindAll(list => (list.Flag.Contains("P")) && (list.PursueTime == pursueTime));
            AssetByTimeList = vAssetMappingList.FindAll(list => list.SeqTime == sequesterTime);
            gvSequesterView.DataSource = AssetByTimeList;
            gvSequesterView.DataBind();
        }

        public void GetUpdateSelectAssetMapping()
        {
            //AssetByTimeList = vAssetMappingList.FindAll(list => (list.Flag.Contains("P")) && (list.PursueTime == pursueTime));
            AssetByTimeList = vAssetMappingList.FindAll(list => list.SequesterID != 0);
            gvSequesterView.DataSource = AssetByTimeList;
            gvSequesterView.DataBind();
        }

        public void GetUpdateSelectAsset()
        {
            gvSelectAssetSeq.DataSource = vAssetMappingList;
            gvSelectAssetSeq.DataBind();
        }

        public void GetUpdateAuctionAssign()
        {
            gvAuctionAssign.DataSource = vAuctionAssignList;
            gvAuctionAssign.DataBind();
        }

        public void GetUpdateAuctionAssignByOrder(int order)
        {
            vauctionAssignByOrderList = vAuctionAssignByOrderList;
            vauctionAssignByOrderList = vAuctionAssignList.FindAll(A => A.AuctionOrder == order);
            vAuctionAssignByOrderList = vauctionAssignByOrderList;
            gvAuctionAssign.DataSource = vAuctionAssignByOrderList;
            gvAuctionAssign.DataBind();
        }

        public void GetUpdateAuctionDoc()
        {
            gvAuctionDoc.DataSource = vAttachDocList.FindAll(A => A.LegalNo.Equals(LegalData.LegalNo));
            gvAuctionDoc.DataBind();
        }

        public void GetUpdateAuctionDocByOrder(int order)
        {
            documentByTimeList = DocumentByTimeList;
            documentByTimeList = vAttachDocList.FindAll(A => A.AuctionOrder == order);
            DocumentByTimeList = documentByTimeList;
            gvAuctionDoc.DataSource = DocumentByTimeList;
            gvAuctionDoc.DataBind();
        }

        public void GetUpdateAuctionAsset()
        {
            gvAuctionAsset.DataSource = vAuctionAssetList;
            gvAuctionAsset.DataBind();
        }

        public void GetUpdateAuctionAssetByOrder(int order)
        {
            vauctionAssetByOrderList = vAuctionAssetByOrderList;
            vauctionAssetByOrderList = vAuctionAssetList.FindAll(A => A.AuctionOrder == order);
            vAuctionAssetByOrderList = vauctionAssetByOrderList;
            gvAuctionAsset.DataSource = vAuctionAssetByOrderList;
            gvAuctionAsset.DataBind();
        }

        public void GetUpdateMortgage()
        {
            gvMortgage.DataSource = vMortgageList.OrderBy(M => M.MortgageID).ToList();
            gvMortgage.DataBind();
        }

        public void GetUpdateAverage()
        {
            gvAverage.DataSource = vAverageList.OrderBy(A => A.AverageID).ToList();
            gvAverage.DataBind();
        }

        public void GetUpdateRetain()
        {
            gvRetain.DataSource = vRetainList.OrderBy(R => R.RetainID).ToList();
            gvRetain.DataBind();
        }

        public void GetUpdateReqSeq()
        {
            gvReqSeq.DataSource = ReqSeqList.OrderBy(R => R.ReqSeqID).ToList();
            //gvReqSeq.DataSource = vReqSeqList.OrderBy(R => R.ReqSeqID).ToList();
            gvReqSeq.DataBind();
        }

        public void GetUpdateReqseqAndOrder()
        {
            gvReqSeq.DataSource = ReqSeqList.OrderBy(R => R.ReqSeqNo).ToList();
            gvReqSeq.DataBind();
        }
        #endregion
        #endregion

        #region GridView Event
        #region gvPursue Event
        protected void gvPursue_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetUpdatePursue();
        }

        protected void gvPursue_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";

                string sdate = e.Row.Cells[2].Text;
                e.Row.Cells[2].Text = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy");
            }
        }

        protected void gvPursue_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPursue.PageIndex = e.NewPageIndex;
            gvPursue.DataSource = PetitionAssignList;
            gvPursue.DataBind();
        }

        protected void gvPursue_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string pid = (gvPursue.Rows[index].FindControl("hidPID") as HiddenField).Value.Trim();
                int ipid = Convert.ToInt32(pid);
                string ptime = (gvPursue.Rows[index].FindControl("lbPDate") as Label).Text.Trim();
                int iPTime = Convert.ToInt32(ptime);
                string flag = "P";
                string pdate = gvPursue.Rows[index].Cells[2].Text.Trim();
                string foundAsset = "0"; bool bfound = false;
                if (gvPursue.Rows[index].Cells[1].Text.Equals("True"))
                {
                    foundAsset = "1";
                    bfound = true;
                }

                // bind to controller
                hidid_pursuemodal.Value = pid;
                txtPTime.Value = ptime;
                txtdate_pursuemodal.Value = Convert.ToDateTime(pdate).ToString("dd/MM/yyyy"); ;
                ddlFoundAsset_pursuemodal.SelectedValue = foundAsset;

                // set UI
                if (bfound) hiddiv_pursuemodal.Attributes.Add("style", "display:block");
                else hiddiv_pursuemodal.Attributes.Add("style", "display:none");
                txtPTime.Disabled = true;
                vAttachDocList = vAttachDocList;
                GetUpdateAssetMapping(iPTime);
                GetUpdateDocMapping(flag, iPTime);
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "pursueModal", "modalshowpursue();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int pursueID = Convert.ToInt32((gvPursue.Rows[index].FindControl("hidPID") as HiddenField).Value.Trim());

                // Add delete files to LegalSession
                if (LegalSession.filesdelete == null) LegalSession.filesdelete = new List<filedelete>();
                List<vDocumentMapping> deleteList = vAttachDocList.FindAll(x => x.Flag.Equals("P") && x.FID == pursueID);
                foreach (var item in deleteList)
                {
                    string namedelete = item.DocPath;
                    LegalSystem.Common.filedelete file = new filedelete();
                    file.DocID = item.DocID;
                    file.FileName = namedelete;
                    LegalSession.filesdelete.Add(file);
                }

                assetMappingList = AssetMappingList.FindAll(m => m.PursueID == pursueID);
                foreach (trAssetMapping itm in assetMappingList)
                {
                    AssetList.RemoveAll(a => a.AssetID == itm.AssetID);
                }
                AssetMappingList.RemoveAll(m => m.PursueID == pursueID);
                vAssetMappingList.RemoveAll(m => m.PursueID == pursueID);
                PursueList.RemoveAll(p => p.PursueID == pursueID);
                GetUpdatePursue();
                UpdatePanel1.Update();
            }
        }
        #endregion

        #region gvSequester Event
        protected void gvSequester_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //GetUpdateSequester();
        }

        protected void gvSequester_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";

                string sdate = e.Row.Cells[1].Text;
                if (!string.IsNullOrEmpty(sdate)) e.Row.Cells[1].Text = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy");
                string sdate2 = e.Row.Cells[2].Text;
                if (!string.IsNullOrEmpty(sdate2)) e.Row.Cells[2].Text = Convert.ToDateTime(sdate2).ToString("dd/MM/yyyy");
            }
        }

        protected void gvSequester_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSequester.PageIndex = e.NewPageIndex;
            gvSequester.DataSource = SequesterList;
            gvSequester.DataBind();
        }

        protected void gvSequester_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string sseqid = (gvSequester.Rows[index].FindControl("hidSeqID") as HiddenField).Value.Trim();
                int seqid = Convert.ToInt32(sseqid);
                string seqtime = SequesterList.Find(seq => seq.SequesterID == seqid).SeqTime.ToString();
                string date1 = SequesterList.Find(seq => seq.SequesterID == seqid).GotDeedDate.ToString();
                string proposition = SequesterList.Find(seq => seq.SequesterID == seqid).Proposition.ToString();
                string date2 = SequesterList.Find(seq => seq.SequesterID == seqid).SendDeedDate.ToString();
                string executeID = SequesterList.Find(seq => seq.SequesterID == seqid).ExecutionID.ToString();
                string debt = SequesterList.Find(seq => seq.SequesterID == seqid).Debt.ToString();
                string courtzoneID = SequesterList.Find(seq => seq.SequesterID == seqid).CourtZoneID.ToString();
                string soldtypeID = SequesterList.Find(seq => seq.SequesterID == seqid).SoldType.ToString();
                string date3 = SequesterList.Find(seq => seq.SequesterID == seqid).SeqDate.ToString();
                string seqfee = SequesterList.Find(seq => seq.SequesterID == seqid).SeqFee.ToString();
                string date4 = SequesterList.Find(seq => seq.SequesterID == seqid).Seq2Date.ToString();
                string seq2fee = SequesterList.Find(seq => seq.SequesterID == seqid).Seq2Fee.ToString();
                string remark = SequesterList.Find(seq => seq.SequesterID == seqid).Comment;

                // bind to controller
                if (!string.IsNullOrEmpty(executeID)) ddlExecution.SelectedValue = executeID;
                if (!string.IsNullOrEmpty(courtzoneID)) ddlCourtZone_seqmodal.SelectedValue = courtzoneID;
                if (!string.IsNullOrEmpty(soldtypeID)) ddlSoldType.SelectedValue = soldtypeID;
                if (!string.IsNullOrEmpty(date1)) txtdate1_seqmodal.Value = Convert.ToDateTime(date1).ToString("dd/MM/yyyy");
                if (!string.IsNullOrEmpty(date2)) txtdate2_seqmodal.Value = Convert.ToDateTime(date2).ToString("dd/MM/yyyy");
                if (!string.IsNullOrEmpty(date3)) txtseq2date.Value = Convert.ToDateTime(date3).ToString("dd/MM/yyyy");
                if (!string.IsNullOrEmpty(date4)) txtdate4_seqmodal.Value = Convert.ToDateTime(date4).ToString("dd/MM/yyyy");
                hidseqID_seqmodal.Value = sseqid;
                txtSTime_seqmodal.Value = seqtime;
                txtProposition.Value = proposition;
                txtDebt_sqemodal.Value = Convert.ToDecimal(debt).ToString("F2");
                txtseqfee.Value = Convert.ToDecimal(seqfee).ToString("F2");
                seq2fee_seqmodal.Value = Convert.ToDecimal(seq2fee).ToString("F2");
                txtRemark_seqmodal.Value = remark;

                GetUpdateSelectAssetByTime(Convert.ToInt32(seqtime));
                UpdatePanel4.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "sequesterModal", "modalshowsequester();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int seqID = Convert.ToInt32((gvSequester.Rows[index].FindControl("hidSeqID") as HiddenField).Value.Trim());

                //===========vAssetMapping===========
                List<vAssetMapping> vlist = new List<vAssetMapping>();
                vlist = vAssetMappingList.FindAll(seq => seq.SequesterID == seqID);
                vassetMappingList = vAssetMappingList;
                vassetMappingList.RemoveAll(seq => seq.SequesterID == seqID);
                foreach (vAssetMapping view in vlist)
                {
                    view.SequesterID = 0;
                    view.SeqTime = 0;
                    view.LegalNo_seq = string.Empty;
                    vassetMappingList.Add(view);
                }
                vAssetMappingList = vassetMappingList;
                //===========trAssetMapping===========
                List<trAssetMapping> mlist = new List<trAssetMapping>();
                assetMappingList = AssetMappingList;
                mlist = assetMappingList.FindAll(seq => seq.SequesterID == seqID);
                assetMappingList = AssetMappingList;
                assetMappingList.RemoveAll(seq => seq.SequesterID == seqID);
                foreach (trAssetMapping map in mlist)
                {
                    map.SequesterID = 0;
                    assetMappingList.Add(map);
                }
                AssetMappingList = assetMappingList;
                //===========trSequester===========
                sequesterList = SequesterList;
                SequesterList.RemoveAll(seq => seq.SequesterID == seqID);
                SequesterList = sequesterList;

                GetUpdateSequester();
                UpdatePanel1.Update();
            }
        }
        #endregion

        #region gvAssetView Event
        protected void gvAssetView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //int index = e.RowIndex;
            //int assID = Convert.ToInt32((gvAssetView.Rows[index].FindControl("hidAssetID") as HiddenField).Value.Trim());
            //int ptime = Convert.ToInt32(vAssetMappingList.Find(list => list.AssetID == assID).PursueTime);
            //GetUpdateAssetMapping(ptime);
        }

        protected void gvAssetView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";


            }
        }

        protected void gvAssetView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAssetView.PageIndex = e.NewPageIndex;
            gvAssetView.DataSource = AssetByTimeList;
            gvAssetView.DataBind();
        }

        protected void gvAssetView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                SubCommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string assID = (gvAssetView.Rows[index].FindControl("hidAssetID") as HiddenField).Value.Trim();
                int iassID = Convert.ToInt32(assID);
                string assType = AssetList.Find(list => list.AssetID == iassID).AssetTypeID.ToString();
                string address = AssetList.Find(list => list.AssetID == iassID).Address;
                string district = AssetList.Find(list => list.AssetID == iassID).District;
                string province = AssetList.Find(list => list.AssetID == iassID).Province;
                string zipcode = AssetList.Find(list => list.AssetID == iassID).ZipCode;

                // bind to controller
                hidAssetID.Value = assID;
                ddlAssetType.SelectedValue = assType;
                txtAddress_assetmodal.Value = address;
                txtDistrinct_assetmodal.Value = district;
                txtProvince_assetmodal.Value = province;
                txtZipcode_assetmodal.Value = zipcode;

                UpdatePanel3.Update(); //asset modal
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "assetModal", "modalshowasset();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int assID = Convert.ToInt32((gvAssetView.Rows[index].FindControl("hidAssetID") as HiddenField).Value.Trim());
                int ptime = Convert.ToInt32(vAssetMappingList.Find(list => list.AssetID == assID).PursueTime);

                //===========trAssetList===========
                assetList = AssetList;
                assetList.RemoveAll(list => list.AssetID == assID);
                AssetList = assetList;
                //===========trAssetMapping===========
                assetMappingList = AssetMappingList;
                assetMappingList.RemoveAll(list => list.AssetID == assID);
                AssetMappingList = assetMappingList;
                //===========vAssetMapping===========
                vassetMappingList = vAssetMappingList;
                vassetMappingList.RemoveAll(list => list.AssetID == assID);
                vAssetMappingList = vassetMappingList;

                GetUpdateAssetMapping(ptime);
                UpdatePanel2.Update();
            }
        }
        #endregion

        #region gvPursueAttach Event
        protected void gvPursueAttach_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gvPursueAttach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvPursueAttach_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPursueAttach.PageIndex = e.NewPageIndex;
            gvPursueAttach.DataSource = vAttachDocList;
            gvPursueAttach.DataBind();
        }

        protected void gvPursueAttach_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewCommand")
            {
                #region OLD VERSION : NOT USE
                //// get data from grid
                //GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //int index = rindex.RowIndex;
                //string sDocID = (gvPursueAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim();
                //int iDocID = Convert.ToInt32(sDocID);
                //string docPath = AttachDocList.Find(list => list.DocID == iDocID).DocPath;

                //// Add delete files to LegalSession
                //if (LegalSession.filesdelete == null) LegalSession.filesdelete = new List<filedelete>();
                //string namedelete = AttachDocList.First(x => x.DocID == iDocID).DocPath;
                //LegalSystem.Common.filedelete file = new filedelete();
                //file.DocID = iDocID;
                //file.FileName = namedelete;
                //LegalSession.filesdelete.Add(file);

                //// set UI
                //iPDF.Src = docPath;
                //ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "pdfModal", "modalshowpdf();", true);
                #endregion
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int docid = Convert.ToInt32((gvPursueAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());
                int ptime = Convert.ToInt32(vAttachDocList.Find(list => list.DocID == docid).PursueTime);
                string flag = "P";
                //===========trDocument===========
                attachDocList = AttachDocList;
                attachDocList.RemoveAll(list => list.DocID == docid);
                AttachDocList = attachDocList;
                //===========trDocumentMapping===========
                docMappingList = DocMappingList;
                docMappingList.RemoveAll(list => list.DocID == docid);
                DocMappingList = docMappingList;
                //===========vDocumentMapping===========
                vattachDocList = vAttachDocList;
                vattachDocList.RemoveAll(list => list.DocID == docid);
                vAttachDocList = vattachDocList;

                GetUpdateDocMapping(flag, ptime);
                UpdatePanel2.Update();
            }
        }
        #endregion

        #region gvSelectAssetSeq Event
        protected void gvSelectAssetSeq_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //CheckBox chkBox =  e.Row.FindControl("hidSeqID") as CheckBox;
                HiddenField hidSeqID = e.Row.FindControl("hidSeqID") as HiddenField;
                // enaabled
                // map AssetID with other SeqID : hidSeqID != hidseqID_seqmodal + hidSeqID no value
                if (!hidseqID_seqmodal.Value.Equals(hidSeqID.Value) && !hidSeqID.Value.Equals("0"))
                {
                    CheckBox chkBox = e.Row.FindControl("CheckBox1") as CheckBox;
                    chkBox.Checked = false;
                    chkBox.Enabled = false;
                }
                // AssetID not yet map : hidSeqID has not value
                else if (hidSeqID.Value.Equals("0"))
                {
                    CheckBox chkBox = e.Row.FindControl("CheckBox1") as CheckBox;
                    chkBox.Checked = false;
                    chkBox.Enabled = true;
                }
                // edit
                // AssetID map with this SeqID : hidSeqID == hidseqID_seqmodal
                else if (hidseqID_seqmodal.Value.Equals(hidSeqID.Value))
                {
                    CheckBox chkBox = e.Row.FindControl("CheckBox1") as CheckBox;
                    chkBox.Checked = true;
                    chkBox.Enabled = true;
                }
            }
        }

        protected void gvSelectAssetSeq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSelectAssetSeq.PageIndex = e.NewPageIndex;
            gvSelectAssetSeq.DataSource = AssetByTimeList;
            gvSelectAssetSeq.DataBind();
        }

        protected void gvSelectAssetSeq_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                SubCommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string assID = (gvAssetView.Rows[index].FindControl("hidAssetID") as HiddenField).Value.Trim();
                int iassID = Convert.ToInt32(assID);
                string assType = AssetList.Find(list => list.AssetID == iassID).AssetTypeID.ToString();
                string address = AssetList.Find(list => list.AssetID == iassID).Address;
                string district = AssetList.Find(list => list.AssetID == iassID).District;
                string province = AssetList.Find(list => list.AssetID == iassID).Province;
                string zipcode = AssetList.Find(list => list.AssetID == iassID).ZipCode;

                // bind to controller
                hidAssetID.Value = assID;
                ddlAssetType.SelectedValue = assType;
                txtAddress_assetmodal.Value = address;
                txtDistrinct_assetmodal.Value = district;
                txtProvince_assetmodal.Value = province;
                txtZipcode_assetmodal.Value = zipcode;

                UpdatePanel3.Update(); //asset modal
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "assetModal", "modalshowasset();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int assID = Convert.ToInt32((gvAssetView.Rows[index].FindControl("hidAssetID") as HiddenField).Value.Trim());
                int ptime = Convert.ToInt32(vAssetMappingList.Find(list => list.AssetID == assID).PursueTime);

                //===========trAssetList===========
                assetList = AssetList;
                assetList.RemoveAll(list => list.AssetID == assID);
                AssetList = assetList;
                //===========trAssetMapping===========
                assetMappingList = AssetMappingList;
                assetMappingList.RemoveAll(list => list.AssetID == assID);
                AssetMappingList = assetMappingList;
                //===========vAssetMapping===========
                vassetMappingList = vAssetMappingList;
                vassetMappingList.RemoveAll(list => list.AssetID == assID);
                vAssetMappingList = vassetMappingList;

                GetUpdateAssetMapping(ptime);
                UpdatePanel2.Update();
            }
        }
        #endregion

        #region gvSequesterView Event
        protected void gvSequesterView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                //btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvSequesterView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSequesterView.PageIndex = e.NewPageIndex;
            gvSequesterView.DataSource = AssetByTimeList;
            gvSequesterView.DataBind();
        }
        #endregion

        #region gvAuction Event
        protected void gvAuction_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gvAuction_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnEdit = e.Row.FindControl("btnEdit") as Button;
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                HiddenField aucid = e.Row.FindControl("hidaucID") as HiddenField;
                Label aucorder = e.Row.FindControl("lbAuctionOrder") as Label;
                string auctionDate = e.Row.Cells[2].Text;


                int minTime = 0;
                int auctime = Convert.ToInt32(e.Row.Cells[1].Text);
                int iaucid = Convert.ToInt32(aucid.Value);
                int iaucorder = Convert.ToInt32(aucorder.Text);
                vauctionAssignList = vAuctionAssignList.FindAll(A => A.LegalNo.Equals(LegalData.LegalNo) && A.AuctionOrder == iaucorder);
                minTime = vauctionAssignList.Min(A => A.ActionTime);
                e.Row.Cells[2].Text = Convert.ToDateTime(auctionDate).ToString("dd/MM/yyyy");

                // when delete
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";

                // hide button : 
                if (minTime == auctime)
                {
                    aucorder.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {
                    aucorder.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                }
            }
        }

        protected void gvAuction_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAuction.PageIndex = e.NewPageIndex;
            gvAuction.DataSource = vAuctionAssignList;
            gvAuction.DataBind();
        }

        protected void gvAuction_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                CommandName = "Edit";

                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string aucid = (gvAuction.Rows[index].FindControl("hidaucID") as HiddenField).Value.Trim();
                string aucorder = (gvAuction.Rows[index].FindControl("lbAuctionOrder") as Label).Text.Trim();
                int iaucorder = Convert.ToInt32(aucorder);

                hidAucID_auctionmodal.Value = aucid;
                txtOrder_auctionmodal.Value = aucorder;
                txtOrder_auctionmodal.Disabled = true;
                GetUpdateAuctionAssignByOrder(iaucorder); // bind gvAuctionAssign
                GetUpdateAuctionDocByOrder(iaucorder); // bind gvAuctionDoc
                GetUpdateAuctionAssetByOrder(iaucorder); // bind gvAuctionAsset

                UpdatePanel7.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "auctionModal", "modalshowauction();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int aucid = Convert.ToInt32((gvAuction.Rows[index].FindControl("hidaucID") as HiddenField).Value.Trim());

                //=============== trAuction ===============
                auctionList = AuctionList;
                auctionList.RemoveAll(auc => auc.AuctionID == aucid);
                AuctionList = auctionList;
                //=============== trAuctionAssign ===============
                auctionAssignList = AuctionAssignList;
                auctionAssignList.RemoveAll(auc => auc.AuctionID == aucid);
                AuctionAssignList = auctionAssignList;
                //=============== trAuctionAsset ===============
                auctionAssetList = AuctionAssetList;
                auctionAssetList.RemoveAll(auc => auc.AuctionID == aucid);
                AuctionAssetList = auctionAssetList;
                //=============== vAuctionAssign ===============
                vauctionAssignList = vAuctionAssignList;
                vauctionAssignList.RemoveAll(auc => auc.AuctionID == aucid);
                vAuctionAssignList = vauctionAssignList;
                //=============== vAuctionAsset ===============
                vauctionAssetList = vAuctionAssetList;
                vauctionAssetList.RemoveAll(auc => auc.AuctionID == aucid);
                vAuctionAssetList = vauctionAssetList;

                GetUpdateAuction();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datetimepicker", "postback();", true);
            }
        }
        #endregion

        #region gvAuctionAssign Event
        protected void gvAuctionAssign_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gvAuctionAssign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";

                string sdate = e.Row.Cells[1].Text;
                if (!string.IsNullOrEmpty(sdate)) e.Row.Cells[1].Text = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy");
            }
        }

        protected void gvAuctionAssign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAuctionAssign.PageIndex = e.NewPageIndex;
            gvAuctionAssign.DataSource = vAuctionAssignByOrderList;
            gvAuctionAssign.DataBind();
        }

        protected void gvAuctionAssign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                SubCommandName = "Edit";
                BindDDlAuctionTime();

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string aucID = (gvAuctionAssign.Rows[index].FindControl("hidAuctionID") as HiddenField).Value.Trim();
                string aucTime = (gvAuctionAssign.Rows[index].FindControl("lbAuctionTime") as Label).Text.Trim();
                string aucOrder = txtOrder_auctionmodal.Value.Trim();
                string assignDate = gvAuctionAssign.Rows[index].Cells[1].Text.Trim();
                string remark = gvAuctionAssign.Rows[index].Cells[2].Text.Trim();

                // bind to controller
                hidAucID_auctionassignModal.Value = aucID;
                txtOrder_auctionassignModal.Value = aucOrder;
                ddlAuctionTime_auctionAssignModal.SelectedValue = aucTime;
                txtDate_auctionAssignModal.Value = assignDate;
                txtRemark_auctionAssignModal.Value = remark;

                //ddlAuctionTime_auctionAssignModal.Enabled = false;
                //ddlAuctionTime_auctionAssignModal.Attributes.Add("CssClass", "form-control");
                //txtDate_auctionAssignModal.Disabled = true;

                UpdatePanel11.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel11, UpdatePanel11.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "auctionAssignModal", "modalshowAuctionAssign();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string aucID = (gvAuctionAssign.Rows[index].FindControl("hidAuctionID") as HiddenField).Value.Trim();
                string aucTime = (gvAuctionAssign.Rows[index].FindControl("lbAuctionTime") as Label).Text.Trim();
                int iaucID = Convert.ToInt32(aucID);
                int iaucTime = Convert.ToInt32(aucTime);
                //===========trAuctionAssign===========
                auctionAssignList = AuctionAssignList;
                auctionAssignList.RemoveAll(A => A.AuctionID == iaucID && A.ActionTime == iaucTime);
                AuctionAssignList = auctionAssignList;
                //===========vAuctionAssign===========
                vauctionAssignList = vAuctionAssignList;
                vauctionAssignList.RemoveAll(A => A.AuctionID == iaucID && A.ActionTime == iaucTime);
                vAuctionAssignList = vauctionAssignList;

                GetUpdateAuctionAssignByOrder(Convert.ToInt32(txtOrder_auctionmodal.Value));
                UpdatePanel7.Update();
            }
        }
        #endregion

        #region gvAuctionAsset Event
        protected void gvAuctionAsset_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gvAuctionAsset_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvAuctionAsset_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAuctionAsset.PageIndex = e.NewPageIndex;
            gvAuctionAsset.DataSource = vAuctionAssetByOrderList;
            gvAuctionAsset.DataBind();
        }

        protected void gvAuctionAsset_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                SubCommandName = "Edit";

                //// get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string assetID = (gvAuctionAsset.Rows[index].FindControl("hidAsset") as HiddenField).Value.Trim();
                int aucID = Convert.ToInt32((gvAuctionAsset.Rows[index].FindControl("hidAucID") as HiddenField).Value.Trim());
                int iassetID = Convert.ToInt32(assetID);
                int order = Convert.ToInt32(txtOrder_auctionmodal.Value.Trim());
                //DateTime? otherBuyDate = null;
                //DateTime? bankBuyDate = null;
                //DateTime? gotMoneyDate = null;
                DateTime? otherBuyDate = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).AnotherBuyDate;
                DateTime? bankBuyDate = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).BankBuyDate;
                DateTime? gotMoneyDate = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).GotMoneyDate;
                string otherBuyAmt = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).AnotherBuyAmt.ToString();
                string bankBuyAmt = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).BankBuyAmt.ToString();
                string gotMoneyAmt = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).AccountPerAmt.ToString();
                string price = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).Price.ToString();
                string priceOnBoard = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).PriceOnBoard.ToString();
                string priceOnBoardSold = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).PriceOnBoardSold.ToString();
                string remark = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).Remark;
                string flag = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).Flag;
                string assetTypeID = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).AssetTypeID.ToString();
                string assetTypeName = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).AssetTypeName;
                string address = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).Address;
                string district = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).District;
                string province = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).Province;
                string zipcode = vAuctionAssetList.First(auc => auc.AuctionID == aucID && auc.AssetID == iassetID).ZipCode;

                //Bind_ddlAuctionAsset();
                Bind_ddlAuctionAsset2();

                hidAuctionid_modal.Value = aucID.ToString();
                ddlAsset_auctionassetmodal.SelectedValue = assetID;
                if (otherBuyDate != null) txtOtherDate_modal.Value = Convert.ToDateTime(otherBuyDate).ToString("dd/MM/yyyy");
                else txtOtherDate_modal.Value = null;
                if (bankBuyDate != null) txtBankBuy_modal.Value = Convert.ToDateTime(bankBuyDate).ToString("dd/MM/yyyy");
                else txtBankBuy_modal.Value = null;
                if (gotMoneyDate != null) txtGetMoneyDate.Value = Convert.ToDateTime(gotMoneyDate).ToString("dd/MM/yyyy");
                else txtGetMoneyDate.Value = null;
                txtOtherAmt_modal.Value = Convert.ToDecimal(otherBuyAmt).ToString("F2");
                txtBankAmt_modal.Value = Convert.ToDecimal(bankBuyAmt).ToString("F2");
                txtGetMoneyAmt.Value = Convert.ToDecimal(gotMoneyAmt).ToString("F2");
                txtPrice_modal.Value = Convert.ToDecimal(price).ToString("F2");
                txtPriceBoard_modal.Value = Convert.ToDecimal(priceOnBoardSold).ToString("F2");
                txtPriceApprove.Value = Convert.ToDecimal(priceOnBoardSold).ToString("F2");
                txtremark_modal.Value = remark;

                ddlAsset_auctionassetmodal.Enabled = false;
                UpdatePanel12.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "auctionAssetModal", "modalshowAuctionAsset();", true);
            }
            else if (e.CommandName == "Delete")
            {
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int aucID = Convert.ToInt32((gvAuctionAsset.Rows[index].FindControl("hidAucID") as HiddenField).Value.Trim());
                int assetID = Convert.ToInt32((gvAuctionAsset.Rows[index].FindControl("hidAsset") as HiddenField).Value.Trim());
                int order = Convert.ToInt32(txtOrder_auctionmodal.Value.Trim());

                //===========trAuctionAsset===========
                auctionAssetList = AuctionAssetList;
                auctionAssetList.RemoveAll(list => list.AssetID == assetID);
                AuctionAssetList = auctionAssetList;
                //===========vAuctionAsset===========
                vauctionAssetList = vAuctionAssetList;
                vauctionAssetList.RemoveAll(list => list.AssetID == assetID);
                vAuctionAssetList = vauctionAssetList;

                GetUpdateAuctionAssetByOrder(order);
                UpdatePanel7.Update();
            }
        }
        #endregion

        #region gvAuctionDoc Event
        protected void gvAuctionDoc_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gvAuctionDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvAuctionDoc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAuctionDoc.PageIndex = e.NewPageIndex;
            gvAuctionDoc.DataSource = vAttachDocList;
            gvAuctionDoc.DataBind();
        }

        protected void gvAuctionDoc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewCommand")
                {
                    #region NOT USE : OLD VERSION
                    //// get data from grid
                    //GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    //int index = rindex.RowIndex;
                    //string sDocID = (gvAuctionDoc.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim();
                    //int iDocID = Convert.ToInt32(sDocID);
                    //string docPath = AttachDocList.Find(list => list.DocID == iDocID).DocPath;

                    //// set UI
                    //iPDF.Src = docPath;
                    //ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "pdfModal", "modalshowpdf();", true);
                    #endregion
                }
                else if (e.CommandName == "Delete")
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int docid = Convert.ToInt32((gvAuctionDoc.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());
                    int auctionorder = Convert.ToInt32(vAttachDocList.Find(list => list.DocID == docid).AuctionOrder);
                    string flag = "A";
                    //===========trDocument===========
                    attachDocList = AttachDocList;
                    attachDocList.RemoveAll(list => list.DocID == docid);
                    AttachDocList = attachDocList;
                    //===========trDocumentMapping===========
                    docMappingList = DocMappingList;
                    docMappingList.RemoveAll(list => list.DocID == docid);
                    DocMappingList = docMappingList;
                    //===========vDocumentMapping===========
                    vattachDocList = vAttachDocList;
                    vattachDocList.RemoveAll(list => list.DocID == docid);
                    vAttachDocList = vattachDocList;

                    GetUpdateDocMapping(flag, auctionorder);
                    UpdatePanel7.Update();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region gvMortgage Event
        protected void gvMortgage_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                HiddenField hidMorID_shw = gvMortgage.Rows[index].FindControl("hidMorID_shw") as HiddenField;
                int imorID_shw = Convert.ToInt32(hidMorID_shw.Value);

                //===========MortgageList===========
                mortgageList = MortgageList;
                mortgageList.RemoveAll(list => list.MortgageID == imorID_shw);
                MortgageList = mortgageList;
                //===========vMortgageList===========
                vmortgageList = vMortgageList;
                vmortgageList.RemoveAll(list => list.MortgageID == imorID_shw);
                vMortgageList = vmortgageList;

                CommandName = "Delete";
                GetUpdateMortgage();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMortgage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMortgage.PageIndex = e.NewPageIndex;
            gvMortgage.DataSource = vMortgageList;
            gvMortgage.DataBind();
        }

        protected void gvMortgage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;

                switch (e.CommandName)
                {
                    case "Update":

                        #region get data from GridView
                        int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                        HiddenField hidMorID_edt = gvMortgage.Rows[index].FindControl("hidMorID_edt") as HiddenField;
                        TextBox txtMorDate = gvMortgage.Rows[index].FindControl("txtMorDate") as TextBox;
                        DropDownList ddlAsset = gvMortgage.Rows[index].FindControl("ddlAsset") as DropDownList;
                        TextBox morFee = gvMortgage.Rows[index].FindControl("morFee") as TextBox;
                        TextBox txtInvestDate = gvMortgage.Rows[index].FindControl("txtInvestDate") as TextBox;
                        TextBox txtFiatDate = gvMortgage.Rows[index].FindControl("txtFiatDate") as TextBox;
                        TextBox txHandDate = gvMortgage.Rows[index].FindControl("txHandDate") as TextBox;

                        DateTime updateDate = DateTime.Now;
                        //string user = "T67511";
                        int imorID_edit = Convert.ToInt32(hidMorID_edt.Value.Trim());
                        DateTime morDate = new DateTime();
                        int assetID = 0;
                        decimal mortgageFee = 0;
                        DateTime? investdate = null;
                        DateTime fiatDate = new DateTime();
                        DateTime? handDate = null;
                        string assetDesc = string.Empty;
                        int pursueID = 0;

                        if (!string.IsNullOrEmpty(ddlAsset.SelectedValue)) assetID = Convert.ToInt32(ddlAsset.SelectedValue.Trim());
                        else throw new Exception("กรุณาเลือก \"ทรัย์สิน\"");
                        if (!string.IsNullOrEmpty(morFee.Text)) mortgageFee = Convert.ToDecimal(morFee.Text.Trim());
                        if (!string.IsNullOrEmpty(txtMorDate.Text)) morDate = Convert.ToDateTime(txtMorDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่\"");
                        if (!string.IsNullOrEmpty(txtInvestDate.Text)) investdate = Convert.ToDateTime(txtInvestDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txHandDate.Text)) handDate = Convert.ToDateTime(txHandDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtFiatDate.Text)) fiatDate = Convert.ToDateTime(txtFiatDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่ศาลมีคำสั่ง\"");
                        if (vAssetMappingList.FindAll(A => A.AssetID == assetID).Count > 0)
                        {
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = vAssetMappingList.Find(A => A.AssetID == assetID).AssetTypeName;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = assetDesc + ": " + vAssetMappingList.Find(A => A.AssetID == assetID).Address;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).District)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).District;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Province)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).Province;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode;
                            if (vAssetMappingList.Find(A => A.AssetID == assetID).PursueID > 0) pursueID = vAssetMappingList.Find(A => A.AssetID == assetID).PursueID;
                        }
                        #endregion

                        //===========MortgageList===========
                        trMortgage trMor = new trMortgage();
                        trMor.MortgageID = imorID_edit;
                        trMor.LegalNo = LegalData.LegalNo;
                        trMor.AssetID = assetID;
                        trMor.MorDate = morDate;
                        trMor.MorFee = mortgageFee;
                        trMor.InvestigateDate = investdate;
                        trMor.FiatDate = fiatDate;
                        trMor.HandDate = handDate;
                        trMor.UpdateBy = userCode;
                        trMor.UpdatedateTime = updateDate;
                        if (CommandName.Equals("Add"))
                        {
                            trMor.CreateBy = userCode;
                            trMor.CreateDateTime = updateDate;
                        }
                        else
                        {
                            trMor.CreateBy = MortgageList.Find(M => M.MortgageID == imorID_edit).CreateBy;
                            trMor.CreateDateTime = MortgageList.Find(M => M.MortgageID == imorID_edit).CreateDateTime;
                        }

                        mortgageList = MortgageList;
                        mortgageList.RemoveAll(list => list.MortgageID == imorID_edit);
                        mortgageList.Add(trMor);
                        MortgageList = mortgageList;
                        //===========vMortgageList===========       
                        vMortgage viewMor = new vMortgage();
                        viewMor.MortgageID = imorID_edit;
                        viewMor.LegalNo = LegalData.LegalNo;
                        viewMor.AssetID = assetID;
                        viewMor.MorDate = morDate;
                        viewMor.MorFee = mortgageFee;
                        viewMor.InvestigateDate = investdate;
                        viewMor.FiatDate = fiatDate;
                        viewMor.HandDate = handDate;
                        viewMor.PursueID = pursueID;
                        viewMor.AssetDesc = assetDesc;

                        vmortgageList = vMortgageList;
                        vmortgageList.RemoveAll(list => list.MortgageID == imorID_edit);
                        vmortgageList.Add(viewMor);
                        vMortgageList = vmortgageList;
                        gvMortgage.EditIndex = -1;
                        CommandName = "Update";
                        break;
                }
                GetUpdateMortgage();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }

        }

        protected void gvMortgage_RowEditing1(object sender, GridViewEditEventArgs e)
        {
            gvMortgage.EditIndex = e.NewEditIndex;
            GetUpdateMortgage();

            if (CommandName.Equals("Add"))
            {
                TextBox txtMorDate = gvMortgage.Rows[e.NewEditIndex].FindControl("txtMorDate") as TextBox;
                DropDownList ddlAsset = gvMortgage.Rows[e.NewEditIndex].FindControl("ddlAsset") as DropDownList;
                TextBox morFee = gvMortgage.Rows[e.NewEditIndex].FindControl("morFee") as TextBox;
                TextBox txtInvestDate = gvMortgage.Rows[e.NewEditIndex].FindControl("txtInvestDate") as TextBox;
                TextBox txtFiatDate = gvMortgage.Rows[e.NewEditIndex].FindControl("txtFiatDate") as TextBox;
                TextBox txHandDate = gvMortgage.Rows[e.NewEditIndex].FindControl("txHandDate") as TextBox;

                txtMorDate.Text = string.Empty;
                ddlAsset.SelectedIndex = 0;
                morFee.Text = string.Empty;
                txtInvestDate.Text = string.Empty;
                txtFiatDate.Text = string.Empty;
                txHandDate.Text = string.Empty;
            }
        }

        protected void gvMortgage_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvMortgage_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                int index = e.RowIndex;
                HiddenField hidMorID_edt = gvMortgage.Rows[index].FindControl("hidMorID_edt") as HiddenField;
                int imorID_shw = Convert.ToInt32(hidMorID_edt.Value);

                //===========MortgageList===========
                mortgageList = MortgageList;
                mortgageList.RemoveAll(list => list.MortgageID == imorID_shw);
                MortgageList = mortgageList;
                //===========vMortgageList===========
                vmortgageList = vMortgageList;
                vmortgageList.RemoveAll(list => list.MortgageID == imorID_shw);
                vMortgageList = vmortgageList;
            }
            CommandName = "Cancel";
            gvMortgage.EditIndex = -1;
            GetUpdateMortgage();
        }

        protected void gvMortgage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlAsset = e.Row.FindControl("ddlAsset") as DropDownList;

                DataTable dtddlAsset = new DataTable();
                dtddlAsset.Columns.Add("AssetID");
                dtddlAsset.Columns.Add("AssetDesc");
                vassetMappingList = vAssetMappingList.FindAll(list => list.LegalNo_pur.Equals(LegalData.LegalNo));
                if (vassetMappingList.Count > 0)
                {
                    foreach (vAssetMapping map in vassetMappingList)
                    {
                        string add = map.AssetTypeName;
                        if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                        if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                        if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                        if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                        DataRow row = dtddlAsset.NewRow();
                        row["AssetID"] = map.AssetID;
                        row["AssetDesc"] = add;
                        dtddlAsset.Rows.Add(row);
                    }
                    if (ddlAsset == null) { return; } // for edit mode
                    ddlAsset.DataSource = dtddlAsset;
                    ddlAsset.DataBind();
                }
            }
        }
        #endregion

        #region gvAverage Event
        protected void gvAverage_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = e.RowIndex;
            HiddenField hidAvgID_shw = gvAverage.Rows[index].FindControl("hidAvgID_shw") as HiddenField;
            int iavgID_shw = Convert.ToInt32(hidAvgID_shw.Value);

            //===========MortgageList===========
            averageList = AverageList;
            averageList.RemoveAll(list => list.AverageID == iavgID_shw);
            AverageList = averageList;
            //===========vMortgageList===========
            vaverageList = vAverageList;
            vaverageList.RemoveAll(list => list.AverageID == iavgID_shw);
            vAverageList = vaverageList;

            CommandName = "Delete";
            GetUpdateAverage();
            UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
        }

        protected void gvAverage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAverage.PageIndex = e.NewPageIndex;
            gvAverage.DataSource = vAverageList;
            gvAverage.DataBind();
        }

        protected void gvAverage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;

                switch (e.CommandName)
                {
                    case "Update":
                        #region get data from GridView
                        int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                        HiddenField hidAvgID_edt = gvAverage.Rows[index].FindControl("hidAvgID_edt") as HiddenField;
                        TextBox txtAvgDate = gvAverage.Rows[index].FindControl("txtAvgDate") as TextBox;
                        DropDownList ddlAsset = gvAverage.Rows[index].FindControl("ddlAsset") as DropDownList;
                        TextBox txtAvgFee = gvAverage.Rows[index].FindControl("txtAvgFee") as TextBox;
                        TextBox txtInvestDate = gvAverage.Rows[index].FindControl("txtInvestDate") as TextBox;
                        TextBox txtFiatDate = gvAverage.Rows[index].FindControl("txtFiatDate") as TextBox;
                        TextBox txtHandDate = gvAverage.Rows[index].FindControl("txHandDate") as TextBox;
                        TextBox txtReceiveDate = gvAverage.Rows[index].FindControl("txtReceiveDate") as TextBox;
                        TextBox txtReceiveAmt = gvAverage.Rows[index].FindControl("txtReceiveAmt") as TextBox;

                        DateTime updateDate = DateTime.Now;
                        //string user = "T67511";
                        int iavgID_edit = Convert.ToInt32(hidAvgID_edt.Value.Trim());
                        DateTime avgDate = new DateTime();
                        int assetID = 0;
                        decimal avgFee = 0;
                        DateTime? investdate = null;
                        DateTime fiatDate = new DateTime();
                        DateTime? handDate = null;
                        string assetDesc = string.Empty;
                        int pursueID = 0;
                        DateTime? receiveDate = null;
                        decimal receiveAmt = 0;

                        if (!string.IsNullOrEmpty(txtAvgDate.Text)) avgDate = Convert.ToDateTime(txtAvgDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่\"");
                        if (!string.IsNullOrEmpty(ddlAsset.SelectedValue)) assetID = Convert.ToInt32(ddlAsset.SelectedValue.Trim());
                        else throw new Exception("กรุณาเลือก \"ทรัพย์สิน\"");
                        if (!string.IsNullOrEmpty(txtAvgFee.Text)) avgFee = Convert.ToDecimal(txtAvgFee.Text.Trim());
                        if (!string.IsNullOrEmpty(txtInvestDate.Text)) investdate = Convert.ToDateTime(txtInvestDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtHandDate.Text)) handDate = Convert.ToDateTime(txtHandDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtReceiveDate.Text)) receiveDate = Convert.ToDateTime(txtReceiveDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtReceiveAmt.Text)) receiveAmt = Convert.ToDecimal(txtReceiveAmt.Text.Trim());
                        if (!string.IsNullOrEmpty(txtFiatDate.Text)) fiatDate = Convert.ToDateTime(txtFiatDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่ศาลมีคำสั่ง\"");
                        if (vAssetMappingList.FindAll(A => A.AssetID == assetID).Count > 0)
                        {
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = vAssetMappingList.Find(A => A.AssetID == assetID).AssetTypeName;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = assetDesc + ": " + vAssetMappingList.Find(A => A.AssetID == assetID).Address;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).District)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).District;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Province)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).Province;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode;
                            if (vAssetMappingList.Find(A => A.AssetID == assetID).PursueID > 0) pursueID = vAssetMappingList.Find(A => A.AssetID == assetID).PursueID;
                        }
                        #endregion

                        //===========AverageList===========
                        trAverage trAvg = new trAverage();
                        trAvg.AverageID = iavgID_edit;
                        trAvg.LegalNo = LegalData.LegalNo;
                        trAvg.AssetID = assetID;
                        trAvg.AvrDate = avgDate;
                        trAvg.AvrFee = avgFee;
                        trAvg.InvestigateDate = investdate;
                        trAvg.FiatDate = fiatDate;
                        trAvg.HandDate = handDate;
                        trAvg.UpdateBy = userCode;
                        trAvg.UpdateDateTime = updateDate;
                        trAvg.ReceiveDate = receiveDate;
                        trAvg.ReceiveAmt = receiveAmt;
                        if (CommandName.Equals("Add"))
                        {
                            trAvg.CreateBy = userCode;
                            trAvg.CreateDateTime = updateDate;
                        }
                        else
                        {
                            trAvg.CreateBy = AverageList.Find(A => A.AverageID == iavgID_edit).CreateBy;
                            trAvg.CreateDateTime = AverageList.Find(A => A.AverageID == iavgID_edit).CreateDateTime;
                        }

                        averageList = AverageList;
                        averageList.RemoveAll(list => list.AverageID == iavgID_edit);
                        averageList.Add(trAvg);
                        AverageList = averageList;
                        //===========vAverageList===========       
                        vAverage viewAvg = new vAverage();
                        viewAvg.AverageID = iavgID_edit;
                        viewAvg.LegalNo = LegalData.LegalNo;
                        viewAvg.AssetID = assetID;
                        viewAvg.AvrDate = avgDate;
                        viewAvg.AvrFee = avgFee;
                        viewAvg.InvestigateDate = investdate;
                        viewAvg.FiatDate = fiatDate;
                        viewAvg.HandDate = handDate;
                        viewAvg.PursueID = pursueID;
                        viewAvg.AssetDesc = assetDesc;
                        viewAvg.ReceiveDate = receiveDate;
                        viewAvg.ReceiveAmt = receiveAmt;

                        vaverageList = vAverageList;
                        vaverageList.RemoveAll(list => list.AverageID == iavgID_edit);
                        vaverageList.Add(viewAvg);
                        vAverageList = vaverageList;
                        gvAverage.EditIndex = -1;
                        CommandName = "Update";
                        break;
                }
                GetUpdateAverage();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        protected void gvAverage_RowEditing1(object sender, GridViewEditEventArgs e)
        {
            gvAverage.EditIndex = e.NewEditIndex;
            GetUpdateAverage();

            if (CommandName.Equals("Add"))
            {
                TextBox txtAvgDate = gvAverage.Rows[e.NewEditIndex].FindControl("txtAvgDate") as TextBox;
                DropDownList ddlAsset = gvAverage.Rows[e.NewEditIndex].FindControl("ddlAsset") as DropDownList;
                TextBox txtAvgFee = gvAverage.Rows[e.NewEditIndex].FindControl("txtAvgFee") as TextBox;
                TextBox txtInvestDate = gvAverage.Rows[e.NewEditIndex].FindControl("txtInvestDate") as TextBox;
                TextBox txtFiatDate = gvAverage.Rows[e.NewEditIndex].FindControl("txtFiatDate") as TextBox;
                TextBox txtHandDate = gvAverage.Rows[e.NewEditIndex].FindControl("txHandDate") as TextBox;
                TextBox txtReceiveDate = gvAverage.Rows[e.NewEditIndex].FindControl("txtReceiveDate") as TextBox;
                TextBox txtReceiveAmt = gvAverage.Rows[e.NewEditIndex].FindControl("txtReceiveAmt") as TextBox;

                txtAvgDate.Text = string.Empty;
                ddlAsset.SelectedIndex = 0;
                txtAvgFee.Text = string.Empty;
                txtInvestDate.Text = string.Empty;
                txtFiatDate.Text = string.Empty;
                txtHandDate.Text = string.Empty;
                txtReceiveDate.Text = string.Empty;
                txtReceiveAmt.Text = string.Empty;
            }
        }

        protected void gvAverage_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvAverage_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                int index = e.RowIndex;
                HiddenField hidAvgID_edt = gvAverage.Rows[index].FindControl("hidAvgID_edt") as HiddenField;
                int iAvgID = Convert.ToInt32(hidAvgID_edt.Value);

                //===========MortgageList===========
                averageList = AverageList;
                averageList.RemoveAll(list => list.AverageID == iAvgID);
                AverageList = averageList;
                //===========vMortgageList===========
                vaverageList = vAverageList;
                vaverageList.RemoveAll(list => list.AverageID == iAvgID);
                vAverageList = vaverageList;
                CommandName = "Cancel";
            }
            gvAverage.EditIndex = -1;
            GetUpdateAverage();
        }

        protected void gvAverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlAsset = e.Row.FindControl("ddlAsset") as DropDownList;

                DataTable dtddlAsset = new DataTable();
                dtddlAsset.Columns.Add("AssetID");
                dtddlAsset.Columns.Add("AssetDesc");
                vassetMappingList = vAssetMappingList.FindAll(list => list.LegalNo_pur.Equals(LegalData.LegalNo));
                if (vassetMappingList.Count > 0)
                {
                    foreach (vAssetMapping map in vassetMappingList)
                    {
                        string add = map.AssetTypeName;
                        if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                        if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                        if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                        if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                        DataRow row = dtddlAsset.NewRow();
                        row["AssetID"] = map.AssetID;
                        row["AssetDesc"] = add;
                        dtddlAsset.Rows.Add(row);
                    }
                    if (ddlAsset == null) { return; } // for edit mode
                    ddlAsset.DataSource = dtddlAsset;
                    ddlAsset.DataBind();
                }
            }
        }
        #endregion

        #region gvRetain Event
        protected void gvRetain_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = e.RowIndex;
            HiddenField hidRetianID_shw = gvRetain.Rows[index].FindControl("hidRetainID_shw") as HiddenField;
            int iretID_shw = Convert.ToInt32(hidRetianID_shw.Value);

            //===========MortgageList===========
            retainList = RetainList;
            retainList.RemoveAll(list => list.RetainID == iretID_shw);
            RetainList = retainList;
            //===========vMortgageList===========
            vretainList = vRetainList;
            vretainList.RemoveAll(list => list.RetainID == iretID_shw);
            vRetainList = vretainList;

            CommandName = "Delete";
            GetUpdateRetain();
            UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
        }

        protected void gvRetain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRetain.PageIndex = e.NewPageIndex;
            gvRetain.DataSource = vRetainList;
            gvRetain.DataBind();
        }

        protected void gvRetain_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;

                switch (e.CommandName)
                {
                    case "Update":
                        #region get data from GridView
                        int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                        HiddenField hidRetainID_edt = gvRetain.Rows[index].FindControl("hidRetainID_edt") as HiddenField;
                        TextBox txtRetainDate = gvRetain.Rows[index].FindControl("txtRetainDate") as TextBox;
                        DropDownList ddlAsset = gvRetain.Rows[index].FindControl("ddlAsset") as DropDownList;
                        TextBox txtRetFee = gvRetain.Rows[index].FindControl("txtRetainFee") as TextBox;
                        TextBox txtInvestDate = gvRetain.Rows[index].FindControl("txtInvestDate") as TextBox;
                        TextBox txtFiatDate = gvRetain.Rows[index].FindControl("txtFiatDate") as TextBox;
                        TextBox txtHandDate = gvRetain.Rows[index].FindControl("txHandDate") as TextBox;
                        TextBox txtReceiveDate = gvRetain.Rows[index].FindControl("txtReceiveDate") as TextBox;
                        TextBox txtReceiveAmt = gvRetain.Rows[index].FindControl("txtReceiveAmt") as TextBox;

                        DateTime updateDate = DateTime.Now;
                        //string user = "T67511";
                        int iretID_edit = Convert.ToInt32(hidRetainID_edt.Value.Trim());
                        DateTime retDate = new DateTime();
                        int assetID = 0;
                        int pursueID = 0;
                        string assetDesc = string.Empty;
                        decimal retFee = 0;
                        decimal receiveAmt = 0;
                        DateTime? investdate = null;
                        DateTime? handDate = null;
                        DateTime? receiveDate = null;
                        DateTime fiatDate = new DateTime();

                        if (!string.IsNullOrEmpty(txtRetainDate.Text)) retDate = Convert.ToDateTime(txtRetainDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่\"");
                        if (!string.IsNullOrEmpty(ddlAsset.SelectedValue)) assetID = Convert.ToInt32(ddlAsset.SelectedValue.Trim());
                        else throw new Exception("กรุณาเลือก \"วันที่ทรัพย์สิน\"");
                        if (!string.IsNullOrEmpty(txtRetFee.Text)) retFee = Convert.ToDecimal(txtRetFee.Text.Trim());
                        if (!string.IsNullOrEmpty(txtInvestDate.Text)) investdate = Convert.ToDateTime(txtInvestDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtHandDate.Text)) handDate = Convert.ToDateTime(txtHandDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtReceiveDate.Text)) receiveDate = Convert.ToDateTime(txtReceiveDate.Text.Trim());
                        if (!string.IsNullOrEmpty(txtReceiveAmt.Text)) receiveAmt = Convert.ToDecimal(txtReceiveAmt.Text.Trim());
                        if (!string.IsNullOrEmpty(txtFiatDate.Text)) fiatDate = Convert.ToDateTime(txtFiatDate.Text.Trim());
                        else throw new Exception("กรุณากรอกข้อมูล \"วันที่ศาลมีคำสัง\"");
                        if (vAssetMappingList.FindAll(A => A.AssetID == assetID).Count > 0)
                        {
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = vAssetMappingList.Find(A => A.AssetID == assetID).AssetTypeName;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Address)) assetDesc = assetDesc + ": " + vAssetMappingList.Find(A => A.AssetID == assetID).Address;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).District)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).District;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).Province)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).Province;
                            if (!string.IsNullOrEmpty(vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode)) assetDesc = assetDesc + ", " + vAssetMappingList.Find(A => A.AssetID == assetID).ZipCode;
                            if (vAssetMappingList.Find(A => A.AssetID == assetID).PursueID > 0) pursueID = vAssetMappingList.Find(A => A.AssetID == assetID).PursueID;
                        }
                        #endregion

                        //===========RetainList===========
                        trRetain trRet = new trRetain();
                        trRet.RetainID = iretID_edit;
                        trRet.LegalNo = LegalData.LegalNo;
                        trRet.AssetID = assetID;
                        trRet.RetDate = retDate;
                        trRet.RetFee = retFee;
                        trRet.InvestigateDate = investdate;
                        trRet.FiatDate = fiatDate;
                        trRet.HandDate = handDate;
                        trRet.UpdateBy = userCode;
                        trRet.UpdateDateTime = updateDate;
                        trRet.ReceiveDate = receiveDate;
                        trRet.ReceiveAmt = receiveAmt;
                        if (CommandName.Equals("Add"))
                        {
                            trRet.CreateBy = userCode;
                            trRet.CreateDateTime = updateDate;
                        }
                        else
                        {
                            trRet.CreateBy = RetainList.Find(R => R.RetainID == iretID_edit).CreateBy;
                            trRet.CreateDateTime = RetainList.Find(R => R.RetainID == iretID_edit).CreateDateTime;
                        }

                        retainList = RetainList;
                        retainList.RemoveAll(list => list.RetainID == iretID_edit);
                        retainList.Add(trRet);
                        RetainList = retainList;
                        //===========vRetainList===========       
                        vRetain viewRet = new vRetain();
                        viewRet.RetainID = iretID_edit;
                        viewRet.LegalNo = LegalData.LegalNo;
                        viewRet.AssetID = assetID;
                        viewRet.RetDate = retDate;
                        viewRet.RetFee = retFee;
                        viewRet.InvestigateDate = investdate;
                        viewRet.FiatDate = fiatDate;
                        viewRet.HandDate = handDate;
                        viewRet.PursueID = pursueID;
                        viewRet.AssetDesc = assetDesc;
                        viewRet.ReceiveDate = receiveDate;
                        viewRet.ReceiveAmt = receiveAmt;

                        vretainList = vRetainList;
                        vretainList.RemoveAll(list => list.RetainID == iretID_edit);
                        vretainList.Add(viewRet);
                        vRetainList = vretainList;
                        gvRetain.EditIndex = -1;
                        CommandName = "Update";
                        break;
                }
                GetUpdateRetain();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        protected void gvRetain_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvRetain.EditIndex = e.NewEditIndex;
            GetUpdateRetain();

            if (CommandName.Equals("Add"))
            {
                TextBox txtRetainDate = gvRetain.Rows[e.NewEditIndex].FindControl("txtRetainDate") as TextBox;
                DropDownList ddlAsset = gvRetain.Rows[e.NewEditIndex].FindControl("ddlAsset") as DropDownList;
                TextBox txtRetFee = gvRetain.Rows[e.NewEditIndex].FindControl("txtRetainFee") as TextBox;
                TextBox txtInvestDate = gvRetain.Rows[e.NewEditIndex].FindControl("txtInvestDate") as TextBox;
                TextBox txtFiatDate = gvRetain.Rows[e.NewEditIndex].FindControl("txtFiatDate") as TextBox;
                TextBox txtHandDate = gvRetain.Rows[e.NewEditIndex].FindControl("txHandDate") as TextBox;
                TextBox txtReceiveDate = gvRetain.Rows[e.NewEditIndex].FindControl("txtReceiveDate") as TextBox;
                TextBox txtReceiveAmt = gvRetain.Rows[e.NewEditIndex].FindControl("txtReceiveAmt") as TextBox;

                txtRetainDate.Text = string.Empty;
                ddlAsset.SelectedIndex = 0;
                txtRetFee.Text = string.Empty;
                txtInvestDate.Text = string.Empty;
                txtFiatDate.Text = string.Empty;
                txtHandDate.Text = string.Empty;
                txtReceiveDate.Text = string.Empty;
                txtReceiveAmt.Text = string.Empty;
            }
        }

        protected void gvRetain_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvRetain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                int index = e.RowIndex;
                HiddenField hidRetainID_edt = gvRetain.Rows[index].FindControl("hidRetainID_edt") as HiddenField;
                int iRetainID = Convert.ToInt32(hidRetainID_edt.Value);

                //===========MortgageList===========
                retainList = RetainList;
                retainList.RemoveAll(list => list.RetainID == iRetainID);
                RetainList = retainList;
                //===========vMortgageList===========
                vretainList = vRetainList;
                vretainList.RemoveAll(list => list.RetainID == iRetainID);
                vRetainList = vretainList;
            }
            CommandName = "Cancel";
            gvRetain.EditIndex = -1;
            GetUpdateRetain();
        }

        protected void gvRetain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlAsset = e.Row.FindControl("ddlAsset") as DropDownList;

                DataTable dtddlAsset = new DataTable();
                dtddlAsset.Columns.Add("AssetID");
                dtddlAsset.Columns.Add("AssetDesc");
                vassetMappingList = vAssetMappingList.FindAll(list => list.LegalNo_pur.Equals(LegalData.LegalNo));
                if (vassetMappingList.Count > 0)
                {
                    foreach (vAssetMapping map in vassetMappingList)
                    {
                        string add = map.AssetTypeName;
                        if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                        if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                        if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                        if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                        DataRow row = dtddlAsset.NewRow();
                        row["AssetID"] = map.AssetID;
                        row["AssetDesc"] = add;
                        dtddlAsset.Rows.Add(row);
                    }
                    if (ddlAsset == null) { return; } // for edit mode
                    ddlAsset.DataSource = dtddlAsset;
                    ddlAsset.DataBind();
                }
            }
        }
        #endregion

        #region gvReqSeq Event
        protected void gvReqSeq_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = e.RowIndex;
            HiddenField hidReqseqID_shw = gvReqSeq.Rows[index].FindControl("hidReqseqID_shw") as HiddenField;
            int ireqseqID = Convert.ToInt32(hidReqseqID_shw.Value);

            reqseqList = ReqSeqList;
            reqseqList.RemoveAll(list => list.ReqSeqID == ireqseqID);
            ReqSeqList = reqseqList;

            CommandName = "Delete";
            GetUpdateReqSeq();
            UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
        }

        protected void gvReqSeq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReqSeq.PageIndex = e.NewPageIndex;
            gvReqSeq.DataSource = ReqSeqList;
            gvReqSeq.DataBind();
        }

        protected void gvReqSeq_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;
                switch (e.CommandName)
                {
                    case "Update":

                        #region get data from GridView
                        int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;

                        HiddenField hidReqseqID = gvReqSeq.Rows[index].FindControl("hidReqseqID") as HiddenField;
                        DropDownList ddlReqseqNo = gvReqSeq.Rows[index].FindControl("ddlReqseqNo") as DropDownList;
                        TextBox txtReceiveDate = gvReqSeq.Rows[index].FindControl("txtReceiveDate") as TextBox;
                        TextBox txtReceiveAmt = gvReqSeq.Rows[index].FindControl("txtReceiveAmt") as TextBox;

                        DateTime updateDate = DateTime.Now;
                        //string user = "T67511";
                        int iReqSeqID = Convert.ToInt32(hidReqseqID.Value);
                        int reqseqno = 0;
                        DateTime? receiveDate = null;
                        decimal receiveAmt = 0;

                        if (!string.IsNullOrEmpty(ddlReqseqNo.SelectedValue)) reqseqno = Convert.ToInt32(ddlReqseqNo.SelectedValue.Trim());
                        if (!string.IsNullOrEmpty(txtReceiveDate.Text)) receiveDate = Convert.ToDateTime(txtReceiveDate.Text.Trim());
                        else throw new Exception("กรุณากรอก \"วันที่รับเงิน\"");
                        if (!string.IsNullOrEmpty(txtReceiveAmt.Text)) receiveAmt = Convert.ToDecimal(txtReceiveAmt.Text.Trim());
                        else throw new Exception("กรุณากรอก \"จำนวนเงินที่รับ\"");
                        #endregion

                        //===========ReqSeqList===========
                        trRequestSequester reqseq = new trRequestSequester();
                        reqseq.LegalNo = LegalData.LegalNo;
                        reqseq.ReqSeqID = iReqSeqID;
                        reqseq.ReqSeqNo = reqseqno;
                        reqseq.ReqSeqDate = Convert.ToDateTime(receiveDate);
                        reqseq.ReqSeqAmt = Convert.ToDecimal(receiveAmt);
                        if (CommandName.Equals("Add"))
                        {
                            reqseq.CreateBy = userCode;
                            reqseq.CreateDateTime = updateDate;
                        }
                        else
                        {
                            reqseq.CreateBy = ReqSeqList.Find(R => R.ReqSeqID == iReqSeqID).CreateBy;
                            reqseq.CreateDateTime = ReqSeqList.Find(R => R.ReqSeqID == iReqSeqID).CreateDateTime;
                        }

                        reqseqList = ReqSeqList;
                        reqseqList.RemoveAll(list => list.ReqSeqID == iReqSeqID);
                        reqseqList.Add(reqseq);
                        ReqSeqList = reqseqList;
                        gvReqSeq.EditIndex = -1;
                        CommandName = "Update";
                        break;
                }
                GetUpdateReqSeq();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        protected void gvReqSeq_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvReqSeq.EditIndex = e.NewEditIndex;
            GetUpdateReqSeq();

            if (CommandName.Equals("Add"))
            {
                //HiddenField hidReqseqID = gvReqSeq.Rows[e.NewEditIndex].FindControl("hidReqseqID") as HiddenField;
                //DropDownList ddlReqseqNo = gvReqSeq.Rows[e.NewEditIndex].FindControl("ddlReqseqNo") as DropDownList;
                TextBox txtReceiveDate = gvReqSeq.Rows[e.NewEditIndex].FindControl("txtReceiveDate") as TextBox;
                TextBox txtReceiveAmt = gvReqSeq.Rows[e.NewEditIndex].FindControl("txtReceiveAmt") as TextBox;
                txtReceiveDate.Text = string.Empty;
                txtReceiveAmt.Text = string.Empty;
            }
        }

        protected void gvReqSeq_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvReqSeq_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                int index = e.RowIndex;
                HiddenField hidReqseqID = gvReqSeq.Rows[index].FindControl("hidReqseqID") as HiddenField;
                int ireqid = Convert.ToInt32(hidReqseqID.Value);

                //===========MortgageList===========
                reqseqList = ReqSeqList;
                reqseqList.RemoveAll(list => list.ReqSeqID == ireqid);
                ReqSeqList = reqseqList;
            }
            CommandName = "Cancel";
            gvReqSeq.EditIndex = -1;
            GetUpdateReqSeq();
        }

        protected void gvReqSeq_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hidReqseqID = e.Row.FindControl("hidReqseqID") as HiddenField;
                DropDownList ddlReqseqNo = e.Row.FindControl("ddlReqseqNo") as DropDownList;
                if (hidReqseqID == null) { return; }
                int reqseqid = Convert.ToInt32(hidReqseqID.Value);

                DataTable dt = new DataTable();
                dt.Columns.Add("ReqSeqNo");

                for (int i = 1; i <= 10; i++)
                {
                    int edtNO = ReqSeqList.Find(R => R.ReqSeqID == reqseqid).ReqSeqNo;
                    reqseqList = ReqSeqList.FindAll(R => R.ReqSeqNo == i);
                    if (reqseqList.Count == 0 || (reqseqList.Count > 0 && i == edtNO))
                    {
                        DataRow row = dt.NewRow();
                        row["ReqSeqNo"] = i;
                        dt.Rows.Add(row);
                    }
                }

                if (ddlReqseqNo == null) { return; }
                ddlReqseqNo.DataSource = dt;
                ddlReqseqNo.DataBind();
            }
        }
        #endregion
        #endregion

        #region Modal Event
        // Pursue Modal
        protected void btnAdd_Peusue_Click(object sender, EventArgs e)
        {
            try
            {
                hidid_pursuemodal.Value = string.Empty;
                txtPTime.Value = string.Empty;
                txtdate_pursuemodal.Value = string.Empty;
                ddlFoundAsset_pursuemodal.SelectedIndex = 0;
                CommandName = "Add";


                // set UI
                ClearAssetModal();
                ClearAttachDocumentModal();
                txtPTime.Disabled = false;
                hiddiv_pursuemodal.Attributes.Add("style", "display:none");
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "courtModal", "modalshowpursue();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnView_PursueModal_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "pdfModal", "modalshowpdf();", true);
        }

        protected void btnAddDoc_PursueModal_Click(object sender, EventArgs e)
        {
            try
            {
                if (CommandName.Equals("Edit"))
                {
                    //hidid_pursuemodal.Value
                }
                SubCommandName = "Add";
                ClearAssetModal();
                BindDDLAssettype_pursuemodal();
                if (string.IsNullOrEmpty(txtPTime.Value)) throw new Exception("กรุณากรอกข้อมูล \"ครั้งที่\"");
                else
                {
                    UpdatePanel3.Update();
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "assetmodal", "modalshowasset();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }

        }

        protected void btnUpload_PursueModal_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPTime.Value)) throw new Exception("กรุณากรอกข้อมูล \"ครั้งที่\"");
                else
                {
                    SubCommandName = "Add";
                    hidFlag_attachmodal.Value = "P";
                    BindddlDoctype_attachmodal("P");
                    UpdatePanel6.Update();
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "attachModal1", "modalshowattach1();", true);
                }
            }
            catch (Exception ex)
            {
                string err = "validateAlert('" + ex.Message + "')";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        protected void ddlFoundAsset_pursuemodal_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFoundAsset_pursuemodal.SelectedValue.Equals("0") && CommandName.Equals("Edit"))
                {
                    int pid = Convert.ToInt32(hidid_pursuemodal.Value.Trim());

                    //===========trAsset===========
                    assetMappingList = AssetMappingList.FindAll(list => list.PursueID == pid);
                    foreach (trAssetMapping ass in assetMappingList)
                    {
                        int assID = ass.AssetID;
                        assetList = AssetList;
                        assetList.RemoveAll(asset => asset.AssetID == assID);
                        AssetList = assetList;
                    }
                    //===========trAssetMapping===========
                    assetMappingList = AssetMappingList;
                    assetMappingList.RemoveAll(list => list.PursueID == pid);
                    AssetMappingList = assetMappingList;
                    //===========vAssetMapping===========
                    vassetMappingList = vAssetMappingList;
                    vassetMappingList.RemoveAll(list => list.PursueID == pid);
                    vAssetMappingList = vassetMappingList;
                    //===========trDocument===========
                    docMappingList = DocMappingList.FindAll(list => list.FID == pid && list.Flag.Equals("P"));
                    foreach (trDocumentMapping doc in docMappingList)
                    {
                        int docID = doc.DocID;
                        attachDocList = AttachDocList;
                        attachDocList.RemoveAll(asset => asset.DocID == docID);
                        AttachDocList = attachDocList;
                    }
                    //===========trDocumentMapping===========
                    docMappingList = DocMappingList;
                    docMappingList.RemoveAll(list => list.FID == pid && list.Flag.Equals("P"));
                    DocMappingList = docMappingList;
                    //===========vDocumentMapping===========
                    vattachDocList = vAttachDocList;
                    vattachDocList.RemoveAll(list => list.FID == pid && list.Flag.Equals("P"));
                    vAttachDocList = vattachDocList;
                }

                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "pageLoad", "postback()", true);
                if (ddlFoundAsset_pursuemodal.SelectedValue.Equals(string.Empty) || ddlFoundAsset_pursuemodal.SelectedValue.Equals("0"))
                {
                    ClearAssetModal();
                    ClearAttachDocumentModal();
                    hiddiv_pursuemodal.Attributes.Add("style", "display:none");
                }
                else if (ddlFoundAsset_pursuemodal.SelectedValue.Equals("1"))
                {
                    hiddiv_pursuemodal.Attributes.Add("style", "display:block");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ValidateSavePursueModal()
        {
            if (string.IsNullOrEmpty(txtPTime.Value)) throw new Exception("กรุณากรอกข้อมูล \"ครั้งที่\"");
            vassetMappingList = vAssetMappingList.FindAll(list => list.PursueTime == Convert.ToInt32(txtPTime.Value));
            if (string.IsNullOrEmpty(txtdate_pursuemodal.Value)) throw new Exception("กรุณากรอกข้อมูล \"วันที่สืบทรัพย์\"");
            if (ddlFoundAsset_pursuemodal.SelectedValue.Equals("1") && vassetMappingList.Count == 0) throw new Exception("กรุณาเพิ่มรายการ \"รายละเอียดทรัพย์\"");
        }

        protected void btnSave_PursueModal_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateSavePursueModal();

                int pid = 0;
                int ptime = Convert.ToInt32(txtPTime.Value);
                DateTime pdate = Convert.ToDateTime(txtdate_pursuemodal.Value);
                bool foundAsset = false;
                if (ddlFoundAsset_pursuemodal.SelectedValue.Equals("1")) foundAsset = true;
                string userCode = "T45677";
                DateTime dateUpdate = DateTime.Now;

                trPursue p = new trPursue();

                if (CommandName.Equals("Add"))
                {
                    //int check_ptime = PursueList.FindAll(pursue => pursue.PursueTime == ptime).Count;
                    //if (check_ptime < 1)
                    {
                        pid = 1;
                        if (PursueList.Count > 0)
                        {
                            pid = PursueList.Max(pursue => pursue.PursueID) + 1;
                        }
                        p.PursueID = pid;
                        p.LegalNo = LegalData.LegalNo;
                        p.PursueTime = ptime;
                        p.PusrsueDate = pdate;
                        p.FoundAssets = foundAsset;
                        p.CreateBy = userCode;
                        p.CreateDateTime = dateUpdate;
                        p.UpdateBy = userCode;
                        p.UpdateDateTime = dateUpdate;
                        pursueList = PursueList;
                        pursueList.Add(p);
                        PursueList = pursueList;
                    }
                }
                else if (CommandName.Equals("Edit"))
                {
                    pid = Convert.ToInt32(hidid_pursuemodal.Value);

                    p.PursueID = pid;
                    p.LegalNo = LegalData.LegalNo;
                    p.PursueTime = ptime;
                    p.PusrsueDate = pdate;
                    p.FoundAssets = foundAsset;
                    p.CreateBy = PursueList.Find(pursueList => pursueList.PursueID == pid).CreateBy;
                    p.CreateDateTime = PursueList.Find(pursueList => pursueList.PursueID == pid).CreateDateTime;
                    p.UpdateBy = userCode;
                    p.UpdateDateTime = dateUpdate;
                    List<trPursue> list = PursueList;
                    list.RemoveAll(pursue => pursue.PursueID == pid);
                    list.Add(p);
                    PursueList = list;
                }

                GetUpdatePursue();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "hide-pursueModal", "modalhidepursue();", true);
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        // Sequester Modal
        protected void btnAdd_Seq_Click(object sender, EventArgs e)
        {
            try
            {
                txtSTime_seqmodal.Value = string.Empty;
                txtdate1_seqmodal.Value = string.Empty;
                txtProposition.Value = string.Empty;
                txtdate2_seqmodal.Value = string.Empty;
                ddlExecution.SelectedIndex = 0;
                txtDebt_sqemodal.Value = string.Empty;
                ddlCourtZone_seqmodal.SelectedIndex = 0;
                ddlSoldType.SelectedIndex = 0;
                txtseq2date.Value = string.Empty;
                txtseqfee.Value = string.Empty;
                txtdate4_seqmodal.Value = string.Empty;
                seq2fee_seqmodal.Value = string.Empty;
                txtRemark_seqmodal.Value = string.Empty;
                hidseqID_seqmodal.Value = string.Empty;

                CommandName = "Add";

                GetUpdateSelectAssetByTime(-1); // set empty gridview
                UpdatePanel4.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "sequesterModal", "modalshowsequester();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnAddAsset_seqmodal_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hidseqID_seqmodal.Value) || SequesterList.Count(seq => seq.SequesterID == Convert.ToInt32(hidseqID_seqmodal.Value)) == 0)
                {
                    SubCommandName = "Add";
                    int seqid = 1;
                    if (SequesterList.Count > 0)
                    {
                        seqid = SequesterList.Max(seq => seq.SequesterID) + 1;
                    }
                    hidseqID_seqmodal.Value = seqid.ToString();
                }
                else
                {
                    hidseqID_seqmodal.Value = SequesterList.Find(seq => seq.SequesterID == Convert.ToInt32(hidseqID_seqmodal.Value)).SequesterID.ToString();
                    SubCommandName = "Edit";
                }

                GetUpdateSelectAsset();
                UpdatePanel10.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "assetSeqModal", "modalshowassetSeq();", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ValidateSequesterModal()
        {
            if (string.IsNullOrEmpty(txtSTime_seqmodal.Value)) throw new Exception("กรุณากรอก \"ยึด/อายัดครั้งที่\"");
            else
            {
                int seqtime = Convert.ToInt32(txtSTime_seqmodal.Value);
                if (SubCommandName.Equals("Add"))
                {
                    List<trSequester> newseq = SequesterList.FindAll(s => s.SeqTime == seqtime);
                    if (newseq.Count > 0) throw new Exception("ข้อมูล \"ยึด/อายัดครั้งที่\" ซ้ำ กรุณาเลือกใหม่");
                }
            }
            if (string.IsNullOrEmpty(txtseq2date.Value)) throw new Exception("กรุณากรอก \"วันตั้งเรื่องยึด\"");
            if (string.IsNullOrEmpty(txtdate4_seqmodal.Value)) throw new Exception("กรุณากรอก \"วันตั้งเรื่องอายัด\"");
        }

        protected void btnSave_seqmodal_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateSequesterModal();

                int seqid = 0;
                int seqtime = 0;
                int? courtzoneID = null;
                int? soldtypeID = null;
                int? executeID = null;
                DateTime? date1 = null;
                DateTime? date2 = null;
                DateTime date3 = new DateTime();
                DateTime date4 = new DateTime();
                decimal debt = 0;
                decimal seqfee = 0;
                decimal seq2fee = 0;
                string proposition = string.Empty;
                string remark = string.Empty;

                seqtime = Convert.ToInt32(txtSTime_seqmodal.Value.Trim());
                date3 = Convert.ToDateTime(txtseq2date.Value.Trim());
                date4 = Convert.ToDateTime(txtdate4_seqmodal.Value.Trim());
                if (!string.IsNullOrEmpty(txtdate1_seqmodal.Value)) date1 = Convert.ToDateTime(txtdate1_seqmodal.Value.Trim());
                if (!string.IsNullOrEmpty(txtdate2_seqmodal.Value)) date2 = Convert.ToDateTime(txtdate2_seqmodal.Value.Trim());
                if (Convert.ToInt32(ddlCourtZone_seqmodal.SelectedIndex) != 0) courtzoneID = Convert.ToInt32(ddlCourtZone_seqmodal.SelectedValue.Trim());
                if (Convert.ToInt32(ddlSoldType.SelectedIndex) != 0) soldtypeID = Convert.ToInt32(ddlSoldType.SelectedValue.Trim());
                if (Convert.ToInt32(ddlExecution.SelectedIndex) != 0) executeID = Convert.ToInt32(ddlExecution.SelectedValue.Trim());
                if (!string.IsNullOrEmpty(txtDebt_sqemodal.Value)) debt = Convert.ToDecimal(txtDebt_sqemodal.Value.Trim());
                if (!string.IsNullOrEmpty(txtseqfee.Value)) seqfee = Convert.ToDecimal(txtseqfee.Value.Trim());
                if (!string.IsNullOrEmpty(seq2fee_seqmodal.Value)) seq2fee = Convert.ToDecimal(seq2fee_seqmodal.Value.Trim());
                proposition = txtProposition.Value.Trim();
                remark = txtRemark_seqmodal.Value.Trim();
                DateTime dateUpdate = DateTime.Now;

                trSequester s = new trSequester();

                if (CommandName.Equals("Add"))
                {
                    seqid = 1;
                    if (SequesterList.Count > 0)
                    {
                        seqid = SequesterList.Max(seq => seq.SequesterID) + 1;
                    }
                    s.SequesterID = seqid;
                    s.LegalNo = LegalData.LegalNo;
                    s.SeqTime = seqtime;
                    s.GotDeedDate = date1;
                    s.Proposition = proposition;
                    s.SendDeedDate = date2;
                    s.ExecutionID = executeID;
                    s.Debt = debt;
                    s.CourtZoneID = courtzoneID;
                    s.SoldType = soldtypeID;
                    s.SeqDate = date3;     //ยึด
                    s.SeqFee = seqfee;     //ยึด
                    s.Seq2Date = date4;  //อายัด
                    s.Seq2Fee = seq2fee;  //อายัด
                    s.Comment = remark;
                    s.CreateBy = userCode;
                    s.CreateDateTime = dateUpdate;
                    s.UpdateBy = userCode;
                    s.UpdateDateTime = dateUpdate;

                    sequesterList = SequesterList;
                    sequesterList.Add(s);
                }
                else if (CommandName.Equals("Edit"))
                {
                    seqid = Convert.ToInt32(hidseqID_seqmodal.Value.Trim());

                    s.SequesterID = seqid;
                    s.LegalNo = LegalData.LegalNo;
                    s.SeqTime = seqtime;
                    s.GotDeedDate = date1;
                    s.Proposition = proposition;
                    s.SendDeedDate = date2;
                    s.ExecutionID = executeID;
                    s.Debt = debt;
                    s.CourtZoneID = courtzoneID;
                    s.SoldType = soldtypeID;
                    s.SeqDate = date3;     //ยึด
                    s.SeqFee = seqfee;     //ยึด
                    s.Seq2Date = date4;  //อายัด
                    s.Seq2Fee = seq2fee;  //อายัด
                    s.Comment = remark;
                    s.CreateBy = SequesterList.Find(seq => seq.SequesterID == seqid).CreateBy;
                    s.CreateDateTime = SequesterList.Find(seq => seq.SequesterID == seqid).CreateDateTime;
                    s.UpdateBy = userCode;
                    s.UpdateDateTime = dateUpdate;

                    List<trSequester> list = SequesterList;
                    SequesterList.RemoveAll(seq => seq.SequesterID == seqid);
                    list.Add(s);
                    sequesterList = SequesterList;
                    sequesterList = list;
                }

                SequesterList = sequesterList;

                GetUpdateSequester();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "hide-sequesterModal", "modalhidesequester();", true);
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, UpdatePanel4.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        // Attach Document Modal
        public void BindddlDoctype_attachmodal(string flag)
        {
            // flag => P: Pursue, A: Auction, L: Legal(Tab7)
            DocumentTypeRep rep = new DocumentTypeRep();
            List<DocumentType> A = rep.GetDocumentTypeByFlag(flag);

            ddlDoctype_attachmodal.DataSource = A;
            if (A.Count > 0)
            {
                ddlDoctype_attachmodal.DataValueField = "DocTypeID";
                ddlDoctype_attachmodal.DataTextField = "DocTypeName";
            }
            ddlDoctype_attachmodal.DataBind();
            //ddlDoctype_attachmodal.Enabled = true;
        }

        public void BindDDLAssettype_pursuemodal()
        {
            AssetTypeRep rep = new AssetTypeRep();
            List<AssetType> A = rep.GetAssetTypeList();
            ddlAssetType.DataSource = A;
            if (A.Count > 0)
            {
                ddlAssetType.DataValueField = "AssetTypeID";
                ddlAssetType.DataTextField = "AssetTypeName";
            }
            ddlAssetType.DataBind();
            //ddlAssetType.Enabled = true;
        }

        public void ClearAttachDocumentModal()
        {
            ddlDoctype_attachmodal.SelectedIndex = 0;
            string flag = hidFlag_attachmodal.Value.Trim();

            int pursueTime = 0;
            GetUpdateDocMapping(flag, pursueTime);  // Set Gridview for empty data
            UpdatePanel6.Update();
        }

        public string GetPath(int docTypeID, string docName)
        {
            string path = string.Empty;
            switch (docTypeID)
            {
                default:
                    path = ConfigurationManager.DocPathType0;
                    break;
                case 1:
                    path = ConfigurationManager.DocPathType1;
                    break;
                case 2:
                    path = ConfigurationManager.DocPathType2;
                    break;
                case 3:
                    path = ConfigurationManager.DocPathType3;
                    break;
                case 4:
                    path = ConfigurationManager.DocPathType4;
                    break;
                case 5:
                    path = ConfigurationManager.DocPathType5;
                    break;
                case 6:
                    path = ConfigurationManager.DocPathType6;
                    break;
                case 7:
                    path = ConfigurationManager.DocPathType7;
                    break;
                case 8:
                    path = ConfigurationManager.DocPathType8;
                    break;
                case 9:
                    path = ConfigurationManager.DocPathType9;
                    break;
                case 10:
                    path = ConfigurationManager.DocPathType10;
                    break;
                case 11:
                    path = ConfigurationManager.DocPathType11;
                    break;
                case 12:
                    path = ConfigurationManager.DocPathType12;
                    break;
                case 13:
                    path = ConfigurationManager.DocPathType13;
                    break;
                case 14:
                    path = ConfigurationManager.DocPathType14;
                    break;
                case 15:
                    path = ConfigurationManager.DocPathType15;
                    break;
                case 16:
                    path = ConfigurationManager.DocPathType16;
                    break;
                case 17:
                    path = ConfigurationManager.DocPathType17;
                    break;
                case 18:
                    path = ConfigurationManager.DocPathType18;
                    break;
                case 19:
                    path = ConfigurationManager.DocPathType19;
                    break;
                case 20:
                    path = ConfigurationManager.DocPathType20;
                    break;
                case 21:
                    path = ConfigurationManager.DocPathType21;
                    break;
                case 22:
                    path = ConfigurationManager.DocPathType22;
                    break;
                case 23:
                    path = ConfigurationManager.DocPathType23;
                    break;
                case 24:
                    path = ConfigurationManager.DocPathType24;
                    break;
                case 25:
                    path = ConfigurationManager.DocPathType25;
                    break;
            }
            return path + docName;
        }

        public string GetFileName(string fileNameWithExtension)
        {
            int last = fileNameWithExtension.LastIndexOf(".");
            string name = fileNameWithExtension.Substring(0, last)
                + DateTime.Now.ToString("_yyyyMMddHHmmss") + "_t" // _t : before submit save
                //+ DateTime.Now.ToString("_yyyyMMddHHmmss")
                + fileNameWithExtension.Substring(last, fileNameWithExtension.Length - last);
            return name;
        }

        protected void btnadd_aucdoc_Click(object sender, EventArgs e)
        {
            SubCommandName = "Add";
            hidFlag_attachmodal.Value = "A";
            BindddlDoctype_attachmodal("A");
            UpdatePanel6.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "attachModal", "modalshowattach();", true);
        }

        protected void btnsave_attachdoc_Click(object sender, EventArgs e)
        {
            int docID = 1;
            int fID = 1;
            int pursueTime = 0;
            int auctionOrder = 0;
            int docTypeID = 0;
            string docTypeName = string.Empty;
            string docName = string.Empty;
            string docPath = string.Empty;
            string flag = hidFlag_attachmodal.Value.Trim();
            DateTime date = DateTime.Now;

            trDocument doc = new trDocument();
            trDocumentMapping map = new trDocumentMapping();
            vDocumentMapping view = new vDocumentMapping();

            // Save file as LegalSession
            if (AttachDocList.Count > 0)
            {
                docID += LegalSession.lastDocID;
                LegalSession.lastDocID++;
            }
            docTypeID = Convert.ToInt32(ddlDoctype_attachmodal.SelectedValue.Trim());
            docTypeName = ddlDoctype_attachmodal.SelectedItem.Text.Trim();
            if (LegalSession.filessave == null) LegalSession.filessave = new List<fileupload>();
            if (LegalSession.fileupload == null) LegalSession.fileupload = new FileUpload();
            FileUpload file = LegalSession.fileupload;
            docName = GetFileName(file.FileName);
            docPath = GetPath(docTypeID, docName);
            file.SaveAs(docPath); // Err: Can not access a close file.

            if (CommandName.Equals("Add"))
            {
                if (flag.Equals("P"))
                {
                    if (PursueList.Count > 0) fID = PursueList.Max(list => list.PursueID) + 1;
                    pursueTime = Convert.ToInt32(txtPTime.Value);
                }
                else if (flag.Equals("A"))
                {
                    if (AuctionList.Count > 0) fID = AuctionList.Max(list => list.AuctionID) + 1;
                    auctionOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);
                }
                //else if (flag.Equals("L"))
                //{
                //    fID = 0;
                //}
            }
            else if (CommandName.Equals("Edit"))
            {
                if (flag.Equals("P"))
                {
                    if (PursueList.Count > 0) fID = Convert.ToInt32(hidid_pursuemodal.Value);
                    pursueTime = Convert.ToInt32(txtPTime.Value);
                }
                else if (flag.Equals("A"))
                {
                    if (AuctionList.Count > 0) fID = Convert.ToInt32(hidAucID_auctionmodal.Value);
                    auctionOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);
                }
                //else if (flag.Equals("L"))
                //{
                //    fID = 0;
                //}
            }

            if (SubCommandName.Equals("Add"))
            {
                //===========trDocument===========
                doc.DocID = docID;
                doc.DocTypeID = docTypeID;
                doc.DocName = docName;
                doc.DocPath = docPath;
                doc.CreateBy = userCode;
                doc.CreateDateTime = date;
                doc.UpdateBy = userCode;
                doc.UpdateDateTime = date;
                attachDocList = AttachDocList;
                attachDocList.Add(doc);
                AttachDocList = attachDocList;
                //===========trDocumentMapping===========
                map.DocID = docID;
                map.LegalNo = LegalData.LegalNo;
                map.Flag = flag;
                map.FID = fID;
                map.CreateBy = userCode;
                map.CreateDateTime = date;
                map.UpdateBy = userCode;
                map.UpdateDateTime = date;
                docMappingList = DocMappingList;
                docMappingList.Add(map);
                DocMappingList = docMappingList;
                //===========vDocumentMapping===========
                view.DocID = docID;
                view.LegalNo = LegalData.LegalNo;
                view.DocTypeID = docTypeID;
                view.DocTypeName = docTypeName;
                view.DocName = docName;
                view.DocPath = docPath;
                view.Flag = flag;
                view.FID = fID;
                view.LegalNo = LegalData.LegalNo;
                view.PursueTime = pursueTime;
                view.AuctionOrder = auctionOrder;
                vattachDocList = vAttachDocList;
                vattachDocList.Add(view);
                vAttachDocList = vattachDocList;
            }

            if (flag.Equals("P"))
            {
                GetUpdateDocMapping(flag, pursueTime);
                UpdatePanel2.Update();
            }
            else if (flag.Equals("A"))
            {
                GetUpdateDocMapping(flag, auctionOrder);
                UpdatePanel7.Update();
            }
            //// do a below case on tab7
            //else if (flag.Equals("L"))
            //{
            //    if (CommandName.Equals("Add"))
            //    {
            //        GetUpdateDocMapping(flag, 0);
            //        UpdatePanel1.Update();
            //    }
            //    else
            //    {
            //        GetUpdateLegalDocByType(docTypeID);
            //        UpdatePanel13.Update();
            //    }

            //}
            ScriptManager.RegisterClientScriptBlock(UpdatePanel6, UpdatePanel6.GetType(), "hide-attachModal", "modalhideattach1();", true);
        }

        // Asset Modal
        public void ClearAssetModal()
        {
            hidAssetID.Value = string.Empty;
            ddlAssetType.SelectedIndex = 0;
            txtAddress_assetmodal.Value = string.Empty;
            txtDistrinct_assetmodal.Value = string.Empty;
            txtProvince_assetmodal.Value = string.Empty;
            txtZipcode_assetmodal.Value = string.Empty;

            int pursueTime = 0;
            GetUpdateAssetMapping(pursueTime);  // Set Gridview for empty data
            UpdatePanel3.Update();
        }

        public void ValidateSaveAssetModal()
        {
            if (Convert.ToInt32(ddlAssetType.SelectedValue) == -1) throw new Exception("กรุณากรอกข้อมูล \"ประเภท\"");
        }

        protected void btnSave_AssetModal_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateSaveAssetModal();

                int Time = 0;
                //===========trAsset===========
                int assID = 0;
                int assType = Convert.ToInt32(ddlAssetType.SelectedValue.Trim());
                string address = txtAddress_assetmodal.Value.Trim();
                string district = txtDistrinct_assetmodal.Value.Trim();
                string province = txtProvince_assetmodal.Value.Trim();
                string zipcode = txtZipcode_assetmodal.Value.Trim();
                //string user = "T2345";
                DateTime date = DateTime.Now;
                //===========trAssetMapping===========
                //string flag = "P"; // separate Pursue/Sequester
                int pursueID = 1;
                int pursueTime = Convert.ToInt32(txtPTime.Value);
                //===========vAssetMapping===========
                string assTypeName = ddlAssetType.SelectedItem.Text.Trim();

                trAsset ass = new trAsset();
                trAssetMapping map = new trAssetMapping();
                vAssetMapping view = new vAssetMapping();

                if (SubCommandName.Equals("Add"))
                {
                    //===========trAsset===========
                    assID = 1;
                    if (CommandName.Equals("Add"))
                    {
                        if (PursueList.Count > 0)
                        {
                            pursueID = PursueList.Max(list => list.PursueID) + 1;
                        }
                    }
                    else if (CommandName.Equals("Edit"))
                    {
                        pursueID = Convert.ToInt32(hidid_pursuemodal.Value);
                    }
                    if (AssetList.Count > 0) assID = AssetList.Max(asset => asset.AssetID) + 1;
                    ass.AssetID = assID;
                    ass.AssetTypeID = assType;
                    ass.Address = address;
                    ass.District = district;
                    ass.Province = province;
                    ass.ZipCode = zipcode;
                    ass.CreateBy = userCode;
                    ass.CreateDateTime = date;
                    ass.UpdateBy = userCode;
                    ass.UpdateDateTime = date;
                    assetList = AssetList;
                    assetList.Add(ass);
                    //===========trAssetMapping===========
                    map.AssetID = assID;
                    //map.Flag = flag;
                    map.PursueID = pursueID;
                    map.CreateBy = userCode;
                    map.CreateDateTime = date;
                    map.UpdateBy = userCode;
                    map.UpdateDateTime = date;
                    assetMappingList = AssetMappingList;
                    assetMappingList.Add(map);
                    //===========vAssetMapping===========
                    view.AssetID = assID;
                    view.AssetTypeID = assType;
                    view.AssetTypeName = assTypeName;
                    view.Address = address;
                    view.District = district;
                    view.Province = province;
                    view.ZipCode = zipcode;
                    //view.Flag = flag;
                    view.PursueID = pursueID;
                    view.LegalNo_pur = LegalData.LegalNo;
                    view.PursueTime = pursueTime;
                    vassetMappingList = vAssetMappingList;
                    vassetMappingList.Add(view);

                    Time = pursueTime;
                }
                else if (SubCommandName.Equals("Edit"))
                {
                    assID = Convert.ToInt32(hidAssetID.Value.Trim());
                    pursueID = Convert.ToInt32(txtPTime.Value);
                    //===========trAsset===========
                    ass.AssetID = assID;
                    ass.AssetTypeID = assType;
                    ass.Address = address;
                    ass.District = district;
                    ass.Province = province;
                    ass.ZipCode = zipcode;
                    ass.CreateBy = userCode;
                    ass.CreateDateTime = date;
                    ass.UpdateBy = userCode;
                    ass.UpdateDateTime = date;
                    assetList = AssetList;
                    assetList.RemoveAll(asset => asset.AssetID == assID);
                    assetList.Add(ass);
                    //===========trAssetMapping===========
                    map.AssetID = assID;
                    //map.Flag = flag;
                    map.PursueID = pursueID;
                    map.CreateBy = userCode;
                    map.CreateDateTime = date;
                    map.UpdateBy = userCode;
                    map.UpdateDateTime = date;
                    assetMappingList = AssetMappingList;
                    assetMappingList.RemoveAll(list => list.AssetID == assID);
                    assetMappingList.Add(map);
                    //===========vAssetMapping===========
                    view.AssetID = assID;
                    view.AssetTypeID = assType;
                    view.AssetTypeName = assTypeName;
                    view.Address = address;
                    view.District = district;
                    view.Province = province;
                    view.ZipCode = zipcode;
                    //view.Flag = flag;
                    view.PursueID = pursueID;
                    view.LegalNo_pur = LegalData.LegalNo;
                    view.PursueTime = pursueTime;
                    vassetMappingList = vAssetMappingList;
                    vassetMappingList.RemoveAll(list => list.AssetID == assID);
                    vassetMappingList.Add(view);

                    Time = pursueTime;
                }

                AssetList = assetList;
                AssetMappingList = assetMappingList;
                vAssetMappingList = vassetMappingList;

                GetUpdateAssetMapping(Time);
                UpdatePanel2.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "datetimepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel3, UpdatePanel3.GetType(), "hide-assetModal", "modalhideasset();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel3, UpdatePanel3.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        // Select Asset for Sequester modal
        protected void btnSave_AssetSeqModal_Click(object sender, EventArgs e)
        {
            int Time = -1;
            int sTime = Convert.ToInt32(txtSTime_seqmodal.Value);
            Time = sTime;
            int seqID = Convert.ToInt32(hidseqID_seqmodal.Value);

            foreach (GridViewRow row in gvSelectAssetSeq.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("CheckBox1") as CheckBox);
                    string spurID = (row.Cells[1].FindControl("hidPurID") as HiddenField).Value;
                    string sassetID = (row.Cells[1].FindControl("hidAssetID") as HiddenField).Value;
                    int purID = Convert.ToInt32(spurID);
                    int assetID = Convert.ToInt32(sassetID);

                    //if (SubCommandName.Equals("Add"))
                    //{
                    //    if (chkRow.Checked)
                    //    {
                    //        //===========trAssetMapping===========
                    //        trAssetMapping map = new trAssetMapping();
                    //        map = AssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                    //        map.SequesterID = seqID;

                    //        assetMappingList = AssetMappingList;
                    //        assetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                    //        assetMappingList.Add(map);
                    //        AssetMappingList = assetMappingList;
                    //        //===========vAssetMapping===========
                    //        vAssetMapping view = new vAssetMapping();
                    //        view = vAssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                    //        view.SequesterID = seqID;
                    //        view.SeqTime = Time;
                    //        view.LegalNo_seq = LegalNo;

                    //        vassetMappingList = vAssetMappingList;
                    //        vassetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                    //        vassetMappingList.Add(view);
                    //        vAssetMappingList = vassetMappingList;
                    //    }
                    //}
                    //else if (SubCommandName.Equals("Edit"))
                    //{
                    // if checked and not found => add to list
                    int countList = AssetMappingList.FindAll(M => (M.SequesterID == seqID) && (M.AssetID == assetID)).Count;
                    if (chkRow.Checked && countList < 1)
                    {
                        //===========trAssetMapping===========
                        trAssetMapping map = new trAssetMapping();
                        map = AssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                        map.SequesterID = seqID;

                        assetMappingList = AssetMappingList;
                        assetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                        assetMappingList.Add(map);
                        AssetMappingList = assetMappingList;
                        //===========vAssetMapping===========
                        vAssetMapping view = new vAssetMapping();
                        view = vAssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                        view.SequesterID = seqID;
                        view.SeqTime = Time;
                        view.LegalNo_seq = LegalData.LegalNo;

                        vassetMappingList = vAssetMappingList;
                        vassetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                        vassetMappingList.Add(view);
                        vAssetMappingList = vassetMappingList;
                    }
                    // if not checked and found => remove from list
                    if (!chkRow.Checked && AssetMappingList.FindAll(M => M.SequesterID == seqID && M.AssetID == assetID).Count > 0)
                    {
                        //===========trAssetMapping===========
                        trAssetMapping map = new trAssetMapping();
                        map = AssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                        map.SequesterID = 0;

                        assetMappingList = AssetMappingList;
                        assetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                        assetMappingList.Add(map);
                        AssetMappingList = assetMappingList;
                        //===========vAssetMapping===========
                        vAssetMapping view = new vAssetMapping();
                        view = vAssetMappingList.Find(M => M.PursueID == purID && M.AssetID == assetID);
                        view.SequesterID = 0;
                        view.SeqTime = 0;
                        view.LegalNo_seq = string.Empty;

                        vassetMappingList = vAssetMappingList;
                        vassetMappingList.RemoveAll(M => M.PursueID == purID && M.AssetID == assetID);
                        vassetMappingList.Add(view);
                        vAssetMappingList = vassetMappingList;
                    }
                    //}
                }
            }

            GetUpdateSelectAssetByTime(Time);
            UpdatePanel4.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel10, UpdatePanel10.GetType(), "hide-assetSeqModal", "modalhideassetseq();", true);
        }

        // Auction Modal
        public void ClearAuctionModal()
        {
            hidAucID_auctionmodal.Value = string.Empty;
            txtOrder_auctionmodal.Value = string.Empty;

            GetUpdateAuctionAssignByOrder(-1); //gvAuctionAssign
            GetUpdateAuctionDocByOrder(-1);//gvAuctionDoc
            GetUpdateAuctionAssetByOrder(-1); //gvAuctionAsset
        }

        //public void Bind_ddlAuctionAsset()
        //{
        //    //vAuctionAssetFromSeqList = new List<vAuctionAssetFromSeq>();
        //    DataTable dtddlAsset = new DataTable();
        //    dtddlAsset.Columns.Add("AssetID");
        //    dtddlAsset.Columns.Add("AssetDesc");
        //    if (vAssetMappingList.FindAll(list => !string.IsNullOrEmpty(list.LegalNo_seq) && list.LegalNo_seq.Contains(LegalData.LegalNo)).Count > 0)//!string.IsNullOrEmpty(list.LegalNo_seq) && 
        //    {
        //        vassetMappingList = vAssetMappingList.FindAll(list => !string.IsNullOrEmpty(list.LegalNo_seq) && list.LegalNo_seq.Equals(LegalData.LegalNo));
        //        foreach (vAssetMapping map in vassetMappingList)
        //        {
        //            if (AuctionAssetList.FindAll(list => list.AssetID == map.AssetID).Count == 0)
        //            {
        //                //vAuctionAssetFromSeq aucfromseq = new vAuctionAssetFromSeq();
        //                //aucfromseq.LegalNo = map.LegalNo_seq;
        //                //aucfromseq.AssetID = map.AssetID;
        //                //aucfromseq.AssetDesc = map.AssetTypeName + ": " + map.Address + "" + map.District + "" + map.Province + "" + map.ZipCode;
        //                //aucfromseq.SequesterID = map.SequesterID;
        //                //vAuctionAssetFromSeqList.Add(aucfromseq);

        //                string add = map.AssetTypeName;
        //                if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
        //                if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
        //                if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
        //                if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

        //                DataRow row = dtddlAsset.NewRow();
        //                row["AssetID"] = map.AssetID;
        //                row["AssetDesc"] = add;
        //                dtddlAsset.Rows.Add(row);

        //                foundassetdiv.Attributes.Add("style", "display:block");
        //                notfoundassetdiv.Attributes.Add("style", "display:none");
        //                btnSave_auctionasset.Visible = true;
        //            }
        //            else
        //            {
        //                int assetID = Convert.ToInt32(ddlAsset_auctionassetmodal.SelectedValue);
        //                int aucOrder = Convert.ToInt32(hidorder_auctionassetmodal.Value);

        //                string add = map.AssetTypeName;
        //                if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
        //                if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
        //                if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
        //                if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

        //                DataRow row = dtddlAsset.NewRow();
        //                row["AssetID"] = map.AssetID;
        //                row["AssetDesc"] = add;
        //                dtddlAsset.Rows.Add(row);

        //                foundassetdiv.Attributes.Add("style", "display:block");
        //                notfoundassetdiv.Attributes.Add("style", "display:none");
        //                btnSave_auctionasset.Visible = true;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        foundassetdiv.Attributes.Add("style", "display:none");
        //        notfoundassetdiv.Attributes.Add("style", "display:block");
        //        btnSave_auctionasset.Visible = false;
        //    }

        //    //ddlAsset_auctionassetmodal.DataSource = vAuctionAssetFromSeqList;
        //    ddlAsset_auctionassetmodal.DataSource = dtddlAsset;
        //    ddlAsset_auctionassetmodal.DataValueField = "AssetID";
        //    ddlAsset_auctionassetmodal.DataTextField = "AssetDesc";
        //    ddlAsset_auctionassetmodal.DataBind();
        //    UpdatePanel12.Update();
        //    ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "datepicker", "postback();", true);
        //}

        public void Bind_ddlAuctionAsset2()
        {
            DataTable dtddlAsset = new DataTable();
            dtddlAsset.Columns.Add("AssetID");
            dtddlAsset.Columns.Add("AssetDesc");

            vassetMappingList = vAssetMappingList.FindAll(list => !string.IsNullOrEmpty(list.LegalNo_seq) && list.LegalNo_seq.Equals(LegalData.LegalNo));
            auctionList = AuctionList;
            auctionAssetList = AuctionAssetList;

            foundassetdiv.Attributes.Add("style", "display:none");
            notfoundassetdiv.Attributes.Add("style", "display:block");
            btnSave_auctionasset.Visible = false;

            foreach (vAssetMapping map in vassetMappingList)
            {
                if (CommandName.Equals("Add"))
                {
                    //int assetID = Convert.ToInt32(ddlAsset_auctionassetmodal.SelectedValue);
                    //int aucOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);
                    if (SubCommandName.Equals("Add"))
                    {
                        if (auctionAssetList.FindAll(list => list.AssetID == map.AssetID && list.AuctionID == Convert.ToInt32(txtOrder_auctionmodal.Value)).Count == 0) // if => not found same AssetID
                        {
                            string add = map.AssetTypeName;
                            if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                            if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                            if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                            if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                            DataRow row = dtddlAsset.NewRow();
                            row["AssetID"] = map.AssetID;
                            row["AssetDesc"] = add;
                            dtddlAsset.Rows.Add(row);

                            foundassetdiv.Attributes.Add("style", "display:block");
                            notfoundassetdiv.Attributes.Add("style", "display:none");
                            btnSave_auctionasset.Visible = true;
                        }
                    }
                    else if (SubCommandName.Equals("Edit"))
                    {
                        string add = map.AssetTypeName;
                        if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                        if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                        if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                        if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                        DataRow row = dtddlAsset.NewRow();
                        row["AssetID"] = map.AssetID;
                        row["AssetDesc"] = add;
                        dtddlAsset.Rows.Add(row);

                        foundassetdiv.Attributes.Add("style", "display:block");
                        notfoundassetdiv.Attributes.Add("style", "display:none");
                        btnSave_auctionasset.Visible = true;
                    }
                }
                if (CommandName.Equals("Edit"))
                {
                    if (SubCommandName.Equals("Add"))
                    {
                        int assetID = Convert.ToInt32(ddlAsset_auctionassetmodal.SelectedValue);
                        int aucOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);

                        if (auctionAssetList.FindAll(list => list.AssetID == map.AssetID && list.AuctionID == Convert.ToInt32(txtOrder_auctionmodal.Value)).Count == 0) // if => not found same AssetID
                        {
                            string add = map.AssetTypeName;
                            if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                            if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                            if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                            if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                            DataRow row = dtddlAsset.NewRow();
                            row["AssetID"] = map.AssetID;
                            row["AssetDesc"] = add;
                            dtddlAsset.Rows.Add(row);

                            foundassetdiv.Attributes.Add("style", "display:block");
                            notfoundassetdiv.Attributes.Add("style", "display:none");
                            btnSave_auctionasset.Visible = true;
                        }
                    }
                    else if (SubCommandName.Equals("Edit"))
                    {
                        //if (auctionAssetList.FindAll(list => list.AssetID == map.AssetID).Count == 0) // if => not found same AssetID
                        //{
                        string add = map.AssetTypeName;
                        if (!string.IsNullOrEmpty(map.Address)) add = add + ": " + map.Address;
                        if (!string.IsNullOrEmpty(map.District)) add = add + ", " + map.District;
                        if (!string.IsNullOrEmpty(map.Province)) add = add + ", " + map.Province;
                        if (!string.IsNullOrEmpty(map.ZipCode)) add = add + ", " + map.ZipCode;

                        DataRow row = dtddlAsset.NewRow();
                        row["AssetID"] = map.AssetID;
                        row["AssetDesc"] = add;
                        dtddlAsset.Rows.Add(row);

                        foundassetdiv.Attributes.Add("style", "display:block");
                        notfoundassetdiv.Attributes.Add("style", "display:none");
                        btnSave_auctionasset.Visible = true;
                        //}
                    }
                }
            }

            ddlAsset_auctionassetmodal.DataSource = dtddlAsset;
            ddlAsset_auctionassetmodal.DataValueField = "AssetID";
            ddlAsset_auctionassetmodal.DataTextField = "AssetDesc";
            ddlAsset_auctionassetmodal.DataBind();
            UpdatePanel12.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "datepicker", "postback();", true);
        }

        protected void btnAddAuction_Click(object sender, EventArgs e)
        {
            ClearAuctionModal();
            txtOrder_auctionmodal.Disabled = false;

            CommandName = "Add";
            int hidAucID = 1;
            if (AuctionList.Count > 0)
            {
                hidAucID = AuctionList.Max(auc => auc.AuctionID) + 1;
            }
            hidAucID_auctionmodal.Value = hidAucID.ToString();
            UpdatePanel7.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "auctionModal", "modalshowauction();", true);
        }

        public void BindDDlAuctionTime()
        {
            // this binded function is bind by case
            // Add, is called from: btnAddAuctionAssign_auction_Click
            // Edit, is called from: gvAuction_RowCommand

            DataTable dtDdlAuctionTime = new DataTable();
            dtDdlAuctionTime.Columns.Add("ordervalue");
            dtDdlAuctionTime.Columns.Add("ordertext");
            vauctionAssignList = vAuctionAssignList.FindAll(A => A.AuctionID == Convert.ToInt32(hidAucID_auctionmodal.Value));
            for (int i = 1; i <= 4; i++)
            {
                bool same = false;
                same = vauctionAssignList.FindAll(A => A.ActionTime == i).Count > 0;
                if (!same || (same && SubCommandName.Equals("Edit")))
                {
                    DataRow row = dtDdlAuctionTime.NewRow();
                    row["ordervalue"] = i;
                    row["ordertext"] = i;
                    dtDdlAuctionTime.Rows.Add(row);
                }
            }

            txtOrder_auctionassignModal.Value = txtOrder_auctionmodal.Value;
            hidAucID_auctionassignModal.Value = hidAucID_auctionmodal.Value;
            if (SubCommandName.Equals("Add"))
            {
                if (vauctionAssignList.Count < 4)
                {
                    outoftimesdiv_aucassign.Attributes.Add("style", "display:none");
                    intimesdiv_aucassign.Attributes.Add("style", "display:block");
                    ddlAuctionTime_auctionAssignModal.DataSource = dtDdlAuctionTime;
                    ddlAuctionTime_auctionAssignModal.DataBind();
                    btnSave_auctionassignModal.Visible = true;
                }
                else
                {
                    outoftimesdiv_aucassign.Attributes.Add("style", "display:block");
                    intimesdiv_aucassign.Attributes.Add("style", "display:none");
                    btnSave_auctionassignModal.Visible = false;
                }
            }
            else
            {
                outoftimesdiv_aucassign.Attributes.Add("style", "display:none");
                intimesdiv_aucassign.Attributes.Add("style", "display:block");
                ddlAuctionTime_auctionAssignModal.DataSource = dtDdlAuctionTime;
                ddlAuctionTime_auctionAssignModal.DataBind();
                btnSave_auctionassignModal.Visible = true;
            }
        }

        protected void btnAddAuctionAssign_auction_Click(object sender, EventArgs e)
        {
            SubCommandName = "Add";

            ClearAuctionAssignModal();
            BindDDlAuctionTime();

            UpdatePanel11.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel11, UpdatePanel11.GetType(), "datepicker", "postback();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "auctionAssignModal", "modalshowAuctionAssign();", true);
        }

        protected void btnAddauctionAsset_auction_Click(object sender, EventArgs e)
        {
            vAssetMappingList = vAssetMappingList;
            AssetMappingList = AssetMappingList;
            SubCommandName = "Add";

            ClearAuctionAssetModal();
            //Bind_ddlAuctionAsset();
            Bind_ddlAuctionAsset2();
            hidAuctionid_modal.Value = hidAucID_auctionmodal.Value;
            hidorder_auctionassetmodal.Value = txtOrder_auctionmodal.Value;
            ddlAsset_auctionassetmodal.Enabled = true;
            UpdatePanel12.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "datepicker", "postback();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "auctionAssetModal", "modalshowAuctionAsset();", true);
        }

        public void ValidateAuctionModal()
        {
            int aucid = Convert.ToInt32(hidAucID_auctionmodal.Value);
            int auctime = Convert.ToInt32(txtOrder_auctionmodal.Value);
            if (string.IsNullOrEmpty(txtOrder_auctionmodal.Value)) throw new Exception("กรุุณากรอก \"ลำดับการบันทึก\"");
            else
            {
                if (CommandName.Equals("Add"))
                {
                    //int aucid = Convert.ToInt32(hidAucID_auctionmodal.Value);
                    //int auctime = Convert.ToInt32(txtOrder_auctionmodal.Value);
                    trAuctionAssign assign = AuctionAssignList.Find(s => s.ActionTime == auctime && s.AuctionID == aucid);
                    if (assign != null)
                    {
                        List<trAuction> auction = AuctionList.FindAll(s => s.AuctionID == assign.AuctionID);
                        if (auction.Count > 0) throw new Exception("ข้อมูล\"ลำดับการบันทึก\" ซ้ำ กรุณาเลือกใหม่");
                    }
                }
            }
            auctionAssignList = AuctionAssignList.FindAll(list => list.AuctionID == aucid);
            if (auctionAssignList.Count == 0) throw new Exception("กรุุณาเพิ่มรายการ \"นัดการขายทอดตลาด\"");
        }

        protected void btnSave_auctionModal_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateAuctionModal();

                //string user = "T12345";
                DateTime updatedate = DateTime.Now;
                int aucID = Convert.ToInt32(hidAucID_auctionmodal.Value);
                int aucOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);

                trAuction auc = new trAuction();
                auc.AuctionID = aucID;
                auc.AuctionOrder = aucOrder;
                auc.LegalNo = LegalData.LegalNo;
                auc.CreateBy = userCode;
                auc.CreateDateTime = updatedate;
                auc.UpdateBy = userCode;
                auc.UpdateDateTime = updatedate;

                if (CommandName.Equals("Add"))
                {
                    //=============== trAuction ===============
                    AuctionList.Add(auc);
                }
                else if (CommandName.Equals("Edit"))
                {
                    //=============== trAuction ===============
                    auctionList = AuctionList;
                    auctionList.RemoveAll(A => A.AuctionID == aucID && A.AuctionOrder == aucOrder);
                    auctionList.Add(auc);
                    AuctionList = auctionList;
                }
                GetUpdateAuction();
                UpdatePanel1.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "hideAuctionModal", "modalhideauction();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel7, UpdatePanel7.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        // AuctionAssign Modal
        public void ClearAuctionAssignModal()
        {
            hidAucID_auctionassignModal.Value = string.Empty;
            txtOrder_auctionassignModal.Value = string.Empty;
            txtDate_auctionAssignModal.Value = string.Empty;
            txtRemark_auctionAssignModal.Value = string.Empty;
        }

        public void ValidateAuctionAssignModal()
        {
            //txtOrder_auctionassignModal.Value; always has a value.
            //ddlAuctionTime_auctionAssignModal.SelectTedValue; always has a value.
            if (string.IsNullOrEmpty(txtDate_auctionAssignModal.Value)) throw new Exception("กรุุณากรอก \"วันที่ประกาศขาย\"");
            if (string.IsNullOrEmpty(txtRemark_auctionAssignModal.Value)) throw new Exception("กรุุณากรอก \"หมายเหตุ\"");
        }

        protected void btnSave_auctionassignModal_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateAuctionAssignModal();

                int aucID = Convert.ToInt32(hidAucID_auctionassignModal.Value.Trim());
                int aucOrder = Convert.ToInt32(txtOrder_auctionassignModal.Value.Trim());
                int aucAssign = Convert.ToInt32(ddlAuctionTime_auctionAssignModal.SelectedValue.Trim());
                DateTime aucDate = Convert.ToDateTime(txtDate_auctionAssignModal.Value.Trim());
                string remark = txtRemark_auctionAssignModal.Value.Trim();
                DateTime updateDate = DateTime.Now;
                //string user = "T12541";

                //DateTime lastAssignDate = AuctionAssignList.Max(a => a.AuctionDate);
                //if (lastAssignDate > aucDate && SubCommandName.Equals("Add")) throw new Exception("กรุณาเลือก 'วันที่ประกาศขาย' ใหม่");

                trAuctionAssign assign = new trAuctionAssign();
                vAuctionAssign vassign = new vAuctionAssign();

                if (SubCommandName.Equals("Add"))
                {
                    //===========trAuctionAssign===========
                    assign.AuctionID = aucID;
                    assign.ActionTime = aucAssign;
                    assign.AuctionDate = aucDate;
                    assign.Remark = remark;
                    assign.CreateBy = userCode;
                    assign.CreateDateTime = updateDate;
                    assign.UpdateBy = userCode;
                    assign.UpdateDateTime = updateDate;

                    auctionAssignList = AuctionAssignList;
                    auctionAssignList.Add(assign);
                    AuctionAssignList = auctionAssignList;
                    //===========vAuctionAssign===========
                    vassign.AuctionID = aucID;
                    vassign.LegalNo = LegalData.LegalNo;
                    vassign.AuctionOrder = aucOrder;
                    vassign.ActionTime = aucAssign;
                    vassign.AuctionDate = aucDate;
                    vassign.Remark = remark;

                    vauctionAssignList = vAuctionAssignList;
                    vauctionAssignList.Add(vassign);
                    vAuctionAssignList = vauctionAssignList;
                }
                else if (SubCommandName.Equals("Edit"))
                {
                    //===========trAuctionAssign===========
                    assign.AuctionID = aucID;
                    assign.ActionTime = aucAssign;
                    assign.AuctionDate = aucDate;
                    assign.Remark = remark;
                    assign.CreateBy = AuctionAssignList.Find(A => A.AuctionID == aucID && A.ActionTime == aucAssign).CreateBy;
                    assign.CreateDateTime = AuctionAssignList.Find(A => A.AuctionID == aucID && A.ActionTime == aucAssign).CreateDateTime;
                    assign.UpdateBy = userCode;
                    assign.UpdateDateTime = updateDate;

                    auctionAssignList = AuctionAssignList;
                    auctionAssignList.RemoveAll(A => A.AuctionID == aucID && A.ActionTime == aucAssign);
                    auctionAssignList.Add(assign);
                    AuctionAssignList = auctionAssignList;
                    //===========vAuctionAssign===========
                    vassign.AuctionID = aucID;
                    vassign.LegalNo = LegalData.LegalNo;
                    vassign.AuctionOrder = aucOrder;
                    vassign.ActionTime = aucAssign;
                    vassign.AuctionDate = aucDate;
                    vassign.Remark = remark;

                    vauctionAssignList = vAuctionAssignList;
                    vauctionAssignList.RemoveAll(A => A.AuctionID == aucID && A.ActionTime == aucAssign);
                    vauctionAssignList.Add(vassign);
                    vAuctionAssignList = vauctionAssignList;
                }
                GetUpdateAuctionAssignByOrder(aucOrder); // update trAuctionAssign and vAuctionAssign
                UpdatePanel7.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel11, UpdatePanel11.GetType(), "hide-auctionAssignModal", "modalhideAuctionAssign();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel11, UpdatePanel11.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }

        // AuctionAsset Modal
        public void ClearAuctionAssetModal()
        {
            hidAuctionid_modal.Value = string.Empty;
            hidorder_auctionassetmodal.Value = string.Empty;
            //ddlAsset_auctionassetmodal.SelectedIndex = 0; // comment for use fn. Bind_ddlAuctionAsset();
            txtOtherDate_modal.Value = string.Empty;
            txtOtherAmt_modal.Value = string.Empty;
            txtBankBuy_modal.Value = string.Empty;
            txtBankAmt_modal.Value = string.Empty;
            txtGetMoneyDate.Value = string.Empty;
            txtGetMoneyAmt.Value = string.Empty;
            txtPrice_modal.Value = string.Empty;
            txtPriceBoard_modal.Value = string.Empty;
            txtPriceApprove.Value = string.Empty;
            txtremark_modal.Value = string.Empty;
        }

        protected void ddlAsset_auctionassetmodal_SelectedIndexChanged(object sender, EventArgs e)
        {
            int auctionID = Convert.ToInt32(hidAuctionid_modal.Value.Trim());
            int assetID = Convert.ToInt32(ddlAsset_auctionassetmodal.SelectedValue.Trim());

            vAuctionAsset auc = vAuctionAssetList.Find(A => A.AuctionID == auctionID && A.AssetID == assetID);

            hidAuctionid_modal.Value = auc.AuctionID.ToString();
            ddlAsset_auctionassetmodal.SelectedValue = auc.AssetID.ToString();
            hidorder_auctionassetmodal.Value = auc.AuctionOrder.ToString();
            //hidTypeID.Value = auc.AssetTypeID.ToString();
            //hidTypeName.Value = auc.AssetTypeName.ToString();
            //hidAddress.Value = auc.Address.ToString();
            //hidDistrict.Value = auc.District.ToString();
            //hidProvince.Value = auc.Province.ToString();
            //hidZipcode.Value = auc.ZipCode.ToString();
            txtOtherDate_modal.Value = auc.AnotherBuyDate.ToString();
            txtOtherAmt_modal.Value = auc.AnotherBuyAmt.ToString();
            txtBankBuy_modal.Value = auc.BankBuyDate.ToString();
            txtBankAmt_modal.Value = auc.BankBuyAmt.ToString();
            txtGetMoneyDate.Value = auc.GotMoneyDate.ToString();
            txtGetMoneyAmt.Value = auc.AccountPerAmt.ToString();
            txtPrice_modal.Value = auc.Price.ToString();
            txtPriceBoard_modal.Value = auc.PriceOnBoard.ToString();
            txtPriceApprove.Value = auc.PriceOnBoardSold.ToString();
            txtremark_modal.Value = auc.Remark.ToString();

            UpdatePanel12.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "datepicker", "postback();", true);
        }

        protected void btnSave_auctionasset_Click(object sender, EventArgs e)
        {
            try
            {
                int auctionID = 0;
                int assetID = 0;
                int auctionorder = 0;
                DateTime? otherBuyDate = null;
                DateTime? bankBuyDate = null;
                DateTime? gotMoneyDate = null;
                decimal otherBuyAmt = 0;
                decimal bankBuyAmt = 0;
                decimal gotMoneyAmt = 0;
                decimal price = 0;
                decimal priceOnBoard = 0;
                decimal priceOnBoardSold = 0;
                string remark = string.Empty;
                string flag = "A"; // A:auction, P:pursue, S:sequester
                int assetTypeID = 0;
                string assetTypeName = string.Empty;
                string address = string.Empty;
                string district = string.Empty;
                string province = string.Empty;
                string zipcode = string.Empty;
                DateTime updateDate = DateTime.Now;
                //string user = "T12541";
                auctionID = Convert.ToInt32(hidAuctionid_modal.Value.Trim());
                assetID = Convert.ToInt32(ddlAsset_auctionassetmodal.SelectedValue.Trim());
                auctionorder = Convert.ToInt32(hidorder_auctionassetmodal.Value.Trim());
                assetTypeID = vAssetMappingList.First(A => A.AssetID == assetID).AssetTypeID;
                assetTypeName = vAssetMappingList.First(A => A.AssetID == assetID).AssetTypeName;
                address = vAssetMappingList.First(A => A.AssetID == assetID).Address;
                district = vAssetMappingList.First(A => A.AssetID == assetID).District;
                province = vAssetMappingList.First(A => A.AssetID == assetID).Province;
                zipcode = vAssetMappingList.First(A => A.AssetID == assetID).ZipCode;
                if (!string.IsNullOrEmpty(txtOtherDate_modal.Value)) otherBuyDate = Convert.ToDateTime(txtOtherDate_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtOtherAmt_modal.Value)) otherBuyAmt = Convert.ToDecimal(txtOtherAmt_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtBankBuy_modal.Value)) bankBuyDate = Convert.ToDateTime(txtBankBuy_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtBankAmt_modal.Value)) bankBuyAmt = Convert.ToDecimal(txtBankAmt_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtGetMoneyDate.Value)) gotMoneyDate = Convert.ToDateTime(txtGetMoneyDate.Value.Trim());
                if (!string.IsNullOrEmpty(txtGetMoneyAmt.Value)) gotMoneyAmt = Convert.ToDecimal(txtGetMoneyAmt.Value.Trim());
                if (!string.IsNullOrEmpty(txtPrice_modal.Value)) price = Convert.ToDecimal(txtPrice_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtPriceBoard_modal.Value)) priceOnBoard = Convert.ToDecimal(txtPriceBoard_modal.Value.Trim());
                if (!string.IsNullOrEmpty(txtPriceApprove.Value)) priceOnBoardSold = Convert.ToDecimal(txtPriceApprove.Value.Trim());
                if (!string.IsNullOrEmpty(txtremark_modal.Value)) remark = txtremark_modal.Value.Trim();

                trAuctionAsset asset = new trAuctionAsset();
                vAuctionAsset vasset = new vAuctionAsset();
                //===========trAuctionAsset===========
                asset.AuctionID = auctionID;
                asset.AssetID = assetID;
                asset.AnotherBuyDate = otherBuyDate;
                asset.AnotherBuyAmt = otherBuyAmt;
                asset.BankBuyDate = bankBuyDate;
                asset.BankBuyAmt = bankBuyAmt;
                asset.GotMoneyDate = gotMoneyDate;
                asset.AccountPerAmt = gotMoneyAmt;
                asset.Price = price;
                asset.PriceOnBoard = priceOnBoard;
                asset.PriceOnBoardSold = priceOnBoardSold;
                asset.Remark = remark;
                asset.Flag = flag;
                asset.CreateBy = userCode;
                asset.CreateDateTime = updateDate;
                asset.UpdateBy = userCode;
                asset.UpdateDateTime = updateDate;
                //===========vAuctionAsset===========
                vasset.AuctionID = auctionID;
                vasset.AssetID = assetID;
                vasset.LegalNo = LegalData.LegalNo;
                vasset.AuctionOrder = auctionorder;
                vasset.AssetTypeID = assetTypeID;
                vasset.AssetTypeName = assetTypeName;
                vasset.Address = address;
                vasset.District = district;
                vasset.Province = province;
                vasset.ZipCode = zipcode;
                vasset.AnotherBuyDate = otherBuyDate;
                vasset.AnotherBuyAmt = otherBuyAmt;
                vasset.BankBuyDate = bankBuyDate;
                vasset.BankBuyAmt = bankBuyAmt;
                vasset.GotMoneyDate = gotMoneyDate;
                vasset.AccountPerAmt = gotMoneyAmt;
                vasset.Price = price;
                vasset.PriceOnBoard = priceOnBoard;
                vasset.PriceOnBoardSold = priceOnBoardSold;
                vasset.Remark = remark;
                vasset.Flag = flag;

                if (SubCommandName.Equals("Add"))
                {
                    //===========trAuctionAsset===========
                    auctionAssetList = AuctionAssetList;
                    auctionAssetList.Add(asset);
                    AuctionAssetList = auctionAssetList;
                    //===========vAuctionAsset===========
                    vauctionAssetList = vAuctionAssetList;
                    vauctionAssetList.Add(vasset);
                    vAuctionAssetList = vauctionAssetList;
                }
                else if (SubCommandName.Equals("Edit"))
                {
                    //===========trAuctionAsset===========
                    asset.CreateBy = AuctionAssetList.Find(A => A.AuctionID == auctionID && A.AssetID == assetID).CreateBy;
                    asset.CreateDateTime = AuctionAssetList.Find(A => A.AuctionID == auctionID && A.AssetID == assetID).CreateDateTime;

                    auctionAssetList = AuctionAssetList;
                    auctionAssetList.RemoveAll(A => A.AuctionID == auctionID && A.AssetID == assetID);
                    auctionAssetList.Add(asset);
                    AuctionAssetList = auctionAssetList;
                    //===========vAuctionAsset===========
                    vauctionAssetList = vAuctionAssetList;
                    vauctionAssetList.RemoveAll(A => A.AuctionID == auctionID && A.AssetID == assetID);
                    vauctionAssetList.Add(vasset);
                    vAuctionAssetList = vauctionAssetList;
                }
                //GetUpdateAuctionAsset();
                GetUpdateAuctionAssetByOrder(auctionorder);
                UpdatePanel7.Update();
                UpdatePanel12.Update();
                ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "hide-auctionAssetModal", "modalhideAuctionAsset();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel12, UpdatePanel12.GetType(), "validateAlert", "validateAlert('" + ex.Message + "');", true);
            }
        }
        #endregion
    }

    public class Tab5Data
    {
        public trLegal LegalData { get; set; }
        public List<trDocument> DocumentList { get; set; }
        public List<trDocumentMapping> DocumentMappingList { get; set; }
        public List<vDocumentMapping> vDocumentList { get; set; }
        public List<trAsset> AssetList { get; set; }
        public List<trAssetMapping> AssetMappingList { get; set; }
        public List<vAssetMapping> vAssetMappingList { get; set; }
        public List<trPursue> PursueList { get; set; }
        public List<trSequester> SequesterList { get; set; }
        public List<trMortgage> MortgageList { get; set; }
        public List<trAverage> AverageList { get; set; }
        public List<trAuction> AuctionList { get; set; }
        public List<trAuctionAssign> AuctionAssignList { get; set; }
        public List<trAuctionAsset> AuctionAssetList { get; set; }
        public List<trRetain> RetainList { get; set; }
        public List<trRequestSequester> RequestSequesterList { get; set; }
    }
}