﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab7.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab7" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<script type="text/javascript">
    function validateAlert(v_text) {
        alert(v_text);
    }
    function modalshowdoclist() {
        $('#doclistModal').modal('toggle');
    }
    function modalshowattach() {
        $('#test').modal('toggle');
        //window.showModalDialog('UploadFile.aspx'); // support IE
        //window.open('UploadFile.aspx');
    }
    function modalhidedoclist() {
        $('#doclistModal').modal('hide');
    }
    function modalhideattach() {
        $('#test').modal('hide');
    }
    function modalhideattach() {
        $('#test').modal('hide');
    }
</script>

<asp:Panel ID="Panel1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row justify-content-center">
                <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                <div class="col-sm-12">
                    <div class="row col-md-12 col-lg-12">
                        <asp:Button ID="btnadd_attach" runat="server" Text="Add" CssClass="btn btn-info btn-sm" OnClick="btnadd_attach_Click" CausesValidation="false"/>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12">
                            <asp:GridView ID="gvAttach" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover " GridLines="None" RowStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                OnRowDataBound="gvAttach_RowDataBound" OnPageIndexChanging="gvAttach_PageIndexChanging"
                                OnRowCommand="gvAttach_RowCommand" OnRowDeleting="gvAttach_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ประเภท">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidDocID" runat="server" Value='<%#Eval("DocID")%>' />
                                            <asp:Label ID="lbLegalStatus" runat="server" Text='<%#Eval("DocTypeName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField >
                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="ชื่อเอกสาร" DataField="DocName" />
                                    <asp:TemplateField >
                                        <ItemTemplate >
                                            <asp:HyperLink ID="lnkPcName" runat="server" Text="View" Target="_blank" CssClass="btn btn-fill btn-info btn-sm"
                                                CommandName="ViewCommand" NavigateUrl='<%# "~/Pages/Helper/PDFPage.aspx?val=" + Eval("DocID") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                        <ItemTemplate>                                            
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%-- Document Modal --%>
    <div id="test" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" >
                    <ContentTemplate>
                        <div class="modal-header">
                            เอกสารแนบ
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    ประเภทเอกสาร
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidFlag_attachmodal" runat="server"/>
                                        <asp:DropDownList ID="ddlDoctype_attachmodal" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">เลือกเอกสาร :</label>
                                <div class="col-sm-8">
                                    <iframe id="iRender" runat="server" src="~/Pages/Helper/UploadFile.aspx" style="width:100%;border:0;"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnsave_attachdoc" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnsave_attachdoc_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Panel>
