﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab62.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab62" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>



<asp:Panel ID="Panel1" runat="server">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
            <asp:Button ID="btnadd_racha" runat="server" Text="Add" CssClass="btn btn-sm btn-info" OnClick="btnadd_racha_Click" />
            <%--<asp:GridView ID="gvRacha" runat="server"></asp:GridView>--%>
            <asp:GridView ID="gvRacha" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" ShowHeaderWhenEmpty="true"
                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                Width="100%" HeaderStyle-HorizontalAlign="Center"
                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                OnRowDataBound="gvRacha_RowDataBound"
                OnRowCommand="gvRacha_RowCommand" OnRowEditing="gvRacha_RowEditing1"
                OnRowUpdating="gvRacha_RowUpdating" OnRowCancelingEdit="gvRacha_RowCancelingEdit" OnRowDeleting="gvRacha_RowDeleting">
                <Columns>
                    <asp:TemplateField ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandName="Edit" runat="server">Edit</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandName="Update" runat="server">Update</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server">Cancel</asp:LinkButton>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียม">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidFeeID_shw" runat="server" Value='<%#Eval("FeeID")%>' />
                            <asp:HiddenField ID="hidFeeTypeID" runat="server" Value='<%#Eval("CostTypeID")%>' />
                            <asp:Label ID="lbFeeType" runat="server" Text='<%#Eval("CostTypeName")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:HiddenField ID="hidFeeID_edt" runat="server" Value='<%#Eval("FeeID")%>' />
                            <asp:DropDownList ID="ddlFeeType" runat="server" CssClass="form-control"
                                DataValueField="CostTypeID" DataTextField="CostTypeName">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียมศาลจ่าย">
                        <ItemTemplate>
                            <asp:Label ID="lbCourtPay" runat="server" Text='<%#String.Format("{0:f2}", Eval("CourtPayAmt"))%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCourtPay" runat="server" CssClass="form-control" Text='<%#Eval("CourtPayAmt")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ค่าธรรมเนียมศาลคืน">
                        <ItemTemplate>
                            <asp:Label ID="lbCourtRefund" runat="server" Text='<%#String.Format("{0:f2}", Eval("CourtRefundAmt"))%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCourtRefund" runat="server" CssClass="form-control" Text='<%#Eval("CourtRefundAmt")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่บันทึกค่าธรรมเนียมศาลคืน">
                        <ItemTemplate>
                            <asp:Label ID="lbCourtRefundDate" runat="server" Text='<%#Eval("CourtRefundDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCourtRefundDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("CourtRefundDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Cheque ธนาคาร / รายการโอนคืน">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidBankCode" runat="server" Value='<%#Eval("BankCode")%>' />
                            <asp:Label ID="lbBankName" runat="server" Text='<%#Eval("BankName")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBank" runat="server" CssClass="form-control"
                                DataValueField="Code" DataTextField="BankName">
                                <asp:ListItem Value="">-กรุณาเลือก-</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ Cheque">
                        <ItemTemplate>
                            <asp:Label ID="lbChequeDate" runat="server" Text='<%#Eval("ChequeDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtChequeDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("ChequeDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Cheque No.">
                        <ItemTemplate>
                            <asp:Label ID="lbChequeNo" runat="server" Text='<%#Eval("ChequeNo")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control" Text='<%#Eval("ChequeNo")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="ธนาคารสาขา">
                        <ItemTemplate>
                            <asp:Label ID="lbBranch" runat="server" Text='<%#Eval("BankBranch")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBranch" runat="server" CssClass="form-control" Text='<%#Eval("BankBranch")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ส่ง Cheque เรียกเก็บ">
                        <ItemTemplate>
                            <asp:Label ID="lbSendChqDate" runat="server" Text='<%#Eval("SendChqDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSendChqDate" runat="server" CssClass="datepicker form-control" Text='<%#Eval("SendChqDate", "{0:dd/MM/yyyy}")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Panel>
