﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab1.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab1" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<asp:Panel ID="Panel1" runat="server">
    <div class="row justify-content-center">
        <div class="col-lg-11 col-md-11 col-11 ">
            <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันรับเรื่อง :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateGotRequest" runat="server" class="datepicker form-control" disabled />
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5>
                            <span class="tim-note">วันส่งเบิกเอกสาร :</span>
                        </h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateSentReqDoc" runat="server" class="datepicker form-control" disabled/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">ทนายใน :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <%--<asp:DropDownList ID="ddlAttorneyType" runat="server" AutoPostBack="true"
                            CssClass="form-control" OnSelectedIndexChanged="ddlAttorneyType_SelectedIndexChanged">
                            <asp:ListItem Value="">- กรุณาเลือก -</asp:ListItem>
                        </asp:DropDownList>--%>

                        <asp:DropDownList ID="ddlAttorneyType" runat="server" AutoPostBack="true"
                            CssClass="form-control" OnSelectedIndexChanged="ddlAttorneyType_SelectedIndexChanged">
                            <asp:ListItem Value="">- กรุณาเลือก -</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5>
                            <span class="tim-note">ทนายนอก :</span>
                        </h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <asp:DropDownList ID="ddlLawyer" runat="server" CssClass="form-control">
                            <asp:ListItem Value="">- กรุณาเลือก -</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันออกหนังสือทวงถาม :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateSentCallDoc" runat="server" class="datepicker form-control" disabled/>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5>
                            <span class="tim-note">วันรับเอกสาร <span style="color: red">*</span> :</span>
                        </h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateGetDoc" runat="server" class="datepicker form-control"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5>
                            <span class="tim-note">วันส่งเรื่องให้ทนาย :</span>
                        </h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateSentCaseToLawyer" runat="server" class="datepicker form-control" disabled/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
