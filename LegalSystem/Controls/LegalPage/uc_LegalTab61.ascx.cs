﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab61 : System.Web.UI.UserControl
    {
        #region Properties
        public LegalDbContext context;
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }
        public string LegalNo
        {
            get
            {
                return (string)Session["LegalNo"];
            }
            set
            {

                Session["LegalNo"] = value;
            }
        }
        //public trLegal LegalData
        //{
        //    get
        //    {
        //        return (trLegal)Session["LegalData"];
        //    }
        //    set
        //    {
        //        Session["LegalData"] = value;
        //    }
        //}

        private List<trCost> costList;
        public List<trCost> CostList
        {
            get
            {
                return (List<trCost>)Session["CostList"];
            }
            set
            {
                Session["CostList"] = value;
            }
        }

        private List<vCost> vcostList;
        public List<vCost> vCostList
        {
            get
            {
                return (List<vCost>)Session["vCostList"];
            }
            set
            {
                Session["vCostList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{
            //    context = LegalSession._legalDbContext;
            //}
        }
        public void SetUI(string legalno)
        {
            LegalNo = legalno;

            //============trCost============
            trCostRep costrep = new trCostRep();
            costList = costrep.GetAllCost(LegalNo);
            CostList = costList;
            //============vCost============
            vCostRep vcost_rep = new vCostRep();
            vcostList = vcost_rep.GetAllCost(LegalNo);
            vCostList = vcostList.OrderBy(list => list.CostTypeID).ThenBy(list => list.CostID).ToList();
            if (vCostList.Count == 0) InitialCost();
            gvCost.DataSource = vCostList;
            gvCost.DataBind();

            decimal sumcost = 0;
            decimal sumpay = 0;
            decimal sumreceive = 0;
            sumcost = CostList.Sum(c => c.CostAmt);
            sumpay = CostList.Sum(c => c.PayAmt);
            sumreceive = CostList.Sum(c => c.Income);
            lbCostAmt.Text = String.Format("{0:0.00}", sumcost);
            lbPayAmt.Text = String.Format("{0:0.00}", sumpay);
            lbReceiveAmt.Text = String.Format("{0:0.00}", sumreceive);
        }
        public List<trCost> GetUI()
        {
            return CostList;
        }
        public string ValidateTab()
        {
            return string.Empty;
        }
        public void InitialCost()
        {
            List<vCostType> typeList = new List<vCostType>();
            using (vCostTypeRep rep = new vCostTypeRep())
            {
                typeList = rep.GetCostTypeByFlag("C", "F");
            }

            int costID = 1;
            int costTypeID = 0;
            int attTypeID = 0;
            string costTypeName = string.Empty;
            string attTypeName = string.Empty;
            string tflag = string.Empty;
            string sflag = string.Empty;
            string costDesc = string.Empty;
            string adviseNo = string.Empty;
            //string user = "T65007";
            decimal costAmt = 0;
            DateTime updatedate = DateTime.Now;
            using (MaxValueRep rep = new MaxValueRep())
            {
                costAmt = rep.GetMaxValueByMaxName("DefaultCost").MaxAmt;
            }

            costList = CostList;
            vcostList = vCostList;
            foreach (vCostType item in typeList)
            {
                if (vcostList.Count > 0)
                {
                    costID = vcostList.Max(c => c.CostID) + 1;
                }
                costTypeID = item.CostTypeID;
                attTypeID = item.AttorneyTypeID;
                costTypeName = item.CostTypeName;
                attTypeName = item.AttorneyTypeName;
                tflag = item.TFlag;
                sflag = item.SFlag;

                //=========trCost=========
                trCost clist = new trCost();
                clist.CostID = costID;
                clist.LegalNo = LegalNo;
                clist.CostTypeID = costTypeID;
                clist.AttorneyTypeID = attTypeID;
                clist.CostDate = updatedate;
                clist.PayDate = updatedate;
                clist.ReceiveDate = updatedate;
                clist.CostAmt = costAmt;
                clist.PayAmt = costAmt;
                clist.Income = 0;
                clist.CostDesc = costDesc;
                clist.AdviseNo = adviseNo;
                clist.CreateBy = userCode;
                clist.CreateDateTime = updatedate;
                clist.UpdateBy = userCode;
                clist.UpdateDateTime = updatedate;

                costList.Add(clist);
                //=========vCost=========
                vCost vlist = new vCost();
                vlist.CostID = costID;
                vlist.LegalNo = LegalNo;
                vlist.TFlag = tflag;
                vlist.SFlag = sflag;
                vlist.CostTypeID = costTypeID;
                vlist.AttorneyTypeID = attTypeID;
                vlist.CostTypeName = costTypeName;
                vlist.AttorneyTypeName = attTypeName;
                vlist.CostDate = updatedate;
                vlist.PayDate = updatedate;
                vlist.ReceiveDate = updatedate;
                vlist.CostAmt = costAmt;
                vlist.PayAmt = costAmt;
                vlist.Income = 0;
                vlist.CostDesc = costDesc;
                vlist.AdviseNo = adviseNo;

                vcostList.Add(vlist);
            }
            CostList = costList;
            vCostList = vcostList;

            decimal sumcost = 0;
            decimal sumpay = 0;
            decimal sumreceive = 0;
            sumcost = CostList.Sum(c => c.CostAmt);
            sumpay = CostList.Sum(c => c.PayAmt);
            sumreceive = CostList.Sum(c => c.Income);
            lbCostAmt.Text = String.Format("{0:0.00}", sumcost);
            lbPayAmt.Text = String.Format("{0:0.00}", sumpay);
            lbReceiveAmt.Text = String.Format("{0:0.00}", sumreceive);
        }
        public void GetUpdateCost()
        {
            gvCost.DataSource = vCostList.OrderBy(C => C.CostID).ToList();
            gvCost.DataBind();
        }

        #region GridView Event
        protected void gvCost_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = e.RowIndex;
            HiddenField hidCostID_shw = gvCost.Rows[index].FindControl("hidCostID_shw") as HiddenField;
            int costID = Convert.ToInt32(hidCostID_shw.Value);

            //===========MortgageList===========
            costList = CostList;
            costList.RemoveAll(list => list.CostID == costID);
            CostList = costList;
            //===========vMortgageList===========
            vcostList = vCostList;
            vcostList.RemoveAll(list => list.CostID == costID);
            vCostList = vcostList;

            CommandName = "Delete";
            GetUpdateCost();
            //UpdatePanel1.Update();
            //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datepicker", "postback();", true);
        }

        protected void gvCost_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Session["PageIndex"] = e.NewPageIndex;
            gvCost.PageIndex = e.NewPageIndex;
            gvCost.DataSource = vCostList;
            gvCost.DataBind();
        }

        protected void gvCost_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Edit")) CommandName = e.CommandName;

            switch (e.CommandName)
            {
                case "Update":
                    #region get data from GridView
                    int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                    HiddenField hidCostID_edt = gvCost.Rows[index].FindControl("hidCostID_edt") as HiddenField;
                    DropDownList ddlCostType = gvCost.Rows[index].FindControl("ddlCostType") as DropDownList;
                    TextBox txtCostDate = gvCost.Rows[index].FindControl("txtCostDate") as TextBox;
                    TextBox txtCostAmt = gvCost.Rows[index].FindControl("txtCostAmt") as TextBox;
                    TextBox txtCostDesc = gvCost.Rows[index].FindControl("txtCostDesc") as TextBox;
                    TextBox txtPayDate = gvCost.Rows[index].FindControl("txtPayDate") as TextBox;
                    TextBox txtPayAmt = gvCost.Rows[index].FindControl("txtPayAmt") as TextBox;
                    TextBox txtReceiveDate = gvCost.Rows[index].FindControl("txtReceiveDate") as TextBox;
                    TextBox txtReceiveAmt = gvCost.Rows[index].FindControl("txtReceiveAmt") as TextBox;
                    TextBox txtAdviseNo = gvCost.Rows[index].FindControl("txtAdviseNo") as TextBox;

                    int costID = Convert.ToInt32(hidCostID_edt.Value);
                    int costTypeID = Convert.ToInt32(ddlCostType.SelectedValue);
                    int attTypeID = 0;
                    string costTypeName = string.Empty;
                    string attTypeName = string.Empty;
                    string tflag = string.Empty;
                    string sflag = string.Empty;
                    string costDesc = string.Empty;
                    string adviseNo = string.Empty;
                    //string user = "T65007";
                    decimal costAmt = 0;
                    decimal payAmt = 0;
                    decimal receiveAmt = 0;
                    DateTime? costdate = null;
                    DateTime? paydate = null;
                    DateTime? receivedate = null;
                    DateTime updatedate = DateTime.Now;

                    vCostType f = new vCostType();
                    using (vCostTypeRep rep = new vCostTypeRep())
                    {
                        f = rep.GetCostTypeByID(costTypeID);
                    }
                    attTypeID = f.AttorneyTypeID;
                    costTypeName = f.CostTypeName;
                    attTypeName = f.AttorneyTypeName;
                    tflag = f.TFlag;
                    sflag = f.SFlag;

                    if (!string.IsNullOrEmpty(txtCostDate.Text)) costdate = Convert.ToDateTime(txtCostDate.Text.Trim());
                    if (!string.IsNullOrEmpty(txtPayDate.Text)) paydate = Convert.ToDateTime(txtPayDate.Text.Trim());
                    if (!string.IsNullOrEmpty(txtReceiveDate.Text)) receivedate = Convert.ToDateTime(txtReceiveDate.Text.Trim());
                    if (!string.IsNullOrEmpty(txtPayAmt.Text)) payAmt = Convert.ToDecimal(txtPayAmt.Text.Trim());
                    if (!string.IsNullOrEmpty(txtCostAmt.Text)) costAmt = Convert.ToDecimal(txtCostAmt.Text.Trim());
                    if (!string.IsNullOrEmpty(txtReceiveAmt.Text)) receiveAmt = Convert.ToDecimal(txtReceiveAmt.Text.Trim());
                    if (!string.IsNullOrEmpty(txtCostDesc.Text)) costDesc = txtCostDesc.Text.Trim();
                    if (!string.IsNullOrEmpty(txtAdviseNo.Text)) adviseNo = txtAdviseNo.Text.Trim();
                    #endregion

                    //=========trCost=========
                    trCost clist = new trCost();
                    clist.CostID = costID;
                    clist.LegalNo = LegalNo;
                    clist.CostTypeID = costTypeID;
                    clist.AttorneyTypeID = attTypeID;
                    clist.CostDate = costdate;
                    clist.PayDate = paydate;
                    clist.ReceiveDate = receivedate;
                    clist.CostAmt = costAmt;
                    clist.PayAmt = payAmt;
                    clist.Income = receiveAmt;
                    clist.CostDesc = costDesc;
                    clist.AdviseNo = adviseNo;
                    clist.UpdateBy = userCode;
                    clist.UpdateDateTime = updatedate;
                    if (CommandName.Equals("Add"))
                    {
                        clist.CreateBy = userCode;
                        clist.CreateDateTime = updatedate;
                    }
                    else
                    {
                        clist.CreateBy = CostList.Find(M => M.CostID == costID).CreateBy;
                        clist.CreateDateTime = CostList.Find(M => M.CostID == costID).CreateDateTime;
                    }

                    costList = CostList;
                    costList.RemoveAll(list => list.CostID == costID);
                    costList.Add(clist);
                    CostList = costList;
                    //=========vCost=========
                    vCost vlist = new vCost();
                    vlist.CostID = costID;
                    vlist.LegalNo = LegalNo;
                    vlist.TFlag = tflag;
                    vlist.SFlag = sflag;
                    vlist.CostTypeID = costTypeID;
                    vlist.AttorneyTypeID = attTypeID;
                    vlist.CostTypeName = costTypeName;
                    vlist.AttorneyTypeName = attTypeName;
                    vlist.CostDate = costdate;
                    vlist.PayDate = paydate;
                    vlist.ReceiveDate = receivedate;
                    vlist.CostAmt = costAmt;
                    vlist.PayAmt = payAmt;
                    vlist.Income = receiveAmt;
                    vlist.CostDesc = costDesc;
                    vlist.AdviseNo = adviseNo;

                    vcostList = vCostList;
                    vcostList.RemoveAll(list => list.CostID == costID);
                    vcostList.Add(vlist);
                    vCostList = vcostList;

                    gvCost.EditIndex = -1;
                    CommandName = "Update";

                    decimal sumcost = 0;
                    decimal sumpay = 0;
                    decimal sumreceive = 0;
                    sumcost = CostList.Sum(c => c.CostAmt);
                    sumpay = CostList.Sum(c => c.PayAmt);
                    sumreceive = CostList.Sum(c => c.Income);
                    lbCostAmt.Text = String.Format("{0:0.00}", sumcost);
                    lbPayAmt.Text = String.Format("{0:0.00}", sumpay);
                    lbReceiveAmt.Text = String.Format("{0:0.00}", sumreceive);
                    break;
            }
            GetUpdateCost();
            //UpdatePanel1.Update();
            //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datepicker", "postback();", true);
        }

        protected void gvCost_RowEditing1(object sender, GridViewEditEventArgs e)
        {
            gvCost.EditIndex = e.NewEditIndex;
            GetUpdateCost();

            bool isFix = false;
            int index = e.NewEditIndex;
            int costTypeID = 0;
            HiddenField hidCostID_edt = gvCost.Rows[index].FindControl("hidCostID_edt") as HiddenField;
            DropDownList ddlCostType = gvCost.Rows[index].FindControl("ddlCostType") as DropDownList;
            //if (ddlCostType == null) { return; }

            int costID = Convert.ToInt32(hidCostID_edt.Value);
            costTypeID = vCostList.Find(c => c.CostID == costID).CostTypeID;
            isFix = CheckFixCostType(costTypeID);
            List<vCostType> list = new List<vCostType>();
            if (isFix)
            {
                using (vCostTypeRep rep = new vCostTypeRep())
                {
                    vCostType item = rep.GetCostTypeByID(costTypeID);
                    list.Add(item);
                }
            }
            else
            {
                using (vCostTypeRep rep = new vCostTypeRep())
                {
                    list = rep.GetCostTypeByFlag("C", "N");
                }
            }
            ddlCostType.DataSource = list;
            ddlCostType.DataBind();
            ddlCostType.SelectedIndex = 0;
        }

        protected void gvCost_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvCost_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                int index = e.RowIndex;
                HiddenField hidCostID_edt = gvCost.Rows[index].FindControl("hidCostID_edt") as HiddenField;
                int costID = Convert.ToInt32(hidCostID_edt.Value);

                //===========trCost===========
                costList = CostList;
                costList.RemoveAll(list => list.CostID == costID);
                CostList = costList;
                //===========vCost===========
                vcostList = vCostList;
                vcostList.RemoveAll(list => list.CostID == costID);
                vCostList = vcostList;
            }
            CommandName = "Cancel";
            gvCost.EditIndex = -1;
            GetUpdateCost();
        }

        protected void gvCost_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton LinkButton2 = e.Row.FindControl("LinkButton2") as LinkButton; // delete button
                HiddenField hidCostTypeID = e.Row.FindControl("hidCostTypeID") as HiddenField;
                if (LinkButton2 == null) { return; } // for edit mode
                if (hidCostTypeID == null) { return; }

                int costTypeID = 0;
                bool isFix = false;

                costTypeID = Convert.ToInt32(hidCostTypeID.Value);
                isFix = CheckFixCostType(costTypeID);
                if (isFix)
                {
                    LinkButton2.Visible = false;
                }
            }
        }

        public bool CheckFixCostType(int costTypeID)
        {
            bool isfix = false;
            if (costTypeID != 0)
            {
                string sSFlag = string.Empty;
                using (vCostTypeRep rep = new vCostTypeRep())
                {
                    sSFlag = rep.GetCostTypeByID(costTypeID).SFlag;
                }
                if (sSFlag.Equals("F")) //SFlag => F:fix: N:not fix
                {
                    isfix = true;
                }
            }

            return isfix;
        }
        #endregion

        #region Controls Event
        protected void btnadd_cost_Click(object sender, EventArgs e)
        {
            int costID = 1; int vcostID = 0; int index = 0;
            if (CostList.Count > 0)
            {
                costID = CostList.Max(M => M.CostID) + 1;
            }
            if (vCostList.Count > 0)
            {
                vcostID = vCostList.Max(M => M.CostID);
            }
            if (costID > vcostID)
            {
                CommandName = "Add";

                int costTypeID = 0;
                int attTypeID = 0;
                string costTypeName = string.Empty;
                string attTypeName = string.Empty;
                string tflag = "C"; // cost
                string sflag = "N"; // not fix
                string costDesc = string.Empty;
                string adviseNo = string.Empty;
                //string user = "T65007";
                decimal costAmt = 0;
                DateTime updatedate = DateTime.Now;
                using (MaxValueRep rep = new MaxValueRep())
                {
                    costAmt = rep.GetMaxValueByMaxName("DefaultCost").MaxAmt;
                }

                costList = CostList;
                vcostList = vCostList;
                //=========trCost=========
                trCost clist = new trCost();
                clist.CostID = costID;
                clist.LegalNo = LegalNo;
                clist.CostTypeID = costTypeID;
                clist.AttorneyTypeID = attTypeID;
                clist.CostDate = updatedate;
                clist.PayDate = updatedate;
                clist.ReceiveDate = updatedate;
                clist.CostAmt = costAmt;
                clist.PayAmt = costAmt;
                clist.Income = 0;
                clist.CostDesc = costDesc;
                clist.AdviseNo = adviseNo;
                clist.CreateBy = userCode;
                clist.CreateDateTime = updatedate;
                clist.UpdateBy = userCode;
                clist.UpdateDateTime = updatedate;
                costList.Add(clist);
                CostList = costList;

                //=========vCost=========
                vCost vlist = new vCost();
                vlist.CostID = costID;
                vlist.LegalNo = LegalNo;
                vlist.TFlag = tflag;
                vlist.SFlag = sflag;
                vlist.CostTypeID = costTypeID;
                vlist.AttorneyTypeID = attTypeID;
                vlist.CostTypeName = costTypeName;
                vlist.AttorneyTypeName = attTypeName;
                vlist.CostDate = updatedate;
                vlist.PayDate = updatedate;
                vlist.ReceiveDate = updatedate;
                vlist.CostAmt = costAmt;
                vlist.PayAmt = costAmt;
                vlist.Income = 0;
                vlist.CostDesc = costDesc;
                vlist.AdviseNo = adviseNo;
                vcostList.Add(vlist);
                vCostList = vcostList;

                GetUpdateCost();

                gvCost.PageIndex = gvCost.PageCount - 1;
                if (gvCost.Rows.Count > 0)
                {
                    index = gvCost.Rows.Count - 1;
                }
                gvCost.SetEditRow(index);
            }
            //UpdatePanel1.Update();
            //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datepicker", "postback();", true);
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datepicker", "postback();", true);
        }

        protected void ddlCostType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int costTypeID = 0;
            int index = gvCost.EditIndex;

            Label lbAttorney = gvCost.Rows[index].FindControl("lbAttorney") as Label;
            DropDownList ddlCostType = gvCost.Rows[index].FindControl("ddlCostType") as DropDownList;
            costTypeID = Convert.ToInt32(ddlCostType.SelectedValue);

            vCostType f = new vCostType();
            using (vCostTypeRep rep = new vCostTypeRep()) f = rep.GetCostTypeByID(costTypeID);
            lbAttorney.Text = f.AttorneyTypeName;
        }
        #endregion
    }
}