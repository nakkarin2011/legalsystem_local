﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab3.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab3" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<script src="../Scripts/jquery1-1.12.0.min.js"></script>
<script src="../Scripts/plugin/bootstrap-datepicker.js"></script>
<%--<script src="../../Scripts/plugin/bootstrap-datepicker.th.min.js"></script>--%>
<script type="text/javascript">
    function modalshowcor() {
        $('#courtAssignModal').modal('toggle');
    }
    function modalhidecor() {
        $('#courtAssignModal').modal('hide');
    }
    function validateAlert(v_text) {
        alert(v_text);
    }
</script>

<asp:Panel ID="Panel1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row justify-content-center">
                <div class="col-lg-11 col-md-11 col-11 ">
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">คำฟ้องถูกต้อง/ไม่ถูกต้อง <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlProsecute1Result" runat="server" CssClass="form-control"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlProsecute1Result_SelectedIndexChanged">
                                    <asp:ListItem Text="คำฟ้องถูกต้อง" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="คำฟ้องไม่ถูกต้อง" Value="false"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">รายละเอียด <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <input type="text" id="txtProcecuteNote" runat="server" class=" form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">คดีดำ <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <input type="text" id="txtUndecidedCase" runat="server" class=" form-control" />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">ศาล <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlCourt" runat="server" class="form-control" OnSelectedIndexChanged="ddlCourt_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">ทุนทรัพย์ <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <input id="txtCapital" type="number" step="any" class="form-control" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">ค่าธรรมเนียมศาล :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group"></div>
                            <input id="txtCourt1Fee" runat="server" class="form-control" disabled="disabled" />

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">ศาลนัด (ศาลชั้นต้น) <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-7 col-sm-1 col-1">
                            <asp:Button ID="btnAdd_CourtAssign" runat="server" Text="Add" CssClass="btn btn-info btn-sm"
                                OnClick="btnAdd_CourtAssign_Click" UseSubmitBehavior="false" />
                            <asp:GridView ID="gvCourtAssign" runat="server" CellPadding="0" CellSpacing="0" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                OnRowDataBound="gvCourtAssign_RowDataBound" OnPageIndexChanging="gvCourtAssign_PageIndexChanging"
                                OnRowCommand="gvCourtAssign_RowCommand" OnRowDeleting="gvCourtAssign_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลนัด">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDate" runat="server" Text='<%#Eval("AssignDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                            <asp:HiddenField ID="hidAssignID1" runat="server" Value='<%#Eval("AssignID")%>' />
                                            <asp:HiddenField ID="hidAssignID" runat="server" Value='<%#Eval("AssignID")%>' />
                                            <asp:HiddenField ID="hidCourtID" runat="server" Value="1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="เหตุผล" DataField="AssignReason" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditAA" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">วันพิพากษา <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm- col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtExctDate" runat="server" class="datepicker form-control" />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">วันพิพากษาตามยอม <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="TEXT" id="txtBendDate" runat="server" class="datepicker form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">คดีแดง <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <input type="text" id="txtDecidedCase" runat="server" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">บัญชีค่าฤชาธรรมเนียม <span style="color: red">*</span>:</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlRachaFee" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="มี" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="ไม่มี" Value="false"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">คำพิพากษาถูกต้อง/ไม่ถูกต้อง <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlExct1Result" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlExct1Result_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="คำพิพากษาถูกต้อง" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="คำพิพากษาไม่ถูกต้อง" Value="false"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">รายละเอียด <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1">
                            <div class="form-group">
                                <input type="text" id="txtExct1Desc" runat="server" class=" form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 text-right">
                            <div class="tim-typo mt-4-5">
                                <h5><span class="tim-note">อุทรณ์/งดอุทรณ์ <span style="color: red">*</span> :</span></h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                            <div class="form-group">
                                <asp:RadioButtonList ID="radioCourt1_Next" runat="server" CssClass="radio-inline"
                                    OnSelectedIndexChanged="radioCourt1_Next_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="อุทรณ์" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="งดอุทรณ์" Value="false"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div id="divCourt1_Next" runat="server" style="display: none">
                        <div class="card" style="width: 100%;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">วันอุทรณ์ <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="text" id="dateApp" runat="server" class="datepicker form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">คดีดำ (อุทรณ์) <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="text" id="txtundecided_app" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">ทุนทรัพย์ชั้นอุทรณ์ <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="number" step="any" id="txtasset_app" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">ค่าธรรมเนียมชั้นอุทรณ์ :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="number" step="any" id="txtfee_app" runat="server" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">วันยื่นแก้อุทรณ์ <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="text" id="txtSendAdjd_app" runat="server" class="datepicker form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">ศาลนัด <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-lg-7 col-sm-1 col-1">
                                        <asp:Button ID="btnAdd_AppealAssign" runat="server" Text="Add" CssClass="btn btn-info btn-sm"
                                            OnClick="btnAdd_AppealAssign_Click" UseSubmitBehavior="false" />
                                        <asp:GridView ID="gvAppealAssign" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                            AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                            EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                            AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                            PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                            PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                            OnRowDataBound="gvAppealAssign_RowDataBound" OnPageIndexChanging="gvAppealAssign_PageIndexChanging"
                                            OnRowCommand="gvAppealAssign_RowCommand" OnRowDeleting="gvAppealAssign_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลนัด">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDate" runat="server" Text='<%#Eval("AssignDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                        <asp:HiddenField ID="hidAssignID" runat="server" Value='<%#Eval("AssignID")%>' />
                                                        <asp:HiddenField ID="hidCourtID" runat="server" Value="1" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="เหตุผล" DataField="AssignReason" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">วันพิพากษาชั้นอุทรณ์ <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="text" id="dateExecute_app" runat="server" class="datepicker form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5><span class="tim-note">คดีแดง (อุทรณ์) <span style="color: red">*</span> :</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                        <div class="form-group">
                                            <input type="text" id="txtDecidedCase_app" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 text-right">
                                <div class="tim-typo mt-4-5">
                                    <h5><span class="tim-note">ฎีกา/งดฎีกา <span style="color: red">*</span> :</span></h5>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="radioCourt2_Next" runat="server" CssClass="radio-inline"
                                        OnSelectedIndexChanged="radioCourt2_Next_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="ฎีกา" Value="true"></asp:ListItem>
                                        <asp:ListItem Text="งดฎีกา" Value="false"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div id="divCourt2_Next" runat="server" style="display: none">
                            <div class="card" style="width: 100%;">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">วันฏีกา <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="datePetition" runat="server" class="datepicker form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">คดีดำ (ฏีกา) <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtUndecided_pet" runat="server" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">ทุนทรัพย์ชั้นฏีกา :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="number" step="any" id="txtAsset_pet" runat="server" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">ค่าธรรมเนียมชั้นฏีกา :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtfee_pet" runat="server" class="form-control" disabled="disabled" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">วันยื่นแก้ฏีกา <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="dateSendAdjd_pet" runat="server" class="datepicker form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">ศาลนัด <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-lg-7 col-sm-1 col-1">
                                            <asp:Button ID="btnAdd_PetitionAssign" runat="server" Text="Add" CssClass="btn btn-info btn-sm"
                                                OnClick="btnAdd_PetitionAssign_Click" UseSubmitBehavior="false" />
                                            <asp:GridView ID="gvPetitionAssign" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText"
                                                AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                                OnRowDataBound="gvPetitionAssign_RowDataBound" OnPageIndexChanging="gvPetitionAssign_PageIndexChanging"
                                                OnRowCommand="gvPetitionAssign_RowCommand" OnRowDeleting="gvPetitionAssign_RowDeleting">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="วันที่ศาลนัด">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDate" runat="server" Text='<%#Eval("AssignDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                            <asp:HiddenField ID="hidAssignID" runat="server" Value='<%#Eval("AssignID")%>' />
                                                            <asp:HiddenField ID="hidCourtID" runat="server" Value="1" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="เหตุผล" DataField="AssignReason" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info btn-sm" CommandName="EditCommand" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger btn-sm" CommandName="Delete" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">วันพิพากษาชั้นฏีกา <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="dateSendPet" runat="server" class="datepicker form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5><span class="tim-note">คดีแดง (ฏีกา) <span style="color: red">*</span> :</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                            <div class="form-group">
                                                <input type="text" id="txtDecided_pet" runat="server" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div id="courtAssignModal" class="modal fade" role="dialog">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            ศาลนัด
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    วันที่ศาลนัด
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidAssignID_modal" runat="server" />
                                        <asp:HiddenField ID="hidCourtID_modal" runat="server" />
                                        <input type="text" id="txtDate_courtModal" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 col-form-label">
                                    เหตุผล
                                    <label style="color: red">*</label>
                                    :</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="txtReason_courtModal" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSave_CourtModal" runat="server" Text="Save" class="btn btn-fill btn-info" OnClick="btnSave_CourtModal_Click" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Panel>


