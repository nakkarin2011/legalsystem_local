﻿using LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab4 : System.Web.UI.UserControl
    {
        #region Properties
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public trLegal LegalData
        {
            get
            {
                return (trLegal)Session["LegalData"];
            }
            set
            {
                Session["LegalData"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Panel1, Panel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{
            //    BindControl();
            //}
        }
        public void BindControl()
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    List<LegalEntities.AttorneyType> Att;
                    using (AttorneyTypeRep rep = new AttorneyTypeRep()) Att = rep.GetAttorneyTypeList();
                    ddlAttorneyType41.DataSource = Att;
                    ddlAttorneyType41.DataValueField = "AttorneyTypeID";
                    ddlAttorneyType41.DataTextField = "AttorneyTypeName";
                    ddlAttorneyType41.DataBind();

                    List<msAttorney> Lw = unitOfwork.MSAttorneyRep.GetLawyerByTypeID(1);
                    ddlLawyer41.DataSource = Lw;
                    ddlLawyer41.DataValueField = "LawyerID";
                    ddlLawyer41.DataTextField = "LawyerName";
                    ddlLawyer41.DataBind();
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }

        }

        public string ValidateTab()
        {
            string err = string.Empty;
            if (string.IsNullOrEmpty(txtReceiveExctDate.Value)) err = "กรุณากรอก \"วันรับเรื่องบังคับคดี\"";
            return err;
        }

        public trLegal GetUI()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtGenExctDate.Value)) LegalData.T4_GenSubpoenaDate = Convert.ToDateTime(txtGenExctDate.Value.ToString());
                else LegalData.T4_GenSubpoenaDate = null;
                if (!string.IsNullOrEmpty(txtReceiveExctDate.Value)) LegalData.T4_GotExecutionDate = Convert.ToDateTime(txtReceiveExctDate.Value.ToString());
                else LegalData.T4_GotExecutionDate = null;
                if (!string.IsNullOrEmpty(txtSentToLawyerDate.Value)) LegalData.T4_SendToLawyerDate = Convert.ToDateTime(txtSentToLawyerDate.Value.ToString());
                else LegalData.T4_SendToLawyerDate = null;
                if (!string.IsNullOrEmpty(ddlLawyer41.SelectedValue))
                    if (Convert.ToInt32(ddlLawyer41.SelectedValue) > 0)
                        LegalData.T4_LawyerID = Convert.ToInt32(ddlLawyer41.SelectedValue.ToString());
                    else LegalData.T4_LawyerID = null;
                else LegalData.T4_LawyerID = null;
                LegalData.UpdateBy = userCode;
                LegalData.UpdateDateTime = DateTime.Now;
            }
            catch(Exception ex)
            {

            }

            return LegalData;
        }

        public void SetUI(trLegal data)
        {
            try
            {
                LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
                LegalData = data;
                BindControl();

                int attorneytypeID = 0;
                int lawyerID = 0;

                try { lawyerID = (int)LegalData.T4_LawyerID; }
                catch (Exception ex) { }

                if (lawyerID > 0)
                {
                    attorneytypeID =unitOfwork.MSAttorneyRep.GetLawyerByID(lawyerID).AttorneyTypeID;
                    ddlAttorneyType41.SelectedValue = attorneytypeID.ToString();
                    ddlLawyer41.SelectedValue = LegalData.T4_LawyerID.ToString();
                }
                else
                {
                    ddlAttorneyType41.SelectedIndex = 0;
                    ddlLawyer41.SelectedIndex = 0;
                }

                if (LegalData.T4_GenSubpoenaDate != null) txtGenExctDate.Value = ((DateTime)LegalData.T4_GenSubpoenaDate).ToString("dd/MM/yyyy");
                if (LegalData.T4_GotExecutionDate != null) txtReceiveExctDate.Value = ((DateTime)LegalData.T4_GotExecutionDate).ToString("dd/MM/yyyy");
                if (LegalData.T4_SendToLawyerDate != null) txtSentToLawyerDate.Value = ((DateTime)LegalData.T4_SendToLawyerDate).ToString("dd/MM/yyyy");

                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
        }

        protected void ddlAttorneyType41_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                int atnType = Convert.ToInt32(ddlAttorneyType41.SelectedValue);
                List<msAttorney> A =unitOfwork.MSAttorneyRep.GetLawyerByTypeID(atnType);
                if (A.Count > 0)
                {
                    ddlLawyer41.Enabled = true;
                    ddlLawyer41.DataSource = A;
                    ddlLawyer41.DataBind();
                    ddlLawyer41.DataValueField = "LawyerID";
                    ddlLawyer41.DataTextField = "LawyerName";
                    ddlLawyer41.SelectedIndex = 0;
                }
                else ddlLawyer41.Enabled = false;
            }
        }
    }
}