﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab2.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab2" %>

<asp:Panel ID="Panel1" runat="server">
        <div class="row justify-content-center">
        <div class="col-lg-11 col-md-11 col-11 ">
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันออก Notice <span style="color: red">*</span> :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="datePintNotice" runat="server" class="datepicker form-control"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันออกกำหนดฟ้อง <span style="color: red">*</span> :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateAssignExecute" runat="server" class="datepicker form-control"  />
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5>
                            <span class="tim-note">วันที่ฟ้อง <span style="color: red">*</span> :</span>
                        </h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="dateExecute" runat="server" class="datepicker form-control"  />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
