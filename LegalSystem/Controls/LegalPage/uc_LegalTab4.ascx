﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_LegalTab4.ascx.cs" Inherits="LegalSystem.Controls.LegalPage.uc_LegalTab4" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Panel ID="Panel1" runat="server">
    <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12 col-12">
            <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
            <div class="row">
                <div class="col-md-2 col-sm-2 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันออกหมายบังคับคดี :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="txtGenExctDate" runat="server" class="datepicker form-control" />
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 text-right">
                    <div class="tim-typo mt-4-5">
                        <h5><span class="tim-note">วันรับเรื่องบังคับคดี <span style="color: red">*</span> :</span></h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                    <div class="form-group">
                        <input type="text" id="txtReceiveExctDate" runat="server" class="datepicker form-control" />
                    </div>
                </div>
            </div>
            <div class="card" style="width: 100%;">
                <div class="card-header card-header-tabs card-header-warning">
                    <h5 class="card-title">Assign บังคับคดี</h5>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-2 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5><span class="tim-note">ทนายใน/นอก :</span></h5>
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1 col-1 ">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlAttorneyType41" runat="server" AutoPostBack="true"
                                CssClass="form-control" OnSelectedIndexChanged="ddlAttorneyType41_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">ชื่อทนาย :</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlLawyer41" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">วันส่งเรื่องให้ทนาย :</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                        <div class="form-group">
                            <input type="text" id="txtSentToLawyerDate" runat="server" class="datepicker form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
