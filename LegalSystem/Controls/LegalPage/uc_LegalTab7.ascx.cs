﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls.LegalPage
{
    public partial class uc_LegalTab7 : System.Web.UI.UserControl
    {
        #region Properties
        public string userCode
        {
            get
            {
                return (string)Session["userCode"];
            }
            set
            {
                Session["userCode"] = value;
            }
        }
        public string LegalNo
        {
            get
            {
                return (string)Session["LegalNo"];
            }
            set
            {

                Session["LegalNo"] = value;
            }
        }
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }
        public string SubCommandName
        {
            get
            {
                return (string)Session["SubCommandName"];
            }
            set
            {

                Session["SubCommandName"] = value;
            }
        }
        private List<trDocument> attachDocList;
        public List<trDocument> AttachDocList
        {
            get
            {
                return (List<trDocument>)Session["AttachDocList"];
            }
            set
            {
                Session["AttachDocList"] = value;
            }
        }

        private List<trDocumentMapping> docMappingList;
        public List<trDocumentMapping> DocMappingList
        {
            get
            {
                return (List<trDocumentMapping>)Session["DocMappingList"];
            }
            set
            {
                Session["DocMappingList"] = value;
            }
        }

        private List<vDocumentMapping> vattachDocList;
        public List<vDocumentMapping> vAttachDocList
        {
            get
            {
                return (List<vDocumentMapping>)Session["vAttachDocList"];
            }
            set
            {
                Session["vAttachDocList"] = value;
            }
        }

        private List<vDocumentMapping> documentByTimeList;
        public List<vDocumentMapping> DocumentByTimeList
        {
            get
            {
                return (List<vDocumentMapping>)Session["DocumentByTimeList"];
            }
            set
            {
                Session["DocumentByTimeList"] = value;
            }
        }
        public List<vAuctionAssign> vAuctionAssignList
        {
            get
            {
                return (List<vAuctionAssign>)Session["vAuctionAssignList"];
            }
            set
            {
                Session["vAuctionAssignList"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "datetimepicker", "postback();", true);

            //if (!IsPostBack)
            //{
            //    BindControl();
            //}
        }

        public void SetUI(string legalNo, List<trDocument> document, List<trDocumentMapping> documentMapping, List<vDocumentMapping> viewDocMapping)
        {
            LegalNo = legalNo;
            vAttachDocList = viewDocMapping;
            DocMappingList = documentMapping;
            AttachDocList = document;

            gvAttach.DataSource = vAttachDocList.FindAll(d => d.Flag.Equals("L")).OrderBy(d => d.DocTypeName).ToList();
            gvAttach.DataBind();
            UpdatePanel1.Update();
        }

        public Tab7Data GetDocument()
        {
            Tab7Data data = new Tab7Data();
            data.DocMapList = DocMappingList;
            data.DocumentList = AttachDocList;

            return data;
        }

        #region function
        public void BindddlDoctype_attachmodal(string flag)
        {
            // flag => P: Pursue, A: Auction, L: Legal(Tab7)

            List<DocumentType> A;
            using (DocumentTypeRep rep = new DocumentTypeRep())
            {
                A = rep.GetDocumentTypeByFlag(flag);
            }

            //// NOT USE : Remove DocType from dropDownList
            //if (flag.Equals("L"))
            //{
            //    vattachDocList = vAttachDocList.FindAll(list => list.Flag.Equals("L"));
            //    if (CommandName.Equals("Add"))
            //    {
            //        foreach (vDocumentMapping item in vattachDocList)
            //        {
            //            A.RemoveAll(list => list.DocTypeID == item.DocTypeID);
            //        }
            //    }
            //}

            ddlDoctype_attachmodal.DataSource = A;
            if (A.Count > 0)
            {
                ddlDoctype_attachmodal.DataValueField = "DocTypeID";
                ddlDoctype_attachmodal.DataTextField = "DocTypeName";
                ddlDoctype_attachmodal.SelectedIndex = 0;
            }
            ddlDoctype_attachmodal.DataBind();

            ddlDoctype_attachmodal.Enabled = true;

        }
        public void GetUpdateDocMapping()
        {
            // flag => P:Pursue(สีบทรัพย์) A:Auction(ยึก/อายัดทรัพย์) L:Legal(เอกสารแนบ Tab7)

            //DocumentByTimeList = documentByTimeList;
            gvAttach.DataSource = vAttachDocList.FindAll(list => list.Flag.Equals("L")).OrderBy(list => list.DocTypeID).ToList();
            gvAttach.DataBind();
        }
        public string GetPath(int docTypeID, string docName)
        {
            string path = string.Empty;
            switch (docTypeID)
            {
                default:
                    path = ConfigurationManager.DocPathType0;
                    break;
                case 1:
                    path = ConfigurationManager.DocPathType1;
                    break;
                case 2:
                    path = ConfigurationManager.DocPathType2;
                    break;
                case 3:
                    path = ConfigurationManager.DocPathType3;
                    break;
                case 4:
                    path = ConfigurationManager.DocPathType4;
                    break;
                case 5:
                    path = ConfigurationManager.DocPathType5;
                    break;
                case 6:
                    path = ConfigurationManager.DocPathType6;
                    break;
                case 7:
                    path = ConfigurationManager.DocPathType7;
                    break;
                case 8:
                    path = ConfigurationManager.DocPathType8;
                    break;
                case 9:
                    path = ConfigurationManager.DocPathType9;
                    break;
                case 10:
                    path = ConfigurationManager.DocPathType10;
                    break;
                case 11:
                    path = ConfigurationManager.DocPathType11;
                    break;
                case 12:
                    path = ConfigurationManager.DocPathType12;
                    break;
                case 13:
                    path = ConfigurationManager.DocPathType13;
                    break;
                case 14:
                    path = ConfigurationManager.DocPathType14;
                    break;
                case 15:
                    path = ConfigurationManager.DocPathType15;
                    break;
                case 16:
                    path = ConfigurationManager.DocPathType16;
                    break;
                case 17:
                    path = ConfigurationManager.DocPathType17;
                    break;
                case 18:
                    path = ConfigurationManager.DocPathType18;
                    break;
                case 19:
                    path = ConfigurationManager.DocPathType19;
                    break;
                case 20:
                    path = ConfigurationManager.DocPathType20;
                    break;
                case 21:
                    path = ConfigurationManager.DocPathType21;
                    break;
                case 22:
                    path = ConfigurationManager.DocPathType22;
                    break;
                case 23:
                    path = ConfigurationManager.DocPathType23;
                    break;
                case 24:
                    path = ConfigurationManager.DocPathType24;
                    break;
                case 25:
                    path = ConfigurationManager.DocPathType25;
                    break;
            }
            return path + docName;
        }
        public string GetFileName(string fileNameWithExtension)
        {
            int last = fileNameWithExtension.LastIndexOf(".");
            //string name = fileNameWithExtension.Substring(0, last)
            //    + DateTime.Now.ToString("_yyyyMMddHHmmss") + "_t" // _t : before submit save
            //    + fileNameWithExtension.Substring(last, fileNameWithExtension.Length - last);
            string name = System.IO.Path.GetFileNameWithoutExtension(fileNameWithExtension)
                + DateTime.Now.ToString("_yyyyMMddHHmmss") + "_t"
                + System.IO.Path.GetExtension(fileNameWithExtension);
            return name;
        }
        #endregion

        #region Controls Event
        protected void btnadd_attach_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            SubCommandName = "Add";

            hidFlag_attachmodal.Value = "L";
            BindddlDoctype_attachmodal("L");

            //UpdatePanel6.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "attachModal", "modalshowattach();", true);
        }
        protected void btnsave_attachdoc_Click(object sender, EventArgs e)
        {
            try
            {
                int docID = 1;
                int fID = 1; // Flag
                int pursueTime = 0;
                int auctionOrder = 0;
                int docTypeID = 0;
                string docTypeName = string.Empty;
                string docName = string.Empty;
                string docPath = string.Empty;
                string flag = "L"; //string flag = string.Empty;
                DateTime date = DateTime.Now;

                if (AttachDocList.Count > 0)
                { 
                    docID += LegalSession.lastDocID;
                    LegalSession.lastDocID++;
                }
                docTypeID = Convert.ToInt32(ddlDoctype_attachmodal.SelectedValue.Trim());
                docTypeName = ddlDoctype_attachmodal.SelectedItem.Text.Trim();
                if (LegalSession.filessave == null) LegalSession.filessave = new List<fileupload>();
                if (LegalSession.fileupload == null) LegalSession.fileupload = new FileUpload();
                FileUpload file = LegalSession.fileupload;
                docName = GetFileName(file.FileName);
                docPath = GetPath(docTypeID, docName);
                file.SaveAs(docPath); // Err: Can not access a close file.

                #region NOT USE
                //// do comment as tab5
                //if (flag.Equals("P"))
                //{
                //    if (PursueList.Count > 0) fID = PursueList.Max(list => list.PursueID) + 1;
                //    pursueTime = Convert.ToInt32(txtPTime.Value);
                //}
                //else if (flag.Equals("A"))
                //{
                //    if (AuctionList.Count > 0) fID = AuctionList.Max(list => list.AuctionID) + 1;
                //    auctionOrder = Convert.ToInt32(txtOrder_auctionmodal.Value);
                //}
                #endregion
                if (flag.Equals("L"))
                {
                    fID = 0;
                }

                trDocument doc = new trDocument();
                trDocumentMapping map = new trDocumentMapping();
                vDocumentMapping view = new vDocumentMapping();
                fileupload newfile = new fileupload();

                if (SubCommandName.Equals("Add"))
                {
                    newfile.DocID = docID;
                    newfile.File = file;
                    LegalSession.filessave.Add(newfile);
                    //===========trDocument===========
                    doc.DocID = docID;
                    doc.DocTypeID = docTypeID;
                    doc.DocName = docName;
                    doc.DocPath = docPath;
                    doc.CreateBy = userCode;
                    doc.CreateDateTime = date;
                    doc.UpdateBy = userCode;
                    doc.UpdateDateTime = date;
                    attachDocList = AttachDocList;
                    attachDocList.Add(doc);
                    AttachDocList = attachDocList;
                    //===========trDocumentMapping===========
                    map.DocID = docID;
                    map.LegalNo = LegalNo;
                    map.Flag = flag;
                    map.FID = fID;
                    map.CreateBy = userCode;
                    map.CreateDateTime = date;
                    map.UpdateBy = userCode;
                    map.UpdateDateTime = date;
                    docMappingList = DocMappingList;
                    docMappingList.Add(map);
                    DocMappingList = docMappingList;
                    //===========vDocumentMapping===========
                    view.DocID = docID;
                    view.LegalNo = LegalNo;
                    view.DocTypeID = docTypeID;
                    view.DocTypeName = docTypeName;
                    view.DocName = docName;
                    view.DocPath = docPath;
                    view.Flag = flag;
                    view.FID = fID;
                    view.LegalNo = LegalNo;
                    view.PursueTime = pursueTime;
                    view.AuctionOrder = auctionOrder;
                    vattachDocList = vAttachDocList;
                    vattachDocList.Add(view);
                    vAttachDocList = vattachDocList;
                }
                if (flag.Equals("L"))
                {
                    GetUpdateDocMapping();
                    UpdatePanel1.Update();
                }
                ScriptManager.RegisterClientScriptBlock(UpdatePanel6, UpdatePanel6.GetType(), "hide-attachModal", "modalhideattach();", true);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region gvAttach Event
        protected void gvAttach_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gvAttach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvAttach_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAttach.PageIndex = e.NewPageIndex;
            gvAttach.DataSource = vAttachDocList;
            gvAttach.DataBind();
        }

        protected void gvAttach_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewCommand")
            {
                #region NOT USE : OLD VERSION - open modal
                //// get data from grid
                //CommandName = "View";
                //GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //int index = rindex.RowIndex;
                //int docID = Convert.ToInt32((gvAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());
                //int docTypeID = 0;

                //docTypeID = vAttachDocList.First(list => list.DocID == docID).DocTypeID;

                //hidDocType_doclist.Value = docTypeID.ToString();
                //GetUpdateLegalDocByType(docTypeID);
                //UpdatePanel13.Update();
                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "doclistModal", "modalshowdoclist();", true);
                #endregion

                CommandName = "View";
                //GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //int index = rindex.RowIndex;
                //int docID = Convert.ToInt32((gvAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());
                //string docPath = string.Empty;

                //trDocumentRep rep = new trDocumentRep();
                //docPath = rep.GetDocumentByID(docID).DocPath;

                //Response.Write("<script>window.open('PDFPage.aspx?val=" + docPath
                //    + "' ,'_blank', 'menubar=0, scrollbars=1, width=600, height=800');</script>");
            }
            else if (e.CommandName == "Delete")
            {
                #region NOT USE : OLD VERSION - Delete by DocType
                //CommandName = "Delete";
                //GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //int index = rindex.RowIndex;
                //int docID = Convert.ToInt32((gvAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());
                //int docTypeID = 0;
                //docTypeID = vAttachDocList.First(list => list.DocID == docID).DocTypeID;
                //List<vDocumentMapping> doclist = new List<vDocumentMapping>();
                //doclist = vAttachDocList.FindAll(list => list.DocTypeID == docTypeID);

                //vattachDocList = vAttachDocList;
                //docMappingList = DocMappingList;
                //attachDocList = AttachDocList;
                //vattachDocList.RemoveAll(list => list.DocTypeID == docTypeID);
                //foreach (vDocumentMapping item in doclist)
                //{
                //    docMappingList.RemoveAll(list => list.DocID == item.DocID);
                //    attachDocList.RemoveAll(list => list.DocID == item.DocID);
                //}
                //vAttachDocList = vattachDocList;
                //DocMappingList = docMappingList;
                //AttachDocList = attachDocList;
                //GetUpdateDocMapping();
                //UpdatePanel1.Update();
                #endregion

                // NEW VERSION - Delete by DocID
                CommandName = "Delete";
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                int docID = Convert.ToInt32((gvAttach.Rows[index].FindControl("hidDocID") as HiddenField).Value.Trim());

                if (LegalSession.filesdelete == null) LegalSession.filesdelete = new List<filedelete>();
                string namedelete = AttachDocList.First(x => x.DocID == docID).DocPath;
                LegalSystem.Common.filedelete file = new filedelete();
                file.DocID = docID;
                file.FileName = namedelete;
                LegalSession.filesdelete.Add(file);

                vattachDocList = vAttachDocList;
                docMappingList = DocMappingList;
                attachDocList = AttachDocList;
                vattachDocList.RemoveAll(list => list.DocID == docID);
                docMappingList.RemoveAll(list => list.DocID == docID);
                attachDocList.RemoveAll(list => list.DocID == docID);
                vAttachDocList = vattachDocList;
                DocMappingList = docMappingList;
                AttachDocList = attachDocList;
                GetUpdateDocMapping();
                UpdatePanel1.Update();                
            }
        }
        #endregion
    }

    public class Tab7Data
    {
        public List<trDocument> DocumentList { get; set; }
        public List<trDocumentMapping> DocMapList { get; set; }
    }
}