﻿using LegalData;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls
{
    public partial class ucReassignHistory : System.Web.UI.UserControl
    {
        public string RequestNo
        {
            get
            {
                return (string)Session["RequestNo"];
            }
            set
            {
                Session["RequestNo"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetUI();
            }
        }
        private void SetUI()
        {
            using (trReAssignHisRep rep = new trReAssignHisRep())
            {
                var ReAssignHis = rep.GetbyCondition(RequestNo);
                if (ReAssignHis.Count > 0)
                {
                    PanelReAssign.Visible = true;
                    UcMessageBoxMain.VisibleControl();
                    gvHistory.DataSource = ReAssignHis.ToList();
                    gvHistory.DataBind();
                }
                else
                {
                    PanelReAssign.Visible = false;
                    UcMessageBoxMain.Show(string.Format("{0}", MessageEvent.NoReAssignHis), "P");
                }
            }
        }

        protected void gvHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvHistory.PageIndex = e.NewPageIndex;
            SetUI();
        }
    }
}