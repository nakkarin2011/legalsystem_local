﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Controls
{
    public partial class ucMessageBox : System.Web.UI.UserControl
    {
        public void Show(string Message, string msgstyle)
        {
            switch (msgstyle)
            {
                case "Q":
                case "P":
                    //primary
                    Icon.Attributes["class"] = "fa fa-info-circle fa-lg";
                    PanelMsg.Attributes["class"] = "alert alert-primary";
                    //PanelMsg.Attributes["style"] = "line-height:0.5em";
                    //lblError.CssClass = "alert alert-primary";  #cce5ff    
                    break;
                case "E":
                    //Error
                    Icon.Attributes["class"] = "fa fa-exclamation-triangle fa-lg";
                    PanelMsg.Attributes["class"] = "alert alert-danger"; break;
                case "W":
                    //Warning
                    Icon.Attributes["class"] = "fa fa-question-circle fa-lg";
                    PanelMsg.Attributes["class"] = "alert alert-warning"; break;
                case "S":
                    //Success
                    Icon.Attributes["class"] = "fa fa-check fa-lg";
                    PanelMsg.Attributes["class"] = "alert alert-success"; break;
                case "I":
                    //Infomation
                    Icon.Attributes["class"] = "fa fa-info-circle fa-lg";
                    PanelMsg.Attributes["class"] = "alert alert-info"; break;
                case "X":
                //Exception
                default:
                    Icon.Attributes["class"] = "fa fa-exclamation-triangle fa-lg";
                    PanelMsg.Attributes["style"] = "background-color:#8e9296;color:white;line-height:0.5em";
                    //PanelMsg.Attributes["class"] = "alert alert-dark";
                    break;
            }
            PanelMsg.Visible = Icon.Visible = true;
            lblError.Text = Message;
            lblError.Visible = true;

        }
        public void VisibleControl()
        {
            PanelMsg.Visible = Icon.Visible = lblError.Visible = false;
            //panelerror.visible = false;
        }
        public void AlertMessageBox(String message, string type, Page pg, Object obj)
        {
            ScriptManager.RegisterStartupScript(pg, this.GetType(), "myModal", "alertData('" + message + "','" + type + "');", true);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "alertData()", true);
        }
    }
}