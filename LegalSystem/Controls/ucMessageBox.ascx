﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMessageBox.ascx.cs" Inherits="LegalSystem.Controls.ucMessageBox" %>
<style>
    .panel-error-green {
        width: 100%;
        border: 2px solid #ffffff !important;
        margin-top: 1px;
        vertical-align: middle;
        text-align: left;
        padding-left: 17px;
        padding-top: 3px;
        height: 33px;
        background-color: #5affa4 !important;
    }

    .panel-error-red {
        width: 100%;
        /*border:1px solid #f04430;*/
        border: 2px solid #ffffff !important;
        margin-top: 1px;
        vertical-align: middle;
        text-align: left;
        padding-left: 17px;
        padding-top: 3px;
        height: 33px;
        background-color: rgba(230, 136, 136,1) !important;
    }

    .panel-error-yellow {
        width: 100%;
        border: 2px solid #ffffff !important;
        margin-top: 1px;
        vertical-align: middle;
        text-align: left;
        padding-left: 17px;
        padding-top: 3px;
        height: 33px;
        background-color: #ffffcc !important;
    }


    .labelerror {
        margin-top: 8px;
        margin-left: 3px;
        display: inline;
        color: White;
        font-weight: bold;
        font-size: 14px;
        font-family: sans-serif;
    }

    .labelsuccess {
        margin-top: 8px;
        margin-left: 3px;
        display: inline;
        color: black;
        font-weight: bold;
        font-size: 14px;
        font-family: sans-serif;
    }
</style>

<script src="../Scripts/jsSwal.js"></script>

<asp:Panel ID="PanelMsg" runat="server" Class="alert" Style="line-height: 0.5em" role="alert" Visible="false">
    <div class="alert-icon">
        <i id="Icon" runat="server" class="fa fa-exclamation-triangle fa-lg" style="color: white"></i>
    </div>
    <div>
        <asp:Label ID="lblError" runat="server" Style="padding-top: 0.3em;"></asp:Label>
    </div>
</asp:Panel>


<%--Function ModalAlert--%>
<script>
    function alertData(body, type) {
        swal('Message', body, type);
        //swal('Good job!', 'You clicked the button!', 'success');
    }
    function alertDataRedirect(body, type, page) {
        swal({
            title: "Message",
            text: body,
            type: type,
        }).then(function () {
            window.location = page;
        });
    }

</script>



