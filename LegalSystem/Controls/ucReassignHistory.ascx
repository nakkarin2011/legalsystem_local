﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucReassignHistory.ascx.cs" Inherits="LegalSystem.Controls.ucReassignHistory" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
<asp:Panel ID="PanelReAssign" runat="server" Visible="true">
    <div class="row">
        <div class="card ">

            <div class="card-body">

                <div class="col-md-12">
                    <div class="material-datatables table-responsive">
                        <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                            ShowHeaderWhenEmpty="True" DataKeyNames="RequestNo" AllowPaging="True" PageSize="5" OnPageIndexChanging="gvHistory_PageIndexChanging"
                            OnRowCommand="gvHistory_RowCommand" OnRowDataBound="gvHistory_RowDataBound" PageIndex="1">
                            <Columns>
                                <asp:BoundField Visible="false" DataField="RequestID" HeaderText="RequestID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>                                
                                <%--<asp:BoundField DataField="RequestNo" SortExpression="RequestNo" HeaderText="Request No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="CifNo" SortExpression="CifNo" HeaderText="CIF No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="LegalNo" SortExpression="LegalNo" HeaderText="Legal No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>--%>
                                <asp:BoundField DataField="LegalStatus" SortExpression="LegalStatus" HeaderText="LegalStatus" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                <asp:BoundField DataField="LawyerAttorneyID" SortExpression="LawyerAttorneyID" HeaderText="OA/IA" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="LawyerID" SortExpression="LawyerID" HeaderText="Lawyer" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="ReAssignBy" SortExpression="ReAssignBy" HeaderText="Assign By" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="ReAssignDate" SortExpression="ReAssignDate" HeaderText="Assign Date" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                <asp:BoundField DataField="Comment" SortExpression="Comment" HeaderText="Comment" ItemStyle-HorizontalAlign="Left" ></asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="false" ForeColor="Black" />
                            <HeaderStyle HorizontalAlign="Left" Font-Bold="true"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
