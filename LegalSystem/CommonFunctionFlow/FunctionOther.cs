﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace LegalSystem.CommonFunctionFlow
{
    public class FunctionOther
    {

        public static void InsertDataLnmastRep(LegalUnitOfWork unitOfwork, LNMAST l)
        {
            try
            {
                unitOfwork.LnmastRep.Insert(new LNMAST
                {
                    #region
                    ACCI3 = l.ACCI3,
                    ACCTNO = l.ACCTNO,
                    ACCINT = l.ACCINT,
                    ACNAME = l.ACNAME,
                    ACTYPE = l.ACTYPE,
                    BRN = l.BRN,
                    CBAL = l.CBAL,
                    CFADDR = l.CFADDR,
                    CFNAME = l.CFNAME,
                    CIFNO = l.CIFNO,
                    CollateralDesc = l.CollateralDesc,
                    FCODE = l.FCODE,
                    FSEQ = l.FSEQ,
                    INTRATE = l.INTRATE,
                    LID = unitOfwork.LegalStoreProcedure.GetGuidID(),
                    LMCOST = l.LMCOST,
                    LMPRDC = l.LMPRDC,
                    ORGAMT = l.ORGAMT,
                    ORGDT = l.ORGDT,
                    PDDAYS = l.PDDAYS,
                    PENIN3 = l.PENIN3,
                    PENINT = l.PENINT,
                    STATUS = l.STATUS,
                    TYPE = l.TYPE,
                    #endregion
                });
            }
            catch
            {
                throw;
            }
        }
        public static void InsertTrRequestRep(LegalUnitOfWork unitOfwork, trRequest tmpReq, trRequest req)
        {
            try
            {
                unitOfwork.TrRequestRep.Insert(new trRequest
                {
                    Address = req.Address,
                    OwnerApprove = req.OwnerApprove,
                    SeniorLawyer = req.SeniorLawyer,
                    Attachments = req.Attachments,
                    CifNo = req.CifNo,
                    Comment = req.Comment,
                    CreateBy = tmpReq.CreateBy,
                    CreateDate = tmpReq.CreateDate,
                    DefendantNo = req.DefendantNo,
                    ColChecker = req.ColChecker,
                    ColHead = req.ColHead,
                    LawyerID = tmpReq.LawyerID,
                    LawyerStatus = tmpReq.LawyerStatus,
                    LawyerStatusDate = tmpReq.LawyerStatusDate,
                    LegalStatus = tmpReq.LegalStatus,
                    LegalStatusDate = tmpReq.LegalStatusDate,
                    Owner = tmpReq.Owner,
                    RefRequestDate = tmpReq.RefRequestDate,
                    RefRequestNo = tmpReq.RefRequestNo,
                    RefRequestReason = tmpReq.RefRequestReason,
                    RefRequestType = tmpReq.RefRequestType,
                    RequestDate = tmpReq.RequestDate,
                    RequestID = tmpReq.RequestID,
                    RequestNo = tmpReq.RequestNo,
                    RequestReason = req.RequestReason,
                    //RequestReason = tmpReq.RequestReason,
                    RequestStatus = tmpReq.RequestStatus,
                    RequestStatusDate = tmpReq.RequestStatusDate,
                    RequestType = tmpReq.RequestType,
                    UpdateBy = GlobalSession.UserLogin.Emp_Code,
                    UpdateDate = DateTime.Now,
                });
            }
            catch
            {
                throw;
            }
        }
        public static void InsertMasterReassign(trRequest RNO, trLegal LEG,string Attorney, string LawyerID, string AssignBy)
        {
            try
            {
                using (trReAssignHisRep rep = new trReAssignHisRep())
                {
                    var LisReassign = rep.GetbyCondition(RNO.RequestNo);
                    LegalData.trReAssignHi REA = new LegalData.trReAssignHi();
                    REA.ReassignID = Guid.NewGuid();
                    REA.RequestID = RNO.RequestID;
                    REA.RequestNo = RNO.RequestNo;
                    REA.RequestType = RNO.RequestType;
                    REA.RequestStatus = RNO.RequestStatus;
                    REA.LegalID = RNO.LegalID;
                    REA.LegalNo = LEG.LegalNo;
                    REA.LegalStatus = "101";
                    REA.CifNo = RNO.CifNo;
                    REA.Owner = RNO.Owner;
                    REA.SeniorLawyerOwner = RNO.SeniorLawyer;
                    REA.LegalTab = RNO.ActiveTab;
                    REA.Seq = 0;
                    REA.LawyerAttorneyID = Attorney;
                    REA.LawyerID = LawyerID;
                    REA.ReAssignDate = RNO.SeniorLawyerApprove;
                    REA.ReAssignBy = AssignBy;
                    rep.InsertData(REA);
                }
            }
            catch
            {
                throw;
            }
        }
        public static void InsertHistoryReassign(trRequest RNO, trLegal LEG, string LawyerAttorneyID, string LawyerID)
        {
            try
            {
                using (trReAssignHisRep rep = new trReAssignHisRep())
                {
                    var LisReassign = rep.GetbyCondition(RNO.RequestNo);
                    LegalData.trReAssignHi REA = new LegalData.trReAssignHi();
                    REA.ReassignID = Guid.NewGuid();
                    REA.RequestID = RNO.RequestID;
                    REA.RequestNo = RNO.RequestNo;
                    REA.RequestType = RNO.RequestType;
                    REA.RequestStatus = RNO.RequestStatus;
                    REA.LegalID = RNO.LegalID;
                    REA.LegalNo = LEG.LegalNo;
                    REA.LegalStatus = RNO.LegalStatus;
                    REA.CifNo = RNO.CifNo;
                    REA.Owner = RNO.Owner;
                    REA.SeniorLawyerOwner = RNO.SeniorLawyer;
                    REA.LegalTab = RNO.ActiveTab;

                    var ListSeq = rep.GetbySeq(RNO.RequestNo);
                    int seq = Convert.ToInt32(ListSeq.Seq) + 1;
                    REA.Seq = seq;
                    REA.LawyerAttorneyID = LawyerAttorneyID;
                    REA.LawyerID = LawyerID;
                    REA.ReAssignBy = GlobalSession.UserLogin.TH_Name;
                    REA.ReAssignDate = DateTime.Now;
                    REA.Comment = "REASSIGN ครั้งที่ " + seq;
                    rep.InsertData(REA);
                }
            }
            catch
            {
                throw;
            }
        }

        
    }
}