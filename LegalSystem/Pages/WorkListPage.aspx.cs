﻿using LegalData;
using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class WorkListPage : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        private List<sp_GetWorkList> WLs
        {
            get { return Session["WorkList"] != null ? Session["WorkList"] as List<sp_GetWorkList> : null; }
            set { Session["WorkList"] = value; }
        }

        #region Function
        private bool ValidateField()
        {
            if (string.IsNullOrEmpty(txtAccountNo.Value) && string.IsNullOrEmpty(txtCIFNo.Value) && string.IsNullOrEmpty(txtDPD.Value) && string.IsNullOrEmpty(txtName.Value))
            {
                //UcMessageBoxMain.Show("กรุณาเลือกค้นหาด้วย Parameter", "W");
                divgvWorkList.Visible = false;
                Msg.MsgBox(MessageEvent.PleaseInsertParam, MessageEventType.warning, this, this.GetType());

            }
            else
                return true;
            return false;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            UcMessageBoxMain.VisibleControl();
            divgvWorkList.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateField())
                {
                    using (LegalService.sp_GetDataWorkList_Rep rep = new LegalService.sp_GetDataWorkList_Rep())
                    {
                        string DPD = (string.IsNullOrEmpty(txtDPD.Value.Trim()) ? "0" : txtDPD.Value.Trim());
                        string AccNo = txtAccountNo.Value.Trim(); 
                        string CIFNo = txtCIFNo.Value.Trim();  
                        var WLs = rep.GetAll(AccNo, CIFNo, DPD, txtName.Value.Trim()).ToList();

                        if (WLs.Count > 0)
                        {
                            divgvWorkList.Visible = true;
                            gvWorkList.Visible = true;
                            gvWorkList.DataSource = WLs;
                            gvWorkList.DataBind();
                            //if (WLs.Count > 500)
                            //{
                            //    txtCIFNo.Focus();
                            //    Msg.MsgBox(MessageEvent.PleaseSelectOther, MessageEventType.info, this, this.GetType());
                            //}
                        }
                        else
                        {
                            Msg.MsgBox(MessageEvent.NoData, MessageEventType.info, this, this.GetType());
                            gvWorkList.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtAccountNo.Value = txtCIFNo.Value = txtDPD.Value = txtName.Value = string.Empty;
            gvWorkList.DataSource = null;
            gvWorkList.DataBind();
        }

        protected void gvWorkList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sp_GetWorkList wl = e.Row.DataItem as sp_GetWorkList;
                var control = ((Button)(e.Row.FindControl("btnAction")));
                control.CssClass = "btn btn-fill btn-success";
                //control.Text = "Create Request";
                Button btnAction = e.Row.FindControl("btnAction") as Button;
                btnAction.OnClientClick = "return ShowConfirmDialog(this);";
                //var controlV = ((Button)(e.Row.FindControl("btnView")));
                //controlV.CssClass = "btn btn-fill btn-info";
                //controlV.Text = "View";
                

            }
        }

        protected void gvWorkList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Page": break;
                    case "P":
                        string URL = string.Empty;
                        URL = string.Format("RequestPage.aspx?{0}", e.CommandArgument.ToString());
                        Response.Redirect(URL);
                        break;
                
            }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }

        protected void gvWorkList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvWorkList.PageIndex = e.NewPageIndex;
            btnSearch_Click(null, null);
        }
    }
}