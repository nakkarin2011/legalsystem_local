﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirstLetterPage.aspx.cs" Inherits="LegalSystem.Pages.FirstLetterPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <div>
            <asp:Button ID="btnReturn" runat="server" Text="Return" OnClick="btnReturn_Click" />
            <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" />
            <asp:Button ID="btnPreview" runat="server" Text="Preview" OnClick="btnPreview_Click" />
            <asp:Button ID="btnSend" runat="server" Text="Send" OnClick="btnSend_Click" />
        </div>
    </form>
</body>
</html>
