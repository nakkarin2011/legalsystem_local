﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="WorkListPage.aspx.cs" Inherits="LegalSystem.Pages.WorkListPage" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="shortcut icon" href="#" />
    <link href="../Content/plugin/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/plugin/bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <link href="../Content/vendor/material-dashboard.css" rel="stylesheet" />
    <script src="../Scripts/vendor/material-dashboard.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="card ">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 class="card-title">Worklist</h4>
                        </div>
                        <div class="card-body ">
                            <div class="card" style="margin-top: -1px; padding-bottom: 2em">
                                <div class="row col-lg-12">
                                    <div class="col-md-1 text-right">
                                        <div class="tim-typo mt-4-5">
                                            <h5>
                                                <span class="tim-note">Account No</span>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" id="txtAccountNo" runat="server" class="form-control" style="padding-left: 1em;" />
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="tim-typo mt-4-5">
                                            <h5>
                                                <span class="tim-note">CIF No</span>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" id="txtCIFNo" runat="server" class="form-control" style="padding-left: 1em;" />
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="tim-typo mt-4-5">
                                            <h5>
                                                <span class="tim-note">DPD</span>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" id="txtDPD" runat="server" class="form-control" style="padding-left: 1em;" />
                                        </div>
                                    </div>
                                    <div class="col-md-auto">
                                        <div class="tim-typo mt-4-5">
                                            <h5>
                                                <span class="tim-note">Name</span>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" id="txtName" runat="server" class="form-control" style="padding-left: 1em;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-lg-12" style="padding-bottom: 2em"></div>
                                <div class="row col-lg-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-fill btn-info" OnClick="btnSearch_Click" />
                                            &nbsp;
                                            <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-fill btn-warning" OnClick="btnClear_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divgvWorkList" class="row" runat="server">
                                <div class="card ">
                                    <div class="card-body ">
                                        <div class="col-md-12">
                                            <div class="material-datatables">
                                                <asp:GridView ID="gvWorkList" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                    ShowHeaderWhenEmpty="true" DataKeyNames="ID" AllowPaging="true" PageSize="10" AllowSorting="false" OnPageIndexChanging="gvWorkList_PageIndexChanging"
                                                    OnRowCommand="gvWorkList_RowCommand" OnRowDataBound="gvWorkList_RowDataBound" PageIndex="1">
                                                    <Columns>
                                                        <asp:BoundField DataField="ACCTNO" SortExpression="ACCTNO" HeaderText="Account No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="A_CFCIFN" SortExpression="A_CFCIFN" HeaderText="CIF No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="A_CFNAME" SortExpression="A_CFNAME" HeaderText="CIF Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="A_STATUS" SortExpression="A_STATUS" HeaderText="Acc Status" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="RelationName" SortExpression="RelationName" HeaderText="Relationship Type" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="PDDAYS" SortExpression="PDDAYS" HeaderText="DPD" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:N0}"></asp:BoundField>
                                                        <asp:BoundField DataField="TYPE" SortExpression="TYPE" HeaderText="Loan Type" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="RequestNo" SortExpression="RequestNo" HeaderText="Request No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="LegalNo" SortExpression="LegalNo" HeaderText="Legal No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <asp:BoundField DataField="LegalStatus" SortExpression="LegalStatus" HeaderText="Legal Status" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                        <%--<asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnView" runat="server" Text="View" class="btn btn-fill btn-info" CommandName="VIEW" CommandArgument='<%# "&CIF="+Eval("A_CFCIFN")+"&ACCT="+Eval("ACCTNO") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hidACC" runat="server" Value='<%#Eval("ACCTNO")%>' />
                                                                <%--<asp:Button ID="btnView" runat="server" Text="" class="btn btn-fill btn-info" CommandName='<%# Eval("A_CFRELA") %>' CommandArgument='<%# "&ACCT="+Eval("ACCTNO") %>' />--%>
                                                                <asp:Button ID="btnAction" runat="server" Text="Create Request" class="btn btn-fill btn-info" CommandName='<%# Eval("A_CFRELA") %>' CommandArgument='<%# "&CIF="+Eval("A_CFCIFN")+"&ACCT="+Eval("ACCTNO") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="false" ForeColor="Black" />
                                                    <HeaderStyle HorizontalAlign="Left" Font-Bold="true"></HeaderStyle>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="loading-visible" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: white; opacity: 0.75;">
                        <p>
                            <img style="padding: 10px; position: fixed; top: 50%; left: 50%;" src="../Resources/Preloader_4.gif" />
                        </p>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
    <div id="fade" class="black_overlay"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
