﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="CourtRate.aspx.cs" Inherits="LegalSystem.Pages.Setting.CourtRate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- GridView --%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="card">
        <div class="card-header ">
            <h3>Court Rate</h3>
        </div>
        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="material-datatables">
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" placeholder="Search Records" />
                                    <asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-info" OnClick="btnserch_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-info"
                                        data-toggle="modal" data-target="#myModal" OnClick="btnAdd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvCourtRate" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                    DataKeyNames="CourtRateID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png" PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                    OnRowDataBound="gvCourtRate_RowDataBound" OnPageIndexChanging="gvCourtRate_PageIndexChanging" OnRowCommand="gvCourtRate_RowCommand"
                                    AllowSorting="true" OnRowDeleting="gvCourtRate_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField SortExpression="MinAmt" HeaderStyle-Font-Bold="true" HeaderText="Min Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lbMinWage" runat="server" Text='<%#Eval("MinAmt")%>'></asp:Label>
                                                <asp:HiddenField ID="hidRateID" runat="server" Value='<%#Eval("CourtRateID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="MaxAmt" HeaderText="Max Amount" SortExpression="MaxAmt" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="Devide" HeaderText="Devide" SortExpression="Devide" />
                                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Condition" SortExpression="Condition">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCondition" runat="server" Text='<%#Eval("Condition")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PayAmount" HeaderText="PayAmount" SortExpression="PayAmount" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                    CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                    CommandName="EditCommand" CommandArgument='<%# Eval("CourtRateID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("CourtRateID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="gvCourtRate" EventName="RowCommand" />--%>
                    <asp:AsyncPostBackTrigger ControlID="gvCourtRate" />
                    <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Min Amount</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidCourtRateID" runat="server" />
                                        <input id="txtMinAmount" runat="server" type="number" class="form-control" />
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Max Amount</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtMaxAmount" runat="server" type="number" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Percentage</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtPercentage" runat="server" type="number" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Devide</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtDevide" runat="server" type="number" step="any" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Condition</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlCondition" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">PayAmount</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtPayAmount" runat="server" type="number" step="any" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;
                    <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
