﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalEntities;
using System.ComponentModel;
using LegalSystem.Common;
using LegalService;
using LE = LegalEntities;

namespace LegalSystem.Pages.Setting
{
    public partial class SettingType : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private string paramType;
        public string ParamType
        {
            get
            {
                return (string)Session["ParamType"];
            }
            set
            {

                Session["ParamType"] = value;
            }
        }

        private DataTable dtGridview;
        public DataTable DtGritView
        {
            get
            {
                return (DataTable)Session["GVList"];
            }
            set
            {
                Session["GVList"] = value;
            }
        }

        private List<LE.AssetType> assetTypeList;
        public List<LE.AssetType> AssetTypeList
        {
            get
            {
                return (List<LE.AssetType>)Session["AssetTypeList"];
            }
            set
            {
                Session["AssetTypeList"] = value;
            }
        }
        public List<LE.RequestType> RequestTypeList
        {
            get
            {
                return (List<LE.RequestType>)Session["RequestTypeList"];
            }
            set
            {
                Session["RequestTypeList"] = value;
            }
        }

        private List<LE.AttorneyType> attorneyTypeList;
        public List<LE.AttorneyType> AttorneyTypeList
        {
            get
            {
                return (List<LE.AttorneyType>)Session["AttorneyTypeList"];
            }
            set
            {
                Session["AttorneyTypeList"] = value;
            }
        }

        private List<LE.CourtJudgement> courtJudgementList;
        public List<LE.CourtJudgement> CourtJudgementList
        {
            get
            {
                return (List<LE.CourtJudgement>)Session["CourtJudgementList"];
            }
            set
            {
                Session["CourtJudgementList"] = value;
            }
        }

        private List<LE.CourtType> courtTypeList;
        public List<LE.CourtType> CourtTypeList
        {
            get
            {
                return (List<LE.CourtType>)Session["CourtTypeList"];
            }
            set
            {
                Session["CourtTypeList"] = value;
            }
        }

        private List<LE.PaymentType> paymentTypeList;
        public List<LE.PaymentType> PaymentTypeList
        {
            get
            {
                return (List<LE.PaymentType>)Session["PaymentTypeList"];
            }
            set
            {
                Session["PaymentTypeList"] = value;
            }
        }

        private List<LE.ProductType> productTypeList;
        public List<LE.ProductType> ProductTypeList
        {
            get
            {
                return (List<LE.ProductType>)Session["ProductTypeList"];
            }
            set
            {
                Session["ProductTypeList"] = value;
            }
        }

        private List<LE.RequestReason> requestReasonList;
        public List<LE.RequestReason> RequestReasonList
        {
            get
            {
                return (List<LE.RequestReason>)Session["RequestReasonList"];
            }
            set
            {
                Session["RequestReasonList"] = value;
            }
        }

        private List<LE.RevenueType> revenueTypeList;
        public List<LE.RevenueType> RevenueTypeList
        {
            get
            {
                return (List<LE.RevenueType>)Session["RevenueTypeList"];
            }
            set
            {
                Session["RevenueTypeList"] = value;
            }
        }

        private List<LE.StatementAccType> feeList;
        public List<LE.StatementAccType> FeeList
        {
            get
            {
                return (List<LE.StatementAccType>)Session["FeeList"];
            }
            set
            {
                Session["FeeList"] = value;
            }
        }
        public List<LE.CourtZone> CourtZoneList
        {
            get
            {
                return (List<LE.CourtZone>)Session["CourtZoneList"];
            }
            set
            {
                Session["CourtZoneList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            lbRed.InnerText = string.Empty;
            if (!IsPostBack)
            {
                paramType = Request.QueryString["type"];
                ParamType = paramType;
                //ParamType = "AT"; // for test

                InitData();
            }
        }

        public void InitData()
        {
            switch (ParamType)
            {
                case "RT":
                    TypeHeader.InnerText = "Request Type";
                    break;
                case "DT":
                    TypeHeader.InnerText = "Document Type";
                    break;
                case "AT":
                    TypeHeader.InnerText = "Asset Type";
                    break;
                case "AN":
                    TypeHeader.InnerText = "Attorney Type";
                    break;
                case "CJ":
                    TypeHeader.InnerText = "Court Judgement";
                    break;
                case "CT":
                    TypeHeader.InnerText = "Court Type";
                    break;
                case "FE":
                    TypeHeader.InnerText = "Fee Type";
                    break;
                case "PD":
                    TypeHeader.InnerText = "Product Type";
                    break;
                case "PT":
                    TypeHeader.InnerText = "Payment Type";
                    break;
                case "RN":
                    TypeHeader.InnerText = "Revenue Type";
                    break;
                case "RR":
                    TypeHeader.InnerText = "Request Reason";
                    break;
                case "CZ":
                    TypeHeader.InnerText = "Court Zone";
                    break;
            }
            GetAllaType();
            //Response.Redirect("SettingType.aspx?type=" + ParamType, true);
        }

        public void ConvertToDataTable()
        {
            DataTable table = new DataTable();
            PropertyDescriptorCollection props;
            object[] values;

            switch (ParamType)
            {
                case "RT":
                    props = TypeDescriptor.GetProperties(typeof(RequestType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (RequestType item in RequestTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "AT":
                    props = TypeDescriptor.GetProperties(typeof(AssetType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (AssetType item in AssetTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "AN":
                    props = TypeDescriptor.GetProperties(typeof(AttorneyType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (AttorneyType item in AttorneyTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "CJ":
                    props = TypeDescriptor.GetProperties(typeof(CourtJudgement));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (CourtJudgement item in CourtJudgementList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "CT":
                    props = TypeDescriptor.GetProperties(typeof(CourtType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (CourtType item in CourtTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "FE":
                    props = TypeDescriptor.GetProperties(typeof(StatementAccType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (StatementAccType item in FeeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "PD":
                    props = TypeDescriptor.GetProperties(typeof(ProductType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (ProductType item in ProductTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "PT":
                    props = TypeDescriptor.GetProperties(typeof(PaymentType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (LE.PaymentType item in PaymentTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "RN":
                    props = TypeDescriptor.GetProperties(typeof(RevenueType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (RevenueType item in RevenueTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "RR":
                    props = TypeDescriptor.GetProperties(typeof(RequestReason));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (RequestReason item in RequestReasonList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
                case "CZ":
                    props = TypeDescriptor.GetProperties(typeof(LE.CourtZone));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (LE.CourtZone item in CourtZoneList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    break;
            }
            // set name for gridview
            table.Columns[0].ColumnName = "TypeID";
            table.Columns[1].ColumnName = "TypeName";
            DtGritView = table;
        }

        public void BindGrid()
        {
            ConvertToDataTable();
            gvTypeSetting.DataSource = DtGritView;
            gvTypeSetting.DataBind();
        }
        
        #region Initial
        public void GetAllaType()
        {
            switch (ParamType)
            {
                case "RT":
                    using (RequestTypeRep rep = new RequestTypeRep())
                    {
                        List<RequestType> P = rep.GetAllRequestTyp();
                        RequestTypeList = P;
                        BindGrid();
                    }
                    break;
                case "DT":
                    using (DocTypeRep rep = new DocTypeRep())
                    {
                        var p = rep.GetAll();
                        DtGritView = Utility.ConvertToDataTable(p);
                        DtGritView.Columns[0].ColumnName = "TypeID";
                        DtGritView.Columns[1].ColumnName = "TypeName";
                        gvTypeSetting.DataSource = DtGritView;
                        gvTypeSetting.DataBind();
                    }
                    break;
                case "AT":
                    using (AssetTypeRep rep = new AssetTypeRep())
                    {
                        List<AssetType> P = rep.GetAllAssetType();
                        AssetTypeList = P;
                        BindGrid();
                    }
                    break;
                case "AN":
                    using (AttorneyTypeRep rep = new AttorneyTypeRep())
                    {
                        attorneyTypeList = rep.GetAllAttorneyType();
                        AttorneyTypeList = attorneyTypeList;
                        BindGrid();
                    }
                    break;
                case "CJ":
                    using (CourtJudgementRep rep = new CourtJudgementRep())
                    {
                        courtJudgementList = rep.GetAllCourtJudgement();
                        CourtJudgementList = courtJudgementList;
                        BindGrid();
                    }
                    break;
                case "CT":
                    using (CourtTypeRep rep = new CourtTypeRep())
                    {
                        courtTypeList = rep.GetAllCourtType();
                        CourtTypeList = courtTypeList;
                        BindGrid();
                    }
                    break;
                case "FE":
                    using (StatementAccTypeRep rep = new StatementAccTypeRep())
                    {
                        feeList = rep.GetAllStatementAccType();
                        FeeList = feeList;
                        BindGrid();
                    }
                    break;
                case "PD":
                    using (ProducTypeRep rep = new ProducTypeRep())
                    {
                        productTypeList = rep.GetAllProductType();
                        ProductTypeList = productTypeList;
                        BindGrid();
                    }
                    break;
                case "PT":
                    using (PaymentTypeRep rep = new PaymentTypeRep())
                    {
                        paymentTypeList = rep.GetAllPaymentType();
                        PaymentTypeList = paymentTypeList;
                        BindGrid();
                    }
                    break;
                case "RN":
                    using (RevenueTypeRep rep = new RevenueTypeRep())
                    {
                        revenueTypeList = rep.GetAllRevenueType();
                        RevenueTypeList = revenueTypeList;
                        BindGrid();
                    }
                    break;
                case "RR":
                    using (RequestReasonRep rep = new RequestReasonRep())
                    {
                        requestReasonList = rep.GetAllRequestReason();
                        RequestReasonList = requestReasonList;
                        BindGrid();
                    }
                    break;
                case "CZ":
                    using (CourtZoneRep rep = new CourtZoneRep())
                    {
                        CourtZoneList = rep.GetAllCourtZone();
                        BindGrid();
                    }
                    break;
            }

        }
        #endregion

        #region GridView Event
        protected void gvTypeSetting_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvTypeSetting_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTypeSetting.PageIndex = e.NewPageIndex;
            gvTypeSetting.DataSource = DtGritView;
            gvTypeSetting.DataBind();
        }

        protected void gvTypeSetting_RowCommand(object sender, GridViewCommandEventArgs e)
        {

                if (e.CommandName == "EditCommand")
                {
                    CommandName = "Edit";

                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;

                    string typeid = (gvTypeSetting.Rows[index].FindControl("hidTypeID") as HiddenField).Value.Trim();
                    string typename = (gvTypeSetting.Rows[index].FindControl("ibTypeName") as Label).Text.Trim();

                    hidTypeID.Value = typeid;
                    txtTypeName.Value = typename;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { openModal(); });", true);
                }
                else if (e.CommandName == "Delete")
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;

                    string stypeid = (gvTypeSetting.Rows[index].FindControl("hidTypeID") as HiddenField).Value.Trim();
                    int typeid = Convert.ToInt32(stypeid);

                switch (ParamType)
                {
                    case "RT":
                        using (RequestTypeRep rep = new RequestTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "DT":
                        using (DocTypeRep rep = new DocTypeRep())
                        {
                            rep.DeleteData(new LegalData.DocumentType
                            {
                                DocTypeID = typeid
                            });
                        }
                        break;

                    case "AT":
                        using (AssetTypeRep rep = new AssetTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "AN":
                        using (AttorneyTypeRep rep = new AttorneyTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "CJ":
                        using (CourtJudgementRep rep = new CourtJudgementRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "CT":
                        using (CourtTypeRep rep = new CourtTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "FE":
                        using (StatementAccTypeRep rep = new StatementAccTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "PD":
                        using (ProducTypeRep rep = new ProducTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "PT":
                        using (PaymentTypeRep rep = new PaymentTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "RN":
                        using (RevenueTypeRep rep = new RevenueTypeRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "RR":
                        using (RequestReasonRep rep = new RequestReasonRep())
                        {
                            rep.DeleteData(typeid);
                        }
                        break;
                }
                }
        }

        protected void gvTypeSetting_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllaType();
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            txtTypeName.Value = string.Empty;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { openModal(); });", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTypeName.Value))
                {
                    lbRed.InnerText = "This field is required.";
                    return;
                }
               
                if (CommandName.Equals("Add"))
                {
                    switch (ParamType)
                    {
                        case "RT":
                            using (RequestTypeRep rep = new RequestTypeRep())
                            {
                                rep.insertData(new RequestType
                                {
                                    ReqTypeName = txtTypeName.Value.Trim()
                                });
                            }
                            break;
                        case "DT":
                            using (DocTypeRep rep = new DocTypeRep())
                            {
                                rep.InsertData(new LegalData.DocumentType
                                {
                                    DocTypeName = txtTypeName.Value.Trim()
                                });
                            }
                            break;

                        case "AT":
                            using (AssetTypeRep rep = new AssetTypeRep())
                            {
                                AssetType ass = new AssetType();
                                ass.AssetTypeName = txtTypeName.Value.Trim();
                                rep.insertData(ass);
                            }
                            break;
                        case "AN":
                            using (AttorneyTypeRep rep = new AttorneyTypeRep())
                            {
                                AttorneyType att = new AttorneyType();
                                att.AttorneyTypeName = txtTypeName.Value.Trim();
                                rep.insertData(att);
                            }
                            break;
                        case "CJ":
                            using (CourtJudgementRep rep = new CourtJudgementRep())
                            {
                                CourtJudgement courtjudgement = new CourtJudgement();
                                courtjudgement.CJName = txtTypeName.Value.Trim();
                                rep.insertData(courtjudgement);
                            }
                            break;
                        case "CT":
                            using (CourtTypeRep rep = new CourtTypeRep())
                            {
                                CourtType courttype = new CourtType();
                                courttype.CourtTypeName = txtTypeName.Value.Trim();
                                rep.insertData(courttype);
                            }
                            break;
                        case "FE":
                            using (StatementAccTypeRep rep = new StatementAccTypeRep())
                            {
                                StatementAccType type = new StatementAccType();
                                type.StatementName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "PD":
                            using (ProducTypeRep rep = new ProducTypeRep())
                            {
                                ProductType type = new ProductType();
                                type.PrdTypeName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "RN":
                            using (RevenueTypeRep rep = new RevenueTypeRep())
                            {
                                RevenueType type = new RevenueType();
                                type.RevenueTypeName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "RR":
                            using (RequestReasonRep rep = new RequestReasonRep())
                            {
                                RequestReason type = new RequestReason();
                                type.ReasonName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                    }
                }
                else if (CommandName.Equals("Edit"))
                {
                    int _hid = Convert.ToInt32(hidTypeID.Value.Trim());

                    switch (ParamType)
                    {
                        case "RT":
                            using (RequestTypeRep rep = new RequestTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                rep.insertData(new RequestType
                                {
                                    ReqTypeID = id,
                                    ReqTypeName = txtTypeName.Value.Trim()
                                });
                            }
                            break;
                        case "DT":
                            using (DocTypeRep rep = new DocTypeRep())
                            {
                                rep.UpdateData(new LegalData.DocumentType
                                {
                                    DocTypeID = _hid,
                                    DocTypeName = txtTypeName.Value.Trim()
                                });
                            }
                            break;

                        case "AT":
                            using (AssetTypeRep rep = new AssetTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                AssetType ass = new AssetType();
                                ass.AssetTypeID = id;
                                ass.AssetTypeName = txtTypeName.Value.Trim();
                                rep.insertData(ass);
                            }
                            break;
                        case "AN":
                            using (AttorneyTypeRep rep = new AttorneyTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                AttorneyType att = new AttorneyType();
                                att.AttorneyTypeID = id;
                                att.AttorneyTypeName = txtTypeName.Value.Trim();
                                rep.insertData(att);
                            }
                            break;
                        case "CJ":
                            using (CourtJudgementRep rep = new CourtJudgementRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                CourtJudgement courtjudgement = new CourtJudgement();
                                courtjudgement.CJName = txtTypeName.Value.Trim();
                                rep.insertData(courtjudgement);
                            }
                            break;
                        case "CT":
                            using (CourtTypeRep rep = new CourtTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                CourtType courttype = new CourtType();
                                courttype.CourtTypeName = txtTypeName.Value.Trim();
                                rep.insertData(courttype);
                            }
                            break;
                        case "FE":
                            using (StatementAccTypeRep rep = new StatementAccTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                StatementAccType type = new StatementAccType();
                                type.StatementName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "PD":
                            using (ProducTypeRep rep = new ProducTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                LE.ProductType type = new LE.ProductType();
                                type.PrdTypeName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "PT":
                            using (PaymentTypeRep rep = new PaymentTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                LE.PaymentType type = new LE.PaymentType();
                                type.PaymentTypeName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "RN":
                            using (RevenueTypeRep rep = new RevenueTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                RevenueType type = new RevenueType();
                                type.RevenueTypeName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                        case "RR":
                            using (RequestReasonRep rep = new RequestReasonRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidTypeID.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                RequestReason type = new RequestReason();
                                type.ReasonName = txtTypeName.Value.Trim();
                                rep.insertData(type);
                            }
                            break;
                    }
                }
                GetAllaType();

                Msg.MsgBox(MessageEvent.Succesfully, MessageEventType.success, this, this.GetType());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { closeModal(); });", true);
            }
            catch(Exception ex)
            {
                Msg.MsgBox(ex.Message, MessageEventType.error, this, this.GetType());
            }


            //up2.Update();
            //UpdatePanel1.Update();
            //Response.Redirect("SettingType.aspx?type=" + ParamType, true);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            switch (ParamType)
            {
                case "RT":
                    using (RequestTypeRep rep = new RequestTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        RequestTypeList = rep.GetAssetTypeWithCondition(scondition);
                        BindGrid();
                    }

                    break;

                case "DT":
                    using (DocTypeRep rep = new DocTypeRep())
                    {
                        var p = rep.SearchData(new LegalData.DocumentType
                        {
                            DocTypeName = txtfilter.Value.Trim()
                        });

                        DtGritView = Utility.ConvertToDataTable(p);
                        DtGritView.Columns[0].ColumnName = "TypeID";
                        DtGritView.Columns[1].ColumnName = "TypeName";
                        gvTypeSetting.DataSource = DtGritView;
                        gvTypeSetting.DataBind();
                    }
                    break;
                case "AT":
                    using (AssetTypeRep rep = new AssetTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        assetTypeList = rep.GetAssetTypeWithCondition(scondition);
                        AssetTypeList = assetTypeList;
                        BindGrid();
                    }
                    break;
                case "AN":
                    using (AttorneyTypeRep rep = new AttorneyTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        attorneyTypeList = rep.GetAttorneyTypeWithCondition(scondition);
                        AttorneyTypeList = attorneyTypeList;
                        BindGrid();
                    }
                    break;
                case "CJ":
                    using (CourtJudgementRep rep = new CourtJudgementRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        courtJudgementList = rep.GetCourtJudgementWithCondition(scondition);
                        CourtJudgementList = courtJudgementList;
                        BindGrid();
                    }
                    break;
                case "CT":
                    using (CourtTypeRep rep = new CourtTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        courtTypeList = rep.GetCourtTypeWithCondition(scondition);
                        CourtTypeList = courtTypeList;
                        BindGrid();
                    }
                    break;
                case "FE":
                    using (StatementAccTypeRep rep = new StatementAccTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        feeList = rep.GetStatementAccTypeWithCondition(scondition);
                        FeeList = feeList;
                        BindGrid();
                    }
                    break;
                case "PD":
                    using (ProducTypeRep rep = new ProducTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        productTypeList = rep.GetProductTypeWithCondition(scondition);
                        ProductTypeList = productTypeList;
                        BindGrid();
                    }
                    break;
                case "PT":
                    using (PaymentTypeRep rep = new PaymentTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        paymentTypeList = rep.GetPaymentTypeWithCondition(scondition);
                        PaymentTypeList = paymentTypeList;
                        BindGrid();
                    }
                    break;
                case "RN":
                    using (RevenueTypeRep rep = new RevenueTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        revenueTypeList = rep.GetRevenueTypeIDWithCondition(scondition);
                        RevenueTypeList = revenueTypeList;
                        BindGrid();
                    }
                    break;
                case "RR":
                    using (RequestReasonRep rep = new RequestReasonRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        requestReasonList = rep.GetRequestReasonWithCondition(scondition);
                        RequestReasonList = requestReasonList;
                        BindGrid();
                    }
                    break;
            }
        
        }

        #endregion

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtfilter.Value = string.Empty;
        }
    }
}