﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class InboxPoolPage : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        private List<sp_GetRequest> RQs
        {
            get { return Session["ListRequest"] != null ? Session["ListRequest"] as List<sp_GetRequest> : null; }
            set { Session["ListRequest"] = value; }
        }

        #region Function
        private void GetDataUI()
        {
            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
            {
                List<msRequestType> RT = unitOfWork.MSRequestTypeRep.GetAllRequestType();
                ddlRequestType.DataSource = RT;
                ddlRequestType.DataBind();
                ddlRequestType.SelectedIndex = 0;
            }
        }

        private void Validate()
        {
            if (string.IsNullOrEmpty(txtRequestNo.Value) && string.IsNullOrEmpty(txtCIF.Value) && string.IsNullOrEmpty(txtRequestDate.Value)
                && ddlRequestType.SelectedIndex == 0)
            {
                throw new Exception("");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDataUI();
                btnSearch_Click(sender, e);
                UcMessageBoxMain.VisibleControl();
            }
            else { }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dCardTable.Visible = true;
                //Validate();
                string requestType = (ddlRequestType.SelectedValue == "0") ? "" : ddlRequestType.SelectedValue;
                string requestNo = txtRequestNo.Value.Trim();
                string requestDate = (string.IsNullOrEmpty(txtRequestDate.Value.Trim())) ? "" : Convert.ToDateTime(txtRequestDate.Value.Trim()).AddYears(-543).ToString("yyyy-MM-dd");
                string cif = txtCIF.Value.Trim();
                string status = "F3";
                if (GlobalSession.GetPermission == GlobalSession.PermissionID.SeniorLawyer)
                    status = "F4";

                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    RQs = unitOfWork.LegalStoreProcedure.GetRequest(requestType
                            , requestNo
                            , requestDate
                            , cif
                            , status
                            , "").ToList();
                    var ListRQs = RQs.Where(x => string.IsNullOrEmpty(x.SeniorLawyer)).ToList(); //*** Add .b 26082019
                    if (GlobalSession.GetPermission == GlobalSession.PermissionID.SeniorLawyer)
                    {
                        //ListRQs = RQs.Where(x => !string.IsNullOrEmpty(x.SeniorLawyer) && x.SeniorLawyerReceive == null && MasterStatusHelper.StatusTypeID_SL.Contains(x.RequestType) || !string.IsNullOrEmpty(x.SeniorLawyer) && x.RequestStatus == MasterStatusHelper.StatusReturn).ToList();

                        ListRQs = RQs.Where(x => !string.IsNullOrEmpty(x.SeniorLawyer) && x.SeniorLawyerReceive == null && x.RequestStatus == "F4").ToList();
                        RQs = unitOfWork.LegalStoreProcedure.GetRequest(requestType
                            , requestNo
                            , requestDate
                            , cif
                            , MasterStatusHelper.StatusReturntoSeniorLawyer
                            , "").ToList();

                        ListRQs.AddRange(RQs.ToList());
                    }
                    else if (GlobalSession.GetPermission != GlobalSession.PermissionID.LegalSpecialist)
                        ListRQs = RQs.Where(x => !string.IsNullOrEmpty(x.SeniorLawyer)).ToList();


                    if (ListRQs.Count <= 0)
                    {
                        UcMessageBoxMain.Show(string.Format("{0}", MessageEvent.NoData), "Q");
                        dCardTable.Visible = false;
                    }
                    else
                    {
                        gvInquiry.DataSource = ListRQs.OrderBy(x => x.RequestNo).ToList();
                        gvInquiry.DataBind();
                        UcMessageBoxMain.VisibleControl();
                    }
                }
            }
            catch (Exception ex)
            {
                //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlRequestType.SelectedIndex = 0;
            txtRequestNo.Value = txtRequestDate.Value = txtCIF.Value = string.Empty;
            dCardTable.Visible = false;
            gvInquiry.DataSource = null;
            gvInquiry.DataBind();
        }

        protected void gvInquiry_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sp_GetRequest r = e.Row.DataItem as sp_GetRequest;
                var control = ((Button)(e.Row.FindControl("btnSubmit")));
                //control.Visible = false;//ปิดปุ่ม Register ไว้ก่อน
                if (GlobalSession.GetPermission == GlobalSession.PermissionID.SeniorLawyer)//SeniorLawyer
                    //control.Text = control.CommandName = "GetTask";
                    control.Visible = false;
                else if (GlobalSession.GetPermission == GlobalSession.PermissionID.LegalSpecialist)//Lawyer Specialist
                    //control.Text = control.CommandName = "Approve";
                    control.Visible = false;
                //else if (GlobalSession.GetPermission == GlobalSession.PermissionID.Lawyer)//Lawyer Specialist
                //    control.Text = control.CommandName = "Accept";
                else//Senior Lawyer    
                    control.Text = control.CommandName = "Register";
            }
        }

        protected void gvInquiry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == MasterECommandNameHelper.View)
                {
                    string URL = string.Empty;
                    URL = string.Format("RequestPage.aspx?rid={0}", e.CommandArgument.ToString()); //*** ADD 26082019 .b ***//
                    Response.Redirect(URL);
                }
                else if (e.CommandName == MasterECommandNameHelper.GetTask)
                {
                    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                    {
                        trRequest R = unitOfWork.TrRequestRep.GetRequestByID(e.CommandArgument.ToString());
                        if (R != null)
                        {
                            if (R.RequestStatus == MasterStatusHelper.StatusReturntoSeniorLawyer) R.RequestStatus = MasterStatusHelper.StatusRegistered;
                            R.SeniorLawyer = GlobalSession.UserLogin.Emp_Code;
                            R.SeniorLawyerReceive = DateTime.Now;
                            unitOfWork.Save();
                        }
                    }
                    Msg.MsgReturn(MessageEvent.GetTask, Page, this, MasterPageHelper.InboxPoolPage, MessageEventType.success);

                }
                else if (e.CommandName == "Approve")
                {
                    string rid = e.CommandArgument.ToString();
                    //string URL = string.Format("InboxPage.aspx?rno={0}", e.CommandArgument.ToString());
                    //string URL = string.Format("InboxPage.aspx");
                    //string URL = string.Format("RequestPage.aspx?Approve=1");
                    //Response.Redirect(URL);
                    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                    {
                        trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                        //trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(e.CommandArgument.ToString())).FirstOrDefault();
                        int status = (Req.RequestStatus_ + 1);

                        //Req.LegalStatus = trL.LegalStatus;
                        Req.LegalStatusDate = DateTime.Now;
                        Req.LegalSpecial = GlobalSession.UserLogin.Emp_Code;
                        Req.SeniorLawyer = "0";
                        Req.LegalSpecialApprove = DateTime.Now;
                        //Req.RequestStatus = "F" + status;//Approve
                        Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        Req.UpdateDate = DateTime.Now;
                        unitOfWork.Save();
                    }
                    Msg.MsgReturn(MessageEvent.Approve, Page, this, MasterPageHelper.InboxPage /*"InboxPage.aspx"*/, MessageEventType.success);
                }
                else if (e.CommandName == MasterECommandNameHelper.Accept /*"Accept"*/)
                {
                    string rid = e.CommandArgument.ToString();
                    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                    {
                        trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                        //trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(e.CommandArgument.ToString())).FirstOrDefault();
                        int status = (Req.RequestStatus_ + 1);

                        Req.LawyerID = GlobalSession.UserLogin.Emp_Code;
                        Req.LawyerIDReceive = DateTime.Now;
                        Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        Req.UpdateDate = DateTime.Now;
                        unitOfWork.Save();
                    }
                    Msg.MsgReturn(MessageEvent.Accept, Page, this, MasterPageHelper.InboxPage /*"InboxPage.aspx"*/, MessageEventType.success);
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }

        protected void gvInquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvInquiry.PageIndex = e.NewPageIndex;
            btnSearch_Click(null, null);

        }

    }
}