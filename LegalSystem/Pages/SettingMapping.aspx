﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="SettingMapping.aspx.cs" Inherits="LegalSystem.Pages.Setting.SettingMapping1" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-12">
       <asp:UpdatePanel ID="UpdatePanel3" runat="server">
           <ContentTemplate>
                <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                 <div class="card ">
                    <div class="card-header card-header-tabs card-header-info">
                        <h4 class="card-title" id="TypeHeader" runat="server">Inbox</h4>
                        <p class="card-category"><span class="tim-note" id="categoryText" runat="server"></span></p>
                    </div>
                    <div class="card-body">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnserch">
                            <div class="row " style="margin-bottom: -20px;">
                              <div class="col-md-2 col-sm-2 text-right">
                                 <div class="tim-typo mt-4-5">
                                     <h5>
                                         <span class="tim-note" id="spText" runat="server"></span>
                                     </h5>
                                 </div>
                             </div>
                              <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                 <div class="form-group">
                                      <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" />
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <asp:LinkButton ID="btnserch" runat="server" CssClass="btn btn-info" OnClick="btnserch_Click">
                                         <i class="fa fa-search"></i>&nbsp;Search
                                     </asp:LinkButton>
                                     &nbsp;
                                     <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-fill btn-warning" OnClick="btnClear_Click">
                                         <i class="fa fa-trash"></i>&nbsp;Clear
                                     </asp:LinkButton>
                                     &nbsp;
                                     <asp:LinkButton id="btnAdd" runat="server" class="btn btn-success" OnClick="btnAdd_Click">
                                         <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add 
                                     </asp:LinkButton>
                                 </div>
                             </div>
                        </div>
                        </asp:Panel>

                        
                         <div class="row">
                             <div class="card ">
                                  <div class="card-body">
                                      <div class="col-md-12">
                                          <div class="material-datatables table-responsive">
                                           <asp:GridView ID="gvTypeMapping" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-striped table-no-bordered table-hover"
 	                                            GridLines="None"
 	                                            ShowHeaderWhenEmpty="true"
	                                            AllowPaging="true"
 	                                            PageSize="10"
  	                                            AllowSorting="false"
                                                OnRowDataBound="gvTypeMapping_RowDataBound"
	                                            OnPageIndexChanging="gvTypeMapping_PageIndexChanging" 
                                                OnRowCommand="gvTypeMapping_RowCommand"
	                                            OnRowDeleting="gvTypeMapping_RowDeleting"
	                                            PageIndex="1">
                                                    <Columns>
                                                        <asp:TemplateField SortExpression="Type1Name" HeaderStyle-Font-Bold="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbType1Name" runat="server" Text='<%#Eval("Type1Name")%>'></asp:Label>
                                                                <asp:HiddenField ID="hidType1ID" runat="server" Value='<%#Eval("Type1ID")%>' />
                                                                <asp:HiddenField ID="GridHidMapID" runat="server" Value='<%#Eval("MapID")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="Type2Name" HeaderStyle-Font-Bold="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbType2Name" runat="server" Text='<%#Eval("Type2Name")%>'></asp:Label>
                                                                <asp:HiddenField ID="hidType2ID" runat="server" Value='<%#Eval("Type2ID")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                                    CausesValidation="false" 
                                                                    CommandName="EditCommand" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" CommandName="Delete" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:GridView>
                                          </div>
                                     </div>
                                  </div>
                             </div>
                        </div>            
                    </div>
                 </div>
           </ContentTemplate>
           <%-- <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvTypeMapping" EventName="RowCommand" />
           </Triggers>--%>
        </asp:UpdatePanel>
     </div>
  </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width:700px;">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                   <%-- Modal --%>
                   <ContentTemplate>

                        <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="card-body">
                  
                                <div class="form-group bmd-form-group">
                            <div class="input-group">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="material-icons">
                                            <label id="lbType2Name_modal" runat="server"></label>
                                        </i>
                                    </div>
                                  </div>
                                   <asp:HiddenField ID="hidMapID" runat="server" />
                                   <asp:HiddenField ID="hidID2" runat="server" />
                                   <input id="txtType2" runat="server" type="text" class="form-control" />
                                   <asp:DropDownList ID="ddltype2" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">
                                           <i class="material-icons">
                                              <label id ="lbRed2" runat="server" class="text-danger"></label>
                                           </i>
                                       </div>
                                    </div>
                                  
                            </div>
                        </div>
                                <div class="form-group bmd-form-group">
                                    <div class="input-group">
                                          <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="material-icons">
                                                   <label id="lbType1Name_modal" runat="server"></label>
                                                </i>
                                            </div>
                                          </div>
                                          <asp:HiddenField ID="hidID1" runat="server" />
                                          <input id="txtType1" runat="server" type="text" class="form-control" />
                                          <asp:DropDownList ID="ddltype1" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                         <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="material-icons">
                                                    <label id ="lbRed1" runat="server" class="text-danger"></label>
                                                </i>
                                           </div>
                                        </div>  
                                        
                                       
                                    </div>
                                </div>                         
                           
                    </div>
                      <%--   <asp:HiddenField ID="hidMapID" runat="server" />
                            <div class="row" style="padding-left:25px;">
                              <label id="lbType2Name_modal" runat="server" class="tim-typo mt-4 text-right"></label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidID2" runat="server" />
                                        <input id="txtType2" runat="server" type="text" class="form-control" />
                                        <asp:DropDownList ID="ddltype2" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-left:30px;padding-bottom:20px;">
                                <label id="lbType1Name_modal" runat="server" class="tim-typo mt-4 text-right"></label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                     <asp:HiddenField ID="hidID1" runat="server" />
                                        <input id="txtType1" runat="server" type="text" class="form-control" />
                                        <asp:DropDownList ID="ddltype1" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>--%>
                </div>
                        <div class="modal-footer">
                            <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;
                            <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                        </div>
                   </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
