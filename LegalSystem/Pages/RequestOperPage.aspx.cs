﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.CommonFunctionFlow;
using LegalSystem.CommonHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class RequestOperPage : System.Web.UI.Page
    {
        AlertMessage alert = new AlertMessage();
        DateTime Date = DateTime.Now;
        string PathDoc = string.Empty;
        string FileName = string.Empty;
        List<sp_GetRequestOperPage> result = new List<sp_GetRequestOperPage>();
        ExportExcel ExportExcel = new ExportExcel();
        SendMail doSendMail = new SendMail();
        int MailType = 0;
        List<LegalData.sp_GetRequestOperPage_Outsource_Result> LSDocOutsource = new List<LegalData.sp_GetRequestOperPage_Outsource_Result>();
        #region
        public List<sp_GetRequestOperPage> DataList
        {
            get
            {
                return (List<sp_GetRequestOperPage>)ViewState["DataList"];
            }
            set
            {
                ViewState["DataList"] = value;
            }
        }
        public List<LegalData.sp_GetRequestOperPage_Outsource_Result> ListDocOutsource
        {
            get
            {
                return (List<LegalData.sp_GetRequestOperPage_Outsource_Result>)Session["LSDocOutsource"];
            }
            set
            {
                Session["LSDocOutsource"] = value;
            }
        }

        string reqMode = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            reqMode = Request.QueryString["reqMode"];
            if (!IsPostBack)
            {
                switch (reqMode)
                {
                    case MasterReqModeHelper.ReqOSOper:
                        lblHeader.Text = MasterPageTitleHelper.InnerRequestCusToOper;
                        break;
                    case MasterReqModeHelper.ReqDocOper:
                        lblHeader.Text = MasterPageTitleHelper.InnerRequestDocToOper;
                        break;
                    case MasterReqModeHelper.ReqDoctoOutsource:
                        FSMenuNotOutsource.Visible = false;
                        divOAIA1.Visible = divOAIA2.Visible = divRoundSend1.Visible = divRoundSend2.Visible = true;
                        lblHeader.Text = MasterPageTitleHelper.InnerRequestDocToOutsource;
                        break;
                }
                BindControl();
            }
        }

        public void BindControl()
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    ddlReqType.DataSource = unitOfWork.MSRequestTypeRep.GetFixedRequestTypeOperPage();
                    ddlReqType.DataValueField = "ReqTypeID";
                    ddlReqType.DataTextField = "ReqTypeName";
                    ddlReqType.DataBind();
                    switch (reqMode)
                    {
                        case MasterReqModeHelper.ReqDoctoOutsource:
                            var tt = unitOfWork.MSAttorneyRep.GetAllLawyer().ToList();
                            ddlAttroney.DataSource = tt;
                            ddlAttroney.DataValueField = "AttorneyID";
                            ddlAttroney.DataTextField = "LawyerName";
                            ddlAttroney.DataBind();
                            break;
                    }
                }
                ddlEmailSendStatus.DataSource = doSendMail.BindEmailSendStatus(reqMode);
                ddlEmailSendStatus.DataValueField = "Value";
                ddlEmailSendStatus.DataTextField = "Text";
                ddlEmailSendStatus.DataBind();
            }
            catch (Exception ex)
            {
                alert.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string requestType = (ddlReqType.SelectedValue == "0") ? "" : ddlReqType.SelectedValue;
                string requestNo = txtRequestNo.Value.Trim();
                string cif = txtCIF.Value.Trim();
                string cName = txtName.Value.Trim();
                string emailSendStatus = ddlEmailSendStatus.SelectedValue;
                string emailOSSendStatus = ddlEmailSendStatus.SelectedValue;
                if (reqMode == MasterReqModeHelper.ReqOSOper)
                    emailSendStatus = "0";
                else if (reqMode == MasterReqModeHelper.ReqDocOper)
                    emailOSSendStatus = "0";

                switch (reqMode)
                {
                    case MasterReqModeHelper.ReqOSOper:
                    case MasterReqModeHelper.ReqDocOper:

                        using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                        {
                            DataList = unitOfwork.LegalStoreProcedure.GetRequestOperPage(requestType, requestNo, cif, cName, emailSendStatus, emailOSSendStatus).ToList();
                            
                            if (reqMode == MasterReqModeHelper.ReqDocOper) {
                                DataList = DataList.Where(W => W.StatusReqOper.Contains(ddlEmailSendStatus.SelectedItem.Text.Trim())).ToList();
                            }
                            else if (reqMode == MasterReqModeHelper.ReqOSOper)
                            {
                                DataList = DataList.Where(W => W.RequestType == "1" && W.StatusReqOutstanding==ddlEmailSendStatus.SelectedItem.Text && W.SetCaseDate != null).ToList();
                            }
                            if (DataList.Count > 0)
                            {
                                bResult.Attributes.Add("style", "display:block;");
                                gvData.DataSource = DataList.ToList();
                                gvData.DataBind();
                                UcMessageBoxMain.VisibleControl();
                            }
                            else
                            {
                                alert.MsgBox(MessageEvent.NoData, MessageEventType.info, this, GetType());
                                bResult.Attributes.Add("style", "display:none;");
                            }
                        }
                        break;
                    case MasterReqModeHelper.ReqDoctoOutsource:
                        
                        string AttorneyID = ddlAttroney.SelectedItem.Value;
                        if (AttorneyID == "0") alert.MsgBox("กรุณาเลือก OA/IA", MessageEventType.info, this, GetType());
                        else
                        {
                            int Round = (string.IsNullOrEmpty(txtRoundSend.Value.Trim()) ? 0: Convert.ToInt32(txtRoundSend.Value.Trim()));
                            using (trSummaryToOutsourceRep rep = new trSummaryToOutsourceRep())
                            {
                                ListDocOutsource = null;
                                btnSendMail.Text = "Send Mail to Outsource";
                                ListDocOutsource = rep.GetDataDoctoOutsource(Round,AttorneyID.ToString(), ddlEmailSendStatus.SelectedItem.Value).ToList();

                                if (ListDocOutsource.Count > 0)
                                {
                                    bResult.Attributes.Add("style", "display:block;");
                                    gvData.DataSource = ListDocOutsource.ToList();
                                    gvData.DataBind();
                                    UcMessageBoxMain.VisibleControl();
                                }
                                else
                                {
                                    alert.MsgBox(MessageEvent.NoData, MessageEventType.info, this, GetType());
                                    bResult.Attributes.Add("style", "display:none;");
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                alert.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlReqType.SelectedIndex = 0;
            txtName.Value = txtCIF.Value = txtRequestNo.Value = string.Empty;
            gvData.DataSource = null;
            gvData.DataBind();
            bResult.Attributes.Add("style", "display:none;");
            UcMessageBoxMain.Visible = false;
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    switch (reqMode)
                    {
                        case MasterReqModeHelper.ReqDocOper:
                            break;
                        case MasterReqModeHelper.ReqOSOper:
                            this.gvData.Columns[18].Visible = false; //SendDate
                            this.gvData.Columns[19].Visible = false; //StatusReq
                            
                            this.gvData.Columns[20].Visible = true; //SendOutstandingDate
                            this.gvData.Columns[21].Visible = true; //StatusReqOutstanding
                            break;
                        case MasterReqModeHelper.ReqDoctoOutsource:
                            this.gvData.Columns[3].Visible = false; //RequestType
                            this.gvData.Columns[6].Visible = false; //{Req Date}
                            this.gvData.Columns[10].Visible = false; //{Address}

                            //this.gvData.Columns[2].Visible = true; //ACCOUNT 
                            this.gvData.Columns[13].Visible = true; //Topic
                            this.gvData.Columns[14].Visible = true; //NoticeDate
                            this.gvData.Columns[15].Visible = true; //ActionIndictDate
                            this.gvData.Columns[16].Visible = true; //IndictDate
                            this.gvData.Columns[17].Visible = true; //SendRound
                            break;
                    }
                }
                catch (Exception ex)
                {
                    alert.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
                }
            }
        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                alert.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvData.PageIndex = e.NewPageIndex;
                gvData.DataSource = DataList;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                alert.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            string dateEN = Date.ToString("yyyyMMdd", new CultureInfo("th-TH", true));
            int chkCount = 0;
            int seqSendDoc = 1;
            int SeqSendOS = 1;
            List<trRequestRelation> ListAccount = new List<trRequestRelation>();
            List<trRequestRelation> ListAccOS = new List<trRequestRelation>();
            trRequest tr = new trRequest();

            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {
                        if (((CheckBox)gvData.Rows[i].FindControl("Chk")).Checked)
                        {
                            chkCount++;

                            Guid requestID = new Guid(((HiddenField)gvData.Rows[i].FindControl("hidRequestID")).Value);
                            tr = unitOfwork.TrRequestRep.Get(x => x.RequestID == requestID).FirstOrDefault();
                            trLegal legal = unitOfwork.TrLegalRep.Get(x => x.LegalID == tr.LegalID).FirstOrDefault();
                            if (!string.IsNullOrEmpty(legal.SeqSendDoc.ToString())) { seqSendDoc = Convert.ToInt32(legal.SeqSendDoc) + 1; }
                            if (!string.IsNullOrEmpty(legal.SeqSendOS.ToString())) { seqSendDoc = Convert.ToInt32(legal.SeqSendOS) + 1; }
                            if (!tr.EmailSendDate.HasValue)
                                tr.EmailSendDate = DateTime.Now;
                            legal.SeqSendDoc = seqSendDoc;
                            legal.SeqSendOS = SeqSendOS;
                            switch (reqMode)
                            {
                                case MasterReqModeHelper.ReqDocOper:
                                    result.AddRange(DataList.Where(g => g.RequestNo.ToString() == tr.RequestNo.ToString()).ToList());
                                    break;
                                case MasterReqModeHelper.ReqOSOper:
                                    result.AddRange(DataList.Where(g => g.RequestNo.ToString() == tr.RequestNo.ToString()).ToList());
                                    ListAccOS.AddRange(unitOfwork.TrRequestRelateRep.GetRequestRelationByRequestID(tr.RequestID).Where(W => W.RelationType == "P").ToList());
                                    break;
                                case MasterReqModeHelper.ReqDoctoOutsource:
                                    using (trSummaryToOutsourceRep rep = new trSummaryToOutsourceRep())
                                    {
                                        LSDocOutsource.AddRange(ListDocOutsource.Where(W => W.RequestNo == tr.RequestNo.ToString()).ToList());
                                        ListAccount.AddRange(unitOfwork.TrRequestRelateRep.GetRequestRelationByRequestID(tr.RequestID).Where(W => W.RelationType == "P").ToList());
                                        //LSDocOutsource.AddRange(list.Where(W => W.RequestNo == tr.RequestNo.ToString()).ToList());
                                    }
                                    break;
                            }
                        }
                    }
                    if (chkCount == 0)
                        alert.MsgBox(MessageEvent.PleaseSelect1Item, MessageEventType.warning, this, GetType());
                    else if (chkCount > 0)
                    {
                        string seqDoc = seqSendDoc.ToString();
                        string seqOS = SeqSendOS.ToString();
                        switch (reqMode)
                        {
                            case MasterReqModeHelper.ReqDocOper:
                                PathDoc = ConfigurationManager.DocMailToOper;
                                MailType = 1;
                                FileName = string.Format("{0}LegalDOC_{1}_{2}.xlsx", PathDoc, dateEN, seqDoc.PadLeft(2, '0'));
                                ExportExcel.GenerateDocToOperation(result, FileName);
                                tr.EmailSendStatus = doSendMail.DoCSendMailToOper(FileName, MailType, "");
                                unitOfwork.TrRequestRep.Update(tr);
                                unitOfwork.Save();
                                break;
                            case MasterReqModeHelper.ReqOSOper:
                                PathDoc = ConfigurationManager.DocOutstandingMail;
                                MailType = 2;
                                FileName = string.Format("{0}LegalOS_{1}_{2}.xlsx", PathDoc, dateEN, seqOS.PadLeft(2, '0'));
                                ExportExcel.GenerateOSToOperation(result, FileName, ListAccOS);
                                tr.EmailOutstandingSendStatus = doSendMail.DoCSendMailToOper(FileName, MailType, "");
                                tr.EmailOutstandingSendDate = DateTime.Now;
                                unitOfwork.TrRequestRep.Update(tr);
                                unitOfwork.Save();
                                break;
                            case MasterReqModeHelper.ReqDoctoOutsource:
                                PathDoc = ConfigurationManager.DocMailToOutsource;
                                MailType = 3;
                                FileName = string.Format("{0}LegalDOC_{1}.xlsx", PathDoc, dateEN);
                                string Attroney = ddlAttroney.SelectedItem.Value;
                                
                                ExportExcel.GenerateDocToOutsource(LSDocOutsource, FileName, ListAccount);
                                var EmailSendStatus = doSendMail.DoCSendMailToOper(FileName, MailType, Attroney);

                                int Round = 1;
                                int No = 1;
                                
                                foreach (var Smmry in LSDocOutsource)
                                {
                                    if (Smmry.SendRound > 0)
                                        Round = Convert.ToInt32(Smmry.SendRound) + 1;

                                    using (trSummaryToOutsourceRep rep = new trSummaryToOutsourceRep())
                                    {
                                        string Emp_Code = GlobalSession.UserLogin.Emp_Code;
                                        rep.InsertSummaryData(Smmry, Round, No, EmailSendStatus, Emp_Code);
                                    }
                                }
                                break;
                        }
                        alert.MsgBox(MessageEvent.SendEmailSuccess, MessageEventType.success, this, GetType());
                        btnSearch_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                alert.MsgBox(ex.Message.ToString().Replace("'", ""), MessageEventType.error, this, this.GetType());
            }
        }
    }
}