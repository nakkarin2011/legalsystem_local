﻿<%@ Page Title="" Language="C#" MasterPageFile="~/~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="CapitalRate.aspx.cs" Inherits="LegalSystem.Pages.Setting.CapitalRate"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery1-1.12.0.min.js"></script>
    <script src="../../Scripts/jquery-ul.min.js"></script>
    <script src="../../Scripts/tether.min.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
        function closeModal() {
            $('#myModal').modal('hide');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>

    <%-- GridView --%>
    <div class="card">
        <div class="card-header ">
            <h3>Capital Rate</h3>
        </div>
        <div class="card-body">
            <div class="material-datatables">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server"  UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-6"><input type="text" runat="server" id="txtfilter" class="form-control" placeholder="Search Records" /></div>
                            <div class="col-sm-1"><asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-sm btn-info" OnClick="btnserch_Click" /></div>
                            <div class="col-sm-1"><asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-sm btn-info" OnClick="btnAdd_Click" /></div>
                        </div>
                        <%--<asp:TextBox ID="txttet" runat="server" CssClass="form-control"></asp:TextBox>--%>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvCapitalRate" runat="server" CellPadding="0" CellSpacing="0" CssClass="table table-striped table-no-bordered table-hover" GridLines="None" RowStyle-HorizontalAlign="Center"
                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                    DataKeyNames="CapitalRateID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                    PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                    OnRowDataBound="gvCapitalRate_RowDataBound" OnPageIndexChanging="gvCapitalRate_PageIndexChanging" OnRowCommand="gvCapitalRate_RowCommand"
                                    AllowSorting="true" OnRowDeleting="gvCapitalRate_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField SortExpression="MinWage" HeaderStyle-Font-Bold="true" HeaderText="MinWage">
                                            <ItemTemplate>
                                                <asp:Label ID="lbMinWage" runat="server" Text='<%#Eval("MinWage")%>'></asp:Label>
                                                <asp:HiddenField ID="hidCapID" runat="server" Value='<%#Eval("CapitalRateID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="MaxWage" HeaderText="MaxWage" SortExpression="MaxWage" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage" />
                                        <%--<asp:BoundField HeaderStyle-Font-Bold="true" DataField="Condition" HeaderText="Condition" SortExpression="Condition" />--%>
                                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Condition" SortExpression="Condition">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCondition" runat="server" Text='<%#Eval("Condition")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PayAmount" HeaderText="PayAmount" SortExpression="PayAmount" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill  btn-sm btn-info"
                                                    CausesValidation="false"
                                                    CommandName="EditCommand" CommandArgument='<%# Eval("CapitalRateID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill  btn-sm btn-danger" CommandName="Delete" CommandArgument='<%# Eval("CapitalRateID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="gvCapitalRate" EventName="RowCommand" />--%>
                        <asp:AsyncPostBackTrigger ControlID="gvCapitalRate" />
                        <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                        <%--<asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />--%>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Min Wage</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidTypeID" runat="server" />
                                        <%--<input id="txtMinWage" runat="server" type="number" class="form-control"/>--%>
                                        <asp:TextBox ID="txtMinWage" runat="server" CssClass="form-control"></asp:TextBox>
                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="text-danger" ControlToValidate="txtMinWage" runat="server" ErrorMessage="Required decimal" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" ControlToValidate="txtMinWage" runat="server" ValidationGroup="savemodal" ErrorMessage="Required"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Max Wage</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <%--<input id="txtMaxWage" runat="server" type="number" class="form-control"/>--%>
                                        <asp:TextBox ID="txtMaxWage" runat="server" TextMode="Number"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" CssClass="text-danger" ControlToValidate="txtMaxWage" runat="server" ErrorMessage="Required decimal" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" runat="server" ControlToValidate="txtMaxWage" ValidationGroup="savemodal" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Percentage</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtPercentage" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" CssClass="text-danger" ControlToValidate="txtPercentage" runat="server" ErrorMessage="Required decimal" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" runat="server" ControlToValidate="txtPercentage" ValidationGroup="savemodal" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Condition</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlCondition" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="text-danger" ControlToValidate="ddlCondition" ValidationGroup="savemodal" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">PayAmount</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" CssClass="text-danger" ControlToValidate="txtPayAmount" runat="server" ErrorMessage="Required decimal" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtPayAmount" runat="server" TextMode="Number"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="text-danger" runat="server" ControlToValidate="txtPayAmount" ValidationGroup="savemodal" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
