﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalEntities;
using System.ComponentModel;
using LegalService;

namespace LegalSystem.Pages.Setting
{
    public partial class ProductPayment : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<vSubProductType> subProductType;
        public List<vSubProductType> SubProductType
        {
            get
            {
                return (List<vSubProductType>)Session["SubProductType"];
            }
            set
            {
                Session["SubProductType"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        #region Initial
        public void GetFixType()
        {
            ddlFixTyp.Items.Insert(0, new ListItem("- Please Select -", ""));
            ddlFixTyp.Items.Insert(1, new ListItem("False", "0"));
            ddlFixTyp.Items.Insert(2, new ListItem("True", "1"));
            ddlFixTyp.SelectedIndex = 0;
        }

        public void GetproductType()
        {
            using (ProducTypeRep rep = new ProducTypeRep())
            {
                List<ProductType> T = rep.GetProductTypeList();
                ddlPrdtyp.DataSource = T;
                ddlPrdtyp.DataValueField = "PrdTypeID";
                ddlPrdtyp.DataTextField = "PrdTypeName";
                ddlPrdtyp.DataBind();
                ddlPrdtyp.SelectedIndex = 0;
            }
        }

        public void GetAllSubProduct()
        {
            using (vSubProductTypeRep rep = new vSubProductTypeRep())
            {
                List<vSubProductType> P = rep.GetAllSubProductType();
                SubProductType = P;
                gvProductMapping.DataSource = SubProductType;
                gvProductMapping.DataBind();
            }
        }
        #endregion

        public void InitData()
        {
            GetproductType();
            GetFixType();
            GetAllSubProduct();
        }

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hidSubID_modal.Value = string.Empty;
            ddlPrdtyp.SelectedIndex = 0;
            txtSubCode.Value = string.Empty;
            txtSubName.Value = string.Empty;
            ddlFixTyp.SelectedIndex = 0;
            txtCostAmt.Value = string.Empty;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (SubProductTypeRep rep = new SubProductTypeRep())
                {
                    SubProductType sub = new SubProductType();
                    //sub.SubTypeID = 0;
                    sub.PrdTypeID = Convert.ToInt32(ddlPrdtyp.SelectedValue);
                    sub.SubTypeCode = txtSubCode.Value.Trim();
                    sub.SubTypeName = txtSubName.Value.Trim();
                    sub.IsFixCost = Convert.ToBoolean(ddlFixTyp.SelectedItem.Text);
                    //sub.IsFixCost = Convert.ToBoolean(ddlfixtyp2.Value);
                    sub.FixCostAmt = Convert.ToDecimal(txtCostAmt.Value.Trim());

                    rep.insertData(sub);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (SubProductTypeRep rep = new SubProductTypeRep())
                {
                    // delete
                    int subtypID = Convert.ToInt32(hidSubID_modal.Value.Trim());
                    rep.DeleteData(subtypID);

                    // insert
                    SubProductType sub = new SubProductType();
                    sub.SubTypeID = subtypID;
                    sub.PrdTypeID = Convert.ToInt32(ddlPrdtyp.SelectedValue);
                    sub.SubTypeCode = txtSubCode.Value.Trim();
                    sub.SubTypeName = txtSubName.Value.Trim();
                    sub.IsFixCost = Convert.ToBoolean(ddlFixTyp.SelectedItem.Text);
                    //sub.IsFixCost = Convert.ToBoolean(ddlfixtyp2.Value);
                    sub.FixCostAmt = Convert.ToDecimal(txtCostAmt.Value.Trim());
                    rep.insertData(sub);
                }
            }
            GetAllSubProduct();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void ddlFixTyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFixTyp.SelectedValue.Equals("0"))
            {
                txtCostAmt.Value = "0.00";
                txtCostAmt.Disabled = true;
            }
            else
            {
                txtCostAmt.Disabled = false;
                txtCostAmt.Value = "8000.00";
            }
        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (vSubProductTypeRep rep = new vSubProductTypeRep())
            {
                string subcode = txtfilter.Value.Trim();
                string subname = txtfilter.Value.Trim();
                string prdname = txtfilter.Value.Trim();
                string isfix = txtfilter.Value.Trim();
                string fixcost = txtfilter.Value.Trim();
                try
                {
                    bool b_isfix = Convert.ToBoolean(isfix);
                }
                catch (Exception ex)
                {
                    isfix = string.Empty;
                }
                try
                {
                    decimal d_fixcost = Convert.ToDecimal(fixcost);
                }
                catch (Exception ex)
                {
                    fixcost = string.Empty;
                }

                List<vSubProductType> P = rep.GetSubPrtTypWithConditions(subcode, subname, prdname, isfix, fixcost);
                SubProductType = P;

                gvProductMapping.DataSource = SubProductType;
                gvProductMapping.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvProductMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvProductMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProductMapping.PageIndex = e.NewPageIndex;
            gvProductMapping.DataSource = SubProductType;
            gvProductMapping.DataBind();
        }

        protected void gvProductMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string subid = (gvProductMapping.Rows[index].FindControl("hidSubID") as HiddenField).Value.Trim();
                string prdid = (gvProductMapping.Rows[index].FindControl("hidPrdID") as HiddenField).Value.Trim();
                string subcode = gvProductMapping.Rows[index].Cells[1].Text.Trim();
                string subname = gvProductMapping.Rows[index].Cells[2].Text.Trim();
                bool fixtyp = Convert.ToBoolean(gvProductMapping.Rows[index].Cells[3].Text.Trim());
                string fixcost = gvProductMapping.Rows[index].Cells[4].Text.Trim();

                // bind to controller
                if (fixtyp) ddlFixTyp.SelectedValue = "0";
                else ddlFixTyp.SelectedValue = "1";
                hidSubID_modal.Value = subid;
                ddlPrdtyp.SelectedValue = prdid;
                txtSubCode.Value = subcode;
                txtSubName.Value = subname;
                txtCostAmt.Value = fixcost;
            }
            else if (e.CommandName == "Delete")
            {
                using (SubProductTypeRep rep = new SubProductTypeRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int subid = Convert.ToInt32((gvProductMapping.Rows[index].FindControl("hidSubID") as HiddenField).Value.Trim());
                    rep.DeleteData(subid);
                }
            }
        }

        protected void gvProductMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllSubProduct();
        }
        #endregion
    }
}