﻿using LE = LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages.Setting
{
    public partial class Plaintiff : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.Plaintiff> plaintiffList;
        public List<LE.Plaintiff> PlaintiffList
        {
            get
            {
                return (List<LE.Plaintiff>)Session["PlaintiffList"];
            }
            set
            {
                Session["PlaintiffList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            GetCustomerType();
            GetAllPlaintiff();
        }

        #region Initial
        public void GetCustomerType()
        {
            ddlPType.Items.Insert(0, new ListItem("- Please Select -", ""));
            ddlPType.Items.Insert(1, new ListItem("บุคคลธรรมดา", "บุคคลธรรมดา"));
            ddlPType.Items.Insert(2, new ListItem("นิติบุคคล", "นิติบุคคล"));
            ddlPType.SelectedIndex = 0;
        }

        public void GetAllPlaintiff()
        {
            using (PlaintiffRep rep = new PlaintiffRep())
            {
                plaintiffList = rep.GetAllPlaintiff();
                PlaintiffList = plaintiffList;
                gvPlaintiff.DataSource = PlaintiffList;
                gvPlaintiff.DataBind();
            }
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";

            ddlPType.SelectedIndex = 0;
            txtPAddress.Value = string.Empty;
            txtPID.Value = string.Empty;
            txtPName.Value = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (PlaintiffRep rep = new PlaintiffRep())
                {
                    LE.Plaintiff plaintiff = new LE.Plaintiff();
                    plaintiff.PID = txtPID.Value.Trim();
                    plaintiff.PName = txtPName.Value.Trim();
                    plaintiff.PAddress = txtPAddress.Value.Trim();
                    plaintiff.PCusType = ddlPType.SelectedValue.Trim();

                    rep.insertData(plaintiff);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (PlaintiffRep rep = new PlaintiffRep())
                {
                    // delete
                    int plaintiffID = Convert.ToInt32(hid_plaintiffID_modal.Value.Trim());
                    rep.DeleteData(plaintiffID);

                    // insert
                    LE.Plaintiff plaintiff = new LE.Plaintiff();
                    plaintiff.PlaintiffID = plaintiffID;
                    plaintiff.PID = txtPID.Value.Trim();
                    plaintiff.PName = txtPName.Value.Trim();
                    plaintiff.PAddress = txtPAddress.Value.Trim();
                    plaintiff.PCusType = ddlPType.SelectedValue.Trim();
                    rep.insertData(plaintiff);
                }
            }
            GetAllPlaintiff();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (PlaintiffRep rep = new PlaintiffRep())
            {
                string scondition = txtfilter.Value.Trim();

                plaintiffList = rep.GetPlaintiffWithCondition(scondition);
                PlaintiffList = plaintiffList;

                gvPlaintiff.DataSource = PlaintiffList;
                gvPlaintiff.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvPlaintiff_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvPlaintiff_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPlaintiff.PageIndex = e.NewPageIndex;
            gvPlaintiff.DataSource = PlaintiffList;
            gvPlaintiff.DataBind();
        }

        protected void gvPlaintiff_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string plaintiffID = (gvPlaintiff.Rows[index].FindControl("hid_plaintiffID") as HiddenField).Value.Trim();
                string pID = (gvPlaintiff.Rows[index].FindControl("lb_pID") as Label).Text.Trim();
                string pName = gvPlaintiff.Rows[index].Cells[1].Text.Trim();
                string pAddress = gvPlaintiff.Rows[index].Cells[2].Text.Trim();
                string pType = gvPlaintiff.Rows[index].Cells[3].Text.Trim();

                // bind to controller
                hid_plaintiffID_modal.Value = plaintiffID;
                txtPID.Value = pID;
                txtPName.Value = pName;
                txtPAddress.Value = pAddress;
                ddlPType.SelectedValue = pType;
            }
            else if (e.CommandName == "Delete")
            {
                using (PlaintiffRep rep = new PlaintiffRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int pID = Convert.ToInt32((gvPlaintiff.Rows[index].FindControl("hid_plaintiffID") as HiddenField).Value.Trim());
                    rep.DeleteData(pID);
                }
            }
        }

        protected void gvPlaintiff_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllPlaintiff();
        }
        #endregion
    }
}