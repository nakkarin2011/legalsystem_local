﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LE=LegalEntities;
using LegalService;

namespace LegalSystem.Pages.Setting
{
    public partial class Relation : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.Relation> relationList;
        public List<LE.Relation> RelationList
        {
            get
            {
                return (List<LE.Relation>)Session["RelationList"];
            }
            set
            {
                Session["RelationList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            GetAllRelation();
        }

        #region Initial
        public void GetAllRelation()
        {
            using (RelationRep rep = new RelationRep())
            {
                relationList = rep.GetAllRelation();
                RelationList = relationList;
                gvRelation.DataSource = RelationList;
                gvRelation.DataBind();
            }
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hid_RelationID_modal.Value = string.Empty;
            txtRelCode.Value = string.Empty;
            txtRelName.Value = string.Empty;
            txtRelOrder.Value = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (RelationRep rep = new RelationRep())
                {
                    LE.Relation rel = new LE.Relation();
                    rel.RelationCode = txtRelCode.Value.Trim();
                    rel.RelationName = txtRelName.Value.Trim();
                    rel.RelationOrder = Convert.ToInt32(txtRelOrder.Value.Trim());

                    rep.insertData(rel);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (RelationRep rep = new RelationRep())
                {
                    // delete
                    int relID = Convert.ToInt32(hid_RelationID_modal.Value.Trim());
                    rep.DeleteData(relID);

                    // insert
                    LE.Relation rel = new LE.Relation();
                    rel.RelationID = relID;
                    rel.RelationCode = txtRelCode.Value.Trim();
                    rel.RelationName = txtRelName.Value.Trim();
                    rel.RelationOrder = Convert.ToInt32(txtRelOrder.Value.Trim());
                    rep.insertData(rel);
                }
            }
            GetAllRelation();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (RelationRep rep = new RelationRep())
            {
                string scondition = txtfilter.Value.Trim();

                relationList = rep.GetRelationWithCondition(scondition);
                RelationList = relationList;

                gvRelation.DataSource = RelationList;
                gvRelation.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvRelation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvRelation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRelation.PageIndex = e.NewPageIndex;
            gvRelation.DataSource = RelationList;
            gvRelation.DataBind();
        }

        protected void gvRelation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string relID = (gvRelation.Rows[index].FindControl("hid_relationID") as HiddenField).Value.Trim();
                string relCode = (gvRelation.Rows[index].FindControl("lb_relationCode") as Label).Text.Trim();
                string relName = gvRelation.Rows[index].Cells[1].Text.Trim();
                string relOrder = gvRelation.Rows[index].Cells[2].Text.Trim();

                // bind to controller
                hid_RelationID_modal.Value = relID;
                txtRelCode.Value = relCode;
                txtRelName.Value = relName;
                txtRelOrder.Value = relOrder;
            }
            else if (e.CommandName == "Delete")
            {
                using (RelationRep rep = new RelationRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int relID = Convert.ToInt32((gvRelation.Rows[index].FindControl("hid_RelationID") as HiddenField).Value.Trim());
                    rep.DeleteData(relID);
                }
            }
        }

        protected void gvRelation_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllRelation();
        }
        #endregion
    }
}