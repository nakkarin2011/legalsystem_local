﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalEntities;
using System.ComponentModel;
using LegalService;
using LE = LegalEntities;

namespace LegalSystem.Pages.Setting
{
    public partial class CourtZone : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.CourtZone> courtZoneList;
        public List<LE.CourtZone> CourtZoneList
        {
            get
            {
                return (List<LE.CourtZone>)Session["CourtZoneList"];
            }
            set
            {
                Session["CourtZoneList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            GetAllCourtZone();
        }

        #region Initial
        public void GetAllCourtZone()
        {
            using (CourtZoneRep rep = new CourtZoneRep())
            {
                courtZoneList = rep.GetAllCourtZone();
                CourtZoneList = courtZoneList;
                gvCourtZone.DataSource = CourtZoneList;
                gvCourtZone.DataBind();
            }
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";

            txtDay.Value = string.Empty;
            txtfilter.Value = string.Empty;
            txtZone.Value = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (CourtZoneRep rep = new CourtZoneRep())
                {
                    LE.CourtZone zone = new LE.CourtZone();
                    zone.CourtZoneName = txtZone.Value.Trim();
                    zone.Days = Convert.ToInt32(txtDay.Value.Trim());

                    rep.insertData(zone);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (CourtZoneRep rep = new CourtZoneRep())
                {
                    // delete
                    int zoneID = Convert.ToInt32(hidCourtZoneID_modal.Value.Trim());
                    rep.DeleteData(zoneID);

                    // insert
                    LE.CourtZone zone = new LE.CourtZone();
                    zone.CourtZoneID = Convert.ToInt32(hidCourtZoneID_modal.Value.Trim());
                    zone.CourtZoneName = txtZone.Value.Trim();
                    zone.Days = Convert.ToInt32(txtDay.Value.Trim());
                    rep.insertData(zone);
                }
            }
            GetAllCourtZone();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (CourtZoneRep rep = new CourtZoneRep())
            {
                string scondition = txtfilter.Value.Trim();

                courtZoneList = rep.GetCourtZoneWithCondition(scondition);
                CourtZoneList = courtZoneList;

                gvCourtZone.DataSource = CourtZoneList;
                gvCourtZone.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvCourtZone_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllCourtZone();
        }

        protected void gvCourtZone_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvCourtZone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCourtZone.PageIndex = e.NewPageIndex;
            gvCourtZone.DataSource = CourtZoneList;
            gvCourtZone.DataBind();
        }

        protected void gvCourtZone_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit123")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string zoneID = (gvCourtZone.Rows[index].FindControl("hidCourtZoneID") as HiddenField).Value.Trim();
                string zonename = (gvCourtZone.Rows[index].FindControl("lbZoneName") as Label).Text.Trim();
                string days = gvCourtZone.Rows[index].Cells[1].Text.Trim();

                // bind to controller
                hidCourtZoneID_modal.Value = zoneID;
                txtZone.Value = zonename;
                txtDay.Value = days;
            }
            else if (e.CommandName == "Delete")
            {
                using (CourtZoneRep rep = new CourtZoneRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int zoneID = Convert.ToInt32((gvCourtZone.Rows[index].FindControl("hidCourtZoneID") as HiddenField).Value.Trim());
                    rep.DeleteData(zoneID);
                }                
            }
        }
        #endregion
    }
}