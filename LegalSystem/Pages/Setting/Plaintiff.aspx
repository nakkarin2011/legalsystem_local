﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="Plaintiff.aspx.cs" Inherits="LegalSystem.Pages.Setting.Plaintiff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- GridView --%>
    <div class="card">
        <div class="card-header ">
            <h3>Plaintiff</h3>
        </div>
        <div class="card-body">
            <div class="material-datatables">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" placeholder="Search Records" />
                                    <asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-info" OnClick="btnserch_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-info"
                                        data-toggle="modal" data-target="#myModal" OnClick="btnAdd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvPlaintiff" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                    DataKeyNames="PlaintiffID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                    PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                    OnRowDataBound="gvPlaintiff_RowDataBound" OnPageIndexChanging="gvPlaintiff_PageIndexChanging" OnRowCommand="gvPlaintiff_RowCommand"
                                    AllowSorting="true" OnRowDeleting="gvPlaintiff_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField SortExpression="PlaintiffID" HeaderStyle-Font-Bold="true" HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_pID" runat="server" Text='<%#Eval("PID")%>'></asp:Label>
                                                <asp:HiddenField ID="hid_plaintiffID" runat="server" Value='<%#Eval("PlaintiffID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PName" HeaderText="Name" SortExpression="PName" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PAddress" HeaderText="Address" SortExpression="PAddress" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PCusType" HeaderText="Type" SortExpression="PCusType" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                    CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                    CommandName="EditCommand" CommandArgument='<%# Eval("PlaintiffID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("PlaintiffID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvPlaintiff"  />
                        <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Plaintiff ID</label>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <asp:HiddenField ID="hid_plaintiffID_modal" runat="server" />
                                <input id="txtPID" runat="server" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <input id="txtPName" runat="server" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <input id="txtPAddress" runat="server" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlPType" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info"  />&nbsp;
                    <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
