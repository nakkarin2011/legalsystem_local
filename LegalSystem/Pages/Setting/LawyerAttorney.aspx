﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="LawyerAttorney.aspx.cs" Inherits="LegalSystem.Pages.Setting.LawyerAttorney" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Content/plugin/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/plugin/bootstrap-tagsinput.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div class="row">
        <div class="col-md-12">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="row" style="width: 100%; height: 36px; margin-top: -4px; margin-left: 0px; vertical-align: middle;">
                        <uc1:ucMessageBox runat="server" ID="ucMessageBox" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="card">
                <div class="card-header card-header-info card-header-text">
                    <div class="card-text">
                        <h5 class="card-title">Lawyer Attorney</h5>
                    </div>
                </div>
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-8 ml-auto">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalLawyerAttorney">Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-centered">
                                    <div class="material-datatables">
                                        <asp:GridView ID="gvLawyerAttorney" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover"
                                            AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                            DataKeyNames="LawyerID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png" PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                            OnPageIndexChanging="gvLawyerAttorney_PageIndexChanging" OnRowCommand="gvLawyerAttorney_RowCommand" OnRowDataBound="gvLawyerAttorney_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Font-Bold="true" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hidID" runat="server" Value='<%#Eval("LawyerID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="LawyerName" SortExpression="LawyerName" HeaderText="Lawyer Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="AttorneyTypeName" SortExpression="AttorneyTypeName" HeaderText="Attorney Type" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnEditLawyer" runat="server" Text="Edit" class="btn btn-fill btn-info" CommandName="Edit" CommandArgument='<%# Eval("LawyerID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDeleteLawyer" runat="server" Text="Delete" class="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("LawyerID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvLawyerAttorney" EventName="RowCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalLawyerAttorney" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 class="card-title font-thai ">Add/Edit Lawyer Attorney</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h5>
                                            <span class="tim-note">Attorney Type</span>
                                        </h5>
                                        <asp:DropDownList ID="ddlAttorneyType" runat="server" CssClass="form-control" AutoPostBack="false" DataValueField="AttorneyTypeID" DataTextField="AttorneyTypeName"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h5>
                                            <span class="tim-note">Lawyer Name</span>
                                        </h5>
                                        <input type="text" id="txtLawyer" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info">Save</button>
                    &nbsp;
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
