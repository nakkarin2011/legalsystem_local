﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="LawyerCostsPeroid.aspx.cs" Inherits="LegalSystem.Pages.Setting.LawyerCostsPeroid" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">>

    <%-- GridView --%>
    <div class="card">
        <div class="card-header ">
            <h3>Lawyer Cost Period</h3>
        </div>
        <div class="card-body">
            <div class="material-datatables">
                <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" placeholder="Search Records" />
                                    <asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-info" OnClick="btnserch_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-info"
                                        data-toggle="modal" data-target="#myModal" OnClick="btnAdd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvLawyerCostPeriod" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover"
                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                    DataKeyNames="PaymentID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                    PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                    OnPageIndexChanging="gvLawyerCostPeriod_PageIndexChanging" OnRowCommand="gvLawyerCostPeriod_RowCommand"
                                    OnRowDataBound="gvLawyerCostPeriod_RowDataBound" OnRowDeleting="gvLawyerCostPeriod_RowDeleting">
                                    <Columns>
                                        <%--<asp:BoundField HeaderStyle-Font-Bold="true" DataField="PaymentID" HeaderText="PaymentID" />
                                                <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PaymentTypeID" HeaderText="PaymentTypeID" />
                                                <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PaymentTypeName" HeaderText="PaymenType" />--%>
                                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Payment Type">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hid_paymentID" Value='<%#Eval("PaymentID")%>' runat="server" />
                                                <asp:HiddenField ID="hid_paymentTypeID" Value='<%#Eval("PaymentTypeID")%>' runat="server" />
                                                <asp:Label ID="lb_paymentType" runat="server" Text='<%#Eval("PaymentTypeName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PaymentName" HeaderText="Period" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="Percentage" HeaderText="Percentage" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                    CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                    CommandName="EditCommand" CommandArgument='<%# Eval("PaymentID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("PaymentID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="gvCapitalRate" EventName="RowCommand" />--%>
                        <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="gvLawyerCostPeriod" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Payment Type</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hid_paymentID_modal" runat="server" />
                                        <asp:DropDownList ID="ddlPaymentType" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Payment Name</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtPaymentName" runat="server" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Percentage</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtPercentage" runat="server" type="number" step="any" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;
                    <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

