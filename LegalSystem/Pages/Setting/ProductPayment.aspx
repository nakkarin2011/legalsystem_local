﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="ProductPayment.aspx.cs" Inherits="LegalSystem.Pages.Setting.ProductPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- GridView --%>
    <div class="card">
        <div class="card-header ">
            <h3>Product Payment</h3>
        </div>
        <div class="card-body">
            <div class="material-datatables">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" placeholder="Search Records" />
                                    <asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-info" OnClick="btnserch_Click" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_info">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-info"
                                        data-toggle="modal" data-target="#myModal" OnClick="btnAdd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvProductMapping" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                    DataKeyNames="SubTypeID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png" PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                    OnRowDataBound="gvProductMapping_RowDataBound" OnPageIndexChanging="gvProductMapping_PageIndexChanging" OnRowCommand="gvProductMapping_RowCommand"
                                    AllowSorting="true" OnRowDeleting="gvProductMapping_RowDeleting">
                                    <Columns>
                                        <%--<asp:TemplateField SortExpression="SubTypeID" HeaderStyle-Font-Bold="true" HeaderText="SubTypeID" Visible="false" >
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidSubID" runat="server" Value='<%#Eval("SubTypeID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="PrdTypeName" HeaderStyle-Font-Bold="true" HeaderText="PrdTypeName" >
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidPrdID" runat="server" Value='<%#Eval("PrdTypeID")%>' />
                                            <label title='<%#Eval("PrdTypeName")%>'></label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderStyle-Font-Bold="true" DataField="PrdTypeName" HeaderText="PrdTypeName" SortExpression="PrdTypeName" />--%>
                                        <asp:TemplateField SortExpression="PrdTypeName" HeaderStyle-Font-Bold="true" HeaderText="PrdTypeName">
                                            <ItemTemplate>
                                                <asp:Label ID="lbPrdName" runat="server" Text='<%#Eval("PrdTypeName")%>'></asp:Label>
                                                <asp:HiddenField ID="hidSubID" runat="server" Value='<%#Eval("SubTypeID")%>' />
                                                <asp:HiddenField ID="hidPrdID" runat="server" Value='<%#Eval("PrdTypeID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="SubTypeCode" HeaderText="SubTypeCode" SortExpression="SubTypeCode" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="SubTypeName" HeaderText="SubTypeName" SortExpression="SubTypeName" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="IsFixCost" HeaderText="IsFixCost" SortExpression="IsFixCost" />
                                        <asp:BoundField HeaderStyle-Font-Bold="true" DataField="FixCostAmt" HeaderText="FixCostAmt" SortExpression="FixCostAmt" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                    CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                    CommandName="EditCommand" CommandArgument='<%# Eval("SubTypeID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("SubTypeID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvProductMapping" EventName="RowCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <%-- Modal --%>
                        <ContentTemplate>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <%--<div class="row">
                                <label class="col-sm-2 col-form-label">Sub Product Type ID</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtSubTypID" runat="server" type="text" class="form-control" disabled="disabled" />
                                    </div>
                                </div>
                            </div>--%>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Product Type</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:HiddenField ID="hidSubID_modal" runat="server" />
                                        <asp:DropDownList ID="ddlPrdtyp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Sub Type Code</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtSubCode" runat="server" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Sub Type Name</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtSubName" runat="server" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Fix Cost</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlFixTyp" runat="server" OnSelectedIndexChanged="ddlFixTyp_SelectedIndexChanged" 
                                            AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Cost Amount</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input id="txtCostAmt" runat="server" type="number" step="any" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlFixTyp" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;
                    <%--<asp:Button ID="Button2" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-fill btn-danger" data-dismiss="modal" ValidationGroup="savemodal" />--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
