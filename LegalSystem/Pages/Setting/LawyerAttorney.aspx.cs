﻿using LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages.Setting
{
    public partial class LawyerAttorney : System.Web.UI.Page
    {
        #region Function
        private void GetDataUI()
        {
            GetDataLawyerAttorney();

            using (AttorneyTypeRep rep = new AttorneyTypeRep())
            {
                List<AttorneyType> A = rep.GetAllAttorneyType();
                ddlAttorneyType.DataSource = A;
                ddlAttorneyType.DataBind();
                ddlAttorneyType.SelectedIndex = 0;
            }
        }

        public void GetDataLawyerAttorney()
        {
            //using (vLawyerRep rep = new vLawyerRep())
            //{
            //    List<vLawyer> L = rep.GetAllLawyer();
            //    gvLawyerAttorney.DataSource = L;
            //    gvLawyerAttorney.DataBind();
            //}
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetDataUI();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalLawyerAttorney", "popupModal()", true);
        }

        protected void gvLawyerAttorney_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDeleteLawyer") as Button;
                btn.OnClientClick = "return confirm('คุณต้องการลบรายการใช่หรือไม่?');";
            }
        }

        protected void gvLawyerAttorney_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string lawyer = e.CommandArgument.ToString();
            if (e.CommandName == "Edit")
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalLawyerAttorney", "popupModal()", true);
            }
            else
            {

            }
        }

        protected void gvLawyerAttorney_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvLawyerAttorney.PageIndex = e.NewPageIndex;
            gvLawyerAttorney.DataBind();
        }
    }
}