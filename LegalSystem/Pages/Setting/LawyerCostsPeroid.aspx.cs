﻿
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LE = LegalEntities;


namespace LegalSystem.Pages.Setting
{
    public partial class LawyerCostsPeroid : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.vLawyerCostPeriod> lawyerCostPeriodList;
        public List<LE.vLawyerCostPeriod> LawyerCostPeriodList
        {
            get
            {
                return (List<LE.vLawyerCostPeriod>)Session["LawyerCostPeriodList"];
            }
            set
            {
                Session["LawyerCostPeriodList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitDataAndUI();
            }
        }

        private void InitDataAndUI()
        {
            GetPaymentType();
            GetAllPeriod();
        }

        #region Initail Data
        public void GetPaymentType()
        {
            using (PaymentTypeRep rep = new PaymentTypeRep())
            {
                List<LE.PaymentType> T = rep.GetDDLPaymentType();
                ddlPaymentType.DataSource = T;
                ddlPaymentType.DataValueField = "PaymentTypeID";
                ddlPaymentType.DataTextField = "PaymentTypeName";
                ddlPaymentType.DataBind();
                ddlPaymentType.SelectedIndex = 0;
            }
        }

        public void GetAllPeriod()
        {
            using (vLawyerCostPeriodRep rep = new vLawyerCostPeriodRep())
            {
                List<LE.vLawyerCostPeriod> P = rep.GetAllLawyerCostPeriod();
                gvLawyerCostPeriod.DataSource = P;
                gvLawyerCostPeriod.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvLawyerCostPeriod_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvLawyerCostPeriod_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvLawyerCostPeriod.PageIndex = e.NewPageIndex;
            gvLawyerCostPeriod.DataSource = gvLawyerCostPeriod;
            gvLawyerCostPeriod.DataBind();
        }

        protected void gvLawyerCostPeriod_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string paymentID = (gvLawyerCostPeriod.Rows[index].FindControl("hid_paymentID") as HiddenField).Value.Trim();
                string paymentTypeID = (gvLawyerCostPeriod.Rows[index].FindControl("hid_paymentTypeID") as HiddenField).Value.Trim();
                string paymentName = gvLawyerCostPeriod.Rows[index].Cells[1].Text.Trim();
                string percentage = gvLawyerCostPeriod.Rows[index].Cells[2].Text.Trim();

                // bind to controller
                hid_paymentID_modal.Value = paymentID;
                ddlPaymentType.SelectedValue = paymentTypeID;
                txtPaymentName.Value = paymentName;
                txtPercentage.Value = percentage;
            }
            else if (e.CommandName == "Delete")
            {
                using (LawyerCostPeriodRep rep = new LawyerCostPeriodRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int capID = Convert.ToInt32((gvLawyerCostPeriod.Rows[index].FindControl("hid_paymentID") as HiddenField).Value.Trim());
                    rep.DeleteData(capID);
                }
            }
        }

        protected void gvLawyerCostPeriod_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllPeriod();
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hid_paymentID_modal.Value = string.Empty;
            ddlPaymentType.SelectedIndex = 0;
            txtPaymentName.Value = string.Empty;
            txtPercentage.Value = string.Empty;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (LawyerCostPeriodRep rep = new LawyerCostPeriodRep())
                {
                    LE.Payment period = new LE.Payment();
                    period.PaymentTypeID = Convert.ToInt32(ddlPaymentType.SelectedValue.Trim());
                    period.PaymentName = txtPaymentName.Value.Trim();
                    period.Percentage = Convert.ToDecimal(txtPercentage.Value.Trim());

                    rep.insertData(period);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (LawyerCostPeriodRep rep = new LawyerCostPeriodRep())
                {
                    // delete
                    int paymentID = Convert.ToInt32(hid_paymentID_modal.Value.Trim());
                    rep.DeleteData(paymentID);

                    // insert
                    LE.Payment period = new LE.Payment();
                    period.PaymentID = paymentID;
                    period.PaymentTypeID = Convert.ToInt32(ddlPaymentType.SelectedValue.Trim());
                    period.PaymentName = txtPaymentName.Value.Trim();
                    period.Percentage = Convert.ToDecimal(txtPercentage.Value.Trim());
                    rep.insertData(period);
                }
            }
            GetAllPeriod();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (vLawyerCostPeriodRep rep = new vLawyerCostPeriodRep())
            {
                string scondition = txtfilter.Value.Trim();

                lawyerCostPeriodList = rep.GetAllCostPeriodWithCondition(scondition);
                LawyerCostPeriodList = lawyerCostPeriodList;

                gvLawyerCostPeriod.DataSource = LawyerCostPeriodList;
                gvLawyerCostPeriod.DataBind();
            }
        }
        #endregion
    }
}