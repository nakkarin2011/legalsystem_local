﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.CommonHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class AssignPage : System.Web.UI.Page
    {
        #region
        public List<trRequest> RequestList
        {
            get
            {
                return (List<trRequest>)Session["RequestList"];
            }
            set
            {
                Session["RequestList"] = value;
            }
        }
        public List<vAssignLawyer> AssignLawyerList
        {
            get
            {
                return (List<vAssignLawyer>)Session["AssignLawyerList"];
            }
            set
            {
                Session["AssignLawyerList"] = value;
            }
        }
        #endregion
        AlertMessage Msg = new AlertMessage();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControl();
            }
        }

        public void BindControl()
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    //ddlLegalStatus.DataSource = unitOfWork.MSLegalStatusRep.GetLegalStatusByAssign();
                    ddlLegalStatus.DataSource = unitOfWork.MSLegalStatusRep.GetLegalStatusList().OrderBy(O=>O.ActionCode);
                    ddlLegalStatus.DataValueField = "ActionCode";
                    ddlLegalStatus.DataTextField = "LegalStatusDisplay";
                    ddlLegalStatus.DataBind();

                    ddlLawyer.DataSource = unitOfWork.VAttorneyLawyerRep.GetAllUsers();
                    ddlLawyer.DataValueField = "Emp_Code";
                    ddlLawyer.DataTextField = "TH_Name";
                    ddlLawyer.DataBind();

                    ddlAttorney.DataSource = unitOfWork.MSAttorneyRep.GetAllLawyer();
                    ddlAttorney.DataValueField = "AttorneyID";
                    ddlAttorney.DataTextField = "LawyerName";
                    ddlAttorney.DataBind();
                }
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string CIF = txtCIF.Value.Trim();
                string LegalStatus = ddlLegalStatus.SelectedItem.Value.Trim();
                string LegalNo = txtLegalNo.Value.Trim();
                string RequestNo = txtReqNo.Value.Trim();
                string Lawyer = ddlLawyer.SelectedItem.Value.Trim();
                string Attorney = ddlAttorney.SelectedItem.Value.Trim();
                if (Lawyer == "0") Lawyer = "";
                if (Attorney == "0") Attorney = "";
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    AssignLawyerList = unitOfWork.VAssignLawyerRep.GetAssignByCondition(CIF, LegalStatus, LegalNo, RequestNo, Lawyer, Attorney);

                    if (AssignLawyerList.Count > 0)
                    {
                        UcMessageBoxMain.VisibleControl();
                        bResult.Visible = true;
                        gvAssign.DataSource = AssignLawyerList;                        
                        gvAssign.DataBind();                        
                    }
                    else
                    {
                        bResult.Visible = false;
                        
                        UcMessageBoxMain.Show(MessageEvent.NoData, "P");//Msg.MsgBox(MessageEvent.NoData, MessageEventType.info, this, this.GetType()); 
                    }
                }

            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
                //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "X");
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtLegalNo.Value = txtReqNo.Value = txtCIF.Value = string.Empty;
            ddlAttorney.SelectedIndex = ddlLawyer.SelectedIndex = ddlLegalStatus.SelectedIndex = 0;
            AssignLawyerList = null;
            gvAssign.DataSource = AssignLawyerList;
            gvAssign.DataBind();
            bResult.Visible = false;
            UcMessageBoxMain.VisibleControl();
        }

        #region CANCEL
        protected void btnAssign_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            //    {
            //        // validate : status, select lawyer
            //        List<trRequest> datas = new List<trRequest>();
            //        foreach (GridViewRow row in gvAssign.Rows)
            //        {
            //            if (row.RowType == DataControlRowType.DataRow)
            //            {
            //                string requestNo = row.Cells[1].Text;
            //                CheckBox ChkAS_RT = (row.FindControl("ChkAS_RT") as CheckBox);
            //                DropDownList ddlLawyer = row.FindControl("ddlLawyer") as DropDownList;
            //                DropDownList ddlAttorney = row.FindControl("ddlAttorney") as DropDownList;
            //                if (ChkAS_RT.Checked)
            //                {
            //                    //vAssignLawyerRep a_rep = new vAssignLawyerRep();
            //                    //int lawyerstatus =unitOfwork.VAssignLawyerRep.GetAssignByReqNo(requestNo).LawyerStatus;
            //                    //if (lawyerstatus != 0) throw new Exception("Req No. \"" + requestNo + "\" ถูก Assign แล้ว"); // LegalStatus = 100 : ส่งเรื่องให้ฝ่ายกฎหมายดำเนินการ

            //                    if (Convert.ToInt32(ddlAttorney.SelectedValue) > 0 && Convert.ToInt32(ddlLawyer.SelectedValue) > 0)
            //                    {
            //                        // RequestList = new List<trRequest>();
            //                        //trRequest req = RequestList.First(x => x.RequestNo == requestNo);
            //                        //trRequestRep r_rep = new trRequestRep();
            //                        //trRequest req = r_rep.GetRequestDataByRequestNo(requestNo);
            //                        trRequest req = new trRequest();
            //                        req.RequestNo = requestNo;
            //                        req.RequestStatus = "F5"; // 3: Approve
            //                        req.RequestStatusDate = DateTime.Now;
            //                        req.LawyerStatus = 1;
            //                        req.LawyerStatusDate = DateTime.Now;
            //                        req.LawyerID = ddlLawyer.SelectedValue.ToString();
            //                        req.LawyerIDReceive = DateTime.Now;
            //                        datas.Add(req);
            //                        //RequestList.RemoveAll(x => x.RequestNo == requestNo);
            //                        //RequestList.Add(req);
            //                    }
            //                    else throw new Exception("Req No. \"" + requestNo + "\", กรุณาเลือก \"Assign OA/IA\" และ \"Assign Lawyer\"");

            //                }
            //            }
            //        }

            //        if (datas == null) throw new Exception("กรุณาเลือกรายการเพื่อ \"Assign\"");


            //        for (int i = 0; i < datas.Count; i++)
            //        {
            //            string reqno = datas[i].RequestNo;
            //            trRequest tr = unitOfwork.TrRequestRep.Get(x => x.RequestNo == reqno).FirstOrDefault();
            //            tr.RequestStatus = datas[i].RequestStatus;
            //            tr.RequestStatusDate = datas[i].RequestStatusDate;
            //            tr.LawyerStatus = datas[i].LawyerStatus;
            //            tr.LawyerStatusDate = datas[i].LawyerStatusDate;
            //            tr.LawyerID = datas[i].LawyerID;
            //            tr.LawyerIDReceive = datas[i].LawyerIDReceive;
            //            tr.SeniorLawyerApprove = DateTime.Now;
            //        }
            //        //LegalUnitOfWork unitOfWork = new LegalUnitOfWork();
            //        //unitOfwork.TrRequestRep.Update(RequestList);
            //        unitOfwork.Save();
            //    }
            //    btnSearch_Click(sender, e);
            //    //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "success", "updateassign()", true);
            //    // UcMessageBoxMain.VisibleControl();
            //}
            //catch (Exception ex)
            //{
            //    if (ex.InnerException == null) UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            //    else UcMessageBoxMain.AlertMessageBox(ex.ToString(), MessageEventType.error, this, this.GetType());
            //}
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    LegalUnitOfWork unitOfWork = new LegalUnitOfWork();
            //    // validate
            //    RequestList = new List<trRequest>();
            //    foreach (GridViewRow row in gvAssign.Rows)
            //    {
            //        if (row.RowType == DataControlRowType.DataRow)
            //        {
            //            string requestNo = row.Cells[2].Text;
            //            CheckBox ChkAS_RT = (row.FindControl("ChkAS_RT") as CheckBox);
            //            DropDownList ddlLawyer = row.FindControl("ddlLawyer") as DropDownList;
            //            if (ChkAS_RT.Checked)
            //            {
            //                //trRequestRep rep = new trRequestRep();
            //                //int lawyerstatus = rep.GetRequestDataByRequestNo(requestNo).LawyerStatus;
            //                //int lawyerstatus = unitOfWork.TrRequestRep.GetRequestByID(requestNo).LawyerStatus;
            //                //if (lawyerstatus == 0) // 0: no assign, 1: Assign by senior lawyer, 2: Register by lawyer
            //                //{
            //                //trRequestRep r_rep = new trRequestRep();
            //                //trRequest req = r_rep.GetRequestDataByRequestNo(requestNo);
            //                trRequest req = unitOfWork.TrRequestRep.GetRequestDataByRequestNo(requestNo);
            //                req.RequestStatus = MasterStatusHelper.StatusRegistered; //"4"; // 4: return to pool
            //                req.RequestStatusDate = DateTime.Now;
            //                req.LawyerID = null;
            //                req.SeniorLawyer = "0";
            //                req.SeniorLawyerApprove = req.SeniorLawyerReceive = null;
            //                RequestList.RemoveAll(x => x.RequestNo == requestNo);
            //                RequestList.Add(req);
            //                //}
            //                //else throw new Exception("Req No. \"" + requestNo + "\" ถูก Assign แล้ว ไม่สามารถ Return ได้");
            //            }
            //        }
            //    }
            //    if (RequestList == null || RequestList.Count < 1)
            //    {
            //        Msg.MsgBox(MessageEvent.PleaseSelectToReturn_Assign, MessageEventType.warning, this, this.GetType());
            //    }
            //    else
            //    {
            //        foreach (var item in RequestList)
            //        {
            //            item.UpdateBy = GlobalSession.UserLogin.Emp_Code;
            //            item.UpdateDate = DateTime.Now;
            //        }
            //        unitOfWork.TrRequestRep.Update(RequestList);
            //        unitOfWork.Save();
            //        btnSearch_Click(null,null);
            //        Msg.MsgBox(MessageEvent.ReturnToPool, MessageEventType.success, this, this.GetType());
            //    }
            //    UcMessageBoxMain.VisibleControl();                
            //}
            //catch (Exception ex)
            //{
            //    Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            //}
        }
        #endregion
        protected void gvAssign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    // bind data and set data to controller
                    DropDownList ddlLawyer = e.Row.FindControl("ddlLawyer") as DropDownList;
                    //DropDownList ddlAttorneyType = e.Row.FindControl("ddlAttorneyType") as DropDownList;
                    DropDownList ddlAttorney = e.Row.FindControl("ddlAttorney") as DropDownList;
                    //CheckBox chkBox = e.Row.FindControl("ChkAS_RT") as CheckBox;
                    //chkBox.Checked = false;

                    LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
                    List<msAttorney> tmp = unitOfwork.MSAttorneyRep.GetAllLawyer().ToList();                    
                }
                catch (Exception ex)
                {
                    Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
                    //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "X");
                }
            }
        }

        protected void gvAssign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void gvAssign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAssign.PageIndex = e.NewPageIndex;
                gvAssign.DataSource = AssignLawyerList;
                gvAssign.DataBind();
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void ddlAttorneyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
                DropDownList ddlAttorneyType = (DropDownList)sender;
                GridViewRow row = ((GridViewRow)ddlAttorneyType.Parent.Parent);
                int iAttType = Convert.ToInt32(ddlAttorneyType.SelectedValue);
                if (iAttType > 0)
                {
                    List<msAttorney> lawyerList = unitOfwork.MSAttorneyRep.GetLawyerByTypeID(iAttType);
                    DropDownList ddlLawyer = row.FindControl("ddlLawyer") as DropDownList;
                    ddlLawyer.DataSource = lawyerList;
                    ddlLawyer.DataBind();
                    ddlLawyer.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "S");
            }
        }
    }
}