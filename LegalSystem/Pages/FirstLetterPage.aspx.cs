﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class FirstLetterPage : System.Web.UI.Page
    {
        string id = "";
        string legalid = "";
        string cif = "";
        string legalStatus = "";
        string redirect = "Y";
        string mode = "";
        private void SetUI()
        {
            btnApprove.Visible=false;
            btnReturn.Visible=false;
            btnSend.Visible=false;
            if (mode == "new")
            {
                btnSend.Visible = true;
            }
            else if(mode=="verify")
            {
                btnApprove.Visible = true;
                btnReturn.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            legalid = Request.QueryString["legalid"];
            cif = Request.QueryString["cif"];
            legalStatus=Request.QueryString["legal"];
            mode = Request.QueryString["mode"];
            if(!IsPostBack)
            {
                SetUI();
            }
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                msDocumentType docType = unitOfwork.MSDocumentTypeRep.Get(x => x.DocTypeID == 1).FirstOrDefault();

                wfDocument wfDoc= unitOfwork.WFDocumentRep.Get(x => x.wfDocID==new Guid(id)).FirstOrDefault();

                if (wfDoc != null)
                    wfDoc.IsCurrent = null;

                unitOfwork.WFDocumentRep.Insert(new wfDocument
                {
                    DocTypeID = 1,
                    DocTypeDesc = docType.DocTypeName,
                    LegalID = new Guid(legalid),
                    CreateBy = GlobalSession.UserLogin.Emp_Code,
                    CreateDate = DateTime.Now,
                    UpdateBy = GlobalSession.UserLogin.Emp_Code,
                    UpdateDate = DateTime.Now,
                    IsCurrent = "Y",
                    DocStatus = 1,
                    wfDocID = new Guid(id),

                });
                unitOfwork.Save();
                Response.Redirect("TrayPage.aspx?redirect=Y&cif=" + cif + "&legal=" + legalStatus);
            }
        }
        protected void btnReturn_Click(object sender, EventArgs e)
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                wfDocument wfDoc = unitOfwork.WFDocumentRep.Get(x => x.wfDocID == new Guid(id)).FirstOrDefault();

                if (wfDoc != null)
                {
                    wfDoc.DocStatus = 2;
                    wfDoc.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    wfDoc.UpdateDate = DateTime.Now;
                }
                unitOfwork.Save();
                Response.Redirect("TrayPage.aspx?redirect=Y&cif=" + cif + "&legal=" + legalStatus);
            }
        }
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                wfDocument wfDoc = unitOfwork.WFDocumentRep.Get(x => x.wfDocID == new Guid(id)).FirstOrDefault();

                if (wfDoc != null)
                {
                    wfDoc.DocStatus = 3;
                    wfDoc.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    wfDoc.UpdateDate = DateTime.Now;
                }
                unitOfwork.Save();
                Response.Redirect("TrayPage.aspx?redirect=Y&cif=" + cif + "&legal=" + legalStatus);
            }
         
        }

       

        protected void btnPreview_Click(object sender, EventArgs e)
        {

        }

       
        
    }
}