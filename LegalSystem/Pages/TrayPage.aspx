﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="TrayPage.aspx.cs" Inherits="LegalSystem.Pages.TrayPage" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="card ">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 id="trayname" runat="server" class="card-title"></h4>
                            <%--<p class="card-category">
                                <asp:Label ID="Label33" runat="server"><text style="color:#e6f2ff; font-style:italic;">กรุณาระบุข้อมูลที่มีเครื่องหมาย </text><text style="color:red; font-style:italic;">*</text> <text style="color:#e6f2ff; font-style:italic;">ให้ครบ (Fields marked with an asterisk (</text> <text style="color:red; font-style:italic;">*</text> <text style="color:#e6f2ff; font-style:italic;">) are required.)</text> </asp:Label>
                            </p>--%>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body" style="">
                                    <div class="row col-sm-12" style="margin-top:-20px;margin-bottom:2em;" >
                                        <%--<div class="col-md-1 col-sm-1 text-right"></div>--%>
                                        <div class="col-md-offset-1 text-right" style="padding-left:2em">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Print :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlPrintDoc" runat="server" DataTextField="DocTypeName" CssClass="form-control" DataValueField="DocTypeID" />
                                            </div>
                                        </div>
                                        <div class="col-md-offset-1 text-right" style="padding-left:2em">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">CIF :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" id="txtCIF" runat="server" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-offset-1 text-right" style="padding-left:2em">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Legal Status :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlLegalStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-offset-2" style="padding-left:2em">
                                            <div class="form-group">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-info" OnClick="btnSearch_Click" />
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-left:2em">
                                            <div class="form-group">
                                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-warning" OnClick="btnClear_Click" />                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1 col-sm-1 text-right"></div>                                    
                                        
                                    </div>
                                    
                                </div>
                                <%--<div class="card-footer m-auto" style="padding-bottom: 30px">                                 
                                </div>--%>
                            </div>
                            <div id="bResult" runat="server" class="card" style="display: none">
                                <div class="card-body">
                                    <asp:GridView ID="gvDatas" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                        AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" RowStyle-HorizontalAlign="Center"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText" PagerStyle-HorizontalAlign="Center"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" OnRowCommand="gvDatas_RowCommand" OnRowDataBound="gvDatas_RowDataBound">
                                        <Columns>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req No." DataField="RequestNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Type" DataField="RequestTypeName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="CIF" DataField="CifNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Name" DataField="CFNAME" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal No." DataField="LegalNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" Visible="false" DataField="LegalStatus" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" DataField="LegalStatusName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="SLA OA Send date"  ItemStyle-Width="12%" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Owner OA" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Doc Status" DataField="DocStatusName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่ออกหนังสือ" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Loan Type" />
                                            <asp:TemplateField HeaderText="Request" ItemStyle-Width="5%" ControlStyle-Font-Size="Smaller">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgBtnRequest" runat="server" ImageUrl="~/Images/first.png" CommandName="Request" />
                                                    <asp:HiddenField ID="hidDocStatus" runat="server" Value='<%# Eval("Docstatus") %>' />
                                                    <asp:HiddenField ID="hidLegalID" runat="server" Value='<%# Eval("LegalID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Verify" ItemStyle-Width="5%" ControlStyle-Font-Size="Smaller">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgBtnVer" runat="server" ImageUrl="~/Images/first.png" CommandName="Verify" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Print" ItemStyle-Width="5%" ControlStyle-Font-Size="Smaller">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgPrint" runat="server" ImageUrl="~/Images/add-icon.png" CommandName="Print" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <div class="row">
                                        <div class="card-footer m-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
