﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="AssignPage.aspx.cs" Inherits="LegalSystem.Pages.AssignPage" %>

<%@ OutputCache Location="None" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery1-1.12.0.min.js"></script>
    <script src="../Scripts/plugin/bootstrap-datepicker.js"></script>
    <script src="../Scripts/plugin/bootstrap-datepicker-custom.js"></script>
    <script src="../Scripts/plugin/bootstrap-datepicker.th.min.js"></script>
    <%-- Script .b ADD 20190903 --%>
    <script src="../Scripts/Site.js"></script>

    <script type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <uc1:ucmessagebox runat="server" id="UcMessageBoxMain" />
                            <div class="card ">
                                <div class="card-header card-header-tabs card-header-info">
                                    <h4 class="card-title">Reassign Lawyer and OA/IA</h4>
                                </div>
                                <div class="card-body col-lg-12">
                                    <div class="card" style="margin-top: -1px;">
                                        <div class="card-body">
                                            <div class="row" style="margin-bottom: -20px; margin-top: -20px;">
                                                <div class="col-md-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">CIF :</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtCIF" runat="server" class="form-control" style="padding-left: 1em;" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">Request No :</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtReqNo" runat="server" style="padding-left: 1em;" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-sm-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">Legal No :</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" id="txtLegalNo" runat="server" style="padding-left: 1em;" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-info" OnClick="btnSearch_Click" />&nbsp;
                                                        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-warning" OnClick="btnClear_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">Legal Status :</span>
                                                            <%--<span class="tim-note">Request Type :</span>--%>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlLegalStatus" runat="server" CssClass="form-control" Style="padding-left: 1em;"></asp:DropDownList>
                                                        <%--<asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">Lawyer :</span>
                                                            <%--<span class="tim-note">Request Type :</span>--%>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlLawyer" runat="server" CssClass="form-control" Style="padding-left: 1em;"></asp:DropDownList>
                                                        <%--<asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <div class="tim-typo mt-4-5">
                                                        <h5>
                                                            <span class="tim-note">OA / IA :</span>
                                                            <%--<span class="tim-note">Request Type :</span>--%>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlAttorney" runat="server" CssClass="form-control" Style="padding-left: 1em;"></asp:DropDownList>
                                                        <%--<asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                                    </div>
                                                </div>

                                            </div>
                                            <br>
                                        </div>

                                    </div>
                                    <div id="bResult" runat="server" class="card" visible="false">
                                        <div class="card-body">
                                            <asp:GridView ID="gvAssign" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False" RowStyle-HorizontalAlign="Center"
                                                EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText" PagerStyle-HorizontalAlign="Center"
                                                AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                                PagerSettings-NextPageImageUrl="~/Images/next.png" OnRowDataBound="gvAssign_RowDataBound" OnRowCommand="gvAssign_RowCommand" OnPageIndexChanging="gvAssign_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Request ID" DataField="RequestID" Visible="false" />
                                                    <%--                                                    <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="...">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkAS_RT" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req No." DataField="RequestNo" ItemStyle-Width="1%" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Type" DataField="ReqTypeName" ItemStyle-Width="9%" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="CIF" DataField="CifNo" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Name" DataField="CFNAME" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Date" DataField="RequestDate" DataFormatString="{0:d}" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal No." DataField="LegalNo" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" Visible="false" DataField="LegalStatus" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" DataField="LegalStatusName" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Address" DataField="Address" ItemStyle-Width="20%" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Date" DataField="LegalStatusDate" DataFormatString="{0:d}" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Assign OA/IA" DataField="AttorneyName" />
                                                    <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Assign Lawyer" DataField="LawyerName" />
                                                    <%--<asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Assign OA/IA" ItemStyle-Width="7%">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlAttorney" runat="server" DataValueField="AttorneyTypeID" DataTextField="LawyerName" CssClass="form-control"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Assign Lawyer" ItemStyle-Width="7%">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlLawyer" DataValueField="Emp_Code" DataTextField="TH_Name" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                                    <asp:TemplateField ItemStyle-Width="5%" ControlStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%--Style="padding: 2px; width: 35px;"--%>
                                                            <asp:HyperLink ID="lnkPcName" runat="server" Text="View" CssClass="btn btn-info btn-sm"
                                                                NavigateUrl='<%# "~/Pages/RequestPage.aspx?reassign=" + Eval("RequestID") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <div class="row" style="padding-bottom: 2em">
                                                <div class="card-footer m-auto">
                                                    <%--                                                    <asp:Button runat="server" ID="btnAssign" CommandName="Assign" Text="Assign" CssClass="btn btn-success" OnClientClick="ConfirmAssign(this);" OnClick="btnAssign_Click" Visible="false" />&nbsp;
                                                    <asp:Button runat="server" ID="btnReturn" CommandName="Return" Text="Return to pool" CssClass="btn btn-warning" OnClientClick="ConfirmAssign(this);" OnClick="btnReturn_Click" />--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="loading-visible" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: white; opacity: 0.75;">
                        <p>
                            <img style="padding: 10px; position: fixed; top: 50%; left: 50%;" src="../Resources/Preloader_4.gif" />
                        </p>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
    <div id="fade" class="black_overlay"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
