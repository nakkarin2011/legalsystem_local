﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TCRB.Securities;
using System.Globalization;
using TCRB.Securities.UserManagement;
using LegalSystem.CommonHelper;
using LegalSystem.Common.Common_RequestPage;
using LegalSystem.CommonFunctionFlow;

namespace LegalSystem.Pages
{
    public partial class RequestPage : System.Web.UI.Page
    {
        #region
        List<trRequest> ListRNO = new List<trRequest>();
        trRequest RNO = new trRequest();
        string vCIF = string.Empty;
        trLegal Legal = new trLegal();
        string User;
        string ListUserLogin;
        int GroupID = 0;
        List<int> LiGroupID = new List<int>();
        AlertMessage Msg = new AlertMessage();
        InsertDataCaseEditRefer insertCaseEdit = new InsertDataCaseEditRefer();
        trRequest trCIF = new trRequest();
        SetUI_RequestPage UI_RequestPage = new SetUI_RequestPage();
        #region SESSION
        public UserIdentity UserLogin
        {
            get { return Session["UserLogin"] != null ? Session["UserLogin"] as UserIdentity : null; }
            set { Session["UserLogin"] = value; }
        }
        private string rid
        {
            get { return Session["RID"] != null ? Session["RID"] as string : null; }
            set { Session["RID"] = value; }
        }
        private string acct
        {
            get { return Session["ACCT"] != null ? Session["ACCT"] as string : null; }
            set { Session["ACCT"] = value; }
        }
        private bool IsSubmited
        {
            get { return Session["IsSubmited"] != null ? Convert.ToBoolean(Session["IsSubmited"]) : false; }
            set { Session["IsSubmited"] = value; }
        }
        private List<LNMAST> LNs
        {
            get { return Session["ListAcctLoan"] != null ? Session["ListAcctLoan"] as List<LNMAST> : null; }
            set { Session["ListAcctLoan"] = value; }
        }
        public string RequestNo
        {
            get { return Request.QueryString["rno"] != null ? Request.QueryString["rno"] : null; }
            set { Request.QueryString["rno"] = value; }
        }
        public string IDReassign
        {
            get { return Request.QueryString["reassign"] != null ? Request.QueryString["reassign"] : null; }
            set { Request.QueryString["reassign"] = value; }
        }
        #endregion
        #endregion
        #region Functions
        private void SetUI()
        {
            if (RequestNo == null)
                Session.Remove("ListAcctLoan");
            rid = (Request.QueryString["rid"] != null) ? Request.QueryString["rid"].ToString() : string.Empty;
            acct = (Request.QueryString["ACCT"] != null) ? Request.QueryString["ACCT"].ToString() : string.Empty;
            string cif = (Request.QueryString["CIF"] != null) ? Request.QueryString["CIF"].ToString() : string.Empty;

            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
            {
                //string Reason = ddlReason.SelectedItem.Value;
                #region /**** SET DropDownList ****/
                List<vRequestTypeMapLegalStstus> ddlRType = new List<vRequestTypeMapLegalStstus>();
                UI_RequestPage.Set_DropDownList(unitOfWork, ddlRequestStatus, ddlLegalStatus, ddlRequestType, ddlApprover, rid, User, ddlRType, MasterECommandNameHelper.Edit);
                GetRequestReason(ddlReason, unitOfWork);
                #endregion

                if (!string.IsNullOrEmpty(rid)) /****Edit****/
                {
                    #region
                    titlePage.InnerText = MasterPageTitleHelper.InnerRequest;
                    trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                    trLegal Legal = unitOfWork.TrLegalRep.Get(x => x.LegalID == Req.LegalID).FirstOrDefault();

                    hidRequestID.Value = Req.RequestID.ToString();
                    hidRefRequestNo.Value = Req.RefRequestNo;
                    hidLegalStatus.Value = Req.LegalStatus;

                    /****Set disable fields****/
                    IsSubmited = (Req.RequestStatus_ >= 2 && Req.RequestStatus_ != 4);//ใช้ Disable field
                    if (Req.RequestStatus_ > 1)
                        txtCIF.Attributes["disabled"] = "disabled";

                    divComment.Visible = (Req.RequestStatus_ >= 1);

                    InitField(Req.RequestStatus_, Req, unitOfWork);

                    /****Set value fields****/
                    txtRequestNo.Value = Req.RequestNo;
                    txtRequestDate.Value = Req.RequestDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                    ddlRequestStatus.SelectedValue = Req.RequestStatus;
                    ddlRequestType.SelectedValue = Req.RequestType;
                    ddlReason.SelectedValue = Req.RequestReason;
                    txtReferRequestNo.Value = Req.RefRequestNo;
                    if (!string.IsNullOrEmpty(Req.RefRequestType))
                        txtReferRequestType.Value = ddlRType[Convert.ToInt32(Req.RefRequestType)].ReqTypeName.ToString();
                    txtLegalNo.Value = Legal == null ? "" : Legal.LegalNo;
                    ddlLegalStatus.SelectedValue = Req.LegalStatus;
                    txtCIF.Value = Req.CifNo.ToString();
                    txtAddress.Value = Req.Address;
                    ddlApprover.SelectedValue = (!string.IsNullOrEmpty(Req.ColHead) ? Req.ColHead : Req.ColChecker);
                    ddlReason.SelectedValue = (!string.IsNullOrEmpty(Req.RequestReason) ? Req.RequestReason : "0");
                    txtComment.Text = Req.Comment;

                    LNs = unitOfWork.LnmastRep.GetInfomationByCIF(LegalSystem.Properties.Settings.Default.LinkServer, Req.CifNo.ToString(), rid, Req.RequestStatus_);
                    BinducAccountLoan();

                    #region  /*** SET Attachments ****/
                    UI_RequestPage.SetAttachments(Req, divAttachments, IsSubmited, GroupID);
                    UI_RequestPage.Set_ddlAssignLawyer(unitOfWork, ddlAttorney, ddlLawyer, Req.RequestType, User);
                    if (Req.RequestType == "2" || ddlRequestType.SelectedItem.Value == "2")
                    {
                        var headApprover = unitOfWork.VCollectionHeadApproveRep.GetApprove();
                        if (ddlRequestType.SelectedItem.Value == "0")
                            ddlRequestType.SelectedItem.Value = Req.RequestType;
                        UI_RequestPage.Set_DDLApproverType(unitOfWork, ddlApprover, ddlRequestType, GroupID.ToString());
                    }

                    #endregion
                    if (GroupID == MasterPermissionHelper.LegalSpecialist || GroupID == MasterPermissionHelper.SeniorLawyer || GroupID == MasterPermissionHelper.Lawyer || GroupID == MasterPermissionHelper.CollectionHead)
                    { SetDisableGridRelation(); }
                    else { SetEnableGridRelation(); }

                    #endregion
                }
                else /****Add****/
                {
                    #region
                    if (!string.IsNullOrEmpty(cif)) /****New Request form WorkList****/
                    {
                        txtCIF.Value = cif;
                        btnFindCIF_Click(null, null);
                    }
                    else if (vCIF == "0")
                    {
                        txtCIF.Value = "";
                    }

                    string[] chks = SetUI_RequestPage.chks;

                    for (int i = 0; i < SetUI_RequestPage.chks.Count(); i++)
                    {
                        var control = ((HtmlInputCheckBox)divAttachments.FindControl(chks[i]));
                        control.Attributes.Add("enabled", "enabled");
                        control.Attributes.Remove("checked");
                    }
                    SetEnableGridRelation();
                    hidRequestID.Value = unitOfWork.LegalStoreProcedure.GetGuidID().ToString();
                    txtRequestDate.Value = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    ddlRequestStatus.SelectedIndex = 0;
                    ddlRequestType.SelectedIndex = 0;
                    ddlApprover.SelectedIndex = 0;
                    divComment.Visible = btnSubmit.Enabled = false;
                    btnDeleteDraf.Visible = btnHeadReject.Visible = btnApproveReturn.Visible = false;
                    #endregion
                }
            }
        }

        private void InitField(int status, trRequest Req, LegalUnitOfWork unitOfWork)
        {
            divFooterHeader.Visible = false;
            divFooterMakerChecker.Visible = false;
            divFooterReferCase.Visible = false;
            btnFindCIF.Visible = false;
            divApprover1.Visible = false;
            divApprover2.Visible = false;
            txtCIF.Attributes.Add(MasterAttributesHelper.Readonly, MasterAttributesHelper.Readonly);
            switch (status)
            {
                case 1:
                    if (GroupID == MasterPermissionHelper.CollectionMaker || GroupID == MasterPermissionHelper.CollectionChecker)
                    {
                        btnApproveReturn.Visible = btnHeadReject.Visible = false; btnDeleteDraf.Visible = true;
                    }
                    if (status == 1)
                    {
                        IsSubmited = false;
                        btnSaveDraf.Visible = true; btnSubmit.Visible = true; divFooterMakerChecker.Visible = true;
                    }
                    break;
                case 2:
                    if (GroupID == MasterPermissionHelper.CollectionHead /*442*/)
                    {
                        btnApproveReturn.Visible = btnHeadReject.Visible = true;
                        divFooterMakerChecker.Visible = btnApproveReturn.Visible = true;
                        btnDeleteDraf.Visible = btnSaveDraf.Visible = false;
                        FSApprover1.Visible = FSApprover2.Visible = true;
                        IsSubmited = true;
                        divComment.Visible = true;
                        divAttachments.Visible = true;
                        ddlReason.Enabled = false; ddlRequestType.Enabled = false;
                    }
                    else if (GroupID == MasterPermissionHelper.CollectionMaker /*440*/)
                    { btnApproveReturn.Visible = btnHeadReject.Visible = false; }
                    else if (GroupID == MasterPermissionHelper.CollectionChecker /*441*/)
                    {
                        btnSaveDraf.Visible = btnDeleteDraf.Visible = btnApproveReturn.Visible = false; btnHeadReject.Visible = true;
                        divFooterMakerChecker.Visible = btnApproveReturn.Visible = true;
                    }
                    else
                    {
                        divFooterHeader.Visible = true;
                        divAttachments.Visible = true;
                        divComment.Visible = false;
                        IsSubmited = true;
                    }
                    break;
                case 3:
                    IsSubmited = false;
                    btnFindCIF.Visible = false;
                    divFooterHeader.Visible = true;
                    txtComment.Enabled = true;
                    if (string.IsNullOrEmpty(Req.LegalSpecial))
                    {
                        ddlApprover.Enabled = false;
                        btnLSApprove.Visible = true;
                        btnApprove.Visible = false;
                        btnHeadReject.Visible = btnReturn.Visible = btnReject.Visible = false;
                        FSApprover1.Visible = FSApprover2.Visible = true;
                        ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetAllUsers();
                        ddlApprover.DataBind();
                    }
                    else
                    {
                        btnLSApprove.Visible = false;
                        ddlApprover.Enabled = true;
                        btnApprove.Visible = true;
                        btnHeadReject.Visible = btnReturn.Visible = btnReject.Visible = true;
                        btnApprove.Text = MasterButtonHelper.Register;
                    }
                    UpdatePanel1.Attributes.Add("disabled", "true");
                    FieldAttachment.Disabled = true;
                    ddlRequestType.Enabled = false; ddlReason.Enabled = false;
                    break;
                case 4:
                    if (Req.SeniorLawyerReceive == null)
                    {
                        ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetAllUsers();
                        ddlApprover.DataBind();
                        btnLSApprove.Text = MasterButtonHelper.GetTask + " to Inbox";
                        btnLSApprove.Visible = true;
                        btnReturn.Visible = btnReject.Visible = btnApprove.Visible = false;
                        divFooterMakerChecker.Visible = divFooterReferCase.Visible = false;
                        divFooterHeader.Visible = true;
                        //FSApprover1.Visible = divApprover1.Visible = false;
                        //FSApprover2.Visible = divApprover2.Visible = false;
                        ddlApprover.Enabled = false;
                        Label33.Visible = false;
                        FieldAttachment.Disabled = FieldView.Disabled = true; ddlRequestType.Enabled = false;
                        txtComment.Enabled = false;
                    }
                    else if (!string.IsNullOrEmpty(Req.SeniorLawyer))
                    {
                        //btnApprove.Text = MasterButtonHelper.Register;
                        //divFooterMakerChecker.Visible = false;
                        //divFooterHeader.Visible = true;
                        //divFooterReferCase.Visible = false;
                        //FSApprover1.Visible = divApprover1.Visible = false;
                        //FSApprover2.Visible = divApprover2.Visible = false;
                        //FieldAttachment.Disabled = FieldView.Disabled = true; ddlRequestType.Enabled = btnLSApprove.Visible = btnReturn.Visible = btnReject.Visible = false;
                        UpdatePanel1.Attributes.Add("disabled", "true");
                        ddlApprover.Enabled = ddlReason.Enabled = false;
                        FieldAttachment.Disabled = true;
                        IsSubmited = false;
                        btnFindCIF.Visible = false;
                        divFooterHeader.Visible = true;
                        //ddlRequestType.Attributes.Add("disabled", "disabled");
                        //ddlReason.Attributes.Add("disabled", "disabled");
                        btnReturn.Visible = btnApprove.Visible = btnReject.Visible = ddlRequestType.Enabled = false;
                        FSApprover1.Visible = divApprover1.Visible = false;
                        FSApprover2.Visible = divApprover2.Visible = false;
                        btnSave.Visible = btnCFAssign.Visible = btnReturnToPool.Visible = true;
                        FSAssignOA_IA1.Visible = FSAssignOA_IA2.Visible = FSAssignLawyer1.Visible = FSAssignLawyer2.Visible = true;
                        ddlAttorney.SelectedValue = Req.LawyerAttorneyID; ddlLawyer.SelectedValue = Req.LawyerID;
                        if (Req.RequestType.Contains("7"))
                        {
                            FSApprover1.Visible = divApprover1.Visible = true;
                            FSApprover2.Visible = divApprover2.Visible = true;
                        }
                        if (Req.RequestType.Contains("2"))
                        {
                            btnReturn.Visible = true;
                        }
                    }
                    ddlReason.Enabled = false;
                    break;
                case 5:
                    UpdatePanel1.Attributes.Add("disabled", "true");
                    ddlApprover.Enabled = ddlReason.Enabled = false;
                    FieldAttachment.Disabled = true;
                    IsSubmited = false;
                    btnFindCIF.Visible = false;
                    divFooterHeader.Visible = true;
                    //ddlRequestType.Attributes.Add("disabled", "disabled");
                    //ddlReason.Attributes.Add("disabled", "disabled");
                    btnReturn.Visible = btnApprove.Visible = btnReject.Visible = ddlRequestType.Enabled = false;
                    FSApprover1.Visible = divApprover1.Visible = false;
                    FSApprover2.Visible = divApprover2.Visible = false;
                    btnSave.Visible = btnCFAssign.Visible = btnReturnToPool.Visible = true;
                    FSAssignOA_IA1.Visible = FSAssignOA_IA2.Visible = FSAssignLawyer1.Visible = FSAssignLawyer2.Visible = true;
                    ddlAttorney.SelectedValue = Req.LawyerAttorneyID; ddlLawyer.SelectedValue = Req.LawyerID;
                    break;
                case 6:
                case 7:
                    UpdatePanel1.Attributes.Add("disabled", "true");
                    ddlApprover.Enabled = ddlReason.Enabled = false;
                    FieldAttachment.Disabled = true;
                    IsSubmited = false;
                    btnFindCIF.Visible = false;
                    divFooterHeader.Visible = true;
                    //ddlRequestType.Attributes.Add("disabled", "disabled");
                    //ddlReason.Attributes.Add("disabled", "disabled");
                    btnApprove.Visible = ddlRequestType.Enabled = false;
                    FSApprover1.Visible = divApprover1.Visible = true;
                    FSApprover2.Visible = divApprover2.Visible = true;
                    ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetAllUsers();
                    ddlApprover.DataBind();

                    if (status == 6)
                    {
                        btnAccept.Visible = true;
                        btnSave.Visible = btnReject.Visible = btnReturn.Visible = false;
                    }
                    else
                    {
                        btnReject.Visible = btnReturn.Visible = true;
                        btnSave.Visible = btnAccept.Visible = false;
                    }
                    break;
                default:
                    ddlRequestType.Attributes.Add("disabled", "disabled");
                    ddlReason.Attributes.Add("disabled", "disabled");
                    ddlApprover.Attributes.Add("disabled", "disabled");
                    btnSaveDraf.Visible = btnSubmit.Visible = false;
                    if (status < 3 || status > 4)
                        txtComment.Attributes.Add("readonly", "readonly");
                    IsSubmited = true;
                    btnFindCIF.Visible = true;
                    divFooterHeader.Visible = status == 3;//เป็น Legal Specialist
                    divFooterMakerChecker.Visible = (status == 3 && Req.LegalID == null);
                    divFooterReferCase.Visible = (status == 3 && Req.LegalID != null);
                    break;
            }
        }

        private void GetRequestReason(DropDownList ddl, LegalUnitOfWork unitOfwork)
        {
            //*** Application List : View ***//            
            try
            {
                if (btnRefer.Visible == false)
                {
                    var Reason = ((ddl.SelectedIndex > 0) ? ddl.SelectedIndex : 0);
                    ddl.DataSource = unitOfwork.VRequestReasonRep.GetReasonByRequest(Convert.ToInt32(ddlRequestType.SelectedValue));
                    ddl.DataBind();
                    ddl.SelectedIndex = Reason;
                }
                else if (btnRefer.Visible == true)
                {
                    using (LegalUnitOfWork Rep = new LegalUnitOfWork())
                    {
                        switch (Convert.ToInt32(ddlRequestType.SelectedValue))
                        {
                            case MasterRequestReasonHelper.LitigationsCancelReason://5:ยกเลิกดำเนินคดี                                
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlLitigationsCancelReason);
                                break;
                            case MasterRequestReasonHelper.ReasonWithdrawal://6:กรณีถอนฟ้อง
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlReasonWithdrawal);
                                break;
                            case MasterRequestReasonHelper.ExecutionsReason: //7:กรณีขอบังคับคดี
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlExecutionsReason);
                                break;
                            case MasterRequestReasonHelper.CancelExecutionReason: //8:กรณียกเลิกบังคับคดี
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlCancelExecutionReason);
                                break;
                            case MasterRequestReasonHelper.ExecutiondelayReason:
                            case MasterRequestReasonHelper.CancelExecutiondelayReason:
                            case MasterRequestReasonHelper.AuctionDelayReason:
                            case MasterRequestReasonHelper.AuctionDelayReasonToDate:
                                //9:กรณีขอชะลอการบังคับคดี
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlExecutiondelayReason);
                                //10:กรณียกเลิกชะลอการบังคับคดี
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlCancelExecutiondelayReason);
                                //11:กรณีขอชะลอขายทอดตลาด
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlAuctionDelayReason);
                                //12:กรณียกเลิกชะลอขายทอดตลาด
                                unitOfwork.VRequestReasonRep.GetReasonByRefercase(Convert.ToInt32(ddlRequestType.SelectedValue), ddlAuctionDelayReasonToDate);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        private void BinducAccountLoan()
        {
            gvAccountLoan.DataSource = LNs;
            gvAccountLoan.DataBind();
            if (LNs.Count > 0)
            {
                txtAddress.Value = LNs[0].CFADDR;
                txtCFNAME.Value = LNs[0].ACNAME;
            }
            else
            {
                txtAddress.Value = txtCIF.Value = string.Empty;
                txtCFNAME.Value = string.Empty;
            }
        }

        private string GetTypeFile()
        {
            string Attachments = (chk1.Checked ? "1," : "0,")//หนังสือบอกกล่าวผู้ค้ำประกัน พร้อมไปรษณีย์ตอบรับ
                + (chk11.Checked ? "1," : "0,")   //มี
                + (chk12.Checked ? "1," : "0,")   //ไม่มี เพราะไม่มีผู้ค้ำประกัน
                + (chk2.Checked ? "1," : "0,")    //หนังสือแจ้งวันขายทอดตลาดหลักทรัพย์จำนำ พร้อมไปรษณีย์ตอบรับ
                + (chk21.Checked ? "1," : "0,")   //ใบรายงานการประมูล
                + (chk3.Checked ? "1," : "0,")    //มีบัญชีเงินฝาก
                + (chk31.Checked ? "1," : "0,")   //มี ตัดบัญชีเงินฝากชำระหนี้เงินกู้ทั้งจำนวน
                + (chk32.Checked ? "1," : "0,")   //มีบันทึกถึงฝ่ายปฏิบัติการกลาง (ไม่สามารถตัดบัญชีออมทรัพย์ได้)
                + (chk4.Checked ? "1," : "0,")    //ไม่มีบัญชีเงินฝาก
                + (chk41.Checked ? "1," : "0,")   //ลูกหนี้หลบหนี/ไม่สามารถติดต่อลูกหนี้ได้เป็นเวลานาน
                + (chk42.Checked ? "1" : "0");    //ลูกหนี้ไม่มีความสามารถในการชำระหนี้
            return Attachments;
        }


        private bool ValidateField(string btn)
        {
            ListGroupID.ClassListGroupID(LiGroupID);
            GroupID = UserLogin.Groups[0].GroupID;
            GetTypeFile();
            LiGroupID.Contains(GroupID);
            if (RequestNo == null && btn == "D" && GroupID == 440 || RequestNo != null && btn != "D" && GroupID == 441 || RequestNo == null && btn == "D" && GroupID == 441)
                return true;
            else if (ddlRequestType.SelectedIndex == 0 || string.IsNullOrEmpty(txtCIF.Value) || ddlReason.SelectedIndex == 0 || ddlApprover.SelectedIndex == 0 && !LiGroupID.Contains(GroupID) || ddlApprover.SelectedIndex == 0 && btn == "S")
            {
                Msg.MsgBox(MessageEvent.RequiredFieldAsterisk, MessageEventType.info, this, this.GetType());
            }
            else
                return true;
            return false;
        }

        private trRequest GetUIRequest()
        {
            trRequest req = new trRequest();
            req.RequestNo = txtRequestNo.Value;
            req.RequestDate = DateTime.Today;
            req.RequestType = ddlRequestType.SelectedValue;
            req.RequestStatusDate = DateTime.Today;
            req.RequestReason = ddlReason.SelectedValue;
            req.RefRequestNo = txtReferRequestNo.Value;
            req.RefRequestType = hidReferRequestType.Value;
            req.RefRequestReason = string.Empty;
            req.LegalStatus = ddlLegalStatus.SelectedValue;
            req.CifNo = Convert.ToDecimal(txtCIF.Value);
            req.Address = txtAddress.Value;

            return req;
            #region Cancel
            /*
            if (Req != null)
            {
                if (Req.RequestNo == txtRequestNo.Value)
                {
                    Req.RequestType = ddlRequestType.SelectedValue;
                    Req.RequestReason = ddlReason.SelectedValue;
                    Req.ApproverCol = ddlApprover.SelectedValue;
                }
                else//Refer Case
                {
                    Req.RequestNo = txtRequestNo.Value;
                    Req.RequestDate = DateTime.Today;
                    Req.RequestType = ddlRequestType.SelectedValue;
                    Req.RequestStatusDate = DateTime.Today;
                    Req.RefRequestNo = txtReferRequestNo.Value;
                    Req.RefRequestType = hidReferRequestType.Value;
                    Req.RefRequestDate = Convert.ToDateTime(txtDelayProsecution.Value);
                    Req.RefRequestReason = ddlLitigationsCancelReason.SelectedValue;
                   // Req.ApproverCol = ddlApprover.SelectedValue;
                  //  Req.Owner = GlobalSession.UserLogin.Emp_Code;
                }
            }
            else
            {
                req = new trRequest()
                {
                    RequestNo = txtRequestNo.Value,
                    RequestDate = DateTime.Today,
                    RequestType = ddlRequestType.SelectedValue,
                    RequestStatusDate = DateTime.Today,
                    RequestReason = ddlReason.SelectedValue,
                    //DefendantNo = txtRequestNo.Value,
                    RefRequestNo = txtReferRequestNo.Value,
                    RefRequestType = hidReferRequestType.Value,
                    //RefRequestDate=
                    RefRequestReason = string.Empty,
                    LegalNo = txtLegalNo.Value,
                    LegalStatus = ddlLegalStatus.SelectedValue,
                    //LegalStatusDate = txtRequestNo.Value,
                    CifNo = Convert.ToDecimal(txtCIF.Value),
                    Address = txtAddress.Value,
                    Owner = GlobalSession.UserLogin.Emp_Code,
                    ApproverCol = ddlApprover.SelectedValue,
                    ApproverLaw = string.Empty
                    //Attachments = txtRequestNo.Value,
                };
            }
            */
            #endregion
        }

        private List<trRequestRelation> GetUIRequestRelation(ref int DefendantNo, string ReqID)
        {
            List<trRequestRelation> RRs = new List<trRequestRelation>();
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                trRequestRelation RR = new trRequestRelation();
                string CollateralType = string.Empty;
                DefendantNo = 0;
                foreach (GridViewRow rowA in gvAccountLoan.Rows)
                {
                    var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                    if (chkAL.Checked)
                    {
                        DefendantNo++;
                        var txtAL = (HtmlInputText)rowA.FindControl("txtAcctLoan");
                        var txtCT = (HtmlTextArea)rowA.FindControl("txtCollateralType");
                        if (string.IsNullOrEmpty(CollateralType))
                            CollateralType = txtCT.Value;
                        else
                        {
                            if (CollateralType != txtCT.Value)
                                throw new Exception(MessageEvent.SelectSameCollateral);
                        }
                        var gvR = (GridView)rowA.FindControl("gvRelationship");
                        int rrNum = 0;
                        foreach (GridViewRow rowR in gvR.Rows)
                        {
                            var hidCIF = (HiddenField)rowR.FindControl("hidCIF");
                            var hidType = (HiddenField)rowR.FindControl("hidType");
                            var hidOrder = (HiddenField)rowR.FindControl("hidOrder");
                            var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                            if (chkR.Checked)
                            {
                                rrNum++;
                                RR = new trRequestRelation()
                                {
                                    RRID = unitOfwork.LegalStoreProcedure.GetGuidID(),
                                    RequestID = new Guid(ReqID),
                                    Account = Convert.ToDecimal(txtAL.Value),
                                    RelationCifNo = Convert.ToDecimal(hidCIF.Value),
                                    RelationType = hidType.Value,
                                    RelationSeq = Convert.ToInt32(hidOrder.Value)
                                };
                                RRs.Add(RR);
                            }
                        }
                        if (rrNum == 0)
                            throw new Exception(MessageEvent.SelectRelationship + txtAL.Value);

                    }
                    else
                    {
                        var gvR = (GridView)rowA.FindControl("gvRelationship");
                        foreach (GridViewRow rowR in gvR.Rows)
                        {
                            var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                            chkR.Attributes.Remove("checked");
                        }
                    }
                }
            }
            if (DefendantNo == 0 || RRs.Count == 0)
                throw new Exception(MessageEvent.SelectLoanAccountAndRelationship);

            return RRs;
        }

        #region Cancel
        //[WebMethod]
        //public static void gvRelationshipUpdate(String ACCTNO)
        //{
        //    gvRelationshipUpdate(ACCTNO);
        //}

        //public static void gvRelationshipUpdate(String ACCTNO)
        //{
        //    Page objp = new Page();
        //    List<vCoverpage> tmps = objp.Session["Coverpages"] as List<vCoverpage>;
        //    vCoverpage r = tmps.Where(D => D.DocTypeID == Convert.ToInt32(ACCTNO)).FirstOrDefault();
        //    r.IsSelect = !r.IsSelect;
        //    objp.Session["Coverpages"] = tmps;
        //}
        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    User = UserLogin.Emp_Code;
                    GroupID = UserLogin.Groups[0].GroupID;
                    if (IDReassign != null)
                    {
                        ViewReAssign();
                    }
                    else
                    {
                        if (RequestNo != null)//*** รับค่า RequestNo จาก Inquiry Request > View || Inbox View Case F3 ***//
                        { sendViewClick(); }
                        else
                        {  //*** Default || Create Request ***//             
                            SetUI();
                            txtCIF.Focus();
                            divApprover1.Visible = divApprover2.Visible = true;
                        }
                    }
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                Msg.MsgReturn(ex.Message.ToString().Replace("'", " "), Page, this, MasterPageHelper.RedirectLoginPage/* "../Login.aspx"*/, MessageEventType.error);
            }
        }

        protected void ddlRequestType_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                if (string.IsNullOrEmpty(hidRefRequestNo.Value))
                    GetRequestReason(ddlReason, unitOfwork);

                switch (ddlRequestType.SelectedItem.Value)
                {
                    case "1":
                    case "2":
                        if (GlobalSession.GetPermission == GlobalSession.PermissionID.CollectionMaker || GlobalSession.GetPermission == GlobalSession.PermissionID.CollectionChecker)
                        {
                            UI_RequestPage.Set_DDLApproverType(unitOfwork, ddlApprover, ddlRequestType, GlobalSession.UserLogin.Groups[0].GroupID.ToString());
                        }
                        else
                        {

                        }
                        break;
                    case "0":
                        FieldCaseRefer.Visible = false;
                        break;
                    default:
                        if (btnEdit.Visible == true)
                        {
                            CaseReferHideOrShow();
                        }
                        break;
                }

            }
        }

        protected void btnFindCIF_Click(object sender, EventArgs e)
        {
            try
            {
                trCIF = new trRequest();
                if (!string.IsNullOrEmpty(txtCIF.Value))
                {
                    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                    {
                        var ListCif = unitOfwork.TrRequestRep.GetRequestByCIF(txtCIF.Value);
                        ListCif = ListCif.Where(w => w.RequestStatus != MasterStatusHelper.StatusRejected && w.RequestStatus != MasterStatusHelper.StatusRejectByLawyer).ToList();
                        if (ListCif.Count < 1 || sender.ToString() == MasterECommandNameHelper.View)
                        {
                            LNs = unitOfwork.LnmastRep.GetInfomationByCIF(LegalSystem.Properties.Settings.Default.LinkServer, txtCIF.Value, rid, 0);
                            if (LNs.Count > 0)
                                BinducAccountLoan();
                            else
                            {
                                Msg.MsgBox(MessageEvent.LoanAccountNotFound, MessageEventType.info, this, this.GetType());
                                LNs = new List<LNMAST>();
                                BinducAccountLoan();
                            }
                        }
                        else
                        {
                            Msg.MsgBox(MessageEvent.SearchCIFDuplicate, MessageEventType.info, this, this.GetType());
                            LNs = new List<LNMAST>();
                            BinducAccountLoan();
                        }
                    }
                }
                else
                {
                    Msg.MsgBox(MessageEvent.PleaseEnterCIFNo, MessageEventType.error, this, this.GetType());
                    LNs = new List<LNMAST>();
                    BinducAccountLoan();
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void gvAccountLoan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            using (LegalUnitOfWork unitofwork = new LegalUnitOfWork())
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LNMAST ln = e.Row.DataItem as LNMAST;
                    var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkAccount")));
                    control.Attributes.Add("onclick", "javascript:checkChange('" + ln.ACCTNO + "');");
                    control.Style.Add("cursor", "pointer");
                    if (ln.isSelect)
                        control.Attributes.Add("checked", "checked");
                    if (IsSubmited || !ln.isEnable)//
                        control.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled/*"disabled", "disabled"*/);
                    ((HtmlInputText)(e.Row.FindControl("txtAcctLoan"))).Value = ln.ACCTNO.ToString();

                    using (ProdTypeDetailRep rep = new ProdTypeDetailRep())
                    {
                        var LoanDesc = rep.GetByTypeCode(ln.TYPE);
                        ((HtmlInputText)(e.Row.FindControl("txtLoanDesc"))).Value = string.Format("{0} ({1})", ln.TYPE, LoanDesc.SubTypeName);
                    }
                    ((HtmlInputText)(e.Row.FindControl("txtAcctStatus"))).Value = ln._STATUS;
                    ((HtmlInputText)(e.Row.FindControl("txtDPD"))).Value = ln.PDDAYS.ToString();
                    ((HtmlInputText)(e.Row.FindControl("txtTDRCNT"))).Value = ln.TDRCNT;
                    ((HtmlInputText)(e.Row.FindControl("txtSTAGE"))).Value = (string.IsNullOrEmpty(ln.STAGE) ? string.Empty : ln.STAGE);
                    ((HtmlTextArea)(e.Row.FindControl("txtCollateralType"))).Value = ln.CollateralDesc.Replace('#', '\n');

                    List<sp_GetDataRelate> Rs = new List<sp_GetDataRelate>();
                    string[] Collateral = ln.CollateralDesc.Split(' ');
                    Rs = unitofwork.LegalStoreProcedure.GetDataRelate(LegalSystem.Properties.Settings.Default.LinkServer, ln.ACCTNO, Collateral[0].ToString());

                    foreach (sp_GetDataRelate tmp in Rs)
                    {
                        tmp.IsSelect = ln.isSelect;
                        tmp.IsEnable = ln.isEnable;
                    }
                    ((GridView)(e.Row.FindControl("gvRelationship"))).DataSource = Rs;
                    ((GridView)(e.Row.FindControl("gvRelationship"))).DataBind();

                    ((HtmlInputText)(e.Row.FindControl("txtLoanType"))).Value = ln.TYPE;
                    ((HtmlInputText)(e.Row.FindControl("txtCurBalAmt"))).Value = ln.CBAL.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtOriginalAmt"))).Value = ln.ORGAMT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtAcctInterest"))).Value = ln.ACCINT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtInterestRate"))).Value = ln.INTRATE.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtACCI3"))).Value = ln.ACCI3.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtNumOfDPD"))).Value = ln.PDDAYS.ToString("N0");
                    ((HtmlInputText)(e.Row.FindControl("txtPENIN3"))).Value = ln.PENIN3.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtPENINT"))).Value = ln.PENINT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtPOTHER"))).Value = "0.00";
                    ((HtmlInputText)(e.Row.FindControl("txtTotal"))).Value = (ln.CBAL + ln.ACCINT + ln.ACCI3 + ln.PENIN3 + ln.PENINT).ToString("N2");
                }
            }
        }

        protected void gvAccountLoan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == MasterECommandNameHelper.Select)
            { }
        }

        protected void gvAccountLoan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvRelationship_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sp_GetDataRelate r = e.Row.DataItem as sp_GetDataRelate;
                var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkRelationship")));
                if (r.IsSelect)
                    control.Attributes.Add("checked", "checked");
                if (IsSubmited || !r.IsEnable)
                    control.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled/*"disabled", "disabled"*/);
            }
        }

        #region //*** Button && Function Save & Submit && ViewClick ***//    

        private void Save()
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                User = UserLogin.Emp_Code;
                int DefendantNo = 0;
                trRequest req = GetUIRequest();

                req.Attachments = GetTypeFile();
                req.DefendantNo = DefendantNo;
                req.RequestStatus = MasterStatusHelper.StatusDraft;//Draf
                if (GlobalSession.GetPermission == GlobalSession.PermissionID.CollectionMaker)
                    req.ColChecker = ddlApprover.SelectedValue;
                else
                    req.ColHead = ddlApprover.SelectedValue;
                req.RequestReason = ddlReason.SelectedValue;
                if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;

                req.RequestType = ddlRequestType.SelectedValue;
                if (req.ColHead == "" || req.ColHead == "0") req.ColHead = GlobalSession.UserLogin.Emp_Code;

                if (string.IsNullOrEmpty(rid))
                {
                    string reqNo = unitOfwork.TrRequestRep.GetNewID();
                    string requestID = hidRequestID.Value;

                    List<trRequestRelation> RRs = GetUIRequestRelation(ref DefendantNo, requestID);
                    req.RequestID = new Guid(requestID);
                    req.RequestNo = reqNo;
                    req.Owner = User;/* GlobalSession.UserLogin.Emp_Code;*/

                    req.CreateBy = req.UpdateBy = User; /*GlobalSession.UserLogin.Emp_Code;*/
                    req.CreateDate = req.UpdateDate = DateTime.Now;
                    var ListUI = unitOfwork.TrRequestRep.GetRequestByID(requestID);
                    if (ListUI == null)
                    {
                        unitOfwork.TrRequestRep.Insert(req);

                        for (int i = 0; i < RRs.Count; i++)
                        {
                            LNMAST l = LNs.Where(x => x.ACCTNO == RRs[i].Account).FirstOrDefault();
                            if (unitOfwork.LnmastRep.Get(x => x.ACCTNO == l.ACCTNO).FirstOrDefault() == null)
                            {
                                #region /**** InsertDataLnmastRep ****/
                                FunctionOther.InsertDataLnmastRep(unitOfwork, l);
                                #endregion
                            }
                            unitOfwork.TrRequestRelateRep.Insert(RRs[i]);
                        }
                    }
                    unitOfwork.Save();
                }
                else
                {
                    List<trRequestRelation> RRs = GetUIRequestRelation(ref DefendantNo, rid);

                    trRequest tmpReq = unitOfwork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                    unitOfwork.TrRequestRep.Delete(tmpReq);
                    unitOfwork.Save();

                    tmpReq.RequestType = req.RequestType;

                    #region /**** InsertTrRequestRep ****/
                    FunctionOther.InsertTrRequestRep(unitOfwork, tmpReq, req);
                    unitOfwork.Save();
                    #endregion

                    List<trRequestRelation> tmpRels = unitOfwork.TrRequestRelateRep.Get(x => x.RequestID == new Guid(rid)).ToList();
                    UpdateTrRelation(tmpRels, RRs, unitOfwork);

                }
            }
        }
        protected void btnSaveDraf_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateField("D"))
                {
                    Save();
                    btnSubmit.Enabled = true;
                    btnSubmit.Visible = true;
                    Msg.MsgBox(MessageEvent.SaveDraft, MessageEventType.success, this, this.GetType());
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnDeleteDraf_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfwork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                    unitOfwork.TrRequestRep.Delete(req);
                    unitOfwork.Save();
                    List<trRequestRelation> tmpRels = unitOfwork.TrRequestRelateRep.Get(x => x.RequestID == new Guid(rid)).ToList();
                    for (int i = 0; i < tmpRels.Count; i++)
                    { unitOfwork.TrRequestRelateRep.Delete(tmpRels[i]); unitOfwork.Save(); }
                }
                Msg.MsgReturn(MessageEvent.DeleteDraft, Page, this, MasterPageHelper.InboxPage/*"InboxPage.aspx"*/, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                User = UserLogin.Emp_Code; //*** Add 23082019 .b
                GroupID = UserLogin.Groups[0].GroupID;

                if (ValidateField("S"))
                {
                    string Attachments = GetTypeFile();

                    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                    {
                        trRequest req = unitOfwork.TrRequestRep.Get(x => x.RequestID == new Guid(hidRequestID.Value)).FirstOrDefault();
                        if (string.IsNullOrEmpty(rid))
                            rid = hidRequestID.Value;
                        List<trRequestRelation> tmpRels = unitOfwork.TrRequestRelateRep.Get(x => x.RequestID == new Guid(rid)).ToList();
                        int DefendantNo = req.DefendantNo;
                        List<trRequestRelation> RRs = GetUIRequestRelation(ref DefendantNo, rid);
                        UpdateTrRelation(tmpRels, RRs, unitOfwork);
                        var s = req.RequestStatus.Replace("F", "").Replace("R", "").ToString();
                        int status = Convert.ToInt32(s);
                        switch (status)
                        {
                            case 1:
                                if (Attachments.Contains("1"))
                                {
                                    switch (GlobalSession.GetPermission)
                                    {
                                        case GlobalSession.PermissionID.CollectionMaker:
                                            req.ColChecker = ddlApprover.SelectedValue;
                                            break;
                                        case GlobalSession.PermissionID.CollectionChecker: //** Checker Create Case                                      
                                            req.ColChecker = GlobalSession.UserLogin.Emp_Code;
                                            req.ColCheckerApprove = DateTime.Now;
                                            req.ColHead = ddlApprover.SelectedValue;
                                            break;
                                    }
                                    req.Attachments = Attachments;
                                    req.RequestStatus = MasterStatusHelper.StatusWaitingforApproved;
                                    req.RequestStatusDate = DateTime.Now;
                                    req.Owner = User;/* GlobalSession.UserLogin.Emp_Code;*/
                                    req.OwnerApprove = DateTime.Now;
                                    req.RequestReason = ddlReason.SelectedValue;
                                    req.RequestType = ddlRequestType.SelectedValue;
                                }
                                else
                                {
                                    Msg.MsgBox("กรุณาระบุสิ่งที่แนบมาด้วย", MessageEventType.error, this, this.GetType());
                                }
                                break;
                            case 2:
                            case 3:
                                if (GroupID == MasterPermissionHelper.CollectionChecker /*441*/)         //*** Collection Checker ***//
                                {
                                    if (Attachments.Contains("1"))
                                    {
                                        req.RequestStatus = MasterStatusHelper.StatusWaitingforApproved;//Waiting for Approved
                                        req.RequestStatusDate = DateTime.Now;
                                        //req.OwnerApprove = DateTime.Now;
                                        req.ColCheckerApprove = DateTime.Now;
                                        req.ColHead = ddlApprover.SelectedValue;
                                        req.RequestReason = ddlReason.SelectedValue;
                                        req.Attachments = Attachments;
                                    }
                                    else
                                    {
                                        Msg.MsgBox("กรุณาระบุสิ่งที่แนบมาด้วย", MessageEventType.error, this, this.GetType());
                                    }

                                }
                                else if (GroupID == MasterPermissionHelper.CollectionHead /*442*/)    //*** Collection Head ***//
                                {
                                    var Approver = unitOfwork.VCollectionHeadApproveRep.GetApprove();
                                    var IDApprover = ddlApprover.SelectedValue;
                                    switch (ddlRequestType.SelectedItem.Value)
                                    {
                                        case "1":
                                            if (IDApprover == Approver[0].Emp_Code) { req.RequestStatus = MasterStatusHelper.StatusWaitingforApproved; req.ColHead = IDApprover; }
                                            else if (User == Approver[0].Emp_Code) {
                                                req.RequestStatus = MasterStatusHelper.StatusApproved; req.ColHead = User;                                                
                                                req.LegalStatus = MasterStatusHelper.LegalStatus100;
                                                req.LegalStatusDate = DateTime.Now;
                                                
                                                trLegal LG = unitOfwork.TrLegalRep.GetByID(req.LegalID);
                                                LG.LegalStatus = req.LegalStatus;
                                                LG.UpdateBy = User;
                                                LG.UpdateDateTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                req.RequestStatus = MasterStatusHelper.StatusApproved;// "F3";
                                                req.RequestStatusDate = DateTime.Now;
                                                req.ColHead = User;
                                                req.LegalStatus = MasterStatusHelper.LegalStatus100;
                                                req.LegalStatusDate = DateTime.Now;                                                
                                            }
                                            req.OwnerApprove = DateTime.Now;
                                            req.ColHeadApprove = DateTime.Now;
                                            req.Comment = txtComment.Text;
                                            if (User != Approver[0].Emp_Code)
                                            {
                                                trLegal LG = new trLegal();
                                                if (req.LegalID != null)
                                                    LG = unitOfwork.TrLegalRep.GetByID(req.LegalID);
                                                if (string.IsNullOrEmpty(LG.LegalNo))
                                                {
                                                    var ID = unitOfwork.LegalStoreProcedure.GetGuidID();
                                                    req.LegalID = ID;
                                                    trLegal trleg = new trLegal()
                                                    {
                                                        LegalID = ID,
                                                        Owner = req.Owner,
                                                        CreateBy = User,
                                                        CreateDateTime = DateTime.Now,
                                                        UpdateBy = User,
                                                        UpdateDateTime = DateTime.Now,
                                                        LegalStatus = req.LegalStatus,                                                    
                                                };
                                                    unitOfwork.TrLegalRep.Insert(trleg);
                                                }
                                            }
                                            break;
                                        case "2":
                                            if (ddlApprover.SelectedItem.Value == Approver[0].Emp_Code)
                                            {
                                                req.ColHead = ddlApprover.SelectedItem.Value;
                                            }
                                            else
                                            {
                                                req.RequestStatus = MasterStatusHelper.StatusRegistered;// "F4";
                                                req.RequestStatusDate = DateTime.Now;
                                                req.ColHead = User;
                                                req.ColHeadApprove = DateTime.Now;
                                                req.SeniorLawyer = ddlApprover.SelectedItem.Value;
                                                req.RequestReason = ddlReason.SelectedItem.Value;
                                            }
                                            break;
                                    }


                                }
                                break;
                        }
                        req.UpdateBy = User;/* GlobalSession.UserLogin.Emp_Code;*/
                        req.UpdateDate = DateTime.Now;
                        req.Comment = txtComment.Text;
                        req.RequestReason = ddlReason.SelectedItem.Value;
                        unitOfwork.Save();
                    }
                    Msg.MsgReturn(MessageEvent.Save, Page, this, MasterPageHelper.InboxPage /*"InboxPage.aspx"*/, MessageEventType.success);
                }
            }
            catch (Exception ex)
            {
                if (Session["UserLogin"] == null)
                    Msg.MsgReturn("กรุณาเข้าสู่ระบบอีกครั้ง", Page, this, MasterPageHelper.RedirectLoginPage, MessageEventType.error);
                else
                    Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlApprover.SelectedIndex == 0 && !MasterPermissionHelper.PermissionPool.Contains(GlobalSession.UserLogin.Groups[0].GroupID.ToString())) Msg.MsgBox(MessageEvent.PleaseSelectApprover /*"กรุณาเลือกผู้อนุมัติ"*/, MessageEventType.info, this, this.GetType());
                else
                {
                    string Message = string.Empty; string PageRedirec = string.Empty;
                    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                    {
                        trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                        int status = (Req.RequestStatus_ + 1);
                        Req.Comment = txtComment.Text;
                        switch (status)
                        {
                            case 2:
                                Req.Owner = GlobalSession.UserLogin.Emp_Code;
                                Req.OwnerApprove = DateTime.Now;
                                Req.ColHead = ddlApprover.SelectedValue;
                                break;
                            case 3:
                                Req.ColHead = GlobalSession.UserLogin.Emp_Code;
                                Req.ColHeadApprove = DateTime.Now;
                                break;
                            case 4:
                                string LegNo = unitOfWork.TrLegalRep.GetNewLegalID();
                                int year = DateTime.Now.Year;
                                trLegal trL = unitOfWork.TrLegalRep.GetByID(Req.LegalID);
                                trL.CreateBy = GlobalSession.UserLogin.Emp_Code; //Legal Specialist
                                trL.CreateDateTime = DateTime.Now;
                                trL.LegalNo = LegNo;
                                trL.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                                trL.UpdateDateTime = DateTime.Now;
                                trL.LegalStatus = MasterStatusHelper.LegalStatus101;


                                Req.SeniorLawyer = "0";
                                Req.LegalStatus = trL.LegalStatus;
                                Req.LegalStatusDate = DateTime.Now;
                                Req.LegalNoRegister = DateTime.Now;
                                Req.LegalSpecial = GlobalSession.UserLogin.Emp_Code;
                                Req.LegalSpecialApprove = DateTime.Now;
                                break;
                            case 5:
                                if (Req.RequestType == "7") //*** การบังคับคดี ***//
                                {
                                    Req.LawyerID = ddlApprover.SelectedValue;
                                    Req.SeniorLawyer = GlobalSession.UserLogin.Emp_Code;
                                    Req.SeniorLawyerApprove = DateTime.Now;
                                    status = 6;
                                }
                                else
                                {
                                    Req.SeniorLawyer = GlobalSession.UserLogin.Emp_Code;
                                    Req.SeniorLawyerApprove = DateTime.Now;
                                }
                                break;
                            case 6:
                                Req.LawyerID = "0";
                                break;
                            case 7:
                                Req.LawyerID = GlobalSession.UserLogin.Emp_Code;
                                Req.LawyerIDApprove = DateTime.Now;
                                Req.LawyerIDReceive = DateTime.Now;
                                break;
                        }

                        Req.RequestStatus = "F" + status;//Approve
                        Req.RequestStatusDate = DateTime.Now;
                        Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        Req.UpdateDate = DateTime.Now;
                        Message = MessageEvent.Register;
                        unitOfWork.Save();
                        Msg.MsgReturn(MessageEvent.Register, Page, this, MasterPageHelper.InboxPage, MessageEventType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                    Req.Comment = txtComment.Text;

                    Req.RequestStatusDate = DateTime.Now;
                    Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    Req.UpdateDate = DateTime.Now;
                    switch (Req.RequestStatus)
                    {
                        case MasterStatusHelper.StatusApprovedtoLawyer:
                            Req.SeniorLawyer = "0";
                            Req.SeniorLawyerReceive = null;
                            Req.SeniorLawyerApprove = null;
                            Req.LawyerAttorneyID = null;
                            Req.LawyerID = null;
                            Req.LawyerIDReceive = null;
                            Req.LawyerIDApprove = null;
                            Req.RequestStatus = MasterStatusHelper.StatusReturntoSeniorLawyer;//Return
                            break;
                        default:
                            Req.OwnerApprove = null;
                            Req.ColChecker = null;
                            Req.ColCheckerApprove = null;
                            Req.ColHead = null;
                            Req.ColHeadApprove = null;
                            Req.LegalSpecial = null;
                            Req.LegalSpecialApprove = null;
                            Req.RequestStatus = MasterStatusHelper.StatusReturn;//Return
                            break;
                    }
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Return, Page, this, MasterPageHelper.InboxPage /*"InquiryPage.aspx"*/, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                string statusReject = string.Empty; GroupID = UserLogin.Groups[0].GroupID;
                switch (GroupID)
                {
                    case MasterPermissionHelper.CollectionMaker /*440*/:
                    case MasterPermissionHelper.CollectionChecker /*441*/:
                    case MasterPermissionHelper.CollectionHead /*442*/:
                        statusReject = MasterStatusHelper.StatusRejected;
                        break;
                    default: statusReject = MasterStatusHelper.StatusRejectByLawyer; break;
                }
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(rid)).FirstOrDefault();
                    Req.Comment = txtComment.Text;
                    Req.RequestStatus = statusReject;//Reject
                    Req.RequestStatusDate = DateTime.Now;
                    Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    Req.UpdateDate = DateTime.Now;
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Reject, Page, this, MasterPageHelper.InboxPage, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        #region //*** btnRefer  ***//
        protected void btnRefer_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    switch (ddlRequestType.SelectedValue)
                    {
                        case "1":

                            if (string.IsNullOrEmpty(txtDelayProsecution.Value.Trim()))
                            {
                                Msg.MsgBox("กรุณาระบุ", MessageEventType.info, this, this.GetType());
                            }
                            else
                            {

                            }
                            break;
                        case "2":
                            break;
                        case "3":
                            if (divDelayProsecution.Visible == false)
                            {
                                Msg.MsgBox(MessageEvent.ReFerCancelProcess, MessageEventType.info, this, this.GetType());
                            }
                            else
                            {
                                //<% --กรณีการชะลอการดำเนินคดี-- %>
                                Msg.MsgReturn(MessageEvent.Succesfully, Page, this, MasterPageHelper.InquiryPage, MessageEventType.success);
                            }
                            break;

                    }
                }
                ////<% --กรณีการชะลอการดำเนินคดี-- %>
                //if (string.IsNullOrEmpty(txtDelayProsecution.Value.Trim()))
                //{
                //    Msg.MsgBox(MessageEvent.PleaseRequiredRequestType, MessageEventType.info, this, this.GetType());
                //}
                //else
                //{
                //    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                //    {
                //        titlePage.InnerText = MasterPageTitleHelper.InnerReferRequest;
                //        txtReferRequestNo.Value = txtRequestNo.Value;
                //        hidReferRequestType.Value = ddlRequestType.SelectedValue;
                //        txtReferRequestType.Value = ddlRequestType.SelectedItem.ToString();

                //        //using (RequestRep rep = new RequestRep())
                //        //    txtRequestNo.Value = rep.GetNewID();
                //        txtRequestDate.Value = DateTime.Today.AddYears(543).ToString("dd/MM/yyyy");
                //        ddlRequestStatus.SelectedValue = "0";

                //        ddlRequestType.DataSource = unitOfWork.VRequestTypeMapLegalStstusRep.GetRequestTypeByLegalStstus(hidLegalStatus.Value);
                //        ddlRequestType.DataBind();
                //        ddlRequestType.SelectedIndex = 0;
                //        ddlRequestType.Attributes.Remove(MasterAttributesHelper.disabled/*"disabled"*/);

                //        //using (vRequestTypeMapLegalStstusRep rep = new vRequestTypeMapLegalStstusRep())
                //        //{
                //        //    ddlRequestType.DataSource = rep.GetRequestTypeByLegalStstus(hidLegalStatus.Value);
                //        //    ddlRequestType.DataBind();
                //        //    ddlRequestType.SelectedIndex = 0;
                //        //    ddlRequestType.Attributes.Remove("disabled");
                //        //}
                //        GetRequestReason(ddlLitigationsCancelReason, unitOfWork);
                //        divFooterMakerChecker.Visible = btnSaveDraf.Visible = btnSubmit.Visible = true;
                //        divFooterHeader.Visible = divFooterReferCase.Visible = false;
                //        //if (ddlRequestType.SelectedItem.Text.Contains("ยกเลิก"))
                //        //{ }//divReferCaseReason.Visible = true;
                //        //else if (ddlRequestType.SelectedItem.Text.Contains("ชะลอ"))
                //        //    divReferCaseAsOfDate.Visible = true;
                //    }

                //    //กรณี Refer
                //    if (ddlRequestType.SelectedIndex == 0)
                //    {
                //        Msg.MsgBox(MessageEvent.PleaseSelectCaseRefer, MessageEventType.info, this, this.GetType());
                //    }
                //    else
                //    {
                //        string DateRefer = string.Empty;
                //        string newRequestNo; int DefendantNo = 1; string requestID; string dlRequestType;
                //        trRequest reqEditCase = GetUIRequest();

                //        //switch (ddlRequestType.SelectedValue)
                //        //{
                //        //    case "3":
                //        //        DateRefer = txtDelayProsecution.Value;
                //        //        string[] sNotic = (string.IsNullOrEmpty(txtDelayProsecution.Value) ? null : txtDelayProsecution.Value.Split('/'));
                //        //        Req.RefRequestDate = (string.IsNullOrEmpty(txtDelayProsecution.Value) ? (DateTime?)null : new DateTime(Convert.ToInt32(sNotic[2]) - 543, Convert.ToInt32(sNotic[1]), Convert.ToInt32(sNotic[0]), 0, 0, 0, 0));
                //        //        reqEditCase.RefRequestDate = Req.RefRequestDate; //ชะลอ
                //        //        break;
                //        //    case "4":
                //        //        DateRefer = txtDelayProsecutionCancel.Value;
                //        //        DateRefer = string.Format("{0}/{1}/{2}", DateRefer.Split('/')[0], DateRefer.Split('/')[1], Convert.ToInt32(DateRefer.Split('/')[2]) - 543);
                //        //        Req.RefRequestDateCancelCase = Convert.ToDateTime(DateRefer);
                //        //        reqEditCase.RefRequestDateCancelCase = DateTime.Now;//ยกเลิกชะลอ
                //        //        break;
                //        //}
                //        //dlRequestType = ddlRequestType.SelectedValue;

                //        //newRequestNo = unitOfWork.TrRequestRep.GetCaseEditID(Req.RequestNo);
                //        //insertCaseEdit.InsertCase(reqEditCase, unitOfWork, Req, newRequestNo, dlRequestType);
                //        //requestID = reqEditCase.RequestID.ToString();

                //        //List<trRequestRelation> RRs = GetUIRequestRelation(ref DefendantNo, requestID);
                //        //unitOfWork.TrRequestRep.Insert(reqEditCase);
                //        //Req.RefRequestNo = newRequestNo;
                //        //Req.RefRequestType = reqEditCase.RequestType;
                //        //Req.RefRequestReason = ddlRequestType.SelectedValue;
                //        //Req.DefendantNo = reqEditCase.DefendantNo;
                //        ////Req.RefRequestDate = Convert.ToDateTime(txtDelayProsecution.Value);
                //        //unitOfWork.Save();
                //        Msg.MsgReturn(MessageEvent.Succesfully, Page, this, MasterPageHelper.InquiryPage, MessageEventType.success);
                //    }
                //}

            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion

        #region //*** Click View จาก Inquiry Request ***//
        private void sendViewClick()
        {
            try
            {
                titlePage.InnerText = MasterPageTitleHelper.InnerInquiryRequest;
                Label33.Visible = Label1.Visible = false;
                FieldAttachment.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                ListGroupID.ClassListGroupID(LiGroupID);
                divComment.Visible = true; txtComment.Enabled = false;
                divAttachments.Disabled = true;
                Legal = new trLegal();
                if (RNO == null)
                    RNO = new trRequest();
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    RNO = unitOfwork.TrRequestRep.GetRequestDataByRequestNo(RequestNo);
                    rid = RNO.RequestID.ToString();

                    List<vRequestTypeMapLegalStstus> ddlRType = new List<vRequestTypeMapLegalStstus>();
                    UI_RequestPage.Set_DropDownList(unitOfwork, ddlRequestStatus, ddlLegalStatus, ddlRequestType, ddlApprover, rid, User, ddlRType, MasterECommandNameHelper.View);
                    UI_RequestPage.Set_ddlAssignLawyer(unitOfwork, ddlAttorney, ddlLawyer, RNO.RequestType, User);
                    GetRequestReason(ddlReason, unitOfwork);

                    Legal = unitOfwork.TrLegalRep.Get(g => g.LegalID == RNO.LegalID).FirstOrDefault();
                    vCIF = RNO.CifNo.ToString();
                    if (Legal == null) txtLegalNo.Value = ""; else txtLegalNo.Value = Legal.LegalNo;

                    txtRequestNo.Value = RNO.RequestNo;
                    txtRequestDate.Value = RNO.CreateDate.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    hidLegalStatus.Value = RNO.LegalStatus;
                    hidRequestID.Value = RNO.RequestID.ToString();
                    hidRefRequestNo.Value = RNO.RefRequestNo;
                    ddlRequestStatus.SelectedValue = RNO.RequestStatus;
                    ddlRequestType.SelectedValue = RNO.RequestType;
                    txtReferRequestNo.Value = RNO.RefRequestNo;
                    if (!string.IsNullOrEmpty(RNO.RefRequestType))
                        txtReferRequestType.Value = ddlRType[Convert.ToInt32(RNO.RefRequestType)].ReqTypeName.ToString();
                    txtCIF.Value = vCIF;
                    txtComment.Text = RNO.Comment;
                    ddlLegalStatus.SelectedValue = RNO.LegalStatus;

                    ddlReason.SelectedValue = RNO.RequestReason; //*** ddlเหตุผล ***//
                                                                 //ddlApprover.SelectedValue = RNO.ColHead;     //*** ddlApprove ***//             
                    ddlApprover.SelectedValue = (!string.IsNullOrEmpty(RNO.ColHead) ? RNO.ColHead : RNO.ColChecker);     //*** ddlApprove ***//             
                    btnFindCIF_Click(MasterECommandNameHelper.View, null);

                    divFooterHeader.Visible = true;
                    btnBack2.Visible = true;
                    divFooterMakerChecker.Visible = btnHeadReject.Visible = btnSubmit.Visible = btnSaveDraf.Visible = btnApproveReturn.Visible = btnFindCIF.Visible = btnApprove.Visible = btnReturn.Visible = false;
                    txtCIF.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                    btnDeleteDraf.Visible = btnReject.Visible = false;
                    ddlApprover.Enabled = ddlReason.Enabled = false;
                    ddlRequestType.Enabled = false;
                    SetDisableGridRelation();
                    
                    /*** SET Attachments ****/
                    UI_RequestPage.SetAttachments(RNO, divAttachments, true, GroupID);
                    int LEStatus = string.IsNullOrEmpty(RNO.LegalStatus) ? 0 : Convert.ToInt32(RNO.LegalStatus);
                    if (LEStatus >= 100 && LEStatus < 200)
                    {
                        btnEdit.Visible = true;
                        if(RNO.CaseDate == null) { txtDateCase.Value = ""; } else txtDateCase.Value = RNO.CaseDate.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    }
                    btnEdit.Visible = false;
                    if (RNO.RefRequestDate != null)
                    {
                        txtDelayProsecution.Value = RNO.RefRequestDate.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                        txtDelayProsecution.Disabled = true;
                        FieldCaseRefer.Visible = true;
                        divDelayProsecution.Visible = true;
                    }
                    if (RNO.RefRequestDateCancelCase != null)
                    {
                        txtDelayProsecutionCancel.Value = RNO.RefRequestDateCancelCase.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                        txtDelayProsecutionCancel.Disabled = true;
                        FieldCaseRefer.Visible = true;
                        divDelayProsecutionCancel.Visible = true;
                    }
                    if (RNO.RequestStatus == MasterStatusHelper.StatusAssigned || RNO.RequestStatus == MasterStatusHelper.StatusApprovedtoLawyer)
                    {
                        FSApprover1.Visible = FSApprover2.Visible = false;
                        ddlAttorney.Enabled = ddlLawyer.Enabled = false;
                        FSAssignOA_IA1.Visible = FSAssignOA_IA2.Visible = FSAssignLawyer1.Visible = FSAssignLawyer2.Visible = true;
                        ddlAttorney.SelectedValue = RNO.LawyerAttorneyID; ddlLawyer.SelectedValue = RNO.LawyerID;
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion

        #region //*** btnEditCase ชะลอ, ยกเลิก,  ***//
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            ddlRequestType.Attributes.Remove(MasterAttributesHelper.enable);
            ddlRequestType.Enabled = true;
            divFooterHeader.Visible = false;
            //btnApprove.Text = MasterButtonHelper.Submitted; //"Submitted";
            divFooterReferCase.Visible = true;
            txtComment.Enabled = true;
            SprintddlRequestType();

            MasterReasonHelper.BindCICReason<string, string>(ddlCICReason);
            MasterReasonHelper.BindCICReason<string, string>(ddlCiC_CancelReason);
        }

        private void CaseReferHideOrShow()
        {
            if (ddlRequestType.SelectedValue == "0" && txtDelayProsecution.Value == null)
                FieldCaseRefer.Visible = false;
            else if (ddlRequestType.SelectedValue == "3")
            {
                using (LegalUnitOfWork rep = new LegalUnitOfWork())
                {
                    trRequest Req = new trRequest();
                    DateTime DateNow = DateTime.Now.AddDays(-7);
                    Req = rep.TrRequestRep.GetRequestByID(hidRequestID.Value);
                    DateTime CaseDate = Convert.ToDateTime(Req.CaseDate);
                    if (CaseDate > DateNow)
                    {
                        Msg.MsgBox(MessageEvent.ReFerCancelProcess, MessageEventType.warning, this, this.GetType());
                    }
                    else
                    {
                        FieldCaseRefer.Visible = true;
                        divDelayProsecution.Visible = true;

                        divDelayProsecutionCancel.Visible = (string.IsNullOrEmpty(txtDelayProsecutionCancel.Value) ? false : true);
                    }
                }
            }
            else if (ddlRequestType.SelectedValue == "4")
            {
                FieldCaseRefer.Visible = true;
                divDelayProsecutionCancel.Visible = true;

                divDelayProsecution.Visible = (string.IsNullOrEmpty(txtDelayProsecution.Value) ? false : true);
            }
            else if (ddlRequestType.SelectedItem.Text.Replace("การ", "").Contains(MasterReasonHelper.ReasonLitigationsCancel))
            {
                FieldCaseRefer.Visible = true;
                divLitigationsCancel.Visible = true;
            }
        }
        #endregion
        #region //*** btnReturnToPool ***//
        protected void btnReturnToPool_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfWork.TrRequestRep.GetByID(new Guid(rid));
                    req.RequestStatus = MasterStatusHelper.StatusRegistered;
                    req.RequestStatusDate = DateTime.Now;
                    req.LawyerID = null;
                    req.SeniorLawyer = "0";
                    req.SeniorLawyerApprove = req.SeniorLawyerReceive = null;
                    req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    req.UpdateDate = DateTime.Now;
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.ReturnToPool, this, this.GetType(), MasterPageHelper.InboxPage, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion
        #region //*** btnSave ตอน Status มาอยู่ที่หน้า Assign ***//
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string PageRe = string.Empty;
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfWork.TrRequestRep.GetRequestByID(hidRequestID.Value);
                    //vAssignLawyer AsLawyer = unitOfWork.VAssignLawyerRep.GetAssignByReqNo(req.RequestNo);
                    if (req.RequestStatus == MasterStatusHelper.StatusApprovedtoLawyer)
                    {
                        if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                        req.LawyerIDReceive = DateTime.Now;
                        req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        req.UpdateDate = DateTime.Now;
                        PageRe = MasterPageHelper.InboxPage;
                    }
                    else
                    {
                        req.LawyerAttorneyID = ddlAttorney.SelectedValue;//EmpCode
                        req.LawyerID = ddlLawyer.SelectedValue;
                        if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                        req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        req.UpdateDate = DateTime.Now;
                        PageRe = MasterPageHelper.InboxPage;
                    }
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Save, this, this.GetType(), PageRe, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion
        private void ViewReAssign()
        {
            try
            {
                FSucHistoryReassign.Visible = true;
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    RNO = unitOfWork.TrRequestRep.GetByID(new Guid(IDReassign));
                    trLegal LEG = unitOfWork.TrLegalRep.GetByID(RNO.LegalID);
                    var listAdd = unitOfWork.LegalStoreProcedure.GetRequest(RNO.RequestType, RNO.RequestNo, "", "", RNO.RequestStatus, RNO.LegalStatus).FirstOrDefault();
                    List<vRequestTypeMapLegalStstus> ddlRType = new List<vRequestTypeMapLegalStstus>();
                    UI_RequestPage.Set_DropDownList(unitOfWork, ddlRequestStatus, ddlLegalStatus, ddlRequestType, ddlApprover, rid, User, ddlRType, MasterECommandNameHelper.View);
                    UI_RequestPage.Set_ddlAssignLawyer(unitOfWork, ddlAttorney, ddlLawyer, RNO.RequestType, User);
                    GetRequestReason(ddlReason, unitOfWork);
                    txtRequestNo.Value = RNO.RequestNo;
                    txtRequestDate.Value = RNO.RequestDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
                    ddlRequestStatus.SelectedValue = RNO.RequestStatus;
                    ddlRequestType.SelectedValue = RNO.RequestType;
                    ddlRequestType.SelectedValue = RNO.RequestType;
                    ddlReason.SelectedValue = RNO.RequestReason;
                    txtReferRequestNo.Value = RNO.RefRequestNo;
                    txtCIF.Value = RNO.CifNo.ToString();
                    txtLegalNo.Value = LEG.LegalNo;
                    ddlLawyer.SelectedValue = RNO.LawyerID;
                    ddlAttorney.SelectedValue = RNO.LawyerAttorneyID;
                    txtComment.Text = RNO.Comment;
                    txtAddress.Value = listAdd.Address;
                }
                titlePage.InnerText = "Reassign Lawyer and OA/IA";
                Label33.Visible = false;

                FieldAttachment.Visible = FSLoanInfo.Visible = ddlReason.Enabled = false;
                FSApprover2.Visible = FSApprover1.Visible = false;
                FieldCaseRefer.Visible = divFooterMakerChecker.Visible = false;
                FSAssignOA_IA1.Visible = FSAssignOA_IA2.Visible = FSAssignLawyer1.Visible = FSAssignLawyer2.Visible = true;
                FieldView.Disabled = divFooterHeader.Visible = true;
                btnCFAssign.Visible = btnBackAssign.Visible = true;
                btnLSApprove.Visible = btnReject.Visible = btnReturnToPool.Visible = btnFindCIF.Visible = btnBack2.Visible = btnApprove.Visible = btnReturn.Visible = false;
                ucReassignHistory.RequestNo = RNO.RequestNo;
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion

        private void SetDisableGridRelation()
        {
            foreach (GridViewRow rowA in gvAccountLoan.Rows)
            {
                var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                chkAL.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                var gvR = (GridView)rowA.FindControl("gvRelationship");
                foreach (GridViewRow rowR in gvR.Rows)
                {
                    var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                    chkR.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                }
            }
        }

        private void SetEnableGridRelation()
        {
            foreach (GridViewRow rowA in gvAccountLoan.Rows)
            {
                var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                chkAL.Attributes.Remove(MasterAttributesHelper.disabled);
                var gvR = (GridView)rowA.FindControl("gvRelationship");
                foreach (GridViewRow rowR in gvR.Rows)
                {
                    var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                    chkR.Attributes.Remove(MasterAttributesHelper.disabled);
                }
            }
        }

        private void SprintddlRequestType()
        {
            try
            {
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    string LegalStatus = string.Empty;

                    ddlRequestType.DataSource = unitOfwork.VRequestTypeMapLegalStstusRep.GetRequestTypeRefer((!string.IsNullOrEmpty(rid)) ? "-1" : ddlLegalStatus.SelectedValue);
                    ddlRequestType.DataBind();

                }
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        protected void btnCFAssign_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlAttorney.SelectedItem.Value == "0" || ddlLawyer.SelectedItem.Value == "0")
                {
                    Msg.MsgBox(MessageEvent.RequiredField, MessageEventType.info, this, this.GetType());
                }
                else
                {
                    using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                    {
                        if (IDReassign == null)
                        {
                            trRequest req = unitOfWork.TrRequestRep.GetRequestByID(hidRequestID.Value);
                            switch (req.RequestType)
                            {
                                case "1":
                                    req.LawyerAttorneyID = ddlAttorney.SelectedValue;//EmpCode
                                    req.LawyerID = ddlLawyer.SelectedValue;
                                    if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                                    req.RequestStatus = MasterStatusHelper.StatusApprovedtoLawyer;
                                    req.RequestStatusDate = DateTime.Now;
                                    req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                                    req.UpdateDate = DateTime.Now;
                                    req.SeniorLawyerApprove = DateTime.Now;
                                    unitOfWork.Save();
                                    break;
                                case "2":
                                    var ID = unitOfWork.LegalStoreProcedure.GetGuidID();
                                    string LegNo = unitOfWork.TrLegalRep.GetNewNoticeLegalNo();

                                    trLegal trleg = new trLegal()
                                    {
                                        LegalID = ID,
                                        LegalNo = LegNo,
                                        LegalStatus = MasterStatusHelper.LegalStatus101,
                                        Owner = req.Owner,
                                        CreateBy = GlobalSession.UserLogin.Emp_Code,
                                        CreateDateTime = DateTime.Now,
                                        UpdateBy = User,
                                        UpdateDateTime = DateTime.Now,
                                    };
                                    unitOfWork.TrLegalRep.Insert(trleg);

                                    req.SeniorLawyerApprove = DateTime.Now;
                                    req.LegalID = ID;
                                    req.LegalStatus = MasterStatusHelper.LegalStatus101;
                                    req.LegalStatusDate = DateTime.Now;
                                    req.LegalNoRegister = DateTime.Now;
                                    req.LawyerAttorneyID = ddlAttorney.SelectedValue;
                                    if (ddlLawyer.SelectedItem.Value == GlobalSession.UserLogin.Emp_Code)
                                    {
                                        req.RequestStatus = MasterStatusHelper.StatusAccepted;
                                    }
                                    else
                                    {
                                        req.RequestStatus = MasterStatusHelper.StatusApprovedtoLawyer;
                                    }
                                    req.LawyerID = ddlLawyer.SelectedValue;
                                    if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;

                                    req.RequestStatusDate = DateTime.Now;
                                    req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                                    req.UpdateDate = DateTime.Now;
                                    unitOfWork.Save();

                                    break;
                            }
                            Msg.MsgReturn(MessageEvent.Assign, this, this.GetType(), MasterPageHelper.InboxPage, MessageEventType.success);
                        }
                        else if (IDReassign != null)
                        {

                            RNO = unitOfWork.TrRequestRep.GetByID(new Guid(IDReassign));
                            if (ddlAttorney.SelectedValue == RNO.LawyerAttorneyID)
                                Msg.MsgBox("กรุณาระบุ OA/IA ใหม่ที่ไม่ซ้ำเดิม", MessageEventType.info, this, this.GetType());
                            else
                            {
                                if (ddlLawyer.SelectedValue == RNO.LawyerID)
                                    Msg.MsgBox("กรุณาระบุ Lawyer ใหม่ที่ไม่ซ้ำเดิม", MessageEventType.info, this, this.GetType());
                                else
                                {
                                    using (trReAssignHisRep rep = new trReAssignHisRep())
                                    {
                                        var LisReassign = rep.GetbyCondition(RNO.RequestNo);
                                        var LEG = unitOfWork.TrLegalRep.GetByID(RNO.LegalID);

                                        if (LisReassign.Count < 1)
                                        {
                                            var List = unitOfWork.VEmpLegalRep.GetAllUsers();
                                            var ListAtt = unitOfWork.MSAttorneyRep.GetAllLawyer();
                                            var AssignBy = List.Where(W => W.Emp_Code == RNO.SeniorLawyer).FirstOrDefault();
                                            var Attorney = ListAtt.Where(W => W.AttorneyID == Convert.ToInt32(RNO.LawyerAttorneyID)).FirstOrDefault();
                                            var Lawyer = List.Where(W => W.Emp_Code == RNO.LawyerID).FirstOrDefault();
                                            FunctionOther.InsertMasterReassign(RNO, LEG, Attorney.LawyerName, Lawyer.TH_Name, AssignBy.TH_Name);
                                        }
                                        string LawyerAttorneyID = ddlAttorney.SelectedItem.Text.Trim().ToString();
                                        string LawyerID = ddlLawyer.SelectedItem.Text.Trim().ToString();
                                        FunctionOther.InsertHistoryReassign(RNO, LEG, LawyerAttorneyID, LawyerID);

                                        RNO.LawyerAttorneyID = ddlAttorney.SelectedItem.Value.Trim().ToString();
                                        RNO.LawyerID = ddlLawyer.SelectedItem.Value.Trim().ToString();
                                        RNO.SeniorLawyer = GlobalSession.UserLogin.Emp_Code;
                                        RNO.SeniorLawyerApprove = DateTime.Now;
                                        unitOfWork.Save();
                                    }
                                    Msg.MsgReturn(MessageEvent.Assign, this, this.GetType(), MasterPageHelper.AssignPage, MessageEventType.success);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfWork.TrRequestRep.GetRequestByID(hidRequestID.Value);
                    req.LawyerIDReceive = DateTime.Now;
                    req.LawyerIDApprove = DateTime.Now;
                    req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    req.UpdateDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                    req.RequestStatus = MasterStatusHelper.StatusAccepted;
                    req.RequestStatusDate = DateTime.Now;
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Accept, this, this.GetType(), MasterPageHelper.InboxPage, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnReciept_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest Req = unitOfWork.TrRequestRep.Get(x => x.RequestID == new Guid(hidRequestID.Value)).FirstOrDefault();
                    int status = (Req.RequestStatus_ + 1);
                    Req.LegalStatusDate = DateTime.Now;
                    Req.LegalSpecial = GlobalSession.UserLogin.Emp_Code;
                    Req.SeniorLawyer = "0";
                    Req.LegalSpecialApprove = DateTime.Now;
                    Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    Req.UpdateDate = DateTime.Now;
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Reciept, Page, this, MasterPageHelper.InboxPage, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        private void UpdateTrRelation(List<trRequestRelation> tmpRels, List<trRequestRelation> RRs, LegalUnitOfWork unitOfwork)
        {
            try
            {
                for (int i = 0; i < tmpRels.Count; i++)
                {
                    unitOfwork.TrRequestRelateRep.Delete(tmpRels[i]);
                }

                for (int i = 0; i < RRs.Count; i++)
                {
                    unitOfwork.TrRequestRelateRep.Insert(RRs[i]);
                }
                unitOfwork.Save();

                for (int i = 0; i < RRs.Count; i++)
                {
                    LNMAST l = LNs.Where(x => x.ACCTNO == RRs[i].Account).FirstOrDefault();
                    if (unitOfwork.LnmastRep.Get(x => x.ACCTNO == l.ACCTNO).FirstOrDefault() == null)
                    {
                        unitOfwork.LnmastRep.Insert(new LNMAST
                        {
                            #region
                            ACCI3 = l.ACCI3,
                            ACCTNO = l.ACCTNO,
                            ACCINT = l.ACCINT,
                            ACNAME = l.ACNAME,
                            ACTYPE = l.ACTYPE,
                            BRN = l.BRN,
                            CBAL = l.CBAL,
                            CFADDR = l.CFADDR,
                            CFNAME = l.CFNAME,
                            CIFNO = l.CIFNO,
                            CollateralDesc = l.CollateralDesc,
                            FCODE = l.FCODE,
                            FSEQ = l.FSEQ,
                            INTRATE = l.INTRATE,
                            LID = unitOfwork.LegalStoreProcedure.GetGuidID(),
                            LMCOST = l.LMCOST,
                            LMPRDC = l.LMPRDC,
                            ORGAMT = l.ORGAMT,
                            ORGDT = l.ORGDT,
                            PDDAYS = l.PDDAYS,
                            PENIN3 = l.PENIN3,
                            PENINT = l.PENINT,
                            STATUS = l.STATUS,
                            TYPE = l.TYPE,
                            #endregion
                        });
                    }
                }
                unitOfwork.Save();
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }

        #region Approve or GetTask to Inbox
        protected void btnLSApprove_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest Req = unitOfWork.TrRequestRep.GetRequestByID(hidRequestID.Value);
                    if (btnLSApprove.Text.Contains(MasterButtonHelper.Approve))
                    {
                        int status = (Req.RequestStatus_ + 1);
                        Req.LegalSpecial = GlobalSession.UserLogin.Emp_Code;
                        Req.LegalStatusDate = DateTime.Now;
                        Req.SeniorLawyer = "0";
                        Req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        Req.UpdateDate = DateTime.Now;
                        unitOfWork.Save();

                        Msg.MsgReturn(MessageEvent.Approve, Page, this, MasterPageHelper.InboxPoolPage, MessageEventType.success);
                    }
                    else
                    {
                        Req.RequestStatus = MasterStatusHelper.StatusRegistered;
                        Req.RequestStatusDate = DateTime.Now;
                        Req.SeniorLawyer = GlobalSession.UserLogin.Emp_Code;
                        Req.SeniorLawyerReceive = DateTime.Now;
                        unitOfWork.Save();

                        Msg.MsgReturn(MessageEvent.GetTask, Page, this, MasterPageHelper.InboxPoolPage, MessageEventType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString().Replace("'", " "), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion
    }
}