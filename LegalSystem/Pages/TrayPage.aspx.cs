﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class TrayPage : System.Web.UI.Page
    {
        string menuval = "";
        string cif = "";
        string legalStatus = "";
        string redirect = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            legalStatus = (ddlLegalStatus.SelectedValue == "0") ? "" : ddlLegalStatus.SelectedValue;
            cif = txtCIF.Value;
            menuval = Request.QueryString["menutray"];
            redirect = Request.QueryString["redirect"];
            if (!IsPostBack) 
            {
               
                SetUI(); 
                if(redirect=="Y")
                {
                    legalStatus = Request.QueryString["legal"];
                    cif = Request.QueryString["cif"];
                    btnSearch_Click(sender, e);
                }
            }
        }

        public void SetUI()
        {
            //string menuval = Request["menuval"];
            //string menuval = "1";
            switch (menuval)
            {
                case "1":
                    trayname.InnerHtml = "TRAY 1 : รับเรื่องดำเนินคดี";
                    break;
                case "2":
                    trayname.InnerHtml = "TRAY 2 : ออกหนังสือทวงถาม";
                    break;
                case "3":
                    trayname.InnerHtml = "TRAY 3 : อยู่ระหว่างพิจารณาคดี";
                    break;
                case "4":
                    trayname.InnerHtml = "TRAY 4 : อยู่ระหว่างบังคับคดี";
                    break;
                case "5":
                    trayname.InnerHtml = "TRAY 5 : สืบทรัพย์/ยึดทรัพย์";
                    break;
            }
            using(LegalUnitOfWork unitOfwork=new LegalUnitOfWork())
            {
               ddlPrintDoc.DataSource = unitOfwork.MSDocumentTypeRep.Get().ToList();
               ddlPrintDoc.DataBind();
            }

            
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                List<sp_GetRequest> DataList = null;
               
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    DataList = unitOfwork.LegalStoreProcedure.GetRequest("", "", "", cif, "F6", legalStatus).ToList();
                }
                gvDatas.DataSource = DataList;
                gvDatas.DataBind();
                UcMessageBoxMain.VisibleControl();

                if (DataList.Count > 0) bResult.Attributes.Add("style", "display:block;");
                else bResult.Attributes.Add("style", "display:none;");
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void gvDatas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                string legalID = (row.FindControl("hidLegalID") as HiddenField).Value;
                string id = "";
                int docTypeID = int.Parse(ddlPrintDoc.SelectedValue);
                wfDocument wfDoc = unitOfwork.WFDocumentRep.Get(x => x.LegalID == new Guid(legalID) && x.DocTypeID==docTypeID && x.IsCurrent == "Y").FirstOrDefault();

                if (wfDoc != null)
                    id = wfDoc.wfDocID.ToString();
                else
                    id = unitOfwork.LegalStoreProcedure.GetGuidID().ToString();


                if (e.CommandName == "Request")
                {
                    Response.Redirect("FirstLetterPage.aspx?mode=new&cif="+cif+"&legalid="+legalID+"&legal="+legalStatus+"&id=" + id);
                }
                else if (e.CommandName == "Verify")
                {
                    Response.Redirect("FirstLetterPage.aspx?mode=verify&cif=" + cif + "&legalid=" + legalID + "&legal=" + legalStatus + "&id=" + id);
                }
            }
        }

        protected void gvDatas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ((ImageButton)e.Row.FindControl("imgBtnRequest")).Visible = false;
                ((ImageButton)e.Row.FindControl("imgBtnVer")).Visible = false;
                ((ImageButton)e.Row.FindControl("imgPrint")).Visible = false;
                if (((HiddenField)e.Row.FindControl("hidDocStatus")).Value == "1" && GlobalSession.PermissionID.Lawyer==GlobalSession.GetPermission)
                {
                    ((ImageButton)e.Row.FindControl("imgBtnRequest")).Visible = true;
                    ((ImageButton)e.Row.FindControl("imgBtnVer")).Visible = true;
                }
                else if (((HiddenField)e.Row.FindControl("hidDocStatus")).Value == "1" && GlobalSession.PermissionID.SeniorLawyer == GlobalSession.GetPermission)
                {
                    ((ImageButton)e.Row.FindControl("imgPrint")).Visible = true;
                }
               
                //try
                //{
                //    if (reqMode == "OSOper")
                //    {
                //        e.Row.Cells[11].Text = "";
                //    }
                //    else if (reqMode == "DocOper")
                //    {
                //        e.Row.Cells[11].Text = 
                //    }

                //}
                //catch (Exception ex)
                //{
                //    UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
                //}
            }
        }
    }
}