﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class CoverPage : System.Web.UI.Page
    {
        public List<vCoverpage> CPs
        {
            get { return Session["Coverpages"] != null ? Session["Coverpages"] as List<vCoverpage> : null; }
            set { Session["Coverpages"] = value; }
        }

        private List<DateAsCountClass> Ds
        {
            get { return Session["DateAsCount"] != null ? Session["DateAsCount"] as List<DateAsCountClass> : null; }
            set { Session["DateAsCount"] = value; }
        }

        private string DocTypeID
        {
            get { return Session["DocTypeID"] != null ? Session["DocTypeID"] as string : null; }
            set { Session["DocTypeID"] = value; }
        }

        class DateAsCountClass
        {
            public int id { get; set; }
            public DateTime DateAsC { get; set; }
        }

        #region Function
        private void GetCoverpageGroup()
        {
            using (CoverPageGroupRep rep = new CoverPageGroupRep())
            {
                List<CoverPageGroup> Gs = rep.GetAllGroup();
                ddlGroup.DataSource = Gs;
                ddlGroup.DataBind();
                ddlGroup.SelectedIndex = 0;
                GetCoverpage(ddlGroup.SelectedValue);
            }
        }

        private void GetCoverpage(string group)
        {
            using (vCoverpageRep rep = new vCoverpageRep())
            {
                CPs = rep.GetCoverpage(Convert.ToInt32(group));
                gvDocument.DataSource = CPs;
                gvDocument.DataBind();
            }
            //List<sp_GetRequest> RQs = Session["ListRequest"] as List<sp_GetRequest>;
            //using (wfGroupRep rep = new wfGroupRep())
            //{
            //    ddlApprover.DataSource = rep.GetGroupByRole(12);
            //    ddlApprover.DataBind();
            //    ddlApprover.SelectedIndex = 0;///อันนี้Test////// ต้องเซ็ตตาม Senior Lawyer ที่ Get Task ในหน้า Pool Inbox ////เพราะต้องลงลายเซ็นของSenior Lawyerนั้นๆ/////
            //}
        }

        private void ClareModal()
        {
            txtName.Text = lblName.Text = hidName.Value = txtFreeText.Text = string.Empty;
            ddlOriginalCopy.SelectedIndex = ddlTypeDeed.SelectedIndex = 0;
            txtNumOrder.Value = txtNumRound.Value = txtNumEdition.Value = txtNumDeed.Value = txtNumPlot.Value = txtNumber.Value = txtDeedNo.Value = "1";
            txtNumSuite.Value = "2";
            txtDateAs.Value = txtDateAsCount.Value = DateTime.Today.ToString("dd/MM/yyyy");
            hidLastSeq.Value = "0";
            Ds = new List<DateAsCountClass>();
            gvDateAsCountDataBind();
        }

        private void gvDateAsCountDataBind()
        {
            Ds = Ds.OrderBy(D => D.DateAsC).ToList();
            gvDateAsCount.DataSource = Ds;
            gvDateAsCount.DataBind();
        }

        private void GetUI()
        {
            string DateAsCountStr = string.Empty;
            if (divDateAsCount.Visible)
            {
                if (Ds.Count > 0)
                {
                    foreach (DateAsCountClass d in Ds)
                        DateAsCountStr += d.DateAsC.ToString("dd MMMM yyyy", GlobalSession.CultureTH) + ", ";
                    DateAsCountStr = DateAsCountStr.Substring(0, DateAsCountStr.Length - 2);
                }
            }
            txtName.Text = string.Format(hidName.Value, (divOriginalCopy.Visible ? ddlOriginalCopy.SelectedItem.Text : "")
                                                      , (divNumOrder.Visible ? txtNumOrder.Value : "")
                                                      , (divNumRound.Visible ? txtNumRound.Value : "")
                                                      , (divNumEdition.Visible ? txtNumEdition.Value : "")
                                                      , (divNumDeed.Visible ? txtNumDeed.Value : "")
                                                      , (divNumPlot.Visible ? txtNumPlot.Value : "")
                                                      , (divNumSuite.Visible ? txtNumSuite.Value : "")
                                                      , (divNumber.Visible ? txtNumber.Value : "")
                                                      , (divDeedNo.Visible ? txtDeedNo.Value : "")
                                                      , (divFreeText.Visible ? txtFreeText.Text : "")
                                                      , (divDateAs.Visible ? Convert.ToDateTime(txtDateAs.Value).ToString("dd MMMM yyyy", GlobalSession.CultureTH) : "")
                                                      , (divDateAsCount.Visible ? DateAsCountStr : ""));
            if (divTypeDeed.Visible)
                txtName.Text = txtName.Text.Replace("เป็นประกัน", string.Format(" {0} เป็นประกัน", ddlTypeDeed.SelectedItem.Text));
            UpdatePanel2.Update();
        }

        [WebMethod]
        public static void gvDocumentUpdate(String DocTypeID)
        {
            gvDocumentUpdated(DocTypeID);
        }

        public static void gvDocumentUpdated(String DocTypeID)
        {
            Page objp = new Page();
            List<vCoverpage> tmps = objp.Session["Coverpages"] as List<vCoverpage>;
            vCoverpage r = tmps.Where(D => D.DocTypeID == Convert.ToInt32(DocTypeID)).FirstOrDefault();
            r.IsSelect = !r.IsSelect;
            objp.Session["Coverpages"] = tmps;
        }

        private void GeneratePDF(bool isSubmit)
        {
            LegalUnitOfWork unitOfwork = new LegalUnitOfWork();
            List<sp_GetRequest> RQs = Session["ListRequest"] as List<sp_GetRequest>;
            string rno = (Request.QueryString["rno"] != null) ? Request.QueryString["rno"].ToString() : string.Empty;
            sp_GetRequest RQ = RQs.Where(D => D.RequestNo == rno).FirstOrDefault();
            wfGroup Group = new wfGroup();

            Group = unitOfwork.WFGroupRep.Get(D => D.EmpCode == RQ.SeniorLawyer).FirstOrDefault();
    
            //using (wfGroupRep rep = new wfGroupRep())
            //    Group = rep.SelectDataWithCodition(D => D.EmpCode == RQ.ApproverLaw).FirstOrDefault();
            bool status = Convert.ToBoolean(Request.QueryString["status"]);
            DateTime printDate = DateTime.Today.AddYears(543);
            DateTime LitigateDate = printDate.AddDays(status ? 60 : 90);//รวมวันหยุด

            List<ReportParameter> rptParamList = new List<ReportParameter>();
            rptParamList.Add(new ReportParameter("LegalNo", RQ.LegalNo));
            rptParamList.Add(new ReportParameter("PrintDate", printDate.ToString("dd MMMM yyyy", GlobalSession.CultureTH)));
            rptParamList.Add(new ReportParameter("AttorneyName", "AAAAAAAAAAAAAA"));
            rptParamList.Add(new ReportParameter("ProductName", "BBBBBBBBBB"));//////Test
            rptParamList.Add(new ReportParameter("ProductCode", "CC"));//////Test
            rptParamList.Add(new ReportParameter("CustomerName", RQ.CFNAME));
            rptParamList.Add(new ReportParameter("NoticeDateFrom", printDate.AddDays(1).ToString("dd MMMM yyyy", GlobalSession.CultureTH)));
            rptParamList.Add(new ReportParameter("NoticeDateTo", printDate.AddDays(7).ToString("dd MMMM yyyy", GlobalSession.CultureTH)));
            rptParamList.Add(new ReportParameter("LitigateDate", LitigateDate.ToString("dd MMMM yyyy", GlobalSession.CultureTH)));
            rptParamList.Add(new ReportParameter("NotificationDate", LitigateDate.AddDays(20).ToString("dd MMMM yyyy", GlobalSession.CultureTH)));//รวมวันหยุด
            rptParamList.Add(new ReportParameter("LegalApprover", Group.EmpName));
            rptParamList.Add(new ReportParameter("OwnerName", GlobalSession.UserLogin.TH_Name));

            List<vCoverpage> selectedCP = CPs.Where(D => D.IsSelect).OrderBy(D => D.DocTypeID).ToList();
            ReportViewer RptVw = new ReportViewer();
            RptVw.ProcessingMode = ProcessingMode.Local;
            RptVw.LocalReport.ReportPath = Server.MapPath("~/Reports/rpCoverpage.rdlc");
            RptVw.LocalReport.DataSources.Clear();
            RptVw.LocalReport.DataSources.Add(new ReportDataSource("dsCoverpageReport", selectedCP));
            RptVw.LocalReport.SetParameters(rptParamList);

            if (!isSubmit)
            {
                string ReportFileName = GlobalSession.PathFilePDF = Server.MapPath("~/Documents/CP_tmp_" + Guid.NewGuid().ToString() + ".pdf");
                PDFViewer V = new PDFViewer();
                V.WritePDF(ReportFileName, RptVw);
                iPDF.Attributes.Add("src", GlobalSession.PathFilePDF);
                UpdatePanel3.Update();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PreviewModal", "$('#PreviewModal').modal('show');", true);
                //Response.Redirect("PreviewPDFPage.aspx");
                //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Popup", @"OpenDialog('PreviewPDFPage.aspx','500px','800px');", true);
            }
            else
            {
                string ReportFileName = Server.MapPath("~/Documents/CP_" + rno + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
                PDFViewer V = new PDFViewer();
                V.WritePDF(ReportFileName, RptVw);

                /**Insert database**/


                Response.Redirect("InboxPage.aspx");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetCoverpageGroup();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCoverpage(ddlGroup.SelectedValue);
        }

        protected void gvDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                vCoverpage r = e.Row.DataItem as vCoverpage;
                var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkDocument")));
                if (r.IsSelect)
                    control.Attributes.Add("checked", "checked");
                control.Attributes.Add("onclick", "javascript:checkChange('" + r.DocTypeID + "');");
                control.Style.Add("cursor", "pointer");
            }
        }

        protected void gvDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ClareModal();
                DocTypeID = e.CommandArgument.ToString();
                vCoverpage r = CPs.Where(D => D.DocTypeID == Convert.ToDecimal(e.CommandArgument.ToString())).FirstOrDefault();
                lblName.Text = r.CoverpageText;
                txtName.Text = r.DocTypeName;
                divOriginalCopy.Visible = (r.OriginalCopy);
                divNumOrder.Visible = (r.NumOrder);
                divNumRound.Visible = (r.NumRound);
                divNumEdition.Visible = (r.NumEdition);
                divNumDeed.Visible = (r.NumDeed);
                divNumPlot.Visible = (r.NumPlot);
                divNumSuite.Visible = (r.NumSuite);
                divTypeDeed.Visible = (r.TypeDeed);//ไป Replace ทีหลัง
                divNumber.Visible = (r.Number);
                divDateAs.Visible = (r.DateAs);
                divDateAsCount.Visible = (r.DateAsCount);
                divDeedNo.Visible = (r.DeedNo);
                divFreeText.Visible = (r.FreeText);

                int i = 0;
                hidName.Value = string.Format("{{0}}", i.ToString()) + r.DocTypeName_Begin
                                + (r.NumOrder ? " ลำดับ" : "") + "{" + (++i).ToString() + "}"
                                + (r.NumRound ? " ครั้งที่" : "") + "{" + (++i).ToString() + "}"
                                + (r.NumEdition ? " รวม" : "") + "{" + (++i).ToString() + "}" + (r.NumEdition ? "ฉบับ" : "")
                                + (r.NumDeed ? " รวม" : "") + "{" + (++i).ToString() + "}" + (r.NumDeed ? "โฉนด" : "")
                                + (r.NumPlot ? " รวม" : "") + "{" + (++i).ToString() + "}" + (r.NumPlot ? "แปลง" : "")
                                + (r.NumSuite ? " รวม" : "") + "{" + (++i).ToString() + "}" + (r.NumSuite ? "ห้องชุด" : "")
                                + (r.WithOriginalContract ? " พร้อมต้นฉบับสัญญาต่อท้ายหนังสือจำนองฯ" : "")
                                + (r.AddSecuritiesIncreasing ? " เพิ่มหลักทรัพย์โดยไม่เพิ่มวงเงินจำนอง" : "")
                                + (r.Particular ? " เฉพาะส่วน(ระหว่างภาระจำยอม)" : "")
                                + (r.MortgageeIssue ? " (ฉบับผู้รับจำนอง)" : "")
                                + (r.Number ? " เลขที่" : "") + "{" + (++i).ToString() + "}"
                                + (r.DeedNo ? " [โฉนดเลขที่" : "") + "{" + (++i).ToString() + "}" + (r.DeedNo ? "]" : "")
                                + (r.FreeText ? " [" : "") + "{" + (++i).ToString() + "}" + (r.FreeText ? "]" : "")
                                + (r.DateAs ? " ลงวันที่" : "") + "{" + (++i).ToString() + "}"
                                + (r.DateAsCount ? " ลงวันที่(" : "") + "{" + (++i).ToString() + "}" + (r.DateAsCount ? ")" : "");
                GetUI();
                UpdatePanel2.Update();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AdditionDocModal", "$('#AdditionDocModal').modal('show');", true);
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "W");
            }
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                GeneratePDF(false);
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                GeneratePDF(true);
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        #region Modal
        protected void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetUI();
        }

        protected void txt_TextChanged(object sender, EventArgs e)
        {
            GetUI();
        }

        protected void btnAddDateAs_Click(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(hidLastSeq.Value);
            Ds.Add(new DateAsCountClass() { id = num, DateAsC = Convert.ToDateTime(txtDateAsCount.Value) });
            gvDateAsCountDataBind();
            hidLastSeq.Value = (num + 1).ToString();
        }

        protected void gvDateAsCount_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Ds.RemoveAt(Convert.ToInt32(e.CommandArgument));
            gvDateAsCountDataBind();
        }

        protected void btnSaveDoc_Click(object sender, EventArgs e)
        {
            GetUI();
            vCoverpage r = CPs.Where(D => D.DocTypeID == Convert.ToDecimal(DocTypeID)).FirstOrDefault();
            r.DocTypeName = txtName.Text;
            r.IsSelect = true;
            gvDocument.DataSource = CPs;
            gvDocument.DataBind();
            UpdatePanel1.Update();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AdditionDocModal", "$('#AdditionDocModal').modal('hide');", true);
        }
        #endregion
    }
}