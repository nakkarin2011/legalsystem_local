﻿using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages.Helper
{
    public partial class PDFPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string docID = Request.QueryString["val"];
            OpenInNewTab(Convert.ToInt32(docID));
        }

        public void OpenInNewTab(int docID)
        {
            trDocumentRep rep = new trDocumentRep();
            string docPath = rep.GetDocumentByID(docID).DocPath;
            string extension = System.IO.Path.GetExtension(docPath);
            switch (extension)
            {
                case ".pdf":
                    Response.ContentType = "Application/pdf";
                    break;
                case ".png":
                case ".jpg":
                case ".jfif":
                    Response.ContentType = "image/jpeg";
                    break;
            }            
            //Response.TransmitFile(@"C:\Users\WIN10\Desktop\PDFFile.pdf");
            Response.TransmitFile(docPath);
        }
    }
}