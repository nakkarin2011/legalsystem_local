﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LegalSystem.Pages
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
            }
            else
            {
            }
        }


        protected void Save_Click(object sender, EventArgs e)
        {
            //upload
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "hide-attachModal-222222", "upload();", true);
            var file1 = Request.Files["file1"];
            byte[] bytes = new byte[Request.Files["file1"].ContentLength];
            string fullname = file1.FileName;
            string filename = GetFileName(fullname);
            string fileextension = GetFileExtension(fullname);
            filename += DateTime.Now.ToString("_yyyyMMddHHmmss") + fileextension;
        }

        public string GetFileName(string fileNameWithExtension)
        {
            int last = fileNameWithExtension.LastIndexOf(".");
            string name = fileNameWithExtension.Substring(0, last);
            return name;
        }

        public string GetFileExtension(string fileNameWithExtension)
        {
            int last = fileNameWithExtension.LastIndexOf(".");
            string extension = fileNameWithExtension.Substring(last, fileNameWithExtension.Length - last);
            return extension;
        }

        protected void Save_Click1(object sender, EventArgs e)
        {
            string bts = sbytes.Value;
            var file = Request.Files["file1"];
            byte[] someString = Encoding.ASCII.GetBytes(bts);

        }
    }
}