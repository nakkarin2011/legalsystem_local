﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="LegalSystem.Pages.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/jquery1-1.12.0.min.js"></script>
    <%--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
        //function pageLoad(sender, args){
            var name = document.getElementById('name');
            //var bytes = document.getElementById('bytes');
            var sbytes = document.getElementById('sbytes');
            $('input[id="file1"]').change(function (e) {
                var fileName = e.target.files[0].name;
                //console.log(fileName);
                name.innerHTML = fileName;

                var reader = new FileReader();
                reader.onload = function () {
                    var arrayBuffer = this.result,
                        array = new Uint8Array(arrayBuffer),
                        binaryString = String.fromCharCode.apply(null, array);
                    var bytesSting = unpack(binaryString);
                    console.log(array);
                    //bytes.innerHTML = binaryString;
                    sbytes.innerHTML = bytesSting;
                }
                reader.readAsArrayBuffer(this.files[0]);
            });
        });
        function upload() {
            alert('a');
            $(document).ready(function () {
                alert('a');
            var name = document.getElementById('name');
            //var bytes = document.getElementById('bytes');
            var sbytes = document.getElementById('sbytes');
            $('input[id="file1"]').change(function (e) {
                var fileName = e.target.files[0].name;
                //console.log(fileName);
                name.innerHTML = fileName;

                var reader = new FileReader();
                reader.onload = function () {
                    var arrayBuffer = this.result,
                        array = new Uint8Array(arrayBuffer),
                        binaryString = String.fromCharCode.apply(null, array);
                    var bytesSting = unpack(binaryString);
                    console.log(array);
                    //bytes.innerHTML = binaryString;
                    sbytes.innerHTML = bytesSting;
                }
                reader.readAsArrayBuffer(this.files[0]);
            });
        });
        }
        
        function unpack(str) {
            var bytes = [];
            for (var i = 0; i < str.length; i++) {
                var char = str.charCodeAt(i);
                // You can combine both these calls into one,
                //    bytes.push(char >>> 8, char & 0xff);
                bytes.push(char >>> 8);
                bytes.push(char & 0xFF);
            }
            return bytes;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                <ContentTemplate>
                    <input type="hidden" id="hid1" runat="server" />
                    <input type="file" id="file1" runat="server" />
                    <asp:Button ID="Save" runat="server" Text="Save" OnClick="Save_Click1"/>
                    <p id="name">name</p>
                    <hr />
                    <textarea id="sbytes" runat="server" style="max-width: 800px; width: 600px; height: 400px;">sbytes</textarea>
                    <%--<textarea id="bytes" runat="server" style="max-width: 800px; width: 600px; height: 400px;">bytes</textarea>--%>
                </ContentTemplate>
                <%--<Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Save" EventName="Click" />
                </Triggers>--%>
            </asp:UpdatePanel>



        </div>
    </form>
</body>
</html>
