﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="SettingType.aspx.cs" Inherits="LegalSystem.Pages.Setting.SettingType" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
   <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="card ">
                         <div class="card-header card-header-tabs card-header-info">
                             <h4 class="card-title" id="TypeHeader" runat="server">Inbox</h4>      
                          </div>
                         <div class="card-body">
                             <asp:Panel ID="Panel1" runat="server" DefaultButton="btnserch">
                                   <div class="row " style="margin-bottom: -20px;">
                                 <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">TypeName</span>
                                        </h5>
                                    </div>
                                </div>
                                 <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group">
                                         <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnserch" runat="server" CssClass="btn btn-info" OnClick="btnserch_Click">
                                            <i class="fa fa-search"></i>&nbsp;Search
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-fill btn-warning" OnClick="btnClear_Click">
                                            <i class="fa fa-trash"></i>&nbsp;Clear
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton id="btnAdd" runat="server" class="btn btn-success" OnClick="btnAdd_Click">
                                            <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add 
                                        </asp:LinkButton>
                                    </div>
                                </div>
                             </asp:Panel>
                             </div>
                             <div class="row">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="material-datatables table-responsive">
                                              <%--  <asp:GridView ID="gvTypeSetting" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover"
                                                    AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                                    DataKeyNames="TypeID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                                    PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                                    OnRowDataBound="gvTypeSetting_RowDataBound" OnPageIndexChanging="gvTypeSetting_PageIndexChanging"
                                                    OnRowCommand="gvTypeSetting_RowCommand" OnRowDeleting="gvTypeSetting_RowDeleting">--%>
                                                 <asp:GridView ID="gvTypeSetting"  runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-striped table-no-bordered table-hover"    
                                                 GridLines="None"
                                                 ShowHeaderWhenEmpty="true"
                                                 DataKeyNames="TypeID"
                                                 AllowPaging="true"
                                                 PageSize="10"
                                                 AllowSorting="false"
                                                 OnRowDataBound="gvTypeSetting_RowDataBound"
                                                 OnPageIndexChanging="gvTypeSetting_PageIndexChanging"
                                                 OnRowCommand="gvTypeSetting_RowCommand"
                                                 OnRowDeleting="gvTypeSetting_RowDeleting"
                                                 PageIndex="1">
                                                    <Columns>
                                                        <asp:TemplateField SortExpression="TypeName" HeaderStyle-Font-Bold="true" HeaderText="TypeName">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ibTypeName" runat="server" Text='<%#Eval("TypeName")%>'></asp:Label>
                                                                <asp:HiddenField ID="hidTypeID" runat="server" Value='<%#Eval("TypeID")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                           <%--     <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info" CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                                    CommandName="EditCommand" CommandArgument='<%# Eval("TypeID") %>' />--%>
                                                                   <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info" CausesValidation="false"
                                                                    CommandName="EditCommand" CommandArgument='<%# Eval("TypeID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" 
                                                                    CommandName="Delete" CommandArgument='<%# Eval("TypeID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:GridView>
                                           </div>
                                        </div>
                                    </div>
                                 </div>
                             </div>
                             
                              <%--<div class="row">
                                 <div class="card ">
                                    <div class="card-body">
                                       <div class="col-md-12">
                                            <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                  <tr>
                                                    <th>DocTypeID</th>
                                                    <th>DocTypeName</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                       </div>
                                    </div>
                                </div>
                              </div>--%>
                         </div>
                    </div>
               </ContentTemplate>
                 <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvTypeSetting" />
                    <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>



    <%-- GridView --%>
    <%--<div class="card">
        <div class="card-header ">
            <h3 id="TypeHeader" runat="server"></h3>
        </div>
        <div class="card-body">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_info">
                                <input type="text" runat="server" id="txtfilter" class="form-control col-sm-8" />
                                <asp:Button ID="btnserch" runat="server" Text="Search" class="btn btn-info" OnClick="btnserch_Click" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_info">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-info"
                                    data-toggle="modal" data-target="#myModal" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <asp:GridView ID="gvTypeSetting" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover"
                                AutoGenerateColumns="False" Font-Size="12px" Font-Strikeout="False" Font-Underline="False"
                                DataKeyNames="TypeID" AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                PagerSettings-NextPageImageUrl="~/Images/next.png" PagerSettings-LastPageImageUrl="~/Images/last.png"
                                OnRowDataBound="gvTypeSetting_RowDataBound" OnPageIndexChanging="gvTypeSetting_PageIndexChanging"
                                OnRowCommand="gvTypeSetting_RowCommand" OnRowDeleting="gvTypeSetting_RowDeleting">
                                <Columns>
                                    <asp:TemplateField SortExpression="TypeName" HeaderStyle-Font-Bold="true" HeaderText="TypeName">
                                        <ItemTemplate>
                                            <asp:Label ID="ibTypeName" runat="server" Text='<%#Eval("TypeName")%>'></asp:Label>
                                            <asp:HiddenField ID="hidTypeID" runat="server" Value='<%#Eval("TypeID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-fill btn-info"
                                                CausesValidation="false" data-toggle="modal" data-target="#myModal"
                                                CommandName="EditCommand" CommandArgument='<%# Eval("TypeID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-fill btn-danger" CommandName="Delete" CommandArgument='<%# Eval("TypeID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="False" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvTypeSetting" />
                    <asp:AsyncPostBackTrigger ControlID="btnserch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>--%>

    <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-lg" style="width:600px;">
            <!-- Modal content-->
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                   <%-- Modal --%>
                   <ContentTemplate>
                        <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="row"  style="padding-left:30px;padding-bottom:20px;">
                                        <label class="tim-typo mt-4 text-right">Type Name *</label>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <asp:HiddenField ID="hidTypeID" runat="server" />
                                                <input id="txtTypeName" runat="server" type="text" class="form-control" style="padding-top:20px;" />
                                                <label id ="lbRed" runat="server" class="text-danger"></label>
                                              <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required." ControlToValidate="txtTypeName"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                    </div>

                        </div>
                        <div class="modal-footer">
                           <%-- <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;--%>
                             <asp:Button ID="Button1" runat="server" Text="Save" OnClick="btnSave_Click" class="btn btn-fill btn-info" />&nbsp;
                        </div>
                    </ContentTemplate>
               </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
