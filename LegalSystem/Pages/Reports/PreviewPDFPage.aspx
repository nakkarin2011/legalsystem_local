﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewPDFPage.aspx.cs" Inherits="LegalSystem.Pages.Reports.PreviewPDFPage" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<!DOCTYPE html>

<script type="text/javascript">
    function AutoSize() {
        var myWidth = 0, myHeight = 0;
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
        }
        var scoll = 16;
        myWidth -= scoll;
        myHeight -= scoll;
        //window.alert('Width = ' + myWidth);
        //window.alert('Height = ' + myHeight);
        document.getElementById("iPDF").width = myWidth;
        document.getElementById("iPDF").height = myHeight;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Preview PDF</title>
</head>
<body onload="AutoSize()" onresize="AutoSize()">
    <form id="form1" runat="server">
        <div>
            <%--<uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />--%>
            <asp:Label ID="lbError" runat="server" CssClass="cssLabelError"></asp:Label>
            <table width="100%" cellpadding="0" cellspacing="5">
                <tr>
                    <td>
                        <iframe id="iPDF" runat="server" scrolling="auto" width="100%" height="90%"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
