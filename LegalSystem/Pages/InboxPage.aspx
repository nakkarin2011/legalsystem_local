﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="InboxPage.aspx.cs" Inherits="LegalSystem.Pages.InboxPage" %>

<%@ OutputCache Location="None" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>

                    <uc1:ucmessagebox runat="server" id="UcMessageBoxMain" />
                    <div class="card ">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 class="card-title" id="titlePageInbox" runat="server">Inbox</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Request Type</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlRequestType" runat="server" CssClass="form-control" AutoPostBack="false" DataValueField="ReqTypeID" DataTextField="ReqTypeName"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Request No</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtRequestNo" runat="server" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Request Status</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlRequestStatus" runat="server" CssClass="form-control" DataValueField="ReqStatusID" DataTextField="ReqStatusName"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-auto text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">CIF</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtCIF" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row " style="margin-bottom: -20px;">
                                <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Request Date</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtRequestDate" runat="server" class="datepicker form-control" autocomplete="off" />
                                    </div>
                                </div>                                
                                <div class="col-md-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Legal No</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" id="txtLegalNo" runat="server" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Legal Status</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlLegalStatus" runat="server" CssClass="form-control" DataValueField="ActionCode" DataTextField="LegalStatusDisplay"></asp:DropDownList>
                                    </div>
                                </div>


                            </div>
                            <div class="row" style="margin-bottom: -20px; padding-bottom: 1.5em">
                                <div class="col-md-auto "></div>
                                <div class="col-md-3 ">
                                    <div class="form-group">                                        
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-fill btn-info" OnClick="btnSearch_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-fill btn-warning" OnClick="btnClear_Click" />
                                    </div>
                                </div>
                            </div>
                            <fieldset id="FSgvInquiry" runat="server" visible="false">
                                <div class="row">
                                    <div class="card ">
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="material-datatables table-responsive">
                                                    <asp:GridView ID="gvInquiry" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                        ShowHeaderWhenEmpty="true" DataKeyNames="RequestNo" AllowPaging="true" PageSize="10" AllowSorting="false" OnPageIndexChanging="gvInquiry_PageIndexChanging"
                                                        OnRowCommand="gvInquiry_RowCommand" OnRowDataBound="gvInquiry_RowDataBound" PageIndex="1">
                                                        <Columns>
                                                            <asp:BoundField Visible="false" DataField="RequestID" HeaderText="RequestID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="RequestNo" SortExpression="RequestNo" HeaderText="Request No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="RequestTypeName" SortExpression="RequestTypeName" HeaderText="Request Type" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="CifNo" SortExpression="CifNo" HeaderText="CIF No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="DefendantNo" SortExpression="DefendantNo" HeaderText="No of defendant" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="RequestDate" SortExpression="RequestDate" HeaderText="Request Date" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                                            <asp:BoundField DataField="RequestStatusName" SortExpression="RequestStatusName" HeaderText="Request Status" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="LegalNo" SortExpression="LegalNo" HeaderText="Legal No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="LegalStatusName" SortExpression="LegalStatusName" HeaderText="Legal Status" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="LegalStatusDate" SortExpression="LegalStatusDate" HeaderText="Legal Date" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
<%--                                                            <asp:BoundField DataField="LegalStatusName" SortExpression="Assign OA/IA" HeaderText="Assign OA/IA" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            <asp:BoundField DataField="LegalStatusDate" SortExpression="Assign Lawyer" HeaderText="Assign Lawyer" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>--%>

                                                            
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnView" runat="server" Text="View" class="btn btn-fill btn-info"
                                                                        CommandName='<%# Eval("RequestStatus") %>' CommandArgument='<%# Eval("RequestNo") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnDocument" runat="server" Text="Document"
                                                                        class="btn btn-fill btn-info" CommandArgument='<%# Eval("RequestNo") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="false" ForeColor="Black" />
                                                        <HeaderStyle HorizontalAlign="Left" Font-Bold="true"></HeaderStyle>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="loading-visible" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: white; opacity: 0.75;">
                        <p>
                            <img style="padding: 10px; position: fixed; top: 50%; left: 50%;" src="../Resources/Preloader_4.gif" />
                        </p>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
    <div id="fade" class="black_overlay"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script src="../Scripts/vendor/material-dashboard.min.js"></script>
    <link href="../Content/vendor/material-dashboard.css" rel="stylesheet" />
    <script src="../Scripts/plugin/bootstrap-datepicker.th.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: 'calendar.png',
                format: 'dd/mm/yyyy',
                todayBtn: false,
                language: 'th-th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            })//.datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
        });

    </script>
</asp:Content>
