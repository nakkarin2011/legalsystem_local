﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TCRB.Securities;
using System.Globalization;
using TCRB.Securities.UserManagement;
using LegalSystem.CommonHelper;
using LegalSystem.Common.Common_RequestPage;
using LegalSystem.CommonFunctionFlow;

namespace LegalSystem.Pages
{
    public partial class LegalInfo : System.Web.UI.Page
    {

        List<trRequest> ListRNO = new List<trRequest>();
        trRequest RNO = new trRequest();
        string vCIF = string.Empty;
        trLegal Legal = new trLegal();
        AlertMessage Msg = new AlertMessage();
        trRequest trCIF = new trRequest();
        SetUI_RequestPage UI_RequestPage = new SetUI_RequestPage();
        string User;
        int GroupID = 0;
        public UserIdentity UserLogin
        {
            get { return Session["UserLogin"] != null ? Session["UserLogin"] as UserIdentity : null; }
            set { Session["UserLogin"] = value; }
        }
        private string rid
        {
            get { return Session["RID"] != null ? Session["RID"] as string : null; }
            set { Session["RID"] = value; }
        }
        private string acct
        {
            get { return Session["ACCT"] != null ? Session["ACCT"] as string : null; }
            set { Session["ACCT"] = value; }
        }
        private bool IsSubmited
        {
            get { return Session["IsSubmited"] != null ? Convert.ToBoolean(Session["IsSubmited"]) : false; }
            set { Session["IsSubmited"] = value; }
        }
        private List<LNMAST> LNs
        {
            get { return Session["ListAcctLoan"] != null ? Session["ListAcctLoan"] as List<LNMAST> : null; }
            set { Session["ListAcctLoan"] = value; }
        }

        public string RequestNo
        {
            get { return Request.QueryString["rno"] != null ? Request.QueryString["rno"] : null; }
            set { Request.QueryString["rno"] = value; }
        }             

        private void BinducAccountLoan()
        {
            gvAccountLoan.DataSource = LNs;
            gvAccountLoan.DataBind();
            if (LNs.Count > 0)
                txtAddress.Value = LNs[0].CFADDR;
            else
                txtAddress.Value = txtCIF.Value = string.Empty;
        }

        private string GetTypeFile()
        {
            string Attachments = (chk1.Checked ? "1," : "0,")//หนังสือบอกกล่าวผู้ค้ำประกัน พร้อมไปรษณีย์ตอบรับ
                + (chk11.Checked ? "1," : "0,")   //มี
                + (chk12.Checked ? "1," : "0,")   //ไม่มี เพราะไม่มีผู้ค้ำประกัน
                + (chk2.Checked ? "1," : "0,")    //หนังสือแจ้งวันขายทอดตลาดหลักทรัพย์จำนำ พร้อมไปรษณีย์ตอบรับ
                + (chk21.Checked ? "1," : "0,")   //ใบรายงานการประมูล
                + (chk3.Checked ? "1," : "0,")    //มีบัญชีเงินฝาก
                + (chk31.Checked ? "1," : "0,")   //มี ตัดบัญชีเงินฝากชำระหนี้เงินกู้ทั้งจำนวน
                + (chk32.Checked ? "1," : "0,")   //มีบันทึกถึงฝ่ายปฏิบัติการกลาง (ไม่สามารถตัดบัญชีออมทรัพย์ได้)
                + (chk4.Checked ? "1," : "0,")    //ไม่มีบัญชีเงินฝาก
                + (chk41.Checked ? "1," : "0,")   //ลูกหนี้หลบหนี/ไม่สามารถติดต่อลูกหนี้ได้เป็นเวลานาน
                + (chk42.Checked ? "1" : "0");    //ลูกหนี้ไม่มีความสามารถในการชำระหนี้
            return Attachments;
        }

        
        private trRequest GetUIRequest()
        {
            trRequest req = new trRequest();
            req.RequestNo = txtRequestNo.Value;
            req.RequestDate = DateTime.Today;
            req.RequestType = ddlRequestType.SelectedValue;
            req.RequestStatusDate = DateTime.Today;
            req.RequestReason = ddlReason.SelectedValue;
            req.RefRequestNo = txtReferRequestNo.Value;
            req.RefRequestType = hidReferRequestType.Value;
            req.RefRequestReason = string.Empty;
            req.LegalStatus = ddlLegalStatus.SelectedValue;
            req.CifNo = Convert.ToDecimal(txtCIF.Value);
            req.Address = txtAddress.Value;

            return req;            
        }

        private List<trRequestRelation> GetUIRequestRelation(ref int DefendantNo, string ReqID)
        {
            List<trRequestRelation> RRs = new List<trRequestRelation>();
            using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
            {
                trRequestRelation RR = new trRequestRelation();
                string CollateralType = string.Empty;
                DefendantNo = 0;
                foreach (GridViewRow rowA in gvAccountLoan.Rows)
                {
                    var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                    if (chkAL.Checked)
                    {
                        DefendantNo++;
                        var txtAL = (HtmlInputText)rowA.FindControl("txtAcctLoan");
                        var txtCT = (HtmlInputText)rowA.FindControl("txtCollateralType");
                        if (string.IsNullOrEmpty(CollateralType))
                            CollateralType = txtCT.Value;
                        else
                        {
                            if (CollateralType != txtCT.Value)
                                throw new Exception(MessageEvent.SelectSameCollateral);
                            //throw new Exception("กรุณาเลือกบัญชีที่มีหลักประกันเดียวกัน");
                        }
                        var gvR = (GridView)rowA.FindControl("gvRelationship");
                        int rrNum = 0;
                        foreach (GridViewRow rowR in gvR.Rows)
                        {
                            var hidCIF = (HiddenField)rowR.FindControl("hidCIF");
                            var hidType = (HiddenField)rowR.FindControl("hidType");
                            var hidOrder = (HiddenField)rowR.FindControl("hidOrder");
                            var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                            if (chkR.Checked)
                            {
                                rrNum++;
                                RR = new trRequestRelation()
                                {
                                    RRID = unitOfwork.LegalStoreProcedure.GetGuidID(),
                                    RequestID = new Guid(ReqID),
                                    Account = Convert.ToDecimal(txtAL.Value),
                                    RelationCifNo = Convert.ToDecimal(hidCIF.Value),
                                    RelationType = hidType.Value,
                                    RelationSeq = Convert.ToInt32(hidOrder.Value)
                                };
                                RRs.Add(RR);
                            }
                        }
                        if (rrNum == 0)
                            throw new Exception(MessageEvent.SelectRelationship + txtAL.Value);
                        //throw new Exception("กรุณาเลือก Relationship ของบัญชี " + txtAL.Value);
                    }
                    else
                    {
                        var gvR = (GridView)rowA.FindControl("gvRelationship");
                        foreach (GridViewRow rowR in gvR.Rows)
                        {
                            var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                            chkR.Attributes.Remove("checked");
                        }
                    }
                }
            }
            if (DefendantNo == 0 || RRs.Count == 0)
                throw new Exception(MessageEvent.SelectLoanAccountAndRelationship); //throw new Exception("กรุณาเลือกบัญชีสินเชื่อและ Relationship อย่างน้อย 1 บัญชี");

            return RRs;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    rid = Request.QueryString["rid"].ToString();
                    User = UserLogin.Emp_Code;
                    GroupID = UserLogin.Groups[0].GroupID;
                    setUI_Request();
                }
                UcMessageBoxMain.VisibleControl();
            }
            catch (Exception ex)
            {
                Msg.MsgReturn(ex.Message.ToString(), Page, this, MasterPageHelper.RedirectLoginPage/* "../Login.aspx"*/, MessageEventType.error);
            }
        }

        

        protected void btnFindCIF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                trCIF = new trRequest();
                if (!string.IsNullOrEmpty(txtCIF.Value))
                {
                    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                    {
                        var ListCif = unitOfwork.TrRequestRep.GetRequestByCIF(txtCIF.Value);
                        if (ListCif.Count < 1 || sender.ToString() == MasterECommandNameHelper.View)
                        {
                            LNs = unitOfwork.LnmastRep.GetInfomationByCIF(LegalSystem.Properties.Settings.Default.LinkServer, txtCIF.Value, rid, 0);
                            if (LNs.Count > 0)
                                BinducAccountLoan();
                            else
                            {
                                Msg.MsgBox(MessageEvent.LoanAccountNotFound, MessageEventType.info, this, this.GetType());
                                LNs = new List<LNMAST>();
                                BinducAccountLoan();
                            }
                        }
                        else
                        {
                            Msg.MsgBox(MessageEvent.SearchCIFDuplicate, MessageEventType.info, this, this.GetType());
                            LNs = new List<LNMAST>();
                            BinducAccountLoan();
                        }
                    }
                }
                else
                {
                    Msg.MsgBox(MessageEvent.PleaseEnterCIFNo, MessageEventType.error, this, this.GetType());
                    LNs = new List<LNMAST>();
                    BinducAccountLoan();
                }
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
                //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }

        protected void gvAccountLoan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            using (LegalUnitOfWork unitofwork = new LegalUnitOfWork())
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LNMAST ln = e.Row.DataItem as LNMAST;
                    var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkAccount")));
                    control.Attributes.Add("onclick", "javascript:checkChange('" + ln.ACCTNO + "');");
                    control.Style.Add("cursor", "pointer");
                    if (ln.isSelect)
                        control.Attributes.Add("checked", "checked");
                    if (IsSubmited || !ln.isEnable)//
                        control.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled/*"disabled", "disabled"*/);
                    ((HtmlInputText)(e.Row.FindControl("txtAcctLoan"))).Value = ln.ACCTNO.ToString();
                    ((HtmlInputText)(e.Row.FindControl("txtAcctStatus"))).Value = ln._STATUS;
                    ((HtmlInputText)(e.Row.FindControl("txtDPD"))).Value = ln.PDDAYS.ToString();
                    ((HtmlInputText)(e.Row.FindControl("txtCollateralType"))).Value = ln.CollateralDesc;


                    List<sp_GetDataRelate> Rs = new List<sp_GetDataRelate>();
                    string[] Collateral = ln.CollateralDesc.Split(' ');
                    Rs = unitofwork.LegalStoreProcedure.GetDataRelate(LegalSystem.Properties.Settings.Default.LinkServer, ln.ACCTNO, Collateral[0].ToString());

                    //using (LegalDataContext rep = new LegalDataContext())
                    //    Rs = rep.GetRelationship(ln.ACCTNO.ToString(), rno).ToList();
                    foreach (sp_GetDataRelate tmp in Rs)
                    {
                        tmp.IsSelect = ln.isSelect;
                        tmp.IsEnable = ln.isEnable;
                    }
                    ((GridView)(e.Row.FindControl("gvRelationship"))).DataSource = Rs;
                    ((GridView)(e.Row.FindControl("gvRelationship"))).DataBind();

                    ((HtmlInputText)(e.Row.FindControl("txtLoanType"))).Value = ln.TYPE;
                    ((HtmlInputText)(e.Row.FindControl("txtCurBalAmt"))).Value = ln.CBAL.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtOriginalAmt"))).Value = ln.ORGAMT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtAcctInterest"))).Value = ln.ACCINT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtInterestRate"))).Value = ln.INTRATE.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtACCI3"))).Value = ln.ACCI3.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtNumOfDPD"))).Value = ln.PDDAYS.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtPENIN3"))).Value = ln.PENIN3.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtPENINT"))).Value = ln.PENINT.ToString("N2");
                    ((HtmlInputText)(e.Row.FindControl("txtPOTHER"))).Value = "0.00";
                    ((HtmlInputText)(e.Row.FindControl("txtTotal"))).Value = (ln.CBAL + ln.ACCINT + ln.ACCI3 + ln.PENIN3 + ln.PENINT).ToString("N2");
                }
            }
        }

        protected void gvAccountLoan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == MasterECommandNameHelper.Select)
            { }
        }
        protected void gvAccountLoan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvRelationship_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                sp_GetDataRelate r = e.Row.DataItem as sp_GetDataRelate;
                var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkRelationship")));
                if (r.IsSelect)
                    control.Attributes.Add("checked", "checked");
                if (IsSubmited || !r.IsEnable)
                    control.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled/*"disabled", "disabled"*/);
            }
        }

        #region //*** Button && Function Save & Submit && ViewClick ***//    
       

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
               
                
                    using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                    {
                        trRequest req = unitOfwork.TrRequestRep.Get(x => x.RequestID == new Guid(hidRequestID.Value)).FirstOrDefault();
                        List<trRequestRelation> tmpRels = unitOfwork.TrRequestRelateRep.Get(x => x.RequestID == new Guid(rid)).ToList();
                        int DefendantNo = req.DefendantNo;
                        List<trRequestRelation> RRs = GetUIRequestRelation(ref DefendantNo, rid);
                        
                        var s = req.RequestStatus.Replace("F", "").Replace("R", "").ToString();
                        int status = Convert.ToInt32(s);
                        switch (status)
                        {
                            case 1:
                                //req.RequestStatus = "F2";//Waiting for Approved
                                req.RequestStatus = MasterStatusHelper.StatusWaitingforApproved;
                                req.RequestStatusDate = DateTime.Now;
                                
                                req.OwnerApprove = DateTime.Now;
                                if (ddlRequestType.SelectedItem.Value == "1")
                                    req.ColChecker = ddlApprover.SelectedValue;
                                else if (ddlRequestType.SelectedItem.Value == "2")
                                {
                                    req.ColHead = ddlApprover.SelectedValue;
                                    status = 2;
                                }
                                req.RequestReason = ddlReason.SelectedValue;
                                req.RequestType = ddlRequestType.SelectedValue;
                                break;
                           
                        }

                        req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        req.UpdateDate = DateTime.Now;
                        req.Comment = txtComment.Text;
                        unitOfwork.Save();

                    }
                    //*** Save and Return กลับไปหน้า InquiryPage ***//
                    Msg.MsgReturn(MessageEvent.Save, Page, this, MasterPageHelper.InboxPage /*"InboxPage.aspx"*/, MessageEventType.success);
                    //Response.Redirect("InboxPage.aspx");
               
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
                //UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "E");
            }
        }
        

        #region //*** Click View จาก Inquiry Request ***//
        private void setUI_Request()
        {
            try
            {                
                //myDiv.Attributes.Add("disabled", "disabled");
                titlePage.InnerText = MasterPageTitleHelper.InnerInquiryRequest;
                Label33.Visible = false;
                //FieldView.Attributes.Add("disabled", "disabled");
                FieldAttachment.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                //FieldAccount.Attributes.Add("disabled", "disabled");
                
                divComment.Visible = true; txtComment.Enabled = false;
                divAttachments.Disabled = true;
                Legal = new trLegal();
                RNO = new trRequest();
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {                   
                    RNO = unitOfwork.TrRequestRep.GetByID(new Guid(rid));
                    
                    List<vRequestTypeMapLegalStstus> ddlRType = new List<vRequestTypeMapLegalStstus>();
                    UI_RequestPage.Set_DropDownList(unitOfwork, ddlRequestStatus, ddlLegalStatus, ddlRequestType, ddlApprover, rid, User, ddlRType, MasterECommandNameHelper.View);
                    UI_RequestPage.Set_ddlAssignLawyer(unitOfwork, ddlAttorney, ddlLawyer, RNO.RequestType, User);
                    //GetRequestReason(ddlReason, unitOfwork);

                    Legal = unitOfwork.TrLegalRep.Get(g => g.LegalID == RNO.LegalID).FirstOrDefault();
                    vCIF = RNO.CifNo.ToString();
                    if (Legal == null) txtLegalNo.Value = ""; else txtLegalNo.Value = Legal.LegalNo;

                    
                    txtRequestNo.Value = RNO.RequestNo;
                    txtRequestDate.Value = RNO.CreateDate.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    hidLegalStatus.Value = RNO.LegalStatus;
                    hidRequestID.Value = RNO.RequestID.ToString();
                    hidRefRequestNo.Value = RNO.RefRequestNo;
                    ddlRequestStatus.SelectedValue = RNO.RequestStatus;
                    ddlRequestType.DataValueField = "ReqTypeID";
                    ddlRequestType.DataTextField = "ReqTypeName";
                    ddlRequestType.DataBind();
                    ddlRequestType.SelectedValue = RNO.RequestType;
                    txtReferRequestNo.Value = RNO.RefRequestNo;
                    if (!string.IsNullOrEmpty(RNO.RefRequestType))
                        txtReferRequestType.Value = ddlRType[Convert.ToInt32(RNO.RefRequestType)].ReqTypeName.ToString();
                    txtCIF.Value = vCIF;
                    txtComment.Text = RNO.Comment;
                    ddlLegalStatus.SelectedValue = RNO.LegalStatus;
                    ddlReason.DataSource = unitOfwork.VRequestReasonRep.GetReasonByRequest(Convert.ToInt32(ddlRequestType.SelectedValue));
                    ddlReason.DataBind();
                    ddlReason.SelectedValue = RNO.RequestReason; //*** ddlเหตุผล ***//
                    ddlApprover.SelectedValue = RNO.ColHead;     //*** ddlApprove ***//             
                    btnFindCIF_Click(MasterECommandNameHelper.View, null);

                    LNs = unitOfwork.LnmastRep.GetInfomationByCIF(LegalSystem.Properties.Settings.Default.LinkServer, RNO.CifNo.ToString(), rid, RNO.RequestStatus_);

                    BinducAccountLoan();
                    SetDisableGridRelation();
                }
            }
            catch (Exception ex)
            {
                //UcMessageBoxMain.AlertMessageBox(ex.ToString(), MessageEventType.error, this, this.GetType());
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion

        #region //*** btnReturnToPool ***//
        protected void btnReturnToPool_Click(object sender, EventArgs e)
        {
            try
            {
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfWork.TrRequestRep.GetByID(new Guid(rid));
                    req.RequestStatus = MasterStatusHelper.StatusRegistered;
                    req.RequestStatusDate = DateTime.Now;
                    req.LawyerID = null;
                    req.SeniorLawyer = "0";
                    req.SeniorLawyerApprove = req.SeniorLawyerReceive = null;
                    req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                    req.UpdateDate = DateTime.Now;
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.ReturnToPool, this, this.GetType(), MasterPageHelper.AssignPage, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion
        #region //*** btnSave ตอน Status มาอยู่ที่หน้า Assign ***//
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string PageRe = string.Empty;
                using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                {
                    trRequest req = unitOfWork.TrRequestRep.GetRequestByID(hidRequestID.Value);
                    //vAssignLawyer AsLawyer = unitOfWork.VAssignLawyerRep.GetAssignByReqNo(req.RequestNo);
                    if (req.RequestStatus == MasterStatusHelper.StatusApprovedtoLawyer)
                    {
                        if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                        req.LawyerIDReceive = DateTime.Now;
                        req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        req.UpdateDate = DateTime.Now;
                        PageRe = MasterPageHelper.InboxPage;
                    }
                    else
                    {
                        req.LawyerAttorneyID = ddlAttorney.SelectedValue;//EmpCode
                        req.LawyerID = ddlLawyer.SelectedValue;
                        if (!string.IsNullOrEmpty(txtComment.Text)) req.Comment = txtComment.Text;
                        req.UpdateBy = GlobalSession.UserLogin.Emp_Code;
                        req.UpdateDate = DateTime.Now;
                        PageRe = MasterPageHelper.AssignPage;
                    }
                    unitOfWork.Save();
                }
                Msg.MsgReturn(MessageEvent.Save, this, this.GetType(), PageRe, MessageEventType.success);
            }
            catch (Exception ex)
            {
                Msg.MsgBox(ex.Message.ToString(), MessageEventType.error, this, this.GetType());
            }
        }
        #endregion

        #endregion

        private void SetDisableGridRelation()
        {
            foreach (GridViewRow rowA in gvAccountLoan.Rows)
            {
                var chkAL = (HtmlInputCheckBox)rowA.FindControl("chkAccount");
                chkAL.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                var gvR = (GridView)rowA.FindControl("gvRelationship");
                foreach (GridViewRow rowR in gvR.Rows)
                {
                    var chkR = (HtmlInputCheckBox)rowR.FindControl("chkRelationship");
                    chkR.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                }
            }
        }

        
    }
}