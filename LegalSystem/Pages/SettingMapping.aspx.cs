﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalEntities;
using System.ComponentModel;
using LegalService;
using LegalSystem.Common;

namespace LegalSystem.Pages.Setting
{
    public partial class SettingMapping1 : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private string paramType;
        public string ParamType
        {
            get
            {
                return (string)Session["ParamType"];
            }
            set
            {

                Session["ParamType"] = value;
            }
        }

        private DataTable dtGridview;
        public DataTable DtGritView
        {
            get
            {
                return (DataTable)Session["GVList"];
            }
            set
            {
                Session["GVList"] = value;
            }
        }

        private List<vCostType> costtypeList;
        public List<vCostType> CostTypeList
        {
            get
            {
                return (List<vCostType>)Session["costtypeList"];
            }
            set
            {
                Session["costtypeList"] = value;
            }
        }

        private List<vDocumentType> documentTypeList;
        public List<vDocumentType> DocumentTypeList
        {
            get
            {
                return (List<vDocumentType>)Session["DocumentTypeList"];
            }
            set
            {
                Session["DocumentTypeList"] = value;
            }
        }

        private List<vLegalStatusMapping> legalStatusMappingList;
        public List<vLegalStatusMapping> LegalStatusMappingList
        {
            get
            {
                return (List<vLegalStatusMapping>)Session["LegalStatusMappingList"];
            }
            set
            {
                Session["LegalStatusMappingList"] = value;
            }
        }
        public List<vLawyerOAIA> OAIAList
        {
            get
            {
                return (List<vLawyerOAIA>)Session["OAIAList"];
            }
            set
            {
                Session["OAIAList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            lbRed1.InnerText = String.Empty;
            lbRed2.InnerText = string.Empty;
            if (!IsPostBack)
            {
                paramType = Request.QueryString["type"];
                ParamType = paramType;
                //ParamType = "CT"; // for test

                InitData();
            }
        }

        public void InitData()
        {
            switch (ParamType)
            {
                case "CT":
                    // Set Header
                    TypeHeader.InnerText = "Cost Type";
                    // Set Modal
                    lbType1Name_modal.InnerText = "Attorney Type";
                    lbType2Name_modal.InnerText = "Cost Name";
                    txtType1.Visible = false;
                    ddltype2.Visible = false;
                    BindDdlAttorneyType();
                    // set columns in gridview
                    gvTypeMapping.Columns[0].HeaderText = "Attorney Type";
                    gvTypeMapping.Columns[1].HeaderText = "Cost Name";
                    break;
                case "DM":
                    // Set Header
                    TypeHeader.InnerText = "Document Type Mapping";
                    categoryText.InnerText = "Mapping Request Type and Document Type";
                    spText.InnerText = "Request Type or Document Type";
                    lbType1Name_modal.InnerText = "Request Type*";
                    lbType2Name_modal.InnerText = "Document Type*";
                    txtType1.Visible = false;
                    txtType2.Visible = false;
                    BindDdlRequestType();
                    gvTypeMapping.Columns[0].HeaderText = "Request Type";
                    gvTypeMapping.Columns[1].HeaderText = "Document Type";
                    break;
                case "OA":
                    // Set Header
                    TypeHeader.InnerText = "OA/IA";
                    spText.InnerText = "Attorney Type or Lawyer Name";
                    // Set Modal
                    lbType1Name_modal.InnerText = "Attorney Type*";
                    lbType2Name_modal.InnerText = "Lawyer Name*";
                    txtType1.Visible = false;
                    ddltype2.Visible = false;
                    BindDdlAttorneyType();
                    // set columns in gridview
                    gvTypeMapping.Columns[0].HeaderText = "Lawyer Name";
                    gvTypeMapping.Columns[1].HeaderText = " Attorney Type";
                    break;
                case "LM":
                    // Set Header
                    TypeHeader.InnerText = "Legal Status Mapping";
                    categoryText.InnerText = "Request Type and Legal Status Mapping";
                    spText.InnerText = "Request Type or Legal Status";
                    // Set Modal
                    lbType1Name_modal.InnerText = "Request Type*";
                    lbType2Name_modal.InnerText = "Legal Status*";
                    txtType1.Visible = false;
                    txtType2.Visible = false;
                    BindDdlLegalStatus();
                    BindDdlRequestType();
                    // set columns in gridview
                    gvTypeMapping.Columns[0].HeaderText = "Legal Status";
                    gvTypeMapping.Columns[1].HeaderText = "Request Type";
                    break;
            }
            GetAllaType();
        }

        public void ConvertToDataTable()
        {
            DataTable table = new DataTable();
            PropertyDescriptorCollection props;
            object[] values;

            switch (ParamType)
            {
                case "CT":
                    props = TypeDescriptor.GetProperties(typeof(vCostType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (vCostType item in CostTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    table.Columns[1].ColumnName = "Type1ID";
                    table.Columns[0].ColumnName = "Type2ID";
                    table.Columns[3].ColumnName = "Type2Name";
                    table.Columns[2].ColumnName = "Type1Name";
                    table.Columns[4].ColumnName = "MapID";
                    DtGritView = table;
                    break;
                case "DT":
                    props = TypeDescriptor.GetProperties(typeof(vDocumentType));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (vDocumentType item in DocumentTypeList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    table.Columns[0].ColumnName = "Type2ID";
                    table.Columns[1].ColumnName = "Type1ID";
                    table.Columns[2].ColumnName = "Type1Name";
                    table.Columns[3].ColumnName = "Type2Name";
                    DtGritView = table;
                    break;
                case "OA":
                    props = TypeDescriptor.GetProperties(typeof(vLawyerOAIA));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (vLawyerOAIA item in OAIAList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    table.Columns[0].ColumnName = "Type1ID";
                    table.Columns[1].ColumnName = "Type2ID";
                    table.Columns[2].ColumnName = "Type1Name";
                    table.Columns[3].ColumnName = "Type2Name";
                    table.Columns[4].ColumnName = "MapID";
                    DtGritView = table;
                    break;
                case "LM":
                    props = TypeDescriptor.GetProperties(typeof(vLegalStatusMapping));

                    for (int i = 0; i < props.Count; i++)
                    {
                        PropertyDescriptor prop = props[i];
                        table.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    values = new object[props.Count];
                    foreach (vLegalStatusMapping item in LegalStatusMappingList)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item);
                        }
                        table.Rows.Add(values);
                    }
                    //table.Columns[0].ColumnName = "MapID";
                    table.Columns[0].ColumnName = "Type1ID";
                    table.Columns[1].ColumnName = "Type2ID";
                    table.Columns[2].ColumnName = "Type1Name";
                    table.Columns[3].ColumnName = "Type2Name";
                    table.Columns[4].ColumnName = "MapID";
                    DtGritView = table;
                    break;
            }
        }

        public void EditLinqName()
        {
            switch (ParamType)
            {
                case "CT":
                    break;
                case "DT":
                    break;
                case "OA":
                    break;
                case "LM":
                    break;
            }
        }

        public void BindGrid()
        {
            ConvertToDataTable();
            gvTypeMapping.DataSource = DtGritView;
            gvTypeMapping.DataBind();
        }

        public void SetModaltoDefault()
        {
            //hidID1.Value = string.Empty;
            //txtType1.Value = string.Empty;
            //ddltype1.SelectedIndex = 0;
            //hidID2.Value = string.Empty;
            //txtType2.Value = string.Empty;
            //ddltype2.SelectedIndex = 0;

            switch (ParamType)
            {
                case "CT": // case: type1=dropdown type2=textbox
                case "DT":
                case "OA":
                    hidID1.Value = string.Empty;
                    ddltype1.SelectedIndex = 0;
                    hidID2.Value = string.Empty;
                    txtType2.Value = string.Empty;
                    break;
                case "LM": // case: type1=dropdown type2=dropdown
                    ddltype1.SelectedIndex = 0;
                    ddltype2.SelectedIndex = 0;
                    break;
            }
        }

        #region Initial
        public void GetAllaType()
        {
            switch (ParamType)
            {
                case "CT":
                    using (vCostTypeRep rep = new vCostTypeRep())
                    {
                        costtypeList = rep.GetAllCostType();
                        CostTypeList = costtypeList;
                        BindGrid();
                    }
                    break;
                case "DM":
                    using (DocTypeMapRep rep = new DocTypeMapRep())
                    {
                        var p = rep.GetView();
                        DtGritView = Utility.ConvertToDataTable(p);
                        DtGritView.Columns[0].ColumnName = "Type1ID";
                        DtGritView.Columns[1].ColumnName = "Type1Name"; 
                        DtGritView.Columns[2].ColumnName = "Type2ID";
                        DtGritView.Columns[3].ColumnName = "Type2Name";
                        DtGritView.Columns[4].ColumnName = "MapID";
                        gvTypeMapping.DataSource = DtGritView;
                        gvTypeMapping.DataBind();
                    }
                        break;
                case "OA":
                    using (vLawyerOAIARep rep = new vLawyerOAIARep())
                    {
                        OAIAList = rep.GetAllOAIAMapping();
                        OAIAList = OAIAList;
                        BindGrid();
                    }

                    break;
                case "LM":
                    using (vLegalStatusMappingRep rep = new vLegalStatusMappingRep())
                    {
                        legalStatusMappingList = rep.GetAllLegalStatusMapping();
                        LegalStatusMappingList = legalStatusMappingList;
                        BindGrid();
                    }
                    break;
            }
        }

        public void BindDdlAttorneyType()
        {
            using (AttorneyTypeRep rep = new AttorneyTypeRep())
            {
                List<AttorneyType> T = rep.GetAllAttorneyType();
                ddltype1.DataSource = T;
                ddltype1.DataValueField = "AttorneyTypeID";
                ddltype1.DataTextField = "AttorneyTypeName";
                ddltype1.DataBind();
                ddltype1.SelectedIndex = 0;
            }
        }

        public void BindDdlRequestType()
        {
            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
            {
                List<msRequestType> T = unitOfWork.MSRequestTypeRep.GetAllRequestType();
                ddltype1.DataSource = T;
                ddltype1.DataValueField = "ReqTypeID";
                ddltype1.DataTextField = "ReqTypeName";
                ddltype1.DataBind();
                ddltype1.SelectedIndex = 0;
            }
        }

        public void BindDdlLegalStatus()
        {
            using (LegalUnitOfWork unitofWork = new LegalUnitOfWork())
            {
                List<msLegalStatus> T = unitofWork.MSLegalStatusRep.GetLegalStatusList();
                ddltype2.DataSource = T;
                ddltype2.DataValueField = "ActionCode";
                ddltype2.DataTextField = "LegalStatusName";
                ddltype2.DataBind();
                ddltype2.SelectedIndex = 0;
            }
        }
        public void BindDdlDocumentType(DropDownList ddltype2,string status)
        {
            ddltype2.DataSource = null;

            using (DocTypeRep rep = new DocTypeRep())
            {
                List<LegalData.DocumentType> listp = new List<LegalData.DocumentType>();
                switch (status)
                {
                    //Add ExcepData Already Mapping
                    case "1":
                        listp = rep.BindDDLExceptData();
                        break;
                    //Edit Get All Data
                    case "2":
                        listp = rep.GetAll();
                        break;
                }
              
                listp.Add(new LegalData.DocumentType
                {
                    DocTypeID = 0,
                    DocTypeName = "- Please Select -"
                });

                ddltype2.DataSource = listp;
                ddltype2.DataValueField = "DocTypeID";
                ddltype2.DataTextField = "DocTypeName";
                ddltype2.DataBind();
                ddltype2.SelectedValue = "0";
            }
        }

        #endregion

        #region GridView Event
        protected void gvTypeMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvTypeMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTypeMapping.PageIndex = e.NewPageIndex;
            gvTypeMapping.DataSource = DtGritView;
            gvTypeMapping.DataBind();
        }

        protected void gvTypeMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "EditCommand")
            {
                CommandName = "Edit";
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;

                switch (ParamType)
                {
                    case "CT": // case: type1=dropdown type2=textbox
                    case "DM":
                        string DMtype1 = (gvTypeMapping.Rows[index].FindControl("hidType1ID") as HiddenField).Value.Trim();
                        string DMtype2 = (gvTypeMapping.Rows[index].FindControl("hidType2ID") as HiddenField).Value.Trim();
                        string DMHidmapID = (gvTypeMapping.Rows[index].FindControl("GridHidMapID") as HiddenField).Value.Trim();

                        BindDdlDocumentType(ddltype2, "2");

                        ddltype1.SelectedValue = DMtype1;
                        ddltype2.SelectedValue = DMtype2;
                        hidMapID.Value = DMHidmapID;
                        break;
                    case "OA":
                        string type2id = (gvTypeMapping.Rows[index].FindControl("hidType2ID") as HiddenField).Value.Trim();
                        string type1id = (gvTypeMapping.Rows[index].FindControl("hidType1ID") as HiddenField).Value.Trim();
                        string type1name = (gvTypeMapping.Rows[index].FindControl("lbType1Name") as Label).Text.Trim();

                        hidMapID.Value = type1id;
                        ddltype1.SelectedValue = type2id;
                        txtType2.Value = type1name;
                        break;
                    case "LM": // case : type1=dropdown type2=dropdown
                        string type1name_c2 = (gvTypeMapping.Rows[index].FindControl("hidType1ID") as HiddenField).Value.Trim();
                        string type2name_c2 = (gvTypeMapping.Rows[index].FindControl("hidType2ID") as HiddenField).Value.Trim();
                        string gHidmapID = (gvTypeMapping.Rows[index].FindControl("GridHidMapID") as HiddenField).Value.Trim();

                        ddltype2.SelectedValue = type1name_c2;
                        ddltype1.SelectedValue = type2name_c2;
                        hidMapID.Value = gHidmapID;
                        break;

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { openModal(); });", true);
            }
            else if (e.CommandName == "Delete")
            {
                switch (ParamType)
                {
                    case "CT":
                        using (CostTypeRep rep = new CostTypeRep())
                        {
                            GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                            int index = rindex.RowIndex;

                            string stypeid = (gvTypeMapping.Rows[index].FindControl("hidType2ID") as HiddenField).Value.Trim();
                            int typeid = Convert.ToInt32(stypeid);
                            rep.DeleteData(typeid);
                        }
                        break;
                    case "DM":
                        using (DocTypeMapRep rep = new DocTypeMapRep())
                        {
                            GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                            int index = rindex.RowIndex;

                            string gHidmapID = (gvTypeMapping.Rows[index].FindControl("GridHidMapID") as HiddenField).Value.Trim();
                            rep.DeleteData(new LegalData.DocumentTypeMapping
                            {
                                ID = Convert.ToInt32(gHidmapID)
                            });
                        }
                        break;
                    case "OA":
                        using (AttorneyRep rep = new AttorneyRep())
                        {
                            GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                            int index = rindex.RowIndex;

                            string gHidmapID = (gvTypeMapping.Rows[index].FindControl("GridHidMapID") as HiddenField).Value.Trim();
                            rep.DeleteData(Convert.ToInt32(gHidmapID));

                        }

                        break;
                    case "LM":
                        using (LegalStatusMappingRep rep = new LegalStatusMappingRep())
                        {
                            GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                            int index = rindex.RowIndex;

                            string gHidmapID = (gvTypeMapping.Rows[index].FindControl("GridHidMapID") as HiddenField).Value.Trim();
                            rep.DeleteData(Convert.ToInt32(gHidmapID));
                        }
                        break;
                }
            }

        }

        protected void gvTypeMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllaType();
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            SetModaltoDefault();
            txtType2.Value = string.Empty;
            ddltype1.SelectedValue = "0";
            ddltype2.SelectedValue = "0";
            BindDdlDocumentType(ddltype2, "1");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { openModal(); });", true);
        }

        private bool validateData()
        {
            bool result = true;

            if (txtType2.Visible == true && string.IsNullOrEmpty(txtType2.Value))
            {
                lbRed2.InnerText = "This field is required.";
                result = false;
            }
            else if (txtType1.Visible == true && string.IsNullOrEmpty(txtType1.Value))
            {
                lbRed1.InnerText = "This field is required.";
                result = false;
            }
            else if ((ddltype1.SelectedValue == "0" || string.IsNullOrEmpty(ddltype1.SelectedValue))
                && (ddltype2.SelectedValue == "0" || string.IsNullOrEmpty(ddltype2.SelectedValue)))
            {
                lbRed1.InnerText = "This field is required.";
                lbRed2.InnerText = "This field is required.";
                result = false;
            }
            else if (ddltype1.SelectedValue == "0" || string.IsNullOrEmpty(ddltype1.SelectedValue))
            {
                lbRed1.InnerText = "This field is required.";
                result = false;
            }
            else if (ddltype2.SelectedValue == "0" || string.IsNullOrEmpty(ddltype2.SelectedValue))
            {
                lbRed2.InnerText = "This field is required.";
                result = false;
            }


            return result;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validateData())
                {
                    return;
                }

                if (CommandName.Equals("Add"))
                {
                    switch (ParamType)
                    {
                        case "CT":
                            using (CostTypeRep rep = new CostTypeRep())
                            {
                                CostType ass = new CostType();
                                //ass.CostTypeID = Convert.ToInt32(hidID2.Value.Trim());
                                ass.AttorneyTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                ass.CostTypeName = txtType2.Value.Trim();
                                rep.insertData(ass);
                            }
                            break;
                        case "DM":
                            using (DocTypeMapRep rep = new DocTypeMapRep())
                            {
                                rep.InsertData(new LegalData.DocumentTypeMapping
                                {
                                    ReqTypeID = Convert.ToInt32(ddltype1.SelectedValue),
                                    DocTypeID = Convert.ToInt32(ddltype2.SelectedValue)
                                });
                            }
                            break;
                        case "OA":
                            using (AttorneyRep rep = new AttorneyRep())
                            {
                                rep.insertData(new Attorney
                                {
                                    AttorneyTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim()),
                                    LawyerName = txtType2.Value.Trim()
                                });
                            }
                            break;
                        case "LM":
                            using (LegalStatusMappingRep rep = new LegalStatusMappingRep())
                            {
                                LegalStatusMapping doctype = new LegalStatusMapping();
                                doctype.LegalStatus = ddltype2.SelectedValue.Trim();
                                doctype.ReqTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                rep.insertData(doctype);
                            }
                            break;
                    }
                }
                else if (CommandName.Equals("Edit"))
                {
                    int mid = Convert.ToInt32(hidMapID.Value.Trim());

                    switch (ParamType)
                    {                         
                        case "CT":
                            using (CostTypeRep rep = new CostTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidID2.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                CostType ass = new CostType();
                                ass.CostTypeID = id;
                                ass.AttorneyTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                ass.CostTypeName = txtType2.Value.Trim();
                                rep.insertData(ass);
                            }
                            break;
                        case "DM":
                            using (DocTypeMapRep rep = new DocTypeMapRep())
                            {
                                rep.UpdateData(new LegalData.DocumentTypeMapping
                                {
                                    ID = mid,
                                    ReqTypeID = Convert.ToInt32(ddltype1.SelectedValue),
                                    DocTypeID = Convert.ToInt32(ddltype2.SelectedValue)
                                });
                            }
                            break;
                        case "DT":
                            using (DocumentTypeRep rep = new DocumentTypeRep())
                            {
                                // delete
                                int id = Convert.ToInt32(hidID2.Value.Trim());
                                rep.DeleteData(id);

                                // insert
                                DocumentType doctype = new DocumentType();
                                doctype.DocTypeID = id;
                                doctype.ReqTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                doctype.DocTypeName = txtType2.Value.Trim();
                                rep.insertData(doctype);
                            }
                            break;
                        case "OA":
                            using (AttorneyRep rep = new AttorneyRep())
                            {
                                // delete
                                //int mid = Convert.ToInt32(hidMapID.Value.Trim());
                                rep.DeleteData(mid);
                                
                                // insert
                                rep.insertData(new Attorney
                                {
                                    AttorneyTypeID = Convert.ToInt32(ddltype1.SelectedValue.Trim()),
                                    LawyerName = txtType2.Value.Trim()
                                });
                            }
                               
                            break;
                        case "LM":
                            using (LegalStatusMappingRep rep = new LegalStatusMappingRep())
                            {
                                // delete
                                string ddlLegalStatus = ddltype2.SelectedValue.Trim();
                                int ddlReqType = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                rep.DeleteData(mid);

                                //string id1 = ddltype2.SelectedValue.Trim();
                                //int id2 = Convert.ToInt32(ddltype1.SelectedValue.Trim());
                                //rep.DeleteData(id1, id2);

                                //insert

                                rep.insertData(new LegalStatusMapping
                                {
                                    LegalStatus = ddlLegalStatus,
                                    ReqTypeID = ddlReqType
                                });
                            }
                            break;
                    }
                }
                GetAllaType();
                Msg.MsgBox(MessageEvent.Succesfully, MessageEventType.success, this, this.GetType());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CallModelPopup", "$(function() { closeModal(); });", true);
            }
            catch(Exception ex)
            {
                Msg.MsgBox(ex.Message, MessageEventType.error, this, this.GetType());
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            switch (ParamType)
            {
                case "CT":
                    using (vCostTypeRep rep = new vCostTypeRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        costtypeList = rep.GetCostTypeWithCondition(scondition);
                        CostTypeList = costtypeList;
                        BindGrid();
                        break;
                    }
                case "DM":
                    using (DocTypeMapRep rep = new DocTypeMapRep())
                    {
                        var p = rep.GetView().Where(a => a.DocTypeName.Contains(txtfilter.Value.Trim())
                                                || a.ReqTypeName.Contains(txtfilter.Value.Trim())).OrderBy(a => a.DocTypeName).ThenBy(a => a.ReqTypeID).ToList();

                        DtGritView = Utility.ConvertToDataTable(p);
                        DtGritView.Columns[0].ColumnName = "Type1ID";
                        DtGritView.Columns[1].ColumnName = "Type1Name";
                        DtGritView.Columns[2].ColumnName = "Type2ID";
                        DtGritView.Columns[3].ColumnName = "Type2Name";
                        DtGritView.Columns[4].ColumnName = "MapID";
                        gvTypeMapping.DataSource = DtGritView;
                        gvTypeMapping.DataBind();
                        break;
                    }
                case "OA":
                    using (vLawyerOAIARep rep = new vLawyerOAIARep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        OAIAList = rep.GetOAIAWithCondition(scondition);
                        OAIAList = OAIAList;
                        BindGrid();
                        break;
                    }
                case "LM":
                    using (vLegalStatusMappingRep rep = new vLegalStatusMappingRep())
                    {
                        string scondition = txtfilter.Value.Trim();
                        legalStatusMappingList = rep.GetAllLegalStatusMappingWithCondition(scondition);
                        LegalStatusMappingList = legalStatusMappingList;
                        BindGrid();
                        break;
                    }
            }
       
        }
        #endregion

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtfilter.Value = string.Empty;
        }
    }
}