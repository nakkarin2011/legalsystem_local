﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using LegalSystem.CommonHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TCRB.Securities;

namespace LegalSystem.Pages
{
    public partial class InboxPage : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        string requestType;
        string requestNo;
        string requestDate;
        string cif;
        string requestStatus;
        string legalStatus;
        string LegalNo;

        private List<sp_GetRequestInbox> RQs
        {
            get { return Session["ListRequest"] != null ? Session["ListRequest"] as List<sp_GetRequestInbox> : null; }
            set { Session["ListRequest"] = value; }
        }
        private List<sp_GetRequest> ListCaseEdit
        {
            get { return Session["ListCaseEdit"] != null ? Session["ListCaseEdit"] as List<sp_GetRequest> : null; }
            set { Session["ListCaseEdit"] = value; }
        }
        public UserIdentity UserLogin
        {
            get { return Session["UserLogin"] != null ? Session["UserLogin"] as UserIdentity : null; }
            set { Session["UserLogin"] = value; }
        }
        private string CaseEdit
        {
            get { return Request.QueryString["CaseEdit"] != null ? Request.QueryString["CaseEdit"] as string : null; }
            set { Request.QueryString["CaseEdit"] = value; }
        }

        #region Function
        private void GetDataUI()
        {
            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
            {
                List<msRequestType> RT = unitOfWork.MSRequestTypeRep.GetAllRequestType();
                ddlRequestType.DataSource = RT;
                ddlRequestType.DataBind();
                ddlRequestType.SelectedIndex = 0;

                ddlRequestStatus.DataSource = unitOfWork.MSRequestStatusRep.Get();
                ddlRequestStatus.DataBind();
                ddlRequestStatus.SelectedIndex = 0;

                ddlLegalStatus.DataSource = unitOfWork.MSLegalStatusRep.Get();
                ddlLegalStatus.DataBind();
                ddlLegalStatus.SelectedIndex = 0;
            }
        }

        private void SearchAll()
        {
            try
            {
                //Validate();
                int SearchType = 0; //กดSearchเลยหรือตอน Load

                if (ddlRequestType.SelectedValue == "0" && string.IsNullOrEmpty(txtRequestDate.Value) && string.IsNullOrEmpty(txtRequestNo.Value) && string.IsNullOrEmpty(txtCIF.Value) && ddlRequestStatus.SelectedValue == "0" && (ddlLegalStatus.SelectedValue == "0"))
                    SearchType = 0;
                else
                    SearchType = 1;

                requestType = (ddlRequestType.SelectedValue == "0") ? "" : ddlRequestType.SelectedValue;
                requestNo = txtRequestNo.Value.Trim();
                if (string.IsNullOrEmpty(txtRequestDate.Value))
                    requestDate = "";
                else
                {
                        var DateRequest = txtRequestDate.Value.Trim().Split('/');
                        requestDate = string.Format("{0}-{1}-{2}", Convert.ToInt32(DateRequest[2]) - 543, DateRequest[1], DateRequest[0]);
                }
                //requestDate = (string.IsNullOrEmpty(txtRequestDate.Value)) ? "" : Convert.ToDateTime(txtRequestDate.Value).AddYears(-543).ToString("yyyy-MM-dd");
                cif = txtCIF.Value.Trim();
                requestStatus = (ddlRequestStatus.SelectedValue == "0") ? "" : ddlRequestStatus.SelectedValue;
                legalStatus = (ddlLegalStatus.SelectedValue == "0") ? "" : ddlLegalStatus.SelectedValue;
                LegalNo = txtLegalNo.Value.Trim();
                using (LegalUnitOfWork unitOfwork = new LegalUnitOfWork())
                {
                    RQs = unitOfwork.LegalStoreProcedure.GetRequestInbox(requestType, requestNo, requestDate, cif, requestStatus, legalStatus, LegalNo, SearchType.ToString()).ToList();

                    switch (GlobalSession.GetPermission)
                    {
                        case GlobalSession.PermissionID.CollectionMaker:
                            RQs = RQs.Where(x => x.Owner == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F1"
                                                || x.RequestStatus == "R1" && x.Owner == GlobalSession.UserLogin.Emp_Code
                                                || x.RequestStatus == "5" && x.Owner == GlobalSession.UserLogin.Emp_Code).ToList();
                            break;
                        case GlobalSession.PermissionID.CollectionChecker:
                            RQs = RQs.Where(x => x.ColChecker == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F2" && x.ColCheckerApprove == null
                                              || x.Owner == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F1"
                                              || x.Owner == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "R1"
                                              || x.Owner == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "5"
                                              ).ToList();
                            break;
                        case GlobalSession.PermissionID.CollectionHead:
                            RQs = RQs.Where(x => x.RequestStatus == "F2" && x.ColHead == GlobalSession.UserLogin.Emp_Code).ToList();
                            break;
                        case GlobalSession.PermissionID.LegalSpecialist:
                            RQs = RQs.Where(x => x.RequestStatus == "F3" && x.SeniorLawyer == "0").ToList();
                            break;
                        case GlobalSession.PermissionID.SeniorLawyer:
                            RQs = RQs.Where(x => x.SeniorLawyer == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F4"
                                              /*|| x.SeniorLawyer == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F5"*/).ToList();

                            break;
                        case GlobalSession.PermissionID.Lawyer:
                            RQs = RQs.Where(x => x.LawyerID == GlobalSession.UserLogin.Emp_Code && x.RequestStatus == "F6").ToList();
                            break;
                        default: RQs.Clear(); break;
                    }

                    if (RQs.Count <= 0)
                    {
                        UcMessageBoxMain.Show(string.Format("{0}", MessageEvent.NoData), "P");
                        FSgvInquiry.Visible = false;
                    }
                    else
                    {
                        FSgvInquiry.Visible = true;
                        UcMessageBoxMain.VisibleControl();
                        if (CaseEdit == "Y")
                        {
                            titlePageInbox.InnerText = MasterPageTitleHelper.InnerRequest + " Inbox";
                            gvInquiry.DataSource = ListCaseEdit.OrderBy(o => o.RequestNo).ToList();
                        }
                        else
                            gvInquiry.DataSource = RQs.OrderBy(o => o.RequestNo).ToList();
                        gvInquiry.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "X");
            }
        }
        private void Validate()
        {
            if (string.IsNullOrEmpty(txtRequestNo.Value) && string.IsNullOrEmpty(txtCIF.Value) && string.IsNullOrEmpty(txtRequestDate.Value)
                && ddlRequestType.SelectedIndex == 0 && ddlRequestStatus.SelectedIndex == 0 && ddlLegalStatus.SelectedIndex == 0)
            //&& string.IsNullOrEmpty(txtCitizenID.Value) && string.IsNullOrEmpty(txtAccount.Value)
            {
                UcMessageBoxMain.Show(string.Format("{0}", new Exception()), "X");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDataUI();
                UcMessageBoxMain.VisibleControl();
                SearchAll();
            }
            else { }
            //dCardTable.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchAll();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlRequestType.SelectedIndex = 0;
            ddlRequestStatus.SelectedIndex = 0;
            ddlLegalStatus.SelectedIndex = 0;
            txtRequestNo.Value = txtRequestDate.Value = txtCIF.Value = string.Empty;
            FSgvInquiry.Visible = false;
            gvInquiry.DataSource = null;
            gvInquiry.DataBind();
        }

        protected void gvInquiry_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var control = ((Button)(e.Row.FindControl(MasterButtonHelper.btnDocument)));
                control.Visible = false;
            }
        }

        protected void gvInquiry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string URL = string.Empty;
            sp_GetRequestInbox R = RQs.Where(D => D.RequestNo == e.CommandArgument.ToString()).FirstOrDefault();
            if (e.CommandName != MasterECommandNameHelper.Page)
            {
                switch (e.CommandName)
                {
                    //case "3": URL = string.Format("{0}?{1}={2}", MasterPageHelper.LegalPage2,MasterParamPageHelper.ParamRID, R.RequestID); break;
                    case "F": URL = string.Format("CoverPage.aspx?rid={0}&type={1}", R.RequestID, e.CommandName); break;
                    case "C": URL = string.Format("CoverPage.aspx?rid={0}&status={1}", R.RequestID, R.IsGuarantee); break;
                    case "N": URL = string.Format("CoverPage.aspx?rid={0}&type={1}", R.RequestID, e.CommandName); break;
                    default:
                        switch (R.RequestStatus)
                        {
                            case MasterStatusHelper.StatusReturn /*R1*/:
                            case MasterStatusHelper.StatusDraft /*"F1"*/:
                            case MasterStatusHelper.StatusWaitingforApproved /*"F2"*/:
                            case MasterStatusHelper.StatusRegistered /*"F4"*/:
                            case MasterStatusHelper.StatusAssigned/*"F5"*/:
                            case MasterStatusHelper.StatusApprovedtoLawyer/*"F6"*/ :
                            case MasterStatusHelper.StatusAccepted/*"F7"*/ :
                                URL = string.Format("RequestPage.aspx?rid={0}", R.RequestID); break;
                            case MasterStatusHelper.StatusApproved /*"F3"*/:
                            case "5":

                                if (GlobalSession.GetPermission == GlobalSession.PermissionID.LegalSpecialist)
                                    URL = string.Format("RequestPage.aspx?rid={0}", R.RequestID);
                                else
                                    URL = string.Format("RequestPage.aspx?rno={0}", R.RequestNo);
                                break;
                        }
                        break;
                }
                Response.Redirect(URL);
            }
        }

        protected void gvInquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvInquiry.PageIndex = e.NewPageIndex;
            btnSearch_Click(null, null);
        }
    }
}