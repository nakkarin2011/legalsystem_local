﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LE=LegalEntities;
using System.ComponentModel;
using LegalService;

namespace LegalSystem.Pages.Setting
{
    public partial class CapitalRate : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.CapitalRate> capitalRateList;
        public List<LE.CapitalRate> CapitalRateList
        {
            get
            {
                return (List<LE.CapitalRate>)Session["CapitalRateList"];
            }
            set
            {
                Session["CapitalRateList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            GetCondition();
            GetAllCapitalRate();
        }

        #region Initial
        public void GetCondition()
        {
            ddlCondition.Items.Insert(0, new ListItem("-- Please Select --", ""));
            ddlCondition.Items.Insert(1, new ListItem("จ่ายขั้นต่ำ (>)", ">"));
            ddlCondition.Items.Insert(2, new ListItem("จ่ายไม่เกิน (<=)", "<="));
            ddlCondition.SelectedIndex = 0;
        }



        public void GetAllCapitalRate()
        {
            using (CapitalRateRep rep = new CapitalRateRep())
            {
                capitalRateList = rep.GetAllCapitalRate();
                CapitalRateList = capitalRateList;
                gvCapitalRate.DataSource = CapitalRateList;
                gvCapitalRate.DataBind();
            }
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            
            CommandName = "Add";
            txtMinWage.Text = string.Empty;
            txtMaxWage.Text = string.Empty;
            txtPercentage.Text = string.Empty;
            txtPayAmount.Text = string.Empty;

            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "openmodal", "openModal();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (CapitalRateRep rep = new CapitalRateRep())
                {
                    LE.CapitalRate cap = new LE.CapitalRate();
                    cap.Percentage = Convert.ToDecimal(txtPercentage.Text.Trim());
                    cap.MinWage = Convert.ToDecimal(txtMinWage.Text.Trim());
                    cap.MaxWage = Convert.ToDecimal(txtMaxWage.Text.Trim());
                    cap.Condition = ddlCondition.SelectedValue.Trim();
                    //cap.Condition = ddlCondition2.Value.Trim();
                    cap.PayAmount = Convert.ToDecimal(txtPayAmount.Text.Trim());

                    rep.insertData(cap);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (CapitalRateRep rep = new CapitalRateRep())
                {
                    // delete
                    int caprateID = Convert.ToInt32(hidTypeID.Value.Trim());
                    rep.DeleteData(caprateID);

                    // insert
                    LE.CapitalRate cap = new LE.CapitalRate();
                    cap.CapitalRateID = caprateID;
                    cap.MinWage = Convert.ToDecimal(txtMinWage.Text.Trim());
                    cap.MaxWage = Convert.ToDecimal(txtMaxWage.Text.Trim());
                    cap.Condition = ddlCondition.SelectedValue.Trim();
                    //cap.Condition = ddlCondition2.Value.Trim();
                    cap.PayAmount = Convert.ToDecimal(txtPayAmount.Text.Trim());
                    rep.insertData(cap);
                }
            }
            GetAllCapitalRate();
            UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel2, UpdatePanel2.GetType(), "closemodal", "closeModal();", true);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (CapitalRateRep rep = new CapitalRateRep())
            {
                string scondition = txtfilter.Value.Trim();

                capitalRateList = rep.GetCapitalRateWithCondition(scondition);
                CapitalRateList = capitalRateList;

                gvCapitalRate.DataSource = CapitalRateList;
                gvCapitalRate.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvCapitalRate_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllCapitalRate();
        }

        protected void gvCapitalRate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                btnDelete.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvCapitalRate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCapitalRate.PageIndex = e.NewPageIndex;
            gvCapitalRate.DataSource = CapitalRateList;
            gvCapitalRate.DataBind();
        }

        protected void gvCapitalRate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string capID = (gvCapitalRate.Rows[index].FindControl("hidCapID") as HiddenField).Value.Trim();
                string minwage = (gvCapitalRate.Rows[index].FindControl("lbMinWage") as Label).Text.Trim();
                string maxwage = gvCapitalRate.Rows[index].Cells[1].Text.Trim();
                string percentage = gvCapitalRate.Rows[index].Cells[2].Text.Trim();
                string condition = (gvCapitalRate.Rows[index].FindControl("lbCondition") as Label).Text.Trim();
                string payamount = gvCapitalRate.Rows[index].Cells[4].Text.Trim();

                
                // bind to controller
                hidTypeID.Value = capID;
                txtMinWage.Text = minwage;
                txtMaxWage.Text = maxwage;
                txtPercentage.Text = percentage;
                txtPayAmount.Text = payamount;
                ddlCondition.SelectedValue = condition;
                

                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "txt", "openModal();", true);
            }
            else if (e.CommandName == "Delete")
            {
                using (CapitalRateRep rep = new CapitalRateRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int capID = Convert.ToInt32((gvCapitalRate.Rows[index].FindControl("hidCapID") as HiddenField).Value.Trim());
                    rep.DeleteData(capID);
                }
                //GetAllCapitalRate();
                //Response.Redirect("CapitalRate.aspx");
                //UpdatePanel1.Update();
            }
        }
        #endregion
    }
}