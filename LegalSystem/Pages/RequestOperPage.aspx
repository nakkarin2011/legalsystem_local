﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="RequestOperPage.aspx.cs" Inherits="LegalSystem.Pages.RequestOperPage" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery1-1.12.0.min.js"></script>
    <script src="../Scripts/plugin/bootstrap-datepicker.js"></script>
    <%--<script src="../Scripts/plugin/bootstrap-datepicker.th.min.js"></script>--%>
    <script src="../Scripts/Site.js"></script>
    <script type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager11" EnablePageMethods="true" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12">
                    <uc1:ucmessagebox runat="server" id="UcMessageBoxMain" />
                    <div class="card ">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 class="card-title">
                                <asp:Label ID="lblHeader" runat="server" /></h4>
                        </div>
                        <div class="card-body">
                            <%--<div class="card">--%>
                            <div class="card-body">
                                <fieldset id="FSMenuNotOutsource" runat="server">
                                    <div class="row col-md-12" style="margin-top: -20px">
                                        <div class="col-md-auto col-sm-1 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Request Type :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-auto">
                                            <div class="form-group ">
                                                <%--<input type="text" id="txtReqType" runat="server" class="form-control" />--%>
                                                <asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control"  style="padding-left:1em;padding-right:1em;"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-auto col-sm-1 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">CIF :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-2 col-2 ">
                                            <div class="form-group">
                                                <input type="text" id="txtCIF" runat="server" class="form-control" style="padding-left:1em;"/>
                                            </div>
                                        </div>
                                        <div class="col-md-auto col-sm-1 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Name :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-2 col-2 ">
                                            <div class="form-group">
                                                <input type="text" id="txtName" runat="server" class="form-control" style="padding-left:1em;" />
                                            </div>
                                        </div>
                                        <div class="col-md-auto col-sm-1 text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Request No :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-2 col-2 ">
                                            <div class="form-group">
                                                <input type="text" id="txtRequestNo" runat="server" class="form-control" style="padding-left:1em;"/>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="card-footer m-auto" style="/*padding-bottom: 30px; */">
                                    <div class="row col-md-12" style="margin-top: -20px">
                                        <div id="divEmail1" runat="server" class="col-md-auto text-right">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">Email Send Status :</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div id="divEmail2" runat="server" class="col-md-2">
                                            <div class="form-group">
                                                <asp:DropDownList ID="ddlEmailSendStatus" runat="server" CssClass="form-control" style="padding-left:1em;padding-right:1em;"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div id="divOAIA1" runat="server" class="col-md-auto text-right" visible="false">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">OA/IA</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div id="divOAIA2" runat="server" class="col-md-auto" visible="false">
                                            <div class="form-group">
                                                <%--<input type="text" id="txtReqType" runat="server" class="form-control" />--%>
                                                <asp:DropDownList ID="ddlAttroney" runat="server" CssClass="form-control" style="padding-left:1em;padding-right:1em;"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div id="divRoundSend1" runat="server" class="col-md-auto text-right" visible="false">
                                            <div class="tim-typo mt-4-5">
                                                <h5>
                                                    <span class="tim-note">รอบนำส่งเอกสาร</span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div id="divRoundSend2" runat="server" class="col-md-1" visible="false">
                                            <div class="form-group">
                                                <input type="text" id="txtRoundSend" runat="server" class="form-control" style="padding-left:1em;" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-info" OnClick="btnSearch_Click" />
                                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-warning" OnClick="btnClear_Click" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <%--</div>--%>
                            <div id="bResult" runat="server" class="card " style="display: none; margin-top: -20px">
                                <div class="card-body table-responsive table-wrapper-scroll-y my-custom-scrollbar" style="overflow-y: auto">
                                    <asp:GridView ID="gvData" CellPadding="0" CellSpacing="0" runat="server" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                        AutoGenerateColumns="False" Font-Size="13px" Font-Strikeout="False" Font-Underline="False" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Top"
                                        EmptyDataText="No data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="GrayText" PagerStyle-HorizontalAlign="Center"
                                        AllowPaging="true" PageSize="10" Width="100%" HeaderStyle-HorizontalAlign="Center" ShowHeaderWhenEmpty="true"
                                        PagerSettings-FirstPageImageUrl="~/Images/first.png" PagerSettings-PreviousPageImageUrl="~/Images/previous.png"
                                        PagerSettings-NextPageImageUrl="~/Images/next.png" OnRowDataBound="gvData_RowDataBound" OnRowCommand="gvData_RowCommand" OnPageIndexChanging="gvData_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="...">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk" runat="server" />
                                                    <asp:HiddenField ID="hidSendDocDate" runat="server" Value='<%#Eval("SendDocDate") %>' />
                                                    <asp:HiddenField ID="hidRequestID" runat="server" Value='<%#Eval("RequestID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req No." DataField="RequestNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Account No." DataField="ACCTNO" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Type" DataField="RequestTypeName" ItemStyle-Width="10%" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="CIF" DataField="CifNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Name" DataField="CFNAME" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Date" DataField="RequestDate" DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal No." DataField="LegalNo" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" Visible="false" DataField="LegalStatus" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Legal Status" DataField="LegalStatusName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Address" DataField="Address" ItemStyle-Width="16%" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Lawyer" DataField="LawyerName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Assign OA" DataField="AttorneyName" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="เรื่อง" DataField="Topic" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Notice(ภายใน)" DataField="NoticeDate" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="กำหนดฟ้อง(ภายใน)" DataField="ActionIndictDate" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="ฟ้อง(ภายใน)" DataField="IndictDate" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="รอบที่ส่ง" DataField="SendRound" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่ส่งเอกสาร" DataField="SendDocDate" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Oper" DataField="StatusReqOper" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="วันที่ส่งเอกสาร" DataField="SendOutstandingDate" Visible="false" />
                                            <asp:BoundField HeaderStyle-Font-Bold="true" HeaderText="Req Oper" DataField="StatusReqOutstanding" Visible="false" />
                                            <asp:TemplateField ItemStyle-Width="5%" ControlStyle-Font-Size="Smaller">
                                                <ItemTemplate>
                                                    <%--Style="padding: 2px; width: 35px;"--%>
                                                    <asp:HyperLink ID="lnkPcName" runat="server" Text="View" Target="_blank" CssClass="btn btn-info btn-sm"
                                                        NavigateUrl='<%# "~/Pages/RequestPage.aspx?mode=view&rno=" + Eval("RequestNo") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <%--'<%# "msgDisp(" + Eval("LocationId") + ");" %>'--%>
                                </div>
                                <div class="row">
                                    <div class="card-footer m-auto" style="padding-bottom: 2em">
                                        <asp:Button runat="server" ID="btnSendMail" Text="Send Mail to OPeration" CssClass="btn btn-success" OnClientClick="ConfirmDialogReqOperPage(this)" OnClick="btnSendMail_Click" />&nbsp;
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
        <ProgressTemplate>
            <div class="loading-visible" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: white; opacity: 0.75;">
                <p>
                    <img style="padding: 10px; position: fixed; top: 50%; left: 50%;" src="../Resources/Preloader_4.gif" />
                </p>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
