﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="Tray1.aspx.cs" Inherits="LegalSystem.Pages.Tray.Tray1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 <style>

     select.form-control:not([size]):not([multiple]) {
        height:36px;
        color: #aaa;
     }

    /*.table thead tr th {
        font-size: 14px !important;
    }*/

      /*.dataTables_filter {
            display: none;
        }*/

       /*div.dataTables_wrapper {
            width: 100%;
            margin: 0 auto;
        }*/
 </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <div class="row hidde">
     <div class="col-lg-12">

         <div class="card ">
        
            <div class="card-header card-header-tabs card-header-info">
                <h4 id="trayname"  class="card-title">รับเรื่องดำเนินคดี (Tray 1)</h4>
            </div>
            <div class="card-body">
                  
                <div class="form-row">
                    <div class="col form-group">
                        <label class="bmd-label-floating">Req No</label>
                        <input type="text" class="form-control" name="RequestNo" id="RequestNo" />
                    </div>
                    <div class="col form-group">
                        <label class="bmd-label-floating">CIF No</label>
                        <input type="text" class="form-control" name="CIFNo" id="CIFNo" />
                    </div>
                    <div class="col  form-group">
                      <label class="bmd-label-floating">CIF Name</label>
                        <input type="text" class="form-control" name="CIFName" id="CIFName" />
                    </div>
              </div>
               
              <div class="form-row">
                    <div class="col form-group">
                        <label class="bmd-label-floating">Legal No</label>
                        <input type="text" class="form-control" name="LegalNo" id="LegalNo" />
                    </div>
                    <div class="col form-group">
                        <label class="bmd-label-floating">Legal Status</label>
                        <input type="text" class="form-control" name="LegalStatus" id="LegalStatus" />
                    </div>
                    <div class="col  form-group">
                        <label class="bmd-label-floating">SLA OA Send Date</label>
                        <input type="text" class="form-control" name="SLASendDate" id="SLASendDate" />
                    </div>
              </div>

             </div>
            <div class="card-footer" >
           
                 <div class="form-group mb">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-main btn-search" type="submit">Search</button>
                        <button class="mb-sm btn btn-outline-secondary" type="reset">Clear</button>
                    </div>
                </div>
            </div>
      
         </div>

    </div>
</div>

<div class="row">
     <div class="col-lg-12">
         <div class="card" style="margin-top:50px;">
            <div class="card-header card-header-tabs card-header-info">
                <h4 class="card-title">SEARCH RESULT</h4>
            </div>
            <div class="card-body">
                  
                   <div class="material-datatables table-responsive">
                   <table id="table-tray" class="table table-striped table-no-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>		
                            <th>Req No</th>
                            <th>Request Type ID</th>
                            <th>Request Type</th>
                            <th>CIF</th>
                            <th>Name</th>
                            <th>Legal No</th>
                            <th>Legal Status</th>
                            <th>SLA OA Send Date</th>
                            <th>Assign OA ID</th>
                            <th>Assign OA</th>
                            <th>Document Type ID</th>
                            <th>Document Type</th>
                            <th>Document Status ID</th>
                            <th>Document Status</th>
                            <th>วันที่ออกหนังสือ</th>
                            <th>Loan Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                    </table>
                    </div>
             </div>

            <div class="card-footer">
                
            </div>
         </div>
    </div>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">

     <script>

        $(document).ready(function () {
          
            var Tray = {};
            Tray.Form = {};
            Tray.Form.Table = $('#table-tray').DataTable({
                order: [[1, "desc"]],
               // scrollY: '70vh',
                scrollX: true,
                searching: true,
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "/Tray/Search",
                    "type": "POST"
                    //, "data": function (d) {
                    //     return JSON.stringify( d );
                    // }
                },
                columnDefs: [

                    { targets: 0,data: "RowNo",  orderable: false, width: 30},
                    { targets: 1,data: "RequestNo" },
                    { targets: 2,data: "RequestTypeID", "visible": false},
                    { targets: 3,data: "RequestTypeName" , width: 200 },
                    { targets: 4,data: "CIFNo"  },
                    { targets: 5,data: "CIFName" },
                    { targets: 6,data: "LegalNo" },
                    { targets: 7,data: "LegalStatusName" , width: 100},
                    //{ targets: 8, data: "SLASendDate", width: 100 },

                    {
                        targets: 8, data: "SLASendDate", width: 100, render: function (data, type, row, meta) {
                            if (data) {
                               
                                return moment(data).local().format('DD/MM/YYYY');
                            }
                            return data
                        }
                    },

                    { targets: 9,data: "AssignOAID" , "visible": false},
                    { targets: 10,data: "AssignOAName" },
                    { targets: 11,data: "DocumentTypeID" , "visible": false},
                    { targets: 12,data: "DocumentTypeName" },
                    { targets: 13,data: "DocumentStatusID" , "visible": false},
                    { targets: 14,data: "DocumentStatusName"},
                    { targets: 15,data: "DocumentDate" },
                    { targets: 16,data: "LoanType" },
                    {
                        targets: 17,
                        data: "RequestID",
                        width: 60,
                        render: function (data, type, row, meta) {
                            var html = '';
                            html += '<button type="button" class="mb-sm btn btn-outline-info" form-mode="edit"  onclick="location.href=\'/LegalSystem_DEV/Pages/LegalInfo.aspx?rid=' + row.RequestID + '\'" ><i class="icon-pencil "></i> View</button>';
 
                            return html;

                            return ""; 
                        }
                    },

                    { targets: 18,data: "RequestID" , "visible": false},
                    { targets: 19,data: "LegalID" , "visible": false}
                ]

                , fixedColumns: {
                    leftColumns: 3,
                    rightColumns: 19
                 }
            });

            Tray.Form.Search = function () {
                console.log("search processing...");

                $('#form1 :input').each(function () {
                    var input = $(this);
                    var key = input[0].name;

                    switch (key) {
                        case "RequestNo"    : Tray.Form.Table.columns(1).search($(this).val()); break;
                        case "CIFNo"        : Tray.Form.Table.columns(4).search($(this).val()); break;
                        case "CIFName"      : Tray.Form.Table.columns(5).search($(this).val()); break;
                        case "LegalNo"      : Tray.Form.Table.columns(6).search($(this).val()); break;
                        case "LegalStatus"  : Tray.Form.Table.columns(7).search($(this).val()); break;
                        case "SLASendDate"  : Tray.Form.Table.columns(8).search($(this).val()); break;
                    }
                });

                Tray.Form.Table.draw();
            }

            $("#form1").on('submit', function (e) {
                e.preventDefault();

                setTimeout(function () {
                    Tray.Form.Search();
                }, 100);

            });

            $("#form1").on('reset', function (e) {

                setTimeout(function () {

                    $(".is-filled").removeClass("is-filled");

                    Tray.Form.Search();

                }, 100);
             
            });
       
        });

    </script>

</asp:Content>
