﻿using LegalEntities;
using LegalService;
using LegalSystem.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TCRB.Securities;
using System.Data.SqlClient;

namespace LegalSystem.Pages
{
    public partial class InquiryPage : System.Web.UI.Page
    {
        AlertMessage Msg = new AlertMessage();
        string DateRequest;
        private List<sp_GetRequest> RQs
        {
            get { return Session["ListRequest"] != null ? Session["ListRequest"] as List<sp_GetRequest> : null; }
            set { Session["ListRequest"] = value; }
        }
        public UserIdentity UserLogin
        {
            get { return Session["UserLogin"] != null ? Session["UserLogin"] as UserIdentity : null; }
            set { Session["UserLogin"] = value; }
        }

        #region Function
        private void GetDataUI()
        {
            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
            {
                List<msRequestType> RT = unitOfWork.MSRequestTypeRep.GetAllRequestType();
                ddlRequestType.DataSource = RT;
                ddlRequestType.DataBind();
                ddlRequestType.SelectedIndex = 0;

                ddlRequestStatus.DataSource = unitOfWork.MSRequestStatusRep.Get();
                ddlRequestStatus.DataBind();
                ddlRequestStatus.SelectedIndex=0;

                ddlLegalStatus.DataSource =unitOfWork.MSLegalStatusRep.Get();
                ddlLegalStatus.DataBind();
                ddlLegalStatus.SelectedIndex = 0;               
            }
            
        }

        private void Validate()
        {
            if (string.IsNullOrEmpty(txtRequestNo.Value) && string.IsNullOrEmpty(txtCIF.Value) && string.IsNullOrEmpty(txtRequestDate.Value)
                && ddlRequestType.SelectedIndex == 0 && ddlRequestStatus.SelectedIndex == 0 && ddlLegalStatus.SelectedIndex == 0)
            //&& string.IsNullOrEmpty(txtCitizenID.Value) && string.IsNullOrEmpty(txtAccount.Value)
            {
                throw new Exception("");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetDataUI(); dCardtable.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dCardtable.Visible = true;
                using (LegalDataContext rep = new LegalDataContext())
                {
                    if (!string.IsNullOrEmpty(txtRequestDate.Value.Trim()))
                    {
                        string[] Date = txtRequestDate.Value.Trim().Split('/');
                        DateRequest = string.Format("{0}-{1}-{2}", Convert.ToInt32(Date[2]) - 543, Date[1], Date[0]);
                    }
                    RQs = rep.GetRequest((ddlRequestType.SelectedValue == "0") ? "" : ddlRequestType.SelectedValue
                            , txtRequestNo.Value.Trim()
                            , (string.IsNullOrEmpty(txtRequestDate.Value)) ? "" : DateRequest
                            , txtCIF.Value.Trim()
                            , (ddlRequestStatus.SelectedValue == "0") ? "" : ddlRequestStatus.SelectedValue
                            , (ddlLegalStatus.SelectedValue == "0") ? "" : ddlLegalStatus.SelectedValue).ToList();                    
                    
                    if (RQs.Count < 1)
                    {
                        UcMessageBoxMain.Show(string.Format("{0}", "ไม่พบข้อมูล"), "P");
                        gvInquiry.Visible = false;
                    }
                    else
                    {
                        gvInquiry.Visible = true;
                        gvInquiry.DataSource = RQs.Where(w => !w.RequestNo.Contains("R")).OrderBy(o => o.RequestNo).ToList(); /*RQs; OLD*/                                                                     //gvInquiry.DataSource = RQs.OrderBy(o => o.RequestNo).Where(w => w.RequestStatus != "F3"); /*RQs; OLD*/
                        gvInquiry.DataBind();
                        UcMessageBoxMain.VisibleControl();
                    }
                }
            }
            catch (Exception ex)
            {
                UcMessageBoxMain.Show(string.Format("{0}", ex.Message), "X");
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ddlRequestType.SelectedIndex = 0;
            ddlRequestStatus.SelectedIndex = 0;
            ddlLegalStatus.SelectedIndex = 0;
            txtCIF.Value = txtRequestNo.Value = txtRequestDate.Value = string.Empty;
            gvInquiry.Visible = false;
            gvInquiry.DataSource = null;
            gvInquiry.DataBind();
            //tbdInquiry.InnerHtml = string.Empty;
        }

        protected void gvInquiry_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    sp_GetRequest R = e.Row.DataItem as sp_GetRequest;
            //    if(R)
            //    var control = ((HtmlInputCheckBox)(e.Row.FindControl("chkAccount")));
            //}
        }

        //*** BtnView ***//
        protected void gvInquiry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string URL = string.Empty;
            //if (e.CommandName == "3")// && GlobalSession.UserLogin.Permision.Contains("") กรณีเป็นTeam Lagal
            //    URL = string.Format("LegalPage.aspx?rno={0}", e.CommandArgument.ToString());
            //else
                //URL = string.Format("RequestPage.aspx?rno={0}", e.CommandArgument.ToString());

            
                URL = string.Format("RequestPage.aspx?rno={0}", e.CommandArgument.ToString());
            if(e.CommandName != "Page")
            Response.RedirectPermanent(URL);


        }

        protected void gvInquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvInquiry.PageIndex = e.NewPageIndex;
            btnSearch_Click(null,null);            
        }
    }
}