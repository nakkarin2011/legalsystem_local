﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="CoverPage.aspx.cs" Inherits="LegalSystem.Pages.CoverPage" %>

<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/plugin/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/plugin/bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <link href="../Content/vendor/material-dashboard.css" rel="stylesheet" />
    <script src="../Scripts/vendor/material-dashboard.min.js"></script>
    <script src="../Scripts/windows.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <uc1:ucMessageBox runat="server" ID="UcMessageBoxMain" />
                    <div class="card ">
                        <div class="card-header card-header-tabs card-header-info">
                            <h4 class="card-title">Cover Page</h4>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Coverpage Group :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control" DataValueField="CovTypeID" DataTextField="CovTypeName" AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="row">
                                <div class="col-md-2 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Coverpage Approver :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlApprover" runat="server" CssClass="form-control" DataValueField="EmpCode" DataTextField="EmpName"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvDocument" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                        DataKeyNames="DocTypeID" AllowPaging="false" AllowSorting="false" ShowHeader="false"
                                        OnRowCommand="gvDocument_RowCommand" OnRowDataBound="gvDocument_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" id="chkDocument" runat="server"><span class="form-check-sign"><span class="check"></span></span></label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DocTypeName" HeaderText="Coverpage Text"></asp:BoundField>
                                            <asp:BoundField DataField="CoverpageText" HeaderText="Format"></asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnAdditionDoc" runat="server" Text="Add" class="btn btn-fill btn-info" CommandArgument='<%# Eval("DocTypeID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Comment :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtComment" runat="server" CssClass="form-control" TextMode="MultiLine" Height="150px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div id="divFooterPreview" runat="server" class="m-auto" visible="true">
                                    <asp:Button ID="btnPreview" runat="server" Text="Preview PDF" CssClass="btn btn-info" OnClick="btnPreview_Click" />
                                    &nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="btnSubmit_Click" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel1" runat="server" Text="Cancel" CssClass="btn btn-default" PostBackUrl="~/Pages/InquiryPage.aspx" />
                                </div>
                                <div id="divFooterPrint" runat="server" class="m-auto" visible="false">
                                    <asp:Button ID="btnPrint" runat="server" Text="Print PDF" CssClass="btn btn-success" OnClick="btnPrint_Click" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel2" runat="server" Text="Cancel" CssClass="btn btn-default" PostBackUrl="~/Pages/InquiryPage.aspx" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlGroup" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="loading-visible">
                        <p>
                            <img src="../Images/ProgressBar2.gif" alt="" />
                        </p>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>

    <div id="AdditionDocModal" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel">
        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="modal-dialog">
                    <%-- modal-lg--%>
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:Label ID="lblName" runat="server" Font-Bold="true" Text=""></asp:Label>
                                        <asp:Label ID="txtName" runat="server" Visible="false" Text=""></asp:Label>
                                        <asp:HiddenField ID="hidName" runat="server" Value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divOriginalCopy" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right"></div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <asp:DropDownList ID="ddlOriginalCopy" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="ต้นฉบับ" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="สำเนา"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="คู่ฉบับ"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row" id="divNumOrder" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">ลำดับ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input id="txtNumOrder" runat="server" class="form-control" type="number" value="1" min="1" max="100" step="1" required />
                                        <%--<input type="text" id="txtNumOrder" runat="server" class="form-control" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNumRound" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">ครั้งที่ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtNumRound" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNumEdition" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">จำนวนฉบับ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtNumEdition" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNumDeed" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">จำนวนโฉนด :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <%--<input id="txtNumDeed" runat="server" class="form-control" type="number" value="1" min="1" max="100" step="1" required />--%>
                                        <input type="text" id="txtNumDeed" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNumPlot" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">จำนวนแปลง :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtNumPlot" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNumSuite" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">จำนวนห้องชุด :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtNumSuite" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divTypeDeed" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">ประเภทที่ดิน :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <asp:DropDownList ID="ddlTypeDeed" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="น.ส.3 ก." Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="น.ส.3"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="โฉนดตราจอง"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row" id="divNumber" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">เลขที่ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtNumber" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divDateAs" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">ลงวันที่ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtDateAs" runat="server" class="datepicker form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divDateAsCount" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">ลงวันที่ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" id="txtDateAsCount" runat="server" class="datepicker form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Button ID="btnAddDateAs" runat="server" Text="Add" class="btn btn-fill btn-info" OnClick="btnAddDateAs_Click" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <asp:HiddenField ID="hidLastSeq" runat="server" />
                                            <asp:GridView ID="gvDateAsCount" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                DataKeyNames="id" AllowPaging="false" AllowSorting="false" ShowHeader="false" OnRowCommand="gvDateAsCount_RowCommand">
                                                <Columns>
                                                    <asp:BoundField DataField="DateAsC" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDeleteDateAs" runat="server" Text="Delete" class="btn btn-fill btn-danger" CommandArgument='<%# Eval("id") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divDeedNo" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">โฉนดเลขที่ :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <input type="text" id="txtDeedNo" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divFreeText" runat="server" visible="false">
                                <div class="col-md-4 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Free Text :</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-7 col-sm-1 col-1 ">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtFreeText" runat="server" class="form-control" TextMode="MultiLine" Height="150px"></asp:TextBox>
                                        <%--<input type="text" id="txtFreeText" runat="server" class="form-control" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSaveDoc" runat="server" Text="Save" class="btn btn-success pull-right ml-3" OnClick="btnSaveDoc_Click" />
                            &nbsp;
                            <button type="button" class="btn btn-danger pull-right ml-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="PreviewModal" class="modal fade form-control-lg" role="dialog" aria-labelledby="myLargeModalLabel">
        <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="modal-dialog modal-lg">
                    <%-- modal-lg--%>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="card-title">Cover Page - Preview</h4>
                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <iframe id="iPDF" runat="server" scrolling="auto" width="100%" height="100%"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="fade" class="black_overlay"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script src="../Scripts/plugin/bootstrap-datepicker.th.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //Binding Code
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $(document).ready(function () {
                    $('.datepicker').datepicker({
                        showOn: 'button',
                        buttonImageOnly: true,
                        buttonImage: 'calendar.png',
                        format: 'dd/mm/yyyy',
                        todayBtn: false,
                        language: 'th-th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                        thaiyear: true              //Set เป็นปี พ.ศ.
                    })//.datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
                });
            }
        });

        function checkChange(DocTypeID) {
            PageMethods.gvDocumentUpdate(DocTypeID);
        }
    </script>
</asp:Content>
