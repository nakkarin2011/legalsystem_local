﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="ProductDetailPage.aspx.cs" Inherits="LegalSystem.Pages.Master.ProductDetailPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="formProduct" class="card card-nav-tabs">
        <h4 class="card-header card-header-info"> 
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#product" data-toggle="tab">Master Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#detail" data-toggle="tab">Master Product Detail</a>
                    </li>          
                </ul>
            </div></h4>
        <div class="card-body">
             <div class="tab-content text-center">
                <div class="tab-pane active" id="product">
                  <div class="row " style="margin-bottom: -20px;">
                       <div class="col-md-2 col-sm-1 text-right">
                       <div class="tim-typo mt-4-5">
                           <h5>
                               <span class="tim-note">Product Type Name</span>
                           </h5>
                       </div>
                   </div>
                      <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                          <div class="form-group">
                              <input id="strProdTypeName" type="text" class="form-control col-sm-8" v-model="strProdTypeName" />
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <button id="btnserch" type="button" class="btn btn-info" v-on:click="SearchData"><i class="fa fa-search"></i>&nbsp;Search</button>
                              &nbsp;
                           <button id="btnClear" type="button" class="btn btn-fill btn-warning" v-on:click="ResetData(1)"><i class="fa fa-refresh"></i>&nbsp;Clear</button>
                              &nbsp;
                           <button id="btnAdd" type="button" class="btn btn-success" v-on:click="addData(1)"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
                          </div>
                      </div>
                  </div>
                    <div class="row">
                        <div class="card ">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="material-datatables table-responsive">
                                        <table id="gvProduct" class="table table-striped table-no-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="font-weight: bold;">Code</th>
                                                    <th style="font-weight: bold;">Name</th>
                                                    <th style="font-weight: bold;">Description</th>
                                                    <th style="font-weight: bold;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
               
                </div>
                <div class="tab-pane" id="detail">
                    <div class="row">
                                <div class="col-md-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Product</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                         <select class="form-control" v-model="PrdTypeID">
                                            <option :value="0">-- Please Select --</option>
                                            <option v-for="option in listProdType" v-bind:value="option.Value">{{ option.Text }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Code</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-1 ">
                                    <div class="form-group bmd-form-group">
                                          <input id="strSubTypeCode" type="text" class="form-control col-sm-8" v-model="SubTypeCode" />
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Name</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                    <div class="form-group bmd-form-group">
                                       <input id="strSubTypeName" type="text" class="form-control col-sm-8" v-model="SubTypeName" />
                                    </div>
                                </div>
                                
                            </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <button id="btnserch" type="button" class="btn btn-info" v-on:click="SearchData"><i class="fa fa-search"></i>&nbsp;Search</button>
                                &nbsp;
                                   <button id="btnClear" type="button" class="btn btn-fill btn-warning" v-on:click="ResetData(2)"><i class="fa fa-refresh"></i>&nbsp;Clear</button>
                                &nbsp;
                                   <button id="btnAdd" type="button" class="btn btn-success" v-on:click="addData(2)"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="card ">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="material-datatables table-responsive">
                                        <table id="gvProductDetail" class="table table-striped table-no-bordered table-hover" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th style="font-weight: bold;">Product</th>
                                                    <th style="font-weight: bold;">Code</th>
                                                    <th style="font-weight: bold;">Name</th>
                                                    <th style="font-weight: bold;">Description</th>
                                                    <th style="font-weight: bold;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
             <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ modalTitle }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p v-if="errors.length">
                            <b class="text-danger">Please correct the following error(s):</b>
                            <ul>
                                <li v-for="error in errors" class="text-danger">{{ error }}</li>
                            </ul>
                        </p>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Product Type Name *</i></div>
                                </div>
                                <input type="text" v-model="PrdTypeName" class="form-control">
                            </div>
                        </div>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Description *</i></div>
                                </div>
                                <input type="text" v-model="PrdTypeDesc" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" v-on:click="submitFormData(1)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="DetailModalLabel">{{ modalTitle }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p v-if="errors.length">
                            <b class="text-danger">Please correct the following error(s):</b>
                            <ul>
                                <li v-for="error in errors" class="text-danger">{{ error }}</li>
                            </ul>
                        </p>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Product *</i></div>
                                </div>

                                 <select class="form-control" v-model="data_temp.PrdTypeID">
                                            <option :value="0">-- Please Select --</option>
                                            <option v-for="option in listProdTypeDetail" v-bind:value="option.Value">{{ option.Text }}
                                            </option>
                                 </select>
                            </div>
                        </div>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Code *</i></div>
                                </div>
                                <input type="text" v-model="data_temp.SubTypeCode" class="form-control">
                            </div>
                        </div>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Name *</i></div>
                                </div>
                                <input type="text" v-model="data_temp.SubTypeName" class="form-control">
                            </div>
                        </div>
                         <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Description</i></div>
                                </div>
                                <input type="text" v-model="data_temp.SubTypeDesc" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" v-on:click="submitFormData(2)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

       


</div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script type="text/javascript">
        var tableSearch;
        var tableSearchDetail;
        var app = new Vue({
            el: '#formProduct',
            data: function () {
                return {
                    modalTitle: '',
                    status: '',
                    strProdTypeName: '',
                    PrdTypeID: '0',
                    PrdTypeName: '',
                    PrdTypeDesc: '', 
                    SubTypeCode: '',
                    SubTypeName: '',
                    SubTypeDesc: '',
                    errors: [],
                    listProdType: [],
                    listProdTypeDetail: [],
                    data_temp: {},
                    model_create: {
                        PrdTypeDetailID: '',
                        PrdTypeID: '0',
                        SubTypeCode: '',
                        SubTypeName: '',
                        SubTypeDesc: '',
                    }

                }
            },
            methods: {
                ResetData: function (tab) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            self.strProdTypeName = '';
                            self.PrdTypeID = '';
                            self.PrdTypeName = '';
                            self.PrdTypeDesc = '';
                            self.errors = [];
                            break;
                        case 2:
                            self.PrdTypeID = '0';
                            self.SubTypeCode = '';
                            self.SubTypeName = '';
                            self.SubTypeDesc = '';
                            self.errors = [];
                            _.assign(self.data_temp, {});
                            break;
                    }
                },
                addData: function (tab) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            self.modalTitle = "Add New Product Type";
                            self.status = 'Add';
                            self.ResetData(1);
                            $("#exampleModal").modal('show');
                            break;
                        case 2:
                            self.modalTitle = "Add New Product Type Detail";
                            self.status = 'Add';
                            self.ResetData(2);
                            _.assign(self.data_temp, self.model_create);
                            $("#DetailModal").modal('show');
                            break;
                    }
                },
              
                editData: function (tab, ob) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            self.modalTitle = "Edit Product Type";
                            self.status = 'Edit';
                            self.errors = [];

                            $.ajax({
                                type: "POST",
                              url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/GetProductTypeByID",
                                data: "{'PrdTypeID':" + ob + "}",
                                contentType: "application/json",
                                datatype: "json",
                                success: function (res) {
                                    var obj = JSON.parse(res.d);
                                    self.PrdTypeID = obj.PrdTypeID;
                                    self.PrdTypeName = obj.PrdTypeName;
                                    self.PrdTypeDesc = obj.PrdTypeDesc;
                                    console.log(obj);
                                }
                            });

                            $("#exampleModal").modal('show');
                            break;
                        case 2:
                            self.modalTitle = "Edit Product Type Detail";
                            self.status = 'Edit';
                            self.errors = [];
                            _.assign(self.data_temp, ob);
                            $("#DetailModal").modal('show');
                            break;
                    }
                },
                deleteData: function (tab, id) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            var r = confirm("Are you sure to delete this item?");
                            if (r == true) {
                                $.ajax({
                                    type: "POST",
                                  url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/DeleteProductType",
                                    data: "{'PrdTypeID':" + id + "}",
                                    contentType: "application/json",
                                    datatype: "json",
                                    success: function (res) {
                                        console.log(res.d);
                                        toastr.error(res.d);
                                        tableSearch.fnDraw();
                                    }
                                });
                            }
                            break;
                        case 2:
                            var r = confirm("Are you sure to delete this item?");
                            if (r == true) {
                                $.ajax({
                                    type: "POST",
                                  url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/DeleteProductTypeDetail",
                                    data: "{'PrdTypeDetailID':" + id + "}",
                                    contentType: "application/json",
                                    datatype: "json",
                                    success: function (res) {
                                        console.log(res.d);
                                        toastr.error(res.d);
                                        tableSearchDetail.fnDraw();
                                    }
                                });
                            }
                            break;
                    }

                },
                submitFormData: function (tab) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            if (self.validateForm(1)) {
                                $.ajax({
                                    type: "POST",
                                  url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/SaveChangeData",
                                    data: JSON.stringify({
                                        PrdTypeID: self.PrdTypeID,
                                        PrdTypeName: self.PrdTypeName,
                                        PrdTypeDesc: self.PrdTypeDesc,
                                        status: self.status
                                    }),
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (res) {
                                        console.log(res.d);
                                        toastr.success(res.d);
                                        self.ResetData(1);
                                        $("#exampleModal").modal('hide');
                                        tableSearch.fnDraw();
                                    }
                                });
                            }
                            break;
                        case 2:
                            if (self.validateForm(2)) {

                                $.ajax({
                                    type: "POST",
                                  url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/SaveChangeDataDetail",
                                    data: JSON.stringify({
                                        PrdTypeDetailID: self.data_temp.PrdTypeDetailID,
                                        PrdTypeID: self.data_temp.PrdTypeID,
                                        SubTypeCode: self.data_temp.SubTypeCode,
                                        SubTypeName: self.data_temp.SubTypeName,
                                        SubTypeDesc: self.data_temp.SubTypeDesc,
                                        status: self.status
                                    }),
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (res) {
                                        console.log(res.d);
                                        toastr.success(res.d);
                                        self.ResetData(2);
                                        $("#DetailModal").modal('hide');
                                        tableSearchDetail.fnDraw();
                                    }
                                });
                            }
                            break;
                    }
                },
                validateForm: function (tab) {
                    let self = this;
                    //1 = Tab Master Product 2 = Tab Master Product Detail
                    switch (tab) {
                        case 1:
                            self.errors = [];
                            if (self.PrdTypeName === '') {
                                self.errors.push("Product Type Name is required.");
                            }
                            if (self.PrdTypeDesc === '') {
                                self.errors.push("Description is required.");

                            }
                            break;
                        case 2:
                            self.errors = [];

                            if (self.data_temp.PrdTypeID === '0') {
                                self.errors.push("Product is required.");
                            }
                            if (self.data_temp.SubTypeCode === '') {
                                self.errors.push("Code is required.");
                            }
                            if (self.data_temp.SubTypeName === '') {
                                self.errors.push("Name is required.");
                            }

                            break;
                    }

                    if (!self.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }

                },
                SearchData: function () {
                    tableSearch.fnDraw();
                    tableSearchDetail.fnDraw();
                },
                getListProductType: function () {
                    let self = this;
                    $.ajax({
                        type: "POST",
                      url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/GetListProductType",
                        data: "",
                        contentType: "application/json",
                        datatype: "json",
                        success: function (res) {
                            var obj = JSON.parse(res.d);
                            self.listProdType = obj;
                            self.listProdTypeDetail = obj;
                        }
                    });
                }
            },
            mounted() {
                let self = this;
                self.getListProductType();

                tableSearch = $("#gvProduct").dataTable({
                    lengthChange: false,
                    processing: false,
                    searching: false,
                    pageLength: 10,
                    serverSide: true,
                    responsive: true,
                    ajax: {
                        contentType: "application/json; charset=utf-8",
                      url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/InqueryProductType",
                        type: "POST",
                        data: function (d) {
                            console.log(d);

                            return JSON.stringify({
                                option: d, strProdTypeName: self.strProdTypeName
                            });
                        },
                        dataFilter: function (res) {
                            var parsed = JSON.parse(res);
                            console.log(parsed.d);
                            return JSON.stringify(parsed.d);
                        },
                        beforeSend: function (d) {
                            console.log(d);
                            $(".se-pre-con").show();
                        },
                        complete: function (d) {
                            console.log(d);
                            $(".se-pre-con").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(thrownError);
                        }
                    },
                    columns: [
                        { data: "PrdTypeID" },
                        { data: "PrdTypeName" },
                        { data: "PrdTypeDesc" },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="edit" class="btn btn-info" href="javascript:;" data-id="' + o.PrdTypeID + '"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>'
                                    + '&nbsp;&nbsp;<a id="delete" class="btn btn-danger" href="javascript:;" data-id="' + o.PrdTypeID + '"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</a>';
                            }
                        },

                    ],
                    order: [
                        [1, "asc"]
                    ],

                }).on('click', '#edit', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.editData(1,dataRow.PrdTypeID);
                }).on('click', '#delete', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.deleteData(1,dataRow.PrdTypeID);
                });

                tableSearchDetail = $("#gvProductDetail").dataTable({
                    lengthChange: false,
                    processing: false,
                    searching: false,
                    pageLength: 10,
                    serverSide: true,
                    responsive: true,
                    ajax: {
                        contentType: "application/json; charset=utf-8",
                      url: "/LegalSystem_DEV/Controllers/Master/wsProductType.asmx/InqueryProductTypeDetail",
                        type: "POST",
                        data: function (d) {
                            console.log(d);

                            return JSON.stringify({
                                option: d, PrdTypeID: self.PrdTypeID, SubTypeCode: self.SubTypeCode, SubTypeName: self.SubTypeName

                            });
                        },
                        dataFilter: function (res) {
                            var parsed = JSON.parse(res);
                            console.log(parsed.d);
                            return JSON.stringify(parsed.d);
                        },
                        beforeSend: function (d) {
                            console.log(d);
                            $(".se-pre-con").show();
                        },
                        complete: function (d) {
                            console.log(d);
                            $(".se-pre-con").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(thrownError);
                        }
                    },
                    columns: [
                        { data: "PrdTypeName" },
                        { data: "SubTypeCode" },
                        { data: "SubTypeName" },
                        { data: "SubTypeDesc" },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="edit" class="btn btn-info" href="javascript:;" data-id="' + o.PrdTypeID + '"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>'
                                    + '&nbsp;&nbsp;<a id="delete" class="btn btn-danger" href="javascript:;" data-id="' + o.PrdTypeID + '"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</a>';
                            }
                        },

                    ],
                    order: [
                        [1, "asc"]
                    ],

                }).on('click', '#edit', 'tr', function () {
                    var dataRow = tableSearchDetail.fnGetData($(this).parents('tr')[0]);
                    console.log(dataRow);
                    self.editData(2, dataRow);
                }).on('click', '#delete', 'tr', function () {
                    var dataRow = tableSearchDetail.fnGetData($(this).parents('tr')[0]);
                    self.deleteData(2, dataRow.PrdTypeDetailID);
                });
            },
        });
       
</script>
</asp:Content>

