﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="CourtZonePage.aspx.cs" Inherits="LegalSystem.Pages.Master.CourtZonePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="frm">
        <div class="card ">
            <div class="card-header card-header-tabs card-header-info">
                <h4 class="card-title" id="TypeHeader" runat="server">Court Zone</h4>
            </div>
            <div class="card-body">
                <div id="form_inquiry" class="row " style="margin-bottom: -20px;">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Court Zone Name</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                        <div class="form-group">
                            <input id="strSearch" type="text" class="form-control col-sm-8" v-model="strSearch" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button id="btnserch" type="button" class="btn btn-info"  v-on:click="SearchData();"><i class="fa fa-search"></i>&nbsp;Search</button>
                            &nbsp;
                           <button id="btnClear" type="button" class="btn btn-fill btn-warning"  v-on:click="ResetData();"><i class="fa fa-refresh"></i>&nbsp;Clear</button>
                            &nbsp;
                           <button id="btnAdd" type="button" class="btn btn-success" v-on:click="addData();"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card ">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="material-datatables table-responsive">
                                    <table id="gv" class="table table-striped table-no-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="font-weight: bold;">Code</th>
                                                <th style="font-weight: bold;">Name</th>
                                                <th style="font-weight: bold;">Description</th>
                                                <th style="font-weight: bold;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <!-- Modal -->
        <div class="modal fade" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ modalTitle }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p v-if="errors.length">
                            <b class="text-danger">Please correct the following error(s):</b>
                            <ul>
                                <li v-for="error in errors" class="text-danger">{{ error }}</li>
                            </ul>
                        </p>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Court Zone Name *</i></div>
                                </div>
                                <input type="text" v-model="data_temp.CourtZoneName" class="form-control">
                            </div>
                        </div>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Court Zone Description</i></div>
                                </div>
                                <input type="text" v-model="data_temp.CourtZoneDesc" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" v-on:click="submitFormData()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
     <script type="text/javascript">
        var tableSearch;
        var app = new Vue({
            el: '#frm',
            data: function () {
                return {
                    modalTitle: '',
                    status: '',
                    errors: [],
                    strSearch: '',
                    data_temp: {},
                    model_create: {
                        CourtZoneID: '',
                        CourtZoneName: '',
                        CourtZoneDesc: ''
                    }
                }
            },
            methods: {
                ResetData: function () {
                    let self = this;

                    self.errors = [];
                    self.strSearch = '';
                    _.assign(self.data_temp, {});

                },
                addData: function () {
                    let self = this;

                    self.modalTitle = "Add New Court Zone";
                    self.status = 'Add';
                    self.ResetData();
                    _.assign(self.data_temp, self.model_create);
                    $("#frmModal").modal('show');
                },
                editData: function (obj) {
                    let self = this;

                    self.modalTitle = "Edit Court Zone";
                    self.status = 'Edit';
                    self.errors = [];
                    _.assign(self.data_temp, obj);
                    $("#frmModal").modal('show');
                },
                deleteData: function (id) {
                    var r = confirm("Are you sure to delete this item?");
                    if (r == true) {
                        _Ajax("/LegalSystem_DEV/Controllers/Master/wsCourtZone.asmx/DeleteData", { CourtZoneID: id }, null, function (res) {
                            toastr.error(res.d);
                            tableSearch.fnDraw();
                        });
                    }
                },
                validateForm: function () {
                    let self = this;

                    self.errors = [];
                    if (self.data_temp.CourtZoneName === '') {
                        self.errors.push("Court Zone Name is required.");
                    }
                    if (!self.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }
                },
                submitFormData: function () {
                    let self = this;

                    if (self.validateForm()) {

                        _Ajax("/LegalSystem_DEV/Controllers/Master/wsCourtZone.asmx/SaveChangeData",
                            {
                                CourtZoneID: self.data_temp.CourtZoneID,
                                CourtZoneName: self.data_temp.CourtZoneName,
                                CourtZoneDesc: self.data_temp.CourtZoneDesc,
                                status: self.status
                            }
                            , null, function (res) {
                                toastr.success(res.d);
                                self.ResetData();
                                $("#frmModal").modal('hide');
                                tableSearch.fnDraw();
                            });
                    }
                },
                SearchData: function () {
                    tableSearch.fnDraw();
                }
            },
            mounted() {
                let self = this;

                tableSearch = $("#gv").dataTable({
                    lengthChange: false,
                    processing: false,
                    searching: false,
                    pageLength: 10,
                    serverSide: true,
                    responsive: true,

                    ajax: {
                        async: true,
                        contentType: "application/json; charset=utf-8",
                        url: "/LegalSystem_DEV/Controllers/Master/wsCourtZone.asmx/InqueryCourtZone",
                        type: "POST",
                        data: function (d) {
                            console.log(d);

                            return JSON.stringify({
                                option: d, strSearch: self.strSearch
                            });
                        },
                        dataFilter: function (res) {
                            var parsed = JSON.parse(res);
                            console.log(parsed.d);
                            return JSON.stringify(parsed.d);
                        },
                        beforeSend: function () {
                            console.log();
                            $(".se-pre-con").show();
                        },
                        complete: function (d) {
                            console.log(d);
                            $(".se-pre-con").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(thrownError);
                        }
                    },
                    columns: [
                        { data: "CourtZoneID" },
                        { data: "CourtZoneName" },
                        { data: "CourtZoneDesc" },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="edit" class="btn btn-info" href="javascript:;" data-id="' + o.DocTypeID + '"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>'
                                    + '&nbsp;&nbsp;<a id="delete" class="btn btn-danger" href="javascript:;" data-id="' + o.DocTypeID + '"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</a>';
                            }
                        },

                    ],
                    order: [
                        [1, "asc"]
                    ],

                }).on('click', '#edit', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.editData(dataRow);
                }).on('click', '#delete', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.deleteData(dataRow.CourtZoneID);
                });
            },
        });
    </script>
</asp:Content>
