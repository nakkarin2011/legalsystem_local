﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="RequestTypePage.aspx.cs" Inherits="LegalSystem.Pages.Master.RequestTypePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="frm">
        <div class="card ">
            <div class="card-header card-header-tabs card-header-info">
                <h4 class="card-title" id="TypeHeader" runat="server">Request Type</h4>
            </div>
            <div class="card-body" id="form_inquiry">
                <div id="" class="row " style="margin-bottom: -20px;">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Request Name</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input name="strSearch" id="strSearch" type="text" class="form-control col-sm-8" v-model="strSearch" />
                            <%--           <input name="strSearch2" type="text" class="form-control col-sm-8" />
                            <input name="strSearch3" type="text" class="form-control col-sm-8" />--%>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button id="btnserch" type="button" class="btn btn-info" v-on:click="SearchData();"><i class="fa fa-search"></i>&nbsp;Search</button>
                            &nbsp;
                           <button id="btnClear" type="button" class="btn btn-fill btn-warning" v-on:click="ResetData();"><i class="fa fa-refresh"></i>&nbsp;Clear</button>
                            &nbsp;
                           <button id="btnAdd" type="button" class="btn btn-success" v-on:click="addData();"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card ">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="material-datatables table-responsive" id="div_dataTable01"></div>
                                <%-- <div class="material-datatables table-responsive">
                                    <table id="gv" class="table table-striped table-no-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="font-weight: bold;">Request Type</th>
                                                <th style="font-weight: bold;">Request Name</th>
                                                <th style="font-weight: bold;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ modalTitle }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p v-if="errors.length">
                            <b class="text-danger">Please correct the following error(s):</b>
                            <ul>
                                <li v-for="error in errors" class="text-danger">{{ error }}</li>
                            </ul>
                        </p>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Request Type Name *</i></div>
                                </div>
                                <input type="text" v-model="data_temp.ReqTypeName" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" v-on:click="submitFormData()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script type="text/javascript">
        var tableSearch;
        var app = new Vue({
            el: '#frm',
            data: function () {
                return {
                    modalTitle: '',
                    status: '',
                    errors: [],
                    strSearch: '',
                    data_temp: {},
                    model_create: {
                        ReqTypeID: '',
                        ReqTypeName: '',
                    }
                }
            },
            methods: {
                ResetData: function () {
                    let self = this;

                    self.errors = [];
                    self.strSearch = '';
                    _.assign(self.data_temp, {});
                    _resetForm($('#frm'));
                },
                addData: function () {
                    let self = this;

                    self.modalTitle = "Add New Request Type";
                    self.status = 'Add';
                    self.ResetData();
                    _.assign(self.data_temp, self.model_create);
                    $("#frmModal").modal('show');
                },
                editData: function (obj) {
                    let self = this;

                    self.modalTitle = "Edit Request Type";
                    self.status = 'Edit';
                    self.errors = [];
                    _.assign(self.data_temp, obj);
                    $("#frmModal").modal('show');
                },
                deleteData: function (id) {
                    //var r = confirm("Are you sure to delete this item?");
                    swal({
                        title: "Message",
                        text: "Are you sure to delete this item?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ตกลง'
                    })
                        .then((result) => {
                            if (result.value) {
                                _Ajax("/LegalSystem_DEV/Controllers/Master/wsRequestType.asmx/DeleteRequestType", { ReqTypeID: id }, null, function (res) {
                                    toastr.error(res.d);
                                    tableSearch.fnDraw();
                                });
                            }
                        });

                    //if (r == true) {
                    //    _Ajax("/LegalSystem_DEV/Controllers/Master/wsRequestType.asmx/DeleteRequestType", { ReqTypeID: id }, null, function (res) {
                    //        toastr.error(res.d);
                    //        tableSearch.fnDraw();
                    //    });
                    //}
                },
                validateForm: function () {
                    let self = this;

                    self.errors = [];
                    if (self.data_temp.ReqTypeName === '') {
                        self.errors.push("Request Type Name is required.");
                    }
                    if (!self.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }
                },
                submitFormData: function () {
                    let self = this;

                    if (self.validateForm()) {

                        _Ajax("/LegalSystem_DEV/Controllers/Master/wsRequestType.asmx/SaveChangeData",
                            {
                                ReqTypeID: self.data_temp.ReqTypeID,
                                ReqTypeName: self.data_temp.ReqTypeName,
                                status: self.status
                            }
                            , null, function (res) {
                                toastr.success(res.d);
                                self.ResetData();
                                $("#frmModal").modal('hide');
                                tableSearch.fnDraw();
                            });
                    }
                },
                SearchData: function () {

                    tableSearch.fnDraw();
                }
            },
            mounted() {
                let self = this;

                tableSearch = _dataTable("#div_dataTable01", "#dataTable01", "#form_inquiry",
                    {
                        url: "/LegalSystem_DEV/Controllers/Master/wsRequestType.asmx/InqueryRequestType",
                        columns: [
                            { title: "Request Type", data: "ReqTypeID", sClass: "text-center" },
                            { title: "Request Name", data: "ReqTypeName", sClass: "text-left" },
                        ],
                        order: [[1, _Enum.Setting.DataTable.ASC]],
                        autoRow: false,
                        btnEdit: true,
                        btnDelete: true
                    }
                ).on('click', 'a', 'tr', function () {
                    let action = $(this).attr('data-action');
                    let obj = tableSearch.fnGetData($(this).parents('tr')[0]);
                    switch (action) {
                        case _Enum.Operation.EDIT:
                            console.log(_Enum.Operation.EDIT)
                            self.editData(obj);
                            break;
                        case _Enum.Operation.DELETE:
                            console.log(_Enum.Operation.DELETE)
                            self.deleteData(obj.ReqTypeID);
                            break;

                    };
                });


                //tableSearch = $("#gv").dataTable({
                //    lengthChange: false,
                //    processing: false,
                //    searching: false,
                //    pageLength: 10,
                //    serverSide: true,
                //    responsive: true,

                //    ajax: {
                //        async: true,
                //        contentType: "application/json; charset=utf-8",
                //        url: "/LegalSystem_DEV/Controllers/Master/wsRequestType.asmx/InqueryRequestType",
                //        type: "POST",
                //        data: function (d) {
                //            return JSON.stringify({
                //                option: d, model: { strSearch: self.strSearch }
                //            });
                //        },
                //        dataFilter: function (res) {
                //            var parsed = JSON.parse(res);
                //            return JSON.stringify(parsed.d);
                //        },
                //        beforeSend: function () {
                //            $(".se-pre-con").show();
                //        },
                //        complete: function (d) {
                //            $(".se-pre-con").hide();
                //        },
                //        error: function (xhr, ajaxOptions, thrownError) {
                //        }
                //    },
                //    columns: [
                //        { data: "ReqTypeID" },
                //        { data: "ReqTypeName" },
                //        {
                //            data: null,
                //            render: function (o) {
                //                return '<a id="edit" class="btn btn-info" href="javascript:;" data-id="' + o.ReqTypeID + '"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>'
                //                    + '&nbsp;&nbsp;<a id="delete" class="btn btn-danger" href="javascript:;" data-id="' + o.ReqTypeID + '"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</a>';
                //            }
                //        },

                //    ],
                //    order: [
                //        [1, "asc"]
                //    ],

                //}).on('click', '#edit', 'tr', function () {
                //    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                //    self.editData(dataRow);
                //}).on('click', '#delete', 'tr', function () {
                //    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                //    self.deleteData(dataRow.ReqTypeID);
                //});
            },
        });
    </script>
</asp:Content>
