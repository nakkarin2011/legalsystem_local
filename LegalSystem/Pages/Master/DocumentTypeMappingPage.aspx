﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="DocumentTypeMappingPage.aspx.cs" Inherits="LegalSystem.Pages.Master.DocumentTypeMappingPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="frm">
        <div class="card ">
            <div class="card-header card-header-tabs card-header-info">
                <h4 class="card-title" id="TypeHeader" runat="server">Document Type Mapping</h4>
            </div>
            <div class="card-body">
                <div id="form_inquiry" class="row " style="margin-bottom: -20px;">
                       <div class="col-md-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Request Type</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                         <select class="form-control" v-model="ReqTypeID">
                                            <option :value="0">-- Please Select --</option>
                                            <option v-for="option in listRequestType" v-bind:value="option.Value">{{ option.Text }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-1 text-right">
                                    <div class="tim-typo mt-4-5">
                                        <h5>
                                            <span class="tim-note">Document Type Name</span>
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-md-2 col-1 ">
                                    <div class="form-group bmd-form-group">
                                          <input id="strSubTypeCode" type="text" class="form-control col-sm-8" v-model="strDocTypeName" />
                                    </div>
                                </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button id="btnserch" type="button" class="btn btn-info"  v-on:click="SearchData();"><i class="fa fa-search"></i>&nbsp;Search</button>
                            &nbsp;
                           <button id="btnClear" type="button" class="btn btn-fill btn-warning"  v-on:click="ResetData();"><i class="fa fa-refresh"></i>&nbsp;Clear</button>
                            &nbsp;
                           <button id="btnAdd" type="button" class="btn btn-success" v-on:click="addData();"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card ">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="material-datatables table-responsive">
                                    <table id="gv" class="table table-striped table-no-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="font-weight: bold;">Request Type</th>
                                                <th style="font-weight: bold;">Request Name</th>
                                                <th style="font-weight: bold;">Document Type</th>
                                                <th style="font-weight: bold;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <!-- Modal -->
        <div class="modal fade" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ modalTitle }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p v-if="errors.length">
                            <b class="text-danger">Please correct the following error(s):</b>
                            <ul>
                                <li v-for="error in errors" class="text-danger">{{ error }}</li>
                            </ul>
                        </p>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Request Type *</i></div>
                                </div>

                                 <select class="form-control" v-model="data_temp.ReqTypeID">
                                            <option :value="0">-- Please Select --</option>
                                            <option v-for="option in listRequestType" v-bind:value="option.Value">{{ option.Text }}
                                            </option>
                                 </select>
                            </div>
                        </div>
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">Document Type *</i></div>
                                </div>

                                 <select class="form-control" v-model="data_temp.DocTypeID">
                                            <option :value="0">-- Please Select --</option>
                                            <option v-for="option in listDocType" v-bind:value="option.Value">{{ option.Text }}
                                            </option>
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" v-on:click="submitFormData()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script type="text/javascript">
        var tableSearch;
        var app = new Vue({
            el: '#frm',
            data: function () {
                return {
                    modalTitle: '',
                    status: '',
                    errors: [],
                    DocTypeID: '0',
                    ReqTypeID: '0',
                    strDocTypeName: '',
                    listDocType: [],
                    listRequestType: [],
                    data_temp: {},
                    model_create: {
                        ID: '',
                        DocTypeID: '0',
                        ReqTypeID: '0'
                    }
                }
            },
            methods: {
                ResetData: function () {
                    let self = this;

                    self.errors = [];
                    self.strDocTypeName = '';
                    self.ReqTypeID = '0',
                        _.assign(self.data_temp, {});

                },
                addData: function () {
                    let self = this;

                    self.modalTitle = "Add New Document Type Mapping";
                    self.status = 'Add';
                    self.ResetData();
                    _.assign(self.data_temp, self.model_create);
                    $("#frmModal").modal('show');
                },
                editData: function (obj) {
                    let self = this;

                    self.modalTitle = "Edit Document Type Mapping";
                    self.status = 'Edit';
                    self.errors = [];
                    _.assign(self.data_temp, obj);
                    $("#frmModal").modal('show');
                },
                deleteData: function (id) {
                    swal({
                        title: "Message",
                        text: "Are you sure to delete this item?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ตกลง'
                    })
                        .then((result) => {
                            if (result.value) {
                                _Ajax("/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/DeleteDocumentTypeMapping", { ID: id }, null, function (res) {
                                    toastr.error(res.d);
                                    tableSearch.fnDraw();
                                });
                            }
                        });

                    //var r = confirm("Are you sure to delete this item?");
                    //if (r == true) {
                    //    _Ajax("/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/DeleteDocumentTypeMapping", { ID: id }, null, function (res) {
                    //        toastr.error(res.d);
                    //        tableSearch.fnDraw();
                    //    });
                    //}
                },
                validateForm: function () {
                    let self = this;

                    self.errors = [];
                    if (self.data_temp.ReqTypeID === '0') {
                        self.errors.push("Request Type is required.");
                    }
                    if (self.data_temp.DocTypeID === '0') {
                        self.errors.push("Document Type is required.");
                    }

                    if (!self.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }
                },
                submitFormData: function () {
                    let self = this;

                    if (self.validateForm()) {

                        _Ajax("/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/SaveChangeData",
                            {
                                ID: self.data_temp.ID,
                                DocTypeID: self.data_temp.DocTypeID,
                                ReqTypeID: self.data_temp.ReqTypeID,
                                status: self.status
                            }
                            , null, function (res) {
                                toastr.success(res.d);
                                self.ResetData();
                                $("#frmModal").modal('hide');
                                tableSearch.fnDraw();
                            });
                    }
                },
                getListRequestType: function () {
                    let self = this;

                    _Ajax("/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/GetListRequestType",
                        {}, null, function (res) {
                            var obj = JSON.parse(res.d);
                            self.listRequestType = obj;
                        });
                },
                getListDocumentType: function () {
                    let self = this;

                    _Ajax("/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/GetListDocumentType",
                        {}, null, function (res) {
                            var obj = JSON.parse(res.d);
                            self.listDocType = obj;
                        });
                },
                SearchData: function () {
                    tableSearch.fnDraw();
                }
            },
            mounted() {
                let self = this;

                self.getListRequestType();
                self.getListDocumentType();


                tableSearch = $("#gv").dataTable({
                    lengthChange: false,
                    processing: false,
                    searching: false,
                    pageLength: 10,
                    serverSide: true,
                    responsive: true,

                    ajax: {
                        async: true,
                        contentType: "application/json; charset=utf-8",
                        url: "/LegalSystem_DEV/Controllers/Master/wsDocumentTypeMapping.asmx/InqueryDocumentTypeMapping",
                        type: "POST",
                        data: function (d) {
                            console.log(d);

                            return JSON.stringify({
                                option: d
                                , strRequest: self.ReqTypeID
                                , strDocTypeName: self.strDocTypeName

                            });
                        },
                        dataFilter: function (res) {
                            var parsed = JSON.parse(res);
                            console.log(parsed.d);
                            return JSON.stringify(parsed.d);
                        },
                        beforeSend: function () {
                            console.log();
                            $(".se-pre-con").show();
                        },
                        complete: function (d) {
                            console.log(d);
                            $(".se-pre-con").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(thrownError);
                        }
                    },
                    columns: [
                        { data: "ReqTypeID" },
                        { data: "ReqTypeName" },
                        { data: "DocTypeName" },
                        {
                            data: null,
                            render: function (o) {
                                return '<a id="edit" class="btn btn-info" href="javascript:;" data-id="' + o.ID + '"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>'
                                    + '&nbsp;&nbsp;<a id="delete" class="btn btn-danger" href="javascript:;" data-id="' + o.ID + '"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</a>';
                            }
                        },

                    ],
                    order: [
                        [1, "asc"]
                    ],

                }).on('click', '#edit', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.editData(dataRow);
                }).on('click', '#delete', 'tr', function () {
                    var dataRow = tableSearch.fnGetData($(this).parents('tr')[0]);
                    self.deleteData(dataRow.ID);
                });
            },
        });
</script>
</asp:Content>
