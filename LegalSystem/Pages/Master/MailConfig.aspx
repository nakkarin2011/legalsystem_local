﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="MailConfig.aspx.cs" Inherits="LegalSystem.Pages.Master.MailConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div id="ViewMail"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
     <script type="text/javascript">
         $(document).ready(function () {

             _Ajax("/LegalSystem_DEV/MailSetup/Index",{}
                    , null, function (document) {
                        $('#ViewMail').html(document);
                    });
         });



    </script>
</asp:Content>
