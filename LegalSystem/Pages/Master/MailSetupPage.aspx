﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="MailSetupPage.aspx.cs" Inherits="LegalSystem.Pages.Master.MailSetupPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="frm">
        <div class="card">
            <div class="card-header card-header-tabs card-header-info">
                <h4 class="card-title" id="TypeHeader" runat="server">Mail Setup</h4>
            </div>
            <div class="card-body">
                <p v-if="errors.length">
                    <b class="text-danger">Please correct the following error(s):</b>
                    <ul>
                        <li v-for="error in errors" class="text-danger">{{ error }}</li>
                    </ul>
                </p>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Master Template *</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">   
                            <input id="ddlTemplate" type="text" class="form-control " v-model="data_temp.Mail_Type" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Mail To *</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">
                            <input id="mTo" type="text" class="form-control" v-model="data_temp.Mail_To" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Mail CC1</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">
                            <input id="" type="text" class="form-control" v-model="data_temp.Mail_CC1" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Mail CC2</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">
                            <input type="text" class="form-control" v-model="data_temp.Mail_CC2" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Mail Subject *</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">
                            <input type="text" class="form-control" v-model="data_temp.Mail_Subject" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-1 text-right">
                        <div class="tim-typo mt-4-5">
                            <h5>
                                <span class="tim-note">Mail Body *</span>
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group">
                            <textarea id="editorText" class="textarea" placeholder="Place some text here"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: -20px; padding-bottom: 1.5em">
                    <div class="col-md-2 col-sm-1 text-right"></div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <button type="button" class="btn btn-fill btn-info" v-on:click="submitFormData();"><i class="fa fa-floppy-o"></i>&nbsp; Save Changes</button>
                            &nbsp;
                               <button id="btnClear" type="button" class="btn btn-fill btn-warning" v-on:click="ResetData();"><i class="fa fa-refresh"></i>&nbsp; Clear</button>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script type="text/javascript">
        var tableSearch;
        var app = new Vue({
            el: '#frm',
            data: function () {
                return {
                    errors: [],
                    data_temp: {},
                    model_create: {
                        Mail_Type: '',
                        Mail_ID: '',
                        Mail_SMTP: '',
                        Mail_From: '',
                        Mail_To: '',
                        Mail_CC1: '',
                        Mail_CC2: '',
                        Mail_Subject: '',
                        Mail_Body: ''
                    }

                }
            },
            methods: {
                ResetData: function () {
                    let self = this;

                    self.errors = [];
                    _.assign(self.data_temp, self.model_create);
                    $('.textarea').summernote("code", self.model_create.Mail_Body);

                },
                validateForm: function () {
                    let self = this;

                    self.errors = [];
                    //if (isNull(self.data_temp.Mail_SMTP)) {
                    //    self.errors.push("Mail SMTP is required.");
                    //}
                    //if (isNull(self.data_temp.Mail_From)) {
                    //    self.errors.push("Mail From is required.");
                    //}
                    if (isNull(self.data_temp.Mail_To)) {
                        //swal('กรุณากรอกระบุข้อมูลให้ครบถ้วน', '** Mail To is required.\n** Subject is required.', 'warning');
                        self.errors.push("Mail To is required.");
                    }
                    if (isNull(self.data_temp.Mail_Subject)) {
                        self.errors.push("Subject is required.");
                    }
                    if (isNull(self.data_temp.Mail_Body)) {
                        self.errors.push("Body is required.");
                    }

                    if (!self.errors.length) {
                        return true
                    }
                    else {
                        return false
                    }
                },
                submitFormData: function () {
                    let self = this;

                    self.data_temp.Mail_Body = $('#editorText').val();

                    if (self.validateForm()) {

                        _Ajax("/LegalSystem_DEV/MailSetup/SaveChangeData", self.data_temp, null, function (res) {
                            console.log(res);
                            toastr.success(res);
                        });
                    }
                },
                LoadDataMail: function () {
                    let self = this;

                    _Ajax("/LegalSystem_DEV/MailSetup/GetData",
                        {}
                        , null, function (res) {
                            self.data_temp = res;
                            $('.textarea').summernote("code", self.data_temp.Mail_Body);
                        });
                }
            },
            mounted() {
                let self = this;

                $(".se-pre-con").hide();
                $('.textarea').summernote();
                $('form').replaceWith(function () {
                    return $("<div />").append($(this).contents());
                });

                self.LoadDataMail();

            },
        });
    </script>
</asp:Content>
