﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LegalEntities;
using System.ComponentModel;
using LegalService;
using LE = LegalEntities;

namespace LegalSystem.Pages.Setting
{
    public partial class CourtRate : System.Web.UI.Page
    {
        #region Properties
        private string commandname;
        public string CommandName
        {
            get
            {
                return (string)Session["CommandName"];
            }
            set
            {

                Session["CommandName"] = value;
            }
        }

        private List<LE.CourtRate> courtRateList;
        public List<LE.CourtRate> CourtRateList
        {
            get
            {
                return (List<LE.CourtRate>)Session["CourtRateList"];
            }
            set
            {
                Session["CourtRateList"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            GetCondition();
            GetAllCourtRate();
        }

        #region Initial
        public void GetCondition()
        {
            ddlCondition.Items.Insert(0, new ListItem("ไม่ระบุ", ""));
            ddlCondition.Items.Insert(1, new ListItem(">", ">"));
            ddlCondition.Items.Insert(2, new ListItem("<=", "<="));
            ddlCondition.SelectedIndex = 0;
        }

        public void GetAllCourtRate()
        {
            using (CourtRateRep rep = new CourtRateRep())
            {
                courtRateList = rep.GetAllCourtRate();
                CourtRateList = courtRateList;
                gvCourtRate.DataSource = CourtRateList;
                gvCourtRate.DataBind();
            }
        }
        #endregion

        #region Controller Event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CommandName = "Add";
            hidCourtRateID.Value = string.Empty;
            txtPercentage.Value = string.Empty;
            txtMinAmount.Value = string.Empty;
            txtMaxAmount.Value = string.Empty;
            ddlCondition.SelectedValue = string.Empty;
            txtDevide.Value = string.Empty;
            txtPayAmount.Value = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CommandName.Equals("Add"))
            {
                using (CourtRateRep rep = new CourtRateRep())
                {
                    LE.CourtRate courtrate = new LE.CourtRate();
                    courtrate.Percentage = Convert.ToDecimal(txtPercentage.Value.Trim());
                    courtrate.MinAmt = Convert.ToDecimal(txtMinAmount.Value.Trim());
                    courtrate.MaxAmt = Convert.ToDecimal(txtMaxAmount.Value.Trim());
                    courtrate.Condition = ddlCondition.SelectedValue.Trim();
                    courtrate.Devide = Convert.ToDecimal(txtDevide.Value.Trim());
                    courtrate.PayAmount = Convert.ToDecimal(txtPayAmount.Value.Trim());

                    rep.insertData(courtrate);
                }
            }
            else if (CommandName.Equals("Edit"))
            {
                using (CourtRateRep rep = new CourtRateRep())
                {
                    // delete
                    int courtrateid = Convert.ToInt32(hidCourtRateID.Value.Trim());
                    rep.DeleteData(courtrateid);

                    // insert
                    LE.CourtRate courtrate = new LE.CourtRate();
                    courtrate.CourtRateID = courtrateid;
                    courtrate.Percentage = Convert.ToDecimal(txtPercentage.Value.Trim());
                    courtrate.MinAmt = Convert.ToDecimal(txtMinAmount.Value.Trim());
                    courtrate.MaxAmt = Convert.ToDecimal(txtMaxAmount.Value.Trim());
                    courtrate.Condition = ddlCondition.SelectedValue.Trim();
                    courtrate.Devide = Convert.ToDecimal(txtDevide.Value.Trim());
                    courtrate.PayAmount = Convert.ToDecimal(txtPayAmount.Value.Trim());

                    rep.insertData(courtrate);
                }
            }
            GetAllCourtRate();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnserch_Click(object sender, EventArgs e)
        {
            using (CourtRateRep rep = new CourtRateRep())
            {
                string scondition = txtfilter.Value.Trim();

                courtRateList = rep.GetCourtRateWithCondition(scondition);
                CourtRateList = courtRateList;

                gvCourtRate.DataSource = CourtRateList;
                gvCourtRate.DataBind();
            }
        }
        #endregion

        #region GridView Event
        protected void gvCourtRate_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GetAllCourtRate();
        }

        protected void gvCourtRate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = e.Row.FindControl("btnDelete") as Button;
                btn.OnClientClick = "return confirm('Do you want to delete a selected?');";
            }
        }

        protected void gvCourtRate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCourtRate.PageIndex = e.NewPageIndex;
            gvCourtRate.DataSource = CourtRateList;
            gvCourtRate.DataBind();
        }

        protected void gvCourtRate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                // set command name
                CommandName = "Edit";

                // get data from grid
                GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int index = rindex.RowIndex;
                string courtrateID = (gvCourtRate.Rows[index].FindControl("hidRateID") as HiddenField).Value.Trim();
                string minamt = (gvCourtRate.Rows[index].FindControl("lbMinWage") as Label).Text.Trim();
                string maxamt = gvCourtRate.Rows[index].Cells[1].Text.Trim();
                string percentage = gvCourtRate.Rows[index].Cells[2].Text.Trim();
                string condition = (gvCourtRate.Rows[index].FindControl("lbCondition") as Label).Text.Trim();
                string devide = gvCourtRate.Rows[index].Cells[3].Text.Trim();
                string payamount = gvCourtRate.Rows[index].Cells[5].Text.Trim();

                // bind to controller
                hidCourtRateID.Value = courtrateID;
                txtMinAmount.Value = minamt;
                txtMaxAmount.Value = maxamt;
                txtPercentage.Value = percentage;
                txtPayAmount.Value = payamount;
                txtDevide.Value = devide;
                ddlCondition.SelectedValue = condition;
            }
            else if (e.CommandName == "Delete")
            {
                using (CourtRateRep rep = new CourtRateRep())
                {
                    GridViewRow rindex = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = rindex.RowIndex;
                    int crtID = Convert.ToInt32((gvCourtRate.Rows[index].FindControl("hidRateID") as HiddenField).Value.Trim());
                    rep.DeleteData(crtID);
                }
                //GetAllCourtRate();
            }
        }
        #endregion
    }
}