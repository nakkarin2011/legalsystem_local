﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Site1.Master" AutoEventWireup="true" CodeBehind="LegalInfo.aspx.cs" Inherits="LegalSystem.Pages.LegalInfo" %>

<%@ OutputCache Location="None" %>
<%@ Register Src="~/Controls/ucMessageBox.ascx" TagPrefix="uc1" TagName="ucMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .button-Add {
            border-style: none;
            background-position: center;
            background-repeat: no-repeat;
            background-color: transparent;
            width: 27px;
            height: 17px;
            background-image: url('../images/Add.png');
            cursor: pointer;
        }

        .button-delete {
            border-style: none;
            background-position: center;
            background-repeat: no-repeat;
            background-color: transparent;
            width: 27px;
            height: 17px;
            background-image: url('../images/delete.png');
            cursor: pointer;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
    <div id="formProduct" class="card card-nav-tabs">
        <h4 class="card-header " style="background: #003b44">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="#Request" data-toggle="tab">Request Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#Legal" data-toggle="tab">Legal Information</a>
                    </li>
                </ul>
            </div>
        </h4>
        <div class="">
            <div class="tab-content" style="">
                <div class="tab-pane" id="Request">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <uc1:ucmessagebox runat="server" id="UcMessageBoxMain" />
                                    <div class="card">
                                        <fieldset id="myDiv" runat="server" style="padding-top: 0.5em">
                                            <div class="card-header card-header-tabs card-header-warning">
                                                <div class="card-group">
                                                    <h4 class="card-title col-lg-11" id="titlePage" runat="server">Create Request</h4>

                                                    <p class="card-category">
                                                        <asp:Label ID="Label33" runat="server"><text style="color:#e6f2ff; font-style:italic;">กรุณาระบุข้อมูลที่มีเครื่องหมาย </text><text style="color:red; font-style:italic;">*</text> <text style="color:#e6f2ff; font-style:italic;">ให้ครบ (Fields marked with an asterisk (</text> <text style="color:red; font-style:italic;">*</text> <text style="color:#e6f2ff; font-style:italic;">) are required.)</text> </asp:Label>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="card-body ">
                                                <fieldset id="FieldLegal" runat="server">
                                                    <fieldset id="FieldView" runat="server">
                                                        <div class="row" style="margin-bottom: -30px; margin-top: -20px">

                                                            <%--<div class="col-md-2 text-right">--%>
                                                            <div class="col-md-2 text-right ">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Request No :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtRequestNo" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Request Date :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtRequestDate" runat="server" class="datepicker form-control" style="padding-left: 1em;" disabled />
                                                                </div>
                                                            </div>    

                                                        </div>
                                                        <div class="row" style="margin-bottom: -30px">
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Request Status :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="ddlRequestStatus" runat="server" CssClass="form-control" DataValueField="ReqStatusID" DataTextField="ReqStatusName" Style="padding-left: 1em;" disabled></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Request Type</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="ddlRequestType" runat="server" CssClass="form-control" disabled></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: -25px">
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Refer Request No :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtReferRequestNo" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Refer Request Type :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtReferRequestType" runat="server" class="form-control" autocomplete="off" style="padding-left: 1em;" readonly />
                                                                    <asp:HiddenField ID="hidReferRequestType" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: -27px">
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">CIF</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtCIF" runat="server" style="padding-left: 1em;" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 text-right" style="margin-top: auto; padding-top: 20px">
                                                                <asp:ImageButton ID="btnFindCIF" runat="server" CssClass="btn btn-outline-success" ImageUrl="~/Images/Search.png" AutoPostBack="true" OnClick="btnFindCIF_Click" Visible="false"/>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Legal No :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtLegalNo" runat="server" class="form-control" style="padding-left: 1em;" autocomplete="off" readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: -30px">
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Legal Status :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="ddlLegalStatus" runat="server" CssClass="form-control" DataValueField="ActionCode" DataTextField="LegalStatusDisplay" Style="padding-left: 1em;" disabled></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding-bottom: 10px;">
                                                            <div class="col-md-2 text-right">
                                                                <div class="tim-typo mt-4-5">
                                                                    <h5>
                                                                        <span class="tim-note">Address :</span>
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8 ">
                                                                <div class="form-group">
                                                                    <input type="text" id="txtAddress" runat="server" class="form-control" autocomplete="off" style="padding-left: 1em;" readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <div></div>

                                                    <div class="row" style="margin-top: 20px;">
                                                        <%--<div class="col-sm-1"></div>--%>
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-header card-header-tabs card-header-warning">
                                                                    <h4 class="card-title">บัญชีสินเชื่อ</h4>
                                                                </div>
                                                                <div class="">
                                                                    <asp:GridView ID="gvAccountLoan" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                                        ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="Center" DataKeyNames="ACCTNO" AllowPaging="false" AllowSorting="false" ShowHeader="false"
                                                                        OnRowDataBound="gvAccountLoan_RowDataBound" OnRowCommand="gvAccountLoan_RowCommand" OnSelectedIndexChanged="gvAccountLoan_SelectedIndexChanged">
                                                                        <Columns>

                                                                            <asp:TemplateField ItemStyle-BackColor="White" ItemStyle-BorderWidth="0">
                                                                                <ItemTemplate>

                                                                                    <div class="card col-lg-12" style="margin-top: 1em; margin-bottom: -5px">
                                                                                        <div class="card-body" style="background-color: #F0F0F0">
                                                                                            <div class="row">
                                                                                                <div class="col-md-1 ">
                                                                                                    <div>
                                                                                                        <a class="btn btn-primary" data-toggle="collapse" href='#<%#Eval("ACCTNO")%>' role="button" aria-expanded="false"
                                                                                                            aria-controls="CollapseDetailAccount"><i class="fa fa-plus"></i></a>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-1 left">
                                                                                                    <div class="form-check">
                                                                                                        <label class="form-check-label">
                                                                                                            <input class="form-check-input" type="checkbox" id="chkAccount" runat="server"><span class="form-check-sign"><span class="check"></span></span></label>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-1 text-right">
                                                                                                    <div class=" tim-typo mt-3">
                                                                                                        <h5>
                                                                                                            <span class="tim-note">Account :</span>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group " style="margin-top: 0.6em">
                                                                                                        <input type="text" id="txtAcctLoan" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-2 text-right">
                                                                                                    <div class=" tim-typo mt-3" style="margin-top: 1.1em">
                                                                                                        <h5>
                                                                                                            <span class="tim-note">Account Status :</span>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group" style="margin-top: 0.6em">
                                                                                                        <input type="text" id="txtAcctStatus" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-1 text-right">
                                                                                                    <div class=" tim-typo mt-3" style="margin-top: 1.1em">
                                                                                                        <h5>
                                                                                                            <span class="tim-note">DPD :</span>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-1">
                                                                                                    <div class="form-group" style="margin-top: 0.6em">
                                                                                                        <input type="text" id="txtDPD" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-2">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-2 text-right">
                                                                                                    <div class=" tim-typo mt-4-5" style="margin-top: 1.1em">
                                                                                                        <h5>
                                                                                                            <span class="tim-note">Collateral Type :</span>
                                                                                                        </h5>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-9">
                                                                                                    <div class="form-group">
                                                                                                        <input type="text" id="txtCollateralType" runat="server" class="form-control" style="padding-left: 1em;" readonly />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="card">
                                                                                                <div id='<%#Eval("ACCTNO")%>' class="row collapse multi-collapse">
                                                                                                    <div class="col-md-12 col-centered" <%-- style="background-color: white"--%>>
                                                                                                        <div class="wizard-navigation">
                                                                                                            <ul class="nav nav-pills">
                                                                                                                <li class="nav-item">
                                                                                                                    <a class="nav-link active" href='#<%= this.ClientID + "_tabDeposit_" %><%#Eval("ACCTNO")%>' data-toggle="tab" role="tab">Account
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                                <li class="nav-item">
                                                                                                                    <a class="nav-link" href='#<%= this.ClientID + "_tabLoan_" %><%#Eval("ACCTNO")%>' data-toggle="tab" role="tab">Detail
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>

                                                                                                        <div class="tab-content" style="margin-bottom: 4%">

                                                                                                            <div class="tab-pane active" id='<%= this.ClientID + "_tabDeposit_" %><%#Eval("ACCTNO")%>'>
                                                                                                                <div class="row justify-content-center">
                                                                                                                    <div class="col-md-12">
                                                                                                                        <asp:GridView ID="gvRelationship" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-no-bordered table-hover" GridLines="None"
                                                                                                                            ShowHeaderWhenEmpty="true" DataKeyNames="A_CFCIFN" AllowPaging="false" AllowSorting="false" OnRowDataBound="gvRelationship_RowDataBound">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:HiddenField ID="hidCIF" runat="server" Value='<%#Eval("A_CFCIFN") %>' />
                                                                                                                                        <asp:HiddenField ID="hidType" runat="server" Value='<%#Eval("A_CFRELA") %>' />
                                                                                                                                        <asp:HiddenField ID="hidOrder" runat="server" Value='<%#Eval("RelationOrder") %>' />

                                                                                                                                        <div class="form-check">
                                                                                                                                            <label class="form-check-label">

                                                                                                                                                <input class="form-check-input" type="checkbox" id="chkRelationship" runat="server" onclick="OnDeSelectAcctLoanAll(this)" />
                                                                                                                                                <span class="form-check-sign">
                                                                                                                                                    <span class="check"></span>
                                                                                                                                                </span>

                                                                                                                                            </label>
                                                                                                                                        </div>

                                                                                                                                    </ItemTemplate>
                                                                                                                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:BoundField DataField="RelationName" SortExpression="RelationName" HeaderText="Relationship" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                                <asp:BoundField DataField="A_CFCIFN" SortExpression="A_CFCIFN" HeaderText="CIF No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                                <asp:BoundField DataField="A_CFNAME" SortExpression="A_CFNAME" HeaderText="CIF Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                                <asp:BoundField DataField="A_CFSSNO" SortExpression="A_CFSSNO" HeaderText="Citizen ID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                                <asp:BoundField DataField="ACC_NO" SortExpression="ACC_NO" HeaderText="Deposit A/C" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                                                                                <asp:BoundField DataField="CUR_BAL01" SortExpression="CUR_BAL01" HeaderText="Deposit Outstanding" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" ItemStyle-Width="120px"></asp:BoundField>
                                                                                                                            </Columns>
                                                                                                                        </asp:GridView>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="tab-pane" id='<%= this.ClientID + "_tabLoan_" %><%#Eval("ACCTNO")%>'>
                                                                                                                <div class="row justify-content-center">
                                                                                                                    <div class="col-md-12">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <span class="tim-note">Loan Type :</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtLoanType" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">เงินต้น :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtCurBalAmt" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">วงเงินกู้ :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtOriginalAmt" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">ดอกเบี้ยปกติ :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtAcctInterest" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">อัตราดอกเบี้ย % :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtInterestRate" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">ดอกเบี้ยค้างรับตั้งพัก :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtACCI3" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">จำนวนวันค้างชำระ :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtNumOfDPD" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">ดอกเบี้ยผิดนัดตั้งพัก :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtPENIN3" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">ดอกเบี้ยผิดนัดชำระล่าช้า :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtPENINT" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <span class="tim-note">ค่าใช้จ่ายอื่นๆ :</span>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtPOTHER" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-3 col-sm-3 text-right"></div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 "></div>
                                                                                                                            <div class="col-md-3 col-sm-3 text-right">
                                                                                                                                <div class="tim-typo mt-4-5">
                                                                                                                                    <h5>
                                                                                                                                        <strong class="tim-note">รวม :</strong>
                                                                                                                                    </h5>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-2 col-lg-2 col-sm-1 col-1 ">
                                                                                                                                <div class="form-group">
                                                                                                                                    <input type="text" id="txtTotal" runat="server" class="form-control text-right" style="padding-right: 0.7em;" readonly />
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                      
                                                        <div class="col-sm-12">
                                                            <fieldset id="FieldAttachment" runat="server">
                                                                <div class="card" id="divAttachments" runat="server" style="padding-bottom: 1em">
                                                                    <div class="card-header card-header-tabs card-header-warning">
                                                                        <h4 class="card-title">สิ่งที่แนบมาด้วย</h4>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox" id="chk1" runat="server" checked="checked" disabled>
                                                                                        <span class="form-check-sign">
                                                                                            <span class="check"></span>
                                                                                        </span>หนังสือบอกกล่าวผู้ค้ำประกัน พร้อมไปรษณีย์ตอบรับ</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="col-sm-10 checkbox-radios">
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label">
                                                                                            <input class="form-check-input" type="checkbox" id="chk11" runat="server" checked="checked" disabled/>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            มี
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label tim-note">
                                                                                            <input class="form-check-input" type="checkbox" id="chk12" runat="server" disabled/>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            ไม่มี เพราะไม่มีผู้ค้ำประกัน
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-bottom:15px;">
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox" id="chk2" runat="server" checked="checked" disabled>
                                                                                        <span class="form-check-sign">
                                                                                            <span class="check"></span>
                                                                                        </span>หนังสือแจ้งวันขายทอดตลาดหลักทรัพย์จำนำ พร้อมไปรษณีย์ตอบรับ</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-bottom:15px;">
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox" id="chk21" runat="server" checked="checked" disabled>
                                                                                        <span class="form-check-sign">
                                                                                            <span class="check"></span>
                                                                                        </span>ใบรายงานการประมูล</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-bottom:15px;">
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox" id="chk3" runat="server" checked="checked" disabled>
                                                                                        <span class="form-check-sign">
                                                                                            <span class="check"></span>
                                                                                        </span>มีบัญชีเงินฝาก</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="col-sm-10 checkbox-radios">
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label">
                                                                                            <input class="form-check-input" type="checkbox" id="chk31" runat="server" checked="checked" disabled/>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            มี ตัดบัญชีเงินฝากชำระหนี้เงินกู้ทั้งจำนวน
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label tim-note">
                                                                                            <input class="form-check-input" type="checkbox" id="chk32" runat="server" disabled/>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            มีบันทึกถึงฝ่ายปฏิบัติการกลาง (ไม่สามารถตัดบัญชีออมทรัพย์ได้)
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"  style="padding-bottom:15px;">
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox" id="chk4" runat="server" checked="checked" disabled>
                                                                                        <span class="form-check-sign">
                                                                                            <span class="check"></span>
                                                                                        </span>ไม่มีบัญชีเงินฝาก</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-lg-5 col-sm-1 col-1 ">
                                                                                <div class="col-sm-10 checkbox-radios">
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label">
                                                                                            <input class="form-check-input" type="checkbox" id="chk41" runat="server" checked="checked" disabled />
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            ลูกหนี้หลบหนี/ไม่สามารถติดต่อลูกหนี้ได้เป็นเวลานาน
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="form-check">
                                                                                        <label class="form-check-label tim-note">
                                                                                            <input class="form-check-input" type="checkbox" id="chk42" runat="server" disabled/>
                                                                                            <span class="form-check-sign">
                                                                                                <span class="check"></span>
                                                                                            </span>
                                                                                            ลูกหนี้ไม่มีความสามารถในการชำระหนี้
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-1 text-right">
                                                        <div class="tim-typo mt-4-5">
                                                            <h5>
                                                                <span class="tim-note">เหตุผล :</span>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-lg-3 col-sm-1 col-1 ">
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="ddlReason" Style="padding-left: 1em;" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonDisplay" disabled></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <fieldset id="FSApprover1" runat="server" visible="true">
                                                        <div class=" col-md-12 text-right">
                                                            <div id="divApprover1" runat="server" class="tim-typo mt-4-5">
                                                                <h5>
                                                                    <span class="tim-note">ผู้อนุมัติ :</span>
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset id="FSApprover2" runat="server" class="col-md-2" visible="true">
                                                        <div id="divApprover2" runat="server" class="">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="ddlApprover" Style="padding-left: 1em;" runat="server" CssClass="form-control" DataValueField="Emp_Code" DataTextField="TH_Name" disabled></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset id="FSAssignOA_IA1" runat="server" visible="false">
                                                        <div class=" col-md-12 text-right">
                                                            <div id="divAssignOA_IA1" runat="server" class="tim-typo mt-4-5">
                                                                <h5>
                                                                    <span class="tim-note">Assign OA/IA :</span>
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset id="FSAssignOA_IA2" runat="server" class="col-md-2" visible="false">
                                                        <div id="divAssignOA_IA2" runat="server" class="">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="ddlAttorney" runat="server" DataValueField="AttorneyID" DataTextField="LawyerName" CssClass="form-control" Style="padding-left: 1em;" disabled></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset id="FSAssignLawyer1" runat="server" visible="false">
                                                        <div class=" col-md-12 text-right">
                                                            <div id="divAssignLawyer1" runat="server" class="tim-typo mt-4-5">
                                                                <h5>
                                                                    <span class="tim-note">Assign Lawyer :</span>
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset id="FSAssignLawyer2" runat="server" class="col-md-2" visible="false">
                                                        <div id="divAssignLawyer2" runat="server" class="">
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="ddlLawyer" DataValueField="Emp_Code" DataTextField="TH_Name" runat="server" CssClass="form-control" Style="padding-left: 1em;" disabled></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div id="divComment" runat="server" class="row" visible="false" style="padding-bottom: 1em">
                                                    <div class="col-md-2 col-sm-1 text-right">
                                                        <div class="tim-typo mt-4-5">
                                                            <h5>
                                                                <span class="tim-note">Comment :</span>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-lg-8 col-sm-1 col-1 ">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtComment" runat="server" CssClass="form-control" Style="color: dodgerblue; padding-left: 1em;" TextMode="MultiLine" Height="80px" disabled></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <fieldset id="FieldCaseRefer" runat="server" visible="false" style="padding-bottom: 2em">

                                                    <div class="col-lg-12">
                                                        <div class="card">

                                                            <div class="card-header card-header-tabs  card-header-warning" data-toggle="collapse" href='#divReferCaseDate' role="button" aria-expanded="false"
                                                                aria-controls="CollapseCaseAsOfDate">
                                                                <h4 id="titleReferCaseAsOfDate" runat="server" class="card-title"><i class="fa fa-plus-circle">&nbsp;&nbsp;</i>REFER CASE</h4>
                                                            </div>
                                                            <div class="card-body collapse multi-collapse" id="divReferCaseDate" style="margin-top: -20px; padding-bottom: 2em">
                                                                <div class="row col-auto" style="padding-bottom: 4em">
                                                                    <%--กรณีการชะลอการดำเนินคดี--%>
                                                                    <div id="divReferCaseAsOfDate" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="row col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณีการชะลอดำเนินคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%-- <div class="col-lg-auto text-right"></div>--%>
                                                                                <%--<div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="txtReferCaseAsOfDate" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--กรณียกเลิกชะลอการดำเนินคดี--%>
                                                                    <div id="divReferCaseASOfDateCancel" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณียกเลิกชะลอการดำเนินคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="txtReferCaseDateCancel" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--กรณียกเลิกการดำเนินคดี--%>
                                                                    <div id="divLitigationsCancel" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณียกเลิกการดำเนินคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="ddlReferCaseReason" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;"></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row col-auto" style="margin-top: -10px;">
                                                                    <%--กรณียกเลิกบังคับคดี--%>
                                                                    <div id="div1" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณียกเลิกบังคับคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;" required></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--กรณีขอชะลอการบังคับคดี--%>
                                                                    <div id="div2" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณีขอชะลอการบังคับคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px;">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;" required></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text1" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--กรณียกเลิกชะลอการบังคับคดี--%>
                                                                    <div id="div4" runat="server" class="card card-body col-lg-3" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณียกเลิกชะลอการบังคับคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px;">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;" required></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text3" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%--กรณีถอนฟ้อง--%>
                                                                <div id="divReferFilingWithdrawal" runat="server" class="card card-body" visible="false">
                                                                    <div class=" card-header row col-lg-11" style="margin-bottom: -20px;">
                                                                        <div class="col-lg-4">
                                                                            <div class="text-left">
                                                                                <h4><span class="tim-note card-title">กรณีถอนฟ้อง</span></h4>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row col-lg-12" style="margin-top: -10px;">
                                                                        <div class=" col-md-auto text-right">
                                                                            <div class="tim-typo mt-4-5">
                                                                                <h5>
                                                                                    <span class="tim-note">ค่าจ้าง/ค่าทนายความ</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                                                </h5>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="text" id="txtFilingWithdrawal_LawyerFee" runat="server" class="form-control" style="padding-right: 0.7em;" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-auto text-right">
                                                                            <div class="tim-typo mt-4-5">
                                                                                <h5>
                                                                                    <span class="tim-note">ค่าธรรมเนียมศาล</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                                                </h5>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="text" id="txtFilingWithdrawal_CourtFee" runat="server" class="form-control" style="padding-right: 0.7em;" readonly />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-auto text-right">
                                                                            <div class="tim-typo mt-4-5">
                                                                                <h5>
                                                                                    <span class="tim-note">ค่าใช้จ่ายอื่นๆ</span><text style="color: red; font-style: italic;">*</text><span class="tim-note"> :</span>
                                                                                </h5>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="text" id="txtFilingWithdrawal_OtherExpense" runat="server" class="form-control" style="padding-right: 0.7em;" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-auto text-right">
                                                                            <div class="tim-typo mt-4-5">
                                                                                <h5>
                                                                                    <span class="tim-note">ยอดรวม :</span>
                                                                                </h5>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="text" id="txtFilingWithdrawal_Total" runat="server" class="form-control" style="padding-right: 0.7em;" readonly />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row col-lg-12" style="padding-bottom: 1.5em;">
                                                                        <div class="col-md-auto"></div>
                                                                        <div class="col-md-1 text-right">
                                                                            <div class="tim-typo mt-4-5">
                                                                                <h5>
                                                                                    <span class="tim-note">เหตุผล </span>
                                                                                    <text style="color: red; font-style: italic;">*</text>
                                                                                    <span class="tim-note">:</span>
                                                                                </h5>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <asp:DropDownList ID="ddlReasonWithdrawal" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;"></asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row col-auto" style="margin-top: -10px;">
                                                                    <%--กรณีขอชะลอขายทอดตลาด--%>
                                                                    <div id="div3" runat="server" class="card card-body col-lg-5" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">กรณีขอชะลอขายทอดตลาด</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px;">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text2" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text5" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;" required></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--ยกเลิกชะลอขายทอดตลาด--%>
                                                                    <div id="div5" runat="server" class="card card-body col-lg-5" visible="false">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note">ยกเลิกชะลอขายทอดตลาด</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px;">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text4" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ถีงวันที่ </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text6" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px; padding-bottom: 2em">
                                                                                <%--<div class="col-lg-1 text-right"></div>
                                                                <div class="col-lg-1 text-right"></div>--%>
                                                                                <div class="col-lg-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">เหตุผล </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;" required></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row col-auto" style="margin-top: -10px;">
                                                                    <%--กรณีขอ Compromise (CIC)--%>
                                                                    <div id="div6" runat="server" class="card card-body col-lg-5" visible="false">
                                                                        <div class=" card-header row col-lg-12" style="margin-bottom: -20px;">
                                                                            <div class="col-lg-auto">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note card-title">กรณีขอ Compromise (CIC)</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -20px;">
                                                                                <div class="col-md-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">ระบุวันที่นัดทำยอม </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="Text7" runat="server" class="datepicker form-control" style="padding-right: 0.7em;" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row col-lg-12" style="margin-top: -10px; padding-bottom: 1.5em;">
                                                                                <div class="col-md-auto text-right">
                                                                                    <div class="tim-typo mt-4-5">
                                                                                        <h5>
                                                                                            <span class="tim-note">มีการชำระก่อนฟ้อง </span>
                                                                                            <text style="color: red; font-style: italic;">*</text>
                                                                                            <span class="tim-note">:</span>
                                                                                        </h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;"></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-auto"></div>
                                                                    <%--กรณีขอบังคับคดี--%>
                                                                    <div id="div7" runat="server" class="card card-body col-lg-5" visible="false">
                                                                        <div class=" card-header row col-lg-5" style="margin-bottom: -20px;">
                                                                            <div class="col-lg-12">
                                                                                <div class="text-left">
                                                                                    <h4><span class="tim-note card-title">กรณีขอบังคับคดี</span></h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row col-lg-12" style="margin-top: -10px; padding-bottom: 1.5em;">
                                                                            <div class="col-md-auto"></div>
                                                                            <div class="col-md-auto text-right">
                                                                                <div class="tim-typo mt-4-5">
                                                                                    <h5>
                                                                                        <span class="tim-note">เหตุผล </span>
                                                                                        <text style="color: red; font-style: italic;">*</text>
                                                                                        <span class="tim-note">:</span>
                                                                                    </h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <asp:DropDownList ID="DropDownList7" runat="server" CssClass="form-control" DataValueField="ReasonID" DataTextField="ReasonName" Style="padding-right: 0.7em;"></asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </fieldset>
                                        <div class="card-footer" style="padding-top: 1%; padding-bottom: 1%;">
                                            <asp:HiddenField ID="hidLegalStatus" runat="server" />
                                            <asp:HiddenField ID="hidRequestID" runat="server" />
                                            <asp:HiddenField ID="hidRefRequestNo" runat="server" />
                                            <div id="divFooterMakerChecker" runat="server" class="m-auto" visible="true">                             
                                <%--<asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClientClick="return ConfirmDialog(this);" OnClick="btnSubmit_Click" />--%>                                                
                                            </div>
                                            <div id="divFooterHeader" runat="server" class="m-auto" visible="false">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info" OnClientClick="return ConfirmDialog(this);" OnClick="btnSave_Click" Visible="false" />
                                                &nbsp; 
                                <asp:Button ID="btnReturnToPool" runat="server" Text="Return to Pool" CssClass="btn btn-warning" OnClientClick="return ConfirmAssign(this);" OnClick="btnReturnToPool_Click" Visible="false" />
                                                &nbsp;                                
                                <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="btn btn-default" PostBackUrl="~/Pages/InquiryPage.aspx" />
                                            </div>
                                            <div id="divFooterReferCase" runat="server" class="m-auto" visible="false">
                            <asp:Button ID="btnBack3" runat="server" Text="Back" CssClass="btn btn-default" PostBackUrl="~/Pages/InquiryPage.aspx" />
                                            </div>
                                        </div>
                                    </div>

                                </ContentTemplate>
<%--                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlRequestType" EventName="SelectedIndexChanged" />
                                </Triggers>--%>

                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="1">
                                <ProgressTemplate>
                                    <div class="loading-visible" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: white; opacity: 0.75;">
                                        <p>
                                            <img style="padding: 10px; position: fixed; top: 50%; left: 50%;" src="../Resources/Preloader_4.gif" />
                                        </p>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>

                        </div>
                    </div>
                </div>
                <div class="tab-pane active" id="Legal">
                       <div id="ViewLegal"></div>
                </div>
            </div>
        </div>
    </div>









    <div id="fade" class="black_overlay"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                        showOn: 'button',
                        buttonImageOnly: true,
                        buttonImage: 'calendar.png',
                        format: 'dd/mm/yyyy',
                        todayBtn: false,
                        language: 'th-th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                        thaiyear: true              //Set เป็นปี พ.ศ.
                    }); //.datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน

                _Ajax("/LegalSystem_DEV/LegalTab/Index",
                        {
                            rid:'<%= Request.QueryString["rid"].ToString() %>'
                        }
                        ,null, function (document) {
                            $('#ViewLegal').html(document);
                        });
            });



    </script>
</asp:Content>
