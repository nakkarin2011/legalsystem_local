﻿using LegalEntities;
using LegalService;
using LegalSystem.CommonHelper;
using LegalSystem.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LegalSystem.Common.Common_RequestPage
{
    public class SetUI_RequestPage
    {
        AlertMessage Msg = new AlertMessage();
        public static string[] chks => new string[] { "chk1", "chk11", "chk12", "chk2", "chk21", "chk3", "chk31", "chk32", "chk4", "chk41", "chk42" };
        public void SetAttachments(trRequest Req, HtmlGenericControl divAttachments, bool IsSubmited, int GroupID)
        {
            try
            {
                string[] attachs = Req.Attachments.Split(',');
                for (int i = 0; i < chks.Count(); i++)
                {
                    var control = ((HtmlInputCheckBox)divAttachments.FindControl(chks[i]));
                    if (attachs[i] == "1")
                        control.Attributes.Add(MasterAttributesHelper.Checked, MasterAttributesHelper.Checked);
                    else
                        control.Attributes.Remove(MasterAttributesHelper.Checked);

                    if (IsSubmited)
                    {
                        //if (GroupID != 440 || GroupID != 441 || GroupID != 442)
                        if (GroupID == MasterPermissionHelper.CollectionMaker || GroupID == MasterPermissionHelper.CollectionChecker)
                        {
                        }
                        else
                            control.Attributes.Add(MasterAttributesHelper.disabled, MasterAttributesHelper.disabled);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void Set_DropDownList(LegalUnitOfWork unitOfWork, DropDownList ddlRequestStatus, DropDownList ddlLegalStatus, DropDownList ddlRequestType, DropDownList ddlApprover, string rid, string User, List<vRequestTypeMapLegalStstus> ddlRType, string TypeMode)
        {
            try
            {
                var headApprover = unitOfWork.VCollectionHeadApproveRep.GetApprove();
                List<msRequestStatus> dd = unitOfWork.MSRequestStatusRep.Get().ToList();
                ddlRequestStatus.DataSource = dd;
                ddlRequestStatus.DataBind();

                ddlLegalStatus.DataSource = unitOfWork.MSLegalStatusRep.Get();
                ddlLegalStatus.DataBind();

                ddlRType = unitOfWork.VRequestTypeMapLegalStstusRep.GetRequestTypeByLegalStstus((!string.IsNullOrEmpty(rid)) ? "0" : ddlLegalStatus.SelectedValue);
                ddlRequestType.DataSource = ddlRType;
                ddlRequestType.DataBind();
                if (TypeMode == MasterECommandNameHelper.Edit)
                {
                    ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetUsers(User, headApprover[0].Emp_Code);
                    ddlApprover.DataBind();
                }
                else if (TypeMode == MasterECommandNameHelper.View)
                {
                    ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetAllUsers();
                    ddlApprover.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public void Set_ddlAssignLawyer(LegalUnitOfWork unitOfWork, DropDownList ddlAttorney, DropDownList ddlLawyer, string RequestType, string User)
        {
            try
            {
                List<msAttorney> tmpAttorney = unitOfWork.MSAttorneyRep.GetAllLawyer().ToList();
                ddlAttorney.DataSource = tmpAttorney;
                ddlAttorney.DataBind();

                List<vAttorneyLawyer> tmpLawyer = unitOfWork.VAttorneyLawyerRep.GetUsersAssign(User).ToList();
                List<vAttorneyLawyer> tmpLawyer_SN = unitOfWork.VAttorneyLawyerRep.GetAllUsersAssign().ToList();
                switch (RequestType)
                {
                    case "2":
                        ddlLawyer.DataSource = tmpLawyer_SN;
                        ddlLawyer.DataBind();
                        break;
                    case "7":
                        ddlLawyer.DataSource = tmpLawyer;
                        ddlLawyer.DataBind();
                        break;
                    default:
                        ddlLawyer.DataSource = tmpLawyer;
                        ddlLawyer.DataBind();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void Set_DDLApproverType(LegalUnitOfWork unitOfWork, DropDownList ddlApprover, DropDownList ddlRequestType, string PermissionID)
        {
            try
            {
                var headApprover = unitOfWork.VCollectionHeadApproveRep.GetApprove();
                string EmpCode = GlobalSession.UserLogin.Emp_Code;
                switch (PermissionID)
                {
                    case "442":
                        if (ddlRequestType.SelectedItem.Value == "2")
                        {
                            ddlApprover.Items.Clear();
                            ddlApprover.SelectedIndex = -1;
                            ddlApprover.SelectedValue = null;
                            ddlApprover.ClearSelection();
                            ddlApprover.DataSource = unitOfWork.VEmpLegalRep.GetApproverNoticeSenior(headApprover[0].Emp_Code, EmpCode);
                            ddlApprover.DataBind();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void Set_ddlReAssignLawyer(LegalUnitOfWork unitOfWork, DropDownList ddlLawyer, string RequestType, string lawyer)
        {
            try
            {
                List<msAssignLawyer> tmpLawyer = unitOfWork.MSAssignLawyerRep.GetUsers(lawyer).ToList();
                List<msAssignLawyer> tmpLawyer_SN = unitOfWork.MSAssignLawyerRep.GetAllUsers().ToList();
                switch (RequestType)
                {
                    case "2":
                        ddlLawyer.DataSource = tmpLawyer_SN;
                        ddlLawyer.DataBind();
                        break;
                    case "7":
                        ddlLawyer.DataSource = tmpLawyer;
                        ddlLawyer.DataBind();
                        break;
                    default:
                        ddlLawyer.DataSource = tmpLawyer;
                        ddlLawyer.DataBind();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}