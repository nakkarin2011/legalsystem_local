﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace LegalSystem.Common
{
    public class AlertMessage
    {
        public void MsgBox(String message, string type, Page pg, Object obj)
        {
            //Msg.MsgBox(MessageEvent.PleaseSelectCaseRefer, MessageEventType.info, this, this.GetType());
            ScriptManager.RegisterStartupScript(pg, this.GetType(), "myModal", "alertData('" + message + "','" + type + "');", true);
        }

        public void MsgReturn(String message, Page pg, Object obj, string page, string type)
        {
            //Msg.MsgReturn(MessageEvent.Save, Page, this, MasterPageHelper.InboxPage, MessageEventType.success);
            ScriptManager.RegisterStartupScript(pg, this.GetType(), "myModal", "alertDataRedirect('" + message + "','" + type + "','" + page + "');", true);
        }

    }
    public class MessageEventType
    {
        public const string success = "success";
        public const string warning = "warning";
        public const string error = "error";
        public const string info = "info";
    }
    public class MessageConfirm
    {
        public const string ConfirmDelete = "ต้องการ Delete ข้อมูลหรือไม่?";
    }
    public class MessageEvent
    {
        public const string Succesfully = "Successfully";
        public const string SaveDraft = "Draft Saved Successfully";
        public const string Save = "Data Successfully Saved";
        public const string DeleteDraft = "Successfully Deleted";
        public const string Register = "Successfully Registered";
        public const string Return = "Return case Successfully";
        public const string ReturnToPool = "Return to Pool สำเร็จ";
        public const string Reject = "Reject case Successfully";
        public const string GetTask = "GetTask was Successfully";
        public const string Approve = "Successfully Approved";
        public const string Accept = "Successfully Accepted";
        public const string Assign = "Assign completed";
        public const string NewRequest = "Create Request สำเร็จ";
        public const string Reciept = "Reciept สำเร็จ";

        public const string SendEmailSuccess = "ส่ง email สำเร็จ";
        public const string SendEmailError = "ส่ง email ไม่สำเร็จ กรุณาส่งใหม่อีกครั้ง";
        public const string SearchCIFDuplicate = "กรุณาระบุ CIF No. ใหม่ที่ไม่ซ้ำเดิม";

        public const string NoData = "ไม่พบข้อมูล";
        public const string RequiredField = "กรุณาระบุข้อมูลให้ครบถ้วน";
        public const string NoSelectAccount = "กรุณาเลือกอย่างน้อย 1 Account";
        public const string RequiredFieldAsterisk = "กรุณาระบุข้อมูลที่มีเครื่องหมาย * ให้ครบ (Fields marked with an asterisk (*) are required.)";
        public const string SelectSameCollateral = "กรุณาเลือกบัญชีที่มีหลักประกันเดียวกัน";
        public const string SelectRelationship = "กรุณาเลือก Relationship ของบัญชี ";
        public const string SelectLoanAccountAndRelationship = "กรุณาเลือกบัญชีสินเชื่อและ Relationship อย่างน้อย 1 บัญชี";
        public const string LoanAccountNotFound = "ไม่พบบัญชีสินเชื่อ";
        //public const string PleaseEnterCIFNo        = "CIF No. failed, Please enter CIF No.";
        public const string PleaseEnterCIFNo = "Please enter CIF No.";
        public const string PleaseSelectApprover = "กรุณาเลือกผู้อนุมัติ";
        public const string PleaseSelectCaseRefer = "Please select Request Type";
        public const string PleaseSelect1Item = "กรุณาเลือกอย่างน้อย 1 รายการ";
        public const string PleaseSelectToReturn_Assign = "กรุณาเลือกรายการเพื่อ Return";
        public const string PleaseSelectOther = "กรุณาเพิ่มข้อมูลอื่นเพื่อการค้นหาที่เร็วขึ้น";
        public const string PleaseInsertParam = "กรุณาเลือกค้นหาด้วย Parameter";
        public const string NoReAssignHis = "ไม่มีข้อมูลการ REASSIGN LAWYER";
        public const string PleaseRequiredRequestType = "กรุณาระบุ Request Type";
        public const string ReFerCancelProcess = "Cannot process..";

    }
}