﻿using LegalSystem.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using LegalSystem.Model;
using LegalService;
using LegalEntities;
using LegalSystem.CommonHelper;

namespace LegalSystem.Common
{
    public class SendMail
    {
        #region SEND MAIL
        public string DoCSendMailToOper(string FileName, int MailType, string Attroney)
        {

            string result = string.Empty;
            LegalData.msMailConfig objmail = new LegalData.msMailConfig();
            string mailBody = string.Empty;
            try
            {
                var mail = new MailMessage();
                using (MailConfigRep rep = new MailConfigRep())
                {
                    switch (MailType)
                    {
                        case 1:
                            var ListRep = rep.GetByMailType(MailType).ToList();
                            objmail = ListRep.FirstOrDefault();

                            mail.To.Add(new MailAddress(objmail.Mail_To));
                            mailBody = objmail.Mail_Body;

                            break;
                        case 2:
                            objmail = rep.GetByMailType(MailType).FirstOrDefault();
                            mail.To.Add(new MailAddress(objmail.Mail_To));
                            mailBody = objmail.Mail_Body;
                            break;
                        case 3:
                            objmail = rep.GetByMailType(MailType).FirstOrDefault();
                            using (LegalUnitOfWork unitOfWork = new LegalUnitOfWork())
                            {
                                msAttorney AttorneyMail = unitOfWork.MSAttorneyRep.GetLawyerByID(Convert.ToInt32(Attroney));

                                mailBody = string.Format("เรียน {0}{1}", AttorneyMail.LawyerName, objmail.Mail_Body);
                                mail.To.Add(new MailAddress(AttorneyMail.AttorneyMail));
                            }
                            break;
                    }

                    if (objmail != null)
                    {
                        var smtp = new SmtpClient(objmail.Mail_SMTP);
                        mail.IsBodyHtml = true;
                        mail.From = new MailAddress(objmail.Mail_From);

                        if (objmail.Mail_CC1.Contains(","))
                        {
                            List<string> ListCCMail = objmail.Mail_CC1.Split(',').ToList();
                            foreach (var MailCC in ListCCMail)
                            {
                                mail.CC.Add(new MailAddress(MailCC.ToString()));
                            }
                        }
                        else
                            mail.CC.Add(new MailAddress(objmail.Mail_CC1));

                        mail.Subject = objmail.Mail_Subject;
                        mail.BodyEncoding = Encoding.UTF8;
                        mail.Body = mailBody;
                        mail.Priority = MailPriority.Normal;
                        mail.Attachments.Add(new Attachment(FileName));
                        smtp.Send(mail);
                        smtp.Dispose();
                        mail.Dispose();
                    }
                }

                //Set Email Send Status 1 = ส่ง Email สำเร็จ
                result = "1";
            }
            catch
            {
                //Set Email Send Status 2 = ส่ง Email ไม่สำเร็จ
                result = "2";
                throw;
            }

            return result;
        }


        public List<UseListItem> BindEmailSendStatus(string reqMode)
        {
            try
            {
                var listitem = new List<UseListItem>();
                switch (reqMode)
                {
                    case MasterReqModeHelper.ReqOSOper:
                        listitem.Add(new UseListItem
                        {
                            Value = "0",
                            Text = "ยังไม่ได้ส่งขอ O/S"
                        });

                        listitem.Add(new UseListItem
                        {
                            Value = "1",
                            Text = "ส่งขอ O/S แล้ว"
                        });
                        break;
                    case MasterReqModeHelper.ReqDocOper:
                    case MasterReqModeHelper.ReqDoctoOutsource:
                        listitem.Add(new UseListItem
                        {
                            Value = "0",
                            Text = "ยังไม่ได้ส่งเอกสาร"
                        });

                        listitem.Add(new UseListItem
                        {
                            Value = "1",
                            Text = "ส่งเอกสารแล้ว"
                        });
                        break;
                }
                
                listitem.Add(new UseListItem
                {
                    Value = "2",
                    Text = "ส่งเอกสารไม่สำเร็จ"
                });


                return listitem;
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
