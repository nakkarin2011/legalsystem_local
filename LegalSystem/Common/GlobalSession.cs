﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using TCRB.Securities;

namespace LegalSystem.Common
{
    
    public class GlobalSession
    {
        public enum PermissionID
        {
            CollectionMaker,
            CollectionChecker,
            CollectionHead,
            LegalSpecialist,
            SeniorLawyer,
            Lawyer,            
        }
        public static void ClearSessionAll()
        {
            UserIdentity tmp = GlobalSession.UserLogin;
            HttpContext.Current.Session.Clear();
            GlobalSession.UserLogin = tmp;
        }

        public static UserIdentity UserLogin
        {
            get { return (UserIdentity)HttpContext.Current.Session["UserLogin"]; }
            set { HttpContext.Current.Session["UserLogin"] = value; }
        }

        public static List<string> Permission
        {
            get { return (List<string>)HttpContext.Current.Session["Permission"]; }
            set { HttpContext.Current.Session["Permission"] = value; }
        }
        public static PermissionID GetPermission
        {
            get
            {
                if(Permission.IndexOf("N18001")!=-1)
                    return PermissionID.CollectionMaker;                
                else if(Permission.IndexOf("N18003")!=-1)
                    return PermissionID.CollectionHead;
                else if (Permission.IndexOf("N18004") != -1)
                    return PermissionID.LegalSpecialist;
                else if (Permission.IndexOf("N18005") != -1)
                    return PermissionID.SeniorLawyer;
                else if (Permission.IndexOf("N18006") != -1)
                    return PermissionID.Lawyer;
                return PermissionID.CollectionChecker;
            }
        }
        public static int UserRole
        {
            get { return HttpContext.Current.Session["UserRole"] == null ? 0 : (int)HttpContext.Current.Session["UserRole"]; }
            set { HttpContext.Current.Session["UserRole"] = value; }
        }

        public static string PathFilePDF
        {
            get { return (string)HttpContext.Current.Session["PathFilePDF"]; }
            set { HttpContext.Current.Session["PathFilePDF"] = value; }
        }

        public static CultureInfo CultureTH
        {
            get { return new System.Globalization.CultureInfo("th-TH"); }
        }
    }
}