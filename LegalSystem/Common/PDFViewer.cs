﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LegalSystem.Common
{
    public class PDFViewer
    {
        public void WritePDF(string PathFile, ReportViewer viewer)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty,
                encoding = string.Empty,
                extension = string.Empty;
            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            System.IO.File.WriteAllBytes(PathFile, bytes);

            ///*Open file*/
            //using (FileStream fs = new FileStream(PathFile, FileMode.Create))
            //    fs.Write(bytes, 0, bytes.Length);
        }
    }
}