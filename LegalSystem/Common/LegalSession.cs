﻿using LegalEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LegalSystem.Common
{
    public static class LegalSession
    {
        public static LegalService.LegalDbContext _legalDbContext
        {
            get
            {
                return HttpContext.Current.Session["LegalDataContext"] as LegalService.LegalDbContext;
            }
            set
            {
                HttpContext.Current.Session["LegalDataContext"] = value;
            }
        }
        public static FileUpload fileupload
        {
            get
            {
                return HttpContext.Current.Session["FileUpload"] as FileUpload;
            }
            set
            {
                HttpContext.Current.Session["FileUpload"] = value;
            }
        }
        public static List<fileupload> filessave
        {
            get
            {
                return HttpContext.Current.Session["FilesSave"] as List<fileupload>;
            }
            set
            {
                HttpContext.Current.Session["FilesSave"] = value;
            }
        }
        public static List<filedelete> filesdelete
        {
            get
            {
                return HttpContext.Current.Session["FilesDelete"] as List<filedelete>;
            }
            set
            {
                HttpContext.Current.Session["FilesDelete"] = value;
            }
        }
        public static int lastDocID
        {
            get
            {
                return (int)HttpContext.Current.Session["lastDocID"];
            }
            set
            {
                HttpContext.Current.Session["lastDocID"] = value;
            }

        }
    }

    public class fileupload
    {
        public int DocID { get; set; }
        public FileUpload File { get; set; }
    }

    public class filedelete
    {
        public int DocID { get; set; }
        public string FileName { get; set; }
    }
}