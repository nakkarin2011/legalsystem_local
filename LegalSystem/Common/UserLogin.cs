﻿using System;
using System.Collections.Generic;
using TCRB.Securities;

namespace LegalSystem.Common
{
    public class UserLogin
    {
        public UserIdentity Login(string UserName, string Password)
        {
            UserIdentity User;
            TCRBIdentity tcrb = new TCRBIdentity(ConfigurationManager.SystemID);

            TCRBImpersonationContext tic = tcrb.Impersonate(UserName, Password);

            tic.URL = ConfigurationManager.UserManagementURL;
            User = tic.Login();

            return User;
        }
    }
}