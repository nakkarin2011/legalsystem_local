﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace LegalSystem.Common
{
    public class Utility
    {
        public static List<T> MapTableToList<T>(System.Data.DataTable dt) where T : class
        {
            List<T> ls = new List<T>();
            T t;
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                t = Activator.CreateInstance<T>();
                System.Reflection.PropertyInfo[] info = t.GetType().GetProperties();
                foreach (System.Data.DataColumn dc in dt.Columns)
                    foreach (System.Reflection.PropertyInfo pi in info)
                        if (dc.ColumnName.ToUpper() == pi.Name.ToUpper())
                        {
                            if (dr[dc.ColumnName] != DBNull.Value)
                                pi.SetValue(t, Convert.ChangeType(dr[dc.ColumnName], pi.PropertyType), null);
                            break;
                        }
                ls.Add(t);
            }
            return ls;
        }
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }

            return table;

        }
    }

    public class util
    {
        /// <summary>
        /// Get bank name. this property is read only.
        /// </summary>
        //public static string BankName
        //{
        //    get { return util.getGlobalParameter("BankName"); }
        //}

        /// <summary>
        /// Get name of session employee. this constant parameter.
        /// </summary>
        public const string employeeSessionName = "LegalSystem_USER";
        /// <summary>
        /// Get string of decimal position. this constant parameter.
        /// </summary>
        public const string decimalPosition = "2";
        /// <summary>
        /// Get string of date format. this constant parameter.
        /// </summary>
        public const string dateFormat = "dd/MM/yyyy";
        /// <summary>
        /// Get string default to drop down list. this constant parameter.
        /// </summary>
        public const string defaultStringdll = "-- Please Select --";
        /// <summary>
        /// 
        /// </summary>
        public const string systemID = "N18";

        /// <summary>
        /// Return string script confirm box.
        /// </summary>
        /// <param name="msg">Message alert</param>
        /// <returns></returns>
        public static string scriptConfirm(string msg)
        {
            return string.Format("Loading(true); var bc = confirm('{0}'); if (!bc) Loading(false); return bc;", msg);
        }

       
        /// <summary>
        /// Set read only control.
        /// </summary>
        /// <param name="isReadOnly">Is read only</param>
        /// <param name="ctrl">Control</param>
        public static void setControlReadOnly(bool isReadOnly, System.Web.UI.WebControls.TextBox ctrl)
        {
            setControlReadOnly(isReadOnly, ctrl, false);
        }
        /// <summary>
        /// Set read only control.
        /// </summary>
        /// <param name="isReadOnly">Is read only</param>
        /// <param name="ctrl">Control</param>
        public static void setControlReadOnly(bool isReadOnly, System.Web.UI.WebControls.TextBox ctrl, bool isDecimal)
        {
            ctrl.CssClass = isReadOnly ? (isDecimal ? "TextBoxMoneyReadOnly" : "TextBoxReadOnly") :
                (isDecimal ? "TextBoxMoney" : "TextBox");
            ctrl.ReadOnly = isReadOnly;
        }

        /// <summary>
        /// Set read only control.
        /// </summary>
        /// <param name="isReadOnly">Is read only</param>
        /// <param name="ctrl">Control</param>
        public static void setControlReadOnly(bool isReadOnly, System.Web.UI.WebControls.CheckBox ctrl)
        {
            ctrl.Enabled = !isReadOnly;
        }
        /// <summary>
        /// Set read only control.
        /// </summary>
        /// <param name="isReadOnly">Is read only</param>
        /// <param name="ctrl">Control</param>
        public static void setControlReadOnly(bool isReadOnly, System.Web.UI.WebControls.DropDownList ctrl)
        {
            ctrl.Enabled = !isReadOnly;
        }

        /// <summary>
        /// Set default selected to drop down
        /// </summary>
        /// <param name="ddl">Control drop down</param>
        public static void setDefaultDropDown(System.Web.UI.WebControls.DropDownList ddl)
        {
            ddl.DataBind();
            ddl.Items.Insert(0, new System.Web.UI.WebControls.ListItem(defaultStringdll, "-1"));
        }

        /// <summary>
        /// Return string id card format removed. (#############)
        /// </summary>
        /// <param name="id">ID card formatted</param>
        /// <returns></returns>
        public static string toIDCard(string id)
        {
            return id.Replace("-", "");
        }
        /// <summary>
        /// Return to id card formatted. (#-####-#####-##-#)
        /// </summary>
        /// <param name="id">ID card</param>
        /// <returns></returns>
        public static string toIDCardFormat(string id)
        {
            return id.Substring(0, 1) + "-" + id.Substring(1, 4) + "-" +
                id.Substring(5, 5) + "-" + id.Substring(10, 2) + "-" + id.Substring(12, 1);
        }

        /// <summary>
        /// Return string in decimal formatted. (##,###.##)
        /// </summary>
        /// <param name="d">Decimal value</param>
        /// <returns></returns>
        public static string toDecimalString(decimal d)
        {
            return d.ToString(string.Format("n{0}", decimalPosition));
        }
        /// <summary>
        /// Return string in date formatted. (DD/MM/YYYY)
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns></returns>
        public static string toDateFormat(DateTime? date)
        {
            //return (date.Equals(DateTime.MinValue)) ? "" : date.ToString(dateFormat);
            if (date.HasValue)
                return date.Value.ToString(dateFormat);
            else
                return "";
        }

        /// <summary>
        /// Return datetime from string.
        /// </summary>
        /// <param name="date">Date string (DD/MM/YYYY)</param>
        /// <returns></returns>
        public static DateTime toDate(string date)
        {
            DateTime rdate = new DateTime();
            bool isPass = DateTime.TryParse(date, out rdate);
            return isPass ? rdate : DateTime.MinValue;
        }
        /// <summary>
        /// Return decimal from string.
        /// </summary>
        /// <param name="d">Currency string</param>
        /// <returns></returns>
        public static decimal toDecimal(string d)
        {
            decimal rd = 0;
            decimal.TryParse(d, out rd);
            return rd;
        }

        ///// <summary>
        ///// Return value of payment amount per month.
        ///// </summary>
        ///// <param name="ratePerMonth">Interest rate per month</param>
        ///// <param name="numberOfPayments">Installment months</param>
        ///// <param name="amount">Loan amount</param>
        ///// <returns></returns>
        //public static double calculatePaymentAmount(double ratePerMonth, double numberOfPayments, double amount)
        //{
        //    if ((numberOfPayments.Equals(0)) || (amount.Equals(0)))
        //        return 0;
        //    double Percent = Convert.ToDouble(ratePerMonth / 100);
        //    double No = numberOfPayments;
        //    double PV = amount * -1;
        //    return Microsoft.VisualBasic.Financial.Pmt(Percent, No, PV, 0, Microsoft.VisualBasic.DueDate.EndOfPeriod);
        //}

        /// <summary>
        /// Return value ceiling by default position.
        /// </summary>
        /// <param name="d">Decimal value</param>
        /// <returns></returns>
        public static double Ceiling(double d)
        {
            return Ceiling(d, Convert.ToInt32(decimalPosition));
        }
        /// <summary>
        /// Return value ceiling by position.
        /// </summary>
        /// <param name="d">Decimal value</param>
        /// <param name="position">Position</param>
        /// <returns></returns>
        public static double Ceiling(double d, int position)
        {
            double pow = Math.Pow(10, position);
            if ((d % pow) == 0) return d;
            return ((Math.Floor((d / pow)) + 1) * pow);
        }

        /// <summary>
        /// Convert data table to Collections.Generic.List<T>.
        /// </summary>
        /// <typeparam name="T">Class type</typeparam>
        /// <param name="dt">Data table</param>
        /// <returns></returns>
        public static System.Collections.Generic.List<T> mapTableToList<T>(System.Data.DataTable dt) where T : class
        {
            System.Collections.Generic.List<T> ls = new System.Collections.Generic.List<T>();
            T t;
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                t = Activator.CreateInstance<T>();
                System.Reflection.PropertyInfo[] info = t.GetType().GetProperties();
                foreach (System.Data.DataColumn dc in dt.Columns)
                    foreach (System.Reflection.PropertyInfo pi in info)
                        if (dc.ColumnName.ToUpper() == pi.Name.ToUpper())
                        {
                            if (dr[dc.ColumnName] != DBNull.Value)
                                pi.SetValue(t, Convert.ChangeType(dr[dc.ColumnName], pi.PropertyType), null);
                            break;
                        }
                ls.Add(t);
            }
            return ls;
        }

        ///// <summary>
        ///// Get global parameter value by filed name.
        ///// </summary>
        ///// <param name="filedName">Field name</param>
        ///// <returns></returns>
        //public static string getGlobalParameter(string filedName)
        //{
        //    GlobalParameter Global = new GlobalParameter();
        //    using (GlobalParameterRepository GlobalRep = new GlobalParameterRepository())
        //        Global = GlobalRep.SelectDataWithCondition(g => g.FieldName.Equals(filedName)).FirstOrDefault();

        //    return Global.Value;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sources"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<T> cloneList<T>(
            System.Collections.Generic.List<T> sources) where T : class
        {
            T[] arrs = new T[sources.Count];
            sources.CopyTo(arrs);
            return new System.Collections.Generic.List<T>(arrs);
        }

        ///// <summary>
        ///// Check is permission
        ///// </summary>
        ///// <param name="msg"></param>
        ///// <param name="pm"></param>
        ///// <returns></returns>
        //public static bool isPermission(WsUser.UserManagementMSG msg, UserLogin.Permission pm)
        //{
        //    bool isFound = false;
        //    foreach (WsUser.Group g in msg.Employee.Groups)
        //    {
        //        foreach (WsUser.Permission p in g.Permissions)
        //        {
        //            if (p.SystemID.Equals(util.systemID) &&
        //                Convert.ToInt32(p.PermissionID).Equals((Int32)pm))
        //            {
        //                isFound = true;
        //            }
        //        }
        //    }
        //    return isFound;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="msg"></param>
        ///// <returns></returns>
        //public static string getEmployeeType(WsUser.UserManagementMSG msg)
        //{
        //    if (msg.Position != null)
        //    {
        //        if (msg.Position.Type != null)
        //            return msg.Position.Type;
        //        else
        //            return "E";
        //    }
        //    else
        //        return "E";
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="p"></param>
        ///// <returns></returns>
        //public static WsUser.UserManagementMSG getAllUser(string p)
        //{
        //    LeaveSystemV2.WsUser.UserManagement ws = new LeaveSystemV2.WsUser.UserManagement();
        //    p = p.ToLower().Replace(@"tcbank\", @"");

        //    return ws.GetAllUser(getPreRequest(p));
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="p"></param>
        ///// <returns></returns>
        //public static WsUser.PreRequest getPreRequest(string p)
        //{
        //    WsUser.PreRequest pre = new LeaveSystemV2.WsUser.PreRequest();
        //    pre.ADUser = p;
        //    pre.SystemID = LeaveSystemV2.Common.util.systemID;
        //    return pre;
        //}
    }

    /// <summary>
    /// Log of gold for cash SME project.
    /// </summary>
    public class SystemLog
    {
        //public static bool insertEventLog(bool isErrorLog, string desc, string by)
        //{
        //    System.Data.SqlClient.SqlConnection con = util.GetConnection;
        //    try
        //    {
        //        string tableName = (isErrorLog) ? "EventErrorLog" : "EventLog";
        //        string sql = "INSERT INTO [" + tableName + "]" +
        //            "([Version],[IPAddress],[LogDescription],[CreatedBy],[CreatedOn])VALUES" +
        //            "(@Version,@IPAddress,@LogDescription,@CreatedBy,@CreatedOn)";
        //        con.Open();
        //        System.Data.SqlClient.SqlCommand Cmd = new System.Data.SqlClient.SqlCommand(sql, con);
        //        Cmd.Parameters.Add("@Version", System.Data.SqlDbType.VarChar).Value = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        //        Cmd.Parameters.Add("@IPAddress", System.Data.SqlDbType.VarChar).Value = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //        Cmd.Parameters.Add("@LogDescription", System.Data.SqlDbType.VarChar).Value = desc;
        //        Cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.VarChar).Value = by.ToUpper();
        //        Cmd.Parameters.Add("@CreatedOn", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
        //        Cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        util.CloseConnection(con);
        //    }
        //}

        //public static bool insertWorkflowLog(string contractNumber, string statusName,
        //    string name, string tel, string deptName, string note, string by)
        //{
        //    System.Data.SqlClient.SqlConnection Conn =
        //        new System.Data.SqlClient.SqlConnection(util.ConnectionString);
        //    try
        //    {
        //        string sql = "INSERT INTO [WorkflowLog]([ContractNumber],[StatusName],[UserName],[Telephone],[Department],[Note],[UserDomain],[StampDate])VALUES(@ContractNumber,@StatusName,@UserName,@Telephone,@Department,@Note,@UserDomain,@StampDate)";
        //        Conn.Open();
        //        System.Data.SqlClient.SqlCommand Cmd = new System.Data.SqlClient.SqlCommand(sql, Conn);
        //        Cmd.Parameters.Add("@ContractNumber", System.Data.SqlDbType.Char).Value = contractNumber;
        //        Cmd.Parameters.Add("@StatusName", System.Data.SqlDbType.VarChar).Value = statusName;
        //        Cmd.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = name;
        //        Cmd.Parameters.Add("@Telephone", System.Data.SqlDbType.VarChar).Value = tel;
        //        Cmd.Parameters.Add("@Department", System.Data.SqlDbType.VarChar).Value = deptName;
        //        Cmd.Parameters.Add("@Note", System.Data.SqlDbType.VarChar).Value = note;
        //        Cmd.Parameters.Add("@UserDomain", System.Data.SqlDbType.VarChar).Value = by.ToUpper();
        //        Cmd.Parameters.Add("@StampDate", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
        //        Cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        if (Conn != null)
        //        {
        //            Conn.Close();
        //        }
        //    }
        //}
    }
}