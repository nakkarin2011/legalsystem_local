﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Common
{
    public class ConfigurationManager
    {
        public static string SystemID { get { return LegalSystem.Properties.Settings.Default.SystemID; } }
        public static string SystemName { get { return LegalSystem.Properties.Settings.Default.SystemName; } }
        public static string UserManagementURL { get { return LegalSystem.Properties.Settings.Default.UserManagementURL; } }
        public static string LegalConnectionString { get { return LegalSystem.Properties.Settings.Default.LegalConnectionString; } }
        public static string PathDocument { get { return LegalSystem.Properties.Settings.Default.PathDocument; } }

        public static string ExportLog
        {
            get { return @"~/Files/TCRB_Announcement_Log_{0}.xlsx"; }
        }

        public static string ExportCategory
        {
            get { return @"~/Files/TCRB_Announcement_Detail_{0}.xlsx"; }
        }

        public static string FileNameCoverpage
        {
            get { return @"~/Documents/CP_{0}.pdf"; }
        }

        #region Paths of Decument Type
        public static string DocMailToOper
        {
            get { return string.Format(@"{0}0_ขอเบิกเอกสารของลูกหนี้\",PathDocument); }
        }
        public static string DocOutstandingMail
        {
            get { return string.Format(@"{0}1_ขอOutstanding\", PathDocument); }
        }
        public static string DocMailToOutsource
        {
            get { return string.Format(@"{0}2_ส่งเอกสารให้LawyerOutsource\", PathDocument); }
        }
        
        public static string DocPathType1
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\FirstLetter\"; }
        }

        public static string DocPathType2
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือส่งดำเนินคดี_ไม่มีหลักประกัน\"; }
        }

        public static string DocPathType3
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือส่งดำเนินคดี_มีหลักประกัน\"; }
        }

        public static string DocPathType4
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือนำส่งเอกสารในการบังคับคดี_ยึดทรัพย์\"; }
        }

        public static string DocPathType5
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือนำส่งเอกสารในการบังคับคดี_หนี้บุริมสิทธิ\"; }
        }

        public static string DocPathType6
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแก้ไขเพิ่มเติมคำฟ้อง\"; }
        }

        public static string DocPathType7
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแก้ไขเพิ่มเติมคำพิพากษา\"; }
        }

        public static string DocPathType8
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือนำส่งเอกสารในการบังคับคดี_ยึดทรัพย์\"; }
        }

        public static string DocPathType9
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือนำส่งเอกสารในการบังคับคดี_หนี้บุริมสิทธิ\"; }
        }

        public static string DocPathType10
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\Notice_มีหลักประกัน\"; }
        }

        public static string DocPathType11
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\Notice_ไม่มีหลักประกัน\"; }
        }

        public static string DocPathType12
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแจ้งชะลอการดำเนินคดี\"; }
        }

        public static string DocPathType13
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแจ้งยกเลิกชะลอคดีเพื่อดำเนินคดี\"; }
        }

        public static string DocPathType14
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแจ้งยกเลิกการดำเนินคดี\"; }
        }

        public static string DocPathType15
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือถอนฟ้อง\"; }
        }

        public static string DocPathType16
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือบังคับคดี_พิทยา\"; }
        }

        public static string DocPathType17
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือออกหมายบังคับคดี_เจ้าอื่น\"; }
        }

        public static string DocPathType18
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือยกเลิกบังคับคดี_ปิดบัญชีก่อนยึดทรัพย์\"; }
        }

        public static string DocPathType19
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือขอให้ถอนบังคับคดีลูกหนี้_ปิดบัญชีหลังยึดทรัพย์\"; }
        }

        public static string DocPathType20
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือขอให้ดำเนินการงดการบังคับคดีลูกหนี้_ชะลอ\"; }
        }

        public static string DocPathType21
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือขอยกเลิกชะลอบังคับคดี\"; }
        }

        public static string DocPathType22
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือขอให้ชะลอการขายทอดตลาด\"; }
        }

        public static string DocPathType23
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือยกเลิกชะลอขายทอดตลาด\"; }
        }

        public static string DocPathType24
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแจ้งอนุมัติทำยอม_ไม่ชำระก่อนฟ้อง\"; }
        }

        public static string DocPathType25
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\หนังสือแจ้งอนุมัติทำยอม_ชำระก่อนฟ้อง\"; }
        }

        public static string DocPathType0
        {
            get { return @"\\172.17.9.68\cds\Project UAT\LegalSystem\Documents\Others\"; }
        }
        #endregion
    }
}