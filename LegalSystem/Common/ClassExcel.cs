﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using LegalSystem.Common;
using Excels = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace LegalSystem.Common
{
    public class GenExcel
    {
        protected object Missing = System.Reflection.Missing.Value;

        Excels.Application ExcelApp = null;
        Excels.Workbook WorkBook = null;
        Excels.Worksheet WorkSheet = null;
        public string FileName { get; set; }
        public int WorkSheetNo { get; set; }
        public string WorkSheetName { get; set; }

        public GenExcel(string fileName, byte[] Source)
        {
            this.FileName = fileName;
            if (File.Exists(this.FileName)) File.Delete(this.FileName.ToString());
            File.WriteAllBytes(this.FileName.ToString(), Source);
        }
        public void OpenFile()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            ExcelApp.DisplayAlerts = false;
            ExcelApp.Visible = false;

            WorkBook = ExcelApp.Workbooks.Open(FileName.ToString(), Missing, Missing
                , Missing, Missing, Missing, Missing
                , Missing, Missing, Missing, Missing
                , Missing, Missing, Missing, Missing);
            WorkSheet = (Excels.Worksheet)WorkBook.Worksheets[WorkSheetName];
        }

        public void SetArrayValues(string fieldsXls, int Row, object[] Value)
        {
            WorkSheet.get_Range(string.Format(fieldsXls, Row), Missing).Value2 = Value;
        }
        public void SetArrayValues(string fieldsXls, int Row, object Value)
        {
            WorkSheet.get_Range(string.Format(fieldsXls, Row), Missing).Value2 = Value;
        }
        public void SetWorkSheet()
        {
            WorkSheet = (Excels.Worksheet)WorkBook.Worksheets[WorkSheetName];
        }
        public void CreateCopyWorkSheet()
        {
            Excels.Worksheet newWorksheet = (Excels.Worksheet)WorkBook.Worksheets.Add(Missing, Missing, Missing, Missing);
            newWorksheet.Name = "";
            WorkSheet.Copy(Missing, "");

        }
        public void VisibleTabSheet()
        {
            WorkSheet.Visible = Excels.XlSheetVisibility.xlSheetHidden;
        }
        public void SaveAndClose()
        {
            //ExcelApp.Save(Missing);
            WorkBook.Save();
            WorkBook.Close(Missing, Missing, Missing);
            //if (ExcelApp != null)
            //    ExcelApp.Quit();
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(WorkSheet);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(WorkBook);
            ExcelApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp);
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("th-TH");
        }
    }
}
