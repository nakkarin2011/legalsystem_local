﻿using LegalEntities;
using LegalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LegalSystem.Common
{
    public class InsertDataCaseEditRefer
    {
        public void InsertCase(trRequest reqEditCase, LegalUnitOfWork unitOfWork, trRequest Req, string newRequestNo,string dlRequestType)
        {
            int NoDefen = Convert.ToInt32(newRequestNo.Substring(0,3).Replace("R0",""));
            reqEditCase.RequestID = unitOfWork.LegalStoreProcedure.GetGuidID();
            reqEditCase.RequestNo = newRequestNo;
            reqEditCase.RequestStatusDate = DateTime.Now;
            reqEditCase.RequestDate = DateTime.Now;
            reqEditCase.RequestType = dlRequestType;
            reqEditCase.RequestStatus = Req.RequestStatus;
            reqEditCase.RequestReason = Req.RequestReason;
            reqEditCase.LegalID = Req.LegalID;
            reqEditCase.LegalStatus = Req.LegalStatus;
            reqEditCase.LegalStatusDate = Req.LegalStatusDate;
            reqEditCase.Owner = Req.Owner;
            reqEditCase.OwnerApprove = Req.OwnerApprove;
            reqEditCase.Attachments = Req.Attachments;
            reqEditCase.ColHead = Req.ColHead;
            reqEditCase.ColHeadApprove = Req.ColHeadApprove;
            reqEditCase.LegalSpecial = Req.LegalSpecial;
            reqEditCase.LegalSpecialApprove = Req.LegalSpecialApprove;
            reqEditCase.SeniorLawyer = Req.SeniorLawyer;
            reqEditCase.SeniorLawyerReceive = Req.SeniorLawyerReceive;
            reqEditCase.SeniorLawyerApprove = Req.SeniorLawyerApprove;
            reqEditCase.CreateBy = GlobalSession.UserLogin.Emp_Code;
            reqEditCase.CreateDate = Req.CreateDate;
            reqEditCase.UpdateBy = GlobalSession.UserLogin.Emp_Code;
            reqEditCase.UpdateDate = DateTime.Now;
            reqEditCase.Comment = Req.Comment;
            reqEditCase.DefendantNo = NoDefen;
            reqEditCase.LawyerIDApprove = Req.LawyerIDApprove;
            reqEditCase.LawyerID = Req.LawyerID;
            reqEditCase.RefRequestNo = Req.RefRequestNo;
            reqEditCase.RefRequestType = Req.RefRequestType;
            reqEditCase.RefRequestReason = Req.RefRequestReason;
            //reqEditCase.RefRequestDate = Req.RefRequestDate; //ชะลอ
            //reqEditCase.RefRequestDateCancelCase = DateTime.Now;//ยกเลิกชะลอ
        }
    }
}