﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LegalSystem.Common;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using LegalEntities;
using System.Globalization;

namespace LegalSystem.Common
{
    public class ExportExcel
    {
        public void GenerateDocToOperation(List<sp_GetRequestOperPage> ListDataSendmail, string Filename)
        {
            int No = 1;
            int HeadRow = 1;
            int row = 2;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    var workSheet = excel.Workbook.Worksheets.Add("Legal Document");
                    workSheet.View.FreezePanes(2, 1); //ตรึงแนว

                    workSheet.Cells[HeadRow, 1].Value = "No.";
                    workSheet.Cells[HeadRow, 2].Value = "CIF";
                    workSheet.Cells[HeadRow, 3].Value = "ACCOUNT NAME";
                    workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    foreach (var data in ListDataSendmail)
                    {
                        workSheet.Cells[row, 1].Value = No++;
                        workSheet.Cells[row, 2].Value = data.CifNo;
                        workSheet.Cells[row, 3].Value = data.CFNAME;
                        row++;
                    }
                    workSheet.Column(1).AutoFit();
                    workSheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(2).AutoFit();
                    workSheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(3).AutoFit();
                    workSheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    FileStream objFileStrm = File.Create(Filename);
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(Filename, excel.GetAsByteArray());
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void GenerateOSToOperation(List<sp_GetRequestOperPage> ListDataSendmail, string Filename, List<trRequestRelation> ListAccOS)
        {
            int No = 1;
            int HeadRow = 1;
            int row = 2;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    var workSheet = excel.Workbook.Worksheets.Add("Legal Document");
                    workSheet.View.FreezePanes(2, 1); //ตรึงแนว

                    workSheet.Cells[HeadRow, 1].Value = "No.";
                    workSheet.Cells[HeadRow, 2].Value = "CIF";
                    workSheet.Cells[HeadRow, 3].Value = "ACCOUNT NAME";
                    workSheet.Cells[HeadRow, 4].Value = "ACCOUNT";

                    workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    foreach (var data in ListDataSendmail)
                    {
                        var Acc = ListAccOS.Where(W => W.RelationCifNo == data.CifNo).ToList();
                        foreach (var LSAcc in Acc)
                        {
                            workSheet.Cells[row, 1].Value = No++;
                            workSheet.Cells[row, 2].Value = data.CifNo;
                            workSheet.Cells[row, 3].Value = data.CFNAME;
                            workSheet.Cells[row, 4].Value = LSAcc.Account.ToString();
                            row++;
                        }
                    }
                    workSheet.Column(1).AutoFit();
                    workSheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(2).AutoFit();
                    workSheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(3).AutoFit();
                    workSheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Column(4).AutoFit();
                    workSheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    FileStream objFileStrm = File.Create(Filename);
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(Filename, excel.GetAsByteArray());
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void GenerateDocToOutsource(List<LegalData.sp_GetRequestOperPage_Outsource_Result> ListDataSendmail, string Filename, List<trRequestRelation> ListAccount)
        {
            int No = 1;
            int HeadRow = 1;
            int row = 2;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    var workSheet = excel.Workbook.Worksheets.Add("Legal Document");
                    workSheet.View.FreezePanes(2, 1); //ตรึงแนว

                    workSheet.Cells[HeadRow, 1].Value = "No.";
                    workSheet.Cells[HeadRow, 2].Value = "เรื่อง";
                    workSheet.Cells[HeadRow, 3].Value = "ACCOUNT";
                    workSheet.Cells[HeadRow, 4].Value = "CIF";
                    workSheet.Cells[HeadRow, 5].Value = "ACCOUNT NAME";
                    workSheet.Cells[HeadRow, 6].Value = "คดีพิเศษ (บสย.)";
                    workSheet.Cells[HeadRow, 7].Value = "ส่งออก สนง.";
                    workSheet.Cells[HeadRow, 8].Value = "Notice (ภายใน)";
                    workSheet.Cells[HeadRow, 9].Value = "กำหนดฟ้อง (ภายใน)";
                    workSheet.Cells[HeadRow, 10].Value = "ฟ้อง (ภายใน)";
                    workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    
                    foreach (var data in ListDataSendmail)
                    {
                        var Acc = ListAccount.Where(W=>W.RelationCifNo == data.CifNo).ToList();
                        foreach (var LSAcc in Acc)
                        {
                            workSheet.Cells[row, 1].Value = No++;
                            workSheet.Cells[row, 2].Value = data.Topic;// "เรื่อง";
                            workSheet.Cells[row, 3].Value = LSAcc.Account.ToString();// "ACCOUNT";
                            workSheet.Cells[row, 4].Value = data.CifNo;
                            workSheet.Cells[row, 5].Value = data.CFNAME;
                            workSheet.Cells[row, 6].Value = data.SpecialCase;// "คดีพิเศษ (บสย.)";
                                                                             //workSheet.Cells[row, 7].Value = data.SendDocDate;// "วันที่ส่งเมลให้ OUTSOURCE";
                            workSheet.Cells[row, 7].Value = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH")); ;// "วันที่ส่งเมลให้ OUTSOURCE";
                            workSheet.Cells[row, 8].Value = data.NoticeDate;// "Notice";
                            workSheet.Cells[row, 9].Value = data.ActionIndictDate;// "กำหนดฟ้อง (ภายใน)";
                            workSheet.Cells[row, 10].Value = data.IndictDate;// "ฟ้อง (ภายใน)";
                            row++;
                        }
                    }
                    workSheet.Column(1).AutoFit();
                    workSheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(2).AutoFit();
                    workSheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Column(3).AutoFit();
                    workSheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(4).AutoFit();
                    workSheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(5).AutoFit();
                    workSheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Column(6).AutoFit();
                    workSheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(7).AutoFit();
                    workSheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(8).AutoFit();
                    workSheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(9).AutoFit();
                    workSheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Column(10).AutoFit();
                    workSheet.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    FileStream objFileStrm = File.Create(Filename);
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(Filename, excel.GetAsByteArray());
                }
            }
            catch { throw; }
        }
    }
}