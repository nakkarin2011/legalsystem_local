﻿var _Legal = _Legal || {};
_Legal.TrayAction = _Legal.TrayAction || (function () {

    var cfg  = {};
    var vue  = {};

    var _config = {
        Init: function (cfg) {

            this.cfg    = cfg;
            this.vue    = cfg.vue || {};
            this.fn     = cfg.fn; 

            return _config.Action = new Action(cfg.actionCode);
        }

    };


    function Action(code) {

        var result = { success: true, message: "successful" };

        if (Action.prototype[code]) {
            result = Action.prototype[code]();
        }

        else
        {
            result.success = false;
            result.message = `Not implement this action ${code}!`;
        }


        //hanlder return result;
        if (_config.fn) {
            _config.fn(result);
        }

        else
        {
            console.log(result);
            return result;
        }
    }; 

    Action.prototype = {

        TA001: function () {
            const _vue = _config.vue; 
            console.log("You call first letter action.");

            if (_vue.permission.IsApprove) {

                if (_vue.model.DocumentStatusNo == 3 || _vue.model.DocumentStatusNo == 4)
                {
                    _vue.action.IsDisable = true;
                }

                else
                {
                    _vue.isPrintForm = true;
                }

            }

            else
            {
                if (_vue.model.DocumentStatusNo == 3 || _vue.model.DocumentStatusNo == 4) {
                    _vue.isPrintForm = true;
                }

                else {
                    _vue.isFirstLetterForm = true
                }
            }


 

 
            //if (_vue.model.DocumentStatusNo == 3 || _vue.model.DocumentStatusNo == 4) {
            //    _vue.isPrintForm = true;
            //}

            //else
            //{
            //    if (_vue.permission.IsApprove) {
            //        _vue.isPrintForm = true;
            //    }

            //    else
            //    {
            //        _vue.isFirstLetterForm = true
            //    }
            //}

            return this.result;
        },

        TA002: function () {
            const _vue = _config.vue; 
            console.log("You call cover page action.");
            _vue.isCoverPageForm = true;

            return this.result;
        },

        TA006: function () {

            const _vue = _config.vue; 

            console.log("You call notice action.");
 
            _vue.isPrintForm = true;

            return this.result;
        }
    }
  
    return _config;
 
}(function () { }));
