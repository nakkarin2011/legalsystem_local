﻿var _Legal = _Legal || {};
_Legal.Data = _Legal.Data || (function () {

    var _config = {
        init: function () {

        },

        Ajax: function (params) {

            var opt         = params || arguments;

            opt.loadMask    = opt.loadMask || false;
            opt.url         = opt.url || '/'
            opt.method      = opt.method || 'post'
            opt.signature   = opt.signature || '*191*'

            if (opt.loadMask) {
                $('.se-pre-con').fadeIn(100);
            }

            return $.ajax({
                url: opt.url,
                type: opt.method, //send it through get method
                data: JSON.stringify(opt.params),
                beforeSend: function (request) {
                    request.setRequestHeader("signature", opt.signature);
                },
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                cache: false,
                success: function (res) {

                    $('.se-pre-con').fadeOut(1000, function () {
                        var success = opt.success || function () { console.log("No implement callback function when success.")};
                        success(res);
                    });
                 },

                error: function (xhr) {
                    $('.se-pre-con').fadeOut(1000, function () {
                        var error = opt.error || function () { console.log("No implement callback function when error.") };
                         error(xhr);
                    });
                }
            });
        }
    };

    return _config;

}(function () { }));
