﻿//console.log('test');
$('a#logout').click(function () {
    var linkURL = $(this).attr("href");
    console.log(linkURL)

    event.preventDefault();
    swal({
        title: "Message",
        text: "ต้องการออกจากระบบหรือไม่??",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง'
    })
        .then((result) => {
            if (result.value) {
                window.location = linkURL;
            }
        });
});

function ShowConfirmDialog(btnType) {
    if (btnType.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnType.dataset.confirmed = false;
        return true;
    } else {
        // Ask the user to confirm/cancel the action
        event.preventDefault();
        swal({
            title: "Message",
            text: "ต้องการ " + btnType.value + " ข้อมูลหรือไม่?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
        })
            .then((result) => {
                if (result.value) {
                    // Set data-confirmed attribute to indicate that the action was confirmed
                    btnType.dataset.confirmed = true;
                    // Trigger button click programmatically
                    btnType.click();
                }
                //else {
                //    swal("Cancelled", "", "success");
                //}
            });
    }
}

function ConfirmAssign(btnType) {
    if (btnType.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnType.dataset.confirmed = false;
        return true;
    } else {
        // Ask the user to confirm/cancel the action
        event.preventDefault();
        swal({
            title: "Message",
            text: "ต้องการ " + btnType.value + " หรือไม่?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
        })
            .then((result) => {
                if (result.value) {
                    // Set data-confirmed attribute to indicate that the action was confirmed
                    btnType.dataset.confirmed = true;
                    // Trigger button click programmatically
                    btnType.click();

                }
                //else {
                //    swal("Cancelled", "", "success");
                //}
            });
    }
}


function ConfirmDialogReqOperPage(btnType) {
    debugger
    if (btnType.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnType.dataset.confirmed = false;
        return true;
    } else {
        // Ask the user to confirm/cancel the action
        event.preventDefault();
        swal({
            title: "Message",
            text: "ต้องการส่งข้อมูลหรือไม่?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
        })
            .then((result) => {
                if (result.value) {
                    // Set data-confirmed attribute to indicate that the action was confirmed
                    btnType.dataset.confirmed = true;
                    // Trigger button click programmatically
                    btnType.click();
                }
                //else {
                //    swal("Cancelled", "", "success");
                //}
            });
    }
}


function openModal() {
    $('#myModal').modal('show');
}
function closeModal() {
    $('#myModal').modal('hide');
}

