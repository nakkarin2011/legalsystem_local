﻿const _Enum = {
    Message: {
        PleaseSelect: "Please Select",
        NotDelete: "ไม่สามารถลบข้อมูลได้...",
        Duplicate: "ข้อมูลซ้ำ...",
        MaxlengthPercent: "ข้อมูลเปอร์เซ็นต์ต้องอยู่ในช่วง 0-100 เท่านั้น",
        MinNumber: "ข้อมูลตัวเลขไม่สามารถใส่ค่าติดลบได้",
        InvalidRange: "ค่า RangeForm ต้องมากกว่าค่า RangeTo",
        InvalidMaxMin: "ค่า Max ต้องมากกว่าค่า Min"
    },
    Operation: {
        EDIT: "EDIT",
        APPROVED: "APPROVED",
        CREATE: "CREATE",
        UPDATE: "UPDATE",
        DELETE: "DELETE",
    },
    Role: {
        ADMIN: "ADMIN",
        MAKER: "MAKER",
        APPROVER: "APPROVER",
        VIEWER: "VIEWER",
    },
    Setting: {
        DataTable: {
            PageLength: 5,
            ASC: "asc",
            DESC: "desc"
        }
    }
};

var notification;
function blockUI() {
    Loading();
}
function unblockUI() {
    Loaded();
}

function Loading() {
    $('body').loading();
}

function Loaded() {
    $('body').loading('stop');
}

function toasterOptions() {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",

    };
};

function toastrSuccess(message) {
    toasterOptions();
    toastr.success(message);
};

function toastrError(message) {
    toasterOptions();
    toastr.error(message);
};

function toastrWarning(message) {
    toasterOptions();
    toastr.warning(message);
}

function iCheckUpdate() {
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
}

function genID(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function isNull(value) {
    return (typeof value === 'undefined' || value == undefined || value == null || value == '' || value == "");
}

function alertConfirm(callback) {
    var dfd = new jQuery.Deferred();
    $.confirm({
        title: 'Confirm!',
        content: 'Do you want to save change data?',
        type: 'blue',
        buttons: {
            hey: {
                text: 'Yes',
                btnClass: 'btn-blue',
                action: function () {
                    dfd.resolve();
                    callback();
                }
            },
            heyThere: {
                text: 'Cancel',
                action: function () {
                    dfd.reject();
                }
            }
        }
    });
    return dfd.promise();
}

function alertConfirmDelete(callback, callbackreject) {
    var dfd = new jQuery.Deferred();
    $.confirm({
        title: 'Confirm Delete! ',
        content: 'Do you want to delete all data?',
        type: 'red',
        buttons: {
            hey: {
                text: 'Yes',
                btnClass: 'btn-red',
                action: function () {
                    dfd.resolve();
                    callback();
                }
            },
            heyThere: {
                text: 'Cancel',
                action: function () {
                    dfd.reject();
                    if (callbackreject) {
                        callbackreject();
                    }
                }
            }
        }
    });
}

function alertConfirmDelete2(title, content, callback, callbackreject) {
    var dfd = new jQuery.Deferred();
    $.confirm({
        title: isNull(title) ? 'Confirm Delete! ' : title,
        content: isNull(content) ? 'Do you want to delete data?' : content,
        type: 'red',
        buttons: {
            hey: {
                text: 'Yes',
                btnClass: 'btn-red',
                action: function () {
                    dfd.resolve();
                    callback();
                }
            },
            heyThere: {
                text: 'Cancel',
                action: function () {
                    dfd.reject();
                    if (callbackreject) {
                        callbackreject();
                    }
                }
            }
        }
    });
}

function _resetForm($form) {
    //$form[0].reset();

    var readonly = $form.find(':input')
    $.each(readonly, function (i, val) {
        $(this).val('');
    });


    var readonly = $form.find('input[type=text][readonly]')
    $.each(readonly, function (i, val) {
        $(this).prop('readonly', false);
    });

    var select2 = $form.find('select');
    $.each(select2, function (i, val) {
        $(this).parent().find('.select2-selection--single').removeAttr('style');

        $(this).prop('selectedIndex', 0);
        $(this).trigger('change');
    });

    $.each($form.find('.datePicker'), function (i, e) {
        $(this).datepicker('setStartDate', false);
        $(this).datepicker('setEndDate', false);
    });

    $.each($form.find('.text-red'), function (i, e) {
        var _id = $(this).attr('id');
        if (_id != undefined) {
            $(this).removeClass('text-red');
        }
    });

    $.each($('.has-error'), function (i, e) {
        $(e).removeClass('has-error');
    });
    $.each($('.help-block'), function (i, e) {
        $(e).removeClass('help-block');
    });
}

function resetSelector($form) {

    var input = $form.find('input');
    $.each(input, function (i, val) {
        $(this).val(null);
    });

    var textarea = $form.find('textarea');
    $.each(textarea, function (i, val) {
        $(this).val(null);
    });

    var hidden = $form.find('hidden');
    $.each(hidden, function (i, val) {
        $(this).val(null);
    });

    var select2 = $form.find('select');
    $.each(select2, function (i, val) {
        $(this).prop('selectedIndex', 0);
        $(this).trigger('change');
    });

    $.each($('.has-error'), function (i, e) {
        $(e).removeClass('has-error');
    });
    $.each($('.help-block'), function (i, e) {
        $(e).removeClass('help-block');
    });
}

function initialSelect2($placeholder) {
    var $input = $('select');
    $.each($input, function (index, val) {
        if ($(this).find('option:first').val() == "") {
            $(this).select2({
                placeholder: isNull($placeholder) ? "All" : $placeholder,
                allowClear: true
            });
        } else {
            $(this).select2();
        }
    });
}

function initSelect2($selector) {
    var $input = $selector.find('select');
    $.each($input, function (index, val) {
        if ($(this).find('option:first').val() == "") {
            $(this).select2({
                placeholder: 'All',
                allowClear: true
            });
        } else {
            $(this).select2();
        }
    });
}

function datePicker() {
    $('.datePicker').datepicker({
        //todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
}

function setValue($input, $value) {
    $.each($input, function (index, val) {
        if ($(this).val() == "") {
            $(this).val($value);
        }
    });
}

function submitFormSearch($form, callback) {
    $form.on("submit", function () {
        if (callback) {
            callback();
        }
        return false;
    });
}

function postFormData($url, $formData, callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: $url,
        dataType: "json",
        data: $($formData).serialize(),
        //traditional: true,
        beforeSend: function () { blockUI(); },
        success: function (res) {
            var success = false;
            var message = 'The operation failed';
            if (res != null && res != undefined) {
                success = res.status;
                message = res.message != '' ? res.message : message;
            }
            if (success) {
                toastrSuccess(message);
            } else {
                //toastrError(message);
                swal("เกิดข้อผิดพลาด", message, "error");
            }

            callback(res);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            SessionExpired(xhr);
        },
        complete: function () {
            unblockUI();
        },
        async: true
    });
}

function postObjData($url, objs, callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: $url,
        dataType: "json",
        data: objs,
        //traditional: true,
        beforeSend: function () { blockUI(); },
        success: function (res) {
            var success = false;
            var message = 'The operation failed';
            if (res != null && res != undefined) {
                success = res.status;
                message = res.message != '' ? res.message : message;
            }
            if (success) {
                toastrSuccess(message);
            } else {
                swal("เกิดข้อผิดพลาด", message, "error");
                //toastrError(message);
            }

            if (callback) {
                callback(res);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            SessionExpired(xhr);
        },
        complete: function () {
            unblockUI();
        },
        async: true
    });
}

function submitObjData($url, objs, callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: $url,
        dataType: "json",
        data: objs,
        //traditional: true,
        beforeSend: function () { blockUI(); },
        success: function (res) {
            //var success = false;
            //var message = 'The operation failed';
            //if (res != null && res != undefined) {
            //    success = res.status;
            //    message = res.message != '' ? res.message : message;
            //}
            //if (success) {
            //    toastrSuccess(message);
            //} else {
            //    toastrError(message);
            //}

            if (callback) {
                callback(res);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            SessionExpired(xhr);
        },
        complete: function () {
            unblockUI();
        },
        async: true
    });
}

function ajaxGetData($url, objs, callback, ignoreloading) {
    let renderLoading = true;
    if (!isNull(ignoreloading)) {
        if (ignoreloading === true) {
            renderLoading = false
        }
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: $url,
        dataType: "json",
        data: objs,
        beforeSend: function () {
            if (renderLoading) {
                blockUI();
            }

        },
        success: function (res) {
            var success = false;
            var message = 'The operation failed';
            if (res != null && res != undefined) {
                success = res.status;
                message = res.message != '' ? res.message : message;
            }
            if (!success) {
                toastrError(message);
            }
            callback(res);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            SessionExpired(xhr);
        },
        complete: function () {
            if (renderLoading) {
                unblockUI();
            }

        },
        async: true
    });
}

function DropDownList(url, $selector, callback) {
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        dataType: "json",
        data: null,
        traditional: true,
        beforeSend: function () { },
        success: function (res) {
            var success = false;
            var message = 'The operation failed';
            if (res != null && res != undefined) {
                success = res.status;
                message = res.message != '' ? res.message : message;
            }
            if (success) {
                //toastrSuccess(message);
            } else {
                toastrError(message);
            }

            $.each(res.data, function (index, obj) {
                $selector.append(new Option(obj.Text, obj.Value));
            });

            if (callback) {
                callback(res);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastrError("เกิดข้อผิดพลาด: " + thrownError);
            unblockUI();
        },
        complete: function () {
            $selector.select2();
        },
        async: true
    });
}

function DropDownList2($selector, obj, callback) {
    $.each(obj, function (index, item) {
        $selector.append(new Option(item.Text, item.Value));
    });
    $selector.select2({
        placeholder: 'All',
        allowClear: true
    });

    if (callback) {
        callback();
    }
}

function DropDownList3($selector, obj, callback) {
    $.each(obj, function (index, item) {
        $selector.append(new Option(item.Text, item.Value));
    });
    $selector.select2();

    if (callback) {
        callback();
    }
}

function Select2LinQ($selector, obj, where) {
    var datas = JSLINQ(obj)
        .Where(function (item) { return item.ProductType == where; })
        .Select(function (x) {
            return {
                Value: x.TransactionTypeId,
                Text: x.TransactionType1
            }
        });

    $selector.empty();
    if (datas) {
        datas.each(function (obj) {
            $selector.append("<option value='" + obj.Value + "' selected>" + obj.Text + "</option>");
            $selector.trigger('change');
        });
        $selector.prop('selectedIndex', 0).trigger('change');
    }
}

function FeeTypeSelect2LinQ($selector, obj, productType) {
    var datas = JSLINQ(obj)
        .Where(function (item) { return item.ProductType == productType; })
        .Select(function (x) {
            return {
                Value: x.FeeTypeId,
                Text: x.FeeType1
            }
        });

    $selector.empty();
    if (datas) {
        datas.each(function (obj) {
            $selector.append("<option value='" + obj.Value + "' selected>" + obj.Text + "</option>");
            $selector.trigger('change');
        });
        $selector.prop('selectedIndex', 0).trigger('change');
    }
}


function dataTablex(option, rowAuto) {
    let _table = option.selector.dataTable({
        searching: false,
        lengthChange: false,
        processing: false,
        pageLength: 30,
        serverSide: true,
        info: true,
        ordering: false,
        ajax: {
            url: option.url,
            type: "POST",
            data: function (d) {
                return option.formsearch.serializeArray().reduce(function (a, x) {
                    d[x.name] = x.value;
                    return d;
                }, {});
            },
            beforeSend: function () {
                blockUI();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastrError("เกิดข้อผิดพลาด: " + thrownError);
                unblockUI();
            },
            complete: function () {
                unblockUI();
            }
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var info = this.api().page.info();
            $('td:eq(0)', nRow).html(iDisplayIndex + info.start + 1).addClass("text-center");
            return nRow;
        },
        columns: option.columns,
        order: option.order
    });
    return _table;
}

function FunctioCallback(option, _table) {
    $('.delete').on('click', function (e) {
        e.preventDefault();
        var data = _table.fnGetData($(this).parents('tr')[0]);
        data.Action = 3;
        swal({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "ลบข้อมูล  !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: "ยกเลิก",

        }, function (isConfirm) {
            if (isConfirm) {
                var path = option.urlDel;
                var filterParameter = data;
                var request = DataService.PostModel(path, filterParameter);
                $.when(request).done(function (response) {
                    if (response.result) {
                        _table.fnDraw();
                        ShowMessage('Success!', response.message, 'success');
                    }
                    else {
                        ShowMessage('Error!', response.message, 'error');
                    }
                });
            }
        });
    });

    $('.view').on('click', function () {
        var data = _table.fnGetData($(this).parents('tr')[0]);
        setReadOnly(data, function () {
            $(option.viewreadonly.modal).modal('show');
        });
    });
}

function initEdit(option) {
    if (isNull(option.obj)) return;
    let $obj = option.obj;
    let $form = option.selector;
    let $input = $form.find('input[type=hidden], input[type=text], textarea, select,input[type=number], input[type=checkbox]');
    $.each($input, function (indexInArray, val) {
        var $this = $(this);
        let $name = $(this).attr('name');
        if ($name !== undefined) {
            $name = $name.split('.').pop();
            if ($name == "Operation" || $name == "operation") {
                return true;
            }

            if ($this.is("input") || $this.is("textarea") || $this.is("select")) {

                if ($this.is('.datePicker')) {
                    var val = $obj[$name];
                    if (val) {
                        if (moment(val, moment.ISO_8601).isValid()) {
                            var dateFormat = moment(val).format('DD/MM/YYYY');
                            $this.datepicker("setDate", dateFormat);
                        }
                        else {
                            $this.val(val);
                        }
                    }
                } else {
                    if ($this.is(":checkbox")) {
                        var val = ($obj[$name] == "1" || $obj[$name] == true) ? true : false;
                        $this.prop("checked", val);
                    } else {
                        $(this).val($obj[$name]);
                        if ($this.is("select")) {
                            $(this).trigger('change');
                        }
                    }
                }
            }
        }
    });
    //$($form).find("input[name*='Action']").val(2);
}

function setReadonly() {
    //$formUserMaster.find("#UserId").prop('readonly', true);
}

function setDateToDisplay(datetime) {
    if (moment(datetime, moment.ISO_8601).isValid()) {
        return moment(datetime).format('DD/MM/YYYY');
    }
    else {
        //console.log("Datetime is invalid: " + datetime);
        return "";
    }
}

function validation($selector, callback) {

    var rules = new Object();
    var messages = new Object();
    $selector.find('input[required], select[required]').each(function () {
        rules[this.name] = { required: true };
        messages[this.name] = { required: " " };
    });

    $selector.validate({
        errorElement: 'span',
        focusInvalid: false,
        rules: rules,
        messages: messages,
        invalidHandler: function (event, validator) {

        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            if (element.attr("data-error-container")) {
                error.appendTo(element.attr("data-error-container"));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function ($form) {
            if (callback) {
                callback($form);
            }
            return false;
        }
    });
}

function intiHighlightText($selector, obj, bgCorlor) {
    $.each(obj, function ($name) {
        var _nameE = `${$name}E`;
        if (obj[_nameE] == 1) {
            var $this = $selector.find(`[name='${$name}']`);
            if ($this.is("input") || $this.is("textarea")) {
                $this.addClass("text-red");
            }
            if ($this.is("select")) {
                var _id = $selector.find('[id*="select2-' + $name + '-"]').attr('id');

                if (obj[$name] == "" || obj[$name] == null) {

                    if ($selector.find("#" + _id).text() == jqEnum.Message.PleaseSelect && bgCorlor) {
                        $selector.find("#" + _id).closest('.select2-selection--single').css('background-color', '#f6e2e2');
                    } else {
                        $selector.find("#" + _id).find('span').addClass("text-red");
                    }

                } else {

                    if (bgCorlor) {
                        $selector.find("#" + _id).closest('.select2-selection--single').css('background-color', '#f6e2e2');
                    } else {
                        var $this = $selector.find('[id*="select2-' + $name + '-"]');
                        $this.addClass("text-red");
                    }
                }
            }
        }
    });
}

function focusError($selector) {
    $selector.find('.has-error').first().find('input').focus();
}

function getDatainTable($selector, callback) {
    let objs = new Array();

    $.each($selector.find('tr'), function () {
        var row = new Object();
        $.each($(this).find('input, select'), function (e, x) {
            row[x['name']] = $(this).is(':checkbox') ? $(this).prop('checked') : x['value'];
        });

        if (!jQuery.isEmptyObject(row)) {
            objs.push(row);
        }
    });

    return objs;
}

function getDatainTableRow($selector, callback) {
    let row = new Object();
    let $this = $selector.closest('tr').find('input, select').serializeArray();
    $.each($this, function (e, x) {
        row[x['name']] = $(this).is(':checkbox') ? $(this).prop('checked') : x['value'];
    });
    return row;
}

function initColorReadonly($selector) {
    $selector.find('a').removeClass('text-red').css('color', '#ddd');

}

function handleEventValidate($selector) {
    let flagStatus = false;
    if (isNull($selector.val())) {
        $selector.closest('.custom-validate').addClass('has-error');
        flagStatus = false;
    }
    else {
        $selector.closest('.custom-validate').removeClass('has-error');
        flagStatus = true;
    }
    $selector.change(function () {
        if (isNull($(this).val())) {
            $(this).closest('.custom-validate').addClass('has-error');
            return false;
        }
        else {
            $(this).closest('.custom-validate').removeClass('has-error');
            return true;
        }
    });
    return flagStatus;
}

function initDatepickerStartEnd($selectorStart, $selectorEnd) {
    $selectorStart.change(function () {
        if (!isNull($(this).val())) {
            $selectorEnd.datepicker('setStartDate', $(this).val());
        } else {
            $selectorEnd.datepicker('setEndDate', false);
        }
    });

    $selectorEnd.change(function () {
        if (!isNull($(this).val())) {
            $selectorStart.datepicker('setEndDate', $(this).val());
        } else {
            $selectorStart.datepicker('setEndDate', false);
        }
    });
}

function axiosPost($url, objs, flagloading, flagtoast) {
    let loading = true;
    if (!_.isEmpty(flagloading)) {
        loading = flagloading;
    }

    if (loading) {
        axios.interceptors.request.use(function (config) {
            blockUI();
            return config;
        });
        axios.interceptors.response.use(function (response) {
            unblockUI();
            return response;
        });
    }

    return axios.post($url, $.param(objs))
        .then(function (response) {
            if (response.data.status) {
                if (!_.isUndefined(flagtoast)) {
                    if (flagtoast) {
                        toastrSuccess(response.data.message);
                    }
                } else {
                    toastrSuccess(response.data.message);
                }
            } else {
                swal("เกิดข้อผิดพลาด", response.data.message, "error");
            }
            return response.data;
        }).catch(function (error) {
            //swal("เกิดข้อผิดพลาด", error, "error");
            if (error.response.status == 401) {
                window.location.href = error.response.headers.location;
            }
            if (error.response.status == 400) {
                swal("Cross-site Scripting", error.response.data, "error");

            }
            unblockUI();
        });
}

function generateGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function findDuplicateGroups(arrayObject, propertiesName) {

    propertiesName = _.filter(propertiesName, function (item) {
        return !isNull(item);
    });

    if (!_.isArray(arrayObject)) {
        toastrError("เกิดข้อผิดพลาด");
        console.log("Error Parameter arrayObject is not Array!");
        return [];
    }
    if (!_.isArray(propertiesName)) {
        toastrError("เกิดข้อผิดพลาด");
        console.log("Error Parameter propertiesName is not Array!");
        return [];
    }
    let flattenArray = _.flatten(arrayObject);
    for (let i = 0; i < flattenArray.length; i++) {
        for (let j = 0; j < propertiesName.length; j++) {
            if (!_.has(flattenArray[i], propertiesName[j])) {
                toastrError("เกิดข้อผิดพลาด");
                console.log("Error Parameter " + propertiesName[j] + " is not matched!");
                return [];
            }
        }
        break;
    }
    let grouPings = [];
    let groupByProperties = _.groupBy(flattenArray, function (item) {
        let properties = "";
        for (let i = 0; i < propertiesName.length; i++) {

            let value = isNull(item[propertiesName[i]]) ? null : item[propertiesName[i]];
            if (propertiesName[i] == "CCY" && (value == "OTH" || isNull(value))) {
                value = '';
            }

            if (properties) {
                properties += "#" + value;
            }
            else {
                properties += value;
            }
        }

        let str = item.CardID + properties;
        if (!_.contains(grouPings, str) && !_.isEmpty(item.CardID)) {
            grouPings.push(str);
        }

        return properties;
    });
    let duplicateGroup = [];
    _.map(groupByProperties, function (group) {
        if (group.length > 1) {
            let groupObject = { items: [] };
            for (let i = 0; i < propertiesName.length; i++) {
                groupObject[propertiesName[i]] = group[0][propertiesName[i]];
                groupObject.items = group;
            }
            duplicateGroup.push(groupObject);
        }
    });
    duplicateGroup.totalCard = grouPings.length;
    return duplicateGroup;
}
function customVeeValidateScope(vueinstane, scope) {
    return new Promise((resolve, reject) => {
        try {
            vueinstane.$validator.validateAll(scope).then(function (res) {
                if (res) {
                    let objRecheck = vueinstane.$validator.errors.items.filter((r) => r['scope'] == scope);
                    if (objRecheck.length > 0) {
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                } else {
                    resolve(false);
                }
            });
        }
        catch (e) {
            reject(e);
        }

    });
}

function _Ajax($url, $data, $type, callback) {
    $type = isNull($type) ? 'POST' : $type;
    $.ajax({
        async: true,
        type: $type,
        url: $url,
        data: JSON.stringify($data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        cache: false,
        success: function (data) {
            let response = null;
            try {
                let response = jQuery.parseJSON(data);
                if (callback) {
                    callback(response);
                }
            }
            catch (err) {
                response = data;
                if (callback) {
                    callback(response);
                }
            }
        },
        error: function (data, errorThrown) {
            console.log(data);
            console.log(errorThrown);
        },
        beforeSend: function () {
            //loading();
            $(".se-pre-con").show();
        },
        complete: function () {
            //unloading();
            $(".se-pre-con").hide();
        },
    });
}

function _getData_in_Div($form) {
    let objs = new Object();
    $.each($($form).find('input'), function (e, x) {
        objs[x['name']] = x['value'];
    });
    return objs;
}

function _generate_html_dataTable($id, $option) {
    let _th = '';
    $.each($option.columns, function (i, item) {
        _th += `<th style="font-weight: bold;">${item.title}</th>`
    });
    return `<table class="table table-striped table-no-bordered table-hover" id="${$id.replace(".", "").replace("#", "")}">
        <thead>
            <tr>
               ${_th}
            </tr>
        </thead>
        <tbody></tbody>
    </table>`;
}

function _dataTable($scope, $id_dataTable, $form_search, $option) {
    if ($option.autoRow) {
        $option.columns.unshift({ title: "No", data: null, sClass: "text-center", bSortable: false });
    }
    if ($option.btnEdit) {
        $option.columns.push({
            title: "Edit", data: null, sClass: "text-center",
            render: function (o) {
                return '<a class="btn btn-info" data-action=' + _Enum.Operation.EDIT + ' href="javascript:;"><i class="fa fa-edit"></i></a>';
            }
        });
    }
    if ($option.btnDelete) {
        $option.columns.push({
            title: "Delete", data: null, sClass: "text-center",
            render: function (o) {
                return '<a class="btn btn-danger" data-action=' + _Enum.Operation.DELETE + ' href="javascript:;"><i class="fa fa-trash"></i></a>';
            }
        });
    }

    $($scope).append(_generate_html_dataTable($id_dataTable, $option));

    return $($id_dataTable).dataTable({
        lengthChange: false,
        processing: false,
        searching: false,
        pageLength: 10,
        serverSide: true,
        responsive: true,

        ajax: {
            async: true,
            contentType: "application/json; charset=utf-8",
            url: $option.url,
            type: "POST",
            data: function (d) {
                return JSON.stringify({
                    option: d,
                    model: _getData_in_Div($form_search)
                });
            },
            dataFilter: function (res) {
                var parsed = JSON.parse(res);
                return JSON.stringify(parsed.d);
            },
            beforeSend: function () {
                $(".se-pre-con").show();
            },
            complete: function (d) {
                $(".se-pre-con").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        },
        columns: $option.columns,
        order: $option.order,
        fnDrawCallback: function (oSettings) {
            $(this).attr("style", "width:100%");
        },

    });
}

function NumberToFixed(obj) {
    var dCal1 = 0;
    dCal1 = ConvertToNumber(obj.value);
    obj.value = dCal1.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}
function ConvertToNumber(sValue) { //แปลงจากตัวอักษรเป็นตัวเลข
    if ((sValue != null) || (sValue != "")) {
        return Number(sValue.replace(/,/g, '')); //เอา comma ออกทุกตัว
    }
    else
        return 0;
}

function _createObj(source, value) {
    let val = isNull(value) ? null : value;
    return _.mapValues(source, () => val);
}

//function convertDATE(objs) {
//    let i = 0;
//    _.forEach(objs, function (value, key) {
//        debugger
//        objs.key] = i; 
//        i++;
//        console.log(obj.key, key);
//    });

//    return objs;
//}

function _moment_DDMMYY(value) {
    if (isNull(value)) {
        return null;
    } else {
        //let result = moment(value).local().format('DD/MM/YYYY');
      
        let result = moment(value);
        if (result.isValid()) {
            value = moment(value).format('DD/MM/YYYY');
            let str = value.split('/');
            result = str[0] + "/" + str[1] + "/" + (parseInt(str[2])+543);
       
        } else {
            result = value;
        }

        return result;

        //debugger
        //moment.locale('th');
        //let result = moment(value);
        //if (result.isValid()) {
        //    result = moment(value).local().format('DD/MM/YYYY');
        //}
        //else {
        //    result = value;
        //}
        //return result;
    }
}
function _genGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function _genID(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function _formatBytes(bytes, b) {
    if (0 == bytes) return "0 Bytes";
    let c = 1024;
    let d = b || 2;
    let e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    let f = Math.floor(Math.log(bytes) / Math.log(c));
    return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f]
}

function _formatBytes(bytes, decimals) {
    if (bytes === 0) return '0 GB'
    if (isNaN(parseInt(bytes))) return bytes
    if (typeof bytes === 'string') bytes = parseInt(bytes)
    if (bytes === 0) return '0';
    const k = 1000;
    const dm = decimals + 1 || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
}