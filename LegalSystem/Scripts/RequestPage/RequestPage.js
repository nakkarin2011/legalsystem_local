﻿var DelayProsecution, DelayProsecutionCancel, LitigationsCancelReason, RequestType;
function checkChange(ACCTNO) {
    console.log(ACCTNO);
    //PageMethods.gvRelationshipUpdate(ACCTNO);
    var gridView = document.getElementById("#chkAccount");
    //var gridViewControls = gridView.getElementsByTagName("input");
    console.log(gridView);
}


function ConfirmDialog(btnType) {
    console.log(btnType.value);
    if (btnType.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnType.dataset.confirmed = false;
        return true;
    } else {
        // Ask the user to confirm/cancel the action
        event.preventDefault();
        swal({
            title: "Message",
            text: "ต้องการ " + btnType.value + " ข้อมูลหรือไม่?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
        })
            .then((result) => {
                if (result.value) {
                    // Set data-confirmed attribute to indicate that the action was confirmed
                    btnType.dataset.confirmed = true;
                    // Trigger button click programmatically
                    btnType.click();
                } else {
                    //swal("Cancelled", "", "success");
                }
            });
    }
}

function GetData () {
    DelayProsecution = $("[id*=txtDelayProsecution]").val();                /*การชะลอการดำเนินคดี*/
    DelayProsecutionCancel = $("[id*=txtDelayProsecutionCancel]").val();    /*การยกเลิกชะลอการดำเนินคดี*/
    LitigationsCancelReason = $("[id*=ddlLitigationsCancelReason]").val();  /*การยกเลิกการดำเนินคดี*/
}

function ValidateSave() {
    RequestType = $("[id*=ddlRequestType]");
    var selectedText = RequestType.find("option:selected").text();
    var selectedValue = RequestType.val();
    GetData();

    switch (RequestType.val()) {
        case "0":
            swal("Message", "กรุณาระบุ Request Type", "warning");
            return false;
            break;
        case '3': /*การชะลอการดำเนินคดี*/
            if (DelayProsecution != '' && DelayProsecution != undefined && DelayProsecution != null) {
                return true;
            }
            else {
                swal("Message", "กรุณาระบุข้อมูล กรณีชะลอดำเนินคดี", "warning");
                return false;
            }
            break;
        case '4': /*การยกเลิกชะลอการดำเนินคดี*/
            if (DelayProsecutionCancel != '' && DelayProsecutionCancel != undefined && DelayProsecutionCancel != null) {
                return true;
            }
            else {
                swal("Message", "กรุณาระบุข้อมูล กรณียกเลิกชะลอการดำเนินคดี", "warning");
                return false;
            }
            break;
        case '5': /*การยกเลิกการดำเนินคดี*/
            if (LitigationsCancelReason != '' && LitigationsCancelReason != undefined && LitigationsCancelReason != null) {
                return true;
            }
            else {
                swal("Message", "กรุณาระบุข้อมูล กรณียกเลิกการดำเนินคดี", "warning");
                return false;
            }
            break;
    }

}

function ConfirmDialogReferCase(btnType) {

    if (btnType.dataset.confirmed) {
        // The action was already confirmed by the user, proceed with server event
        btnType.dataset.confirmed = false;
        return true;
    } else {
        // Ask the user to confirm/cancel the action
        event.preventDefault();
        swal({
            title: "Message",
            text: "ต้องการ " + btnType.value + " ข้อมูลหรือไม่?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง'
        })
            .then((result) => {
                if (result.value) {
                    var i = ValidateSave();
                    if (i == true) {
                        // Set data-confirmed attribute to indicate that the action was confirmed
                        btnType.dataset.confirmed = true;
                        // Trigger button click programmatically
                        btnType.click();
                    }

                } else {
                    //swal("Cancelled", "", "success");
                }
            });
    }

}

//SET Value NUMBER Ex.1,222.00
function keyNumber(val) {
    var str = val.split(".")
    //console.log(str[0]);
    var n = str[0] ? Number(String(str[0]).replace(/[^0-9.-]+/g, "")) : '';
    val = n.toLocaleString() + '.' + str[1];
    return val;
}

function keyChange(event) {
    var n = event.value ? Number(String(event.value).replace(/[^0-9.-]+/g, "")) : '';
    event.value = keyNumber(n.toFixed(2));
}



