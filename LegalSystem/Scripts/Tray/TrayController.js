﻿var TrayController = function (v) {
    this.vue = v;
    this.BottonInitial();
};


TrayController.prototype.BottonInitial = function () {
    if (this.vue.model.isFirstLetter) {
        if (this.vue.model.documentStatusID == 3) {
            this.vue.lblBtnPrint = "Print 1st Letter";
            this.vue.cssPrint = 'fa fa-print'
        }
        else {
            this.vue.lblBtnPrint = "Print 1st Letter";
            this.vue.cssPrint = 'fa fa-file-o'
        }
    }

    if (this.vue.model.isNotice) {
        if (this.vue.model.documentStatusID == 3) {
            this.vue.lblBtnPrint = "Print Notice";
            this.vue.cssPrint = 'fa fa-print'
        }
        else {
            this.vue.lblBtnPrint = "Print Notice";
            this.vue.cssPrint = 'fa fa-file-o'
        }
    }
} 
TrayController.prototype.Api = function (params){
 
    if (params.loadMask) {
        $('.se-pre-con').fadeIn(100);
    }

    return $.ajax({
        url: params.url,
        type: params.method, //send it through get method
        data: JSON.stringify(params.params),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        cache: false,
        success: function (res) {

            $('.se-pre-con').fadeOut(1000, function () {
                params.success(res);
            });
        },
        error: function (xhr)
        {
            $('.se-pre-con').fadeOut(1000, function () {
                params.error(xhr);
            });
        }
    });
}
TrayController.prototype.PrintFirstLetter = function () {
    let self = this;
 
    this.Api({
        url: "/LegalSystem_DEV/Tray/FirstLetter"
        , method: "post"
        , params: {
              id: self.vue.RequestID
            , printMethod: (self.vue.model.documentStatusID == 3) ? 'print' : 'preview'
            , userID: self.vue.model.userID
        }
        , success: function (res) {
            if (res.Success) {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
} 
TrayController.prototype.Prints = function () {
    let self = this;
  
 
    if (self.vue.model.isFirstLetter)
    {

        if (self.vue.model.documentStatusID == 3)
        {
            this.vue.showModal = true;
        }

        else
        {

            if (self.vue.model.isApprove)
            {

                this.vue.showModal = true;
            }

            else {
                this.vue.FirstLetterForm = true
            }

         
        }
      //  this.PrintFirstLetter();
    }
    if (self.vue.model.isNotice)
    {
        //this.PrintNotice();
        this.vue.showModal = true;
    }
} 

TrayController.prototype.PrintDocument = function () {
    let self = this;

    if (self.vue.model.isFirstLetter) {
        this.PrintFirstLetter();
    }
    if (self.vue.model.isNotice) {
       this.PrintNotice();
    }
} 


TrayController.prototype.PrintNotice = function () {
    let self = this;
    console.log("Tray Controller - Print Notice");
    this.Api({
        url: "/LegalSystem_DEV/Tray/PrintNotice"
        , method: "post"
        , params: {
              id: self.vue.RequestID
            , printMethod: (this.vue.model.documentStatusID == 3) ? 'print' : 'preview'
            , userID: self.vue.model.userID
        }
        , success: function (res) {
            if (res.Success)
            {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);
            }

            else
            {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
} 
TrayController.prototype.Print = function () {
    let self = this;
    console.log("Tray Controller - Print");
    this.Api({
        url: "/LegalSystem_DEV/Tray/Print"
        , method: "post"
        , params: {
            id: self.vue.RequestID
            , documentTypeID: self.vue.model.documentTypeID
            , userID: self.vue.model.userID
        }
        , success: function (res) {
            if (res.Success) {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
} 
TrayController.prototype.Send = function () {

    let self = this;
    console.log("Tray Controller - Send");
    self.vue.model.documentStatusID = null;
    this.Api({
        url: "/LegalSystem_DEV/Tray/Send"
        , loadMask : true
        , method: "post"
        , params: {
            id: self.vue.RequestID
        }
        , success: function (res) {
            if (res.Success) {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);

                self.vue.model.documentStatusID = null;
            }
        }
        , error: function (res) {

            toastr.error(res);
            self.vue.model.documentStatusID = null;
        }
    });
} 
TrayController.prototype.Approve = function () {
    let self = this;
    let _event = event;
    console.log("Tray Controller - Approve");
 
    this.Api({
        url: "/LegalSystem_DEV/Tray/Approve"
        , method: "post"
        , params: {
            id: self.vue.RequestID
        }
        , success: function (res) {
            if (res.Success) {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);

                app.$root.disablePrintButton = true;

                self.BottonInitial();
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
} 
TrayController.prototype.Return = function () {
    let self = this;
    console.log("Tray Controller - Return");

    this.Api({
        url: "/LegalSystem_DEV/Tray/Return"
        , method: "post"
        , params: {
            id: self.vue.RequestID
        }
        , success: function (res) {
            if (res.Success) {
                self.vue.model.documentStatusID = res.Data.DocStatusID;

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
   
}

TrayController.prototype.InitialFirstLetter = function (response) {
    let self = this;

    this.Api({
        url: "/LegalSystem_DEV/Tray/InitialFirstLetter"
        , loadMask : true
        , method: "post"
        , params: {
                requestID: self.vue.RequestID
              , userID: self.vue.model.userID
        }
        , success: function (res) {
            if (res.Success) {


                if (response) {
                    response({ data: res.Data});
                }

                toastr.success(res.Message);
            }

            else
            {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
} 

TrayController.prototype.SaveFirstLetterTab = function (response) {
    let self = this;

    this.Api({
        url: "/LegalSystem_DEV/Tray/SaveFirstLetterTab"
        , method: "post"
        , params: {
            requestID: self.vue.RequestID
            , userID: self.vue.model.userID
            , tabs: self.vue.control.tabs
        }
        , success: function (res) {

            if (res.Success) {
                if (response) {
                    response({ data: res.Data });
                }

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {
            toastr.error(res);
        }
    });
} 

TrayController.prototype.requestInformation = function (response) {
    let self = this;

    this.Api({
        url: "/LegalSystem_DEV/Request/Information"
        , method: "post"
        , loadder: false
        , params: {
            requestID: self.vue.RequestID
            , userID: self.vue.model.userID
        }
        , success: function (res) {
            if (res.Success) {


                if (response) {
                    response({ data: res.Data });
                }

                toastr.success(res.Message);
            }

            else {
                toastr.error(res.Message);
            }
        }
        , error: function (res) {

            toastr.error(res);
        }
    });
}